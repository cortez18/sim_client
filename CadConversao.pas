unit CadConversao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ImgList, DB, ADODB, ActnList, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, System.ImageList, System.Actions;

type
  TfrmCadConversao = class(TForm)
    dtsOperacao: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QOperacao: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    edCNPJ: TEdit;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Label1: TLabel;
    QOperacaoREG: TBCDField;
    QOperacaoCNPJ: TStringField;
    QOperacaoDE: TStringField;
    QOperacaoPARA: TStringField;
    QOperacaoDT_INC: TDateTimeField;
    Label2: TLabel;
    edDe: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbPara: TComboBox;
    QOperacaoTransp: TStringField;
    QOperacaoTIPO: TStringField;
    QOperacaoUSER_INC: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure edCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure QOperacaoCalcFields(DataSet: TDataSet);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure dbgContratoDblClick(Sender: TObject);
  private
    quant: integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadConversao: TfrmCadConversao;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadConversao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadConversao.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  edDe.Text := '';
  cbPara.ItemIndex := -1;
  edCNPJ.SetFocus;
end;

procedure TfrmCadConversao.btnSalvarClick(Sender: TObject);
VAR
  COD: integer;
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('select (nvl(max(reg),0)+1) as codigo from tb_conversao');
      dtmDados.iQuery1.open;
      COD := dtmDados.iQuery1.FieldByName('codigo').value;
      QOperacao.append;
      QOperacaoREG.value := COD;
      QOperacaoUSER_INC.AsString := GLBUSER;
    end
    else
    begin
      QOperacao.edit;
    end;
    QOperacaoCNPJ.value := edCNPJ.Text;
    QOperacaoDE.value := edDe.Text;
    QOperacaoPARA.value := cbPara.Text;
    QOperacaoTIPO.value := 'V';
    QOperacao.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QOperacao.Close;
  QOperacao.open;
end;

procedure TfrmCadConversao.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QOperacao.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadConversao.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QOperacao.Close;
  if Pos('order by', QOperacao.sql.Text) > 0 then
    QOperacao.sql.Text := Copy(QOperacao.sql.Text, 1,
      Pos('order by', QOperacao.sql.Text) - 1) + 'order by ' + Column.FieldName
  else
    QOperacao.sql.Text := QOperacao.sql.Text + ' order by ' + Column.FieldName;
  QOperacao.open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadConversao.edCNPJKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0' .. '9', #8]) then
    Key := #0;
end;

procedure TfrmCadConversao.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QOperacao.Cancel;
end;

procedure TfrmCadConversao.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  edCNPJ.Text := QOperacaoCNPJ.value;
  edDe.Text := QOperacaoDE.value;
  cbPara.ItemIndex := cbPara.Items.IndexOf(QOperacaoPARA.value);
  edCNPJ.SetFocus;
end;

procedure TfrmCadConversao.FormCreate(Sender: TObject);
begin
  QOperacao.open;
  quant := QOperacao.RecordCount;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadConversao.QOperacaoCalcFields(DataSet: TDataSet);
begin
  dtmDados.Query1.Close;
  dtmDados.Query1.sql.clear;
  dtmDados.Query1.sql.add('Select nm_fornecedor from tb_fornecedor ');
  dtmDados.Query1.sql.add('where substr(nr_cnpj_cpf,1,8) = :0');
  dtmDados.Query1.Parameters[0].value := QOperacaoCNPJ.value;
  dtmDados.Query1.open;
  QOperacaoTransp.value := dtmDados.Query1.FieldByName('nm_fornecedor')
    .AsString;
  dtmDados.Query1.Close;
end;

procedure TfrmCadConversao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QOperacao.Close;
end;

procedure TfrmCadConversao.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := True;
  end;
end;

procedure TfrmCadConversao.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QOperacao.First;
    2:
      QOperacao.Prior;
    3:
      QOperacao.Next;
    4:
      QOperacao.Last;
  end;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
