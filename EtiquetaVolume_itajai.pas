unit EtiquetaVolume_itajai;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit,
  JvMemoryDataset;

type
  TfrmEtiquetaVolume_itajai = class(TForm)
    QCTe: TADOQuery;
    dtsCTe: TDataSource;
    QCTeNR_CONHECIMENTO: TFMTBCDField;
    QCTeVOLUME: TFMTBCDField;
    mdTemp: TJvMemoryData;
    mdTempCTe: TBCDField;
    mdTempescol: TIntegerField;
    mdTempvolumes: TBCDField;
    QVolTrip: TADOQuery;
    QCTeID_TRIP: TStringField;
    QCTeFL_EMPRESA: TFMTBCDField;
    sp_Box: TADOStoredProc;
    QVol: TADOQuery;
    QVolBOX: TStringField;
    QVolLINHA: TFMTBCDField;
    Panel2: TPanel;
    Gauge1: TGauge;
    Panel3: TPanel;
    JvDBGrid1: TJvDBGrid;
    btRelatorio: TBitBtn;
    btnEtiqueta: TBitBtn;
    lblQt: TLabel;
    QTrips: TADOQuery;
    btnEtiquetaRee: TBitBtn;
    QOrder: TADOQuery;
    QOrderORDCOM: TStringField;
    QTripsNR_CONHECIMENTO: TFMTBCDField;
    QTripsNR_QTDE_VOL: TFloatField;
    QVolTripNR_CONHECIMENTO: TFMTBCDField;
    QVolTripBOX: TStringField;
    QVolTripRAZSOC: TStringField;
    QVolTripCIDADE: TStringField;
    QVolTripESTADO: TStringField;
    QVolTripDATA_REC: TDateTimeField;
    QVolTripCTE_CHAVE: TStringField;
    QVolTripDESTINO: TStringField;
    QVolTripFL_EMPRESA: TFMTBCDField;
    QVolTripLINHA: TFMTBCDField;
    QVolTripORDER_CLI: TStringField;
    QVolDATA_REC: TDateTimeField;
    QVolTripnf: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure btnEtiquetaClick(Sender: TObject);
    procedure QVolTripCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure mdTempFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QVolBeforeOpen(DataSet: TDataSet);
    procedure btnEtiquetaReeClick(Sender: TObject);
    procedure carregar;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtiquetaVolume_itajai: TfrmEtiquetaVolume_itajai;

implementation

uses Dados, Menu, funcoes, AlteraCteEtiqueta;

{$R *.dfm}

procedure TfrmEtiquetaVolume_itajai.btnEtiquetaClick(Sender: TObject);
var Arq : TextFile;
    Ini: TIniFile;
    impr,razao,nr_order : string;
    d,qt,i,seq, vol : integer;
begin
   Screen.Cursor := crHourGlass;
   razao := glbnmfilial;
   if glbimpr = '' then
    impr := 'C:/SIM/printetiqueta.txt'
   else
    impr := glbimpr;
    AssignFile(Arq,impr);
    ReWrite(Arq);
    Application.ProcessMessages;
    mdTemp.filtered := true;
    Application.ProcessMessages;
    mdTemp.First;
    while not mdTemp.eof do
    begin
      Gauge1.MaxValue := 0;
      // volumes da TRIP
      QVolTrip.Close;
      QVolTrip.Parameters[0].Value := mdTempCTe.AsInteger;
      QVolTrip.Open;
      qt := QVolTrip.RecordCount;
      i := 0;
      seq := 0;
      Gauge1.MaxValue := QVolTrip.RecordCount;
      while not QVolTrip.eof do
      begin
        i := i + 1;
        // volumes que est�o no cte
        QVol.close;
        QVol.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
        QVol.Open;
        QVol.Locate('box',QVolTripBOX.AsString,[]);
        seq := QVollinha.AsInteger;
        vol := QVol.RecordCount;
        QVol.close;

        if QVolTripORDER_CLI.AsString = 'S' then
        begin
          QOrder.close;
          QOrder.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
          QOrder.Parameters[1].Value := GLBFilial;
          QOrder.Open;
          nr_order := QOrderORDCOM.AsString;
          QOrder.close;
        end;

        Writeln(Arq,''+'n');
        Writeln(Arq,''+'M0690');
        Writeln(Arq,''+'L');
        Writeln(Arq,'D11');
        Writeln(Arq,'R0000');
        Writeln(Arq,'ySW1');
        Writeln(Arq,'A2');
        Writeln(Arq,'1911S0102520015P009P009INTECOM');
        Writeln(Arq,'1911S0102520149P009P009'+DateTimeToStr(now)+' - 1');
        Writeln(Arq,'1911S0102520299P009P009'+QVolTripLINHA.AsString+'/'+IntToStr(qt));
        Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolTripDESTINO.AsString,1,30));
        Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolTripCIDADE.value,1,25)+'/'+copy(QVolTripESTADO.AsString,1,2));
        Writeln(Arq,'1911S0101730015P014P014BOX : '+QVolTripBOX.AsString);
        Writeln(Arq,'1911S0101280314P009P009Volumes');
        Writeln(Arq,'1911S0100910015P022P022'+QVolTripNR_CONHECIMENTO.AsString);
        Writeln(Arq,'1911S0100910328P020P018'+IntToStr(seq)+' / '+ intToStr(vol));
        Writeln(Arq,'1X1100002470015L346001');
        Writeln(Arq,'1X1100001420015L346001');
        Writeln(Arq,'1X1100001940015L346001');
        Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolTripRAZSOC.AsString,1,30));
        Writeln(Arq,'1eA504500350019C'+QVolTripBOX.AsString);
        Writeln(Arq,'1911S0100110019P008P008NF: '+ QVolTripnf.AsString);
        Writeln(Arq,'1X1100000280020L346001');
        Writeln(Arq,'1911S0101470015P010P010ORDER:'+ nr_order);
        Writeln(Arq,'1911S0101280019P009P009CTE / BOX');
        Writeln(Arq,'Q0001');
        Writeln(Arq,'E');

        sp_Box.Parameters[0].Value :=  QVolTripNR_CONHECIMENTO.AsInteger;
        sp_Box.Parameters[1].Value :=  QVolTripBOX.AsString;
        sp_Box.Parameters[2].Value :=  seq;
        sp_Box.Parameters[3].Value :=  GLBFilial;
        sp_box.ExecProc;
        Gauge1.AddProgress(1);
        QVolTrip.Next;
      end;
      QVolTrip.close;
      mdTemp.Next;
    end;
    CloseFile(Arq);
  Screen.Cursor := crDefault;
  if impr = 'C:/SIM/printetiqueta.txt' then
      //Winexec(PAnsiChar('notepad++.exe '+impr), Sw_Show);
    ShellExecute(Application.Handle, nil, PChar(impr),nil, nil, SW_SHOWNORMAL);
 // end
 // else
 //   ShowMessage('Este CT-e N�o teve todos os seus volumes recebidos !!');
  mdTemp.filtered := false;
  FormCreate(Sender);
end;

procedure TfrmEtiquetaVolume_itajai.btnEtiquetaReeClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar('frmEtiquetaVolume_itajai') then
    Exit;
  try
    Application.CreateForm(TfrmAlteraCteEtiqueta, frmAlteraCteEtiqueta);
    frmAlteraCteEtiqueta.ShowModal;
  finally
    frmAlteraCteEtiqueta.Free;
  end;
  carregar;
end;

procedure TfrmEtiquetaVolume_itajai.btRelatorioClick(Sender: TObject);
begin
  Gauge1.MaxValue := 0;
  lblqt.Caption := '';
  QTrips.close;
  QTrips.open;
  lblqt.Caption := IntToSTr(QTrips.RecordCount) + ' Volumes';
  mdTemp.Close;
  mdTemp.open;

  while not Qtrips.Eof do
  begin
    mdTemp.Append;
    mdTempCTe.Value := QTripsNR_CONHECIMENTO.AsInteger;
    mdTempvolumes.Value := QTripsNR_QTDE_VOL.AsInteger;
    QTrips.Next;
  end;
  QTrips.Close;
  mdTemp.First;
  btRelatorio.Visible := false;
  QTrips.Close;
  mdTemp.First;
end;

procedure TfrmEtiquetaVolume_itajai.carregar;
begin
  Qcte.Close;
  QVolTrip.Close;
  mdTemp.Close;
  sp_box.Close;

  Gauge1.MaxValue := 0;
  lblqt.Caption := '';

  QTrips.close;
  QTrips.open;
  lblqt.Caption := IntToSTr(QTrips.RecordCount) + ' Volumes';
  mdTemp.Close;
  mdTemp.open;

  while not Qtrips.Eof do
  begin
    mdTemp.Append;
    mdTempCTe.Value := QTripsNR_CONHECIMENTO.AsInteger;
    mdTempvolumes.Value := QTripsNR_QTDE_VOL.AsInteger;
    QTrips.Next;
  end;
  QTrips.Close;
  mdTemp.First;
  btRelatorio.Visible := false;
end;

procedure TfrmEtiquetaVolume_itajai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  mdTemp.close;
  Qtrips.close;
end;

procedure TfrmEtiquetaVolume_itajai.FormCreate(Sender: TObject);
begin
  carregar;
end;

procedure TfrmEtiquetaVolume_itajai.JvDBGrid1CellClick(Column: TColumn);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  if (Column.Field = mdTempescol) then
  begin
    mdTemp.edit;
    if (mdTempEscol.IsNull) or (mdTempEscol.value = 0) then
    begin
      mdTempEscol.value := 1;
    end
    else
    begin
      mdTempEscol.value := 0;
    end;
    mdTemp.post;
  end;
end;

procedure TfrmEtiquetaVolume_itajai.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempEscol) then
    begin
      JvDBGrid1.Canvas.FillRect(Rect);
      dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempEscol.value = 1 then
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmEtiquetaVolume_itajai.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not mdTemp.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmEtiquetaVolume_itajai.mdTempFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdTemp['escol'] = 1;
end;

procedure TfrmEtiquetaVolume_itajai.QVolBeforeOpen(DataSet: TDataSet);
begin
  QVol.Parameters[1].Value := GLBFilial;
end;

procedure TfrmEtiquetaVolume_itajai.QVolTripCalcFields(DataSet: TDataSet);
var d, t : Integer;
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select notfis from tb_cte_nf where codcon = :0 and fl_empresa = :1');
  dtmdados.IQuery1.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
  dtmdados.IQuery1.Parameters[1].Value := QVolTripFL_EMPRESA.AsInteger;
  dtmdados.IQuery1.Open;
  d := 0;
  t := 0;
  While not dtmdados.IQuery1.eof do
  begin
    d := d + 1;
    t := t + Length(dtmdados.IQuery1.FieldByName('notfis').AsString+'-');
    if t > 55 then
    begin
      QVolTripnf.Value := QVolTripnf.Value + '...';
      exit;
    end;
    if d = 1 then
      QVolTripnf.Value := dtmdados.IQuery1.FieldByName('notfis').AsString
    else
      QVolTripnf.Value := QVolTripnf.Value + '-'+ dtmdados.IQuery1.FieldByName('notfis').AsString;

    dtmdados.IQuery1.Next;
  end
end;

end.
