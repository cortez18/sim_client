object frmEdiDocCob: TfrmEdiDocCob
  Left = 344
  Top = 206
  Caption = 'Importa'#231#227'o de DocCob'
  ClientHeight = 535
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 171
    Top = 28
    Width = 59
    Height = 16
    Caption = 'Arquivos :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 8
    Top = 2
    Width = 229
    Height = 20
    Caption = 'Selecione o Arquivo a ser Importado :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 28
    Width = 59
    Height = 16
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object PB1: TGauge
    Left = 90
    Top = 504
    Width = 242
    Height = 24
    Progress = 0
  end
  object Label5: TLabel
    Left = 236
    Top = 28
    Width = 6
    Height = 15
    Caption = '  '
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 183
    Top = 262
    Width = 6
    Height = 16
    Caption = '  '
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object btnImportar: TBitBtn
    Left = 0
    Top = 503
    Width = 84
    Height = 24
    Caption = '&Importar'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0041924E003D8F49003A8C44003689400032873C002F84
      37002C813300287F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF004999580045965300419950007DC28F0096D0A60096CFA60078BE
      8900368D42002C813400297F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00519F61004D9C5D0064B47800A8DBB50087CC980066BC7D0064BA7C0086CB
      9800A5D9B40058AA6B002C813400297F3000FF00FF00FF00FF00FF00FF0059A6
      6B0056A366006AB97D00A8DBB20060BC77005CBA730059B8700059B56F0058B5
      6F005BB77400A5D9B3005AAA6C002C823400297F3000FF00FF00FF00FF005DA9
      700053AB6800AADDB40064C179005FBE710060BC7700FFFFFF00FFFFFF0059B8
      700058B56E005CB77400A6DAB400388F43002C823400FF00FF00FF00FF0061AC
      75008ACC980089D396006BC67A0063C1700055AB6500FFFFFF00FFFFFF0059B8
      700059B870005BB9720085CC97007BBE8D0030853900FF00FF00FF00FF0065AF
      7A00A9DDB3007DCF8A0075CC8100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700067BE7D009CD4AB0034883D00FF00FF00FF00FF0069B2
      7E00B6E2BE008BD597007AC98600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700069C17E009DD4AA00388B4200FF00FF00FF00FF006DB5
      8300ACDDB600A6DFAF0081CB8C007CC986006EBD7900FFFFFF00FFFFFF005BAC
      6A0060BC77005CBA73008BD1990080C592003C8E4700FF00FF00FF00FF0070B8
      870085C79700D2EED70095D9A0008AD394007FC88900FFFFFF00FFFFFF0079CD
      85006BC37C006FC77E00ACDFB500459E570040914C00FF00FF00FF00FF0073BA
      8A0070B88700AADAB700D8F1DC0092D89D0088CD930084CC8E008BD496008AD4
      950083D28E00AFE0B7006BB97D004898560044945100FF00FF00FF00FF00FF00
      FF0073BB8B0070B88700AFDCBB00DCF2E000B6E4BD009BDBA50096D9A000A5DF
      AF00C0E8C50079C28A00509E5F004C9B5B00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0073BB8B0071B8870094CEA400C3E6CB00CFEBD400C9E9CE00AFDD
      B8006DB97F0058A5690054A16500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0074BB8B0071B988006EB684006AB3800067B17C0063AE
      770060AB73005CA86E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    ParentFont = False
    TabOrder = 0
    OnClick = btnImportarClick
  end
  object btnSair: TBitBtn
    Left = 338
    Top = 504
    Width = 75
    Height = 24
    Caption = '&Sair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
      9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
      71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
      9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
      FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
      A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
      FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
      AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
      FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
      B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
      FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
      B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
      3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
      BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
      66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
      BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
      47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
      C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
      FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
      C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
      FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
      CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
      FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
      D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
      FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
      D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
      FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
      D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
    OnClick = btnSairClick
  end
  object JvDriveCombo1: TJvDriveCombo
    Left = 0
    Top = 45
    Width = 165
    Height = 22
    DriveTypes = [dtFixed, dtRemote, dtCDROM]
    Offset = 4
    TabOrder = 2
  end
  object JvDirectoryListBox1: TJvDirectoryListBox
    Left = 0
    Top = 66
    Width = 165
    Height = 431
    Directory = 'C:\'
    FileList = FLB
    DriveCombo = JvDriveCombo1
    ItemHeight = 17
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object FLB: TJvFileListBox
    Left = 171
    Top = 45
    Width = 253
    Height = 452
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ItemHeight = 14
    Mask = '*.txt'
    ParentFont = False
    TabOrder = 4
    OnClick = FLBClick
    ForceFileExtensions = False
  end
  object cbTodos: TCheckBox
    Left = 284
    Top = 3
    Width = 97
    Height = 17
    Caption = 'Todos'
    TabOrder = 5
  end
  object Edit1: TEdit
    Left = 38
    Top = 200
    Width = 321
    Height = 21
    TabOrder = 6
    Text = '*.txt'
    Visible = False
  end
  object QEDI: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    Left = 204
    Top = 92
  end
  object QOperacao: TADOQuery
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select *'
      'from tb_operacao'
      'where empresa = :0'
      'order by cod_operacao'
      '')
    Left = 204
    Top = 156
    object QOperacaoOPERACAO: TStringField
      FieldName = 'OPERACAO'
    end
    object QOperacaoCFOP: TStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QOperacaoCOD_OPERACAO: TIntegerField
      FieldName = 'COD_OPERACAO'
    end
    object QOperacaoEMPRESA: TIntegerField
      FieldName = 'EMPRESA'
    end
  end
  object QDoccob: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select z.codclifor'
      
        'from tb_controle_custo z left join cyber.rodcli fo on fo.codclif' +
        'or = z.codclifor'
      'where z.cte_custo = :0'
      'and substr(fnc_cnpj(fo.codcgc),1,8) = :1'
      'and nvl(status,'#39'N'#39') = '#39'N'#39)
    Left = 203
    Top = 220
    object QDoccobCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
    end
  end
  object QTem: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = '1'
        DataType = ftString
        Size = 35
        Value = '0'
      end
      item
        Name = '2'
        DataType = ftString
        Size = 100
        Value = '0'
      end>
    SQL.Strings = (
      'select count(*) tem'
      'from tb_doccob_unb'
      'where identificacao = :0'
      'and remetente = :1'
      'and arquivo = :2')
    Left = 196
    Top = 284
    object QTemTEM: TBCDField
      FieldName = 'TEM'
      ReadOnly = True
      Precision = 32
    end
  end
end
