object frmCadTabelaReceita_upload: TfrmCadTabelaReceita_upload
  Left = 344
  Top = 206
  Caption = 'Upload de Tabela de Custo'
  ClientHeight = 525
  ClientWidth = 397
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 397
    Height = 59
    Align = alTop
    TabOrder = 3
    object Label4: TLabel
      Left = 4
      Top = 26
      Width = 57
      Height = 13
      Caption = 'Caminho :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 8
      Top = 2
      Width = 8
      Height = 16
      Caption = '  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 93
      Top = 18
      Width = 6
      Height = 16
      Caption = '  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel
      Left = 169
      Top = 41
      Width = 58
      Height = 13
      Caption = 'Arquivos :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      AlignWithMargins = True
      Left = 370
      Top = 18
      Width = 8
      Height = 16
      Alignment = taRightJustify
      Caption = '  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object DriveComboBox1: TDriveComboBox
      Left = 1
      Top = 40
      Width = 156
      Height = 19
      DirList = DirectoryListBox1
      TabOrder = 0
    end
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 0
    Top = 59
    Width = 157
    Height = 425
    Align = alLeft
    FileList = FLB
    TabOrder = 0
  end
  object FLB: TFileListBox
    Left = 157
    Top = 59
    Width = 240
    Height = 425
    Align = alClient
    Color = 8404992
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Mask = '*.xlsx'
    ParentFont = False
    TabOrder = 1
    OnClick = FLBClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 484
    Width = 397
    Height = 41
    Align = alBottom
    TabOrder = 2
    object PB1: TGauge
      Left = 93
      Top = 5
      Width = 211
      Height = 30
      Progress = 0
    end
    object btnImportar: TBitBtn
      Left = 3
      Top = 5
      Width = 84
      Height = 30
      Caption = '&Importar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      ParentFont = False
      TabOrder = 0
      OnClick = btnImportarClick
    end
    object btnSair: TBitBtn
      Left = 308
      Top = 5
      Width = 75
      Height = 30
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
        9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
        71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
        9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
        FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
        A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
        FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
        AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
        FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
        B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
        FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
        B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
        3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
        BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
        66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
        BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
        47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
        C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
        FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
        C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
        FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
        CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
        FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
        D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
        FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
        D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
        FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
        D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
      ParentFont = False
      TabOrder = 1
      OnClick = btnSairClick
    end
  end
  object QImp: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <>
    Left = 240
  end
  object QForne: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select razsoc'
      'from cyber.rodcli'
      'where codclifor = :0')
    Left = 236
    Top = 144
    object QForneRAZSOC: TStringField
      FieldName = 'RAZSOC'
      Size = 80
    end
  end
  object Query: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select max(cod_venda) tem'
      'from tb_tabelavenda')
    Left = 300
    Top = 80
    object QueryTEM: TBCDField
      FieldName = 'TEM'
      ReadOnly = True
      Precision = 32
    end
  end
  object Qcadtabela: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'cod_custo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    SQL.Strings = (
      'select *'
      'from tb_tabelavenda')
    Left = 244
    Top = 212
    object QcadtabelaCOD_VENDA: TFMTBCDField
      FieldName = 'COD_VENDA'
      Precision = 38
      Size = 0
    end
    object QcadtabelaCOD_CLIENTE: TFMTBCDField
      FieldName = 'COD_CLIENTE'
      Precision = 38
      Size = 0
    end
    object QcadtabelaFL_LOCAL: TStringField
      FieldName = 'FL_LOCAL'
      FixedChar = True
      Size = 1
    end
    object QcadtabelaDS_UF: TStringField
      FieldName = 'DS_UF'
      Size = 2
    end
    object QcadtabelaFL_LOCAL_DES: TStringField
      FieldName = 'FL_LOCAL_DES'
      FixedChar = True
      Size = 1
    end
    object QcadtabelaDS_UF_DES: TStringField
      FieldName = 'DS_UF_DES'
      Size = 2
    end
    object QcadtabelaVL_FRETE_MINIMO: TBCDField
      FieldName = 'VL_FRETE_MINIMO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_OUTROS_P: TFloatField
      FieldName = 'VL_OUTROS_P'
    end
    object QcadtabelaVL_OUTROS_MINIMO: TBCDField
      FieldName = 'VL_OUTROS_MINIMO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_EXCEDENTE: TBCDField
      FieldName = 'VL_EXCEDENTE'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_AD: TFloatField
      FieldName = 'VL_AD'
    end
    object QcadtabelaVL_GRIS: TFloatField
      FieldName = 'VL_GRIS'
    end
    object QcadtabelaVL_GRIS_MINIMO: TFloatField
      FieldName = 'VL_GRIS_MINIMO'
    end
    object QcadtabelaVL_ENTREGA: TBCDField
      FieldName = 'VL_ENTREGA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaSEGURO_MINIMO: TFloatField
      FieldName = 'SEGURO_MINIMO'
    end
    object QcadtabelaFL_PESO: TStringField
      FieldName = 'FL_PESO'
      FixedChar = True
      Size = 1
    end
    object QcadtabelaFL_IMPOSTO: TStringField
      FieldName = 'FL_IMPOSTO'
      FixedChar = True
      Size = 1
    end
    object QcadtabelaMODAL: TStringField
      FieldName = 'MODAL'
    end
    object QcadtabelaVEICULO: TStringField
      FieldName = 'VEICULO'
    end
    object QcadtabelaSITE: TFMTBCDField
      FieldName = 'SITE'
      Precision = 38
      Size = 0
    end
    object QcadtabelaUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QcadtabelaDT_CADASTRO: TDateTimeField
      FieldName = 'DT_CADASTRO'
    end
    object QcadtabelaCODMUN: TFMTBCDField
      FieldName = 'CODMUN'
      Precision = 38
      Size = 0
    end
    object QcadtabelaCODMUNO: TFMTBCDField
      FieldName = 'CODMUNO'
      Precision = 38
      Size = 0
    end
    object QcadtabelaVL_TDE: TBCDField
      FieldName = 'VL_TDE'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_TR: TFloatField
      FieldName = 'VL_TR'
    end
    object QcadtabelaVL_TDEP: TFloatField
      FieldName = 'VL_TDEP'
    end
    object QcadtabelaVL_TDEM: TBCDField
      FieldName = 'VL_TDEM'
      Precision = 15
      Size = 2
    end
    object QcadtabelaFL_STATUS: TStringField
      FieldName = 'FL_STATUS'
      Size = 1
    end
    object QcadtabelaOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 30
    end
    object QcadtabelaVL_TDEMAX: TBCDField
      FieldName = 'VL_TDEMAX'
      Precision = 15
      Size = 2
    end
    object QcadtabelaOBS: TStringField
      FieldName = 'OBS'
      Size = 1000
    end
    object QcadtabelaREGIAO: TStringField
      FieldName = 'REGIAO'
      Size = 50
    end
    object QcadtabelaVL_SEG_BALSA: TFloatField
      FieldName = 'VL_SEG_BALSA'
    end
    object QcadtabelaVL_REDEP_FLUVIAL: TFloatField
      FieldName = 'VL_REDEP_FLUVIAL'
    end
    object QcadtabelaVL_AGEND: TBCDField
      FieldName = 'VL_AGEND'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_PALLET: TBCDField
      FieldName = 'VL_PALLET'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_TAS: TBCDField
      FieldName = 'VL_TAS'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_DESPACHO: TBCDField
      FieldName = 'VL_DESPACHO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_ENTREGA_PORTO: TFloatField
      FieldName = 'VL_ENTREGA_PORTO'
    end
    object QcadtabelaVL_ALFAND: TFloatField
      FieldName = 'VL_ALFAND'
    end
    object QcadtabelaVL_CANHOTO: TBCDField
      FieldName = 'VL_CANHOTO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_FLUVIAL: TBCDField
      FieldName = 'VL_FLUVIAL'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_FLUVIAL_M: TBCDField
      FieldName = 'VL_FLUVIAL_M'
      Precision = 15
      Size = 2
    end
    object QcadtabelaDT_INICIAL: TDateTimeField
      FieldName = 'DT_INICIAL'
    end
    object QcadtabelaDT_FINAL: TDateTimeField
      FieldName = 'DT_FINAL'
    end
    object QcadtabelaCONTROLER: TStringField
      FieldName = 'CONTROLER'
      Size = 30
    end
    object QcadtabelaDT_CONTROLER: TDateTimeField
      FieldName = 'DT_CONTROLER'
    end
    object QcadtabelaVL_DEVOLUCAO: TBCDField
      FieldName = 'VL_DEVOLUCAO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaFL_RATEIO: TStringField
      FieldName = 'FL_RATEIO'
      Size = 1
    end
    object QcadtabelaVL_FLUVMIN: TBCDField
      FieldName = 'VL_FLUVMIN'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_AJUDA: TBCDField
      FieldName = 'VL_AJUDA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaFL_FRETE: TStringField
      FieldName = 'FL_FRETE'
      Size = 1
    end
    object QcadtabelaFL_PEDAGIO: TStringField
      FieldName = 'FL_PEDAGIO'
      Size = 1
    end
    object QcadtabelaFL_EXCEDENTE: TStringField
      FieldName = 'FL_EXCEDENTE'
      Size = 1
    end
    object QcadtabelaFL_AD: TStringField
      FieldName = 'FL_AD'
      Size = 1
    end
    object QcadtabelaFL_GRIS: TStringField
      FieldName = 'FL_GRIS'
      Size = 1
    end
    object QcadtabelaFL_ENTREGA: TStringField
      FieldName = 'FL_ENTREGA'
      Size = 1
    end
    object QcadtabelaFL_SEGURO: TStringField
      FieldName = 'FL_SEGURO'
      Size = 1
    end
    object QcadtabelaFL_TDE: TStringField
      FieldName = 'FL_TDE'
      Size = 1
    end
    object QcadtabelaFL_TR: TStringField
      FieldName = 'FL_TR'
      Size = 1
    end
    object QcadtabelaFL_SEG_BALSA: TStringField
      FieldName = 'FL_SEG_BALSA'
      Size = 1
    end
    object QcadtabelaFL_REDEP_FLUVIAL: TStringField
      FieldName = 'FL_REDEP_FLUVIAL'
      Size = 1
    end
    object QcadtabelaFL_AGEND: TStringField
      FieldName = 'FL_AGEND'
      Size = 1
    end
    object QcadtabelaFL_PALLET: TStringField
      FieldName = 'FL_PALLET'
      Size = 1
    end
    object QcadtabelaFL_TAS: TStringField
      FieldName = 'FL_TAS'
      Size = 1
    end
    object QcadtabelaFL_DESPACHO: TStringField
      FieldName = 'FL_DESPACHO'
      Size = 1
    end
    object QcadtabelaFL_ENTREGA_PORTO: TStringField
      FieldName = 'FL_ENTREGA_PORTO'
      Size = 1
    end
    object QcadtabelaFL_ALFAND: TStringField
      FieldName = 'FL_ALFAND'
      Size = 1
    end
    object QcadtabelaFL_CANHOTO: TStringField
      FieldName = 'FL_CANHOTO'
      Size = 1
    end
    object QcadtabelaFL_FLUVIAL: TStringField
      FieldName = 'FL_FLUVIAL'
      Size = 1
    end
    object QcadtabelaFL_DEVOLUCAO: TStringField
      FieldName = 'FL_DEVOLUCAO'
      Size = 1
    end
    object QcadtabelaFL_AJUDA: TStringField
      FieldName = 'FL_AJUDA'
      Size = 1
    end
    object QcadtabelaFL_CTE: TStringField
      FieldName = 'FL_CTE'
      Size = 1
    end
    object QcadtabelaFL_NF_FRETE: TStringField
      FieldName = 'FL_NF_FRETE'
      Size = 1
    end
    object QcadtabelaFL_NF_PEDAGIO: TStringField
      FieldName = 'FL_NF_PEDAGIO'
      Size = 1
    end
    object QcadtabelaFL_NF_EXCEDENTE: TStringField
      FieldName = 'FL_NF_EXCEDENTE'
      Size = 1
    end
    object QcadtabelaFL_NF_AD: TStringField
      FieldName = 'FL_NF_AD'
      Size = 1
    end
    object QcadtabelaFL_NF_GRIS: TStringField
      FieldName = 'FL_NF_GRIS'
      Size = 1
    end
    object QcadtabelaFL_NF_ENTREGA: TStringField
      FieldName = 'FL_NF_ENTREGA'
      Size = 1
    end
    object QcadtabelaFL_NF_SEGURO: TStringField
      FieldName = 'FL_NF_SEGURO'
      Size = 1
    end
    object QcadtabelaFL_NF_TDE: TStringField
      FieldName = 'FL_NF_TDE'
      Size = 1
    end
    object QcadtabelaFL_NF_TR: TStringField
      FieldName = 'FL_NF_TR'
      Size = 1
    end
    object QcadtabelaFL_NF_SEG_BALSA: TStringField
      FieldName = 'FL_NF_SEG_BALSA'
      Size = 1
    end
    object QcadtabelaFL_NF_REDEP_FLUVIAL: TStringField
      FieldName = 'FL_NF_REDEP_FLUVIAL'
      Size = 1
    end
    object QcadtabelaFL_NF_AGEND: TStringField
      FieldName = 'FL_NF_AGEND'
      Size = 1
    end
    object QcadtabelaFL_NF_PALLET: TStringField
      FieldName = 'FL_NF_PALLET'
      Size = 1
    end
    object QcadtabelaFL_NF_TAS: TStringField
      FieldName = 'FL_NF_TAS'
      Size = 1
    end
    object QcadtabelaFL_NF_DESPACHO: TStringField
      FieldName = 'FL_NF_DESPACHO'
      Size = 1
    end
    object QcadtabelaFL_NF_ENTREGA_PORTO: TStringField
      FieldName = 'FL_NF_ENTREGA_PORTO'
      Size = 1
    end
    object QcadtabelaFL_NF_ALFAND: TStringField
      FieldName = 'FL_NF_ALFAND'
      Size = 1
    end
    object QcadtabelaFL_NF_CANHOTO: TStringField
      FieldName = 'FL_NF_CANHOTO'
      Size = 1
    end
    object QcadtabelaFL_NF_FLUVIAL: TStringField
      FieldName = 'FL_NF_FLUVIAL'
      Size = 1
    end
    object QcadtabelaFL_NF_DEVOLUCAO: TStringField
      FieldName = 'FL_NF_DEVOLUCAO'
      Size = 1
    end
    object QcadtabelaFL_NF_AJUDA: TStringField
      FieldName = 'FL_NF_AJUDA'
      Size = 1
    end
    object QcadtabelaFL_NF_CTE: TStringField
      FieldName = 'FL_NF_CTE'
      Size = 1
    end
    object QcadtabelaVL_REENT_MINIMA: TBCDField
      FieldName = 'VL_REENT_MINIMA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaDOC_CONTROLER: TStringField
      FieldName = 'DOC_CONTROLER'
      Size = 300
    end
    object QcadtabelaREPROVADO: TStringField
      FieldName = 'REPROVADO'
      Size = 1000
    end
    object QcadtabelaCONTROLER_REPR: TStringField
      FieldName = 'CONTROLER_REPR'
      Size = 30
    end
    object QcadtabelaVL_ARMAZENAGEM: TBCDField
      FieldName = 'VL_ARMAZENAGEM'
      Precision = 15
    end
    object QcadtabelaVL_DIARIA: TBCDField
      FieldName = 'VL_DIARIA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_REENTREGA: TBCDField
      FieldName = 'VL_REENTREGA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_DEDICADO: TBCDField
      FieldName = 'VL_DEDICADO'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_PERNOITE: TBCDField
      FieldName = 'VL_PERNOITE'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_HORA_PARADA: TBCDField
      FieldName = 'VL_HORA_PARADA'
      Precision = 15
      Size = 2
    end
    object QcadtabelaFL_IMPOSTO_ICMS: TStringField
      FieldName = 'FL_IMPOSTO_ICMS'
      FixedChar = True
      Size = 1
    end
    object QcadtabelaVL_FRETE_VALOR: TFloatField
      FieldName = 'VL_FRETE_VALOR'
    end
    object QcadtabelaVL_TAXA_TRT: TFloatField
      FieldName = 'VL_TAXA_TRT'
    end
    object QcadtabelaVL_AGENDG: TBCDField
      FieldName = 'VL_AGENDG'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_DIARIA48: TBCDField
      FieldName = 'VL_DIARIA48'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_DEVOLUCAOP: TBCDField
      FieldName = 'VL_DEVOLUCAOP'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_REENTREGAP: TBCDField
      FieldName = 'VL_REENTREGAP'
      Precision = 15
      Size = 2
    end
    object QcadtabelaVL_6X1: TBCDField
      FieldName = 'VL_6X1'
      Precision = 15
      Size = 2
    end
    object QcadtabelaQT6X1: TFMTBCDField
      FieldName = 'QT6X1'
      Precision = 38
      Size = 0
    end
  end
  object QCidade: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select codmun'
      'from cyber.rodmun '
      'where estado = :0 and inativ = '#39'N'#39
      'and descri = :1')
    Left = 236
    Top = 272
    object QCidadeCODMUN: TFMTBCDField
      FieldName = 'CODMUN'
      Precision = 38
      Size = 4
    end
  end
end
