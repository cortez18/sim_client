unit CadMensageiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.Mask, System.ImageList,
  Vcl.ImgList, Vcl.ExtCtrls, JvExDBGrids, JvDBGrid, JvDBUltimGrid, JvExStdCtrls,
  JvCheckBox;

type
  Tfrmcadmensageiro = class(TForm)
    Qmsn: TADOQuery;
    QmsnEMAIL: TStringField;
    DtsMsn: TDataSource;
    iml: TImageList;
    QTelas: TADOQuery;
    QTelasDESCRICAO: TStringField;
    QmsnSERVICO: TStringField;
    Qserv: TADOQuery;
    QservSERVICO: TStringField;
    qacoes: TADOQuery;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBMemo1: TDBMemo;
    cbtelas: TComboBox;
    mmemail: TMemo;
    Panel3: TPanel;
    Panel2: TPanel;
    Btadd: TBitBtn;
    btedit: TBitBtn;
    btsave: TBitBtn;
    btdel: TBitBtn;
    btcancel: TBitBtn;
    btfechar: TBitBtn;
    JvDBUltimGrid1: TJvDBUltimGrid;
    cbAtivo: TJvCheckBox;
    QmsnSTATUS: TStringField;
    procedure BtaddClick(Sender: TObject);
    procedure bteditClick(Sender: TObject);
    procedure btsaveClick(Sender: TObject);
    procedure btdelClick(Sender: TObject);
    procedure btfecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btcancelClick(Sender: TObject);
    procedure cbtelasChange(Sender: TObject);
    procedure Restaura;
    procedure JvDBUltimGrid1ColumnResized(Grid: TJvDBGrid; ACol,
      NewWidth: Integer);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBUltimGrid1TitleClick(Column: TColumn);
  private
  tipo : string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmcadmensageiro: Tfrmcadmensageiro;

implementation

{$R *.dfm}

uses Dados, Menu;

procedure Tfrmcadmensageiro.bteditClick(Sender: TObject);
begin
  //Evento do bot�o edit
  tipo := 'A';
  cbtelas.Text := QmsnSERVICO.AsString;
  mmemail.Text := QmsnEMAIL.AsString;
  if QmsnSTATUS.AsString = '1' then
    cbAtivo.Checked := true
  else
    cbAtivo.Checked := false;
  Restaura;
end;

procedure Tfrmcadmensageiro.btsaveClick(Sender: TObject);
begin
  //Evento do bot�o salvar
  if tipo = 'A' then
  begin
    qacoes.Close;
    qacoes.sql.clear;
    qacoes.sql.add('update tb_cadmensageiro set servico =:0,');
    qacoes.SQL.Add('email =:1, dtcriado =:2, atualizado =:3, status=:4 where servico=:5');
    qacoes.Parameters[0].Value := cbtelas.Text;
    qacoes.Parameters[1].Value := mmemail.Text;
    qacoes.Parameters[2].Value := Date;
    qacoes.Parameters[3].Value := GLBUSER;
    if cbativo.Checked = true then
      qacoes.Parameters[4].Value := '1'
    else
      qacoes.Parameters[4].Value := '0';
    qacoes.Parameters[5].Value := cbtelas.Text;
    qacoes.ExecSQL;
  end
  else if tipo = 'I' then
  begin
    qacoes.Close;
    qacoes.sql.clear;
    qacoes.sql.add('insert into tb_cadmensageiro (servico, email, atualizado, dtcriado,status)');
    qacoes.SQL.Add('values (:0, :1, :2, :3, 1)');
    qacoes.Parameters[0].Value := cbtelas.Text;
    qacoes.Parameters[1].Value := mmemail.Text;
    qacoes.Parameters[2].Value := GLBUSER;
    qacoes.Parameters[3].Value := Date;
    qacoes.ExecSQL;
   end;
  Qmsn.Close;
  Qmsn.Open;
  cbtelas.ItemIndex := 0;
  mmemail.Clear;
  restaura;
end;

procedure Tfrmcadmensageiro.cbtelasChange(Sender: TObject);
begin
  //No Evento verifica se j� existe o servi�o gravado
  Qserv.Close;
  Qserv.Parameters[0].Value := cbtelas.Text;
  Qserv.open;
  if QservSERVICO.AsString = cbtelas.Text then
  begin
    ShowMessage('J� existe registro com esse servi�o.');
    cbtelas.ItemIndex := 0;
  end;
end;

procedure Tfrmcadmensageiro.btdelClick(Sender: TObject);
begin
  //Evento para exclus�o
  If Application.MessageBox('Confirma a Exclus�o ?','Cuidado !!!',MB_YESNO +
                           MB_ICONQUESTION + MB_DEFBUTTON2) = IDYES Then
  begin
    qacoes.Close;
    qacoes.sql.clear;
    qacoes.sql.add('delete tb_cadmensageiro where servico =:0');
    qacoes.Parameters[0].Value := QmsnSERVICO.AsString;
    qacoes.ExecSQL;
  end;
  Qmsn.Close;
  Qmsn.Open;
  cbtelas.Clear;
  cbtelas.ItemIndex := 0;
  mmemail.Clear;
end;

procedure Tfrmcadmensageiro.btfecharClick(Sender: TObject);
begin
  close;
end;

procedure Tfrmcadmensageiro.btcancelClick(Sender: TObject);
begin
  Qmsn.Close;
  Qmsn.Open;
  cbtelas.Clear;
  cbtelas.ItemIndex := 0;
  mmemail.Clear;
  restaura;
end;

procedure Tfrmcadmensageiro.BtaddClick(Sender: TObject);
begin
  //Evento para adicionar os servi�os
  tipo := 'I';
  Restaura;
end;

procedure Tfrmcadmensageiro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//Ativando
  Qmsn.Active := false;
  QTelas.Active := false;
end;

procedure Tfrmcadmensageiro.FormCreate(Sender: TObject);
begin
//Adicionando os servi�os no cbtelas
  qmsn.Active := true;
  QTelas.Active := true;
  cbtelas.Items.Add(' ');
  while not QTelas.Eof do
  begin
    cbtelas.Items.Add(QTelasDESCRICAO.AsString);
    QTelas.Next;
  end;
  mmemail.Clear;
end;

procedure Tfrmcadmensageiro.JvDBUltimGrid1ColumnResized(Grid: TJvDBGrid; ACol,
  NewWidth: Integer);
begin
  if JvDBUltimGrid1.Columns[Acol].FieldName = 'EMAIL' then
    DBMemo1.Width := NewWidth;
end;

procedure Tfrmcadmensageiro.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QmsnSTATUS.Value = '0' then
  begin
    JvDBUltimGrid1.canvas.Brush.color := clRed;
    JvDBUltimGrid1.canvas.font.color := clWhite;
  end;
  JvDBUltimGrid1.DefaultDrawDataCell(Rect, JvDBUltimGrid1.Columns[DataCol].field, State);
end;

procedure Tfrmcadmensageiro.JvDBUltimGrid1TitleClick(Column: TColumn);
var
  icount: Integer;
begin
  Qmsn.Close;
  if Pos('order by', Qmsn.sql.Text) > 0 then
  begin
    Qmsn.sql.Text := copy(Qmsn.sql.Text, 1,
      Pos('order by', Qmsn.sql.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    Qmsn.sql.Text := Qmsn.sql.Text + ' order by ' + Column.FieldName;
  Qmsn.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBUltimGrid1.Columns.Count - 1 do
    JvDBUltimGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadMensageiro.Restaura;
begin
  Btadd.Enabled  := not Btadd.Enabled;
  btedit.enabled  := not btedit.enabled;
  btdel.enabled  := not btdel.enabled;
  btsave.enabled   := not btsave.enabled;
  btcancel.enabled := not btcancel.enabled;
  btfechar.enabled   := not btfechar.enabled;
  if Panel1.Visible = true then
    Panel1.Visible := false
  else
    Panel1.Visible := true;
 end;

end.
