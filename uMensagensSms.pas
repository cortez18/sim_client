unit uMensagensSms;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, DB, ADODB, jpeg;

type
  TfrmMensagemSms = class(TForm)
    rlMsg: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLLabel15: TRLLabel;
    RLSystemInfo3: TRLSystemInfo;
    RLDraw1: TRLDraw;
    RLBand2: TRLBand;
    RLBand3: TRLBand;
    RLLabel13: TRLLabel;
    RLLabel22: TRLLabel;
    dbMsg: TRLDBText;
    RLLabel3: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    qrSms: TADOQuery;
    qrSmsNOM_MOTORISTA: TStringField;
    qrSmsCOD_MANIFESTO: TBCDField;
    qrSmsNOM_FANTASIA: TStringField;
    qrSmsDSC_MENSAGEM: TStringField;
    dsSms: TDataSource;
    rlImgIntecom: TRLImage;
    rlImgIW: TRLImage;
    RLLabel1: TRLLabel;
    RLDBText3: TRLDBText;
    qrSmsNUM_PLACA: TStringField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    iManifesto: Extended;
  end;

var
  frmMensagemSms: TfrmMensagemSms;

implementation

uses Dados, menu;

{$R *.dfm}

procedure TfrmMensagemSms.FormCreate(Sender: TObject);
begin
  if (GlbFilial = 8) or (GlbFilial = 9) then
  begin
    rlImgIW.Visible := True;
    rlImgIntecom.Visible := False;
  end
  else
  begin
    rlImgIW.Visible := False;
    rlImgIntecom.Visible := True;
  end;
end;

end.
