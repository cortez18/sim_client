unit CadTabelaCustoFrac;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, ActnList, JvExStdCtrls,
  JvToolEdit, JvBaseEdits, Mask, JvExMask, JvMaskEdit, Grids, ComObj, ShellApi,
  DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, JvMemo, JvEdit,
  System.Actions, JvDBControls;

type
  TfrmCadTabelaCustoFrac = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    dtstabela: TDataSource;
    Detalhes: TTabSheet;
    Panel1: TPanel;
    dtsItem: TDataSource;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    QTransp: TADOQuery;
    QTranspnm_fornecedor: TStringField;
    dtsTrans: TDataSource;
    CBUFO: TComboBox;
    CBLO: TComboBox;
    cbtra: TComboBox;
    edTrans: TJvMaskEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edFreteM: TJvCalcEdit;
    edPedagio: TJvCalcEdit;
    edOutros: TJvCalcEdit;
    edOutrosR: TJvCalcEdit;
    edOutrosM: TJvCalcEdit;
    edExc: TJvCalcEdit;
    EdSeg: TJvCalcEdit;
    EdGris: TJvCalcEdit;
    EdGrisM: TJvCalcEdit;
    Qcadtabela: TADOQuery;
    QTabelaCOD_CUSTO: TBCDField;
    QTabelaDS_UF: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaNM_FORNECEDOR: TStringField;
    QTabelaLOCAL: TStringField;
    QTranspCOD_FORNECEDOR: TBCDField;
    QItemCOD_TAB: TBCDField;
    QItemCOD_CUSTO: TBCDField;
    QItemNR_PESO_DE: TBCDField;
    QItemNR_PESO_ATE: TBCDField;
    QItemVL_RODOVIARIO: TBCDField;
    QItemDATAI: TDateTimeField;
    QItemDATAF: TDateTimeField;
    QItemID_REGISTRO: TStringField;
    QItemVR_CTRC: TBCDField;
    QItemVR_EXCED: TBCDField;
    Label14: TLabel;
    cbUFD: TComboBox;
    cbLd: TComboBox;
    QTabelaDS_UF_DES: TStringField;
    QTabelaLOCAL_DES: TStringField;
    TabSheet1: TTabSheet;
    Label15: TLabel;
    cbPesqTransp: TComboBox;
    Label16: TLabel;
    cbPesqUFO: TComboBox;
    cbPesqLO: TComboBox;
    Label17: TLabel;
    cbPesqUFD: TComboBox;
    cbPesqLD: TComboBox;
    btProcurar: TBitBtn;
    btRetirarFiltro: TBitBtn;
    Label18: TLabel;
    edEntrega: TJvCalcEdit;
    cbImposto: TCheckBox;
    edsegminimo: TJvCalcEdit;
    Label19: TLabel;
    cbPeso: TCheckBox;
    Label20: TLabel;
    cbVeiculo: TComboBox;
    QcadtabelaCOD_CUSTO: TBCDField;
    QcadtabelaCOD_FORNECEDOR: TBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_REAIS: TBCDField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TBCDField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QcadtabelaUSUARIO: TStringField;
    edVeiculo: TJvMaskEdit;
    Label21: TLabel;
    cbCidade: TComboBox;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QcadtabelaCODMUN: TBCDField;
    Label1: TLabel;
    cbCidadeO: TComboBox;
    QcadtabelaCODMUNO: TBCDField;
    Label22: TLabel;
    edTDE: TJvCalcEdit;
    Label23: TLabel;
    edTR: TJvCalcEdit;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TBCDField;
    Label24: TLabel;
    cbVeiculoPesq: TComboBox;
    Label25: TLabel;
    edTDEp: TJvCalcEdit;
    Label26: TLabel;
    edTDEm: TJvCalcEdit;
    QcadtabelaVL_TDEP: TBCDField;
    QcadtabelaVL_TDEM: TBCDField;
    QTabelaVEICULO: TStringField;
    QcadtabelaDIAS: TBCDField;
    cbStatus: TCheckBox;
    QcadtabelaFL_STATUS: TStringField;
    Label28: TLabel;
    cbOperacao: TComboBox;
    Label29: TLabel;
    cbOperacaoPesq: TComboBox;
    QTabelaOPERACAO: TStringField;
    QcadtabelaOPERACAO: TStringField;
    Label30: TLabel;
    edTdeMax: TJvCalcEdit;
    QcadtabelaVL_TDEMAX: TBCDField;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    QcadtabelaOBS: TStringField;
    Label27: TLabel;
    Obs: TJvMemo;
    Label31: TLabel;
    edTRp: TJvCalcEdit;
    Label32: TLabel;
    edTRm: TJvCalcEdit;
    QcadtabelaVL_TRP: TBCDField;
    QcadtabelaVL_TRM: TBCDField;
    Label33: TLabel;
    edHE: TJvCalcEdit;
    QcadtabelaVL_HE: TBCDField;
    RGFiltro: TRadioGroup;
    edUser: TJvEdit;
    edDtIns: TJvDateEdit;
    edUserA: TJvEdit;
    edDtAlt: TJvDateEdit;
    Label34: TLabel;
    cbCidOPesq: TComboBox;
    Label35: TLabel;
    cbCidDPesq: TComboBox;
    QTabelaFL_KM: TStringField;
    OpenDialog: TOpenDialog;
    Qcadtabeladoc_controler: TStringField;
    edSuframa: TJvCalcEdit;
    Label36: TLabel;
    Label37: TLabel;
    edPReceita: TJvCalcEdit;
    edCanhoto: TJvCalcEdit;
    QcadtabelaVL_SUFRA: TBCDField;
    QcadtabelaVL_VD: TBCDField;
    QcadtabelaVL_TDC: TBCDField;
    QcadtabelaVL_PORTO: TBCDField;
    QcadtabelaVL_PORTOMIN: TBCDField;
    QcadtabelaVL_TDA: TBCDField;
    QcadtabelaVL_TDAMIN: TBCDField;
    QcadtabelaVL_TDAMAX: TBCDField;
    Label39: TLabel;
    edPorto: TJvCalcEdit;
    Label40: TLabel;
    edPortoMin: TJvCalcEdit;
    Label41: TLabel;
    edTDA: TJvCalcEdit;
    Label42: TLabel;
    edTDAMin: TJvCalcEdit;
    Label43: TLabel;
    edTDAMax: TJvCalcEdit;
    Panel6: TPanel;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir_: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnDuplicar: TBitBtn;
    Panel5: TPanel;
    btnVer: TBitBtn;
    btnDcto: TBitBtn;
    Panel7: TPanel;
    RBAtivo: TRadioButton;
    RbInativo: TRadioButton;
    Label44: TLabel;
    edArmazem: TJvCalcEdit;
    Label45: TLabel;
    edAjuda: TJvCalcEdit;
    Label46: TLabel;
    edPalet: TJvCalcEdit;
    edDiaria: TJvCalcEdit;
    Label47: TLabel;
    Label48: TLabel;
    edReentrega: TJvCalcEdit;
    Label51: TLabel;
    edHrParada: TJvCalcEdit;
    QcadtabelaREPROVADO: TStringField;
    QcadtabelaCONTROLER_REPR: TStringField;
    QcadtabelaVL_ARMAZENAGEM: TBCDField;
    QcadtabelaVL_AJUDANTES: TBCDField;
    QcadtabelaVL_PALETIZACAO: TBCDField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    cbRegiaoD: TComboBox;
    Label50: TLabel;
    QcadtabelaREGIAO: TStringField;
    TabSheet2: TTabSheet;
    QGenItem: TADOQuery;
    dsGenItem: TDataSource;
    Panel8: TPanel;
    Label52: TLabel;
    eddedicadoGen: TJvCalcEdit;
    LBLDedicado: TLabel;
    Lbldiaria: TLabel;
    eddiariaGen: TJvCalcEdit;
    edpernoiteGen: TJvCalcEdit;
    LBLPernoite: TLabel;
    JvDBGrid1: TJvDBGrid;
    QGenItemCOD_CUSTO: TFMTBCDField;
    QGenItemVL_PERNOITE: TBCDField;
    QGenItemVL_VD: TBCDField;
    QGenItemVL_DIARIA: TBCDField;
    QGenItemVEICULO: TStringField;
    Panel9: TPanel;
    bteditgen: TBitBtn;
    btpostgen: TBitBtn;
    btcancelgen: TBitBtn;
    btinserirgen: TBitBtn;
    btsalvargen: TBitBtn;
    btalterargen: TBitBtn;
    btexcluirgen: TBitBtn;
    btcancelagen: TBitBtn;
    LBLCanhoto: TLabel;
    QGenItemCOD_ID: TFMTBCDField;
    DBMemo1: TDBMemo;
    Label55: TLabel;
    JvDBMaskEdit1: TJvDBMaskEdit;
    Label38: TLabel;
    Label53: TLabel;
    edRee_m: TJvCalcEdit;
    QcadtabelaVL_REENTREGA_MIN: TBCDField;
    QTabelafl_status: TStringField;
    cbveiculogen: TComboBox;
    QItemvl_minimo: TBCDField;
    QTabeladt_controler: TDateField;
    Label54: TLabel;
    edArmMin: TJvCalcEdit;
    QcadtabelaVL_ARMMIN: TBCDField;
    Qcadtabelavl_preceita: TBCDField;
    Label49: TLabel;
    cbregiaoO: TComboBox;
    Qcadtabelaregiaoo: TStringField;
    PageControl2: TPageControl;
    tsFracionado: TTabSheet;
    tsAereo: TTabSheet;
    Panel4: TPanel;
    edDataI: TJvDateEdit;
    edDataF: TJvDateEdit;
    EdValor: TJvCalcEdit;
    edCtrc: TJvCalcEdit;
    edPesoi: TJvCalcEdit;
    edPesof: TJvCalcEdit;
    edExced: TJvCalcEdit;
    edMinimo: TJvCalcEdit;
    DBGrid1: TJvDBUltimGrid;
    Panel3: TPanel;
    DBNavigator1: TDBNavigator;
    btnInserirI: TBitBtn;
    btnsalvarI: TBitBtn;
    btnAlterarI: TBitBtn;
    btnExcluirI: TBitBtn;
    btnCancelarI: TBitBtn;
    Panel10: TPanel;
    Panel11: TPanel;
    DBNavigator2: TDBNavigator;
    edDataAereoI: TJvDateEdit;
    edDataAereoF: TJvDateEdit;
    edAereoColeta: TJvCalcEdit;
    edAereoEntrega: TJvCalcEdit;
    edPesoAereoI: TJvCalcEdit;
    edPesoAereoF: TJvCalcEdit;
    edAereoExc: TJvCalcEdit;
    QAereo: TADOQuery;
    dtsAereo: TDataSource;
    QAereoCOD_CUSTO: TFMTBCDField;
    QAereoPESO_DE: TBCDField;
    QAereoPESO_ATE: TBCDField;
    QAereoVALOR_COLETA: TBCDField;
    QAereoVALOR_ENTREGA: TBCDField;
    QAereoDATAI: TDateTimeField;
    QAereoDATAF: TDateTimeField;
    QAereoVR_EXCED: TBCDField;
    QAereoUSUARIO: TStringField;
    QAereoDATAINC: TDateTimeField;
    QAereoUSUALT: TStringField;
    QAereoDATAALT: TDateTimeField;
    QAereoID_REGISTRO: TStringField;
    JvDBUltimGrid2: TJvDBUltimGrid;
    btnAereoI: TBitBtn;
    btnAereoS: TBitBtn;
    btnAereoA: TBitBtn;
    btnAereoE: TBitBtn;
    btnAereoC: TBitBtn;
    QAereoCOD_TAB: TFMTBCDField;
    QTabelafl_aereo: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure btnExcluir_Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure Restaura;
    procedure RestauraI;
    procedure RestauraGen;
    procedure btnInserirIClick(Sender: TObject);
    procedure btnsalvarIClick(Sender: TObject);
    procedure btnAlterarIClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure Limpadados;
    procedure LimpadadosGen;
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure btnExcluirIClick(Sender: TObject);
    procedure btnCancelarIClick(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    procedure btRetirarFiltroClick(Sender: TObject);
    procedure edExcedExit(Sender: TObject);
    procedure btnDuplicarClick(Sender: TObject);
    procedure cbUFDChange(Sender: TObject);
    procedure CBUFOChange(Sender: TObject);
    procedure edTDEChange(Sender: TObject);
    procedure edTDEpChange(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbPesqLOChange(Sender: TObject);
    procedure cbPesqLDChange(Sender: TObject);
    procedure btnDctoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure RBAtivoClick(Sender: TObject);
    procedure RbInativoClick(Sender: TObject);
    procedure bteditgenClick(Sender: TObject);
    procedure btpostgenClick(Sender: TObject);
    procedure btcancelgenClick(Sender: TObject);
    procedure btinserirgenClick(Sender: TObject);
    procedure btsalvargenClick(Sender: TObject);
    procedure btcancelagenClick(Sender: TObject);
    procedure btalterargenClick(Sender: TObject);
    procedure btexcluirgenClick(Sender: TObject);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure edDataIExit(Sender: TObject);
    procedure edDataFExit(Sender: TObject);
    procedure edPesoiExit(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edPReceitaExit(Sender: TObject);
    procedure tsAereoShow(Sender: TObject);
    procedure tsAereoHide(Sender: TObject);
    procedure JvDBUltimGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnAereoIClick(Sender: TObject);
    procedure RestauraAereo;
    procedure btnAereoAClick(Sender: TObject);
    procedure btnAereoEClick(Sender: TObject);
    procedure btnAereoCClick(Sender: TObject);
    procedure btnAereoSClick(Sender: TObject);
    procedure edDataAereoFExit(Sender: TObject);
    procedure edPesofExit(Sender: TObject);
    procedure edPesoAereoFExit(Sender: TObject);
    procedure colorircampos;

  private
    quant, spot: Integer;
    tipo, sql, status: string;
  public
    { Public declarations }
  end;

var
  frmCadTabelaCustoFrac: TfrmCadTabelaCustoFrac;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadTabelaCustoFrac.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTabelaCustoFrac.btnNovoClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  Restaura;
  Limpadados;
  tipo := 'I';
  PageControl1.ActivePage := Detalhes;
  Panel1.Visible := false;
  cbtra.SetFocus;
end;

procedure TfrmCadTabelaCustoFrac.btnSalvarClick(Sender: TObject);
var
  cod, forn: Integer;
begin
  cod := 0;

  if alltrim(cbOperacao.Text) = '' then
  begin
    ShowMessage('A Opera��o n�o foi escolhida !');
    cbOperacao.SetFocus;
    Exit;
  end;

  if alltrim(cbtra.Text) = '' then
  begin
    ShowMessage('O Transportadora n�o foi escolhido !');
    cbtra.SetFocus;
    Exit;
  end;

  if UpperCase(cbOperacao.Text) <> 'SPOT' then
  begin
    if not dtmDados.PodeInserir(name) then
    begin
      ShowMessage('Voc� N�o Tem Permiss�o Para Cadastrar este tipo de Opera��o');
      btnCancelarClick(Sender);
      exit;
    end;
  end;

  forn := 0;
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select codclifor from cyber.rodcli where nomeab || '' - '' || nvl(codcpf,codcgc) = '
    + #39 + cbtra.Text + #39);
  dtmDados.iQuery1.Open;
  forn := dtmDados.iQuery1.FieldByName('codclifor').value;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('SELECT COUNT(EMAIL) EMAIL FROM CYBER.RODCTC  WHERE SITUAC = ''A'' AND  RODCTC.CODCLIFOR = :0');
  dtmDados.iQuery1.Parameters[0].value := forn;
  dtmDados.iQuery1.Open;
  if dtmDados.iQuery1.FieldByName('email').value = 0 then
  begin
    dtmDados.iQuery1.Close;
    ShowMessage('Fornecedor sem e-mail cadastrado');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  if tipo = 'I' then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('select * from tb_custofrac f left join tb_custofrac_item i on f.cod_custo = i.cod_custo ');
    dtmDados.iQuery1.sql.add('where fl_status = ''S'' ');
    dtmDados.iQuery1.sql.add('and cod_fornecedor = :0 and fl_local = :1 ');
    dtmDados.iQuery1.sql.add('and ds_uf = :2 and f.fl_local_des = :3 ');
    dtmDados.iQuery1.sql.add('and f.ds_uf_des = :4');
    dtmDados.iQuery1.sql.add('and nvl(f.veiculo,'' '') = :5');
    dtmDados.iQuery1.sql.add('and f.codmun = :6');
    dtmDados.iQuery1.sql.add('and f.codmunO = :7');
    dtmDados.iQuery1.sql.add('and f.operacao = :8');
    dtmDados.iQuery1.sql.add('and f.regiao = :9');
    dtmDados.iQuery1.sql.add('and f.regiaoO = :10');
    dtmDados.iQuery1.sql.add('and trunc(i.datai) <= trunc(sysdate) and trunc(i.dataf) >= trunc(sysdate) ');
    dtmDados.iQuery1.Parameters[0].value := forn;
    dtmDados.iQuery1.Parameters[1].value := copy(CBLO.Text, 1, 1);
    dtmDados.iQuery1.Parameters[2].value := CBUFO.Text;
    dtmDados.iQuery1.Parameters[3].value := copy(cbLd.Text, 1, 1);
    dtmDados.iQuery1.Parameters[4].value := cbUFD.Text;
    if cbVeiculo.Text <> '' then
      dtmDados.iQuery1.Parameters[5].value := cbVeiculo.Text
    else
      dtmDados.iQuery1.Parameters[5].value := '';
    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        dtmDados.iQuery1.Parameters[6].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[6].value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        dtmDados.iQuery1.Parameters[7].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[7].value := 0;
    dtmDados.iQuery1.Parameters[8].value := cbOperacao.Text;

    if cbRegiaoD.Text <> '' then
      dtmDados.iQuery1.Parameters[9].value := cbRegiaoD.Text
    else
      dtmDados.iQuery1.Parameters[9].value := '';

    if cbRegiaoO.Text <> '' then
      dtmDados.iQuery1.Parameters[10].value := cbRegiaoO.Text
    else
      dtmDados.iQuery1.Parameters[10].value := '';

    dtmDados.iQuery1.Open;
    if not dtmDados.iQuery1.FieldByName('cod_custo').IsNull then
    begin
      ShowMessage
        ('Este Destino j� est� cadastrado para esta Transportador e Percurso !!');
      btnCancelarClick(Sender);
      QTabela.Locate('cod_custo', dtmDados.iQuery1.FieldByName('cod_custo')
        .value, []);
      dtmDados.iQuery1.Close;
    end;
  end
  else
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('select * from tb_custofrac f left join tb_custofrac_item i on f.cod_custo = i.cod_custo ');
    dtmDados.iQuery1.sql.add('where fl_status = ''S'' ');
    dtmDados.iQuery1.sql.add('and cod_fornecedor = :0 and fl_local = :1 ');
    dtmDados.iQuery1.sql.add('and ds_uf = :2 and f.fl_local_des = :3 ');
    dtmDados.iQuery1.sql.add('and f.ds_uf_des = :4');
    dtmDados.iQuery1.sql.add('and nvl(f.veiculo,'' '') = :5');
    dtmDados.iQuery1.sql.add('and nvl(f.codmun,0) = :6');
    dtmDados.iQuery1.sql.add('and nvl(f.codmunO,0) = :7');
    dtmDados.iQuery1.sql.add('and f.operacao = :8');
    dtmDados.iQuery1.sql.add('and f.cod_custo <> :9');
    dtmDados.iQuery1.sql.add('and f.regiao = :10');
    dtmDados.iQuery1.sql.add('and f.regiaoO = :11');
    dtmDados.iQuery1.sql.add('and trunc(i.datai) <= trunc(sysdate) and trunc(i.dataf) >= trunc(sysdate) ');
    dtmDados.iQuery1.Parameters[0].value := forn;
    dtmDados.iQuery1.Parameters[1].value := copy(CBLO.Text, 1, 1);
    dtmDados.iQuery1.Parameters[2].value := CBUFO.Text;
    dtmDados.iQuery1.Parameters[3].value := copy(cbLd.Text, 1, 1);
    dtmDados.iQuery1.Parameters[4].value := cbUFD.Text;
    if cbVeiculo.Text <> '' then
      dtmDados.iQuery1.Parameters[5].value := cbVeiculo.Text
    else
      dtmDados.iQuery1.Parameters[5].value := ' ';
    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        dtmDados.iQuery1.Parameters[6].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[6].value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        dtmDados.iQuery1.Parameters[7].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[7].value := 0;
    dtmDados.iQuery1.Parameters[8].value := cbOperacao.Text;
    dtmDados.iQuery1.Parameters[9].value := QTabelaCOD_CUSTO.AsInteger;
    if cbRegiaoD.Text <> '' then
      dtmDados.iQuery1.Parameters[10].value := cbRegiaoD.Text
    else
      dtmDados.iQuery1.Parameters[10].value := '';
    if cbRegiaoO.Text <> '' then
      dtmDados.iQuery1.Parameters[11].value := cbRegiaoO.Text
    else
      dtmDados.iQuery1.Parameters[11].value := '';
    dtmDados.iQuery1.Open;
    if not dtmDados.iQuery1.FieldByName('cod_custo').IsNull then
    begin
      ShowMessage
        ('Este Destino j� est� cadastrado para esta Transportador e Percurso !!');
      btnCancelarClick(Sender);
      QTabela.Locate('cod_custo', dtmDados.iQuery1.FieldByName('cod_custo')
        .value, []);
      dtmDados.iQuery1.Close;
    end;
  end;

  try
    if tipo = 'I' then
    begin
      Qcadtabela.append;
      cbStatus.Checked := true;
    end
    else
    begin
      cod := QTabela.RecNo;
      Qcadtabela.edit;
    end;
    edTrans.Text := cbtra.Text;
    edVeiculo.Text := cbVeiculo.Text;
    QcadtabelaCOD_FORNECEDOR.value := forn;
    QcadtabelaFL_LOCAL.value := CBLO.Text;
    QcadtabelaDS_UF.value := CBUFO.Text;
    QcadtabelaFL_LOCAL_DES.value := cbLd.Text;
    QcadtabelaDS_UF_DES.value := cbUFD.Text;
    QcadtabelaVL_FRETE_MINIMO.value := edFreteM.value;
    QcadtabelaVL_PEDAGIO.value := edPedagio.value;
    QcadtabelaVL_OUTROS.value := edOutros.value;
    QcadtabelaVL_OUTROS_REAIS.value := edOutrosR.value; // fluvial
    QcadtabelaVL_OUTROS_MINIMO.value := edOutrosM.value; // fluvial minimo
    QcadtabelaVL_EXCEDENTE.value := edExc.value;
    QcadtabelaVL_AD.value := EdSeg.value;
    QcadtabelaVL_GRIS.value := EdGris.value;
    QcadtabelaVL_GRIS_MINIMO.value := EdGrisM.value;
    QcadtabelaVL_TDE.value := edTDE.value;
    QcadtabelaVL_TDEP.value := edTDEp.value;
    QcadtabelaVL_TDEM.value := edTDEm.value;
    QcadtabelaVL_TR.value := edTR.value;
    QcadtabelaVL_TRP.value := edTRp.value;
    QcadtabelaVL_TRM.value := edTRm.value;
    QcadtabelaVL_TDEMAX.value := edTdeMax.value;
    QcadtabelaUSUARIO.value := GLBUSER;
    QcadtabelaMODAL.value := '';
    QcadtabelaVEICULO.value := cbVeiculo.Text;
    QcadtabelaVL_HE.value := edHE.value;
    QcadtabelaVL_PORTO.value := edPorto.value;
    QcadtabelaVL_PORTOMIN.value := edPortoMin.value;
    // Implementa��o do projeto generalidades
    QcadtabelaVL_ARMAZENAGEM.value := edArmazem.value;
    QcadtabelaVL_AJUDANTES.value := edAjuda.value;
    QcadtabelaVL_PALETIZACAO.value := edPalet.value;
    QcadtabelaVL_DIARIA.value := edDiaria.value;
    QcadtabelaVL_REENTREGA.value := edReentrega.value;
   // QcadtabelaVL_PERNOITE.value := edPernoite.value;
    QcadtabelaVL_HORA_PARADA.value := edHrParada.value;
    QcadtabelaREGIAO.value := cbRegiaoD.Text;
    QcadtabelaREGIAOO.value := cbRegiaoO.Text;

    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        QcadtabelaCODMUN.value := QCidadeCODMUN.AsInteger;
    end
    else
      QcadtabelaCODMUN.value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        QcadtabelaCODMUNO.value := QCidadeCODMUN.AsInteger;
    end
    else
      QcadtabelaCODMUNO.value := 0;

    QcadtabelaVL_ENTREGA.value := edEntrega.value;
    QcadtabelaDT_CADASTRO.value := date;
    QcadtabelaSITE.value := GLBFilial;
    QcadtabelaSEGURO_MINIMO.value := edsegminimo.value;
    if cbImposto.Checked = true then
      QcadtabelaFL_IMPOSTO.value := 'S'
    else
      QcadtabelaFL_IMPOSTO.value := 'N';
    if cbPeso.Checked = true then
      QcadtabelaFL_PESO.value := 'S'
    else
      QcadtabelaFL_PESO.value := 'N';

    if cbStatus.Checked = true then
      QcadtabelaFL_STATUS.value := 'S'
    else
      QcadtabelaFL_STATUS.value := 'N';

    QcadtabelaCONTROLER.value := '';
    QcadtabelaDT_CONTROLER.clear;
    QcadtabelaOBS.value := Obs.Text;

    QcadtabelaOPERACAO.value := cbOperacao.Text;

    QcadtabelaVL_SUFRA.value := edSuframa.value;
    QcadtabelaVL_preceita.value := edpreceita.value;
    QcadtabelaVL_TDC.value := edCanhoto.value;
    QcadtabelaVL_TDA.value := edTDA.Value;
    QcadtabelaVL_TDAMIN.value := edTDAMin.Value;
    QcadtabelaVL_TDAMAX.value := edTDAMax.Value;

    dtmDados.iQuery1.Close;
    Qcadtabela.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Qcadtabela.Close;
  QTabela.Close;
  QTabela.Open;
  Qcadtabela.Open;
  if tipo = 'A' then
    QTabela.RecNo := cod
  else
    QTabela.Last;
  Restaura;
  QItem.Close;
  QItem.Open;
  tipo := '';
  Panel1.Visible := true;
end;

procedure TfrmCadTabelaCustoFrac.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  PageControl1.ActivePage := Consulta;
  tipo := '';
end;

procedure TfrmCadTabelaCustoFrac.btnCancelarIClick(Sender: TObject);
begin
  QItem.Cancel;
  RestauraI;
end;

procedure TfrmCadTabelaCustoFrac.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo, altera: string;
  reg: Integer;
begin
  caminho := glbcontroler;
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      arquivo := caminho + 'C_' + QcadtabelaCOD_CUSTO.AsString +
        ExtractFileName(OpenDialog.FileName);
      // verifica se existe este arquivo j� gravado, sim renomeia
      if FileExists(arquivo) then
      begin
        altera := copy(arquivo, 1, length(arquivo) - 4) + 'A' +
          apcarac(DateToStr(now())) + '.pdf';
        RenameFile(arquivo, altera);
      end;
      CopyFile(Pchar(ori), Pchar(des), true);

      RenameFile(des, arquivo);

      RenameFile(des, arquivo);
      reg := QcadtabelaCOD_CUSTO.AsInteger;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('update tb_custofrac set doc_controler = :c where cod_custo = :n');
      dtmDados.iQuery1.Parameters[0].value := 'C_' +
        QcadtabelaCOD_CUSTO.AsString + ExtractFileName(OpenDialog.FileName);
      dtmDados.iQuery1.Parameters[1].value := QcadtabelaCOD_CUSTO.AsInteger;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;

      QTabela.Close;
      QTabela.Open;
      QTabela.Locate('cod_custo', reg, []);
      reg := 0;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadTabelaCustoFrac.btnDuplicarClick(Sender: TObject);
var
  cod, new: Integer;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Duplicar Esta Tabela?',
    'Duplicar Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      cod := QTabelaCOD_CUSTO.AsInteger;

      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('select * from tb_custofrac where cod_custo = :0');
      dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
      dtmDados.iQuery1.Open;

      // QTabela.Close;
      // QTabela.SQL.Text := Copy(QTabela.SQL.Text, 1, Pos('order by', QTabela.SQL.Text) - 1) + ' order by cod_custo';
      // QTabela.Open;

      // QTabela.locate('cod_custo',cod,[]);
      Qcadtabela.append;
      QcadtabelaCOD_FORNECEDOR.value := dtmDados.iQuery1.FieldByName
        ('COD_FORNECEDOR').AsInteger;
      QcadtabelaFL_LOCAL.value := QcadtabelaFL_LOCAL_DES.value;
      QcadtabelaDS_UF.value := QTabelaDS_UF_DES.value;
      QcadtabelaFL_LOCAL_DES.value := QcadtabelaFL_LOCAL.value;
      QcadtabelaDS_UF_DES.value := QTabelaDS_UF.value;
      QcadtabelaVL_FRETE_MINIMO.value := QTabelaVL_FRETE_MINIMO.value;
      QcadtabelaVL_PEDAGIO.value := dtmDados.iQuery1.FieldByName
        ('vl_pedagio').value;
      QcadtabelaVL_OUTROS.value := dtmDados.iQuery1.FieldByName
        ('vl_outros').value;
      QcadtabelaVL_OUTROS_REAIS.value := dtmDados.iQuery1.FieldByName
        ('vl_outros_reais').value; // fluvial
      QcadtabelaVL_OUTROS_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('vl_outros_minimo').value; // fluvial minimo
      QcadtabelaVL_EXCEDENTE.value := dtmDados.iQuery1.FieldByName
        ('vl_excedente').value;
      QcadtabelaVL_AD.value := QTabelaVL_AD.value;
      QcadtabelaVL_GRIS.value := dtmDados.iQuery1.FieldByName('vl_gris').value;
      QcadtabelaVL_GRIS_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('vl_gris_minimo').value;
      QcadtabelaUSUARIO.value := GLBUSER;
      QcadtabelaMODAL.value := '';
      QcadtabelaVEICULO.value := QTabelaVEICULO.value;
      QcadtabelaVL_ENTREGA.value := dtmDados.iQuery1.FieldByName
        ('vl_entrega').value;
      QcadtabelaDT_CADASTRO.value := date;
      QcadtabelaSITE.value := GLBFilial;
      QcadtabelaSEGURO_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('SEGURO_MINIMO').value;
      QcadtabelaFL_IMPOSTO.value := dtmDados.iQuery1.FieldByName
        ('FL_IMPOSTO').value;
      QcadtabelaFL_PESO.value := dtmDados.iQuery1.FieldByName('FL_Peso').value;
      QcadtabelaCODMUN.value := dtmDados.iQuery1.FieldByName('codmun').value;
      QcadtabelaCODMUNO.value := dtmDados.iQuery1.FieldByName('codmunO').value;
      QcadtabelaVL_TDE.value := dtmDados.iQuery1.FieldByName('vl_tde').value;
      QcadtabelaVL_TR.value := dtmDados.iQuery1.FieldByName('vl_tr').value;
      QcadtabelaVL_TRP.value := dtmDados.iQuery1.FieldByName('vl_trp').value;
      QcadtabelaVL_TRM.value := dtmDados.iQuery1.FieldByName('vl_trm').value;
      QcadtabelaVL_TDEP.value := dtmDados.iQuery1.FieldByName('VL_TDEP').value;
      QcadtabelaVL_TDEM.value := dtmDados.iQuery1.FieldByName('VL_TDEM').value;
      QcadtabelaDIAS.value := dtmDados.iQuery1.FieldByName('dias').value;
      QcadtabelaOPERACAO.value := QTabelaOPERACAO.value;
      QcadtabelaVL_HE.value := dtmDados.iQuery1.FieldByName('vl_he').value;
      QcadtabelaVL_SUFRA.value := dtmDados.iQuery1.FieldByName
        ('vl_sufra').value;
      QcadtabelaVL_VD.value := dtmDados.iQuery1.FieldByName('vl_vd').value;
      QcadtabelaVL_TDC.value := dtmDados.iQuery1.FieldByName('vl_tdc').value;
      QcadtabelaVL_PORTO.value := dtmDados.iQuery1.FieldByName
        ('vl_porto').value;
      QcadtabelaVL_PORTOMIN.value := dtmDados.iQuery1.FieldByName
        ('vl_portomin').value;
      QcadtabelaVL_TDA.value := dtmDados.iQuery1.FieldByName('vl_tda').value;
      QcadtabelaVL_TDAMIN.value := dtmDados.iQuery1.FieldByName
        ('vl_tdamin').value;
      QcadtabelaVL_TDAMAX.value := dtmDados.iQuery1.FieldByName
        ('vl_tdamax').value;
      // Implementa��o do projeto generalidades
      QcadtabelaVL_ARMAZENAGEM.value := dtmDados.iQuery1.FieldByName
        ('VL_ARMAZENAGEM').value;
      QcadtabelaVL_AJUDANTES.value := dtmDados.iQuery1.FieldByName
        ('VL_AJUDANTES').value;
      QcadtabelaVL_PALETIZACAO.value := dtmDados.iQuery1.FieldByName
        ('VL_PALETIZACAO').value;
      QcadtabelaVL_DIARIA.value := dtmDados.iQuery1.FieldByName
        ('VL_DIARIA').value;
      QcadtabelaVL_REENTREGA.value := dtmDados.iQuery1.FieldByName
        ('VL_REENTREGA').value;
      QcadtabelaVL_PERNOITE.value := dtmDados.iQuery1.FieldByName
        ('VL_PERNOITE').value;
      QcadtabelaVL_HORA_PARADA.value := dtmDados.iQuery1.FieldByName
        ('VL_HORA_PARADA').value;
      QcadtabelaREGIAO.value := cbRegiaod.Text;
      QcadtabelaREGIAOO.value := cbRegiaoo.Text;

      Qcadtabela.post;
      Qcadtabela.Close;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add('select max(cod_custo) cod from tb_custofrac ');
      dtmDados.iQuery1.Open;
      new := dtmDados.iQuery1.FieldByName('cod').value;
      // generalidade
      QGenItem.Close;
      QGenItem.Parameters[0].value := cod;
      QGenItem.Open;
      QGenItem.First;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
      ('insert into tb_custofrac_generalidades ( cod_custo,vl_pernoite,vl_vd,vl_diaria,veiculo)');
    dtmDados.iQuery1.sql.add(' values ( :0, :1, :2, :3, :4)');
      while not QGenItem.Eof do
      begin
        dtmDados.iQuery1.Parameters[0].value := new;
        dtmDados.iQuery1.Parameters[1].value := QGenItemVL_PERNOITE.AsFloat;
        dtmDados.iQuery1.Parameters[2].value := QGenItemVL_VD.AsFloat;
        dtmDados.iQuery1.Parameters[3].value := QGenItemVL_DIARIA.AsFloat;
        dtmDados.iQuery1.Parameters[4].value := QGenItemVEICULO.AsString;
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
        QGenItem.next;
      end;
      dtmDados.iQuery1.Close;

      // fra��o
      QItem.Close;
      QItem.Parameters[0].value := cod;
      QItem.Open;
      QItem.First;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add('insert into tb_custofrac_item (cod_custo, ');
      dtmDados.iQuery1.sql.add('datai, dataf, vl_rodoviario, nr_peso_de, ');
      dtmDados.iQuery1.sql.add('nr_peso_ate, vr_ctrc, vr_exced) ');
      dtmDados.iQuery1.sql.add('values (:0, :1, :2, :3, :4, :5, :6, :7)');
      while not QItem.eof do
      begin
        dtmDados.iQuery1.Parameters[0].value := new;
        dtmDados.iQuery1.Parameters[1].value := QItemDATAI.AsDateTime;
        dtmDados.iQuery1.Parameters[2].value := QItemDATAF.AsDateTime;
        dtmDados.iQuery1.Parameters[3].value := QItemVL_RODOVIARIO.AsFloat;
        dtmDados.iQuery1.Parameters[4].value := QItemNR_PESO_DE.AsFloat;
        dtmDados.iQuery1.Parameters[5].value := QItemNR_PESO_ATE.AsFloat;
        dtmDados.iQuery1.Parameters[6].value := QItemVR_CTRC.AsFloat;
        dtmDados.iQuery1.Parameters[7].value := QItemVR_EXCED.AsFloat;
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
        QItem.Next;
      end;
      dtmDados.iQuery1.Close;
      ShowMessage('Tabela Duplicada - Mude algum campo que n�o fique igual a duplicada por motivos de duplicidade');
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    Qcadtabela.Close;
    QTabela.Close;
    QTabela.sql.Text := sql;
    QTabela.Open;
    Qcadtabela.Open;
    QTabela.Last;
    QItem.Close;
    QItem.Open;
    tipo := '';

  end;
end;

procedure TfrmCadTabelaCustoFrac.bteditgenClick(Sender: TObject);
begin
  // Projeto Generalidades implementado por Hugo
  if not dtmDados.PodeInserir(name) then
    Exit;

  status := 'A';

  bteditgen.Enabled := not bteditgen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;
end;

procedure TfrmCadTabelaCustoFrac.btpostgenClick(Sender: TObject);
var cod : Integer;
begin
  // Projeto Generalidades implementado por Hugo , salva na tabela tb_custofrac
  if status = 'A' then
  begin
   { dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_custofrac set vl_paletizacao =:0 , vl_ajudantes =:1, vl_reentrega =:2, vl_armazenagem =:3,');
    dtmDados.iQuery1.sql.add
      (' vl_hora_parada =:4, VL_TDC =:5, VL_REENTREGA_MIN = :6, vl_armmin = :7 where cod_custo =:8');
    dtmDados.iQuery1.Parameters[0].value := edPalet.value;
    dtmDados.iQuery1.Parameters[1].value := edAjuda.value;
    dtmDados.iQuery1.Parameters[2].value := edReentrega.value;
    dtmDados.iQuery1.Parameters[3].value := edArmazem.value;
    dtmDados.iQuery1.Parameters[4].value := edHrParada.value;
    dtmDados.iQuery1.Parameters[5].value := edCanhoto.value;
    dtmDados.iQuery1.Parameters[6].value := edRee_m.value;
    dtmDados.iQuery1.Parameters[8].value := edArmMin.value;
    dtmDados.iQuery1.Parameters[7].value := QTabelaCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;        }
    cod := QTabelaCOD_CUSTO.AsInteger;

    Qcadtabela.edit;
    Qcadtabelavl_paletizacao.value := edPalet.value;
    Qcadtabelavl_ajudantes.value := edAjuda.value;
    Qcadtabelavl_reentrega.value := edReentrega.value;
    Qcadtabelavl_armazenagem.value := edArmazem.value;
    Qcadtabelavl_hora_parada.value := edHrParada.value;
    QcadtabelaVL_TDC.value := edCanhoto.value;
    QcadtabelaVL_REENTREGA_MIN.value := edRee_m.value;
    Qcadtabelavl_armmin.value := edArmMin.value;
    QcadtabelaCONTROLER.value := '';
    QcadtabelaDT_CONTROLER.clear;
    QCadtabela.post;

  end;

  status := '';

  bteditgen.Enabled := not bteditgen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;
end;

procedure TfrmCadTabelaCustoFrac.btcancelgenClick(Sender: TObject);
begin
  QTabela.Cancel;

  bteditgen.Enabled := not bteditgen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;
end;

procedure TfrmCadTabelaCustoFrac.btalterargenClick(Sender: TObject);
begin
  // Projeto Generalidades implementado por Hugo
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.alterar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  status := 'A';

  cbVeiculogen.ItemIndex := cbVeiculogen.Items.IndexOf(QGenItemVEICULO.AsString);
  edpernoiteGen.Text := QGenItemVL_PERNOITE.AsString;
  eddiariaGen.Text := QGenItemVL_DIARIA.AsString;
  eddedicadoGen.Text := QGenItemVL_VD.AsString;

  RestauraGen;
end;

procedure TfrmCadTabelaCustoFrac.btcancelagenClick(Sender: TObject);
begin
  RestauraGen;
end;

procedure TfrmCadTabelaCustoFrac.btexcluirgenClick(Sender: TObject);
begin
  // Projeto Generalidades implementado por Hugo
  if not dtmDados.PodeApagar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.apagar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('apagar').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  if Application.Messagebox('Voc� Deseja Apagar Este Registro?',
    'Apagar Registro', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_custofrac_generalidades where cod_id =:0');
    dtmDados.iQuery1.Parameters[0].value := QGenItemCOD_ID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end;
  LimpadadosGen;
end;

procedure TfrmCadTabelaCustoFrac.btinserirgenClick(Sender: TObject);
begin
  // Projeto Generalidades implementado por Hugo
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  status := 'I';
  RestauraGen;
end;

procedure TfrmCadTabelaCustoFrac.LimpadadosGen;
begin
  edpernoiteGen.Text := '0';
  eddiariaGen.Text := '0';
  eddedicadoGen.Text := '0';
  cbveiculogen.ItemIndex := 0;
end;

procedure TfrmCadTabelaCustoFrac.btsalvargenClick(Sender: TObject);
begin
  if (status = 'I') AND ((edpernoiteGen.Text <> '') or (eddiariaGen.Text <> '')
    or (eddedicadoGen.Text <> '')) then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('insert into tb_custofrac_generalidades ( cod_custo,vl_pernoite,vl_vd,vl_diaria,veiculo)');
    dtmDados.iQuery1.sql.add(' values ( :0, :1, :2, :3, :4)');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.Parameters[1].value := edpernoiteGen.value;
    dtmDados.iQuery1.Parameters[2].value := eddedicadoGen.value;
    dtmDados.iQuery1.Parameters[3].value := eddiariaGen.value;
    dtmDados.iQuery1.Parameters[4].value := cbveiculogen.Text;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end
  else if (status = 'A') AND ((edpernoiteGen.Text <> '') or
    (eddiariaGen.Text <> '') or (eddedicadoGen.Text <> '')) then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_custofrac_generalidades set vl_pernoite =:0');
    dtmDados.iQuery1.sql.add
      (',vl_vd =:1,vl_diaria =:2,veiculo =:3 where cod_id =:4');
    dtmDados.iQuery1.Parameters[0].value := edpernoiteGen.value;
    dtmDados.iQuery1.Parameters[1].value := eddedicadoGen.value;
    dtmDados.iQuery1.Parameters[2].value := eddiariaGen.value;
    dtmDados.iQuery1.Parameters[3].value := cbveiculogen.Text;
    dtmDados.iQuery1.Parameters[4].value := QGenItemCOD_ID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end;


  Qcadtabela.edit;
  QcadtabelaCONTROLER.value := '';
  QcadtabelaDT_CONTROLER.clear;
  QCadtabela.Post;

  status := '';
  RestauraGen;
  LimpadadosGen;
end;

procedure TfrmCadTabelaCustoFrac.btnAereoAClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.ALTERAR FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if QItemDATAF.value < date() then
    Exit;
  tipo := 'A';
  edDataAereoI.date := QAereoDATAI.value;
  edDataAereoF.date := QAereoDATAF.value;
  edAereoEntrega.value := QAereoVALOR_ENTREGA.Value;
  edPesoAereoI.value := QAereoPESO_DE.Value;
  edPesoAereoF.value := QAereoPESO_ATE.Value;
  edAereoColeta.value := QAereoVALOR_COLETA.Value;
  edAereoExc.value := QAereoVR_EXCED.Value;
  RestauraAereo;
  edDataAereoI.SetFocus;
end;

procedure TfrmCadTabelaCustoFrac.btnAereoCClick(Sender: TObject);
begin
  QAereo.Cancel;
  RestauraAereo;
end;

procedure TfrmCadTabelaCustoFrac.btnAereoEClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QAereo.Delete;
  end;
end;

procedure TfrmCadTabelaCustoFrac.btnAereoIClick(Sender: TObject);
begin
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  tipo := 'I';
  RestauraAereo;
  edDataAereoI.SetFocus;
  edDataAereoI.Text := '';
  edDataAereoF.Text := '';
  edAereoEntrega.value := 0;
  edPesoAereoI.value := 0;
  edPesoAereoF.value := 0;
  edAereoColeta.value := 0;
  edAereoExc.value := 0;
end;

procedure TfrmCadTabelaCustoFrac.btnAereoSClick(Sender: TObject);
var
  cod: Integer;
begin
  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    if (edDataAereoF.date - edDataAereoI.date) > 3 then
    begin
      ShowMessage
        ('Esta Tabelas SPOT o periodo de videncia n�o pode ser maior que 3 dias !!');
      Exit;
    end;
  end;

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_tabela_aereo(:0,:1,:2,:3,:4,''R'',:5) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QTabelaCOD_CUSTO.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataAereoI.date;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataAereoF.date;
  dtmDados.iQuery1.Parameters.ParamByName('3').value := edPesoAereoI.value;
  dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoAereoF.value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('5').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('5').value := QAereoCOD_CUSTO.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Data e Peso digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_tabelacusto_aereo (cod_tab, ' +
        'datai, dataf, valor_coleta, peso_de, peso_ate, valor_entrega, vr_exced) ' +
        'values ( ' + ':0, :1, :2, :3, :4, :5, :6, :7)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QTabelaCOD_CUSTO.AsInteger;
    end
    else
    begin
      cod := QItemCOD_TAB.AsInteger;
      dtmDados.iQuery1.sql.add('update tb_tabelacusto_aereo set datai = :1, ' +
        'dataf = :2, valor_coleta = :3, peso_de = :4, peso_ate =:5, valor_entrega = :6, '
        + 'vr_exced = :7 where cod_custo = :10');
      dtmDados.iQuery1.Parameters.ParamByName('10').value :=
        QAereoCOD_CUSTO.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataAereoI.date;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataAereoF.date;
    dtmDados.iQuery1.Parameters.ParamByName('3').value := edAereoColeta.value;
    dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoAereoI.value;
    dtmDados.iQuery1.Parameters.ParamByName('5').value := edPesoAereoF.value;
    dtmDados.iQuery1.Parameters.ParamByName('6').value := edAereoEntrega.value;
    dtmDados.iQuery1.Parameters.ParamByName('7').value := edAereoExc.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_custofrac set controler = null, dt_controler = null ');
    dtmDados.iQuery1.sql.add('where cod_custo = :0 ');
    dtmDados.iQuery1.Parameters[0].value := QAereoCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    if QcadtabelaOPERACAO.value = 'SPOT' then
      edDataI.Enabled := false;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QAereo.Close;
  QAereo.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
  QAereo.Open;
  if tipo = 'A' then
    QAereo.Locate('cod_tab', cod, [])
  else
    QAereo.Last;
  tipo := '';
  RestauraAereo;
end;

procedure TfrmCadTabelaCustoFrac.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.alterar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  Restaura;
  PageControl1.ActivePage := Detalhes;
  cbtra.ItemIndex := cbtra.Items.IndexOf(edTrans.Text);
  cbtra.SetFocus;
  cbVeiculo.ItemIndex := cbVeiculo.Items.IndexOf(edVeiculo.Text);
  tipo := 'A';
end;

procedure TfrmCadTabelaCustoFrac.dbgContratoTitleClick(Column: TColumn);
var
  icount: Integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.sql.Text) > 0 then
  begin
    if Column.FieldName = 'OPERACAO' then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('order by', QTabela.sql.Text) - 1) + 'order by t.operacao'
    else
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('order by', QTabela.sql.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.sql.Text := QTabela.sql.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTabelaCustoFrac.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QItemDATAF.Value < date() then
  begin
    DBGrid1.canvas.Brush.color := $006174FC;
    DBGrid1.canvas.font.color := clBlack;
  end
  else
  begin
    DBGrid1.canvas.Brush.color := clWindow;
    DBGrid1.canvas.font.color := clBlack;
  end;
  DBGrid1.DefaultDrawDataCell(Rect, DBGrid1.Columns[DataCol].field, State);
end;

procedure TfrmCadTabelaCustoFrac.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePage := Consulta;
  if GLBUSER = 'PCORTEZ' then
  begin
    btnExcluir_.Visible := true;
  end;
end;

procedure TfrmCadTabelaCustoFrac.btnExcluir_Click(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Esta Tabela?', 'Apagar Tabela',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_custofrac_item where cod_custo = :cn');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add('delete from tb_custofrac where cod_custo = :cn');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QTabela.Close;
    QTabela.Open;
  end;
end;

procedure TfrmCadTabelaCustoFrac.btnExcluirIClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QItem.Delete;
  end;
end;

procedure TfrmCadTabelaCustoFrac.FormCreate(Sender: TObject);
begin
  QTabela.Open;
  QTabela.First;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  QTransp.Open;
  sql := QTabela.sql.Text;
  cbtra.clear;
  cbtra.Items.add('');
  while not QTransp.eof do
  begin
    cbtra.Items.add(QTranspnm_fornecedor.value);
    QTransp.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct operacao from tb_operacao where fl_receita = ''N'' order by 1');
  dtmDados.iQuery1.Open;
  cbOperacao.clear;
  cbOperacaoPesq.clear;
  cbOperacao.Items.add('');
  cbOperacaoPesq.Items.add('');
  while not dtmDados.iQuery1.eof do
  begin
    cbOperacao.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    cbOperacaoPesq.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    dtmDados.iQuery1.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('SELECT distinct descri FROM cyber.RODFRO order by 1');
  dtmDados.iQuery1.Open;
  cbVeiculo.clear;
  cbVeiculoPesq.clear;
  cbveiculogen.clear;
  cbVeiculo.Items.add('');
  cbVeiculoPesq.Items.add('');
  cbveiculogen.Items.add('');
  while not dtmDados.iQuery1.eof do
  begin
    cbVeiculo.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    cbVeiculoPesq.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    cbveiculogen.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    dtmDados.iQuery1.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_fornecedor from ');
  dtmDados.iQuery1.sql.add('tb_custofrac t inner join cyber.rodcli f ');
  dtmDados.iQuery1.sql.add('on f.codclifor = t.cod_fornecedor order by 1 ');
  dtmDados.iQuery1.Open;

  cbPesqTransp.clear;
  cbPesqTransp.Items.add('');
  While not dtmDados.iQuery1.eof do
  begin
    cbPesqTransp.Items.add
      (alltrim(dtmDados.iQuery1.FieldByName('nm_fornecedor').value));
    dtmDados.iQuery1.Next;
  end;
  dtmDados.iQuery1.Close;
end;

procedure TfrmCadTabelaCustoFrac.JvDBGrid1CellClick(Column: TColumn);
begin
  cbVeiculo.Text := QGenItemVEICULO.AsString;
  edpernoiteGen.Text := QGenItemVL_PERNOITE.AsString;
  eddiariaGen.Text := QGenItemVL_DIARIA.AsString;
  eddedicadoGen.Text := QGenItemVL_VD.AsString;
end;

procedure TfrmCadTabelaCustoFrac.JvDBUltimGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QAereoDATAF.Value < date() then
  begin
    JvDBUltimGrid2.canvas.Brush.color := $006174FC;
    JvDBUltimGrid2.canvas.font.color := clBlack;
  end
  else
  begin
    JvDBUltimGrid2.canvas.Brush.color := clWindow;
    JvDBUltimGrid2.canvas.font.color := clBlack;
  end;
  JvDBUltimGrid2.DefaultDrawDataCell(Rect, JvDBUltimGrid2.Columns[DataCol].field, State);
end;

procedure TfrmCadTabelaCustoFrac.Limpadados;
begin
  edFreteM.value := 0;
  edPedagio.value := 0;
  edOutros.value := 0;
  edOutrosR.value := 0;
  edOutrosM.value := 0;
  edExc.value := 0;
  EdSeg.value := 0;
  EdGris.value := 0;
  edEntrega.value := 0;
  EdGrisM.value := 0;
  edTDE.value := 0;
  edTR.value := 0;
  edTRp.value := 0;
  edTRm.value := 0;
  edTDEp.value := 0;
  edTDEm.value := 0;
  edTdeMax.value := 0;
  edSuframa.value := 0;
  edpreceita.value := 0;
  edCanhoto.value := 0;
  edPorto.value := 0;
  edPorto.value := 0;
  edPorto.value := 0;
  edPorto.value := 0;
  edPorto.value := 0;
  cbImposto.Checked := false;
  cbPeso.Checked := false;
  cbVeiculo.ItemIndex := -1;
  cbCidade.ItemIndex := -1;
  cbCidadeO.ItemIndex := -1;
  cbStatus.Checked := false;
  cbOperacao.ItemIndex := -1;
  Obs.clear;
  edHE.value := 0;
  // Implementa��o do projeto generalidades
  edArmazem.value := 0;
  edAjuda.value := 0;
  edPalet.value := 0;
  edDiaria.value := 0;
  edReentrega.value := 0;
  //edPernoite.value := 0;
  edHrParada.value := 0;
  edUser.Clear;
  edDtIns.Clear;
  edDtAlt.Clear;
  edUserA.Clear;
end;

procedure TfrmCadTabelaCustoFrac.QTabelaAfterScroll(DataSet: TDataSet);
begin
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 +
    IntToStr(QTabela.RecordCount);
  Qcadtabela.Close;
  Qcadtabela.Parameters[0].value := QTabelaCOD_CUSTO.value;
  Qcadtabela.Open;
  QItem.Close;
  QItem.Parameters[0].value := QTabelaCOD_CUSTO.value;
  QItem.Open;

  // Projeto Generalidades implementado por Hugo
  QGenItem.Close;
  QGenItem.Parameters[0].value := QTabelaCOD_CUSTO.value;
  QGenItem.Open;

  {if QTabelaVEICULO.IsNull = false then
  begin
    cbveiculogen.Text := QTabelaVEICULO.AsString;
    cbveiculogen.Enabled := false;
  end
  else
  begin
    cbveiculogen.Text := ''; // QTabelaVEICULO.AsString;
    cbveiculogen.Enabled := true;
  end; }

  if tipo <> 'I' then
  begin
    CBLO.ItemIndex := CBLO.Items.IndexOf(QTabelaLOCAL.value);
    CBUFO.ItemIndex := CBUFO.Items.IndexOf(QcadtabelaDS_UF.value);
    cbLd.ItemIndex := cbLd.Items.IndexOf(QTabelaLOCAL_DES.value);
    cbUFD.ItemIndex := cbUFD.Items.IndexOf(QcadtabelaDS_UF_DES.value);
    edVeiculo.Text := QcadtabelaVEICULO.AsString;
    edTrans.Text := QTabelaNM_FORNECEDOR.value;
    edFreteM.value := QcadtabelaVL_FRETE_MINIMO.value;
    edPedagio.value := QcadtabelaVL_PEDAGIO.value;
    edOutros.value := QcadtabelaVL_OUTROS.value;
    edOutrosR.value := QcadtabelaVL_OUTROS_REAIS.value;
    edOutrosM.value := QcadtabelaVL_OUTROS_MINIMO.value;
    edExc.value := QcadtabelaVL_EXCEDENTE.value;
    EdSeg.value := QcadtabelaVL_AD.value;
    EdGris.value := QcadtabelaVL_GRIS.value;
    EdGrisM.value := QcadtabelaVL_GRIS_MINIMO.value;
    edEntrega.value := QcadtabelaVL_ENTREGA.value;
    edsegminimo.value := QcadtabelaSEGURO_MINIMO.value;
    edTDE.value := QcadtabelaVL_TDE.value;
    edTDEp.value := QcadtabelaVL_TDEP.value;
    edTDEm.value := QcadtabelaVL_TDEM.value;
    edTR.value := QcadtabelaVL_TR.value;
    edTRp.value := QcadtabelaVL_TRP.value;
    edTRm.value := QcadtabelaVL_TRM.value;
    edTdeMax.value := QcadtabelaVL_TDEMAX.value;
    edHE.value := QcadtabelaVL_HE.value;
    edSuframa.value := QcadtabelaVL_SUFRA.value;
    edPreceita.value := QcadtabelaVL_preceita.value;
    edCanhoto.value := QcadtabelaVL_TDC.value;
    edPorto.value := QcadtabelaVL_PORTO.value;
    edPortoMin.value := QcadtabelaVL_PORTOMIN.value;
    edTDA.value := QcadtabelaVL_TDA.value;
    edTDAMin.value := QcadtabelaVL_TDAMIN.value;
    edTDAMax.value := QcadtabelaVL_TDAMAX.value;
    edRee_m.value := QcadtabelaVL_REENTREGA_MIN.value;

    // Implementa��o do projeto generalidades
    edArmazem.value := QcadtabelaVL_ARMAZENAGEM.value;
    edAjuda.value := QcadtabelaVL_AJUDANTES.value;
    edPalet.value := QcadtabelaVL_PALETIZACAO.value;
    edDiaria.value := QcadtabelaVL_DIARIA.value;
    edReentrega.value := QcadtabelaVL_REENTREGA.value;
    edArmMin.value := QcadtabelaVL_ARMMIN.value;
    edHrParada.value := QcadtabelaVL_HORA_PARADA.value;
    cbRegiaoD.ItemIndex := cbRegiaoD.Items.IndexOf(QcadtabelaREGIAO.value);
    cbRegiaoO.ItemIndex := cbRegiaoO.Items.IndexOf(QcadtabelaREGIAOO.value);

    if QcadtabelaCODMUNO.value > 0 then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := QTabelaDS_UF.value;
      QCidade.Open;
      cbCidadeO.clear;
      cbCidadeO.Items.add('');
      while not QCidade.eof do
      begin
        cbCidadeO.Items.add(QCidadeDESCRI.value);
        QCidade.Next;
      end;
      if QCidade.Locate('codmun', QcadtabelaCODMUNO.AsInteger, []) then
        cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(QCidadeDESCRI.AsString);
    end
    else
      cbCidadeO.ItemIndex := -1;
    if QcadtabelaCODMUN.value > 0 then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := QTabelaDS_UF_DES.value;
      QCidade.Open;
      cbCidade.clear;
      cbCidade.Items.add('');
      while not QCidade.eof do
      begin
        cbCidade.Items.add(QCidadeDESCRI.value);
        QCidade.Next;
      end;
      if QCidade.Locate('codmun', QcadtabelaCODMUN.AsInteger, []) then
        cbCidade.ItemIndex := cbCidade.Items.IndexOf(QCidadeDESCRI.AsString);
    end
    else
      cbCidade.ItemIndex := -1;
    if QcadtabelaFL_IMPOSTO.value = 'S' then
      cbImposto.Checked := true
    else
      cbImposto.Checked := false;
    if QcadtabelaFL_PESO.value = 'S' then
      cbPeso.Checked := true
    else
      cbPeso.Checked := false;

    if QcadtabelaFL_STATUS.value = 'S' then
      cbStatus.Checked := true
    else
      cbStatus.Checked := false;

    Obs.clear;
    Obs.Lines.add(QcadtabelaOBS.AsString);

    cbOperacao.ItemIndex := cbOperacao.Items.IndexOf(QTabelaOPERACAO.AsString);

    edUser.Text := QcadtabelaUSUARIO.AsString;
    edDtIns.date := QcadtabelaDT_CADASTRO.value;

    edUserA.Text := QcadtabelaCONTROLER.AsString;
    edDtAlt.date := QcadtabelaDT_CONTROLER.value;

    if QTabelaFL_KM.value = 'S' then
    begin
      DBGrid1.Columns[2].Title.Caption := 'Valor KM';
      DBGrid1.Columns[4].Title.Caption := 'KM Inicial';
      DBGrid1.Columns[5].Title.Caption := 'KM Final';
      DBGrid1.Columns[7].Visible := true;
    end
    else
    begin
      DBGrid1.Columns[2].Title.Caption := 'Frete';
      DBGrid1.Columns[4].Title.Caption := 'Peso Inicial';
      DBGrid1.Columns[5].Title.Caption := 'Peso Final';
      DBGrid1.Columns[7].Visible := false;
    end;

  end;
  if QTabelafl_aereo.AsString = 'S' then
    PageControl2.Pages[1].TabVisible:=true
  else
    PageControl2.Pages[1].TabVisible:=false;

   //colorircampos;

end;

procedure TfrmCadTabelaCustoFrac.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
  QItem.Close;
  QTransp.Close;
  Qcadtabela.Close;
end;

procedure TfrmCadTabelaCustoFrac.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadTabelaCustoFrac.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QTabeladt_controler.IsNull then
  begin
    dbgContrato.canvas.Brush.color := clYellow;
    dbgContrato.canvas.font.color := clRed;
    dbgContrato.DefaultDrawDataCell(Rect, dbgContrato.Columns[DataCol]
      .field, State);
  end;
  if QTabelafl_status.value = 'N' then
  begin
    dbgContrato.canvas.Brush.color := clRed;
    dbgContrato.canvas.font.color := clWhite;
    dbgContrato.DefaultDrawDataCell(Rect, dbgContrato.Columns[DataCol]
      .field, State);
  end;
end;

procedure TfrmCadTabelaCustoFrac.RBAtivoClick(Sender: TObject);
begin
  // QTabela.Close;
  // QTabela.SQL[13] := 'where fl_status = ''S''' ;
  // QTabela.Open;

  QTabela.Close;
  if Pos('fl_status = ', QTabela.sql.Text) > 0 then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('fl_status =', QTabela.sql.Text) - 1) +
      'fl_status = ''S'' order by cod_custo ';
  QTabela.Open;

  // QTabela.SQL.savetofile('c:\sim\sql.txt');
end;

procedure TfrmCadTabelaCustoFrac.RbInativoClick(Sender: TObject);
begin
  // QTabela.Close;
  // QTabela.SQL[13] := 'where t.fl_status = ''N''' ;
  // QTabela.Open;
  QTabela.Close;
  if Pos('fl_status = ', QTabela.sql.Text) > 0 then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('fl_status =', QTabela.sql.Text) - 1) +
      'fl_status = ''N'' order by cod_custo ';
  QTabela.Open;
  // QTabela.SQL.savetofile('c:\sim\sql.txt');
end;

procedure TfrmCadTabelaCustoFrac.RestauraGen;
begin

  btinserirgen.Enabled := not btinserirgen.Enabled;
  btalterargen.Enabled := not btalterargen.Enabled;
  btexcluirgen.Enabled := not btexcluirgen.Enabled;
  btsalvargen.Enabled := not btsalvargen.Enabled;
  btcancelagen.Enabled := not btcancelagen.Enabled;

end;

procedure TfrmCadTabelaCustoFrac.Restaura;
begin
  btnNovo.Enabled := not btnNovo.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if edTrans.Visible = true then
  begin
    edTrans.Visible := false;
    cbtra.Visible := true;
  end
  else
  begin
    edTrans.Visible := true;
    cbtra.Visible := false;
  end;

  if edVeiculo.Visible = true then
  begin
    edVeiculo.Visible := false;
    cbVeiculo.Visible := true;
  end
  else
  begin
    edVeiculo.Visible := true;
    cbVeiculo.Visible := false;
  end;
end;

procedure TfrmCadTabelaCustoFrac.RestauraAereo;
begin
  btnAereoI.Enabled := not btnAereoI.Enabled;
  btnAereoA.Enabled := not btnAereoA.Enabled;
  btnAereoE.Enabled := not btnAereoE.Enabled;
  btnAereoS.Enabled := not btnAereoS.Enabled;
  btnAereoC.Enabled := not btnAereoC.Enabled;
  if edDataAereoI.Visible = true then
    edDataAereoI.Visible := false
  else
    edDataAereoI.Visible := true;
  if edDataAereoF.Visible = true then
    edDataAereoF.Visible := false
  else
    edDataAereoF.Visible := true;
  if edAereoColeta.Visible = true then
    edAereoColeta.Visible := false
  else
    edAereoColeta.Visible := true;
  if edPesoAereoI.Visible = true then
    edPesoAereoI.Visible := false
  else
    edPesoAereoI.Visible := true;
  if edPesoAereoF.Visible = true then
    edPesoAereoF.Visible := false
  else
    edPesoAereoF.Visible := true;
  if edAereoEntrega.Visible = true then
    edAereoEntrega.Visible := false
  else
    edAereoEntrega.Visible := true;
  if edAereoExc.Visible = true then
    edAereoExc.Visible := false
  else
    edAereoExc.Visible := true;
  if Panel10.Visible = true then
    Panel10.Visible := false
  else
    Panel10.Visible := true;
end;

procedure TfrmCadTabelaCustoFrac.edDataAereoFExit(Sender: TObject);
begin
  if edDataAereoF.date < edDataAereoI.date then
  begin
    ShowMessage('A Data Final N�o pode ser Menor que a Data Inicial');
    edDataAereoF.SetFocus;
  end;
end;

procedure TfrmCadTabelaCustoFrac.edDataFExit(Sender: TObject);
begin
  if edDataF.date < edDataI.date then
  begin
    ShowMessage('A Data Final N�o pode ser Menor que a Data Inicial');
    edDataF.SetFocus;
  end;
end;

procedure TfrmCadTabelaCustoFrac.edDataIExit(Sender: TObject);
begin
{  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select max(dataf) dt from tb_custofrac_item where cod_custo = :0 ');
  dtmDados.iQuery1.Parameters[0].value := QItemCOD_CUSTO.AsInteger;
  dtmDados.iQuery1.Open;
  if edDataI.date <= dtmDados.iQuery1.FieldByName('dt').value then
  begin
    ShowMessage
      ('A Data Inicial N�o pode ser Menor que a Maior Data Cadastrada');
    edDataI.SetFocus;
  end;       }
end;

procedure TfrmCadTabelaCustoFrac.edExcedExit(Sender: TObject);
begin
  btnsalvarI.SetFocus;
end;

procedure TfrmCadTabelaCustoFrac.edPesoAereoFExit(Sender: TObject);
begin
  if edPesoAereof.Value < edPesoi.value then
  begin
    ShowMessage('O Peso Final N�o pode ser Menor que o Peso Inicial');
    edPesoAereof.SetFocus;
  end;
end;

procedure TfrmCadTabelaCustoFrac.edPesofExit(Sender: TObject);
begin
  if edPesof.Value < edPesoi.value then
  begin
    ShowMessage('O Peso Final N�o pode ser Menor que o Peso Inicial');
    edPesof.SetFocus;
  end;
end;

procedure TfrmCadTabelaCustoFrac.edPesoiExit(Sender: TObject);
begin
{  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select count(*) linhas from tb_custofrac_item where cod_custo = :0 ');
  dtmDados.iQuery1.sql.add
    ('and to_date(:1,''dd/mm/yy'') >= datai and to_date(:2,''dd/mm/yy'') <= dataf ');
  dtmDados.iQuery1.sql.add('And nvl(nr_peso_de,0) <= :3 ');
  dtmDados.iQuery1.Parameters[0].value := QItemCOD_CUSTO.AsInteger;
  dtmDados.iQuery1.Parameters[1].value := edDataI.Text;
  dtmDados.iQuery1.Parameters[2].value := edDataF.Text;
  dtmDados.iQuery1.Parameters[3].value := edPesoi.value;
  dtmDados.iQuery1.Open;
  if dtmDados.iQuery1.FieldByName('linhas').value > 0 then
  begin
    ShowMessage('Existe Para esta data um Peso Inicial');
    edDataI.SetFocus;
  end;
   }
end;

procedure TfrmCadTabelaCustoFrac.edPReceitaExit(Sender: TObject);
begin
  if edPreceita.Value > 0 then
    ShowMessage('Quando este campo estiver preenchido, o sistema ignorar� todos os outros componentes de c�lculo de custo.');
end;

procedure TfrmCadTabelaCustoFrac.edTDEChange(Sender: TObject);
begin
  if edTDE.value > 0 then
    edTDEp.value := 0;
end;

procedure TfrmCadTabelaCustoFrac.edTDEpChange(Sender: TObject);
begin
  if edTDEp.value > 0 then
    edTDE.value := 0;
end;

procedure TfrmCadTabelaCustoFrac.RestauraI;
begin
  btnInserirI.Enabled := not btnInserirI.Enabled;
  btnAlterarI.Enabled := not btnAlterarI.Enabled;
  btnExcluirI.Enabled := not btnExcluirI.Enabled;
  btnsalvarI.Enabled := not btnsalvarI.Enabled;
  btnCancelarI.Enabled := not btnCancelarI.Enabled;
  if edDataI.Visible = true then
    edDataI.Visible := false
  else
    edDataI.Visible := true;
  if edDataF.Visible = true then
    edDataF.Visible := false
  else
    edDataF.Visible := true;
  if EdValor.Visible = true then
    EdValor.Visible := false
  else
    EdValor.Visible := true;
  if edPesoi.Visible = true then
    edPesoi.Visible := false
  else
    edPesoi.Visible := true;
  if edPesof.Visible = true then
    edPesof.Visible := false
  else
    edPesof.Visible := true;
  if edCtrc.Visible = true then
    edCtrc.Visible := false
  else
    edCtrc.Visible := true;
  if edExced.Visible = true then
    edExced.Visible := false
  else
    edExced.Visible := true;
  if Panel4.Visible = true then
    Panel4.Visible := false
  else
    Panel4.Visible := true;
  if QTabelaFL_KM.value = 'S' then
    edminimo.Visible := true
  else
    edminimo.Visible := false;
end;

procedure TfrmCadTabelaCustoFrac.tsAereoHide(Sender: TObject);
begin
  QAereo.Close;
end;

procedure TfrmCadTabelaCustoFrac.tsAereoShow(Sender: TObject);
begin
  QAereo.Close;
  QAereo.Parameters[0].value := QTabelaCOD_CUSTO.value;
  QAereo.Open;
end;

procedure TfrmCadTabelaCustoFrac.btnInserirIClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  tipo := 'I';
  RestauraI;
  edDataI.SetFocus;
  edDataI.Text := '';
  edDataF.Text := '';
  EdValor.value := 0;
  edPesoi.value := 0;
  edPesof.value := 0;
  edCtrc.value := 0;
  edExced.value := 0;
  edMinimo.value := 0;
end;

procedure TfrmCadTabelaCustoFrac.btnsalvarIClick(Sender: TObject);
var
  cod: string;
begin
  if (EdValor.value = 0) and (edExced.value = 0) then
  begin
    ShowMessage('Esta tabela n�o tem Valor ?');
    Exit;
  end;

  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    if (edDataF.date - edDataI.date) > 3 then
    begin
      ShowMessage
        ('Esta Tabelas SPOT o periodo de videncia n�o pode ser maior que 3 dias !!');
      Exit;
    end;
  end;

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_tabela(:0,:1,:2,:3,:4,''C'',:5) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QTabelaCOD_CUSTO.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataI.date;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataI.date;
  dtmDados.iQuery1.Parameters.ParamByName('3').value := edPesoi.value;
  dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesof.value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('5').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('5').value := QItemCOD_TAB.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Data e Peso digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_custofrac_item (cod_custo, ' +
        'datai, dataf, vl_rodoviario, nr_peso_de, nr_peso_ate, vr_ctrc, vr_exced, usuario, vl_minimo ) '
        + 'values ( ' + ':0, :1, :2, :3, :4, :5, :6, :7, :8, :9)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QTabelaCOD_CUSTO.AsInteger;
    end
    else
    begin
      cod := QItemID_REGISTRO.value;
      dtmDados.iQuery1.sql.add('update tb_custofrac_item set datai = :1, ' +
        'dataf = :2, vl_rodoviario = :3, nr_peso_de = :4, nr_peso_ate =:5, vr_ctrc = :6, '
        + 'vr_exced = :7, usualt = :8, dataalt = sysdate, vl_minimo = :9 where cod_tab = :10');
      dtmDados.iQuery1.Parameters.ParamByName('10').value :=
        QItemCOD_TAB.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataI.date;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataF.date;
    dtmDados.iQuery1.Parameters.ParamByName('3').value := EdValor.value;
    dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoi.value;
    dtmDados.iQuery1.Parameters.ParamByName('5').value := edPesof.value;
    dtmDados.iQuery1.Parameters.ParamByName('6').value := edCtrc.value;
    dtmDados.iQuery1.Parameters.ParamByName('7').value := edExced.value;
    dtmDados.iQuery1.Parameters.ParamByName('8').value := GLBUSER;
    dtmDados.iQuery1.Parameters.ParamByName('9').value := edMinimo.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_custofrac set controler = null, dt_controler = null ');
    dtmDados.iQuery1.sql.add('where cod_custo = :0 ');
    dtmDados.iQuery1.Parameters[0].value := QItemCOD_CUSTO.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    if QcadtabelaOPERACAO.value = 'SPOT' then
      edDataI.Enabled := false;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QItem.Close;
  QItem.Parameters[0].value := QTabelaCOD_CUSTO.AsInteger;
  QItem.Open;
  if tipo = 'A' then
    QItem.Locate('ID_REGISTRO', cod, [])
  else
    QItem.Last;
  tipo := '';
  RestauraI;
end;

procedure TfrmCadTabelaCustoFrac.btnVerClick(Sender: TObject);
var
  caminho: string;
begin
  if Qcadtabeladoc_controler.AsString <> '' then
  begin
    caminho := glbcontroler + Qcadtabeladoc_controler.AsString;
    ShellExecute(Handle, nil, Pchar(caminho), nil, nil, SW_SHOWNORMAL);
  end
  else
    ShowMessage('Tabela Sem Documenta��o Vinculada !!');
end;

procedure TfrmCadTabelaCustoFrac.btProcurarClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QTabela.Close;
  QTabela.sql.clear;
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add('select * from( ');
  QTabela.sql.add
    ('select t.cod_custo, f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_fornecedor, ds_uf, ds_uf_des, vl_frete_minimo, vl_ad, veiculo, t.operacao, op.fl_km, t.fl_status, dt_controler, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local_des, op.fl_aereo ');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add(', max(i.dataf) dataf ');

  QTabela.sql.add
    ('from tb_custofrac t inner join cyber.rodcli f on t.cod_fornecedor = f.codclifor ');
  QTabela.sql.add
    ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''N'' ');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add
      ('left join tb_custofrac_item i on i.cod_custo = t.cod_custo ');

  QTabela.sql.add('where f.nomeab is not null');

  if cbPesqTransp.Text <> '' then
  begin
    QTabela.sql.add('and (f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf)) = ' +
      #39 + cbPesqTransp.Text + #39);
  end;
  if cbVeiculoPesq.Text <> '' then
  begin
    QTabela.sql.add('and t.veiculo = ' + #39 + cbVeiculoPesq.Text + #39);
  end;
  if cbPesqUFO.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf = ' + #39 + cbPesqUFO.Text + #39);
  end;
  if cbPesqUFD.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf_des = ' + #39 + cbPesqUFD.Text + #39);
  end;
  if cbPesqLO.Text <> '' then
  begin
    QTabela.sql.add('and t.fl_local = ' + #39 + copy(cbPesqLO.Text, 1,
      1) + #39);
    if cbCidOPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidOPesq.Text, []);
      QTabela.sql.add('and t.codmuno = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbPesqLD.Text <> '' then
  begin
    QTabela.sql.add('and t.fl_local_des = ' + #39 + copy(cbPesqLD.Text, 1,
      1) + #39);
    if cbCidDPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidDPesq.Text, []);
      QTabela.sql.add('and t.codmun = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbOperacaoPesq.Text <> '' then
  begin
    QTabela.sql.add('and t.operacao = ' + #39 + (cbOperacaoPesq.Text) + #39);
  end;

  if RGFiltro.ItemIndex = 1 then
    QTabela.sql.add('and t.dt_controler is null and t.fl_status = ''S'' ')
  else if RGFiltro.ItemIndex = 2 then
    QTabela.sql.add('and t.reprovado is not null ');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add
      ('group by t.cod_custo , f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf), ds_uf, ds_uf_des, vl_frete_minimo, vl_ad, veiculo, t.fl_local, t.fl_local_des, op.fl_km, t.operacao, t.fl_status, dt_controler ');

  if RGFiltro.ItemIndex = 3 then
  begin
    QTabela.sql.add(') where trunc(dataf) < trunc(sysdate)');
  end;

  if RBAtivo.Checked = true then
    QTabela.sql.add('and fl_status = ''S'' ')
  else
    QTabela.sql.add('and fl_status = ''N'' ');

  QTabela.sql.add('order by cod_custo');

  QTabela.Open;
  Screen.Cursor := crDefault;
  if QTabela.eof then
  begin
    ShowMessage('N�o Encontrado Nenhum Resultado Para Esta Pesquisa !!');
  end
  else
    PageControl1.ActivePageIndex := 0;
end;

procedure TfrmCadTabelaCustoFrac.btRetirarFiltroClick(Sender: TObject);
begin
  cbPesqTransp.ItemIndex := -1;
  cbPesqUFO.ItemIndex := -1;
  cbPesqLO.ItemIndex := -1;
  cbPesqUFD.ItemIndex := -1;
  cbPesqLD.ItemIndex := -1;
  QTabela.Close;
  QTabela.sql.Text := sql;
  QTabela.Open;
  PageControl1.ActivePageIndex := 0;
  sql := '';
end;

procedure TfrmCadTabelaCustoFrac.cbPesqLDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLD.Text, 1, 1));
  QCidade.Open;
  cbCidDPesq.clear;
  cbCidDPesq.Items.add('');
  while not QCidade.eof do
  begin
    cbCidDPesq.Items.add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaCustoFrac.cbPesqLOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLO.Text, 1, 1));
  QCidade.Open;
  cbCidOPesq.clear;
  cbCidOPesq.Items.add('');
  while not QCidade.eof do
  begin
    cbCidOPesq.Items.add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaCustoFrac.cbUFDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbLd.Text, 1, 1));
  QCidade.Open;
  cbCidade.clear;
  cbCidade.Items.add('');
  while not QCidade.eof do
  begin
    cbCidade.Items.add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaCustoFrac.CBUFOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := CBUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(CBLO.Text, 1, 1));
  QCidade.Open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaCustoFrac.colorircampos;
begin
  if edFreteM.value > 0 then
    edFreteM.Enabled := false
  else
    edFreteM.Enabled := true;
  if edPedagio.value > 0 then
    edPedagio.Enabled := false
  else
    edPedagio.Enabled := true;
  if edOutros.value > 0 then
    edOutros.Enabled := false
  else
    edOutros.Enabled := true;
  if edOutrosM.value > 0 then
    edOutrosM.Enabled := false
  else
    edOutrosM.Enabled := true;
  if edExc.value > 0 then
    edExc.Enabled := false
  else
    edExc.Enabled := true;
  if EdSeg.value > 0 then
    EdSeg.Enabled := false
  else
    EdSeg.Enabled := true;
  if EdGris.value > 0 then
    EdGris.Enabled := false
  else
    EdGris.Enabled := true;
  if EdGrisM.value > 0 then
    EdGrisM.Enabled := false
  else
    EdGrisM.Enabled := true;
  if edsegminimo.value > 0 then
    edsegminimo.Enabled := false
  else
    edsegminimo.Enabled := true;
  if edTDE.value > 0 then
    edTDE.Enabled := false
  else
    edTDE.Enabled := true;
  if edTDEp.value > 0 then
    edTDEp.Enabled := false
  else
    edTDEp.Enabled := true;
  if edTDEm.value > 0 then
    edTDEm.Enabled := false
  else
    edTDEm.Enabled := true;
  if edTR.value > 0 then
    edTR.Enabled := false
  else
    edTR.Enabled := true;
  if edTdeMax.value > 0 then
    edTdeMax.Enabled := false
  else
    edTdeMax.Enabled := true;
  if edentrega.value > 0 then
    edentrega.Enabled := false
  else
    edentrega.Enabled := true;

  if edcanhoto.value > 0 then
    edcanhoto.Enabled := false
  else
    edcanhoto.Enabled := true;

  if edHE.Value > 0 then
    edHE.Enabled := false
  else
    edHE.Enabled := true;

  if edOutrosR.Value > 0 then
    edOutrosR.Enabled := false
  else
    edOutrosR.Enabled := true;

  if edTRp.Value > 0 then
    edTRp.Enabled := false
  else
    edTRp.Enabled := true;

  if edTRm.Value > 0 then
    edTRm.Enabled := false
  else
    edTRm.Enabled := true;

  if edSuframa.Value > 0 then
    edSuframa.Enabled := false
  else
    edSuframa.Enabled := true;

  if edPorto.Value > 0 then
    edPorto.Enabled := false
  else
    edPorto.Enabled := true;

  if edPortoMin.Value > 0 then
    edPortoMin.Enabled := false
  else
    edPortoMin.Enabled := true;

  if edTDA.Value > 0 then
    edTDA.Enabled := false
  else
    edTDA.Enabled := true;

  if edTDAMin.Value > 0 then
    edTDAMin.Enabled := false
  else
    edTDAMin.Enabled := true;

  if edTDAmax.Value > 0 then
    edTDAmax.Enabled := false
  else
    edTDAmax.Enabled := true;

  if edDiaria.Value > 0 then
    edDiaria.Enabled := false
  else
    edDiaria.Enabled := true;

  if edPReceita.Value > 0 then
    edPReceita.Enabled := false
  else
    edPReceita.Enabled := true;

  if edPalet.Value > 0 then
    edPalet.Enabled := false
  else
    edPalet.Enabled := true;

  if edAjuda.Value > 0 then
    edAjuda.Enabled := false
  else
    edAjuda.Enabled := true;

  if edReentrega.Value > 0 then
    edReentrega.Enabled := false
  else
    edReentrega.Enabled := true;

  if edRee_m.Value > 0 then
    edRee_m.Enabled := false
  else
    edRee_m.Enabled := true;

  if edArmazem.Value > 0 then
    edArmazem.Enabled := false
  else
    edArmazem.Enabled := true;

  if edArmmin.Value > 0 then
    edArmmin.Enabled := false
  else
    edArmmin.Enabled := true;

  if edHrParada.Value > 0 then
    edHrParada.Enabled := false
  else
    edHrParada.Enabled := true;

end;

procedure TfrmCadTabelaCustoFrac.btnAlterarIClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.* FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''CustoSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  if QItemDATAF.value < date() then
    Exit;

  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    if QItemDATAF.Value <= (now-1) then
    begin
      ShowMessage
        ('Esta Tabelas SPOT ap�s periodo de vig�ncia n�o pode ser alterada !!');
      Exit;
    end;
  end;

  tipo := 'A';
  edDataI.date := QItemDATAI.value;
  edDataF.date := QItemDATAF.value;
  EdValor.value := QItemVL_RODOVIARIO.value;
  edPesoi.value := QItemNR_PESO_DE.value;
  edPesof.value := QItemNR_PESO_ATE.value;
  edCtrc.value := QItemVR_CTRC.value;
  edExced.value := QItemVR_EXCED.value;
  edMinimo.value := QItemvl_minimo.value;
  RestauraI;
  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    edDataI.Enabled := false;
    edValor.SetFocus;
  end
  else
    edDataI.SetFocus;
end;

procedure TfrmCadTabelaCustoFrac.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
