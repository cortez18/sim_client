unit NFFaturar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, DB, ADODB, DBCtrls, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvBaseEdits, JvExMask, JvToolEdit, JvDBLookup,
  Gauges, ComObj;

type
  TfrmNFFaturar = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    Label3: TLabel;
    edTotal: TJvCalcEdit;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    JvDBGrid1: TJvDBGrid;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    DBNavigator1: TDBNavigator;
    QClienteCOD_CLIENTE: TBCDField;
    QCtrcNR_CONHECIMENTO: TBCDField;
    QCtrcFL_TIPO: TStringField;
    QCtrcDT_CONHECIMENTO: TDateTimeField;
    QCtrcNM_USUARIO: TStringField;
    QCtrcNM_CLI_OS: TStringField;
    QCtrcNR_FATURA: TBCDField;
    QCtrcNR_MANIFESTO: TBCDField;
    QCtrcFL_EMPRESA: TBCDField;
    QCtrcNR_SERIE: TStringField;
    QCtrcNM_DESTINATARIO: TStringField;
    QCtrcNM_REMETENTE: TStringField;
    QCtrcCOD_CONHECIMENTO: TBCDField;
    QCtrcNF_VALORTT: TBCDField;
    QCtrcNR_TABELA: TBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNFFaturar: TfrmNFFaturar;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmNFFaturar.btnExcelClick(Sender: TObject);
var
  coluna, linha: integer;
  excel: variant;
  valor: string;
begin
  try
    excel := CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
  except
    Application.MessageBox('Vers�o do Ms-Excel Incompat�vel', 'Erro',
      MB_OK + MB_ICONEXCLAMATION);
  end;
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  try
    for linha := 0 to QCtrc.RecordCount - 1 do
    begin
      for coluna := 1 to JvDBGrid1.Columns.Count do
      begin
        // valor:= QCtrc.Fields[coluna-1].AsString;
        // excel.cells [linha+2,coluna]:=valor;

        valor := QCtrc.Fields[coluna - 1].AsString;
        if QCtrc.Fields[coluna - 1].DataType = ftDateTime then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := 'dd/mm/aaaa';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsDateTime;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftString then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '@';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsString;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftBCD then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsFloat;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftFloat then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsFloat;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftInteger then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsInteger;
        end
        else
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '@';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsString;
        end;
      end;
      Gauge1.AddProgress(1);
      QCtrc.Next;
    end;
    for coluna := 1 to JvDBGrid1.Columns.Count do
    begin
      valor := JvDBGrid1.Columns[coluna - 1].Title.Caption;
      excel.cells[1, coluna] := valor;
    end;
    excel.Columns.AutoFit;
    excel.visible := true;
  except
    Application.MessageBox('Aconteceu um erro desconhecido durante a convers�o'
      + 'da tabela para o Ms-Excel', 'Erro', MB_OK + MB_ICONEXCLAMATION);
  end;
end;

procedure TfrmNFFaturar.btRelatorioClick(Sender: TObject);
begin
  if Trim(ledIdCliente.LookupValue) = '' then
  begin
    ShowMessage('Escolha um Cliente !!');
    exit;
  end;
  Panel1.visible := true;
  Application.ProcessMessages;

  QCtrc.close;
  QCtrc.SQL[12] := 'AND c.cod_pagador = ' + QuotedStr(ledIdCliente.LookupValue);

  // Qctrc.sql.SaveToFile('c:\sim\sql.text');
  QCtrc.open;
  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.clear;
  dtmdados.iQuery1.SQL.add
    ('select coalesce(sum(c.nf_valortt),0) as total from tb_conhecimento c ');
  dtmdados.iQuery1.SQL.add('where nr_conhecimento is not null ');
  dtmdados.iQuery1.SQL.add('AND c.cod_pagador = ' +
    QuotedStr(ledIdCliente.LookupValue));
  dtmdados.iQuery1.SQL.add
    ('and c.fl_tipo = ''C'' and c.nf_valortt > 0 and nvl(c.nf_nr,0) = 0');

  // showmessage(dtmdados.Query1.SQL.text);
  dtmdados.iQuery1.open;
  edTotal.Value := dtmdados.iQuery1.FieldByName('total').Value;
  dtmdados.iQuery1.close;
  Panel1.visible := false;
end;

procedure TfrmNFFaturar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmNFFaturar.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmNFFaturar.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmNFFaturar.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
