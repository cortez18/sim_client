unit Rel_data_vencidas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB, FileCtrl, DBCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ExtCtrls, JvExExtCtrls, JvRadioGroup, Gauges;

type
  TfrmRel_data_vencidas = class(TForm)
    QCNH: TADOQuery;
    navnavig: TDataSource;
    Panel1: TPanel;
    QGR: TADOQuery;
    QRenavam: TADOQuery;
    QGRNM: TStringField;
    QGRDT: TDateTimeField;
    QCNHNM: TStringField;
    QCNHDT: TDateTimeField;
    QRenavamDT: TDateTimeField;
    Panel2: TPanel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Panel3: TPanel;
    RGTipo: TJvRadioGroup;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    btnExcel: TBitBtn;
    btRelatorio: TBitBtn;
    DBNavigator1: TDBNavigator;
    QCNHCOD: TBCDField;
    QGRCOD: TBCDField;
    QRenavamCOD: TBCDField;
    QRenavamNM: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_data_vencidas: TfrmRel_data_vencidas;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_data_vencidas.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  if RGTipo.ItemIndex = 0 then
  begin
    sarquivo := DirectoryListBox1.Directory + '\Rel_Dtos_vencidos_CNH.xls';
    QCNH.first;
    while not QCNH.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        memo1.Add('<th><FONT class=texto2>' + QCNH.fieldbyname(JvDBGrid1.Columns
          [i].FieldName).AsString + '</th>');
      QCNH.Next;
      memo1.Add('<tr>');
    end;
  end
  else if RGTipo.ItemIndex = 1 then
  begin
    sarquivo := DirectoryListBox1.Directory + '\Rel_Dtos_vencidos_GR.xls';
    QGR.first;
    while not QGR.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        memo1.Add('<th><FONT class=texto2>' +
          QGR.fieldbyname(JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
      QGR.Next;
      memo1.Add('<tr>');
    end;
  end
  else
  begin
    sarquivo := DirectoryListBox1.Directory + '\Rel_Dtos_vencidos_Renavam.xls';
    QRenavam.first;
    while not QRenavam.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        memo1.Add('<th><FONT class=texto2>' + QRenavam.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
      QRenavam.Next;
      memo1.Add('<tr>');
    end;
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmRel_data_vencidas.btRelatorioClick(Sender: TObject);
begin
  Panel1.Visible := true;
  Application.ProcessMessages;
  if RGTipo.ItemIndex = 0 then
  begin
    navnavig.DataSet := QCNH;
    QCNH.close;
    QCNH.open;
    JvDBGrid1.Columns[0].Title.Caption := 'C�digo';
    JvDBGrid1.Columns[2].Title.Caption := 'Transportador';
  end
  else if RGTipo.ItemIndex = 1 then
  begin
    navnavig.DataSet := QGR;
    QGR.close;
    QGR.open;
    JvDBGrid1.Columns[0].Title.Caption := 'C�digo';
    JvDBGrid1.Columns[2].Title.Caption := 'Transportador';
  end
  else
  begin
    navnavig.DataSet := QRenavam;
    QRenavam.close;
    QRenavam.open;
    JvDBGrid1.Columns[0].Title.Caption := 'Frota';
    JvDBGrid1.Columns[2].Title.Caption := 'Placa';
  end;
  Panel1.Visible := false;
  btnExcel.Enabled := true;
end;

procedure TfrmRel_data_vencidas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCNH.close;
  QGR.close;
  QRenavam.close;
end;

procedure TfrmRel_data_vencidas.FormCreate(Sender: TObject);
begin
  btnExcel.Enabled := false;
end;

procedure TfrmRel_data_vencidas.RGTipoClick(Sender: TObject);
begin
  btnExcel.Enabled := false;
end;

end.
