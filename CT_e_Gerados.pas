unit CT_e_Gerados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvCombobox, JvDBControls, DBCtrls, JvExMask,
  JvToolEdit, JvBaseEdits, DB, ADODB, ImgList, Mask, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvEdit, ExtCtrls, Buttons, DateUtils, System.ImageList;

type
  TfrmCT_e_Gerados = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel3: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    iml: TImageList;
    Label27: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    QCTRC: TADOQuery;
    Label18: TLabel;
    btnCancelar: TBitBtn;
    Label32: TLabel;
    edFatura: TJvEdit;
    Label33: TLabel;
    dtsCTRC: TDataSource;
    Label34: TLabel;
    Label35: TLabel;
    DBGNF: TJvDBGrid;
    QNF: TADOQuery;
    dtsNF: TDataSource;
    Label24: TLabel;
    Label36: TLabel;
    DBEdit2: TDBEdit;
    Panel4: TPanel;
    Label38: TLabel;
    EdObs: TEdit;
    btnFechar: TBitBtn;
    lbStatus: TLabel;
    btnDacte: TBitBtn;
    Label39: TLabel;
    mCte: TMemo;
    QCTRCCOD_CONHECIMENTO: TBCDField;
    QCTRCFL_EMPRESA: TBCDField;
    QCTRCCUFEMIT: TStringField;
    QCTRCCODUFORIGEM: TStringField;
    QCTRCUFORI: TStringField;
    QCTRCCODCTRC: TBCDField;
    QCTRCCTE_CHAVE: TStringField;
    QCTRCCFOP: TStringField;
    QCTRCNATOPERACAO: TStringField;
    QCTRCNR_SERIE: TStringField;
    QCTRCFORMAPAGTO: TStringField;
    QCTRCMODELOCTE: TStringField;
    QCTRCNUMEROCTE: TBCDField;
    QCTRCEMISSAOCTE: TDateTimeField;
    QCTRCTPIMP: TStringField;
    QCTRCTPEMIS: TStringField;
    QCTRCDIGVERFICHAVEACES: TStringField;
    QCTRCTPAMB: TStringField;
    QCTRCTPCTE: TStringField;
    QCTRCPROCEMI: TStringField;
    QCTRCVERPROC: TStringField;
    QCTRCCODMUNENVI: TStringField;
    QCTRCMUNENVI: TStringField;
    QCTRCUFENV: TStringField;
    QCTRCMODAL: TStringField;
    QCTRCTPSERV: TStringField;
    QCTRCCODMUNINI: TStringField;
    QCTRCMUNINI: TStringField;
    QCTRCUFINI: TStringField;
    QCTRCCMUNFIM: TStringField;
    QCTRCXMUNFIM: TStringField;
    QCTRCUFFIM: TStringField;
    QCTRCRETIRA: TStringField;
    QCTRCTOMA03: TStringField;
    QCTRCCNPJEMI: TStringField;
    QCTRCIEEMI: TStringField;
    QCTRCNOMEEMI: TStringField;
    QCTRCLGREMI: TStringField;
    QCTRCNROEMI: TBCDField;
    QCTRCBAIRROEMMI: TStringField;
    QCTRCCODMUNEMI: TStringField;
    QCTRCMUNEMI: TStringField;
    QCTRCCEPEMI: TStringField;
    QCTRCUFEMI: TStringField;
    QCTRCFONEEMI: TStringField;
    QCTRCCNPJTOM: TStringField;
    QCTRCIETOM: TStringField;
    QCTRCNOMETOM: TStringField;
    QCTRCFONETOM: TStringField;
    QCTRCLGRTOM: TStringField;
    QCTRCCOMPLTOM: TStringField;
    QCTRCNROTOM: TBCDField;
    QCTRCBAIRROTOM: TStringField;
    QCTRCCODMUNTOM: TStringField;
    QCTRCMUNTOM: TStringField;
    QCTRCCEPTOM: TStringField;
    QCTRCUFTOM: TStringField;
    QCTRCCODPAISTOM: TStringField;
    QCTRCNOMEPAISTOM: TStringField;
    QCTRCTIPOPESSOAREM: TStringField;
    QCTRCCNPJREM: TStringField;
    QCTRCIEREM: TStringField;
    QCTRCNOMEREM: TStringField;
    QCTRCFONEREM: TStringField;
    QCTRCLGRREM: TStringField;
    QCTRCCOMPLREM: TStringField;
    QCTRCNROREM: TBCDField;
    QCTRCBAIRROREM: TStringField;
    QCTRCCODMUNREM: TStringField;
    QCTRCMUNREM: TStringField;
    QCTRCCEPREM: TStringField;
    QCTRCUFREM: TStringField;
    QCTRCCODPAISREM: TStringField;
    QCTRCNOMEPAISREM: TStringField;
    QCTRCTIPOPESSOAEXP: TStringField;
    QCTRCCNPJEXP: TStringField;
    QCTRCIEEXP: TStringField;
    QCTRCNOMEEXP: TStringField;
    QCTRCFONEEXP: TStringField;
    QCTRCLGREXP: TStringField;
    QCTRCCOMPLEXP: TStringField;
    QCTRCNROEXP: TBCDField;
    QCTRCBAIRROEXP: TStringField;
    QCTRCCODMUNEXP: TStringField;
    QCTRCMUNEXP: TStringField;
    QCTRCCEPEXP: TStringField;
    QCTRCUFEXP: TStringField;
    QCTRCCODPAISEXP: TStringField;
    QCTRCNOMEPAISEXP: TStringField;
    QCTRCCNPJDES: TStringField;
    QCTRCTIPOPESSOA: TStringField;
    QCTRCIEDES: TStringField;
    QCTRCNOMEDES: TStringField;
    QCTRCFONEDES: TStringField;
    QCTRCLGRDES: TStringField;
    QCTRCCOMPLDES: TStringField;
    QCTRCNRODES: TBCDField;
    QCTRCBAIRRODES: TStringField;
    QCTRCCODMUNDES: TStringField;
    QCTRCMUNDES: TStringField;
    QCTRCCEPDES: TStringField;
    QCTRCUFDES: TStringField;
    QCTRCCODPAISDES: TStringField;
    QCTRCNOMEPAISDES: TStringField;
    QCTRCCNPJRED: TStringField;
    QCTRCIERED: TStringField;
    QCTRCNOMERED: TStringField;
    QCTRCFONERED: TStringField;
    QCTRCLGRRED: TStringField;
    QCTRCCOMPLRED: TStringField;
    QCTRCNRORED: TBCDField;
    QCTRCBAIRRORED: TStringField;
    QCTRCCODMUNRED: TStringField;
    QCTRCMUNRED: TStringField;
    QCTRCCEPRED: TStringField;
    QCTRCUFRED: TStringField;
    QCTRCCODPAISRED: TStringField;
    QCTRCNOMEPAISRED: TStringField;
    QCTRCVLTOTPRESTACAO: TFloatField;
    QCTRCVLRECEBPRESTACAO: TFloatField;
    QCTRCCOMPDESPACHO: TStringField;
    QCTRCCOMPDESPACHOVALOR: TFloatField;
    QCTRCCOMPGRIS: TStringField;
    QCTRCCOMPGRISVALOR: TFloatField;
    QCTRCCOMPADVALOREN: TStringField;
    QCTRCCOMPADVALORENVALOR: TFloatField;
    QCTRCCOMPPEDAGIO: TStringField;
    QCTRCCOMPPEDAGIOVALOR: TFloatField;
    QCTRCCOMPOUTRASTAXAS: TStringField;
    QCTRCCOMPOUTROSTAXASVALOR: TFloatField;
    QCTRCCOMPFLUVIAL: TStringField;
    QCTRCCOMPFLUVIALVALOR: TFloatField;
    QCTRCCOMPTR: TStringField;
    QCTRCCOMPTRVALOR: TFloatField;
    QCTRCCOMPTDE: TStringField;
    QCTRCCOMPTDEVALOR: TFloatField;
    QCTRCCOMPEXED: TStringField;
    QCTRCCOMPEXEDVALOR: TFloatField;
    QCTRCCOMPBALSA: TStringField;
    QCTRCCOMPBALSAVALOR: TFloatField;
    QCTRCCOMPREDFLU: TStringField;
    QCTRCCOMPREDFLUVALOR: TFloatField;
    QCTRCCOMPAGEND: TStringField;
    QCTRCCOMPAGENDVALOR: TFloatField;
    QCTRCCOMPPALLET: TStringField;
    QCTRCCOMPPALLETVALOR: TFloatField;
    QCTRCCOMPPORTO: TStringField;
    QCTRCCOMPPORTOVALOR: TFloatField;
    QCTRCCOMPALFA: TStringField;
    QCTRCCOMPALFAVALOR: TFloatField;
    QCTRCCOMPCANHOTO: TStringField;
    QCTRCCOMPCANHOTOVALOR: TFloatField;
    QCTRCCOMPDEV: TStringField;
    QCTRCCOMPDEVVALOR: TFloatField;
    QCTRCCOMPTAS: TStringField;
    QCTRCCOMPTASVALOR: TFloatField;
    QCTRCCSTIMPOSTO: TStringField;
    QCTRCDESCCSTIMPOSTO: TStringField;
    QCTRCBASEIMPOSTO: TFloatField;
    QCTRCALIQIMPOSTO: TBCDField;
    QCTRCVALORIMMPOSTO: TBCDField;
    QCTRCVALORCARGA: TFloatField;
    QCTRCDESCRICAOCARGA: TStringField;
    QCTRCCODIGOUNIDADE: TStringField;
    QCTRCTIPOMEDIDA: TStringField;
    QCTRCQTDMEDIDA: TFloatField;
    QCTRCRNTRC: TStringField;
    QCTRCDPREV: TDateTimeField;
    QCTRCLOTA: TStringField;
    QCTRCPESOTOTAL: TFloatField;
    QCTRCCUBAGEM: TFloatField;
    QCTRCPESOCALCULO: TBCDField;
    QCTRCDS_TIPO_FRETE: TStringField;
    QCTRCOBSERVACAO: TStringField;
    QCTRCTIPO: TStringField;
    QCTRCFORMAPAGAMENTO: TStringField;
    QCTRCTOMADOR: TStringField;
    QCTRCPROTOCOLO: TStringField;
    QCTRCFL_CONTINGENCIA: TStringField;
    QCTRCUFORICALC: TStringField;
    QCTRCCIDORICALC: TStringField;
    QCTRCUFDESCALC: TStringField;
    QCTRCCIDDESCALC: TStringField;
    QCTRCMOTORISTA: TStringField;
    QCTRCCPF_MOT: TStringField;
    QCTRCNM_USUARIO: TStringField;
    QCTRCSEGURADORA: TStringField;
    QCTRCAPOLICE: TStringField;
    QCTRCRESPSEG: TBCDField;
    QCTRCFL_STATUS: TStringField;
    QCTRCPLACA: TStringField;
    QCTRCRENAVAM: TStringField;
    QCTRCLOTACAO: TStringField;
    QCTRCUFVEI: TStringField;
    QCTRCCTE_CHAVE_CTE_R: TStringField;
    QNFNOTFIS: TStringField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFVLRMER: TBCDField;
    QNFDATNOT: TDateTimeField;
    QNFSERIEN: TStringField;
    QNFPESCUB: TBCDField;
    QNFNOTNFE: TStringField;
    QCTRCCTE_PROT: TStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    QCTRCTP_VEI: TStringField;
    DBEdit6: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit7: TDBEdit;
    DBText1: TDBText;
    DBEdit8: TDBEdit;
    DBText2: TDBText;
    DBEdit9: TDBEdit;
    DBText3: TDBText;
    DBEdit10: TDBEdit;
    DBText4: TDBText;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel5: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label37: TLabel;
    Label15: TLabel;
    Label6: TLabel;
    Label40: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label23: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    Label41: TLabel;
    JvDBCalcEdit1: TJvDBCalcEdit;
    JvDBCalcEdit2: TJvDBCalcEdit;
    JvDBCalcEdit3: TJvDBCalcEdit;
    JvDBCalcEdit4: TJvDBCalcEdit;
    JvDBCalcEdit5: TJvDBCalcEdit;
    JvDBCalcEdit6: TJvDBCalcEdit;
    JvDBCalcEdit7: TJvDBCalcEdit;
    JvDBCalcEdit8: TJvDBCalcEdit;
    JvDBCalcEdit9: TJvDBCalcEdit;
    JvDBCalcEdit10: TJvDBCalcEdit;
    JvDBCalcEdit11: TJvDBCalcEdit;
    JvDBCalcEdit12: TJvDBCalcEdit;
    JvDBCalcEdit13: TJvDBCalcEdit;
    JvDBCalcEdit14: TJvDBCalcEdit;
    JvDBCalcEdit15: TJvDBCalcEdit;
    JvDBCalcEdit16: TJvDBCalcEdit;
    JvDBCalcEdit17: TJvDBCalcEdit;
    JvDBCalcEdit18: TJvDBCalcEdit;
    JvDBCalcEdit19: TJvDBCalcEdit;
    JvDBCalcEdit20: TJvDBCalcEdit;
    JvDBCalcEdit21: TJvDBCalcEdit;
    JvDBCalcEdit22: TJvDBCalcEdit;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBEdit13: TDBEdit;
    JvDBCalcEdit23: TJvDBCalcEdit;
    DBEdit14: TDBEdit;
    Label42: TLabel;
    QOCS: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    BCDField3: TBCDField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    BCDField4: TBCDField;
    DateTimeField1: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    BCDField5: TBCDField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    StringField39: TStringField;
    StringField40: TStringField;
    StringField41: TStringField;
    StringField42: TStringField;
    StringField43: TStringField;
    StringField44: TStringField;
    StringField45: TStringField;
    BCDField6: TBCDField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    BCDField7: TBCDField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    StringField66: TStringField;
    StringField67: TStringField;
    StringField68: TStringField;
    StringField69: TStringField;
    StringField70: TStringField;
    StringField71: TStringField;
    StringField72: TStringField;
    StringField73: TStringField;
    BCDField8: TBCDField;
    StringField74: TStringField;
    StringField75: TStringField;
    StringField76: TStringField;
    StringField77: TStringField;
    StringField78: TStringField;
    StringField79: TStringField;
    StringField80: TStringField;
    StringField81: TStringField;
    StringField82: TStringField;
    StringField83: TStringField;
    StringField84: TStringField;
    StringField85: TStringField;
    StringField86: TStringField;
    StringField87: TStringField;
    BCDField9: TBCDField;
    StringField88: TStringField;
    StringField89: TStringField;
    StringField90: TStringField;
    StringField91: TStringField;
    StringField92: TStringField;
    StringField93: TStringField;
    StringField94: TStringField;
    StringField95: TStringField;
    StringField96: TStringField;
    StringField97: TStringField;
    StringField98: TStringField;
    StringField99: TStringField;
    StringField100: TStringField;
    BCDField10: TBCDField;
    StringField101: TStringField;
    StringField102: TStringField;
    StringField103: TStringField;
    StringField104: TStringField;
    StringField105: TStringField;
    StringField106: TStringField;
    StringField107: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    StringField108: TStringField;
    FloatField3: TFloatField;
    StringField109: TStringField;
    FloatField4: TFloatField;
    StringField110: TStringField;
    FloatField5: TFloatField;
    StringField111: TStringField;
    FloatField6: TFloatField;
    StringField112: TStringField;
    FloatField7: TFloatField;
    StringField113: TStringField;
    FloatField8: TFloatField;
    StringField114: TStringField;
    FloatField9: TFloatField;
    StringField115: TStringField;
    FloatField10: TFloatField;
    StringField116: TStringField;
    FloatField11: TFloatField;
    StringField117: TStringField;
    FloatField12: TFloatField;
    StringField118: TStringField;
    FloatField13: TFloatField;
    StringField119: TStringField;
    FloatField14: TFloatField;
    StringField120: TStringField;
    FloatField15: TFloatField;
    StringField121: TStringField;
    FloatField16: TFloatField;
    StringField122: TStringField;
    FloatField17: TFloatField;
    StringField123: TStringField;
    FloatField18: TFloatField;
    StringField124: TStringField;
    FloatField19: TFloatField;
    StringField125: TStringField;
    FloatField20: TFloatField;
    StringField126: TStringField;
    FloatField21: TFloatField;
    StringField127: TStringField;
    FloatField22: TFloatField;
    StringField128: TStringField;
    StringField129: TStringField;
    FloatField23: TFloatField;
    BCDField11: TBCDField;
    BCDField12: TBCDField;
    FloatField24: TFloatField;
    StringField130: TStringField;
    StringField131: TStringField;
    StringField132: TStringField;
    FloatField25: TFloatField;
    StringField133: TStringField;
    DateTimeField2: TDateTimeField;
    StringField134: TStringField;
    FloatField26: TFloatField;
    FloatField27: TFloatField;
    BCDField13: TBCDField;
    StringField135: TStringField;
    StringField136: TStringField;
    StringField137: TStringField;
    StringField138: TStringField;
    StringField139: TStringField;
    StringField140: TStringField;
    StringField141: TStringField;
    StringField142: TStringField;
    StringField143: TStringField;
    StringField144: TStringField;
    StringField145: TStringField;
    StringField146: TStringField;
    StringField147: TStringField;
    StringField148: TStringField;
    StringField149: TStringField;
    StringField150: TStringField;
    BCDField14: TBCDField;
    StringField151: TStringField;
    StringField152: TStringField;
    StringField153: TStringField;
    StringField154: TStringField;
    StringField155: TStringField;
    StringField156: TStringField;
    StringField157: TStringField;
    StringField158: TStringField;
    Label43: TLabel;
    JvDBCalcEdit24: TJvDBCalcEdit;
    QCTRCCOMPFRETE_PESO: TStringField;
    QCTRCCOMPFRETE_PESOVALOR: TFloatField;
    QCTRCCOMPTXCTE: TStringField;
    QCTRCCOMPTXCTEVALOR: TFloatField;
    QCTRCCOMPVL_FRETE: TStringField;
    QCTRCCOMPVL_FRETEVALOR: TFloatField;
    Label44: TLabel;
    QCTRCNR_CONHECIMENTO_COMP: TBCDField;
    QPedido: TADOQuery;
    QPedidoORDCOM: TStringField;
    Label45: TLabel;
    DBEdit15: TDBEdit;
    dtsPedido: TDataSource;
    QCTRCNR_MANIFESTO: TBCDField;
    DBEdit16: TDBEdit;
    Label46: TLabel;
    DBEdit17: TDBEdit;
    QCTRCNR_TABELA: TBCDField;
    Label47: TLabel;
    DBEdit18: TDBEdit;
    QCTRCCTE_DATA: TDateTimeField;
    QCTRCtempo: TFloatField;
    Label48: TLabel;
    DBEdit19: TDBEdit;
    Label49: TLabel;
    DBEdit20: TDBEdit;
    Label50: TLabel;
    DBEdit21: TDBEdit;
    QCTRCcte_data_can: TDateField;
    QCTRCds_cancelamento: TStringField;
    QCTRCusuario_canc: TStringField;
    QCTRCfl_tipo_2: TStringField;
    Label51: TLabel;
    DBEdit22: TDBEdit;
    QCTRCoperacao: TStringField;
    Label70: TLabel;
    JvDBCalcEdit25: TJvDBCalcEdit;
    QCTRCcompvl_aereo: TStringField;
    QCTRCcompvl_aereovalor: TBCDField;
    Label52: TLabel;
    DBEdit23: TDBEdit;
    QCTRCkm_aereo: TBCDField;
    Label53: TLabel;
    JvDBCalcEdit26: TJvDBCalcEdit;
    QCTRCcompvl_tda: TStringField;
    QCTRCcompvl_tdavalor: TBCDField;
    //QCTRCDS_CANCELAMENTO: TStringField;
    //QCTRCUSUARIO_CANC: TStringField;
    //QCTRCCTE_DATA_CAN: TDateTimeField;
    procedure carregadados;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnDacteClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure QCTRCAfterOpen(DataSet: TDataSet);

  private
    { Private declarations }
     bInutiliza: Boolean;

  public
    { Public declarations }
  end;

var
  frmCT_e_Gerados: TfrmCT_e_Gerados;


implementation

uses Dados, funcoes, Menu, RelCT_Gerado, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmCT_e_Gerados.btnCancelarClick(Sender: TObject);
var iTmp_Cancelar :Integer;
    qrTempo :TADOQuery;
    year, day, month, hora, min, sec, mili, yearN, dayN, monthN, horaN, minN, secN, miliN:word;
    TotMinuto, udm:integer;
    dtUltima, ontem, h24, tempo: TDateTime;     //, m24, s24
    fiscal : String;
begin
  if not dtmDados.PodeApagar('frmGeraCTe') then
    Exit;

  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.sql.add('select nvl(d.departamento,'''') fiscal ');
  dtmdados.IQuery1.sql.add('from tb_usuarios c left join tb_departamento d on c.cod_dpto = d.codigo ');
  dtmdados.IQuery1.sql.add('where c.cod_usuario = :0');
  dtmdados.IQuery1.Parameters[0].value := IntToStr(glbcoduser);
  dtmdados.IQuery1.Open;
  fiscal :=  dtmdados.IQuery1.FieldByName('fiscal').value;
  dtmdados.IQuery1.Close;

  if ((QCTRCCFOP.Value = '5932') or (QCTRCCFOP.Value = '6932')) and (fiscal <> 'FISCAL') then
  begin
    ShowMessage('CT-e de GNRE somente o Departamento Fiscal pode cancelar ! ');
    exit;
  end;

  //iTmp_Cancelar := 0;
  qrTempo := TADOQuery.Create(Self);
  qrTempo.Connection := dtmDados.ConIntecom;
  qrTempo.Sql.Add('select tmp_canc_cte from tb_empresa where codfil = ' + IntToStr(GLBFilial));
  qrTempo.Open;
  iTmp_Cancelar := qrTempo.FieldByName('tmp_canc_cte').AsInteger;
  qrTempo.Close;
  FreeAndNil(qrTempo);

  if (GlbCteAmb = 'P') and (fiscal <> 'FISCAL') then
  begin
   { dtUltima := QCTRCEMISSAOCTE.AsDateTime;
    DecodeDate(dtUltima, year, month, day);
    DecodeTime(dtUltima, hora, min, sec, mili);
    DecodeDate(Now, yearN, monthN, dayN);
    DecodeTime(Now, horaN, minN, secN, miliN);

    ontem := incday(dayN, -1);
    h24 := horaN - hora;
    //m24 := minN - min;
    //s24 := secN - sec;
    TotMinuto := 9999;
    tempo := now()-QCTRCEMISSAOCTE.AsDateTime;
    udm := DaysInMonth(QCTRCEMISSAOCTE.AsDateTime);

    // Abaixo se o dia for o primeiro dia do m�s
    if (day = udm) and (dayN = 1) then
    begin
      if (year = yearN) and (month = (monthN-1)) and (day >= udm) then
      begin
        if (h24 < 0)then
          TotMinuto := 1439
        else if (h24 = 0) and (minN <= min) then
          TotMinuto := 1439;
      end;
    end
    else
    if (year = yearN) and (month = monthN) and (day = dayN)then
    begin
      TotMinuto := 1439;
    end
    else
    begin
    if (year = yearN) and (month = monthN) and (day >= ontem)then
    begin
      if (h24 < 0)then
        TotMinuto := 1439
      else if (h24 = 0) and (minN <= min) then
        TotMinuto := 1439;
      end;
    end;  }

    if (QCTRCtempo.Value > (iTmp_Cancelar)) then
    begin
      ShowMessage(' Tempo para cancelar este CT-e excedeu o tempo Limite, Solicite ao Departamento Fiscal ! ');
      exit;
    end;
  end;

  if edFatura.Text <> '' then
  begin
    ShowMessage('Este CT-e J� Est� em uma Fatura, para Cancelar precisa cancelar a Fatura Primeiro ');
    exit;
  end;

  // verifica se este est� lan�ado em alguma generalidade
  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.sql.add('select count(id_generalidade) id_generalidade ');
  dtmdados.IQuery1.sql.add('from crm_generalidades where cte = :0 and filial = :1 and id_generalidade > 0');
  dtmdados.IQuery1.Parameters[0].value := QCTRCNUMEROCTE.AsInteger;
  dtmdados.IQuery1.Parameters[1].value := QCTRCFL_EMPRESA.AsInteger;
  dtmdados.IQuery1.Open;
  if dtmdados.IQuery1.FieldByName('id_generalidade').value > 0 then
  begin
    ShowMessage('Este CT-e Est� lan�ado em na Generalidade ' +
      dtmdados.IQuery1.FieldByName('id_generalidade').value +
      ' , para Cancelar precisa cancelar a Generalidade Primeiro ');
    dtmdados.IQuery1.Close;
    exit;
  end;
  dtmdados.IQuery1.Close;
  bInutiliza := (
                 //(dtmDados.PodeVisualizar('frmInutilizaCte')) and
                // (Glb_CTe_Prod = 'S') and
                 (QCTRCcte_PROT.value = '') and
                 (QCTRCNR_SERIE.AsString = '1')
                );

  if (bInutiliza) and (fiscal <> 'FISCAL') then
  begin
    ShowMessage('CT-e de Inutiliza��o somente o Departamento Fiscal pode Executar ! ');
    exit;
  end;
               
  //
   if (bInutiliza) and (fiscal <> 'FISCAL') then
  begin
    ShowMessage('CT-e de Inutiliza��o somente o Departamento Fiscal pode Executar ! ');
    exit;
  end;
  if (not bInutiliza) then
  begin
    if ((GlbCTeAmb = 'P') and ( (QCTRCcte_PROT.value = '') or (QCTRCFL_CONTINGENCIA.AsString ='S'))) then
    begin
      ShowMessage(' S� � permitido cancelamento de CT-e(s) que j� foram impressos a DACTE ou est�o em Conting�ncia ! ');
      exit;
    end;
  end;
  //
  if (not bInutiliza) then
  begin

    // verifica se este est� lan�ado em alguma manifesto em aberto
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.sql.add('select count(*) tem ');
    dtmdados.IQuery1.sql.add('from tb_manitem i left join tb_manifesto m on i.manifesto = m.manifesto and i.serie = m.serie and i.filial = m.filial');
    dtmdados.IQuery1.sql.add('where i.coddoc = :0 and i.fildoc = :1 and m.status = ''S'' and i.serdoc = ''1'' ');
    dtmdados.IQuery1.Parameters[0].value := QCTRCNUMEROCTE.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := QCTRCFL_EMPRESA.AsInteger;
    dtmdados.IQuery1.Open;
    if dtmdados.IQuery1.FieldByName('tem').value > 0 then
    begin
      ShowMessage('Este CT-e J� Est� Manifestado, Cancele o manifesto primeiro. ');
      dtmdados.IQuery1.Close;
      exit;
    end;
    dtmdados.IQuery1.Close;

    // verifica se este est� lan�ado em alguma romaneio de carga
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.sql.add('select nvl(nr_romaneio,0) tem from tb_conhecimento where nr_conhecimento = :0 and fl_empresa = :1 ');
    dtmdados.IQuery1.Parameters[0].value := QCTRCNUMEROCTE.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := QCTRCFL_EMPRESA.AsInteger;
    dtmdados.IQuery1.Open;
    if dtmdados.IQuery1.FieldByName('tem').value > 0 then
    begin
      ShowMessage('Este CT-e Est� em Mapa de Separa��o ' + dtmdados.IQuery1.FieldByName('tem').AsString + ', Cancele o mapa primeiro. ');
      dtmdados.IQuery1.Close;
      exit;
    end;
    dtmdados.IQuery1.Close;

    if not dtmDados.PodeApagar('frmGeraCTe') then
      Exit;
    if Application.Messagebox('Voc� Deseja Cancelar Este CT-e ?',
      'Cancelar CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      Panel4.Visible := true;
      Panel4.SetFocus;
      edObs.SetFocus;
    end;
  end
  else
  begin
    if QCtrcnr_MANIFESTO.value > 0 then
    begin
      dtmdados.wQuery1.Close;
      dtmdados.wQuery1.SQL.clear;
      dtmdados.wQuery1.sql.add('select * from tb_portaria where motivo = ''S'' and manifesto = :0');
      dtmdados.wQuery1.Parameters[0].value := QCtrcnr_Manifesto.AsInteger;
      dtmdados.wQuery1.Open;
      if dtmdados.Query1.RecordCount > 0 then
      begin
        ShowMessage('Este CT-e J� passou pela Portaria, n�o � poss�vel o cancelamento !!');
        exit;
      end;
      if not dtmDados.PodeApagar('frmGeraCTe') then
        Exit;
      if (bInutiliza) then
      begin
        if Application.Messagebox('Voc� Deseja Inutilizar Este CT-e ?',
          'Inutilizar CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
          Panel4.Visible := true;
          Panel4.SetFocus;
          edObs.SetFocus;
        end;
      end
      else
      begin
        if Application.Messagebox('Voc� Deseja Cancelar Este CT-e ?',
          'Cancelar CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
           Panel4.Visible := true;
           Panel4.SetFocus;
           edObs.SetFocus;
        end;
      end;
    end
    else
    begin
      if (bInutiliza) then
      begin
        if Application.Messagebox('Voc� Deseja Inutilizar Este CT-e ?',
          'Inutilizar CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
          Panel4.Visible := true;
          Panel4.SetFocus;
          edObs.SetFocus;
        end;
      end
      else
      begin
        if Application.Messagebox('Voc� Deseja Cancelar Este CT-e ?',
          'Cancelar CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
           Panel4.Visible := true;
           Panel4.SetFocus;
           edObs.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TfrmCT_e_Gerados.btnDacteClick(Sender: TObject);
var sData, sCaminho, sMDFe : string;
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select pdf from tb_parametro_sim');
  dtmdados.IQuery1.open;
  if (lbStatus.Caption = 'Autorizado pelo SEFAZ') then
  begin
    sData := FormatDateTime('YYYY', QCTRCEMISSAOCTE.value) +
             FormatDateTime('MM', QCTRCEMISSAOCTE.value);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + '\' + sData + '\CTe\';
    sMDFe := QCTRCCTE_CHAVE.AsString + '-cte.xml';
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar  := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString));

    frmMenu.QCtexml.close;
    frmMenu.QCtexml.SQL[1] := 'Where cod_conhecimento = '+''+ QCTRCCOD_CONHECIMENTO.asString+'';
    frmMenu.QCtexml.open;
    if not frmMenu.Qctexml.eof then
    begin
     // stStreamNF := TStringStream.Create(frmMenu.QCteXmlxml.value);
      frmMenu.ACBrCTe1.Conhecimentos.Clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromString(frmMenu.QCteXmlxml.AsString);
      try
        frmMenu.qrCteEletronico.Close;
        frmMenu.qrCteEletronico.Parameters[0].Value := QCTRCCOD_CONHECIMENTO.asString;
        frmMenu.qrCteEletronico.open;

        frmMenu.QNF.close;
        frmMenu.QNF.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
        frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.Value;
        frmMenu.QNF.Parameters[2].value := frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
        frmMenu.QNF.Open;
        frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
        //Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
        //frmDacte_Cte_2 := TfrmDacte_Cte_2.CreateMonitor(Self, frmMenu.qrCteEletronicoNUMEROCTE.AsInteger, frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger);
        //frmDacte_Cte_2.sChaveCte := frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
        //frmDacte_Cte_2.rlDacte.Preview();
      finally
        //frmDacte_Cte_2.Free;
      end;
    end
    else
    begin
      //arquivo := glbXML_cte + qrMonitorCHAVE_CTE.AsString + '-cte.xml';
      frmMenu.ACBrCTe1.Conhecimentos.Clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + sMDFe);
     // frmMenu.ACBrCTe1.Conhecimentos.ImprimirPDF;
      try
        frmMenu.qrCteEletronico.Close;
        frmMenu.qrCteEletronico.Parameters[0].Value := QCTRCCOD_CONHECIMENTO.asString;
        frmMenu.qrCteEletronico.open;

        frmMenu.QNF.close;
        frmMenu.QNF.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
        frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.Value;
        frmMenu.QNF.Parameters[2].value := frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
        frmMenu.QNF.Open;
        frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
      finally
        //frmDacte_Cte_2.Free;
      end;
    end;

    //frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
  end;
  if (QCTRC.Eof) then
  begin
    ShowMessage(' N�o h� CTRC para imprimir Dacte ! ');
    exit;
  end;
end;

procedure TfrmCT_e_Gerados.btnFecharClick(Sender: TObject);
//var qrBuscaChave: TADOQuery;
    //sChave :String;
begin
  if Length(alltrim(edObs.text)) < 15 then
  begin
    ShowMessage('Campo Motivo deve possuir pelo menos 15 caracteres para o cancelamento/inutiliza��o !');
    EdObs.SetFocus;
  end
  else
  begin
    // Cancelar
    if (not bInutiliza) then
    begin
      // nao existe protocolo nao cancela
      if (alltrim(QCTRCCTE_CHAVE.asstring) <> '') then
      begin
        frmMenu.sJustificativa := TrocaCaracterEspecial(EdObs.Text, true);
        frmMenu.CancelaCTe(QCTRCCOD_CONHECIMENTO.AsInteger);
      end;
    end
    else
    begin
      frmMenu.sJustificativa :=  TrocaCaracterEspecial(EdObs.Text, true);
      frmMenu.InutilizaCTe(QCTRCCOD_CONHECIMENTO.AsInteger);
    end;

    // se for reentrega exclui a tb_coleta
    // pois se for emitido outro ser� gerada outra linha na tb_coleta
    if Qctrcfl_tipo_2.AsString = 'R' then
    begin
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.sql.add('delete from cyber.tb_coleta ');
      dtmdados.iQuery1.sql.add('where doc = :0 and filial = :1 ');
      dtmdados.iQuery1.Parameters[0].Value := QCTRCNUMEROCTE.AsInteger;
      dtmdados.iQuery1.Parameters[1].Value := QCTRCFL_EMPRESA.AsInteger;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end
    else
    begin
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.sql.add('update cyber.tb_coleta  ');
      dtmdados.iQuery1.sql.add('   set doc = null ');
      dtmdados.iQuery1.sql.add(' where doc = :0 and filial = :1 ');
      dtmdados.iQuery1.Parameters[0].Value := QCTRCNUMEROCTE.AsInteger;
      dtmdados.iQuery1.Parameters[1].Value := QCTRCFL_EMPRESA.AsInteger;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end;

    if (not bInutiliza) then
    begin
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.sql.add('update cyber.fisacs ');
      dtmdados.iQuery1.sql.add('set vlrcon = 0, bascal = 0, aliquo = 0, vlrimp = 0, vlrise = 0, ');
      dtmdados.iQuery1.sql.add('vlrout = 0, observ = ''CTE CANCELADO'', datatu = sysdate, usuatu = :0 ');
      dtmdados.iQuery1.sql.add(' where numdoc = :1 and codfil = :2 and especi = ''CTRC'' ');
      dtmdados.iQuery1.Parameters[0].Value := GLBUSER;
      dtmdados.iQuery1.Parameters[1].Value := QCTRCNUMEROCTE.AsInteger;
      dtmdados.iQuery1.Parameters[2].Value := QCTRCFL_EMPRESA.AsInteger;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end
    else
    begin
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.sql.add('update cyber.fisacs ');
      dtmdados.iQuery1.sql.add('set vlrcon = 0, bascal = 0, aliquo = 0, vlrimp = 0, vlrise = 0, ');
      dtmdados.iQuery1.sql.add('vlrout = 0, observ = ''CTE INUTILIZADO'', datatu = sysdate, usuatu = :0 ');
      dtmdados.iQuery1.sql.add(' where numdoc = :1 and codfil = :2 and especi = ''CTRC'' ');
      dtmdados.iQuery1.Parameters[0].Value := GLBUSER;
      dtmdados.iQuery1.Parameters[1].Value := QCTRCNUMEROCTE.AsInteger;
      dtmdados.iQuery1.Parameters[2].Value := QCTRCFL_EMPRESA.AsInteger;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end;

    panel4.Visible := false;
    if bInutiliza then
      label18.Caption := 'INUTILIZADO'
    else
      label18.Caption := 'CANCELADO';
    label18.Font.Color := clRed;
    Application.ProcessMessages;
  end;
end;


procedure TfrmCT_e_Gerados.carregadados;
begin
  if QCTRCfl_status.value = 'A' then
  begin
    label18.Caption := 'ATIVO';
    label18.Font.Color := clGreen;
  end
  else
  begin
    if QCTRCfl_status.value = 'I' then
      label18.Caption := 'INUTILIZADO'
    else
      label18.Caption := 'CANCELADO';
    label18.Font.Color := clRed;
  end;

  //
  if (QCTRCcte_PROT.value <> '') and (not QCTRCcte_PROT.isnull) then
  begin
    lbStatus.Caption := 'Autorizado pelo SEFAZ';
    btnDacte.Visible := True;
    btnCancelar.Caption := 'Cancelar';
  end
  else
    btnCancelar.Caption := 'Inutilizar';

  //
  if (lbStatus.Caption = 'Autorizado pelo SEFAZ') then exit;
  // Situa��o SEFAZ

  if (Qctrccte_chave.IsNull) then
  begin
    lbStatus.Caption := 'SEFAZ: N�o Enviado';
    btnDacte.Visible := false;
    exit;
  end;
  btnDacte.Visible := True;
end;

procedure TfrmCT_e_Gerados.FormActivate(Sender: TObject);
begin
  self.caption := 'Conhecimento de Transportes - Emitido';
end;

procedure TfrmCT_e_Gerados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self.tag := 0;
end;

procedure TfrmCT_e_Gerados.FormCreate(Sender: TObject);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select ambiente_cte, pdf_cte from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := GLBFilial;
  dtmdados.IQuery1.open;
  GlbCTeAmb := dtmdados.IQuery1.FieldByName('ambiente_cte').value;
  //frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));
  //frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar  := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));


  QCTRC.close;
  QCTRC.Parameters[0].Value := frmMenu.tag;
  QCTRC.Open;

  QNF.close;
  QNF.Parameters[0].value := QCTRCNUMEROCTE.AsInteger;
  QNF.Parameters[1].value := QCTRCNR_SERIE.AsString;
  QNF.Parameters[2].value := QCTRCFL_EMPRESA.AsInteger;
  QNF.open;
  dtmdados.Query1.close;
  carregadados;
end;

procedure TfrmCT_e_Gerados.QCTRCAfterOpen(DataSet: TDataSet);
begin
  if QCTRCCOMPFRETE_PESOVALOR.value > 0 then
    JvDBCalcEdit1.Enabled := false
  else
    JvDBCalcEdit1.Enabled := true;
  if QCtrcCOMPPEDAGIOVALOR.value > 0 then
    JvDBCalcEdit2.Enabled := false
  else
    JvDBCalcEdit2.Enabled := true;
  if QCTRCCOMPVL_FRETEVALOR.value > 0 then
    JvDBCalcEdit3.Enabled := false
  else
    JvDBCalcEdit3.Enabled := true;
  if QCtrcCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit4.Enabled := false
  else
    JvDBCalcEdit4.Enabled := true;
  if QCtrcCOMPEXEDVALOR.value > 0 then
    JvDBCalcEdit5.Enabled := false
  else
    JvDBCalcEdit5.Enabled := true;
  if QctrcCOMPTRVALOR.value > 0 then
    JvDBCalcEdit6.Enabled := false
  else
    JvDBCalcEdit6.Enabled := true;
  if QCtrcCOMPDESPACHOVALOR.value > 0 then
    JvDBCalcEdit7.Enabled := false
  else
    JvDBCalcEdit7.Enabled := true;
  if QCtrcCOMPTDEVALOR.value > 0 then
    JvDBCalcEdit8.Enabled := false
  else
    JvDBCalcEdit8.Enabled := true;
  if QctrcCOMPGRISVALOR.value > 0 then
    JvDBCalcEdit9.Enabled := false
  else
    JvDBCalcEdit9.Enabled := true;
  if QCtrcCOMPBALSAVALOR.value > 0 then
    JvDBCalcEdit10.Enabled := false
  else
    JvDBCalcEdit10.Enabled := true;
  if QCtrcCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit11.Enabled := false
  else
    JvDBCalcEdit11.Enabled := true;
  if QctrcCOMPAGENDVALOR.value > 0 then
    JvDBCalcEdit12.Enabled := false
  else
    JvDBCalcEdit12.Enabled := true;
  if QCtrcCOMPPALLETVALOR.value > 0 then
    JvDBCalcEdit13.Enabled := false
  else
    JvDBCalcEdit13.Enabled := true;
  if QCtrcCOMPPORTOVALOR.value > 0 then
    JvDBCalcEdit14.Enabled := false
  else
    JvDBCalcEdit14.Enabled := true;
  if QctrcCOMPALFAVALOR.value > 0 then
    JvDBCalcEdit15.Enabled := false
  else
    JvDBCalcEdit15.Enabled := true;
  if QCtrcCOMPCANHOTOVALOR.value > 0 then
    JvDBCalcEdit16.Enabled := false
  else
    JvDBCalcEdit16.Enabled := true;
  if QCtrcCOMPTASVALOR.value > 0 then
    JvDBCalcEdit17.Enabled := false
  else
    JvDBCalcEdit17.Enabled := true;
  if QctrcCOMPADVALORENVALOR.value > 0 then
    JvDBCalcEdit18.Enabled := false
  else
    JvDBCalcEdit18.Enabled := true;

  if QCtrcBASEIMPOSTO.value > 0 then
    JvDBCalcEdit19.Enabled := false
  else
    JvDBCalcEdit19.Enabled := true;
  if QCtrcALIQIMPOSTO.value > 0 then
    JvDBCalcEdit20.Enabled := false
  else
    JvDBCalcEdit20.Enabled := true;
  if QctrcVALORIMMPOSTO.value > 0 then
    JvDBCalcEdit21.Enabled := false
  else
    JvDBCalcEdit21.Enabled := true;
  if QCtrcVLTOTPRESTACAO.value > 0 then
    JvDBCalcEdit22.Enabled := false
  else
    JvDBCalcEdit22.Enabled := true;
  if QCTRCCOMPTXCTEVALOR.value > 0 then
    JvDBCalcEdit24.Enabled := false
  else
    JvDBCalcEdit24.Enabled := true;
  if QCTRCCOMPOUTROSTAXASVALOR.value > 0 then
    JvDBCalcEdit3.Enabled := false
  else
    JvDBCalcEdit3.Enabled := true;
  if QCTRCcompvl_aereovalor.value > 0 then
    JvDBCalcEdit25.Enabled := false
  else
    JvDBCalcEdit25.Enabled := true;
  dtmdados.RQuery1.Close;
  dtmdados.RQuery1.SQL.clear;
  dtmdados.RQuery1.sql.add('select numdup from cyber.rodcon where codcon = :0 and codfil = :1');
  dtmdados.RQuery1.Parameters[0].value := QCTRCNUMEROCTE.Asinteger;
  dtmdados.RQuery1.Parameters[1].value := QCTRCFL_EMPRESA.AsInteger;
  dtmdados.rQuery1.Open;
  if not dtmdados.rQuery1.Eof then
    edFatura.Text := dtmdados.rQuery1.FieldByName('numdup').AsString;
  dtmdados.RQuery1.Close;

  if QCTRCNR_CONHECIMENTO_COMP.value > 0 then
    Label44.caption := 'Complementar do CT-e : ' + QCTRCNR_CONHECIMENTO_COMP.AsString
  else
    Label44.caption := '';

  QPedido.Close;
  QPedido.Parameters[0].value := QCTRCNUMEROCTE.AsInteger;
  QPedido.Parameters[1].value := QCTRCFL_EMPRESA.AsInteger;
  QPedido.open;
end;

end.

