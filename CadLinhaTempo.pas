unit CadLinhaTempo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, ADODB, DB, ActnList,
  JvToolEdit, JvBaseEdits, JvExStdCtrls, JvCombobox, Mask, JvExMask, JvMaskEdit,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, System.Actions, JvDBLookup;

type
  TfrmCadLinhaTempo = class(TForm)
    dtsPercurso: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QPercurso: TADOQuery;
    btnDupl: TBitBtn;
    Panel2: TPanel;
    PnP: TPanel;
    UFO: TJvComboBox;
    UFD: TJvComboBox;
    dbgContrato: TJvDBUltimGrid;
    Panel1: TPanel;
    DBGrid1: TJvDBUltimGrid;
    pnpi: TPanel;
    edHub: TJvCalcEdit;
    Panel4: TPanel;
    Panel5: TPanel;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QTransp: TADOQuery;
    dtsQTransp: TDataSource;
    QTranspID_LINHA_TEMPO_T: TFMTBCDField;
    QTranspCOD_TRANSP: TFMTBCDField;
    QTranspFL_ATIVO: TStringField;
    QTranspRAZSOC: TStringField;
    QTransportador: TADOQuery;
    dtsTransp: TDataSource;
    Panel6: TPanel;
    btnInserirTransp: TBitBtn;
    btnSalvarTransp: TBitBtn;
    btnAltTransportador: TBitBtn;
    btnExluirTransportador: TBitBtn;
    btnCancelarTransportador: TBitBtn;
    QPercursoID_LINHA_TEMPO_P: TFMTBCDField;
    QPercursoCODIBGE_O: TFMTBCDField;
    QPercursoCODIBGE_D: TFMTBCDField;
    QPercursoFL_ATIVO: TStringField;
    QPercursoID_LINHA_TEMPO_T: TFMTBCDField;
    QPercursoUF_O: TStringField;
    QPercursoCID_O: TStringField;
    QPercursoUF_D: TStringField;
    QPercursoCID_D: TStringField;
    Panel3: TPanel;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Panel7: TPanel;
    btnInserirI: TBitBtn;
    btnsalvarI: TBitBtn;
    btnAlterarI: TBitBtn;
    btnExcluirI: TBitBtn;
    QHub: TADOQuery;
    dtsHub: TDataSource;
    QHubID_LINHA_TEMPO_H: TFMTBCDField;
    QHubID_LINHA_TEMPO_P: TFMTBCDField;
    QHubHUB: TFMTBCDField;
    QHubDIA: TFMTBCDField;
    QHubFL_ATIVO: TStringField;
    edDias: TJvCalcEdit;
    cbCidadeO: TJvComboBox;
    cbCidadeD: TJvComboBox;
    QCidade: TADOQuery;
    QCidadeCODIGO: TBCDField;
    QCidadeDESCRI: TStringField;
    QTransportadorTRANSP: TStringField;
    QTransportadorRAZSOC: TStringField;
    cbTransp: TJvComboBox;
    lblHub: TLabel;
    lblDias: TLabel;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure btnExcluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure Restaura;
    procedure DetalhesShow(Sender: TObject);
    procedure RestauraI;
    procedure RestauraT;
    procedure btnInserirIClick(Sender: TObject);
    procedure btnsalvarIClick(Sender: TObject);
    procedure btnAlterarIClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure QPercursoAfterScroll(DataSet: TDataSet);
    procedure btnExcluirIClick(Sender: TObject);
    procedure btnCancelarIClick(Sender: TObject);
    procedure btnInserirTranspClick(Sender: TObject);
    procedure btnAltTransportadorClick(Sender: TObject);
    procedure btnExluirTransportadorClick(Sender: TObject);
    procedure btnCancelarTransportadorClick(Sender: TObject);
    procedure QTranspAfterScroll(DataSet: TDataSet);
    procedure UFOExit(Sender: TObject);
    procedure UFDExit(Sender: TObject);
    procedure btnSalvarTranspClick(Sender: TObject);
    procedure QHubAfterOpen(DataSet: TDataSet);
  private
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadLinhaTempo: TfrmCadLinhaTempo;

implementation

uses Dados, funcoes, menu;

{$R *.DFM}

procedure TfrmCadLinhaTempo.btnNovoClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  UFO.SetFocus;
end;

procedure TfrmCadLinhaTempo.btnSalvarClick(Sender: TObject);
var
  col, codo, codd: integer;
begin
  col := QPercursoID_LINHA_TEMPO_P.AsInteger;

  if UFO.Text = '' then
  begin
    ShowMessage('A UF de origem n�o foi cadastrado !');
    UFO.SetFocus;
    Exit;
  end;

  if cbcidadeO.Text = '' then
  begin
    ShowMessage('A Cidade de origem n�o foi cadastrado !');
    cbcidadeO.SetFocus;
    Exit;
  end;

  if UFD.Text = '' then
  begin
    ShowMessage('A UF de destino n�o foi cadastrado !');
    UFD.SetFocus;
    Exit;
  end;

  if cbcidadeD.Text = '' then
  begin
    ShowMessage('A Cidade de destino n�o foi cadastrado !');
    cbcidadeD.SetFocus;
    Exit;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select count(*) tem ');
  dtmDados.iQuery1.SQL.add('from tb_linha_tempo_p p left join tb_linha_tempo_t t on t.id_linha_tempo_t = p.id_linha_tempo_t ');
  dtmDados.iQuery1.SQL.add('left join tb_cidade co on co.cdibge = p.codibge_o ');
  dtmDados.iQuery1.SQL.add('left join tb_cidade cd on cd.cdibge = p.codibge_d ');
  dtmDados.iQuery1.SQL.add('where t.cod_transp = :0 ');
  dtmDados.iQuery1.SQL.add('and co.estado = :1 and co.descri = :2 and cd.estado = :3 and cd.descri = :4  ');
  dtmDados.iQuery1.Parameters[0].Value := QTranspCOD_TRANSP.AsInteger;
  dtmDados.iQuery1.Parameters[1].Value := UFO.Text;
  dtmDados.iQuery1.Parameters[2].Value := cbCidadeO.Text;
  dtmDados.iQuery1.Parameters[3].Value := UFD.Text;
  dtmDados.iQuery1.Parameters[4].Value := cbCidadeD.Text;
  dtmDados.iQuery1.Open;
  if dtmDados.iQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage('Este Percurso j� est� cadastrado para este Transportador!!');
    btnCancelarClick(Sender);
    Exit;
  end;
  dtmDados.iQuery1.Close;
  //pegar c�digo ibge origem
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select cdibge from tb_cidade where estado = :0 and descri = :1 ');
  dtmDados.iQuery1.Parameters[0].Value := UFO.Text;
  dtmDados.iQuery1.Parameters[1].Value := cbCidadeO.Text;
  dtmDados.iQuery1.Open;
  codo := dtmDados.iQuery1.FieldByName('cdibge').Value;
  //pegar c�digo ibge destino
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select cdibge from tb_cidade where estado = :0 and descri = :1 ');
  dtmDados.iQuery1.Parameters[0].Value := UFD.Text;
  dtmDados.iQuery1.Parameters[1].Value := cbCidadeD.Text;
  dtmDados.iQuery1.Open;
  codd := dtmDados.iQuery1.FieldByName('cdibge').Value;
  dtmDados.iQuery1.Close;

  try
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add
        ('insert into tb_linha_tempo_p (id_linha_tempo_p, codibge_o, codibge_d, id_linha_tempo_t, user_inc) ');
      dtmDados.iQuery1.SQL.add
        ('values ((select max(id_linha_tempo_p)+1 from tb_linha_tempo_p), :0, :1, :2, :3 )');
      dtmDados.iQuery1.Parameters[0].Value := codo;
      dtmDados.iQuery1.Parameters[1].Value := codd;
      dtmDados.iQuery1.Parameters[2].Value := QTranspID_LINHA_TEMPO_T.AsInteger;
      dtmDados.iQuery1.Parameters[3].Value := GLBUSER;
      dtmDados.iQuery1.ExecSQL;

      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add('select max(id_linha_tempo_p) id from tb_linha_tempo_p');
      dtmDados.iQuery1.Open;
      col := dtmDados.iQuery1.FieldByName('id').Value;
    end
    else
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add('update tb_linha_tempo_p set codibge_o = :0, codibge_d = :1, ');
      dtmDados.iQuery1.SQL.add('user_alt = :2, dt_alt = :3 ');
      dtmDados.iQuery1.SQL.add('where id_linha_tempo_p = :4');
      dtmDados.iQuery1.Parameters[0].Value := codo;
      dtmDados.iQuery1.Parameters[1].Value := codd;
      dtmDados.iQuery1.Parameters[2].Value := GLBUSER;
      dtmDados.iQuery1.Parameters[3].Value := date;
      dtmDados.iQuery1.Parameters[4].Value := col;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  tipo := '';
  Restaura;
  QPercurso.Close;
  QPercurso.Open;
  QPercurso.Locate('id_linha_tempo_p', col, []);
end;

procedure TfrmCadLinhaTempo.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  tipo := '';
end;

procedure TfrmCadLinhaTempo.btnCancelarIClick(Sender: TObject);
begin

  RestauraI;
end;

procedure TfrmCadLinhaTempo.btnCancelarTransportadorClick(Sender: TObject);
begin
  RestauraT;
end;

procedure TfrmCadLinhaTempo.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;

  UFO.SetFocus;
  tipo := 'A';

  UFO.ItemIndex := UFO.Items.IndexOf(QPercursoUF_O.Value);
  UFOExit(Sender);
  cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(QPercursoCID_O.Value);
  UFD.ItemIndex := UFD.Items.IndexOf(QPercursoUF_D.Value);
  UFDExit(Sender);
  cbCidadeD.ItemIndex := cbCidadeD.Items.IndexOf(QPercursoCID_D.Value);
end;

procedure TfrmCadLinhaTempo.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QPercurso.Close;
  if Pos('order by', QPercurso.SQL.Text) > 0 then
  begin
    if Column.FieldName = 'CODIGO' then
      QPercurso.SQL.Text := Copy(QPercurso.SQL.Text, 1,
        Pos('order by', QPercurso.SQL.Text) - 1) + 'order by t.' +
        Column.FieldName
    else
      QPercurso.SQL.Text := Copy(QPercurso.SQL.Text, 1,
        Pos('order by', QPercurso.SQL.Text) - 1) + 'order by ' + Column.FieldName
  end
  else
    QPercurso.SQL.Text := QPercurso.SQL.Text + ' order by ' + Column.FieldName;
  QPercurso.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadLinhaTempo.btnExcluirClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Percurso?',
    'Apagar Percurso', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    // apagar hubs
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_h where id_linha_tempo_p := 0 ');
    dtmDados.iQuery1.Parameters[0].Value := QPercursoID_LINHA_TEMPO_P.AsInteger;
    dtmDados.iQuery1.Close;
    //apagar percurso
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_p ');
    dtmDados.iQuery1.SQL.add('where id_linha_tempo_p = :0)');
    dtmDados.iQuery1.Parameters[0].Value := QPercursoID_LINHA_TEMPO_P.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QPercurso.Close;
    QPercurso.Open;
  end;
end;

procedure TfrmCadLinhaTempo.btnExcluirIClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Hub do Percurso?',
    'Apagar Hub do Percurso', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    // apagar hubs
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_h where id_linha_tempo_h := 0 ');
    dtmDados.iQuery1.Parameters[0].Value := QHubID_LINHA_TEMPO_H.AsInteger;
    dtmDados.iQuery1.Close;
    QHub.Close;
    QHub.Open;
  end;
end;

procedure TfrmCadLinhaTempo.btnExluirTransportadorClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Transportador da Linha do Tempo?',
    'Apagar Transportador', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    // apagar hubs
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_h where id_linha_tempo_p in ');
    dtmDados.iQuery1.SQL.add('(select id_linha_tempo_p from tb_linha_tempo_p ');
    dtmDados.iQuery1.SQL.add(' where id_linha_tempo_t in (select id_linha_tempo_t from tb_linha_tempo_t ');
    dtmDados.iQuery1.SQL.add('where cod_transp = :0))');
    dtmDados.iQuery1.Parameters[0].Value := QTranspCOD_TRANSP.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    //apagar percurso
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_p ');
    dtmDados.iQuery1.SQL.add('where id_linha_tempo_t in (select id_linha_tempo_t from tb_linha_tempo_t ');
    dtmDados.iQuery1.SQL.add('where cod_transp = :0)');
    dtmDados.iQuery1.Parameters[0].Value := QTranspCOD_TRANSP.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    //apagar transportador
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_linha_tempo_t ');
    dtmDados.iQuery1.SQL.add('where cod_transp = :0');
    dtmDados.iQuery1.Parameters[0].Value := QTranspCOD_TRANSP.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    showMessage('Transportador Exclu�do');
    QTransp.Close;
    QTransp.Open;
  end;
end;

procedure TfrmCadLinhaTempo.FormCreate(Sender: TObject);
begin
  QTransp.Open;
  QPercurso.Open;
  QHub.Open;
  QTransportador.Open;
end;

procedure TfrmCadLinhaTempo.QHubAfterOpen(DataSet: TDataSet);
var dias : Integer;
begin
  dias := 0;
  while not QHub.eof do
  begin
    dias := QHubDIA.AsInteger + dias;
    QHub.Next;
  end;
  QHub.First;
  lblHub.Caption := IntToStr(QHub.RecordCount) + ' Hubs Neste Percurso';
  lblDias.Caption := 'Total de Dias : ' + IntToStr(dias);
end;

procedure TfrmCadLinhaTempo.QPercursoAfterScroll(DataSet: TDataSet);
begin
  QHub.Close;
  QHub.Parameters[0].Value := QPercursoID_LINHA_TEMPO_P.AsInteger;
  QHub.Open;
end;

procedure TfrmCadLinhaTempo.QTranspAfterScroll(DataSet: TDataSet);
begin
  QPercurso.Close;
  QPercurso.Parameters[0].Value := QTranspID_LINHA_TEMPO_T.AsInteger;
  QPercurso.Open;
end;

procedure TfrmCadLinhaTempo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QPercurso.Close;
  QTransp.Close;
  QHub.Close;
  QTransportador.Close;
end;

procedure TfrmCadLinhaTempo.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QPercurso.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadLinhaTempo.Restaura;
begin
  btnNovo.Enabled := not btnNovo.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnExcluir.Enabled := not btnExcluir.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  if pnp.Visible = true then
    pnp.Visible := false
  else
    pnp.Visible := true;
end;

procedure TfrmCadLinhaTempo.DetalhesShow(Sender: TObject);
begin
  DBGrid1.top := 5;
  DBGrid1.Height := 308;
end;

procedure TfrmCadLinhaTempo.RestauraI;
begin
  btnInserirI.Enabled := not btnInserirI.Enabled;
  btnAlterarI.Enabled := not btnAlterarI.Enabled;
  btnExcluirI.Enabled := not btnExcluirI.Enabled;
  btnsalvarI.Enabled := not btnsalvarI.Enabled;
  if pnpi.Visible = true then
    pnpi.Visible := false
  else
    pnpi.Visible := true;
end;

procedure TfrmCadLinhaTempo.RestauraT;
begin
  btnInserirTransp.Enabled := not btnInserirTransp.Enabled;
  btnAltTransportador.Enabled := not btnAltTransportador.Enabled;
  btnExluirTransportador.Enabled := not btnExluirTransportador.Enabled;
  btnSalvarTransp.Enabled := not btnSalvarTransp.Enabled;
  btnCancelarTransportador.Enabled := not btnCancelarTransportador.Enabled;
  if panel5.Visible = true then
    panel5.Visible := false
  else
    panel5.Visible := true;
end;

procedure TfrmCadLinhaTempo.UFDExit(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := UFD.text;
  QCidade.open;
  cbCidadeD.clear;
  cbCidadeD.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadeD.Items.add(QCidadeDESCRI.value);
    QCidade.next;
  end;
  QCidade.Close;
end;

procedure TfrmCadLinhaTempo.UFOExit(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := UFO.text;
  QCidade.open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.value);
    QCidade.next;
  end;
  QCidade.Close;
end;

procedure TfrmCadLinhaTempo.btnInserirIClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  tipo := 'I';
  RestauraI;
  edHub.SetFocus;
  edHUb.Value := 0;
end;

procedure TfrmCadLinhaTempo.btnInserirTranspClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  tipo := 'I';
  RestauraT;
  QTransportador.close;
  QTransportador.Open;
  cbTransp.Clear;
  cbTransp.Items.Add('');
  while not QTransportador.Eof do
  begin
    cbTransp.Items.Add(QTransportadorTRANSP.AsString);
    QTransportador.Next;
  end;
  cbTransp.SetFocus;
end;

procedure TfrmCadLinhaTempo.btnsalvarIClick(Sender: TObject);
begin
  if edHub.value = 0 then
  begin
    ShowMessage('O Hub n�o foi cadastrado !');
    EdHub.SetFocus;
    Exit;
  end;

  if edDias.Value = 0 then
  begin
    ShowMessage('A Quantidade de Dias neste Hub est� Zero !');
    edDias.SetFocus;
    Exit;
  end;


  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select distinct t.* ');
  dtmDados.iQuery1.SQL.add('from tb_linha_tempo_h t ');
  dtmDados.iQuery1.SQL.add('where t.hub = :0 and id_linha_tempo_p = :1 ');
  dtmDados.iQuery1.SQL.add('and id_linha_tempo_h <> :2 ');
  dtmDados.iQuery1.Parameters[0].Value := edHub.value;
  dtmDados.iQuery1.Parameters[1].Value := QHubID_LINHA_TEMPO_P.AsInteger;
  dtmDados.iQuery1.Parameters[2].Value := QHubID_LINHA_TEMPO_H.AsInteger;
  dtmDados.iQuery1.Open;
  if not dtmDados.iQuery1.FieldByName('id_linha_tempo_h').IsNull then
  begin
    ShowMessage('Este Hub j� est� cadastrado !!');
    btnCancelarIClick(Sender);
    QHub.Locate('id_linha_tempo_h', dtmDados.iQuery1.FieldByName('id_linha_tempo_h').Value, []);
    Exit;
  end;
  dtmDados.iQuery1.Close;
  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.SQL.add
        ('insert into tb_linha_tempo_h (id_linha_tempo_h, hub, dia, id_linha_tempo_p, user_inc)' + ' values ' +
        '((select max(id_linha_tempo_h)+1 from tb_linha_tempo_h), :0, :1, :2, :3)');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := edhub.value;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := edDias.Value;
      dtmDados.iQuery1.Parameters.ParamByName('2').Value := QPercursoID_LINHA_TEMPO_P.AsInteger;
      dtmDados.iQuery1.Parameters.ParamByName('3').Value := glbuser;
    end
    else
    begin
      dtmDados.iQuery1.SQL.add('update tb_linha_tempo_h set ' +
        'hub = :0, dia = :1, user_alt = :2, dt_alt = :3 where id_linha_tempo_h = :4');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := edHub.value;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := edDias.Value;
      dtmDados.iQuery1.Parameters.ParamByName('2').Value := glbuser;
      dtmDados.iQuery1.Parameters.ParamByName('3').Value := date;
      dtmDados.iQuery1.Parameters.ParamByName('4').Value := QHubID_LINHA_TEMPO_H.AsInteger;
    end;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QHub.Close;
    QHub.Parameters[0].Value := QPercursoID_LINHA_TEMPO_P.AsInteger;
    QHub.Open;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;

  tipo := '';
  RestauraI;
end;

procedure TfrmCadLinhaTempo.btnSalvarTranspClick(Sender: TObject);
var cod, a, b : Integer;
begin
  if cbtransp.text = '' then
  begin
    ShowMessage('Transportador N�o Escolhido !');
    cbtransp.SetFocus;
    Exit;
  end;
  a := Length(cbtransp.Text);
  b := pos('=', cbtransp.Text);
  cod := strToInt(copy(cbtransp.text, b+1, (a-b)));

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select distinct t.* ');
  dtmDados.iQuery1.SQL.add('from tb_linha_tempo_t t ');
  dtmDados.iQuery1.SQL.add('where t.cod_transp = :0');
  dtmDados.iQuery1.Parameters[0].Value := cod;
  dtmDados.iQuery1.Open;
  if not dtmDados.iQuery1.FieldByName('id_linha_tempo_t').IsNull then
  begin
    ShowMessage('Este Transportador j� est� cadastrado !!');
    btnCancelarClick(Sender);
    QTransp.Locate('id_linha_tempo_t', dtmDados.iQuery1.FieldByName('id_linha_tempo_t').Value, []);
    Exit;
  end;
  dtmDados.iQuery1.Close;
  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.SQL.add
        ('insert into tb_linha_tempo_t (id_linha_tempo_t, cod_transp, user_inc) values ' +
        '((select max(id_linha_tempo_t)+1 from tb_linha_tempo_t), :0, :1)');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := cod;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := glbuser;
    end
    else
    begin
      dtmDados.iQuery1.SQL.add('update tb_linha_tempo_t set ' +
        'cod_transp = :0, user_alt = :1, dt_alt = :2 where id_linha_tempo_t = :3');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := cod;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := glbuser;
      dtmDados.iQuery1.Parameters.ParamByName('2').Value := date;
      dtmDados.iQuery1.Parameters.ParamByName('3').Value := QTranspID_LINHA_TEMPO_T.AsInteger;
    end;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QTransp.Close;
    QTransp.Open;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;

  tipo := '';
  RestauraT;
end;

procedure TfrmCadLinhaTempo.btnAlterarIClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  tipo := 'A';
  RestauraI;
  edHub.Value := QHubHUB.AsInteger;
  edDias.value := QHubDIA.AsInteger;
  edHub.SetFocus;
end;

procedure TfrmCadLinhaTempo.btnAltTransportadorClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  tipo := 'A';
  RestauraT;
  QTransportador.Close;
  QTransportador.Open;
  cbTransp.Clear;
  cbTransp.Items.Add('');
  while not QTransportador.Eof do
  begin
    cbTransp.Items.Add(QTransportadorTRANSP.AsString);
    QTransportador.Next;
  end;
  cbTransp.SetFocus;
  cbtransp.ItemIndex := cbTransp.Items.IndexOf(QTranspRAZSOC.AsString+'='+QTranspCOD_TRANSP.AsString);
end;

procedure TfrmCadLinhaTempo.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QPercurso.First;
    2:
      QPercurso.Prior;
    3:
      QPercurso.Next;
    4:
      QPercurso.Last;
  end;
  Screen.Cursor := crDefault;
end;

end.
