unit MonRoadNet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, Vcl.StdCtrls, Vcl.Buttons, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit;

type
  TfrmMonRoadNet = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    grMonitor: TDBGrid;
    qrMonitor: TADOQuery;
    dsMonitor: TDataSource;
    lbTotReg: TLabel;
    qrMonitorID_ROADNET: TFMTBCDField;
    qrMonitorCLIENTE: TStringField;
    qrMonitorDATAINC: TDateTimeField;
    qrMonitorDOC: TStringField;
    qrMonitorSTATUS: TStringField;
    qrMonitorCOD_USUARIO: TFMTBCDField;
    qrMonitorROTA: TStringField;
    qrMonitorVEICULO: TStringField;
    qrMonitorDATAROTA: TDateTimeField;
    qrMonitorNOME_CLI: TStringField;
    qrMonitorNM_STATUS: TStringField;
    qrMonitorCODCON: TFMTBCDField;
    dtInicial: TJvDateEdit;
    Label4: TLabel;
    dtFinal: TJvDateEdit;
    cbTipo: TComboBox;
    btnPesquisa: TBitBtn;
    btnReprocessar: TBitBtn;
    SP_Road: TADOStoredProc;
    qrMonitorCODCGC: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure grMonitorDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnPesquisaClick(Sender: TObject);
    procedure btnReprocessarClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa();

  public
    { Public declarations }
    iEmpresa: integer;

  end;

var
  frmMonRoadNet: TfrmMonRoadNet;

implementation

uses Dados, menu, funcoes;

{$R *.dfm}

procedure TfrmMonRoadNet.btnPesquisaClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TfrmMonRoadNet.btnReprocessarClick(Sender: TObject);
begin
  // enviar os CT-es para o RoadNet
  if not dtmDados.PodeInserir('RoadNet') then
    Exit;

  if (cbTipo.ItemIndex <> 2) then
  begin
    ShowMessage('S� pode ser reprocessado Status Enviado ao Roadnet');
  end;


  if qrMonitor.RecordCount = 0 then
    Exit;

  if Application.Messagebox('Enviar para o RoadNet agora ?', 'Gerar Roteiriza��o',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    qrMonitor.First;

    while not qrMonitor.eof do
    begin
      SP_Road.Parameters[0].Value := qrMonitorID_ROADNET.AsInteger;
      SP_Road.Parameters[1].Value := 'Z';
      SP_Road.Parameters[2].Value := GLBCodUser;
      SP_Road.Parameters[3].Value := qrMonitorCODCGC.AsString;
      SP_Road.Parameters[4].Value := '';
      SP_Road.Parameters[5].Value := GLBFilial.ToString;
      SP_Road.ExecProc;
      qrMonitor.next;
    end;

    ShowMessage('Documentos enviados ao RoadNet');
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmMonRoadNet.FormCreate(Sender: TObject);
begin
  dtInicial.Date := date-1;
  dtFinal.Date := date;
  Pesquisa();
end;

procedure TfrmMonRoadNet.grMonitorDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  // Aguardado Aceite
  if (qrMonitorSTATUS.value = 'A') then
  begin
    grMonitor.canvas.Brush.color := clRed;
    grMonitor.canvas.font.color  := clWhite;
  end;

  // Aguardado Agendamento
  if (qrMonitorSTATUS.value = 'G') then
  begin
    grMonitor.canvas.Brush.color := clBlack;
    grMonitor.canvas.font.color  := clWhite;
  end;

  grMonitor.DefaultDrawDataCell(Rect, grMonitor.Columns[DataCol].field, State);
end;

procedure TfrmMonRoadNet.Pesquisa();
 var   d1, d2 : string;
begin
  d1 := formatdatetime('dd/mm/yy',dtinicial.Date) + ' 00:00:01';
  d2 := formatdatetime('dd/mm/yy',dtfinal.Date) + ' 23:59:59';
  qrMonitor.close;
  qrMonitor.SQL[9] := 'Where datainc between to_date(' + #39 + d1 + #39 + ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + d2 + #39+',''dd/mm/yy hh24:mi:ss'')';

  // Aguardando Agendamento
  if (cbTipo.ItemIndex = 1) then
    qrMonitor.SQL[10] := ' and r.status not in ( ''I'',''R'',''A'',''G'' ) '
  // Aguardando Agendamento
  else if (cbTipo.ItemIndex = 5) then
    qrMonitor.SQL[10] := ' and r.status = ''G'' '
  // Enviado RN
  else if (cbTipo.ItemIndex = 2) then
    qrMonitor.SQL[10] := ' and r.status = ''I'' '
  // Retornado RN
  else if (cbTipo.ItemIndex = 3) then
    qrMonitor.SQL[10] := ' and r.status = ''R'' '
  // Aguardando Aceite
  else if (cbTipo.ItemIndex = 4) then
    qrMonitor.SQL[10] := ' and r.status = ''A'' '
  else
    qrMonitor.SQL[10] := ' ';

  qrMonitor.SQL[11] := ' and filial = ' + QuotedStr(glbfilial.ToString);
//showmessage(qrMonitor.SQL.text);
  qrMonitor.open;
  qrMonitor.First;
  lbTotReg.Caption := 'Total de Registros: ' + InttoStr(qrMonitor.RecordCount);
end;

end.
