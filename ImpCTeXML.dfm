object frmEDICTRCXML: TfrmEDICTRCXML
  Left = 344
  Top = 206
  Caption = 'Importa'#231#227'o do XML de CTRC'
  ClientHeight = 594
  ClientWidth = 661
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelImp: TPanel
    Left = 0
    Top = 0
    Width = 661
    Height = 594
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 171
      Top = 37
      Width = 59
      Height = 16
      Caption = 'Arquivos :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 2
      Width = 229
      Height = 20
      Caption = 'Selecione o Arquivo a ser Importado :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 37
      Width = 59
      Height = 16
      Caption = 'Caminho :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PB1: TGauge
      Left = 8
      Top = 551
      Width = 641
      Height = 34
      Progress = 0
    end
    object Label5: TLabel
      Left = 233
      Top = 37
      Width = 6
      Height = 15
      Caption = '  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel
      Left = 183
      Top = 262
      Width = 6
      Height = 16
      Caption = '  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label8: TLabel
      Left = 8
      Top = 487
      Width = 58
      Height = 13
      Caption = 'Observa'#231#227'o'
    end
    object btnSair: TBitBtn
      Left = 560
      Top = 442
      Width = 84
      Height = 39
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
        9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
        71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
        9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
        FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
        A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
        FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
        AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
        FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
        B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
        FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
        B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
        3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
        BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
        66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
        BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
        47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
        C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
        FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
        C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
        FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
        CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
        FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
        D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
        FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
        D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
        FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
        D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
      ModalResult = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnSairClick
    end
    object btnGravar: TBitBtn
      Left = 560
      Top = 118
      Width = 84
      Height = 34
      Caption = '&Gravar'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      ParentFont = False
      TabOrder = 1
      OnClick = btnGravarClick
    end
    object cbTodos: TCheckBox
      Left = 572
      Top = 66
      Width = 72
      Height = 17
      Caption = 'Todos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object FLB: TFileListBox
      Left = 171
      Top = 58
      Width = 383
      Height = 423
      Color = 6316032
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      Mask = '*cte*.xml'
      ParentFont = False
      TabOrder = 3
      OnClick = FLBClick
    end
    object DirectoryListBox1: TDirectoryListBox
      Left = 0
      Top = 80
      Width = 165
      Height = 401
      FileList = FLB
      TabOrder = 4
    end
    object DriveComboBox1: TDriveComboBox
      Left = 2
      Top = 58
      Width = 163
      Height = 19
      DirList = DirectoryListBox1
      TabOrder = 5
    end
    object Edit1: TEdit
      Left = 264
      Top = 4
      Width = 321
      Height = 21
      TabOrder = 6
      Text = '*.xml'
      Visible = False
    end
    object Panel1: TPanel
      Left = 155
      Top = 138
      Width = 414
      Height = 266
      TabOrder = 7
      Visible = False
      object Label2: TLabel
        Left = 12
        Top = 9
        Width = 359
        Height = 16
        Caption = 'Alguns Arquivos XML n'#227'o foram importados  porque n'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Albertus Extra Bold'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 20
        Top = 31
        Width = 361
        Height = 16
        Caption = 'existe Tabela de Custo para as Transportadoras Listadas.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Albertus Extra Bold'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Memo1: TMemo
        Left = 12
        Top = 80
        Width = 389
        Height = 129
        BorderStyle = bsNone
        Color = 14150376
        Lines.Strings = (
          'Memo1')
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Button1: TButton
        Left = 144
        Top = 215
        Width = 97
        Height = 34
        Caption = 'OK'
        TabOrder = 1
        OnClick = Button1Click
      end
    end
    object Memo2: TMemo
      Left = 8
      Top = 506
      Width = 641
      Height = 39
      TabOrder = 8
    end
  end
  object ACBrCTe1: TACBrCTe
    Configuracoes.Geral.SSLLib = libCapicomDelphiSoap
    Configuracoes.Geral.SSLCryptLib = cryCapicom
    Configuracoes.Geral.SSLHttpLib = httpIndy
    Configuracoes.Geral.SSLXmlSignLib = xsMsXmlCapicom
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Geral.ValidarDigest = False
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Left = 288
    Top = 8
  end
  object SP_Comp: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_COMP'
    Parameters = <
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'EMIT'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = 16
        Value = '0'
      end
      item
        Name = 'NOME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end>
    Left = 452
    Top = 76
  end
  object SP_carga: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_CARGA'
    Parameters = <
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'EMIT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCARGA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VMED'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VQUANT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end>
    Left = 456
    Top = 140
  end
  object SP_NF: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_NF'
    Parameters = <
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'EMIT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VNF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCHAVE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VVALOR'
        DataType = ftFloat
        Value = 0.000000000000000000
      end>
    Left = 460
    Top = 204
  end
  object SP_XML: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_CTE'
    Parameters = <
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'REM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'DES'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VDATE'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VPAG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VBASE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VALIQ'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VIMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VEMIT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCHAVE'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VUSU'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VTOMA'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VOBS'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 388
    Top = 76
  end
  object QTabCusto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select count(*) quant from tb_custofrac '
      'where cod_fornecedor in (select codclifor'
      'from cyber.rodcli where substr(codcgc,1,10) = :0)')
    Left = 232
    Top = 200
    object QTabCustoQUANT: TBCDField
      FieldName = 'QUANT'
      ReadOnly = True
      Precision = 32
    end
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select codclifor, razsoc from cyber.rodcli where codcgc =:0')
    Left = 288
    Top = 200
    object QTranspCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object QTranspRAZSOC: TStringField
      FieldName = 'RAZSOC'
      Size = 80
    end
  end
  object SP_ROD: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_CTE_RODOPAR'
    Parameters = <
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'REM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'DES'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VDATE'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VPAG'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VBASE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VALIQ'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VIMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VEMIT'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCHAVE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSU'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VTOMA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 380
    Top = 204
  end
  object SP_TEM: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'sp_xml_tem'
    Parameters = <
      item
        Name = 'TEM'
        DataType = ftInteger
        Direction = pdOutput
        Value = 0
      end
      item
        Name = 'VID'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'EMIT'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 268
    Top = 80
  end
  object ConIntecom: TADOConnection
    ConnectionString = 
      'Provider=OraOLEDB.Oracle.1;Password=jkl6721h;Persist Security In' +
      'fo=True;User ID=intecom;Data Source=INTECPRD'
    KeepConnection = False
    LoginPrompt = False
    Provider = 'OraOLEDB.Oracle.1'
    Left = 324
    Top = 24
  end
  object SP_INTEGRA: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_XML_CTE_INTEGRA'
    Parameters = <
      item
        Name = 'VCTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'VEMIT'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 464
    Top = 292
  end
end
