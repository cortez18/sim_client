unit GeraRoma;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, ImgList, ComCtrls, JvMaskEdit,
  Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  ExtCtrls, DBCtrls, IniFiles, JvExStdCtrls, JvCombobox, ShellAPI,
  System.ImageList;

Const
  InputBoxMessage = WM_USER + 200;

type
  TfrmGeraRoma = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblqt: TLabel;
    Query1: TADOQuery;
    mdTemp: TJvMemoryData;
    mdTempInserida: TIntegerField;
    mdTempreg: TIntegerField;
    dsTemp: TDataSource;
    iml: TImageList;
    TabSheet4: TTabSheet;
    lbTareFas_romaneio: TLabel;
    qrRomaneio: TADOQuery;
    dsRomaneio: TDataSource;
    dsTempRomaneio: TDataSource;
    mdTempromaneio: TJvMemoryData;
    mdTempromaneioSELECIONADO: TBCDField;
    mdTempromaneioNR_ROMANEIO: TBCDField;
    mdTempromaneioDESTINO: TStringField;
    mdTempnr_romaneio: TIntegerField;
    QMapaItens: TADOQuery;
    pnManifesto: TPanel;
    Label3: TLabel;
    edVcto: TJvDateEdit;
    Label4: TLabel;
    Label11: TLabel;
    edPlaca: TJvMaskEdit;
    Panel5: TPanel;
    btFatura: TBitBtn;
    btnNovoManifesto: TBitBtn;
    btnImpressao: TBitBtn;
    ds: TDataSource;
    dgTarefa: TJvDBUltimGrid;
    dbgFatura: TJvDBUltimGrid;
    mdTempromaneioDEMISSAO: TDateTimeField;
    Panel8: TPanel;
    lblDescricao: TLabel;
    edValor: TEdit;
    QTarefa: TADOQuery;
    qMapaCarga: TADOQuery;
    mdTempromaneioSUSUARIO: TStringField;
    mdTempromaneioPLACA: TStringField;
    mdTempromaneioVEICULO: TStringField;
    mdTempromaneioselecionou: TIntegerField;
    btnLimpaRomaneio: TBitBtn;
    qrRota: TADOQuery;
    dsRota: TDataSource;
    QDestino: TADOQuery;
    pnTarefas: TPanel;
    pgTarefas: TPageControl;
    tbRota: TTabSheet;
    dgRota: TJvDBUltimGrid;
    tbTarefa: TTabSheet;
    dbgColeta: TJvDBUltimGrid;
    Panel4: TPanel;
    Label16: TLabel;
    edTotVolumeTarefa: TJvCalcEdit;
    ckRota: TCheckBox;
    Label18: TLabel;
    Obs: TRichEdit;
    qrRotaNAME: TStringField;
    QDestinoNR_AUF: TStringField;
    qrRotaID_SPEDITEUR: TStringField;
    QTarefaNR_AUF: TStringField;
    QTarefaNAME: TStringField;
    QTarefaTRANSPORTADORA: TStringField;
    QTarefaPERCENT: TBCDField;
    qrRomaneioTRANSPORTADORA: TStringField;
    qrRomaneioTTVOL: TBCDField;
    qrRomaneioPERCENT: TBCDField;
    Label1: TLabel;
    qrRomaneioID_SPEDITEUR: TStringField;
    qMapaCargaNR_ROMANEIO: TBCDField;
    qMapaCargaMOTORISTA: TStringField;
    qMapaCargaPLACA: TStringField;
    qMapaCargaVEICULO: TStringField;
    qMapaCargaDTINC: TDateTimeField;
    qMapaCargaTRANSPORTADORA: TStringField;
    QMapaItensNR_AUF: TStringField;
    QMapaItensNAME: TStringField;
    QMapaItensPERCENT: TBCDField;
    edVeiculo: TComboBox;
    mdTemppedido: TStringField;
    Panel3: TPanel;
    pnBotao: TPanel;
    btRomaneio: TBitBtn;
    btnCarregar_Tarefas: TBitBtn;
    btnVoltaPedido: TBitBtn;
    edMotorista: TJvMaskEdit;
    tbImpresso: TTabSheet;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QImpressos: TADOQuery;
    dtsImpresso: TDataSource;
    QImpressosNR_ROMANEIO: TBCDField;
    QImpressosMOTORISTA: TStringField;
    QImpressosPLACA: TStringField;
    QImpressosVEICULO: TStringField;
    QImpressosDTINC: TDateTimeField;
    QImpressosTRANSPORTADORA: TStringField;
    QImpressosOPID_BEARB: TStringField;
    Panel6: TPanel;
    btnImpresso: TBitBtn;
    qMapaCargaOBSERVACAO: TStringField;
    QDestinoINSERIDA: TBCDField;
    mdTempromaneiocod_transp: TStringField;
    QMapaItensCIDADE: TStringField;
    QMapaItensUF: TStringField;
    btnAlterar: TBitBtn;
    Panel7: TPanel;
    Label2: TLabel;
    cbCliente: TComboBox;
    Label5: TLabel;
    QCliente: TADOQuery;
    QClienteID_KLIENT: TStringField;
    QTarefaSEQ_TOUR: TBCDField;
    DBNavigator1: TDBNavigator;
    QImpressosID_KLIENT: TStringField;
    Edit1: TEdit;
    Label6: TLabel;
    qMapaCargaID_KLIENT: TStringField;
    Panel9: TPanel;
    Label38: TLabel;
    btnFechar: TBitBtn;
    cbOcorr: TJvComboBox;
    JvDBUltimGrid2: TJvDBUltimGrid;
    mdAltera: TJvMemoryData;
    dtsAlterar: TDataSource;
    mdAlteraReg: TIntegerField;
    mdAlteranr_romaneio: TIntegerField;
    mdAlteranf: TStringField;
    btnSair: TBitBtn;
    btnTMS: TBitBtn;
    QImpressosTMS: TStringField;
    Qexporta: TADOQuery;
    Panel10: TPanel;
    Edit2: TEdit;
    Edit3: TEdit;
    QMapaItensSEQ_TOUR: TBCDField;
    DataSource1: TDataSource;
    cbTodos: TCheckBox;
    QArquivoBaruel: TADOQuery;
    QArquivoBaruelNR_ROMANEIO: TBCDField;
    QArquivoBaruelNR_AUF: TStringField;
    QArquivoBaruelNR_AUF_TA: TIntegerField;
    QArquivoBaruelNAME: TStringField;
    QArquivoBaruelSUCHBEGRIFF: TStringField;
    Qverificaromaneio: TADOQuery;
    QUpdateRomaneio: TADOQuery;
    QTotal: TADOQuery;
    QTotalTOTAL: TBCDField;
    QImpressosFL_ARQ: TStringField;
    QImpressosDTSAIDA: TDateTimeField;
    btnListagem: TBitBtn;
    mdAlterasite: TStringField;
    Label7: TLabel;
    cbSite: TComboBox;
    mdTempromaneiopeso: TIntegerField;
    btnRetorna: TBitBtn;
    QMapaItensnr_nf: TStringField;
    QTarefacliente: TStringField;
    QTarefadestino: TStringField;
    mdTarefa: TJvMemoryData;
    mdTarefaNR_AUF: TStringField;
    mdTarefaNAME: TStringField;
    mdTarefaTRANSPORTADORA: TStringField;
    mdTarefaPERCENT: TBCDField;
    mdTarefaSEQ_TOUR: TBCDField;
    mdTarefacliente: TStringField;
    mdTarefadestino: TStringField;
    mdTarefapalete: TIntegerField;
    mdTarefatem: TIntegerField;
    QTarefaqt_palete: TFMTBCDField;
    Label8: TLabel;
    cbClienteI: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgColetaCellClick(Column: TColumn);
    procedure dbgColetaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btFaturaClick(Sender: TObject);
    procedure btRomaneioClick(Sender: TObject);
    procedure btnCarregar_TarefasClick(Sender: TObject);
    procedure btnLimpaRomaneioClick(Sender: TObject);
    procedure dbgFaturaCellClick(Column: TColumn);
    procedure dbgFaturaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCarregaClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure rgTarefasClick(Sender: TObject);
    procedure btnNovoManifestoClick(Sender: TObject);
    procedure dsTempRomaneioDataChange(Sender: TObject; Field: TField);
    procedure btnImpressaoClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure mdTempAfterOpen(DataSet: TDataSet);
    procedure tbTarefaShow(Sender: TObject);
    procedure btnVoltaPedidoClick(Sender: TObject);
    procedure ckRotaClick(Sender: TObject);
    procedure btnImpressoClick(Sender: TObject);
    procedure tbImpressoShow(Sender: TObject);
    procedure tbImpressoHide(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure qrRotaAfterOpen(DataSet: TDataSet);
    procedure cbClienteChange(Sender: TObject);
    procedure qrRomaneioBeforeOpen(DataSet: TDataSet);
    procedure QMapaItensBeforeOpen(DataSet: TDataSet);
    procedure cbClienteExit(Sender: TObject);
    procedure QDestinoBeforeOpen(DataSet: TDataSet);
    procedure Edit1Exit(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure JvDBUltimGrid2CellClick(Column: TColumn);
    procedure JvDBUltimGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnSairClick(Sender: TObject);
    procedure btnTMSClick(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbTodosClick(Sender: TObject);
    procedure JvDBUltimGrid1DblClick(Sender: TObject);
    procedure btnListagemClick(Sender: TObject);
    procedure cbSiteChange(Sender: TObject);
    procedure QImpressosBeforeOpen(DataSet: TDataSet);
    procedure btnRetornaClick(Sender: TObject);
    procedure QTarefaBeforeOpen(DataSet: TDataSet);
    procedure edPlacaExit(Sender: TObject);
    procedure dgTarefaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure mdTarefapaleteChange(Sender: TField);
  private
    { Private declarations }
    iRecDelete: string;
    procedure CarregaRomaneio();
    procedure CarregaTarefas();
    procedure CarregaRomaneioManifesto();
    procedure LimpaCamposManifesto();
    function RetornaTarefasSelecionadas(): String;
    procedure InputBoxSetPasswordChar(var Msg: TMessage);
      message InputBoxMessage;
  public
    { Public declarations }
    bInicio: Boolean;
    bLimpaTodos: Boolean;
    iLimpaRomaneio, iLimpaTarefa: Integer;
    sCli_os, sOs_tarvar, starefa: string;
    sTipoForm: String;
    procedure CarregaBtn();
  end;

var
  frmGeraRoma: TfrmGeraRoma;
  tt: real;
  dife, alterado: string;
  reg: Integer;
  arq: TIniFile;
  localarq: string;

implementation

uses Dados, menu, funcoes, RomaneioRetira;

{$R *.dfm}

procedure TfrmGeraRoma.btnSairClick(Sender: TObject);
begin
  Panel9.Visible := FALSE;
end;

procedure TfrmGeraRoma.btnTMSClick(Sender: TObject);
var
  rom: Integer;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  rom := QImpressosNR_ROMANEIO.AsInteger;
  if QImpressosTMS.AsString = 'S' then
  begin
    ShowMessage('Romaneio J� Importado pelo TMS');
    Exit;
  end;

  Qexporta.close;
  Qexporta.Parameters[0].value := glbuser;
  Qexporta.Parameters[1].value := glbfilial;
  Qexporta.Parameters[2].value := rom;

  Qexporta.ExecSQL;
  Qexporta.close;
  ShowMessage('Romaneio Exportado para o TMS');
  Qexporta.close;
  QImpressos.close;
  QImpressos.open;
  QImpressos.locate('nr_romaneio', rom, []);
end;

procedure TfrmGeraRoma.btFaturaClick(Sender: TObject);
begin
  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('update auftraege set opid_bearb = :0 ');
  Query1.sql.add('where percent = :1 ');
  Query1.Parameters[0].value := edPlaca.Text;
  Query1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
  Query1.ExecSQL;

  mdTempromaneio.First;

  if edMotorista.Text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    edMotorista.SetFocus;
    Exit;
  end;

  if edPlaca.Text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    edPlaca.SetFocus;
    Exit;
  end;

  if edVeiculo.Text = '' then
  begin
    ShowMessage('Informe o ve�culo');
    edVeiculo.SetFocus;
    Exit;
  end;

  if Application.Messagebox('Finalizar Romaneio Agora ?', 'Finalizar Romaneio',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set opid_bearb = :0 ');
    Query1.sql.add('where percent = :1 ');
    Query1.Parameters[0].value := edPlaca.Text;
    Query1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;

    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('insert into tb_romaneio (nr_romaneio, cod_transp, motorista, placa, veiculo, observacao, user_alt, fl_empresa) ');
    Query1.sql.add('values (:0, :1, :2, :3, :4, :5, :6, :7)');
    Query1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.Parameters[1].value := mdTempromaneiocod_transp.AsString;
    Query1.Parameters[2].value := edMotorista.Text;
    Query1.Parameters[3].value := ApCarac(edPlaca.Text);
    Query1.Parameters[4].value := edVeiculo.Text;
    Query1.Parameters[5].value := Obs.Text;
    Query1.Parameters[6].value := glbuser;
    Query1.Parameters[7].value := glbfilial;
    Query1.ExecSQL;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Romaneio Finalizado');
    btFatura.Enabled := FALSE;
    // impress�o
    qMapaCarga.close;
    qMapaCarga.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    qMapaCarga.Parameters[1].value := cbSite.Text;
    qMapaCarga.open;
    QMapaItens.close;
    QMapaItens.Parameters[1].value := cbSite.Text;
    QMapaItens.Parameters[2].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QMapaItens.open;
    try
      Application.CreateForm(TfrmRomaneioRetira, frmRomaneioRetira);
      frmRomaneioRetira.RlReport1.Preview;
    finally
      frmRomaneioRetira.Free;
    end;
    qMapaCarga.close;
    QMapaItens.close;
    mdTemp.close;
    mdTemp.open;
    CarregaTarefas();
    QTarefa.close;
    QTarefa.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTemppedido.value := QDestinoNR_AUF.AsString;
      QDestino.Next;
    end;
    QDestino.close;
  end
  else
  begin
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set opid_bearb = null, percent = null ');
    Query1.sql.add('where percent = :1 ');
    Query1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
  end;
  CarregaRomaneioManifesto();
  LimpaCamposManifesto();
  pnManifesto.Enabled := FALSE;
  dbgFatura.Enabled := True;
  btnImpressao.Enabled := True;
end;

procedure TfrmGeraRoma.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;

  if not QImpressosDTSAIDA.IsNull then
  begin
    ShowMessage('Este Romaneio j� saiu pela portaria !!');
    Exit;
  end;

  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('select distinct r.nr_romaneio, p.nr_auf, p.lager  ');
  Query1.sql.add
    ('from tb_romaneio r left join auftraege p on p.percent = r.nr_romaneio ');
  Query1.sql.add('where p.percent = :0 ');
  Query1.sql.add('order by 2 ');
  Query1.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
  Query1.open;
  mdAltera.close;
  mdAltera.open;
  while not Query1.eof do
  begin
    mdAltera.append;
    mdAlteranf.value := Query1.FieldByName('nr_auf').value;
    mdAlteranr_romaneio.value := Query1.FieldByName('nr_romaneio').value;
    mdAlteraReg.value := 1;
    mdAlterasite.value := Query1.FieldByName('lager').value;
    Query1.Next;
  end;
  mdAltera.First;

  Panel9.Visible := True;
  Panel9.SetFocus;
  cbOcorr.SetFocus;
end;

procedure TfrmGeraRoma.btnCarregaClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraRoma.btnCarregar_TarefasClick(Sender: TObject);
begin
  CarregaBtn();
end;

procedure TfrmGeraRoma.btnFecharClick(Sender: TObject);
begin
  if cbOcorr.Text = '' then
  begin
    ShowMessage('� Obrigat�rio escolher uma ocorr�ncia para a Altera��o !');
    cbOcorr.SetFocus;
  end
  else
  begin
    mdAltera.First;
    while not mdAltera.eof do
    begin
      if mdAlteraReg.value = 1 then
      begin
        Query1.close;
        Query1.sql.clear;
        Query1.sql.add
          ('insert into auftexte (LAGER, ID_KLIENT, NR_AUF, NR_AUF_TA, ');
        Query1.sql.add
          ('ART_TXT, NR_LFD, STAT, TXT, TIME_NEU, OPID_NEU, TIME_AEN, OPID_AEN, ');
        Query1.sql.add('LASTPROG, LASTUSER, LASTUPDT)');
        Query1.sql.add('values (:0, :1, :2, 1, ''BT'', ');
        Query1.sql.add
          ('(select nvl(max(nr_lfd)+10,10) from auftexte where nr_auf = :3), ');
        Query1.sql.add(' ''00'', :4, sysdate, :5,');
        Query1.sql.add(' sysdate, :6, ''AK110'', ''WMS'', sysdate ) ');
        Query1.Parameters[0].value := mdAlterasite.value;
        Query1.Parameters[1].value := QImpressosID_KLIENT.value;
        Query1.Parameters[2].value := mdAlteranf.value;
        Query1.Parameters[3].value := mdAlteranf.value;
        Query1.Parameters[4].value := cbOcorr.Text;
        Query1.Parameters[5].value := copy(glbuser, 1, 7);
        Query1.Parameters[6].value := copy(glbuser, 1, 7);
        Query1.ExecSQL;
        Query1.close;
      end;
      mdAltera.Next;
    end;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set opid_bearb = null ');
    Query1.sql.add('where percent = :0 and id_klient = ' +
      QuotedStr(QImpressosID_KLIENT.AsString));
    Query1.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('update tb_romaneio set user_alt = :0, dtalteracao = sysdate ');
    Query1.sql.add('where nr_romaneio = :1 ');
    Query1.Parameters[0].value := glbuser;
    Query1.Parameters[1].value := QImpressosNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;

    Panel9.Visible := FALSE;
    QImpressos.close;
    QImpressos.open;
    CarregaBtn();
  end;
end;

procedure TfrmGeraRoma.btnLimpaRomaneioClick(Sender: TObject);
begin
  if mdTempromaneio.RecordCount = 0 then
    Exit;

  if Application.Messagebox('Exclui Romaneios agora ?', 'Excluir Romaneios',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set percent = null ');
    Query1.sql.add(' where percent = :0 ');
    Query1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('update tb_romaneio set user_del = :0, dtexclusao = sysdate ');
    Query1.sql.add('where nr_romaneio = :1');
    Query1.Parameters[0].value := glbuser;
    Query1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;

    dtmdados.iQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add('delete from tb_palet_romaneio where tipo = ''R'' and numero = :0 ');
    dtmdados.IQuery1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.iQuery1.close;
    ShowMessage('Romaneio Exclu�dos');
    mdTempromaneio.close;
  end;
  CarregaRomaneioManifesto();
end;

procedure TfrmGeraRoma.btnListagemClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\sim\Romaneio_WEB' + glbuser + '.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>IW - Intecom</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to JvDBUltimGrid1.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + JvDBUltimGrid1.Columns[i]
      .Title.Caption + '</th>');
  end;
  memo1.add('</tr>');
  QImpressos.First;
  while not QImpressos.eof do
  begin
    for i := 0 to JvDBUltimGrid1.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + QImpressos.FieldByName
        (JvDBUltimGrid1.Columns[i].FieldName).AsString + '</th>');
    QImpressos.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  memo1.Free;
  ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
    SW_SHOWNORMAL);
end;

procedure TfrmGeraRoma.btnImpressaoClick(Sender: TObject);
var
  arq_baruel: TextFile;
  nome_arquivo, contrans, dataexped: string;
  znf: Integer;
begin
  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
  qMapaCarga.Parameters[1].value := cbSite.Text;
  qMapaCarga.open;

  QMapaItens.close;
  QMapaItens.Parameters[2].value := mdTempromaneioNR_ROMANEIO.AsInteger;
  QMapaItens.open;

  if (QMapaItensNR_AUF.AsString = '') then
  begin
    ShowMessage('Romaneio sem NF');
    Exit;
  end;

  try
    Application.CreateForm(TfrmRomaneioRetira, frmRomaneioRetira);
    frmRomaneioRetira.RlReport1.Preview;
  finally
    frmRomaneioRetira.Free;
  end;
  qMapaCarga.close;
  QMapaItens.close;

  // Implementado por Hugo referente a RPP 212
  Qverificaromaneio.close;
  Qverificaromaneio.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
  Qverificaromaneio.open;
  if not Qverificaromaneio.IsEmpty then
  begin
    QArquivoBaruel.close;
    QArquivoBaruel.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QArquivoBaruel.open;

    if not QArquivoBaruel.IsEmpty then
    begin
      Sleep(1100);
      contrans := copy(TimeToStr(now), 7, 1) + copy((DateToStr(date)), 5, 1) +
        copy(TimeToStr(now), 4, 2) + copy(TimeToStr(now), 8, 1);
      nome_arquivo := localarq + '\EXPE_NFE' + copy((DateToStr(date)), 1, 2) +
        copy((DateToStr(date)), 4, 2) + copy((DateToStr(date)), 7, 4) +
        contrans + '.txt';
      AssignFile(arq_baruel, nome_arquivo);
      Rewrite(arq_baruel);
      while not QArquivoBaruel.eof do
      Begin
        Write(arq_baruel, 'EXPE_NFE');
        Write(arq_baruel, '03857930000154');
        Write(arq_baruel, QArquivoBaruelSUCHBEGRIFF.AsString);
        Write(arq_baruel, '         ');
        Write(arq_baruel, '   ');
        dataexped := copy((FormatDateTime('dd/mm/yyyy', date)), 1, 2) +
          copy((FormatDateTime('dd/mm/yyyy', date)), 4, 2) +
          copy((FormatDateTime('dd/mm/yyyy', date)), 7, 4) +
          copy(TimeToStr(now), 1, 2) + copy(TimeToStr(now), 4, 2) +
          copy(TimeToStr(now), 7, 2);
        Write(arq_baruel, dataexped);
        znf := 6 - Length(QArquivoBaruelNR_AUF.AsString);
        if Length(QArquivoBaruelNR_AUF.AsString) < 6 then
          Write(arq_baruel, StringOfChar('0', znf) +
            QArquivoBaruelNR_AUF.AsString)
        else
          Write(arq_baruel, QArquivoBaruelNR_AUF.AsString);
        Writeln(arq_baruel, '');
        QArquivoBaruel.Next;
      End;
      closeFile(arq_baruel);
      QUpdateRomaneio.close;
      QUpdateRomaneio.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
      QUpdateRomaneio.ExecSQL;
    end;
  end;
end;

procedure TfrmGeraRoma.btnImpressoClick(Sender: TObject);
var
  arq_baruel: TextFile;
  nome_arquivo, contrans, dataexped: string;
  znf: Integer;
begin
  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
  qMapaCarga.Parameters[1].value := cbSite.Text;
  qMapaCarga.open;

  QMapaItens.close;
  QMapaItens.Parameters[1].value := cbSite.Text;
  QMapaItens.Parameters[2].value := QImpressosNR_ROMANEIO.AsInteger;
  QMapaItens.open;

  try
    Application.CreateForm(TfrmRomaneioRetira, frmRomaneioRetira);
    frmRomaneioRetira.RlReport1.Preview;
  finally
    frmRomaneioRetira.Free;
  end;
  qMapaCarga.close;
  QMapaItens.close;

  // Implementado por Hugo referente a RPP 212
  Qverificaromaneio.close;
  Qverificaromaneio.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
  Qverificaromaneio.open;

  if not Qverificaromaneio.IsEmpty then
  begin
    QArquivoBaruel.close;
    QArquivoBaruel.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
    QArquivoBaruel.open;

    if not QArquivoBaruel.IsEmpty then
    begin
      Sleep(1100);
      contrans := copy(TimeToStr(now), 7, 1) + copy((DateToStr(date)), 5, 1) +
        copy(TimeToStr(now), 4, 2) + copy(TimeToStr(now), 8, 1);
      nome_arquivo := localarq + '\EXPE_NFE' + copy((DateToStr(date)), 1, 2) +
        copy((DateToStr(date)), 4, 2) + copy((DateToStr(date)), 7, 4) +
        contrans + '.txt';
      AssignFile(arq_baruel, nome_arquivo);
      Rewrite(arq_baruel);
      while not QArquivoBaruel.eof do
      Begin
        Write(arq_baruel, 'EXPE_NFE');
        Write(arq_baruel, '03857930000154');
        Write(arq_baruel, QArquivoBaruelSUCHBEGRIFF.AsString);
        Write(arq_baruel, '         ');
        Write(arq_baruel, '   ');
        dataexped := copy((FormatDateTime('dd/mm/yyyy', date)), 1, 2) +
          copy((FormatDateTime('dd/mm/yyyy', date)), 4, 2) +
          copy((FormatDateTime('dd/mm/yyyy', date)), 7, 4) +
          copy(TimeToStr(now), 1, 2) + copy(TimeToStr(now), 4, 2) +
          copy(TimeToStr(now), 7, 2);
        Write(arq_baruel, dataexped);

        znf := 6 - Length(QArquivoBaruelNR_AUF.AsString);
        if Length(QArquivoBaruelNR_AUF.AsString) < 6 then
          Write(arq_baruel, StringOfChar('0', znf) +
            QArquivoBaruelNR_AUF.AsString)
        else
          Write(arq_baruel, QArquivoBaruelNR_AUF.AsString);
        Writeln(arq_baruel, '');
        QArquivoBaruel.Next;
      End;
      closeFile(arq_baruel);
      QUpdateRomaneio.close;
      QUpdateRomaneio.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
      QUpdateRomaneio.ExecSQL;
    end;
  end;
end;

procedure TfrmGeraRoma.btnNovoManifestoClick(Sender: TObject);
begin
  pnManifesto.Enabled := True;
  btnNovoManifesto.Enabled := FALSE;
  btFatura.Enabled := True;
  btnLimpaRomaneio.Enabled := FALSE;
  btnImpressao.Enabled := FALSE;
  dbgFatura.Enabled := FALSE;

  if edMotorista.CanFocus then
    edMotorista.SetFocus;
end;

procedure TfrmGeraRoma.btnRetornaClick(Sender: TObject);
begin
  While not QTarefa.eof do
  begin

    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('select nr_auf_rahmen from auftraege ');
    Query1.sql.add('where nr_auf = :0 and lager = :1 and id_klient = ' +
      QuotedStr(cbCliente.Text));
    Query1.Parameters[0].value := QTarefaNR_AUF.AsString;
    Query1.Parameters[1].value := cbSite.Text;
    Query1.open;
    iRecDelete := Query1.FieldByName('nr_auf_rahmen').AsString;
    mdTemp.locate('pedido', Query1.FieldByName('nr_auf_rahmen').AsString, []);
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      QTarefaSEQ_TOUR.AsInteger;
    mdTemp.edit;
    mdTempInserida.value := 0;
    mdTemp.post;

    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set percent = null ');
    Query1.sql.add('where nr_auf = :0 and lager = :1 and id_klient = ' +
      QuotedStr(cbCliente.Text));
    Query1.Parameters[0].value := QTarefaNR_AUF.AsString;
    Query1.Parameters[1].value := cbSite.Text;
    Query1.ExecSQL;
    Query1.close;
    QTarefa.Next;
  end;
  QTarefa.close;
end;

procedure TfrmGeraRoma.btnVoltaPedidoClick(Sender: TObject);
begin
  iRecDelete := mdTarefaNR_AUF.AsString;
  mdTemp.locate('pedido', QTarefaNR_AUF.AsString, []);
  edTotVolumeTarefa.value := edTotVolumeTarefa.value -
    QTarefaSEQ_TOUR.AsInteger;
  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.post;
  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('update auftraege set percent = null ');
  Query1.sql.add('where nr_auf = :0 and lager = :1 and id_klient = ' +
    QuotedStr(cbCliente.Text));
  Query1.Parameters[0].value := QTarefaNR_AUF.AsString;
  Query1.Parameters[1].value := cbSite.Text;
  Query1.ExecSQL;
  Query1.close;
  CarregaBtn();
end;

procedure TfrmGeraRoma.btRomaneioClick(Sender: TObject);
var
  irom: Integer;
  range, roma: string;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  roma := '';
  if mdTarefa.RecordCount = 0 then
    Exit;
  range := QTarefa.sql[15];


  mdTarefa.First;
  while not mdTarefa.eof do
  begin
    if (mdTarefatem.value = -1) and (mdTarefapalete.Value <=0) then
    begin
      showMessage('Existem Destinat�rios que precisam da quantidade de Palete, favor informar !!');
      exit;
    end;
    mdTarefa.Next;
  end;

  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('select distinct p.id_spediteur from auftraege p ');
  Query1.sql.add('where p.stat in(95) and p.id_klient = ' +
    QuotedStr(cbCliente.Text));
  Query1.sql.add('and p.lager = ' + QuotedStr(cbSite.Text) +
    '  and p.opid_bearb is null ');
  Query1.sql.add(range);
  Query1.open;
  if Query1.RecordCount > 1 then
  begin
    if Application.Messagebox
      ('Existe mais de 1 transportador deseja mesmo fazer o romaneio ?',
      'Gerar Romaneio', MB_YESNO + MB_ICONQUESTION) = IDNO then
      Exit
    else
      roma := 'OK';
  end;
  if roma = '' then
  begin
    if Application.Messagebox('Gerar Romaneio agora ?', 'Gerar Romaneio',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
      roma := 'OK';
  end;
  if roma = 'OK' then
  begin
    Screen.Cursor := crHourGlass;
    mdTarefa.First;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('select nvl(max(percent)+1,1) roma from auftraege ');
    Query1.open;
    irom := Query1.FieldByName('roma').AsInteger;
    while not mdTarefa.eof do
    begin
      Query1.close;
      Query1.sql.clear;
      Query1.sql.add('update auftraege set percent = :0 ');
      Query1.sql.add('where nr_auf = :1 and id_klient = ' +
        QuotedStr(cbCliente.Text));
      Query1.Parameters[0].value := irom;
      Query1.Parameters[1].value := mdTarefaNR_AUF.AsString;
      Query1.ExecSQL;
      // Cadastrar palete do Romaneio
      if mdTarefapalete.Value > 0 then
      begin
        dtmdados.iQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('insert into tb_palet_romaneio  (numero, ');
        dtmdados.IQuery1.sql.add('cnpj_destino, cnpj_cliente, qt_palete, tipo, ');
        dtmdados.IQuery1.sql.add('fl_empresa, usuario) ');
        dtmdados.IQuery1.sql.add('values (:0, :1, :2, :3, ''R'', :4, :5)');
        dtmdados.IQuery1.Parameters[0].value := irom;
        dtmdados.IQuery1.Parameters[1].value := mdTarefadestino.AsString;
        dtmdados.IQuery1.Parameters[2].value := mdTarefacliente.AsString;
        dtmdados.IQuery1.Parameters[3].value := mdTarefapalete.AsInteger;
        dtmdados.IQuery1.Parameters[4].value := GLBFilial;
        dtmdados.IQuery1.Parameters[5].value := GLBUSER;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.iQuery1.close;
      end;

      mdTarefa.Next;
    end;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage(' Romaneio ' + IntToStr(irom) + ' Gerado');
    PageControl1.TabIndex := 1;
    mdTarefa.close;
    CarregaRomaneio();
    CarregaRomaneioManifesto();
  end;
end;

procedure TfrmGeraRoma.CarregaBtn;
begin
  starefa := '';
  starefa := RetornaTarefasSelecionadas();
  if (starefa = '') then
    starefa := QuotedStr('0');
  QTarefa.close;
  QTarefa.sql[13] := ' and p.id_klient = ' + QuotedStr(cbCliente.Text);
  QTarefa.sql[14] := ' and p.lager = ' + QuotedStr(cbSite.Text);
  if cbCliente.Text = 'BRSP' then
  begin
    // QTarefa.sql[0] := 'SELECT distinct p.nr_auf_rahmen NR_AUF, t.name transportadora, p.percent, d.name, ';
    QTarefa.sql[15] := ' and p.nr_auf_rahmen in (' + starefa + ')';
  end
  else
  begin
    // QTarefa.sql[0] := 'SELECT distinct p.NR_AUF, t.name transportadora, p.percent, d.name, ';
    QTarefa.sql[15] := ' and p.nr_auf in (' + starefa + ')';
  end;
  QTarefa.open;
  QTarefa.DisableControls;
  edTotVolumeTarefa.value := 0;
  mdTarefa.Close;
  mdTarefa.Open;
  while not QTarefa.eof do
  begin
    mdTarefa.Append;
    mdTarefaNR_AUF.Value := QTarefaNR_AUF.Value;
    mdTarefaNAME.Value := QTarefaNAME.Value;
    mdTarefaTRANSPORTADORA.Value := QTarefaTRANSPORTADORA.Value;
    mdTarefaPERCENT.Value := QTarefaPERCENT.Value;
    mdTarefaSEQ_TOUR.Value := QTarefaSEQ_TOUR.Value;
    mdTarefacliente.Value := QTarefacliente.Value;
    mdTarefadestino.Value := QTarefadestino.Value;
    mdTarefapalete.value := QTarefaqt_palete.AsInteger;

    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.clear;
    dtmdados.RQuery1.sql.add('select count(*) sim from rodcli where fnc_cnpj(codcgc) = :0');
    dtmdados.RQuery1.sql.add('and upper(titele) = ''RETIRA COM PALETE'' ');
    dtmdados.RQuery1.Parameters[0].Value := QTarefadestino.AsString;
    dtmdados.RQuery1.open;
    if dtmdados.RQuery1.FieldByName('sim').Value = 1 then
      mdTarefatem.Value := -1;
    mdTarefa.Post;

    if (mdTarefapalete.value = 0) and (mdTarefatem.Value = -1) then
    begin
      // verifca se o destino precisa de palete
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add('select fnc_utiliza_pallet(:0, :1) tem from dual');
      dtmdados.IQuery1.Parameters[0].value := QTarefacliente.AsString;
      dtmdados.IQuery1.Parameters[1].value := QTarefadestino.AsString;
      dtmdados.IQuery1.open;
      if dtmdados.IQuery1.FieldByName('tem').value > 0 then
      begin
        showmessage('Este Destino para este Cliente solicita entrega em Palete, favor informar');
        mdTarefa.Edit;
        mdTarefatem.Value := -1;
        mdTarefa.Post;
      end
      else
      begin
        mdTarefa.Edit;
        mdTarefatem.Value := 0;
        mdTarefa.Post;
      end;
    end;

    dtmdados.RQuery1.close;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      QTarefaSEQ_TOUR.AsInteger;
    QTarefa.Next;
  end;
  QTarefa.First;
  QTarefa.EnableControls;
  QTarefa.close;
  // Qtarefa.sql.savetofile('c:\sim\sql.text');

  mdTemp.First;
end;

procedure TfrmGeraRoma.CarregaRomaneio();
begin
  CarregaTarefas();
  mdTemp.close;
  mdTemp.open;
  // carrega em temp tarefas n�o romaneadas
  QDestino.First;
  while not QDestino.eof do
  begin
    mdTemp.append;
    mdTempInserida.value := QDestinoINSERIDA.AsInteger;
    mdTemppedido.value := QDestinoNR_AUF.AsString;
    mdTemp.post;
    QDestino.Next;
  end;
  QDestino.close;
  CarregaBtn();
  mdTemp.First;
end;

procedure TfrmGeraRoma.CarregaRomaneioManifesto;
begin
  mdTempromaneio.close;
  mdTempromaneio.open;
  qrRomaneio.close;
  qrRomaneio.open;
  qrRomaneio.First;
  while not qrRomaneio.eof do
  begin
    mdTempromaneio.append;
    mdTempromaneioNR_ROMANEIO.value := qrRomaneioPERCENT.value;
    // mdTempromaneioDESTINO.Value     := '';
    // mdTempRomaneioDEMISSAO.Value    := '';
    mdTempromaneiopeso.value := qrRomaneioTTVOL.AsInteger;
    mdTempromaneioSUSUARIO.value := qrRomaneioTRANSPORTADORA.value;
    mdTempromaneiocod_transp.AsString := qrRomaneioID_SPEDITEUR.AsString;
    // mdTempromaneioPLACA.AsString    := '';
    // mdTempromaneioVEICULO.AsString  := '';
    mdTempromaneio.post;
    qrRomaneio.Next;
  end;
end;

procedure TfrmGeraRoma.CarregaTarefas();
begin
  if (ckRota.Checked = FALSE) then
  begin
    QDestino.close;
    QDestino.sql[5] := ' ';
    // showmessage(QDestino.SQL.text);
    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTemppedido.value := QDestinoNR_AUF.AsString;
      mdTempInserida.value := 1; // QDestinoINSERIDA.AsInteger;
      QDestino.Next;
    end;
    mdTemp.First;
  end
  else
  begin
    QDestino.close;
    QDestino.sql[5] := ' and p.id_spediteur = :rota ';
    QDestino.Parameters[0].value := qrRotaID_SPEDITEUR.AsString;
    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTemppedido.value := QDestinoNR_AUF.AsString;
      mdTempInserida.value := 1; // QDestinoINSERIDA.AsInteger;
      QDestino.Next;
    end;
    mdTemp.First;
  end;
end;

procedure TfrmGeraRoma.cbClienteChange(Sender: TObject);
var
  starefa_1: string;
begin
  if cbCliente.Text <> '' then
  begin
    qrRota.close;
    qrRota.sql[3] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
    qrRota.sql[4] := 'and t.lager = ' + QuotedStr(cbSite.Text);
    qrRota.open;
    qrRota.First;
    CarregaRomaneio();
    pgTarefas.ActivePageIndex := 0;
    reg := 0;
    PageControl1.TabIndex := 0;
    starefa_1 := RetornaTarefasSelecionadas();
    if (starefa_1 = '') then
      starefa_1 := QuotedStr('0');
    QTarefa.close;
    // qTarefa.sql[4] := ' and p.id_klient = ' + QuotedStr(cbCliente.Text);
    if cbCliente.Text = 'BRSP' then
      QTarefa.sql[15] := ' and p.nr_auf_rahmen in (' + starefa_1 + ')'
    else
      QTarefa.sql[15] := ' and p.nr_auf in (' + starefa_1 + ')';
    QTarefa.open;
    mdTemp.First;
    PageControl1.ActivePage := TabSheet4;

    if cbCliente.Text = 'BRSP' then
    begin
      btnVoltaPedido.Visible := FALSE;
      btnRetorna.Visible := True;
    end
    else
    begin
      btnVoltaPedido.Visible := True;
      btnRetorna.Visible := FALSE;
    end;

  end;
end;

procedure TfrmGeraRoma.ckRotaClick(Sender: TObject);
begin
  if (ckRota.Checked = FALSE) then
  begin
    tbRota.TabVisible := FALSE;
    pgTarefas.ActivePageIndex := 1;
  end
  else
  begin
    pgTarefas.ActivePageIndex := 0;
    tbRota.TabVisible := True;
  end;
end;

procedure TfrmGeraRoma.dbgColetaCellClick(Column: TColumn);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if (mdTempnr_romaneio.AsInteger > 0) then
  begin
    ShowMessage('Este Pedido j� esta Romaneado');
    Exit;
  end;
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    mdTempreg.value := reg;
    mdTempInserida.value := 1;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      mdTempromaneiopeso.AsFloat;
  end
  else
  begin
    mdTempInserida.value := 0;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      mdTempromaneiopeso.AsFloat;
  end;
  mdTemp.post;
end;

procedure TfrmGeraRoma.dbgColetaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgColeta.Canvas.FillRect(Rect);
      iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraRoma.dbgFaturaCellClick(Column: TColumn);
begin
  if (Column <> dbgFatura.Columns[0]) then
    abort;

  if (pnManifesto.Enabled) then
  begin
    ShowMessage(' Grave ou Cancele o Manifesto ');
    if edVcto.CanFocus then
      edVcto.SetFocus;
    Exit;
  end;

  if (Column = dbgFatura.Columns[0]) then
  begin
    mdTempromaneio.edit;
    if (mdTempromaneioSELECIONADO.IsNull) or
      (mdTempromaneioSELECIONADO.value = 0) then
    begin
      mdTempromaneioSELECIONADO.value := 1;
      mdTempromaneioselecionou.AsInteger := 1;
    end
    else
    begin
      mdTempromaneioSELECIONADO.value := 0;
      mdTempromaneioselecionou.AsInteger := 0;
    end;
    mdTempromaneio.post;
    btnNovoManifesto.Enabled := (mdTempromaneioSELECIONADO.value = 1);
    btnLimpaRomaneio.Enabled := btnNovoManifesto.Enabled;
  end;
end;

procedure TfrmGeraRoma.dbgFaturaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdTempromaneioSELECIONADO) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    //
    if mdTempromaneioSELECIONADO.value = 1 then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;

procedure TfrmGeraRoma.dgTarefaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (mdTarefatem.value = -1) and (mdTarefapalete.Value <=0) then
  begin
    dgTarefa.canvas.Brush.color := clRed;
    dgTarefa.canvas.font.color := clWhite;
  end;

  dgTarefa.DefaultDrawDataCell(Rect, dgTarefa.Columns[DataCol].field, State);
end;

procedure TfrmGeraRoma.dsTempRomaneioDataChange(Sender: TObject; Field: TField);
begin
  btnNovoManifesto.Enabled := (mdTempromaneioSELECIONADO.AsInteger = 1) and
    ((not mdTempromaneioNR_ROMANEIO.IsNull));
  btnNovoManifesto.Enabled := btnNovoManifesto.Enabled;
end;

procedure TfrmGeraRoma.Edit1Exit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
begin
  pgTarefas.ActivePageIndex := 1;
  if (Alltrim(Edit1.Text) = '') then
    Exit;
  qrPesquisa := TADOQuery.Create(Nil);
  with qrPesquisa, sql do
  begin
    Connection := dtmDados.ConWmsWeb;
    add('select percent from auftraege t where t.nr_auf = :pedido and t.id_klient = :1');
    Parameters[0].value := Edit1.Text;
    Parameters[1].value := cbClienteI.Text;
    open;
  end;

  if (qrPesquisa.RecordCount = 0) then
  begin
    ShowMessage('NF n�o Encontrada !!');
    FreeAndNil(qrPesquisa);
    Exit;
  end
  else
  begin
    QImpressos.locate('nr_romaneio', qrPesquisa.FieldByName('percent')
      .value, []);
  end;

end;

procedure TfrmGeraRoma.edPlacaExit(Sender: TObject);
begin
  if edPlaca.Text <> '' then
  begin

    if Length(Alltrim(copy(edPlaca.Text, 1, 3))) <> 3 then
    begin
      ShowMessage('Placa sem as letras completas !!');
      edPlaca.SetFocus;
      Exit;
    end;

    if Length(Alltrim(copy(edPlaca.Text, 4, 4))) <> 4 then
    begin
      ShowMessage('Placa sem os n�meros completos !!');
      edPlaca.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmGeraRoma.edValorExit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
  svalor: string;
begin
  pgTarefas.ActivePageIndex := 1;
  if (Alltrim(edValor.Text) = '') then
    Exit;
  svalor := edValor.Text;
  qrPesquisa := TADOQuery.Create(Nil);
  with qrPesquisa, sql do
  begin
    Connection := dtmDados.ConWmsWeb;
    add('select nr_auf from auftraege t where t.nr_auf = :pedido ');
    Parameters[0].value := edValor.Text;
    open;
  end;

  if (qrPesquisa.RecordCount = 0) then
  begin
    ShowMessage('Pedido n�o Encontrado !!');
    FreeAndNil(qrPesquisa);
    Exit;
  end;
  edValor.Text := qrPesquisa.FieldByName('nr_auf').AsString;
  edValor.Text := svalor;
end;

procedure TfrmGeraRoma.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qrRomaneio.close;
  QMapaItens.close;
  QDestino.close;
  Query1.close;
  QTarefa.close;
  mdTemp.close;
  mdTempromaneio.close;
  Action := caFree;
end;

procedure TfrmGeraRoma.FormCreate(Sender: TObject);
begin
  bInicio := True;
  PageControl1.ActivePage := TabSheet4;
  arq := TIniFile.Create(ExtractFilePath(Application.exeName) + '\SIM.ini');
  localarq := arq.ReadString('ARQ', 'caminho', '');

  cbSite.clear;
  cbSite.Items.add('');
  if glbfilial = 1 then
  begin
    cbSite.Items.add('BRR');
    cbSite.Items.add('MAR');
  end
  else if (glbfilial = 4) or (glbfilial = 12) then
    cbSite.Items.add('PBA')
  else if (glbfilial = 16) or (glbfilial = 20) then
    cbSite.Items.add('MAR')
  else if (glbfilial = 21) then
    cbSite.Items.add('ETM')
  else
    cbSite.Items.add('BTM');
end;

procedure TfrmGeraRoma.JvDBUltimGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBUltimGrid1.Columns.Items
    [JvDBUltimGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QImpressos.locate(JvDBUltimGrid1.Columns.Items
    [JvDBUltimGrid1.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBUltimGrid1.Columns.Items[JvDBUltimGrid1.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmGeraRoma.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QImpressosTMS.value = 'S') then
  begin
    JvDBUltimGrid1.Canvas.Font.Color := clWhite;
    JvDBUltimGrid1.Canvas.Brush.Color := clGreen;
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    JvDBUltimGrid1.DefaultDrawDataCell(Rect, Column.Field, State);
  end;

  if (QImpressosTMS.value = 'X') then
  begin
    JvDBUltimGrid1.Canvas.Font.Color := clBlack;
    JvDBUltimGrid1.Canvas.Brush.Color := $000080FF;
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    JvDBUltimGrid1.DefaultDrawDataCell(Rect, Column.Field, State);
  end;

  if (QImpressosTMS.value = 'P') then
  begin
    JvDBUltimGrid1.Canvas.Font.Color := clWhite;;
    JvDBUltimGrid1.Canvas.Brush.Color := clRed;
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    JvDBUltimGrid1.DefaultDrawDataCell(Rect, Column.Field, State);
  end;
end;

procedure TfrmGeraRoma.JvDBUltimGrid2CellClick(Column: TColumn);
begin
  mdAltera.edit;
  //
  if (mdAlteraReg.IsNull) or (mdAlteraReg.value = 0) then
  begin
    mdAlteraReg.value := 1;
  end
  else
  begin
    mdAlteraReg.value := 0;
  end;
  mdAltera.post;
end;

procedure TfrmGeraRoma.JvDBUltimGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdAlteraReg) then
    begin
      JvDBUltimGrid2.Canvas.FillRect(Rect);
      iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdAlteraReg.value = 1 then
        iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraRoma.LimpaCamposManifesto;
begin
  edVcto.clear;
  // edhrcolprev.Clear;
  edMotorista.clear;
  edPlaca.clear;
  edVeiculo.ItemIndex := -1;
end;

procedure TfrmGeraRoma.mdTarefapaleteChange(Sender: TField);
var a : Integer;
begin
  a := mdTarefapalete.AsInteger;
end;

procedure TfrmGeraRoma.mdTempAfterOpen(DataSet: TDataSet);
begin
  mdTemp.First;
end;

procedure TfrmGeraRoma.QDestinoBeforeOpen(DataSet: TDataSet);
begin
  QDestino.sql[3] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
  if cbCliente.Text = 'BRSP' then
  begin
    QDestino.sql[0] :=
      'select distinct p.nr_auf_rahmen nr_auf, case when nvl(p.percent,0) > 0 then 1 else 0 end as inserida ';
    QDestino.sql[4] := 'and p.lager = ' + QuotedStr(cbSite.Text) +
      ' and p.nr_auf_rahmen is not null';
  end
  else
  begin
    QDestino.sql[0] :=
      'select distinct p.nr_auf, case when nvl(p.percent,0) > 0 then 1 else 0 end as inserida ';
    QDestino.sql[4] := 'and p.lager = ' + QuotedStr(cbSite.Text);
  end;

end;

procedure TfrmGeraRoma.QImpressosBeforeOpen(DataSet: TDataSet);
begin
  if cbSite.Text = '' then
  begin
    ShowMessage('Qual o Site ?');
  end
  else
    QImpressos.Parameters[0].value := cbSite.Text;
end;

procedure TfrmGeraRoma.QMapaItensBeforeOpen(DataSet: TDataSet);
begin
  QMapaItens.sql[11] := 'where p.id_klient = ' +
    QuotedStr(qMapaCargaID_KLIENT.value) + ' and e.ID_EIGNER_1 =' +
    QuotedStr(qMapaCargaID_KLIENT.value) + 'and e.KL_EIGNER_2 = ''KU'' ';
  QMapaItens.Parameters[1].value := cbSite.Text;
  QMapaItens.Parameters[0].value := glbfilial;
end;

procedure TfrmGeraRoma.qrRomaneioBeforeOpen(DataSet: TDataSet);
begin
  qrRomaneio.sql[4] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
  qrRomaneio.sql[5] := 'and p.lager = ' + QuotedStr(cbSite.Text);
end;

procedure TfrmGeraRoma.qrRotaAfterOpen(DataSet: TDataSet);
begin
  dtmDados.WWQuery1.close;
  dtmDados.WWQuery1.sql.clear;
  dtmDados.WWQuery1.sql.add
    ('select distinct count(p.nr_auf) quant from auftraege p left join spediteure t on p.id_spediteur = t.id_spediteur ');
  dtmDados.WWQuery1.sql.add('where p.stat in(95) and p.opid_bearb is null ');
  dtmDados.WWQuery1.sql.add('and p.id_klient = ' + QuotedStr(cbCliente.Text));
  dtmDados.WWQuery1.sql.add('and p.lager = ' + QuotedStr(cbSite.Text));
  dtmDados.WWQuery1.open;
  Label2.Caption := 'Total de NF : ' + dtmDados.WWQuery1.FieldByName
    ('quant').AsString;
  dtmDados.WWQuery1.close;
end;

procedure TfrmGeraRoma.QTarefaBeforeOpen(DataSet: TDataSet);
begin
  QTarefa.Parameters[0].value := glbfilial;
end;

function TfrmGeraRoma.RetornaTarefasSelecionadas: String;
var
  sTarSelec: String;
  iCount: Integer;
  // bEntrou: Boolean;
begin
  mdTemp.open;
  mdTemp.First;
  iCount := 0;
  mdTemp.DisableControls;
  if not(QTarefa.Active) then
    QTarefa.open;
  QTarefa.First;
  while not QTarefa.eof do
  begin
    if (QTarefaNR_AUF.AsString <> iRecDelete) then
    begin
      if iCount = 0 then
        sTarSelec := QuotedStr(QTarefaNR_AUF.AsString)
      else
        sTarSelec := sTarSelec + ',' + QuotedStr(QTarefaNR_AUF.AsString);
      inc(iCount);
    end;
    QTarefa.Next;
  end;
  iRecDelete := '';

  while not mdTemp.eof do
  begin
    if mdTempInserida.value = 1 then
    begin
      if iCount = 0 then
        sTarSelec := QuotedStr(mdTemppedido.AsString)
      else
        sTarSelec := sTarSelec + ',' + QuotedStr(mdTemppedido.AsString);
      inc(iCount);
    end;
    mdTemp.Next;
  end;
  mdTemp.EnableControls;
  result := sTarSelec;
end;

procedure TfrmGeraRoma.rgTarefasClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraRoma.TabSheet1Show(Sender: TObject);
begin
  CarregaRomaneioManifesto();
  if edVcto.CanFocus then
    edVcto.SetFocus;
  pnManifesto.Enabled := FALSE;
  btFatura.Enabled := FALSE;
  mdTempromaneio.First;
end;

procedure TfrmGeraRoma.TabSheet4Show(Sender: TObject);
var
  starefa_1: String;
begin
  starefa_1 := RetornaTarefasSelecionadas();
  if (starefa_1 = '') then
    starefa_1 := QuotedStr('0');
  QTarefa.close;
  QTarefa.sql[15] := ' and p.nr_auf in (' + starefa_1 + ')';
  QTarefa.open;
  mdTemp.First;
end;

procedure TfrmGeraRoma.tbImpressoHide(Sender: TObject);
begin
  QImpressos.close;
end;

procedure TfrmGeraRoma.tbImpressoShow(Sender: TObject);
begin
  QCliente.Close;
  QCliente.sql[2] := 'where p.stat in(75,95) ';
  QCliente.sql[3] := 'and p.lager = ' + QuotedStr(cbSite.Text);
  QCliente.open;
  cbClienteI.clear;
  cbClienteI.Items.add('');
  while not QCliente.eof do
  begin
    cbClienteI.Items.add(QClienteID_KLIENT.value);
    QCliente.Next;
  end;
  QCliente.close;
  QImpressos.open;
end;

procedure TfrmGeraRoma.tbTarefaShow(Sender: TObject);
begin
  CarregaTarefas();
end;

procedure TfrmGeraRoma.cbClienteExit(Sender: TObject);
begin
  Label2.Caption := '';
end;

procedure TfrmGeraRoma.cbSiteChange(Sender: TObject);
begin
  QCliente.Close;
  QCliente.sql[2] := 'where p.stat in(75,95) and p.opid_bearb is null';
  QCliente.sql[3] := 'and p.lager = ' + QuotedStr(cbSite.Text);
  QCliente.open;
  cbCliente.clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteID_KLIENT.value);
    QCliente.Next;
  end;
  QCliente.close;
end;

procedure TfrmGeraRoma.cbTodosClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if cbTodos.Checked = True then
  begin
    while not mdTemp.eof do
    begin
      mdTemp.edit;
      reg := reg + 1;
      mdTempreg.value := reg;
      mdTempInserida.value := 1;
      edTotVolumeTarefa.value := edTotVolumeTarefa.value +
        mdTempromaneiopeso.AsFloat;
      mdTemp.Next;
    end;
    mdTemp.First;
  end
  else
  begin
    while not mdTemp.eof do
    begin
      mdTemp.edit;
      mdTempInserida.value := 0;
      edTotVolumeTarefa.value := edTotVolumeTarefa.value -
        mdTempromaneiopeso.AsFloat;
      mdTemp.Next;
    end;
    mdTemp.First;
  end;
end;

procedure TfrmGeraRoma.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

end.
