�
 TFRMCADTABELACUSTOFRAC 04 TPF0TfrmCadTabelaCustoFracfrmCadTabelaCustoFracLeft� Top� BorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaption!Cadastro de Tabela de Frete CustoClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TPageControlPageControl1Left Top Width�Heighto
ActivePage	TabSheet1AlignalClientTabOrder  	TTabSheetConsultaCaptionConsulta TJvDBUltimGriddbgContratoLeft Top Width�HeightSAlignalClient
DataSource	dtstabelaDrawingStyle
gdsClassicOptionsdgTitlesdgIndicator
dgColLines
dgRowLinesdgTabsdgCancelOnExitdgTitleClickdgTitleHotTrack TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnDrawColumnCelldbgContratoDrawColumnCell
OnDblClickdbgContratoDblClickOnTitleClickdbgContratoTitleClickAlternateRowColorclSilverAlternateRowFontColorclBlack"SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldName	COD_CUSTOTitle.AlignmenttaCenterTitle.Caption   CódigoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width&Visible	 Expanded	FieldNameNM_FORNECEDORTitle.AlignmenttaCenterTitle.CaptionTransportadorTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthYVisible	 Expanded	FieldNameLOCALTitle.AlignmenttaCenterTitle.CaptionOrigemTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthGVisible	 	AlignmenttaCenterExpanded	FieldNameDS_UFTitle.AlignmenttaCenterTitle.Caption	UF OrigemTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width;Visible	 Expanded	FieldName	LOCAL_DESTitle.AlignmenttaCenterTitle.CaptionDestinoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthDVisible	 	AlignmenttaCenterExpanded	FieldName	DS_UF_DESTitle.AlignmenttaCenterTitle.Caption
UF DestinoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVL_FRETE_MINIMOTitle.AlignmenttaCenterTitle.Caption   Frete MínimoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthRVisible	 Expanded	FieldNameVL_ADTitle.AlignmenttaCenterTitle.CaptionSeguroTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVEICULOTitle.AlignmenttaCenterTitle.Caption   VeículoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthUVisible	 Expanded	FieldNameOPERACAOTitle.AlignmenttaCenterTitle.Caption
   OperaçãoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	     	TTabSheetDetalhesCaptionDetalhes
ImageIndex TLabelLabel3LeftbTopHWidth-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel6LeftTop� Width.HeightCaptionDestinoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2Left	TopWidthVHeightCaptionTransportadorFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4LeftTopWidthMHeightCaption   Frete MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftgTopWidth4HeightCaption   PedágioFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7Left� TopWidth'HeightCaptionOutrosFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8LeftTop;WidthHHeightCaptionTx Fluvial %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9LeftgTop;Width_HeightCaptionTx. Fluv. MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10Left�TopmWidth@HeightCaption	ExcedenteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11LeftTop�Width;HeightCaptionSeguro %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel12Left� Top�WidthHeightCaptionGrisFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel13Left=Top�WidthFHeightCaption   Gris MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel14Left	TopbWidth,HeightCaptionOrigemFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel18Left=Top�WidthUHeightCaptionValor SinergiaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel19LeftgTop�WidthJHeightCaption   Seg. MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel20Left	Top3Width-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel21Left� Top� Width,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel1Left� TopdWidth,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel22LeftTopmWidthHeightCaptionTDEFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel23Left� Top;WidthHeightCaptionTRTFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel25LeftgTopmWidth+HeightCaptionTDE %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel26Left� TopmWidthJHeightCaption   TDE MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28LeftTop� Width=HeightCaption
   OperaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel30Left=TopmWidthNHeightCaption   TDE MáximoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel27LefthTop�WidthKHeightCaption   ObservaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel31Left>Top;Width+HeightCaptionTRT %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel32Left�Top;WidthJHeightCaption
TRT MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel33Left=TopWidth_HeightCaptionHrs ExcedentesFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel36LeftTop�Width2HeightCaptionSuframaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37Left�TopWidth_HeightCaption% CT-e ReceitaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel39LeftgTop�WidthZHeightCaptionTx. Port. / PesoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel40Left� Top�Width^HeightCaptionTx. Port. MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel41LeftTop
Width+HeightCaptionTDA %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel42LeftiTop
Width7HeightCaptionTDA Min.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel43Left� Top
Width;HeightCaptionTDA Max.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel47Left=TopWidth$HeightCaption   DiáriaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel50Left�Top� Width-HeightCaption   RegiãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel49Left�TopdWidth-HeightCaption   RegiãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TJvMaskEditedTransLeft	TopWidthFHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder%Text      TJvMaskEdit	edVeiculoLeft	TopGWidthFHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder&Text      	TComboBox	cbVeiculoLeft	TopGWidthFHeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible  	TComboBoxcbtraLeft	TopWidthFHeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder Visible  TPanelPanel1LeftWTop Width6Height�ColorclSilverTabOrder$ TPageControlPageControl2LeftTopWidth4Height�
ActivePagetsFracionadoAlignalClientTabOrder  	TTabSheettsFracionadoCaption
Fracionado TPanelPanel4Left Top Width,Height$AlignalTopTabOrder Visible TJvDateEditedDataILeftTopWidthAHeight
ShowButtonShowNullDateTabOrder VisibleOnExitedDataIExit  TJvDateEditedDataFLeft[TopWidthAHeight
ShowButtonShowNullDate
YearDigitsdyTwoTabOrderVisibleOnExitedDataFExit  TJvCalcEditEdValorLeft� TopWidth6HeightDisplayFormat,0.00
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown  TJvCalcEditedCtrcLeft� TopWidth6HeightDisplayFormat,0.00
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown  TJvCalcEditedPesoiLeftTopWidth;HeightDecimalPlacesDisplayFormat0.000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShownOnExitedPesoiExit  TJvCalcEditedPesofLeft^TopWidth3HeightDecimalPlacesDisplayFormat0.000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShownOnExitedPesofExit  TJvCalcEditedExcedLeft�TopWidth5HeightDecimalPlacesDisplayFormat0.0000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShownOnExitedExcedExit  TJvCalcEditedMinimoLeft�TopWidth6HeightDisplayFormat,0.00
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown   TJvDBUltimGridDBGrid1Left Top$Width,HeightaAlignalClient
DataSourcedtsItemDrawingStyle
gdsClassicOptionsdgTitlesdgIndicator
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExitdgTitleClickdgTitleHotTrack TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnDrawColumnCellDBGrid1DrawColumnCellAlternateRowColorclMoneyGreen"SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldNameDATAITitle.AlignmenttaCenterTitle.CaptionData InicialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthJVisible	 Expanded	FieldNameDATAFTitle.AlignmenttaCenterTitle.Caption
Data FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthCVisible	 Expanded	FieldNameVL_RODOVIARIOTitle.AlignmenttaCenterTitle.CaptionFreteTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width7Visible	 Expanded	FieldNameVR_CTRCTitle.AlignmenttaCenterTitle.CaptionTx CT-eTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width8Visible	 Expanded	FieldName
NR_PESO_DETitle.AlignmenttaCenterTitle.CaptionPeso IncialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameNR_PESO_ATETitle.AlignmenttaCenterTitle.Caption
Peso FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVR_EXCEDTitle.AlignmenttaCenterTitle.CaptionPeso Exced.Title.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width?Visible	 Expanded	FieldName	vl_minimoTitle.AlignmenttaCenterTitle.Caption   Valor MínimoTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	    TPanelPanel3Left Top�Width,Height$AlignalBottomTabOrder TDBNavigatorDBNavigator1LeftTopWidth� HeightVisibleButtonsnbFirstnbPriornbNextnbLast TabOrder   TBitBtnbtnInserirILeft� TopWidthBHeightHintIncluir ItensCaptionInserir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � A�N =�I :�D 6�@ 2�< /�7 ,�3 (0 � � � � � � � � � � � � � � I�X E�S A�P } �Ц �Ϧ x�� 6�B ,�4 )0 � � � � � � � � � � Q�a M�] d�x �۵ �̘ f�} d�| �˘ �ٴ X�k ,�4 )0 � � � � � � Y�k V�f j�} �۲ `�w \�s Y�p Y�o X�o [�t �ٳ Z�l ,�4 )0 � � � � ]�p S�h �ݴ d�y _�q `�w ��� ��� Y�p X�n \�t �ڴ 8�C ,�4 � � � � a�u �̘ �Ӗ k�z c�p U�e ��� ��� Y�p Y�p [�r �̗ {�� 0�9 � � � � e�z �ݳ }ϊ ú ��� ��� ��� ��� ��� ��� Y�p g�} �ԫ 4�= � � � � i�~ �� �՗ zɆ ��� ��� ��� ��� ��� ��� Y�p i�~ �Ԫ 8�B � � � � m�� �ݶ �߯ �ˌ |Ɇ n�y ��� ��� [�j `�w \�s �љ �Œ <�G � � � � p�� �Ǘ ��� �٠ �Ӕ ȉ ��� ��� yͅ k�| o�~ �ߵ E�W @�L � � � � s�� p�� �ڷ ��� �؝ �͓ �̎ �Ԗ �ԕ �Ҏ �� k�} H�V D�Q � � � � � � s�� p�� �ܻ ��� �� �ۥ �٠ �߯ ��� y P�_ L�[ � � � � � � � � � � s�� q�� �Τ ��� ��� ��� �ݸ m� X�i T�e � � � � � � � � � � � � � � t�� q�� n�� j�� g�| c�w `�s \�n � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnInserirIClick  TBitBtn
btnsalvarILeft� TopWidthBHeightHintSalvar ItensCaptionSalvarEnabled
Glyph.Data
:  6  BM6      6   (                                   �j6��i5��i5�h5��h5��g4��f4��e3��d3��c2��b2��a2��`1��`1��`1�a1ĺj5��ƭ��ŭ�����������������������������������������Ț|�ǘy��`1��k7��ʳ��z�����b���b���b���b���b���b���b���b�������ʍe�ɛ|��`1��l8��̶��z�����������������������������������������͐h�̞���a2��k8��θ��y�����b���b���b���b���b���b���b���b�������ϓj�Σ���a2��j6��л��z�����������������������������������������Ӗm�ҧ���b2��j6��Ҿ��z��z��z��{��{��x�ޟw�ݟv�ܝt�ٛr�ؙq�֙p�ի���c3��j6������z��z��{��{��{��y��x�ޠw�ޞu�ܝt�ڛs�ٛs�ڰ���d3��j6������{��z��z��{��{��{��y�ߠw�ޟv�ݞt�ۜr�ܝt�ݵ���e4��k6������}�Ȍd�ɍe�Ɏg�˒l�˒m�ʐi�Ȍe�Ȍd�Ȍd�Ȍd�ڜt�ẟ��f4��k6������}�����������������������������������������ޠw�侤��g4��k6������~���������ɍf������������������������������x��©��h5��k6������~���������Ȍd������������������������������z������i5��k6�����骀���������Ȍd������������������������������к��н��p>��k6��������������������������������������������������н�ɉ^�i5c�k6q�k6��k6̼k6�k6��k6��k6��j6��j6��l9��n;��m:��k8�p>˶i5T��� ParentShowHintShowHint	TabOrderOnClickbtnsalvarIClick  TBitBtnbtnAlterarILeft(TopWidthBHeightHintAlterar ItensCaptionAlterar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � �  }!�{� y� � � � � � � � @X�B^�%i��,v��;���� � � � � � � � �+�C�_� {� y� � � � � � BY�]�����������H���!�R��I��C��;�:�^�����F�b� }� y� � � � m������a���@���g��)�[��ʩ��ȥ��ơ��Ş�j�����H�f� }!� y	� � m����������4}��1���1�c��ͭ�o���k���f���a���g�����<�\� %�� �  c� &�������g���`���7�k��ΰ��ͭ��˪��˨�t����ǡ�F�h��5��-� � � � � � &�����������=�o�7�o�4�i�0�c�U�|��˪�O�t��E��=� � � � � � � � � � '�����������`���D�������4�m�Z���(�W�� � � � � � � � � � � � � � � � /�����������`���D���>�v�1�e�;���� � � � � � � � � � � � � � � � � � /�����������`���D�������]���;���� � � � � � � � � � � � � � � � � � /�����������h���o���Y���s���O���� � � � � � � � � � � � � � � � � � /�����������l���������������S���� � � � � � � � � � � � � � � � � � /�����������������������O���� � � � � � � � � � � � � � � � � � � � P���j�����������B���1�®� � � � � � � � � � � � � � � � � � � � /��	O���P���N���/���5��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAlterarIClick  TBitBtnbtnExcluirILeftjTopWidthBHeightHintExcluir ItemCaptionExcluir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ?T�#:P�}8S��5Q��0K��.N��+L�}'H�#� � � � � � � � � � � � � � FX�SBU��<R��uz����������qx��3M��+J��)K�S� � � � � � � � � � MZ�SIY��\e������~���[c��Y]��}�������Q]��+J��)K�S� � � � � � T_�"Sa��ak������T_��P\��MY��NY��LV��PV������T`��*J��)K�"� � � � X`�~KV������Vd��Rf��MY��MY��MY��MY��LX��RZ������4P��*J�~� � � � \b�ہ���~���]s��MY��MY��MY��MY��MY��MY��O[��{���u{��.K��� � � � _c������p���h���������������������������MY��\f������2P��� � � � di���������p���������������������������MY��^j������6O��� � � � gj�ۥ�������w���T_��T_��T_��T_��T_��T_��cw������x��:S��� � � � ji�~}�����������~���u���l���l���l���l���cy������>O��>T�~� � � � ll�"ji�壧�������������z������~���v�������cn��EW��AV�"� � � � � � ml�Sji������������������������������p{��L[��HX�S� � � � � � � � � � ml�Sjj�掓������������������fp��S^��P]�S� � � � � � � � � � � � � � mm�#kj�}hj��cd��ad��]c��[c�}Xb�#� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnExcluirIClick  TBitBtnbtnCancelarILeft�TopWidthFHeightHintCancelar ItemCaptionCancelarEnabled
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnCancelarIClick    	TTabSheettsAereoCaption   Taxa Rodov. Aéreo
ImageIndexOnHidetsAereoHideOnShowtsAereoShow TPanelPanel10Left Top Width,Height$AlignalTopTabOrder Visible TJvDateEditedDataAereoILeftTopWidth@Height
ShowButtonShowNullDate
YearDigitsdyFourTabOrder Visible  TJvDateEditedDataAereoFLeftYTopWidth@Height
ShowButtonShowNullDate
YearDigitsdyFourTabOrderVisibleOnExitedDataAereoFExit  TJvCalcEditedAereoColetaLeft� TopWidthMHeightDisplayFormat,0.00
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown  TJvCalcEditedAereoEntregaLeft� TopWidthLHeightDecimalPlacesDisplayFormat,0.0000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown  TJvCalcEditedPesoAereoILeftETopWidthIHeightDisplayFormat,0.00
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown  TJvCalcEditedPesoAereoFLeft�TopWidth=HeightDecimalPlacesDisplayFormat0.000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShownOnExitedPesoAereoFExit  TJvCalcEdit
edAereoExcLeft�TopWidth?HeightDecimalPlacesDisplayFormat0.000
ShowButtonTabOrderVisibleDecimalPlacesAlwaysShown   TPanelPanel11Left Top�Width,Height$AlignalBottomTabOrder TDBNavigatorDBNavigator2LeftTopWidth� Height
DataSourcedtsAereoVisibleButtonsnbFirstnbPriornbNextnbLast TabOrder   TBitBtn	btnAereoILeft� TopWidthBHeightHintIncluir ItensCaptionInserir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � A�N =�I :�D 6�@ 2�< /�7 ,�3 (0 � � � � � � � � � � � � � � I�X E�S A�P } �Ц �Ϧ x�� 6�B ,�4 )0 � � � � � � � � � � Q�a M�] d�x �۵ �̘ f�} d�| �˘ �ٴ X�k ,�4 )0 � � � � � � Y�k V�f j�} �۲ `�w \�s Y�p Y�o X�o [�t �ٳ Z�l ,�4 )0 � � � � ]�p S�h �ݴ d�y _�q `�w ��� ��� Y�p X�n \�t �ڴ 8�C ,�4 � � � � a�u �̘ �Ӗ k�z c�p U�e ��� ��� Y�p Y�p [�r �̗ {�� 0�9 � � � � e�z �ݳ }ϊ ú ��� ��� ��� ��� ��� ��� Y�p g�} �ԫ 4�= � � � � i�~ �� �՗ zɆ ��� ��� ��� ��� ��� ��� Y�p i�~ �Ԫ 8�B � � � � m�� �ݶ �߯ �ˌ |Ɇ n�y ��� ��� [�j `�w \�s �љ �Œ <�G � � � � p�� �Ǘ ��� �٠ �Ӕ ȉ ��� ��� yͅ k�| o�~ �ߵ E�W @�L � � � � s�� p�� �ڷ ��� �؝ �͓ �̎ �Ԗ �ԕ �Ҏ �� k�} H�V D�Q � � � � � � s�� p�� �ܻ ��� �� �ۥ �٠ �߯ ��� y P�_ L�[ � � � � � � � � � � s�� q�� �Τ ��� ��� ��� �ݸ m� X�i T�e � � � � � � � � � � � � � � t�� q�� n�� j�� g�| c�w `�s \�n � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAereoIClick  TBitBtn	btnAereoSLeft� TopWidthBHeightHintSalvar ItensCaptionSalvarEnabled
Glyph.Data
:  6  BM6      6   (                                   �j6��i5��i5�h5��h5��g4��f4��e3��d3��c2��b2��a2��`1��`1��`1�a1ĺj5��ƭ��ŭ�����������������������������������������Ț|�ǘy��`1��k7��ʳ��z�����b���b���b���b���b���b���b���b�������ʍe�ɛ|��`1��l8��̶��z�����������������������������������������͐h�̞���a2��k8��θ��y�����b���b���b���b���b���b���b���b�������ϓj�Σ���a2��j6��л��z�����������������������������������������Ӗm�ҧ���b2��j6��Ҿ��z��z��z��{��{��x�ޟw�ݟv�ܝt�ٛr�ؙq�֙p�ի���c3��j6������z��z��{��{��{��y��x�ޠw�ޞu�ܝt�ڛs�ٛs�ڰ���d3��j6������{��z��z��{��{��{��y�ߠw�ޟv�ݞt�ۜr�ܝt�ݵ���e4��k6������}�Ȍd�ɍe�Ɏg�˒l�˒m�ʐi�Ȍe�Ȍd�Ȍd�Ȍd�ڜt�ẟ��f4��k6������}�����������������������������������������ޠw�侤��g4��k6������~���������ɍf������������������������������x��©��h5��k6������~���������Ȍd������������������������������z������i5��k6�����骀���������Ȍd������������������������������к��н��p>��k6��������������������������������������������������н�ɉ^�i5c�k6q�k6��k6̼k6�k6��k6��k6��j6��j6��l9��n;��m:��k8�p>˶i5T��� ParentShowHintShowHint	TabOrderOnClickbtnAereoSClick  TBitBtn	btnAereoALeft,TopWidthBHeightHintAlterar ItensCaptionAlterar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � �  }!�{� y� � � � � � � � @X�B^�%i��,v��;���� � � � � � � � �+�C�_� {� y� � � � � � BY�]�����������H���!�R��I��C��;�:�^�����F�b� }� y� � � � m������a���@���g��)�[��ʩ��ȥ��ơ��Ş�j�����H�f� }!� y	� � m����������4}��1���1�c��ͭ�o���k���f���a���g�����<�\� %�� �  c� &�������g���`���7�k��ΰ��ͭ��˪��˨�t����ǡ�F�h��5��-� � � � � � &�����������=�o�7�o�4�i�0�c�U�|��˪�O�t��E��=� � � � � � � � � � '�����������`���D�������4�m�Z���(�W�� � � � � � � � � � � � � � � � /�����������`���D���>�v�1�e�;���� � � � � � � � � � � � � � � � � � /�����������`���D�������]���;���� � � � � � � � � � � � � � � � � � /�����������h���o���Y���s���O���� � � � � � � � � � � � � � � � � � /�����������l���������������S���� � � � � � � � � � � � � � � � � � /�����������������������O���� � � � � � � � � � � � � � � � � � � � P���j�����������B���1�®� � � � � � � � � � � � � � � � � � � � /��	O���P���N���/���5��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAereoAClick  TBitBtn	btnAereoELeftnTopWidthBHeightHintExcluir ItemCaptionExcluir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ?T�#:P�}8S��5Q��0K��.N��+L�}'H�#� � � � � � � � � � � � � � FX�SBU��<R��uz����������qx��3M��+J��)K�S� � � � � � � � � � MZ�SIY��\e������~���[c��Y]��}�������Q]��+J��)K�S� � � � � � T_�"Sa��ak������T_��P\��MY��NY��LV��PV������T`��*J��)K�"� � � � X`�~KV������Vd��Rf��MY��MY��MY��MY��LX��RZ������4P��*J�~� � � � \b�ہ���~���]s��MY��MY��MY��MY��MY��MY��O[��{���u{��.K��� � � � _c������p���h���������������������������MY��\f������2P��� � � � di���������p���������������������������MY��^j������6O��� � � � gj�ۥ�������w���T_��T_��T_��T_��T_��T_��cw������x��:S��� � � � ji�~}�����������~���u���l���l���l���l���cy������>O��>T�~� � � � ll�"ji�壧�������������z������~���v�������cn��EW��AV�"� � � � � � ml�Sji������������������������������p{��L[��HX�S� � � � � � � � � � ml�Sjj�掓������������������fp��S^��P]�S� � � � � � � � � � � � � � mm�#kj�}hj��cd��ad��]c��[c�}Xb�#� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAereoEClick  TBitBtn	btnAereoCLeft�TopWidthFHeightHintCancelar ItemCaptionCancelarEnabled
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAereoCClick   TJvDBUltimGridJvDBUltimGrid2Left Top$Width,HeightaAlignalClient
DataSourcedtsAereoDrawingStyle
gdsClassicOptionsdgTitlesdgIndicator
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExitdgTitleClickdgTitleHotTrack TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnDrawColumnCellJvDBUltimGrid2DrawColumnCellAlternateRowColor�ta "SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldNameDATAITitle.AlignmenttaCenterTitle.CaptionData InicialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthJVisible	 Expanded	FieldNameDATAFTitle.AlignmenttaCenterTitle.Caption
Data FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthCVisible	 Expanded	FieldNameVALOR_COLETATitle.AlignmenttaCenterTitle.CaptionValor ColetaTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthPVisible	 Expanded	FieldNameVALOR_ENTREGATitle.AlignmenttaCenterTitle.CaptionValor EntregaTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthPVisible	 Expanded	FieldNamePESO_DETitle.AlignmenttaCenterTitle.CaptionPeso InicialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthPVisible	 Expanded	FieldNamePESO_ATETitle.AlignmenttaCenterTitle.Caption
Peso FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthPVisible	 Expanded	FieldNameVR_EXCEDTitle.AlignmenttaCenterTitle.Caption	ExcedenteTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width?Visible	       	TComboBoxCBUFOLeft	TopvWidth<HeightStylecsDropDownList	ItemIndexTabOrderTextSPOnChangeCBUFOChangeItems.StringsACALAMAPBACEDFESGOMAMGMSMTPAPBPEPIPRRJRNRORRRSSCSESPTO   	TComboBoxCBLOLeftMTopwWidthjHeightStylecsDropDownList	ItemIndexTabOrderTextCAPITALItems.Strings    CAPITALINTERIOR   TJvCalcEditedFreteMLeftTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder
DecimalPlacesAlwaysShown  TJvCalcEdit	edPedagioLeftgTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedOutrosLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edOutrosRLeftTopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edOutrosMLeftgTopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedExcLeft�Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdSegLeftTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdGrisLeft� Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdGrisMLeft=Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  	TComboBoxcbUFDLeft	Top� Width<HeightStylecsDropDownList	ItemIndexTabOrderTextSPOnChangecbUFDChangeItems.StringsACALAMAPBACEDFESGOMAMGMSMTPAPBPEPIPRRJRNRORRRSSCSESPTO   	TComboBoxcbLdLeftMTop� WidthjHeightStylecsDropDownList	ItemIndexTabOrderTextCAPITALItems.Strings    CAPITALINTERIOR   TJvCalcEdit	edEntregaLeft=Top�WidthAHeightHint7Valor cobrado quando na viagem entrega em outra entregaDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  	TCheckBox	cbImpostoLeft
Top� WidthvHeightCaptionImposto InclusoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TJvCalcEditedsegminimoLeftgTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  	TCheckBoxcbPesoLeft� Top� Width� HeightCaptionAcima Peso Exced.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	  	TComboBoxcbCidadeLeft� Top� Width� HeightStylecsDropDownListDropDownCountTabOrder  	TComboBox	cbCidadeOLeft� TopvWidth� HeightStylecsDropDownListDropDownCountTabOrder  TJvCalcEditedTDELeftTop~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShownOnChangeedTDEChange  TJvCalcEditedTRLeft� TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTDEpLeftgTop~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShownOnChangeedTDEpChange  TJvCalcEditedTDEmLeft� Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  	TCheckBoxcbStatusLeftbTopWidth>HeightCaptionAtivoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder'  	TComboBox
cbOperacaoLeft	Top� Width� HeightStylecsDropDownListDropDownCountTabOrder(Items.Strings       TJvCalcEditedTdeMaxLeft=Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvMemoObsLefthTop�Width�Height9	MaxLength,TabOrder)  TJvCalcEditedTRpLeft>TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTRmLeft�TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedHELeft=TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvEditedUserLeftWTop"WidthyHeight
BevelInnerbvNone
BevelOuterbvNoneBorderStylebsNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder*Text      TJvDateEditedDtInsLeft�Top"WidthAHeightBorderStylebsNone
BevelInnerbvNone
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	
ShowButtonShowNullDateTabOrder+  TJvEditedUserALeft�Top"WidthyHeight
BevelInnerbvNone
BevelOuterbvNoneBorderStylebsNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder,Text      TJvDateEditedDtAltLeft"Top"WidthAHeightBorderStylebsNone
BevelInnerbvNone
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	
ShowButtonShowNullDateTabOrder-  TJvCalcEdit	edSuframaLeftTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit
edPReceitaLeft�TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShownOnExitedPReceitaExit  TJvCalcEditedPortoLeftgTop�WidthAHeightDecimalPlacesDisplayFormat,0.0000
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit
edPortoMinLeft� Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder DecimalPlacesAlwaysShown  TJvCalcEditedTDALeftTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder!DecimalPlacesAlwaysShown  TJvCalcEditedTDAMinLeftgTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder"DecimalPlacesAlwaysShown  TJvCalcEditedTDAMaxLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder#DecimalPlacesAlwaysShown  TJvCalcEditedDiariaLeft=TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder.VisibleDecimalPlacesAlwaysShown  	TComboBox	cbRegiaoDLeft�Top� WidthPHeightStylecsDropDownListTabOrder/Items.Strings       1ª Região   2ª Região   3ª Região   4ª Região   5ª Região   6ª Região   7ª Região   8ª Região   9ª Região   	TComboBox	cbregiaoOLeft�TopvWidthPHeightStylecsDropDownListTabOrder0Items.Strings       1ª Região   2ª Região   3ª Região   4ª Região   5ª Região   6ª Região   7ª Região   8ª Região   9ª Região    	TTabSheet	TabSheet2CaptionGeneralidades
ImageIndex TPanelPanel8Left TopIWidth�Height>AlignalTop	BevelKindbkSoft
BevelOuterbvNoneTabOrder  TLabelLabel52LeftTopWidth-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLBLDedicadoLeft�TopWidth<HeightCaptionDedicadoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel	LbldiariaLeft&TopWidth$HeightCaption   DiáriaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLBLPernoiteLeft� TopWidth2HeightCaptionPernoiteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TJvCalcEditeddedicadoGenLeft�TopWidth� HeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditeddiariaGenLeft%TopWidth� HeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedpernoiteGenLeft� TopWidth� HeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TBitBtnbtinserirgenLeft�TopWidthKHeightCaptionInserir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � A�N =�I :�D 6�@ 2�< /�7 ,�3 (0 � � � � � � � � � � � � � � I�X E�S A�P } �Ц �Ϧ x�� 6�B ,�4 )0 � � � � � � � � � � Q�a M�] d�x �۵ �̘ f�} d�| �˘ �ٴ X�k ,�4 )0 � � � � � � Y�k V�f j�} �۲ `�w \�s Y�p Y�o X�o [�t �ٳ Z�l ,�4 )0 � � � � ]�p S�h �ݴ d�y _�q `�w ��� ��� Y�p X�n \�t �ڴ 8�C ,�4 � � � � a�u �̘ �Ӗ k�z c�p U�e ��� ��� Y�p Y�p [�r �̗ {�� 0�9 � � � � e�z �ݳ }ϊ ú ��� ��� ��� ��� ��� ��� Y�p g�} �ԫ 4�= � � � � i�~ �� �՗ zɆ ��� ��� ��� ��� ��� ��� Y�p i�~ �Ԫ 8�B � � � � m�� �ݶ �߯ �ˌ |Ɇ n�y ��� ��� [�j `�w \�s �љ �Œ <�G � � � � p�� �Ǘ ��� �٠ �Ӕ ȉ ��� ��� yͅ k�| o�~ �ߵ E�W @�L � � � � s�� p�� �ڷ ��� �؝ �͓ �̎ �Ԗ �ԕ �Ҏ �� k�} H�V D�Q � � � � � � s�� p�� �ܻ ��� �� �ۥ �٠ �߯ ��� y P�_ L�[ � � � � � � � � � � s�� q�� �Τ ��� ��� ��� �ݸ m� X�i T�e � � � � � � � � � � � � � � t�� q�� n�� j�� g�| c�w `�s \�n � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrderOnClickbtinserirgenClick  TBitBtnbtsalvargenLeftTopWidthKHeightCaptionSalvarEnabledTabOrderOnClickbtsalvargenClick  TBitBtnbtalterargenLeftfTopWidthKHeightCaptionAlterar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � �  }!�{� y� � � � � � � � @X�B^�%i��,v��;���� � � � � � � � �+�C�_� {� y� � � � � � BY�]�����������H���!�R��I��C��;�:�^�����F�b� }� y� � � � m������a���@���g��)�[��ʩ��ȥ��ơ��Ş�j�����H�f� }!� y	� � m����������4}��1���1�c��ͭ�o���k���f���a���g�����<�\� %�� �  c� &�������g���`���7�k��ΰ��ͭ��˪��˨�t����ǡ�F�h��5��-� � � � � � &�����������=�o�7�o�4�i�0�c�U�|��˪�O�t��E��=� � � � � � � � � � '�����������`���D�������4�m�Z���(�W�� � � � � � � � � � � � � � � � /�����������`���D���>�v�1�e�;���� � � � � � � � � � � � � � � � � � /�����������`���D�������]���;���� � � � � � � � � � � � � � � � � � /�����������h���o���Y���s���O���� � � � � � � � � � � � � � � � � � /�����������l���������������S���� � � � � � � � � � � � � � � � � � /�����������������������O���� � � � � � � � � � � � � � � � � � � � P���j�����������B���1�®� � � � � � � � � � � � � � � � � � � � /��	O���P���N���/���5��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrderOnClickbtalterargenClick  TBitBtnbtexcluirgenLeft�TopWidthKHeightCaptionExcluir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrderOnClickbtexcluirgenClick  TBitBtnbtcancelagenLeft�TopWidthKHeightCaptionCancelarEnabledTabOrderOnClickbtcancelagenClick  	TComboBoxcbveiculogenLeftTopWidth� HeightStylecsDropDownListTabOrder    	TJvDBGrid	JvDBGrid1Left Top� Width�Height�AlignalClient
DataSource	dsGenItemTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnCellClickJvDBGrid1CellClick"SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldName	COD_CUSTOVisible Expanded	FieldNameCOD_IDVisible Expanded	FieldNameVEICULOTitle.AlignmenttaCenterTitle.Caption   VeículoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldNameVL_PERNOITETitle.AlignmenttaCenterTitle.CaptionValor PernoiteTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldNameVL_VDTitle.AlignmenttaCenterTitle.CaptionValor DedicadoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldName	VL_DIARIATitle.AlignmenttaCenterTitle.Caption   Valor DiáriaTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	    TPanelPanel9Left Top Width�HeightIAlignalTop	BevelKindbkSoftTabOrder TLabelLabel45LeftlTopWidth5HeightCaptionAjudanteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel46Left	TopWidthGHeightCaption   PaletizaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel44Left�TopWidth<HeightCaption	Armaz. KgFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel51Left2TopWidth>HeightCaption	Hr ParadaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel48Left� TopWidthOHeightCaptionReentrega %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
LBLCanhotoLeft�TopWidthQHeightCaptionDev. CanhotoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel53Left(TopWidthFHeightCaptionReentr. Min.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel54Left�TopWidthIHeightCaption   Arm. MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TJvCalcEditedPaletLeft	TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder DecimalPlacesAlwaysShown  TJvCalcEditedAjudaLeftjTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edArmazemLeft�TopWidthAHeightDecimalPlacesDisplayFormat,0.0000
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit
edHrParadaLeft2TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedReentregaLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TBitBtn	bteditgenLeft�TopWidthKHeightCaptionAlterar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � �  }!�{� y� � � � � � � � @X�B^�%i��,v��;���� � � � � � � � �+�C�_� {� y� � � � � � BY�]�����������H���!�R��I��C��;�:�^�����F�b� }� y� � � � m������a���@���g��)�[��ʩ��ȥ��ơ��Ş�j�����H�f� }!� y	� � m����������4}��1���1�c��ͭ�o���k���f���a���g�����<�\� %�� �  c� &�������g���`���7�k��ΰ��ͭ��˪��˨�t����ǡ�F�h��5��-� � � � � � &�����������=�o�7�o�4�i�0�c�U�|��˪�O�t��E��=� � � � � � � � � � '�����������`���D�������4�m�Z���(�W�� � � � � � � � � � � � � � � � /�����������`���D���>�v�1�e�;���� � � � � � � � � � � � � � � � � � /�����������`���D�������]���;���� � � � � � � � � � � � � � � � � � /�����������h���o���Y���s���O���� � � � � � � � � � � � � � � � � � /�����������l���������������S���� � � � � � � � � � � � � � � � � � /�����������������������O���� � � � � � � � � � � � � � � � � � � � P���j�����������B���1�®� � � � � � � � � � � � � � � � � � � � /��	O���P���N���/���5��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrderOnClickbteditgenClick  TBitBtn	btpostgenLeft'TopWidthKHeightCaptionSalvarEnabledTabOrder	OnClickbtpostgenClick  TBitBtnbtcancelgenLeftxTopWidthKHeightCaptionCancelarEnabledTabOrder
OnClickbtcancelgenClick  TJvCalcEdit	edCanhotoLeft�TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedRee_mLeft(TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedArmMinLeft�TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown    	TTabSheet	TabSheet1CaptionPesquisa
ImageIndex TLabelLabel15Left	TopWidthVHeightCaptionTransportadorFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel16Left
Top/Width,HeightCaptionOrigemFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel17Left
TopcWidth.HeightCaptionDestinoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel24LeftTop� Width-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel29LeftTop� Width=HeightCaption
   OperaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel34Left� Top2Width,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel35Left� TopfWidth,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel55LeftTop�Width� HeightCaption'   Observação Reprovação Controladoria  TLabelLabel38Left�Top�Width&HeightCaptionLabel38Visible  	TComboBoxcbPesqTranspLeftTopWidthFHeightStylecsDropDownListCharCaseecUpperCaseDropDownCountFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TComboBox	cbPesqUFOLeftTopEWidth<HeightStylecsDropDownListCharCaseecUpperCaseDropDownCount	ItemIndex TabOrderItems.Strings    ACALAMAPBACEDFESGOMAMGMSMTPAPBPEPIPRRJRNRORRRSSCSESPTO   	TComboBoxcbPesqLOLeftQTopDWidthjHeightStylecsDropDownListCharCaseecUpperCase	ItemIndex TabOrderOnChangecbPesqLOChangeItems.Strings    CAPITALINTERIOR   	TComboBox	cbPesqUFDLeftTopyWidth<HeightStylecsDropDownListCharCaseecUpperCaseDropDownCount	ItemIndex TabOrderItems.Strings    ACALAMAPBACEDFESGOMAMGMSMTPAPBPEPIPRRJRNRORRRSSCSESPTO   	TComboBoxcbPesqLDLeftQTopyWidthjHeightStylecsDropDownListCharCaseecUpperCase	ItemIndex TabOrderOnChangecbPesqLDChangeItems.Strings    CAPITALINTERIOR   TBitBtn
btProcurarLeft
TopWidthYHeightHintFiltrarCaption	Localizar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � ��� ��� ��� � � � � � � � � � � � � � � � � � � � � � � � � )R� 1k� 1k� ��� � � � � � � � � � � � � � � � � � � � � � � ��� 1k� 9s� � � � � � � � � � � � � � � � � � � � � � � � � � !R� 1k� { ��� � � � � � � � � � � � � � � � � RRR �sR �c) BJJ s�� ��� � � � � � � � � � � � � � � � � � � ��� ��� �ތ ֥1 ��1 ! � � � � � � � � � � � � � � � � � � � � ��� ��� ��� ��1 ֥1 {Z! � � � � � � � � � � � � � � � � � � � � ��� �Υ � � ��� ��R �sR � � � � � � � � ��� ��� ��� ��� ��� ��� ��� �{s ��� �� ��s RRR � � � � � � � � ��� ��� ��� ｔ s�� ��� ��� ��� ��� ��� ��� � � � � � � � � � � ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� � � � � � � � � � � ��� ��� ��� ｔ ��� ��� ��� ｔ ｔ ��� ��� � � � � � � � � � � ��� ��� ��� ｔ ��� ��� ��� ｔ ｔ ��� ��� � � � � � � � � � � ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� � � � � � � � � � � ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� � � � � � � � � � � ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtProcurarClick  TBitBtnbtRetirarFiltroLeft� TopWidthrHeightHintRetirar FiltroCaptionLimpar Pesquisa
Glyph.Data
:  6  BM6      6   (                                  � �� �� �� �� �� �� �� �� �� �� �� �� ���ޥ������ �� �� �� �� �� �� �� �� �� �� �� �)R�1k�  �  �� �  �  �� �� �� �� �� �� �� �� ����1k�  �  �� �� �  �  �  �� �� �� �� �� �� �� �!R�  �  ����� �� �  �  �  �  �� �� �RRR�sR�c)BJJ  �  �� �� �� �� �� �  �  �  �  ���֥���ތ֥1  �  �� �� �� �� �� �� �� �� �  �  �  ����  �  �  �{Z!� �� �� �� �� �� �� �� �� �  �  �  �  �  ���R�sR� �� �� �� �������������������  �  �  ��絥�sRRR� �� �� �� ����������ｔs��  �  �  �  �  ����� �� �� �� �� �������������  �  �  ք�����  �  �� �� �� �� �� ����������  �  �  ���ｔｔ���  �  �� �� �� �� �������  �  �  �������ｔｔ������  �  �� �� �� ����  �  �  ����������������������� �� �  �� �� �  �  �  �������������������������� �� �� �� �� �  �  ����������������������������� �� �� �� �� �ParentShowHintShowHint	TabOrderOnClickbtRetirarFiltroClick  	TComboBoxcbVeiculoPesqLeftTop� WidthFHeightStylecsDropDownListCharCaseecUpperCaseDropDownCountFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TComboBoxcbOperacaoPesqLeftTop� WidthFHeightStylecsDropDownListCharCaseecUpperCaseDropDownCountFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TRadioGroupRGFiltroLeft�TopWidth� Height� CaptionFiltro	ItemIndex Items.StringsTodas	A Validar
ReprovadasVencidas TabOrder	  	TComboBox
cbCidOPesqLeft� TopDWidth� HeightStylecsDropDownListDropDownCount(TabOrder
  	TComboBox
cbCidDPesqLeft� TopyWidth� HeightStylecsDropDownListDropDownCount(TabOrder  TDBMemoDBMemo1LeftTop�Width�HeightYTabOrder  TJvDBMaskEditJvDBMaskEdit1Left�Top&WidthyHeightTabOrderEditMask        TPanelPanel6Left TopoWidth�Height"AlignalBottomTabOrder TPanelPanel2LeftTopWidthHeight AlignalLeftTabOrder  TLabellblQuantLeftUTopWidth%HeightHintQuantidade de registros	AlignmenttaCenterAutoSizeParentShowHintShowHint	LayouttlCenter  TBitBtn	btnFecharLeft4TopWidthHHeightCaption&Fechar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrder OnClickbtnFecharClick  TBitBtnbtnCancelarLeft�TopWidthHHeightHint!   Desfazer as Últimas AlteraçõesCaptionCancelarEnabled
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnCancelarClick  TBitBtn	btnSalvarLeftTopWidthHHeightHintSalvar DadosCaptionSalvarEnabled
Glyph.Data
:  6  BM6      6   (                                   �j6��i5��i5�h5��h5��g4��f4��e3��d3��c2��b2��a2��`1��`1��`1�a1ĺj5��ƭ��ŭ�����������������������������������������Ț|�ǘy��`1��k7��ʳ��z�����b���b���b���b���b���b���b���b�������ʍe�ɛ|��`1��l8��̶��z�����������������������������������������͐h�̞���a2��k8��θ��y�����b���b���b���b���b���b���b���b�������ϓj�Σ���a2��j6��л��z�����������������������������������������Ӗm�ҧ���b2��j6��Ҿ��z��z��z��{��{��x�ޟw�ݟv�ܝt�ٛr�ؙq�֙p�ի���c3��j6������z��z��{��{��{��y��x�ޠw�ޞu�ܝt�ڛs�ٛs�ڰ���d3��j6������{��z��z��{��{��{��y�ߠw�ޟv�ݞt�ۜr�ܝt�ݵ���e4��k6������}�Ȍd�ɍe�Ɏg�˒l�˒m�ʐi�Ȍe�Ȍd�Ȍd�Ȍd�ڜt�ẟ��f4��k6������}�����������������������������������������ޠw�侤��g4��k6������~���������ɍf������������������������������x��©��h5��k6������~���������Ȍd������������������������������z������i5��k6�����骀���������Ȍd������������������������������к��н��p>��k6��������������������������������������������������н�ɉ^�i5c�k6q�k6��k6̼k6�k6��k6��k6��j6��j6��l9��n;��m:��k8�p>˶i5T��� ParentShowHintShowHint	TabOrderOnClickbtnSalvarClick  TBitBtnbtnNovoLeft� TopWidthHHeightHintIncluir Novo FreteCaptionInserir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � A�N =�I :�D 6�@ 2�< /�7 ,�3 (0 � � � � � � � � � � � � � � I�X E�S A�P } �Ц �Ϧ x�� 6�B ,�4 )0 � � � � � � � � � � Q�a M�] d�x �۵ �̘ f�} d�| �˘ �ٴ X�k ,�4 )0 � � � � � � Y�k V�f j�} �۲ `�w \�s Y�p Y�o X�o [�t �ٳ Z�l ,�4 )0 � � � � ]�p S�h �ݴ d�y _�q `�w ��� ��� Y�p X�n \�t �ڴ 8�C ,�4 � � � � a�u �̘ �Ӗ k�z c�p U�e ��� ��� Y�p Y�p [�r �̗ {�� 0�9 � � � � e�z �ݳ }ϊ ú ��� ��� ��� ��� ��� ��� Y�p g�} �ԫ 4�= � � � � i�~ �� �՗ zɆ ��� ��� ��� ��� ��� ��� Y�p i�~ �Ԫ 8�B � � � � m�� �ݶ �߯ �ˌ |Ɇ n�y ��� ��� [�j `�w \�s �љ �Œ <�G � � � � p�� �Ǘ ��� �٠ �Ӕ ȉ ��� ��� yͅ k�| o�~ �ߵ E�W @�L � � � � s�� p�� �ڷ ��� �؝ �͓ �̎ �Ԗ �ԕ �Ҏ �� k�} H�V D�Q � � � � � � s�� p�� �ܻ ��� �� �ۥ �٠ �߯ ��� y P�_ L�[ � � � � � � � � � � s�� q�� �Τ ��� ��� ��� �ݸ m� X�i T�e � � � � � � � � � � � � � � t�� q�� n�� j�� g�| c�w `�s \�n � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnNovoClick  TBitBtn
btnAlterarLeft[TopWidthHHeightHintAlterar o CadastroCaptionAlterar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � �  }!�{� y� � � � � � � � @X�B^�%i��,v��;���� � � � � � � � �+�C�_� {� y� � � � � � BY�]�����������H���!�R��I��C��;�:�^�����F�b� }� y� � � � m������a���@���g��)�[��ʩ��ȥ��ơ��Ş�j�����H�f� }!� y	� � m����������4}��1���1�c��ͭ�o���k���f���a���g�����<�\� %�� �  c� &�������g���`���7�k��ΰ��ͭ��˪��˨�t����ǡ�F�h��5��-� � � � � � &�����������=�o�7�o�4�i�0�c�U�|��˪�O�t��E��=� � � � � � � � � � '�����������`���D�������4�m�Z���(�W�� � � � � � � � � � � � � � � � /�����������`���D���>�v�1�e�;���� � � � � � � � � � � � � � � � � � /�����������`���D�������]���;���� � � � � � � � � � � � � � � � � � /�����������h���o���Y���s���O���� � � � � � � � � � � � � � � � � � /�����������l���������������S���� � � � � � � � � � � � � � � � � � /�����������������������O���� � � � � � � � � � � � � � � � � � � � P���j�����������B���1�®� � � � � � � � � � � � � � � � � � � � /��	O���P���N���/���5��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnAlterarClick  TBitBtnbtnExcluir_Left�TopWidthHHeightHintExcluir FornecedorCaptionExcluir
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ?T�#:P�}8S��5Q��0K��.N��+L�}'H�#� � � � � � � � � � � � � � FX�SBU��<R��uz����������qx��3M��+J��)K�S� � � � � � � � � � MZ�SIY��\e������~���[c��Y]��}�������Q]��+J��)K�S� � � � � � T_�"Sa��ak������T_��P\��MY��NY��LV��PV������T`��*J��)K�"� � � � X`�~KV������Vd��Rf��MY��MY��MY��MY��LX��RZ������4P��*J�~� � � � \b�ہ���~���]s��MY��MY��MY��MY��MY��MY��O[��{���u{��.K��� � � � _c������p���h���������������������������MY��\f������2P��� � � � di���������p���������������������������MY��^j������6O��� � � � gj�ۥ�������w���T_��T_��T_��T_��T_��T_��cw������x��:S��� � � � ji�~}�����������~���u���l���l���l���l���cy������>O��>T�~� � � � ll�"ji�壧�������������z������~���v�������cn��EW��AV�"� � � � � � ml�Sji������������������������������p{��L[��HX�S� � � � � � � � � � ml�Sjj�掓������������������fp��S^��P]�S� � � � � � � � � � � � � � mm�#kj�}hj��cd��ad��]c��[c�}Xb�#� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderVisibleOnClickbtnExcluir_Click  TBitBtnBitBtn15TagLeftTopWidth(HeightActionacPrimeiroApanhaParentShowHintShowHint	TabOrder  TBitBtnBitBtn16TagLeft*TopWidth(HeightActionacAnteriorApanhaParentShowHintShowHint	TabOrder  TBitBtnBitBtn17TagLeft}TopWidth&HeightActionacProximoApanhaParentShowHintShowHint	TabOrder  TBitBtnBitBtn18TagLeft� TopWidth(HeightActionacUltimoApanhaParentShowHintShowHint	TabOrder	  TBitBtnbtnDuplicarLeft�TopWidthHHeightHintDuplicar TabelaCaptionDuplicar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � ֞r�әn�іh�Βc�ǇV�ÄR�ÄR�ÄR�ÄR�ÄR��wB�� � � � � � � � � � סu�������������������������������������Ŋ]�� � � � � � ֞r�әn�ᮇ������˲��̳��Ǭ��Ǭ��Ǭ��Ȱ��Ȯ�����ĆT�� � � � � � סu�����㱌������ɮ������Ǭ���������������������ƆU�� � ֞r�әn�ଅ�����崏������ƪ��Ƭ��Ǭ��Ǭ��ɰ��Ȱ��̵�����ȊY�� � סu�����嶓�����緔������æ������Ǭ���������������������ˏ_�� � ᮇ�����嶒�����麘������æ��æ��æ��æ��æ��æ��æ�����Γd�� � 㱌�����縖�����뽛�������������������������������������їj�� � 崏�����黙�����쿞������ե��ӡ��˓��Ɖ�~Ä�z���v�|�����ԛo�� � 緔�����뽜������ƨ�������������������������������������נt�� � 麘�����쿟������ˤ��â�����뾝�緓�䲌�⯈�଄�ݩ��ܥ}�ڣz�� � 뽛������ʮ�������������������������������������נt�� � � � � � 쿞������ˤ��Ģ�����㿛�߸��ܳ��ڰ��ⲍ�ܨ�ܥ}�ڣz�� � � � � � ����������������������������������������נt�� � � � � � � � � � �£e��������뾝�緓�䲌�⯈�଄�ݩ��ܥ}�ڣz�� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrder
OnClickbtnDuplicarClick  TBitBtnbtnDctoLeft�TopWidth`HeightHintIncluir DocumentoCaption	DocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������mq�PUم���������������������������������������������=B�������������������������������������������������OU����������������������������������������������w}�z�崸��������el�v|䛠����������������������LU�������fn�-8�FP�]e�ry姫����������������������mu痜����`i����������������������������������������S^�`j�������������������������������������������]h�lv�������������������������������������������t�������������������������������������������iv�������������������������������������������s����������������������������������������������x������������������������������������������������������������������������������������������������������������������������������
ParentFontParentShowHintShowHint	TabOrderOnClickbtnDctoClick  TPanelPanel7LeftXTopWidth� HeightParentBackgroundTabOrder TRadioButtonRBAtivoLeftTopWidth-HeightCaptionAtivoChecked	Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontTabOrder TabStop	OnClickRBAtivoClick  TRadioButton	RbInativoLeft;TopWidthGHeightCaptionInativoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontTabOrderOnClickRbInativoClick    TPanelPanel5LeftTopWidth� Height AlignalRightTabOrder TBitBtnbtnVerLeftHTopWidth>HeightHintVer o DocumentoCaptionPDFFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
:  6  BM6      6   (                  �  �          ���������������������������������������������������������������������������������]�Z����������������������������������������"f�b�"g�[����������������������������������2r�+m�U�����_�������������������������������D�<y�d�ŝ��f��e�������������ݲ�ٮ�֩�ӥР{͜v���u�̫��v��0p�(k����������佛Ḗ�ɮ������������ߺ�Ǩ����A}�9w��������������â�з�������ؽ�ֻ������߻����K������������������Ȩ�������������ؽ�׻������͟{�������������������ή����������������پ�׽���ӥ�������������������Ӵ������������������������ת��������������������׹������������������������۰��������������������۽����������������������ϵ߶�����������������������۾����������������ս���㼚�������������������������ܿ�ٻ�ָ�Ӵ�ϯ�˫�Ʀ������������������������������������������������������������������
ParentFontParentShowHintShowHint	TabOrder OnClickbtnVerClick    TDataSource	dtstabelaDataSetQTabelaLeftLTop4  TDataSourcedtsItemDataSetQItemLeftKToph  TActionListactImagesdtmDados.IMBotoesLeftPTop< TActionacPrimeiroApanhaTagCategoryApanhaHintPrimeiro registro
ImageIndex	OnExecuteacPrimeiroApanhaExecute  TActionacAnteriorApanhaTagCategoryApanhaHintRegistro anterior
ImageIndex	OnExecuteacPrimeiroApanhaExecute  TActionacProximoApanhaTagCategoryApanhaHint   Próximo registro
ImageIndex 	OnExecuteacPrimeiroApanhaExecute  TActionacUltimoApanhaTagCategoryApanhaHint   Último registro
ImageIndex	OnExecuteacPrimeiroApanhaExecute   	TADOQueryQTabela
ConnectiondtmDados.ConIntecom
CursorTypectStatic	AfterOpenQTabelaAfterScrollAfterScrollQTabelaAfterScroll
Parameters SQL.StringsFselect t.*, f.nomeab || ' - ' ||nvl(f.codcgc, f.codcpf) nm_fornecedor,CASE'   WHEN t.fl_local = 'C' THEN 'CAPITAL'ELSE   'INTERIOR'end as local,CASE+   WHEN t.fl_local_des = 'C' THEN 'CAPITAL'ELSE   'INTERIOR''end as local_des, op.fl_km, op.fl_aereoOfrom tb_custofrac t inner join cyber.rodcli f on t.cod_fornecedor = f.codclifor`                    left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = 'N'where fl_status = 'S'order by cod_custo     Left Top4 	TBCDFieldQTabelaCOD_CUSTO	FieldName	COD_CUSTO	Precision   TStringFieldQTabelaNM_FORNECEDOR	FieldNameNM_FORNECEDORSize2  TStringFieldQTabelaDS_UF	FieldNameDS_UFSize  TStringFieldQTabelaDS_UF_DES	FieldName	DS_UF_DES	FixedChar	Size  TStringFieldQTabelaLOCAL	FieldNameLOCALSize  TStringFieldQTabelaLOCAL_DES	FieldName	LOCAL_DESSize  	TBCDFieldQTabelaVL_FRETE_MINIMO	FieldNameVL_FRETE_MINIMODisplayFormat#,##0.00	PrecisionSize  TFloatFieldQTabelaVL_AD	FieldNameVL_ADDisplayFormat	#,##0.00%  TStringFieldQTabelaVEICULO	FieldNameVEICULO  TStringFieldQTabelaOPERACAODisplayWidth(	FieldNameOPERACAOSize  TStringFieldQTabelaFL_KM	FieldNameFL_KMSize  TStringFieldQTabelafl_status	FieldName	fl_statusSize  
TDateFieldQTabeladt_controler	FieldNamedt_controler  TStringFieldQTabelafl_aereo	FieldNamefl_aereoSize   	TADOQueryQItem
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custoDataTypeftStringSizeValue0  SQL.Strings1select distinct t.*,t.ROWID || '' as id_registro from tb_custofrac_item twhere t.cod_custo = :cod_custo+order by datai,dataf,nr_peso_de,nr_peso_ate Left Toph 	TBCDFieldQItemCOD_TAB	FieldNameCOD_TAB	Precision   	TBCDFieldQItemCOD_CUSTO	FieldName	COD_CUSTO	Precision   	TBCDFieldQItemNR_PESO_DE	FieldName
NR_PESO_DEDisplayFormat
##,##0.000	Precision
  	TBCDFieldQItemNR_PESO_ATE	FieldNameNR_PESO_ATEDisplayFormat
##,##0.000	Precision
  	TBCDFieldQItemVL_RODOVIARIO	FieldNameVL_RODOVIARIODisplayFormat
###,##0.00	PrecisionSize  TDateTimeField
QItemDATAI	FieldNameDATAI  TDateTimeField
QItemDATAF	FieldNameDATAF  TStringFieldQItemID_REGISTRO	FieldNameID_REGISTROReadOnly	Size  	TBCDFieldQItemVR_CTRC	FieldNameVR_CTRCReadOnly	DisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQItemVR_EXCED	FieldNameVR_EXCEDReadOnly	DisplayFormat#0.0000	Precision
  	TBCDFieldQItemvl_minimo	FieldName	vl_minimoDisplayFormat
###,##0.00   	TADOQueryQTransp
ConnectiondtmDados.ConIntecom
CursorTypectStatic
Parameters SQL.Strings^select f.codclifor cod_fornecedor,  f.nomeab || ' - ' ||nvl(f.codcgc, f.codcpf) nm_fornecedor from cyber.rodcli f where f.situac = 'A'and classi in (9,2, 11)and nomeab is not null
order by 2     Left Top  TStringFieldQTranspnm_fornecedor	FieldNamenm_fornecedorSize2  	TBCDFieldQTranspCOD_FORNECEDOR	FieldNameCOD_FORNECEDOR	Precision Size    TDataSourcedtsTransDataSetQTranspLeftLTop   	TADOQuery
Qcadtabela
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custo
AttributespaSigned DataType	ftInteger	Precision
Value  SQL.Stringsselect *from tb_custofracwhere cod_custo = :cod_custo Left Top 	TBCDFieldQcadtabelaCOD_CUSTO	FieldName	COD_CUSTO	Precision   	TBCDFieldQcadtabelaCOD_FORNECEDOR	FieldNameCOD_FORNECEDOR	Precision Size   TStringFieldQcadtabelaFL_LOCAL	FieldNameFL_LOCAL	FixedChar	Size  TStringFieldQcadtabelaDS_UF	FieldNameDS_UFSize  TStringFieldQcadtabelaFL_LOCAL_DES	FieldNameFL_LOCAL_DES	FixedChar	Size  TStringFieldQcadtabelaDS_UF_DES	FieldName	DS_UF_DESSize  	TBCDFieldQcadtabelaVL_FRETE_MINIMO	FieldNameVL_FRETE_MINIMO	PrecisionSize  	TBCDFieldQcadtabelaVL_PEDAGIO	FieldName
VL_PEDAGIO	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS	FieldName	VL_OUTROS	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS_REAIS	FieldNameVL_OUTROS_REAIS	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS_MINIMO	FieldNameVL_OUTROS_MINIMO	PrecisionSize  	TBCDFieldQcadtabelaVL_EXCEDENTE	FieldNameVL_EXCEDENTE	PrecisionSize  TFloatFieldQcadtabelaVL_AD	FieldNameVL_AD  TFloatFieldQcadtabelaVL_GRIS	FieldNameVL_GRIS  TFloatFieldQcadtabelaVL_GRIS_MINIMO	FieldNameVL_GRIS_MINIMO  	TBCDFieldQcadtabelaVL_ENTREGA	FieldName
VL_ENTREGA	PrecisionSize  TFloatFieldQcadtabelaSEGURO_MINIMO	FieldNameSEGURO_MINIMO  TStringFieldQcadtabelaFL_PESO	FieldNameFL_PESO	FixedChar	Size  TStringFieldQcadtabelaFL_IMPOSTO	FieldName
FL_IMPOSTO	FixedChar	Size  TStringFieldQcadtabelaMODAL	FieldNameMODAL  TStringFieldQcadtabelaVEICULO	FieldNameVEICULO  	TBCDFieldQcadtabelaSITE	FieldNameSITE	Precision Size   TDateTimeFieldQcadtabelaDT_CADASTRO	FieldNameDT_CADASTRO  TStringFieldQcadtabelaUSUARIO	FieldNameUSUARIO  	TBCDFieldQcadtabelaCODMUN	FieldNameCODMUN	Precision Size   	TBCDFieldQcadtabelaCODMUNO	FieldNameCODMUNO	Precision Size   	TBCDFieldQcadtabelaVL_TDE	FieldNameVL_TDE	PrecisionSize  	TBCDFieldQcadtabelaVL_TR	FieldNameVL_TR	PrecisionSize  	TBCDFieldQcadtabelaVL_TDEP	FieldNameVL_TDEP	PrecisionSize  	TBCDFieldQcadtabelaVL_TDEM	FieldNameVL_TDEM	PrecisionSize  	TBCDFieldQcadtabelaDIAS	FieldNameDIAS	Precision Size   TStringFieldQcadtabelaFL_STATUS	FieldName	FL_STATUSSize  TStringFieldQcadtabelaOPERACAO	FieldNameOPERACAOSize  	TBCDFieldQcadtabelaVL_TDEMAX	FieldName	VL_TDEMAX	PrecisionSize  TStringFieldQcadtabelaCONTROLER	FieldName	CONTROLER  TDateTimeFieldQcadtabelaDT_CONTROLER	FieldNameDT_CONTROLER  TStringFieldQcadtabelaOBS	FieldNameOBSSize,  	TBCDFieldQcadtabelaVL_TRP	FieldNameVL_TRP	PrecisionSize  	TBCDFieldQcadtabelaVL_TRM	FieldNameVL_TRM	PrecisionSize  	TBCDFieldQcadtabelaVL_HE	FieldNameVL_HE	PrecisionSize  TStringFieldQcadtabeladoc_controler	FieldNamedoc_controlerSize�   	TBCDFieldQcadtabelaVL_SUFRA	FieldNameVL_SUFRA  	TBCDFieldQcadtabelaVL_VD	FieldNameVL_VD  	TBCDFieldQcadtabelaVL_TDC	FieldNameVL_TDC  	TBCDFieldQcadtabelaVL_PORTO	FieldNameVL_PORTO  	TBCDFieldQcadtabelaVL_PORTOMIN	FieldNameVL_PORTOMIN  	TBCDFieldQcadtabelaVL_TDA	FieldNameVL_TDA  	TBCDFieldQcadtabelaVL_TDAMIN	FieldName	VL_TDAMIN  	TBCDFieldQcadtabelaVL_TDAMAX	FieldName	VL_TDAMAX  TStringFieldQcadtabelaREPROVADO	FieldName	REPROVADOSize,  TStringFieldQcadtabelaCONTROLER_REPR	FieldNameCONTROLER_REPRSize  	TBCDFieldQcadtabelaVL_ARMAZENAGEM	FieldNameVL_ARMAZENAGEM	Precision  	TBCDFieldQcadtabelaVL_AJUDANTES	FieldNameVL_AJUDANTES	PrecisionSize  	TBCDFieldQcadtabelaVL_PALETIZACAO	FieldNameVL_PALETIZACAO	PrecisionSize  	TBCDFieldQcadtabelaVL_DIARIA	FieldName	VL_DIARIA	PrecisionSize  	TBCDFieldQcadtabelaVL_REENTREGA	FieldNameVL_REENTREGA	PrecisionSize  	TBCDFieldQcadtabelaVL_PERNOITE	FieldNameVL_PERNOITE	PrecisionSize  	TBCDFieldQcadtabelaVL_HORA_PARADA	FieldNameVL_HORA_PARADA	PrecisionSize  TStringFieldQcadtabelaREGIAO	FieldNameREGIAOSize2  	TBCDFieldQcadtabelaVL_REENTREGA_MIN	FieldNameVL_REENTREGA_MIN  	TBCDFieldQcadtabelaVL_ARMMIN	FieldName	VL_ARMMIN  	TBCDFieldQcadtabelavl_preceita	FieldNamevl_preceita  TStringFieldQcadtabelaregiaoo	FieldNameregiaooSize2   	TADOQueryQCidade
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName0DataTypeftStringSize�Value      SQL.Stringsselect codmun, descri from cyber.rodmun "where estado = :0 and inativ = 'N'            
order by 2 Left Top�  	TBCDFieldQCidadeCODMUN	FieldNameCODMUN	Precision   TStringFieldQCidadeDESCRI	FieldNameDESCRISize(   TOpenDialog
OpenDialogLeftTop<  	TADOQueryQGenItem
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custoDataType	ftIntegerValue   SQL.Strings=select * from tb_custofrac_generalidades where cod_custo = :0 Left Top� TFMTBCDFieldQGenItemCOD_ID	FieldNameCOD_ID	Precision&Size   TFMTBCDFieldQGenItemCOD_CUSTO	FieldName	COD_CUSTO	Precision&Size  	TBCDFieldQGenItemVL_PERNOITE	FieldNameVL_PERNOITEDisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQGenItemVL_VD	FieldNameVL_VDDisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQGenItemVL_DIARIA	FieldName	VL_DIARIADisplayFormat
###,##0.00	PrecisionSize  TStringFieldQGenItemVEICULO	FieldNameVEICULOSize   TDataSource	dsGenItemDataSetQGenItemLeftLTop�  	TADOQueryQAereo
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custoDataTypeftStringSizeValue0  SQL.Strings1select distinct t.*,t.ROWID || '' as id_registro from TB_TABELACUSTO_AEREO twhere t.cod_custo = :cod_custo%order by datai,dataf,peso_de,peso_ate Left Top� TFMTBCDFieldQAereoCOD_CUSTO	FieldName	COD_CUSTOReadOnly		Precision&Size   	TBCDFieldQAereoPESO_DE	FieldNamePESO_DEReadOnly	DisplayFormat
##,##0.000	Precision
  	TBCDFieldQAereoPESO_ATE	FieldNamePESO_ATEReadOnly	DisplayFormat
##,##0.000	Precision
  	TBCDFieldQAereoVALOR_COLETA	FieldNameVALOR_COLETAReadOnly	DisplayFormat
###,##0.00	Precision  	TBCDFieldQAereoVALOR_ENTREGA	FieldNameVALOR_ENTREGAReadOnly	DisplayFormat
###,##0.00	Precision  TDateTimeFieldQAereoDATAI	FieldNameDATAIReadOnly	  TDateTimeFieldQAereoDATAF	FieldNameDATAFReadOnly	  	TBCDFieldQAereoVR_EXCED	FieldNameVR_EXCEDReadOnly	DisplayFormat
###,##0.00	PrecisionSize  TStringFieldQAereoUSUARIO	FieldNameUSUARIOReadOnly	  TDateTimeFieldQAereoDATAINC	FieldNameDATAINCReadOnly	  TStringFieldQAereoUSUALT	FieldNameUSUALTReadOnly	  TDateTimeFieldQAereoDATAALT	FieldNameDATAALTReadOnly	  TStringFieldQAereoID_REGISTRO	FieldNameID_REGISTROReadOnly	Size  TFMTBCDFieldQAereoCOD_TAB	FieldNameCOD_TABReadOnly		Precision&Size    TDataSourcedtsAereoDataSetQAereoLeftKTop�   