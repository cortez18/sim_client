unit Rel_ocorr_fatura_pagar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls, JvDBControls, JvMaskEdit, ADODB, DB,
  ExtCtrls, JvExExtCtrls, JvRadioGroup, JvBaseEdits, JvExStdCtrls, JvEdit,
  FileCtrl, JvDBLookup, Mask, JvExMask, JvToolEdit, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, Gauges, ShellApi;

type
  TfrmRel_ocorr_fatura_pagar = class(TForm)
    QOcorrencia: TADOQuery;
    dtsOcorr: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    Gauge1: TGauge;
    Panel1: TPanel;
    btnExcel: TBitBtn;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    JvDBGrid2: TJvDBGrid;
    JvDBNavigator1: TJvDBNavigator;
    Label3: TLabel;
    edFatura: TJvEdit;
    QOcorrenciaID: TBCDField;
    QOcorrenciaFATURA: TStringField;
    QOcorrenciaOCOR: TStringField;
    QOcorrenciaDESCRICAO: TStringField;
    QOcorrenciaDT_INC: TDateTimeField;
    QOcorrenciaUSER_INC: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure edFaturaEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_ocorr_fatura_pagar: TfrmRel_ocorr_fatura_pagar;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_ocorr_fatura_pagar.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_ocor_fatura_pagar.html';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid2.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid2.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QOcorrencia.First;
  while not QOcorrencia.Eof do
  begin
    for i := 0 to JvDBGrid2.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QOcorrencia.fieldbyname
        (JvDBGrid2.Columns[i].FieldName).AsString + '</th>');
    QOcorrencia.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  Memo1.Free;

  ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
    SW_SHOWNORMAL);
end;

procedure TfrmRel_ocorr_fatura_pagar.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QOcorrencia.close;
  if edFatura.Text <> '' then
    QOcorrencia.sql[3] := 'and fatura = ' + #39 + edFatura.Text + #39
  else
    QOcorrencia.sql[3] := '';
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QOcorrencia.sql[4] := 'and dt_inc between to_date(''' + dtInicial.Text +
      ' 00:00:00 '',''dd/mm/yy hh24:mi:ss'') and to_date(''' + dtFinal.Text +
      '23:59:59'',''dd/mm/yy hh24:mi:ss'')'
  else
    QOcorrencia.sql[4] := '';
  QOcorrencia.open;
  Panel1.Visible := false;
end;

procedure TfrmRel_ocorr_fatura_pagar.edFaturaEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_ocorr_fatura_pagar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QOcorrencia.close;
end;

end.
