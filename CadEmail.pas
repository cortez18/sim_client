unit CadEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DB, ImgList, ADODB, ActnList, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, JvExControls, JvDBLookup, ExtCtrls, JvExStdCtrls,
  JvCheckBox, JvCombobox, ComCtrls, Mask, JvExMask, JvToolEdit, JvMaskEdit,
  System.ImageList, System.Actions;

type
  TfrmCadEmail = class(TForm)
    dtsEmail: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QEmail: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    Label1: TLabel;
    dtsCliente: TDataSource;
    Label2: TLabel;
    QCliente: TADOQuery;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    QDestino: TADOQuery;
    dtsDestino: TDataSource;
    QDestinoCOD_CLIENTE: TBCDField;
    QDestinoNM_CLIENTE: TStringField;
    CBLDes: TJvDBLookupCombo;
    CBLCli: TJvDBLookupCombo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dbgContrato: TJvDBUltimGrid;
    QEmailID: TBCDField;
    QEmailNM_CLIENTE: TStringField;
    QEmailNM_DESTINO: TStringField;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    QPortaria: TADOQuery;
    QEntrega: TADOQuery;
    dtsPortaria: TDataSource;
    dtsEntrega: TDataSource;
    DBGrid2: TDBGrid;
    Panel1: TPanel;
    Label3: TLabel;
    btnCancelarP: TBitBtn;
    btnSalvarP: TBitBtn;
    btnAlterarP: TBitBtn;
    btnInserirP: TBitBtn;
    Panel2: TPanel;
    Label4: TLabel;
    btnCancelarE: TBitBtn;
    btnSalvarE: TBitBtn;
    btnAlterarE: TBitBtn;
    btnInserirE: TBitBtn;
    QPortariaPORTARIA: TStringField;
    QEntregaENTREGA: TStringField;
    Panel4: TPanel;
    edemailP: TJvMaskEdit;
    Panel5: TPanel;
    edemailE: TJvMaskEdit;
    QEmailCLIENTE: TBCDField;
    QEmailDESTINO: TBCDField;
    QPortariaID: TBCDField;
    QEntregaID: TBCDField;
    btnExcluirP: TBitBtn;
    btnExcluirE: TBitBtn;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure RestauraP;
    procedure RestauraE;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure btnInserirPClick(Sender: TObject);
    procedure btnAlterarPClick(Sender: TObject);
    procedure btnCancelarPClick(Sender: TObject);
    procedure btnSalvarPClick(Sender: TObject);
    procedure btnSalvarEClick(Sender: TObject);
    procedure btnInserirEClick(Sender: TObject);
    procedure btnAlterarEClick(Sender: TObject);
    procedure btnCancelarEClick(Sender: TObject);
    procedure QEmailAfterScroll(DataSet: TDataSet);
    procedure btnExcluirPClick(Sender: TObject);
    procedure btnExcluirEClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
  private
    tipo: String;
  public
    { Public declarations }
  end;

var
  frmCadEmail: TfrmCadEmail;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadEmail.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadEmail.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  CBLDes.KeyValue := '';
  CBLCli.KeyValue := '';
  CBLCli.SetFocus;
end;

procedure TfrmCadEmail.btnInserirEClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  RestauraE;
  tipo := 'I';
  edemailE.text := '';
  edemailE.SetFocus;
end;

procedure TfrmCadEmail.btnSalvarClick(Sender: TObject);
var
  cli, des: integer;
begin

  if CBLCli.KeyValue = '' then
  begin
    ShowMessage('N�o foi escolhido o Cliente');
    CBLCli.SetFocus;
    Exit;
  end;

  if CBLDes.KeyValue = '' then
  begin
    ShowMessage('N�o foi escolhido o Destino');
    CBLDes.SetFocus;
    Exit;
  end;

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.clear;
  dtmDados.IQuery1.SQL.add
    ('select count(*) tem from tb_email where cliente = :0 and destino = :1 and portaria is null and entrega is null ');
  dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := QDestinoCOD_CLIENTE.AsInteger;
  dtmDados.IQuery1.Open;
  if dtmDados.IQuery1.FieldByName('tem').value > 0 then
  begin
    dtmDados.IQuery1.Close;
    ShowMessage('J� existe Cadastro para este Cliente e Destino');
    Restaura;
    Exit;
  end;
  dtmDados.IQuery1.Close;

  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_email (id, cliente, destino, user_inc) ');
      dtmDados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_email), :0, :1, :2 )');
      dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QDestinoCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := GLBCodUser;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_email set cliente = :0, destino = :1,  ');
      dtmDados.IQuery1.SQL.add('user_alt = :2, dt_alt = sysdate ');
      dtmDados.IQuery1.SQL.add('where id = :3 ');
      dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QDestinoCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := GLBCodUser;
      dtmDados.IQuery1.Parameters[3].value := QEmailID.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QEmail.Close;
  QEmail.Open;
end;

procedure TfrmCadEmail.btnSalvarEClick(Sender: TObject);
begin
  if edemailE.text = '' then
  begin
    ShowMessage('N�o foi digitado e e-mail');
    edemailE.SetFocus;
    Exit;
  end;

  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_email (id, cliente, destino, entrega, user_inc, datinc) ');
      dtmDados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_email), :0, :1, :2, :3, sysdate) ');
      dtmDados.IQuery1.Parameters[0].value := QEmailCLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QEmailDESTINO.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := edemailE.text;
      dtmDados.IQuery1.Parameters[3].value := GLBCodUser;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_email set entrega = :0, user_alt  = :1, dt_alt = sysdate where id = :2 ');
      dtmDados.IQuery1.Parameters[0].value := edemailE.text;
      dtmDados.IQuery1.Parameters[1].value := GLBCodUser;
      dtmDados.IQuery1.Parameters[2].value := QPortariaID.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  RestauraE;
  tipo := '';
  QEntrega.Close;
  QEntrega.Open;

end;

procedure TfrmCadEmail.btnSalvarPClick(Sender: TObject);
begin
  if edemailP.text = '' then
  begin
    ShowMessage('N�o foi digitado e e-mail');
    edemailP.SetFocus;
    Exit;
  end;

  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_email (id, cliente, destino, portaria, user_inc, datinc) ');
      dtmDados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_email), :0, :1, :2, :3, sysdate) ');
      dtmDados.IQuery1.Parameters[0].value := QEmailCLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QEmailDESTINO.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := edemailP.text;
      dtmDados.IQuery1.Parameters[3].value := GLBCodUser;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_email set portaria = :0, user_alt  = :1, dt_alt = sysdate where id = :2 ');
      dtmDados.IQuery1.Parameters[0].value := edemailP.text;
      dtmDados.IQuery1.Parameters[1].value := GLBCodUser;
      dtmDados.IQuery1.Parameters[2].value := QPortariaID.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  RestauraP;
  tipo := '';
  QPortaria.Close;
  QPortaria.Open;
end;

procedure TfrmCadEmail.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QEmail.Close;
  QEmail.SQL.text := Copy(QEmail.SQL.text, 1, Pos('order by', QEmail.SQL.text) -
    1) + 'order by ' + Column.FieldName;
  QEmail.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadEmail.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QEmail.Cancel;
end;

procedure TfrmCadEmail.btnCancelarEClick(Sender: TObject);
begin
  RestauraE;
  QEntrega.Cancel;
end;

procedure TfrmCadEmail.btnCancelarPClick(Sender: TObject);
begin
  RestauraP;
  QPortaria.Cancel;
end;

procedure TfrmCadEmail.btnExcluirEClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este E-mail?',
    'Apagar E-mail Entrega', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add('delete from tb_email where id = :0 ');
      dtmDados.IQuery1.Parameters[0].value := QEntregaID.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    RestauraE;
    QEntrega.Close;
    QEntrega.Open;
  end;
end;

procedure TfrmCadEmail.btnExcluirPClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este E-mail?',
    'Apagar E-mail Portaria', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add('delete from tb_email where id = :0 ');
      dtmDados.IQuery1.Parameters[0].value := QPortariaID.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    RestauraP;
    QPortaria.Close;
    QPortaria.Open;
  end;
end;

procedure TfrmCadEmail.btnInserirPClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  RestauraP;
  tipo := 'I';
  edemailP.text := '';
  edemailP.SetFocus;
end;

procedure TfrmCadEmail.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  CBLCli.SetFocus;
end;

procedure TfrmCadEmail.btnAlterarEClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  RestauraE;
  tipo := 'A';
  edemailE.SetFocus;
end;

procedure TfrmCadEmail.btnAlterarPClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  RestauraP;
  tipo := 'A';
  edemailP.SetFocus;
end;

procedure TfrmCadEmail.FormCreate(Sender: TObject);
begin
  QCliente.Open;
  QDestino.Open;
  QPortaria.Open;
  QEntrega.Open;
  QEmail.Open;
  lblQuant.Caption := IntToStr(QEmail.RecNo) + #13#10 +
    IntToStr(QEmail.RecordCount);
end;

procedure TfrmCadEmail.QEmailAfterScroll(DataSet: TDataSet);
begin
  QPortaria.Close;
  QPortaria.Parameters[0].value := QEmailCLIENTE.AsInteger;
  QPortaria.Parameters[1].value := QEmailDESTINO.AsInteger;
  QPortaria.Open;

  QEntrega.Close;
  QEntrega.Parameters[0].value := QEmailCLIENTE.AsInteger;
  QEntrega.Parameters[1].value := QEmailDESTINO.AsInteger;
  QEntrega.Open;
end;

procedure TfrmCadEmail.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QEmail.Close;
  QCliente.Close;
  QDestino.Close;
  QPortaria.Close;
  QEntrega.Close;
end;

procedure TfrmCadEmail.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := True;
  end;
end;

procedure TfrmCadEmail.RestauraP;
begin
  btnInserirP.Enabled := not btnInserirP.Enabled;
  btnAlterarP.Enabled := not btnAlterarP.Enabled;
  btnExcluirP.Enabled := not btnExcluirP.Enabled;
  btnSalvarP.Enabled := not btnSalvarP.Enabled;
  btnCancelarP.Enabled := not btnCancelarP.Enabled;

  if Panel4.Visible then
  begin
    Panel4.Visible := false;
  end
  else
  begin
    Panel4.Visible := True;
  end;
end;

procedure TfrmCadEmail.RestauraE;
begin
  btnInserirE.Enabled := not btnInserirE.Enabled;
  btnAlterarE.Enabled := not btnAlterarE.Enabled;
  btnExcluirE.Enabled := not btnExcluirE.Enabled;
  btnSalvarE.Enabled := not btnSalvarE.Enabled;
  btnCancelarE.Enabled := not btnCancelarE.Enabled;

  if Panel5.Visible then
    Panel5.Visible := false
  else
    Panel5.Visible := True;
end;

procedure TfrmCadEmail.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QEmail.First;
    2:
      QEmail.Prior;
    3:
      QEmail.Next;
    4:
      QEmail.Last;
  end;
  lblQuant.Caption := IntToStr(QEmail.RecNo) + #13#10 +
    IntToStr(QEmail.RecordCount);
  Screen.Cursor := crDefault;
end;

end.
