unit RelBate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRelBate = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    SPBate: TADOStoredProc;
    QCtrcPRODUTO: TStringField;
    QCtrcWMS: TBCDField;
    QCtrcERP: TBCDField;
    QCtrcDELTA: TBCDField;
    QCtrcDESCRI: TStringField;
    QCtrcREG: TBCDField;
    QCtrcRESERVADO: TBCDField;
    QCtrcRESERVAR: TBCDField;
    JvDBNavigator1: TJvDBNavigator;
    Label7: TLabel;
    cbSite: TComboBox;
    RadioGroup1: TRadioGroup;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbSiteChange(Sender: TObject);
    procedure RadioGroup1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelBate: TfrmRelBate;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelBate.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_batimento_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmRelBate.btRelatorioClick(Sender: TObject);
begin
  QCtrc.close;
  if ledIdCliente.LookupValue = '' then
  begin
    showmessage('Escolha o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;

  if RadioGroup1.ItemIndex = -1 then
  begin
    showmessage('Escolha o Tipo do Sistema !!');
    RadioGroup1.setfocus;
    exit;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;
  SPBate.Parameters[0].value := ledIdCliente.LookupValue;
  SPBate.Parameters[1].value := cbSite.Text;
  if RadioGroup1.ItemIndex = 0 then
    SPBate.Parameters[2].value := 'C'
  else if RadioGroup1.ItemIndex = 1 then
    SPBate.Parameters[2].value := 'W'
  else
    SPBate.Parameters[2].value := 'V';
  SPBate.ExecProc;
  QCtrc.close;
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmRelBate.cbSiteChange(Sender: TObject);
begin
  QCliente.sql[2] := 'where w.lager = ' + QuotedStr(cbSite.Text);
  QCliente.open;
end;

procedure TfrmRelBate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmRelBate.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true;
end;

procedure TfrmRelBate.RadioGroup1Exit(Sender: TObject);
begin
  QCliente.close;
  if RadioGroup1.ItemIndex = 0 then
    QCliente.Connection := dtmDados.ConWms
  else if RadioGroup1.ItemIndex = 1 then
    QCliente.Connection := dtmDados.ConWmsWeb
  else
    QCliente.Connection := dtmDados.ConWMSV11;
end;

end.
