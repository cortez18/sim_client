unit Rel_provisao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRel_provisao = class(TForm)
    QCusto: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    JvDBNavigator1: TJvDBNavigator;
    SP_custo: TADOStoredProc;
    QCustoDOC: TBCDField;
    QCustoFILIAL: TBCDField;
    QCustoCLIENTE: TBCDField;
    QCustoRECEITA: TBCDField;
    QCustoCUSTO: TBCDField;
    QCustoSTATUS: TStringField;
    QCustoTRANSP: TBCDField;
    QCustoMAN_ROM: TBCDField;
    QCustoDT_DOC: TDateTimeField;
    QCustoTIP_DOC: TStringField;
    QCustoOPERACAO: TStringField;
    QCustoFILDOC: TBCDField;
    QCustoCOD_TAB: TBCDField;
    QCustoNM_CLIENTE: TStringField;
    QCustoNM_TRANSP: TStringField;
    QCustoNR_NF: TBCDField;
    QCustoCIDADE: TStringField;
    QCustoESTADO: TStringField;
    QCustoLOCAL: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_provisao: TfrmRel_provisao;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_provisao.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Provisao' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCusto.First;
  Gauge1.MaxValue := QCusto.RecordCount;
  while not QCusto.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCusto.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCusto.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmRel_provisao.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;

  SP_custo.Parameters[0].value := dtInicial.Text;
  SP_custo.Parameters[1].value := dtFinal.Text;
  SP_custo.ExecProc;

  QCusto.close;
  QCusto.open;
  Panel1.Visible := false;
end;

procedure TfrmRel_provisao.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_provisao.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_provisao.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_provisao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCusto.close;
end;

procedure TfrmRel_provisao.FormCreate(Sender: TObject);
begin
  DriveComboBox1.Enabled := true;
end;

procedure TfrmRel_provisao.JvDBGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCusto.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_provisao.JvDBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  QCusto.close;
  if Pos('order by', QCusto.SQL.Text) > 0 then
  begin
    QCusto.SQL.Text := copy(QCusto.SQL.Text, 1, Pos('order by', QCusto.SQL.Text)
      - 1) + 'order by ' + Column.FieldName
  end
  else
    QCusto.SQL.Text := QCusto.SQL.Text + ' order by ' + Column.FieldName;
  QCusto.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
    JvDBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

end.
