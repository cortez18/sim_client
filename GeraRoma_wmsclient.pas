unit GeraRoma_wmsclient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, ImgList, ComCtrls, JvMaskEdit,
  Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  ExtCtrls, DBCtrls, JvExStdCtrls, JvCombobox, System.ImageList;

Const
  InputBoxMessage = WM_USER + 200;

type
  TfrmGeraRoma_wmsclient = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblqt: TLabel;
    Query1: TADOQuery;
    mdTemp: TJvMemoryData;
    mdTempInserida: TIntegerField;
    mdTempreg: TIntegerField;
    dsTemp: TDataSource;
    iml: TImageList;
    TabSheet4: TTabSheet;
    lbTareFas_romaneio: TLabel;
    qrRomaneio: TADOQuery;
    dsRomaneio: TDataSource;
    dsTempRomaneio: TDataSource;
    mdTempromaneio: TJvMemoryData;
    mdTempromaneioSELECIONADO: TBCDField;
    mdTempromaneioNR_ROMANEIO: TBCDField;
    mdTempromaneioDESTINO: TStringField;
    mdTempromaneioPESO: TFloatField;
    mdTempnr_romaneio: TIntegerField;
    QMapaItens: TADOQuery;
    pnManifesto: TPanel;
    Label3: TLabel;
    edVcto: TJvDateEdit;
    Label4: TLabel;
    Label11: TLabel;
    edPlaca: TJvMaskEdit;
    Panel5: TPanel;
    btFatura: TBitBtn;
    btnNovoManifesto: TBitBtn;
    btnImpressao: TBitBtn;
    ds: TDataSource;
    dgTarefa: TJvDBUltimGrid;
    dbgFatura: TJvDBUltimGrid;
    mdTempromaneioDEMISSAO: TDateTimeField;
    Panel8: TPanel;
    lblDescricao: TLabel;
    edValor: TEdit;
    QTarefa: TADOQuery;
    qMapaCarga: TADOQuery;
    mdTempromaneioSUSUARIO: TStringField;
    mdTempromaneioPLACA: TStringField;
    mdTempromaneioVEICULO: TStringField;
    mdTempromaneioselecionou: TIntegerField;
    btnLimpaRomaneio: TBitBtn;
    qrRota: TADOQuery;
    dsRota: TDataSource;
    QDestino: TADOQuery;
    pnTarefas: TPanel;
    pgTarefas: TPageControl;
    tbRota: TTabSheet;
    dgRota: TJvDBUltimGrid;
    tbTarefa: TTabSheet;
    dbgColeta: TJvDBUltimGrid;
    Panel4: TPanel;
    Label16: TLabel;
    edTotVolumeTarefa: TJvCalcEdit;
    ckRota: TCheckBox;
    Label18: TLabel;
    Obs: TRichEdit;
    qrRotaNAME: TStringField;
    QDestinoNR_AUF: TStringField;
    qrRotaID_SPEDITEUR: TStringField;
    QTarefaNR_AUF: TStringField;
    QTarefaNAME: TStringField;
    QTarefaTRANSPORTADORA: TStringField;
    QTarefaPERCENT: TBCDField;
    qrRomaneioTRANSPORTADORA: TStringField;
    qrRomaneioTTVOL: TBCDField;
    qrRomaneioPERCENT: TBCDField;
    Label1: TLabel;
    qrRomaneioID_SPEDITEUR: TStringField;
    qMapaCargaNR_ROMANEIO: TBCDField;
    qMapaCargaMOTORISTA: TStringField;
    qMapaCargaPLACA: TStringField;
    qMapaCargaVEICULO: TStringField;
    qMapaCargaDTINC: TDateTimeField;
    qMapaCargaTRANSPORTADORA: TStringField;
    QMapaItensNR_AUF: TStringField;
    QMapaItensNAME: TStringField;
    QMapaItensPERCENT: TBCDField;
    edVeiculo: TComboBox;
    mdTemppedido: TStringField;
    Panel3: TPanel;
    pnBotao: TPanel;
    btRomaneio: TBitBtn;
    btnCarregar_Tarefas: TBitBtn;
    btnVoltaPedido: TBitBtn;
    edMotorista: TJvMaskEdit;
    tbImpresso: TTabSheet;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QImpressos: TADOQuery;
    dtsImpresso: TDataSource;
    QImpressosNR_ROMANEIO: TBCDField;
    QImpressosMOTORISTA: TStringField;
    QImpressosPLACA: TStringField;
    QImpressosVEICULO: TStringField;
    QImpressosDTINC: TDateTimeField;
    QImpressosTRANSPORTADORA: TStringField;
    QImpressosOPID_BEARB: TStringField;
    Panel6: TPanel;
    btnImpresso: TBitBtn;
    qMapaCargaOBSERVACAO: TStringField;
    QDestinoINSERIDA: TBCDField;
    QVolume: TADOQuery;
    QVolumeTTVOL: TBCDField;
    mdTempromaneiocod_transp: TStringField;
    QMapaItensCIDADE: TStringField;
    QMapaItensUF: TStringField;
    btnAlterar: TBitBtn;
    Panel7: TPanel;
    Label2: TLabel;
    cbCliente: TComboBox;
    Label5: TLabel;
    QCliente: TADOQuery;
    QClienteID_KLIENT: TStringField;
    QTarefaSEQ_TOUR: TBCDField;
    DBNavigator1: TDBNavigator;
    QImpressosID_KLIENT: TStringField;
    Edit1: TEdit;
    Label6: TLabel;
    qMapaCargaID_KLIENT: TStringField;
    Panel9: TPanel;
    Label38: TLabel;
    btnFechar: TBitBtn;
    cbOcorr: TJvComboBox;
    JvDBUltimGrid2: TJvDBUltimGrid;
    mdAltera: TJvMemoryData;
    dtsAlterar: TDataSource;
    mdAlteraReg: TIntegerField;
    mdAlteranr_romaneio: TIntegerField;
    mdAlteranf: TStringField;
    btnSair: TBitBtn;
    btnTMS: TBitBtn;
    QImpressosTMS: TStringField;
    Qexporta: TADOQuery;
    QexportaCNPJ_PAGADOR: TStringField;
    QexportaCNPJ_EMITENTE: TStringField;
    QexportaCNPJ_DESTINATARIO: TStringField;
    QexportaCNPJ_REDESPACHO: TStringField;
    QexportaNOTA_FISCAL: TStringField;
    QexportaSERIE_NOTA_FISCAL: TStringField;
    QexportaEMISSAO_NOTA_FISCAL: TDateTimeField;
    QexportaQTD_VOLUME: TBCDField;
    QexportaPESO: TBCDField;
    QexportaVALOR_TOTAL: TBCDField;
    QexportaCHAVE_NFE: TStringField;
    Panel10: TPanel;
    Edit2: TEdit;
    Edit3: TEdit;
    cbTodos: TCheckBox;
    QMapaItensnro_nf: TStringField;
    QMapaItensSEQ_TOUR: TBCDField;
    QImpressosDTSAIDA: TDateTimeField;
    Label7: TLabel;
    cbSite: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgColetaCellClick(Column: TColumn);
    procedure dbgColetaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btFaturaClick(Sender: TObject);
    procedure btRomaneioClick(Sender: TObject);
    procedure btnCarregar_TarefasClick(Sender: TObject);
    procedure btnLimpaRomaneioClick(Sender: TObject);
    procedure dbgFaturaCellClick(Column: TColumn);
    procedure dbgFaturaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCarregaClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure rgTarefasClick(Sender: TObject);
    procedure btnNovoManifestoClick(Sender: TObject);
    procedure dsTempRomaneioDataChange(Sender: TObject; Field: TField);
    procedure btnImpressaoClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure mdTempAfterOpen(DataSet: TDataSet);
    procedure tbTarefaShow(Sender: TObject);
    procedure btnVoltaPedidoClick(Sender: TObject);
    procedure ckRotaClick(Sender: TObject);
    procedure btnImpressoClick(Sender: TObject);
    procedure tbImpressoShow(Sender: TObject);
    procedure tbImpressoHide(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure qrRotaAfterOpen(DataSet: TDataSet);
    procedure cbClienteChange(Sender: TObject);
    procedure qrRomaneioBeforeOpen(DataSet: TDataSet);
    procedure QVolumeBeforeOpen(DataSet: TDataSet);
    procedure QMapaItensBeforeOpen(DataSet: TDataSet);
    procedure cbClienteExit(Sender: TObject);
    procedure QDestinoBeforeOpen(DataSet: TDataSet);
    procedure Edit1Exit(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure JvDBUltimGrid2CellClick(Column: TColumn);
    procedure JvDBUltimGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnSairClick(Sender: TObject);
    procedure QTarefaAfterOpen(DataSet: TDataSet);
    procedure btnTMSClick(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbTodosClick(Sender: TObject);
    procedure cbSiteChange(Sender: TObject);
    procedure QImpressosBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
    iRecDelete: string;
    procedure CarregaRomaneio();
    procedure CarregaTarefas();
    procedure CarregaRomaneioManifesto();
    procedure LimpaCamposManifesto();
    function RetornaTarefasSelecionadas(): String;
    procedure InputBoxSetPasswordChar(var Msg: TMessage);
      message InputBoxMessage;
  public
    { Public declarations }
    bInicio: Boolean;
    bLimpaTodos: Boolean;
    iLimpaRomaneio, iLimpaTarefa: Integer;
    sCli_os, sOs_tarvar, starefa: string;
    sTipoForm: String;
    procedure CarregaBtn();
  end;

var
  frmGeraRoma_wmsclient: TfrmGeraRoma_wmsclient;
  tt: real;
  dife, alterado: string;
  reg: Integer;

implementation

uses Dados, menu, funcoes, RomaneioRetira_client;

{$R *.dfm}

procedure TfrmGeraRoma_wmsclient.btnSairClick(Sender: TObject);
begin
  Panel9.Visible := FALSE;
end;

procedure TfrmGeraRoma_wmsclient.btnTMSClick(Sender: TObject);
var
  rom: Integer;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  rom := QImpressosNR_ROMANEIO.AsInteger;
  if QImpressosTMS.AsString = 'S' then
  begin
    ShowMessage('Romaneio J� Importado pelo TMS');
    Exit;
  end;

  Qexporta.close;
  Qexporta.Parameters[0].value := glbuser;
  Qexporta.Parameters[1].value := rom;
  Qexporta.ExecSQL;
  Qexporta.close;
  ShowMessage('Romaneio Exportado para o TMS');
  Qexporta.close;
  QImpressos.close;
  QImpressos.open;
  QImpressos.locate('nr_romaneio', rom, []);
end;

procedure TfrmGeraRoma_wmsclient.btFaturaClick(Sender: TObject);
begin
  mdTempromaneio.First;

  if edMotorista.Text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    edMotorista.SetFocus;
    Exit;
  end;

  if edPlaca.Text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    edPlaca.SetFocus;
    Exit;
  end;

  if edVeiculo.Text = '' then
  begin
    ShowMessage('Informe o ve�culo');
    edVeiculo.SetFocus;
    Exit;
  end;

  if Application.Messagebox('Finalizar Romaneio Agora ?', 'Finalizar Romaneio',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set opid_bearb = :0 ');
    Query1.sql.add('where percent = :1 ');
    Query1.Parameters[0].value := edPlaca.Text;
    Query1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;

    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('insert into tb_romaneio (nr_romaneio, cod_transp, motorista, placa, veiculo, observacao) ');
    Query1.sql.add('values (:0, :1, :2, :3, :4, :5 )');
    Query1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.Parameters[1].value := mdTempromaneiocod_transp.AsString;
    Query1.Parameters[2].value := edMotorista.Text;
    Query1.Parameters[3].value := ApCarac(edPlaca.Text);
    Query1.Parameters[4].value := edVeiculo.Text;
    Query1.Parameters[5].value := Obs.Text;
    Query1.ExecSQL;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Romaneio Finalizado');
    btFatura.Enabled := FALSE;
    // impress�o
    qMapaCarga.close;
    qMapaCarga.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    qMapaCarga.Parameters[1].value := cbSite.Text;
    qMapaCarga.open;
    QMapaItens.close;
    QMapaItens.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QMapaItens.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QMapaItens.open;
    try
      Application.CreateForm(TfrmRomaneioRetira_client,
        frmRomaneioRetira_client);
      frmRomaneioRetira_client.RlReport1.Preview;
    finally
      frmRomaneioRetira_client.Free;
    end;
    qMapaCarga.close;
    QMapaItens.close;
    mdTemp.close;
    mdTemp.open;
    CarregaTarefas();
    QTarefa.close;
    QTarefa.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTemppedido.value := QDestinoNR_AUF.AsString;
      QDestino.Next;
    end;
    QDestino.close;
  end;
  CarregaRomaneioManifesto();
  LimpaCamposManifesto();
  pnManifesto.Enabled := FALSE;
  dbgFatura.Enabled := True;
  btnImpressao.Enabled := True;
end;

procedure TfrmGeraRoma_wmsclient.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;

  if not QImpressosDTSAIDA.IsNull then
  begin
    ShowMessage('Este Romaneio j� saiu pela portaria !!');
    Exit;
  end;

  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('select distinct r.nr_romaneio, p.nr_auf  ');
  Query1.sql.add
    ('from tb_romaneio r left join auftraege p on p.percent = r.nr_romaneio ');
  Query1.sql.add('where p.percent = :0 ');
  Query1.sql.add('order by 2 ');
  Query1.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
  Query1.open;
  mdAltera.close;
  mdAltera.open;
  while not Query1.eof do
  begin
    mdAltera.append;
    mdAlteranf.value := Query1.FieldByName('nr_auf').value;
    mdAlteranr_romaneio.value := Query1.FieldByName('nr_romaneio').value;
    mdAlteraReg.value := 1;
    Query1.Next;
  end;
  mdAltera.First;

  Panel9.Visible := True;
  Panel9.SetFocus;
  cbOcorr.SetFocus;
end;

procedure TfrmGeraRoma_wmsclient.btnCarregaClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraRoma_wmsclient.btnCarregar_TarefasClick(Sender: TObject);
begin
  CarregaBtn();
end;

procedure TfrmGeraRoma_wmsclient.btnFecharClick(Sender: TObject);
begin
  if cbOcorr.Text = '' then
  begin
    ShowMessage('� Obrigat�rio escolher uma ocorr�ncia para a Altera��o !');
    cbOcorr.SetFocus;
  end
  else
  begin
    mdAltera.First;
    while not mdAltera.eof do
    begin
      if mdAlteraReg.value = 1 then
      begin
        Query1.close;
        Query1.sql.clear;
        Query1.sql.add
          ('insert into auftexte (LAGER, ID_KLIENT, NR_AUF, NR_AUF_TA, ');
        Query1.sql.add
          ('ART_TXT, NR_LFD, STAT, TXT, TIME_NEU, OPID_NEU, TIME_AEN, OPID_AEN, ');
        Query1.sql.add('LASTPROG, LASTUSER, LASTUPDT)');
        Query1.sql.add('values (''MAR'', :0, :1, 1, ''BT'', ');
        Query1.sql.add
          ('(select nvl(max(nr_lfd)+10,10) from auftexte where nr_auf = :2), ');
        Query1.sql.add(' ''00'', :3, sysdate, :4,');
        Query1.sql.add(' sysdate, :5, ''AK110'', ''WMS'', sysdate ) ');
        Query1.Parameters[0].value := QImpressosID_KLIENT.value;
        Query1.Parameters[1].value := mdAlteranf.value;
        Query1.Parameters[2].value := mdAlteranf.value;
        Query1.Parameters[3].value := cbOcorr.Text;
        Query1.Parameters[4].value := glbuser;
        Query1.Parameters[5].value := glbuser;
        Query1.ExecSQL;
        Query1.close;
      end;
      mdAltera.Next;
    end;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set opid_bearb = null ');
    Query1.sql.add('where percent = :0 and id_klient = ' +
      QuotedStr(QImpressosID_KLIENT.AsString));
    Query1.Parameters[0].value := QImpressosNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('update tb_romaneio set user_alt = :0, dtalteracao = sysdate ');
    Query1.sql.add('where nr_romaneio = :1 ');
    Query1.Parameters[0].value := glbuser;
    Query1.Parameters[1].value := QImpressosNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;

    Panel9.Visible := FALSE;
    QImpressos.close;
    QImpressos.open;
    CarregaBtn();
  end;
end;

procedure TfrmGeraRoma_wmsclient.btnLimpaRomaneioClick(Sender: TObject);
begin
  if mdTempromaneio.RecordCount = 0 then
    Exit;

  if Application.Messagebox('Exclui Romaneios agora ?', 'Excluir Romaneios',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('update auftraege set percent = null ');
    Query1.sql.add(' where percent = :0 ');
    Query1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add
      ('update tb_romaneio set user_del = :0, dtexclusao = sysdate ');
    Query1.sql.add('where nr_romaneio = :1');
    Query1.Parameters[0].value := glbuser;
    Query1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.ExecSQL;
    Query1.close;
    ShowMessage('Romaneio Exclu�dos');
    mdTempromaneio.close;
  end;
  CarregaRomaneioManifesto();
end;

procedure TfrmGeraRoma_wmsclient.btnImpressaoClick(Sender: TObject);
begin
  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
  qMapaCarga.Parameters[1].value := cbSite.Text;
  qMapaCarga.open;
  QMapaItens.close;
  QMapaItens.Parameters[0].value := cbSite.Text;
  QMapaItens.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
  QMapaItens.open;
  if (QMapaItensNR_AUF.AsString = '') then
  begin
    ShowMessage('Romaneio sem NF');
    Exit;
  end;
  try
    Application.CreateForm(TfrmRomaneioRetira_client, frmRomaneioRetira_client);
    frmRomaneioRetira_client.RlReport1.Preview;
  finally
    frmRomaneioRetira_client.Free;
  end;
  qMapaCarga.close;
  QMapaItens.close;
end;

procedure TfrmGeraRoma_wmsclient.btnImpressoClick(Sender: TObject);
begin
  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := cbSite.Text;
  qMapaCarga.Parameters[1].value := QImpressosNR_ROMANEIO.AsInteger;
  qMapaCarga.Parameters[2].value := cbSite.Text;
  qMapaCarga.open;

  QMapaItens.close;
  QMapaItens.Parameters[0].value := cbSite.Text;
  QMapaItens.Parameters[1].value := QImpressosNR_ROMANEIO.AsInteger;
  QMapaItens.open;

  try
    Application.CreateForm(TfrmRomaneioRetira_client, frmRomaneioRetira_client);
    frmRomaneioRetira_client.RlReport1.Preview;
  finally
    frmRomaneioRetira_client.Free;
  end;
  qMapaCarga.close;
  QMapaItens.close;
end;

procedure TfrmGeraRoma_wmsclient.btnNovoManifestoClick(Sender: TObject);
begin
  pnManifesto.Enabled := True;
  btnNovoManifesto.Enabled := FALSE;
  btFatura.Enabled := True;
  btnLimpaRomaneio.Enabled := FALSE;
  btnImpressao.Enabled := FALSE;
  dbgFatura.Enabled := FALSE;

  if edMotorista.CanFocus then
    edMotorista.SetFocus;
end;

procedure TfrmGeraRoma_wmsclient.btnVoltaPedidoClick(Sender: TObject);
begin
  iRecDelete := QTarefaNR_AUF.AsString;
  mdTemp.locate('pedido', QTarefaNR_AUF.AsString, []);
  edTotVolumeTarefa.value := edTotVolumeTarefa.value -
    QTarefaSEQ_TOUR.AsInteger;
  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.post;
  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('update auftraege set percent = null ');
  Query1.sql.add('where nr_auf = :0 and lager = :1 and id_klient = ' +
    QuotedStr(cbCliente.Text));
  Query1.Parameters[0].value := QTarefaNR_AUF.AsString;
  Query1.Parameters[1].value := cbSite.Text;
  Query1.ExecSQL;
  Query1.close;
  CarregaBtn();
end;

procedure TfrmGeraRoma_wmsclient.btRomaneioClick(Sender: TObject);
var
  irom: Integer;
  range, roma: string;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  roma := '';
  if QTarefa.RecordCount = 0 then
    Exit;
  range := QTarefa.sql[6];
  Query1.close;
  Query1.sql.clear;
  Query1.sql.add('select distinct p.id_spediteur from auftraege p ');
  Query1.sql.add('where p.stat in(75,95) and p.id_klient = ' +
    QuotedStr(cbCliente.Text));
  Query1.sql.add('and p.lager = ' + QuotedStr(cbSite.Text) +
    '  and p.opid_bearb is null ');
  Query1.sql.add(range);
  Query1.open;
  if Query1.RecordCount > 1 then
  begin
    if Application.Messagebox
      ('Existe mais de 1 transportador deseja mesmo fazer o romaneio ?',
      'Gerar Romaneio', MB_YESNO + MB_ICONQUESTION) = IDNO then
      Exit
    else
      roma := 'OK';
  end;
  if roma = '' then
  begin
    if Application.Messagebox('Gerar Romaneio agora ?', 'Gerar Romaneio',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
      roma := 'OK';
  end;
  if roma = 'OK' then
  begin
    Screen.Cursor := crHourGlass;
    QTarefa.First;
    Query1.close;
    Query1.sql.clear;
    Query1.sql.add('select nvl(max(percent)+1,1) roma from auftraege ');
    Query1.open;
    irom := Query1.FieldByName('roma').AsInteger;
    while not QTarefa.eof do
    begin
      Query1.close;
      Query1.sql.clear;
      Query1.sql.add('update auftraege set percent = :0 ');
      Query1.sql.add('where nr_auf = :1 and id_klient = ' +
        QuotedStr(cbCliente.Text));
      Query1.Parameters[0].value := irom;
      Query1.Parameters[1].value := QTarefaNR_AUF.AsString;
      Query1.ExecSQL;
      QTarefa.Next;
    end;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage(' Romaneio ' + IntToStr(irom) + ' Gerado');
    PageControl1.TabIndex := 1;
    QTarefa.close;
    CarregaRomaneio();
    CarregaRomaneioManifesto();
  end;
end;

procedure TfrmGeraRoma_wmsclient.CarregaBtn;
begin
  starefa := '';
  starefa := RetornaTarefasSelecionadas();
  if (starefa = '') then
    starefa := QuotedStr('0');
  QTarefa.close;
  QTarefa.sql[4] := ' and p.id_klient = ' + QuotedStr(cbCliente.Text);
  QTarefa.sql[5] := ' and p.lager = ' + QuotedStr(cbSite.Text);
  QTarefa.sql[6] := ' and p.nr_auf in (' + starefa + ')';
  QTarefa.open;
  // Qtarefa.sql.savetofile('c:\iw\sql.text');
  { QVolume.Close;
    QVolume.sql[5] := ' and p.nr_auf in ('  + starefa  + ')';
    QVolume.Open;
    if (starefa <> '') then
    begin
    edTotVolumeTarefa.Value := QVolumeTTVOL.AsInteger;
    end; }
  mdTemp.First;
end;

procedure TfrmGeraRoma_wmsclient.CarregaRomaneio();
begin
  CarregaTarefas();
  mdTemp.close;
  mdTemp.open;
  // carrega em temp tarefas n�o romaneadas
  QDestino.First;
  while not QDestino.eof do
  begin
    mdTemp.append;
    mdTempInserida.value := QDestinoINSERIDA.AsInteger;
    mdTemppedido.value := QDestinoNR_AUF.AsString;
    mdTemp.post;
    QDestino.Next;
  end;
  QDestino.close;
  CarregaBtn();
  mdTemp.First;
end;

procedure TfrmGeraRoma_wmsclient.CarregaRomaneioManifesto;
begin
  mdTempromaneio.close;
  mdTempromaneio.open;
  qrRomaneio.close;
  qrRomaneio.open;
  qrRomaneio.First;
  while not qrRomaneio.eof do
  begin
    mdTempromaneio.append;
    mdTempromaneioNR_ROMANEIO.value := qrRomaneioPERCENT.value;
    // mdTempromaneioDESTINO.Value     := '';
    // mdTempRomaneioDEMISSAO.Value    := '';
    mdTempromaneioPESO.value := qrRomaneioTTVOL.value;
    mdTempromaneioSUSUARIO.value := qrRomaneioTRANSPORTADORA.value;
    mdTempromaneiocod_transp.AsString := qrRomaneioID_SPEDITEUR.AsString;
    // mdTempromaneioPLACA.AsString    := '';
    // mdTempromaneioVEICULO.AsString  := '';
    mdTempromaneio.post;
    qrRomaneio.Next;
  end;
end;

procedure TfrmGeraRoma_wmsclient.CarregaTarefas();
begin
  if (ckRota.Checked = FALSE) then
  begin
    QDestino.close;
    QDestino.sql[5] := ' ';
    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;

      mdTemppedido.value := QDestinoNR_AUF.AsString;
      mdTempInserida.value := 1; // QDestinoINSERIDA.AsInteger;
      QDestino.Next;
    end;
    mdTemp.First;
  end
  else
  begin
    QDestino.close;
    QDestino.sql[5] := ' and p.id_spediteur = :rota ';
    QDestino.Parameters[0].value := qrRotaID_SPEDITEUR.AsString;
    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTemppedido.value := QDestinoNR_AUF.AsString;
      mdTempInserida.value := 1; // QDestinoINSERIDA.AsInteger;
      QDestino.Next;
    end;
    mdTemp.First;
  end;
end;

procedure TfrmGeraRoma_wmsclient.cbClienteChange(Sender: TObject);
var
  starefa_1: string;
begin
  if cbCliente.Text <> '' then
  begin
    qrRota.close;
    qrRota.sql[3] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
    qrRota.sql[4] := 'and t.lager = ' + QuotedStr(cbSite.Text);
    qrRota.open;
    qrRota.First;
    CarregaRomaneio();
    pgTarefas.ActivePageIndex := 0;
    reg := 0;
    PageControl1.TabIndex := 0;
    starefa_1 := RetornaTarefasSelecionadas();
    if (starefa_1 = '') then
      starefa_1 := QuotedStr('0');
    QTarefa.close;
    // qTarefa.sql[4] := ' and p.id_klient = ' + QuotedStr(cbCliente.Text);
    QTarefa.sql[6] := ' and p.nr_auf in (' + starefa_1 + ')';
    QTarefa.open;
    mdTemp.First;
    PageControl1.ActivePage := TabSheet4;
  end;
end;

procedure TfrmGeraRoma_wmsclient.ckRotaClick(Sender: TObject);
begin
  if (ckRota.Checked = FALSE) then
  begin
    tbRota.TabVisible := FALSE;
    pgTarefas.ActivePageIndex := 1;
  end
  else
  begin
    pgTarefas.ActivePageIndex := 0;
    tbRota.TabVisible := True;
  end;
end;

procedure TfrmGeraRoma_wmsclient.dbgColetaCellClick(Column: TColumn);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if (mdTempnr_romaneio.AsInteger > 0) then
  begin
    ShowMessage('Este Pedido j� esta Romaneado');
    Exit;
  end;
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    mdTempreg.value := reg;
    mdTempInserida.value := 1;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      mdTempromaneioPESO.AsFloat;
  end
  else
  begin
    mdTempInserida.value := 0;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      mdTempromaneioPESO.AsFloat;
  end;
  mdTemp.post;
end;

procedure TfrmGeraRoma_wmsclient.dbgColetaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgColeta.Canvas.FillRect(Rect);
      iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraRoma_wmsclient.dbgFaturaCellClick(Column: TColumn);
begin
  if (Column <> dbgFatura.Columns[0]) then
    abort;

  if (pnManifesto.Enabled) then
  begin
    ShowMessage(' Grave ou Cancele o Manifesto ');
    if edVcto.CanFocus then
      edVcto.SetFocus;
    Exit;
  end;

  if (Column = dbgFatura.Columns[0]) then
  begin
    mdTempromaneio.edit;
    if (mdTempromaneioSELECIONADO.IsNull) or
      (mdTempromaneioSELECIONADO.value = 0) then
    begin
      mdTempromaneioSELECIONADO.value := 1;
      mdTempromaneioselecionou.AsInteger := 1;
    end
    else
    begin
      mdTempromaneioSELECIONADO.value := 0;
      mdTempromaneioselecionou.AsInteger := 0;
    end;
    mdTempromaneio.post;
    btnNovoManifesto.Enabled := (mdTempromaneioSELECIONADO.value = 1);
    btnLimpaRomaneio.Enabled := btnNovoManifesto.Enabled;
  end;
end;

procedure TfrmGeraRoma_wmsclient.dbgFaturaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdTempromaneioSELECIONADO) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    //
    if mdTempromaneioSELECIONADO.value = 1 then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;

procedure TfrmGeraRoma_wmsclient.dsTempRomaneioDataChange(Sender: TObject;
  Field: TField);
begin
  btnNovoManifesto.Enabled := (mdTempromaneioSELECIONADO.AsInteger = 1) and
    ((not mdTempromaneioNR_ROMANEIO.IsNull));
  btnNovoManifesto.Enabled := btnNovoManifesto.Enabled;
end;

procedure TfrmGeraRoma_wmsclient.Edit1Exit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
begin
  pgTarefas.ActivePageIndex := 1;
  if (Alltrim(Edit1.Text) = '') then
    Exit;
  qrPesquisa := TADOQuery.Create(Nil);
  with qrPesquisa, sql do
  begin
    Connection := dtmDados.ConWms;
    add('select percent from auftraege t where t.nr_auf = :pedido and t.id_klient = :1');
    Parameters[0].value := Edit1.Text;
    Parameters[1].value := cbCliente.Text;
    open;
  end;

  if (qrPesquisa.RecordCount = 0) then
  begin
    ShowMessage('NF n�o Encontrada !!');
    FreeAndNil(qrPesquisa);
    Exit;
  end
  else
  begin
    QImpressos.locate('nr_romaneio', qrPesquisa.FieldByName('percent')
      .value, []);
  end;

end;

procedure TfrmGeraRoma_wmsclient.edValorExit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
  svalor: string;
begin
  pgTarefas.ActivePageIndex := 1;
  if (Alltrim(edValor.Text) = '') then
    Exit;
  svalor := edValor.Text;
  qrPesquisa := TADOQuery.Create(Nil);
  with qrPesquisa, sql do
  begin
    Connection := dtmDados.ConWms;
    add('select nr_auf from auftraege t where t.nr_auf = :pedido ');
    Parameters[0].value := edValor.Text;
    open;
  end;

  if (qrPesquisa.RecordCount = 0) then
  begin
    ShowMessage('Pedido n�o Encontrado !!');
    FreeAndNil(qrPesquisa);
    Exit;
  end;
  edValor.Text := qrPesquisa.FieldByName('nr_auf').AsString;
  edValor.Text := svalor;
end;

procedure TfrmGeraRoma_wmsclient.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qrRomaneio.close;
  QMapaItens.close;
  QDestino.close;
  Query1.close;
  QTarefa.close;
  mdTemp.close;
  mdTempromaneio.close;
  Action := caFree;
end;

procedure TfrmGeraRoma_wmsclient.FormCreate(Sender: TObject);
begin
  QCliente.open;
  cbCliente.clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteID_KLIENT.value);
    QCliente.Next;
  end;
  QCliente.close;
  bInicio := True;
  PageControl1.ActivePage := TabSheet4;
end;

procedure TfrmGeraRoma_wmsclient.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QImpressosTMS.value = 'S') then
  begin
    JvDBUltimGrid1.Canvas.Font.Color := clWhite;
    JvDBUltimGrid1.Canvas.Brush.Color := clGreen;
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    JvDBUltimGrid1.DefaultDrawDataCell(Rect, Column.Field, State);
  end;

  if (QImpressosTMS.value = 'X') then
  begin
    JvDBUltimGrid1.Canvas.Font.Color := clBlack;
    JvDBUltimGrid1.Canvas.Brush.Color := $000080FF;
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    JvDBUltimGrid1.DefaultDrawDataCell(Rect, Column.Field, State);
  end;
end;

procedure TfrmGeraRoma_wmsclient.JvDBUltimGrid2CellClick(Column: TColumn);
begin
  mdAltera.edit;
  //
  if (mdAlteraReg.IsNull) or (mdAlteraReg.value = 0) then
  begin
    mdAlteraReg.value := 1;
  end
  else
  begin
    mdAlteraReg.value := 0;
  end;
  mdAltera.post;
end;

procedure TfrmGeraRoma_wmsclient.JvDBUltimGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdAlteraReg) then
    begin
      JvDBUltimGrid2.Canvas.FillRect(Rect);
      iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdAlteraReg.value = 1 then
        iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(JvDBUltimGrid2.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraRoma_wmsclient.LimpaCamposManifesto;
begin
  edVcto.clear;
  // edhrcolprev.Clear;
  edMotorista.clear;
  edPlaca.clear;
  edVeiculo.ItemIndex := -1;
end;

procedure TfrmGeraRoma_wmsclient.mdTempAfterOpen(DataSet: TDataSet);
begin
  mdTemp.First;
end;

procedure TfrmGeraRoma_wmsclient.QDestinoBeforeOpen(DataSet: TDataSet);
begin
  QDestino.sql[3] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
  QDestino.sql[4] := 'and p.lager = ' + QuotedStr(cbSite.Text);
end;

procedure TfrmGeraRoma_wmsclient.QImpressosBeforeOpen(DataSet: TDataSet);
begin
  if cbSite.Text = '' then
  begin
    ShowMessage('Qual o Site ?');
  end
  else
  begin
    QImpressos.Parameters[0].value := cbSite.Text;
    QImpressos.Parameters[1].value := cbSite.Text;
  end;
end;

procedure TfrmGeraRoma_wmsclient.QMapaItensBeforeOpen(DataSet: TDataSet);
begin
  QMapaItens.sql[3] := 'where p.id_klient = ' +
    QuotedStr(qMapaCargaID_KLIENT.value) + ' and e.ID_EIGNER_1 =' +
    QuotedStr(qMapaCargaID_KLIENT.value) + 'and e.KL_EIGNER_2 = ''KU'' ';
  QMapaItens.Parameters[0].value := cbSite.Text;
end;

procedure TfrmGeraRoma_wmsclient.qrRomaneioBeforeOpen(DataSet: TDataSet);
begin
  qrRomaneio.sql[4] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
  qrRomaneio.sql[5] := 'and p.lager = ' + QuotedStr(cbSite.Text);
end;

procedure TfrmGeraRoma_wmsclient.qrRotaAfterOpen(DataSet: TDataSet);
begin
  dtmDados.WQuery1.close;
  dtmDados.WQuery1.sql.clear;
  dtmDados.WQuery1.sql.add
    ('select distinct count(p.nr_auf) quant from auftraege p left join spediteure t on p.id_spediteur = t.id_spediteur ');
  dtmDados.WQuery1.sql.add('where p.stat in(75,95) and p.opid_bearb is null ');
  dtmDados.WQuery1.sql.add('and p.id_klient = ' + QuotedStr(cbCliente.Text));
  dtmDados.WQuery1.sql.add('and p.lager = ' + QuotedStr(cbSite.Text));
  dtmDados.WQuery1.open;
  Label2.caption := 'Total de NF : ' + dtmDados.WQuery1.FieldByName
    ('quant').AsString;
  dtmDados.WQuery1.close;
end;

procedure TfrmGeraRoma_wmsclient.QTarefaAfterOpen(DataSet: TDataSet);
begin
  if starefa <> '' then
  begin
    QVolume.close;
    QVolume.sql[5] := ' and p.nr_auf in (' + starefa + ')';
    QVolume.open;
    edTotVolumeTarefa.value := QVolumeTTVOL.AsInteger;
  end
  else
    edTotVolumeTarefa.value := 0;
end;

procedure TfrmGeraRoma_wmsclient.QVolumeBeforeOpen(DataSet: TDataSet);
begin
  QVolume.sql[3] := 'and p.id_klient = ' + QuotedStr(cbCliente.Text);
  QVolume.sql[4] := 'and p.lager= ' + QuotedStr(cbSite.Text);
end;

function TfrmGeraRoma_wmsclient.RetornaTarefasSelecionadas: String;
var
  sTarSelec: String;
  iCount: Integer;
  // bEntrou: Boolean;
begin
  mdTemp.open;
  mdTemp.First;
  iCount := 0;
  mdTemp.DisableControls;
  if not(QTarefa.Active) then
    QTarefa.open;
  QTarefa.First;
  while not QTarefa.eof do
  begin
    if (QTarefaNR_AUF.AsString <> iRecDelete) then
    begin
      if iCount = 0 then
        sTarSelec := QuotedStr(QTarefaNR_AUF.AsString)
      else
        sTarSelec := sTarSelec + ',' + QuotedStr(QTarefaNR_AUF.AsString);
      inc(iCount);
    end;
    QTarefa.Next;
  end;
  iRecDelete := '';

  while not mdTemp.eof do
  begin
    if mdTempInserida.value = 1 then
    begin
      if iCount = 0 then
        sTarSelec := QuotedStr(mdTemppedido.AsString)
      else
        sTarSelec := sTarSelec + ',' + QuotedStr(mdTemppedido.AsString);
      inc(iCount);
    end;
    mdTemp.Next;
  end;
  mdTemp.EnableControls;
  result := sTarSelec;
end;

procedure TfrmGeraRoma_wmsclient.rgTarefasClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraRoma_wmsclient.TabSheet1Show(Sender: TObject);
begin
  CarregaRomaneioManifesto();
  if edVcto.CanFocus then
    edVcto.SetFocus;
  pnManifesto.Enabled := FALSE;
  btFatura.Enabled := FALSE;
  mdTempromaneio.First;
end;

procedure TfrmGeraRoma_wmsclient.TabSheet4Show(Sender: TObject);
var
  starefa_1: String;
begin
  starefa_1 := RetornaTarefasSelecionadas();
  if (starefa_1 = '') then
    starefa_1 := QuotedStr('0');
  QTarefa.close;
  QTarefa.sql[6] := ' and p.nr_auf in (' + starefa_1 + ')';
  QTarefa.open;
  mdTemp.First;
end;

procedure TfrmGeraRoma_wmsclient.tbImpressoHide(Sender: TObject);
begin
  QImpressos.close;
end;

procedure TfrmGeraRoma_wmsclient.tbImpressoShow(Sender: TObject);
begin
  QImpressos.open;
end;

procedure TfrmGeraRoma_wmsclient.tbTarefaShow(Sender: TObject);
begin
  CarregaTarefas();
end;

procedure TfrmGeraRoma_wmsclient.cbClienteExit(Sender: TObject);
begin
  // if cbCliente.Text = '' then
  // cbCliente.SetFocus;
  Label2.caption := '';
end;

procedure TfrmGeraRoma_wmsclient.cbSiteChange(Sender: TObject);
begin
  QCliente.sql[3] := 'and p.lager = ' + QuotedStr(cbSite.Text);
  QCliente.open;
  cbCliente.clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteID_KLIENT.value);
    QCliente.Next;
  end;
  QCliente.close;
end;

procedure TfrmGeraRoma_wmsclient.cbTodosClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if cbTodos.Checked = True then
  begin
    while not mdTemp.eof do
    begin
      mdTemp.edit;
      reg := reg + 1;
      mdTempreg.value := reg;
      mdTempInserida.value := 1;
      edTotVolumeTarefa.value := edTotVolumeTarefa.value +
        mdTempromaneioPESO.AsFloat;
      mdTemp.Next;
    end;
    mdTemp.First;
  end
  else
  begin
    while not mdTemp.eof do
    begin
      mdTemp.edit;
      mdTempInserida.value := 0;
      edTotVolumeTarefa.value := edTotVolumeTarefa.value -
        mdTempromaneioPESO.AsFloat;
      mdTemp.Next;
    end;
    mdTemp.First;
  end;

end;

procedure TfrmGeraRoma_wmsclient.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

end.
