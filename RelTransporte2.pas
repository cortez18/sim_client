unit RelTransporte2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls,
  JvCombobox;

type
  TfrmRelTransporte2 = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    Label5: TLabel;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    QCtrcNM_CLI: TStringField;
    QCtrcCNPJ_CLI: TStringField;
    QCtrcNM_CLIENTE_EXP: TStringField;
    QCtrcNR_CNPJ_CPF_EXP: TStringField;
    QCtrcNM_CLIENTE_DES: TStringField;
    QCtrcNR_CNPJ_CPF_DES: TStringField;
    QCtrcVL_NF: TBCDField;
    QCtrcVL_FRETE: TBCDField;
    QCtrcVL_PEDAGIO: TBCDField;
    QCtrcVL_SEGURO: TBCDField;
    QCtrcVL_GRIS: TBCDField;
    QCtrcVL_OUTROS: TBCDField;
    QCtrcVL_TOTAL: TBCDField;
    QCtrcVL_ALIQUOTA: TFloatField;
    QCtrcVL_IMPOSTO: TFloatField;
    QCtrcFL_TIPO: TWideStringField;
    QCtrcCIDADE_EXP: TStringField;
    QCtrcUF_EXP: TStringField;
    QCtrcCIDADE_DES: TStringField;
    QCtrcUF_DES: TStringField;
    QCtrcNM_CLIENTE_RED: TStringField;
    QCtrcNR_CNPJ_CPF_RED: TStringField;
    QCtrcCIDADE_RED: TStringField;
    QCtrcUF_RED: TStringField;
    QCtrcPESO_KG: TBCDField;
    QCtrcQT_VOLUME: TBCDField;
    QCtrcNR_FATURA: TBCDField;
    Label1: TLabel;
    JvDBLookupEdit1: TJvDBLookupEdit;
    JvDBLookupEdit2: TJvDBLookupEdit;
    QDestino: TADOQuery;
    DataSource2: TDataSource;
    QDestinoNM_CLIENTE: TStringField;
    QDestinoNR_CNPJ: TStringField;
    QCtrcNR_SERIE: TStringField;
    QCtrcCNPJ_TRANSPORTADORA: TStringField;
    QCtrcIESTADUAL: TStringField;
    QCtrcNR_NOTA: TStringField;
    QCtrcNR_CTRC: TBCDField;
    QCtrcDT_CTRC: TDateTimeField;
    QCtrcOPERACAO: TStringField;
    JvDBNavigator1: TJvDBNavigator;
    QCtrcCAT: TBCDField;
    QCtrcVL_ADEME: TBCDField;
    QCtrcVL_DESPACHO: TBCDField;
    Label9: TLabel;
    cbFilial: TJvComboBox;
    QSite: TADOQuery;
    QSiteFILIAL: TStringField;
    QSiteCODFIL: TBCDField;
    QCtrcCFOP_CLESS: TStringField;
    QCtrcTIPCON: TStringField;
    QCtrctipo_cte: TStringField;
    QCtrcPESO_CUB: TBCDField;
    QCtrcVL_DESCARGA: TBCDField;
    QCtrcVL_ENTREGA: TBCDField;
    QCtrcVL_TDE: TBCDField;
    QCtrcVL_ADICIONAL: TBCDField;
    QCtrcDESCRICAO_ADICIONAL: TStringField;
    QCtrcFATURA: TBCDField;
    QCtrcCTE_ID: TStringField;
    QCtrcNR_TABELA: TBCDField;
    QCtrcREGIAO: TStringField;
    QCtrcFL_STATUS: TStringField;
    QCtrcNM_CLIENTE_REM: TStringField;
    QCtrcNR_CNPJ_CPF_REM: TStringField;
    QCtrcCIDADE_REM: TStringField;
    QCtrcUF_REM: TStringField;
    QCtrcMANIFESTO: TBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBLookupEdit1CloseUp(Sender: TObject);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
    procedure QCtrcCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelTransporte2: TfrmRelTransporte2;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelTransporte2.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_transporte2_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmRelTransporte2.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  QCtrc.SQL.clear;
  QCtrc.SQL.Add('select * from VW_ITC_TRANSPORTE where nr_ctrc > 0 ');
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QCtrc.SQL.Add('and dt_ctrc between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')');
  if Trim(ledIdCliente.LookupValue) <> '' then
    QCtrc.SQL.Add('AND cnpj_cli = ''' + ledIdCliente.LookupValue + '''');
  if Trim(JvDBLookupEdit1.LookupValue) <> '' then
    QCtrc.SQL.Add('AND (nr_cnpj_cpf_red = ''' + JvDBLookupEdit1.LookupValue +
      ''' or nr_cnpj_cpf_des = ''' + JvDBLookupEdit1.LookupValue + ''' )');

  if cbfilial.text <> 'Todas' then
    QCtrc.SQL.Add('AND fl_empresa = ' + copy(cbfilial.text, 1,
      pos('-', cbfilial.text) - 1));

  QCtrc.SQL.Add('Order by nr_ctrc');

  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmRelTransporte2.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelTransporte2.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelTransporte2.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';

end;

procedure TfrmRelTransporte2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
  QDestino.close;
end;

procedure TfrmRelTransporte2.FormCreate(Sender: TObject);
begin
  QCliente.open;
  QDestino.open;
  DriveComboBox1.Enabled := true;
  QSite.open;
  cbFilial.clear;
  cbFilial.Items.Add('Todas');
  while not QSite.Eof do
  begin
    cbFilial.Items.Add(QSiteFILIAL.AsString);
    QSite.Next;
  end;
  QSite.Locate('codfil', GLBFilial, []);
  cbFilial.ItemIndex := cbFilial.Items.IndexOf(QSiteFILIAL.AsString);
end;

procedure TfrmRelTransporte2.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRelTransporte2.JvDBLookupEdit1CloseUp(Sender: TObject);
begin
  JvDBLookupEdit1.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  JvDBLookupEdit2.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmRelTransporte2.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmRelTransporte2.QClienteBeforeOpen(DataSet: TDataSet);
begin
  QCliente.Parameters[0].Value := GLBFilial;
end;

procedure TfrmRelTransporte2.QCtrcCalcFields(DataSet: TDataSet);
begin
  if QCtrcTIPCON.Value = 'R' then
    QCtrctipo_cte.Value := 'Reentrega'
  else if QCtrcTIPCON.Value = 'P' then
    QCtrctipo_cte.Value := 'Devolu��o'
  else if QCtrcTIPCON.Value = 'D' then
    QCtrctipo_cte.Value := 'Devolu��o'
  else if QCtrcTIPCON.Value = 'C' then
    QCtrctipo_cte.Value := 'Complementar'
  else
    QCtrctipo_cte.Value := 'Normal';
end;

end.
