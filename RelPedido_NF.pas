unit RelPedido_NF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRelPedido_NF = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    QCtrcPEDIDO: TStringField;
    QCtrcCODOSA: TBCDField;
    QCtrcCODNOT: TBCDField;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    QCtrcTIME_NEU: TDateTimeField;
    QCtrcDATINC: TDateTimeField;
    QCtrcDATEMI: TDateTimeField;
    JvDBNavigator1: TJvDBNavigator;
    QClienteNM_CLIENTE: TStringField;
    QClienteCODCLIFOR: TFMTBCDField;
    QClienteNOME_WMS: TStringField;
    QClienteBASE_WMS: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelPedido_NF: TfrmRelPedido_NF;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelPedido_NF.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_WmsxRodo_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmRelPedido_NF.btRelatorioClick(Sender: TObject);
begin
  if ledIdCliente.LookupValue = '' then
  begin
    showmessage('Escolha o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  QCtrc.SQL[1] := 'FROM AUFTRAEGE' + QClienteBASE_WMS.AsString + ' B';
  QCtrc.SQL[6] := 'and B.time_neu between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCtrc.Parameters[0].value := QClienteCODCLIFOR.AsInteger;
  QCtrc.Parameters[1].value := QClienteNM_CLIENTE.AsString;
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmRelPedido_NF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmRelPedido_NF.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true;
  dtInicial.date := date - 1;
  dtFinal.date := date;
end;

procedure TfrmRelPedido_NF.JvDBGrid1TitleClick(Column: TColumn);
begin
  if Pos('order by', QCtrc.SQL.Text) > 0 then
    QCtrc.SQL.Text := Copy(QCtrc.SQL.Text, 1, Pos('order by', QCtrc.SQL.Text) -
      1) + 'order by ' + Column.FieldName
  else
    QCtrc.SQL.Text := QCtrc.SQL.Text + ' order by ' + Column.FieldName;
  QCtrc.open;
end;

procedure TfrmRelPedido_NF.QClienteBeforeOpen(DataSet: TDataSet);
begin
  QCliente.Parameters[0].Value := GLBFilial;
end;

end.
