unit uLimpaRomaneio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, ADODB, DBCtrls, Buttons, Mask, JvExMask,
  JvToolEdit, JvBaseEdits, Vcl.ComCtrls;

type
  TfrmLimpaRomaneio = class(TForm)
    Panel1: TPanel;
    lblDescricao: TLabel;
    QTarefa: TADOQuery;
    ds: TDataSource;
    QTarefaUF_DESTINO: TStringField;
    cbTodos: TCheckBox;
    btnOk: TBitBtn;
    btnCancela: TBitBtn;
    cbUf: TComboBox;
    Label1: TLabel;
    edRomaneio: TJvCalcEdit;
    edTarefa: TJvCalcEdit;
    Label2: TLabel;
    edMotivo: TRichEdit;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnCancelaClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    sTela: String;
    sRoma: Integer;
    sRota: String;
  end;

var
  frmLimpaRomaneio: TfrmLimpaRomaneio;

implementation

uses Dados, menu , funcoes, GeraManifesto;

{$R *.dfm}

procedure TfrmLimpaRomaneio.btnCancelaClick(Sender: TObject);
begin
   ModalResult := mrCancel;
end;

procedure TfrmLimpaRomaneio.btnOkClick(Sender: TObject);
begin
  if (sTela = '1') then
  begin
    if (edMotivo.Text = '') then
    begin
      ShowMessage('� Obrigat�rio Colocar o Motivo do Cancelamento !');
      exit;
    end;

    sLimpaRomUF    := cbUf.Text;
    bLimpaTodos    := cbTodos.Checked;
    iLimpaRomaneio := edRomaneio.AsInteger;
    iLimpaTarefa   := edTarefa.AsInteger;
    iLimpaMotivo   := edMotivo.text;
  end;
  ModalResult := mrOk;
end;

procedure TfrmLimpaRomaneio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  action := caFree;
  frmLimpaRomaneio := nil;
end;

procedure TfrmLimpaRomaneio.FormCreate(Sender: TObject);
begin
  edMotivo.Clear;
end;

procedure TfrmLimpaRomaneio.FormShow(Sender: TObject);
begin
  if sRota <> '' then
  begin
    edTarefa.Enabled := false;
    cbUf.Enabled := false;
  end
  else
  begin
    QTarefa.Close;
    QTarefa.Parameters[0].Value := sRoma;
    QTarefa.Open;
    QTarefa.First;
    while not QTarefa.eof do
    begin
      cbUf.Items.Add(QTarefaUF_DESTINO.AsString);
      QTarefa.Next;
    end;
    cbUf.ItemIndex := 0;
    QTarefa.Close;
  end;
end;

end.
