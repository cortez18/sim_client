unit MonitImp_Cte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvMemoryDataset, Menus, ImgList, ActnList, ADODB, JvToolEdit,
  StdCtrls, Mask, JvExMask, JvBaseEdits, Buttons, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, System.ImageList, System.Actions, JvDBLookup,
  Vcl.DBCtrls;

type
  TfrmMonitImp_Cte = class(TForm)
    QImp: TADOQuery;
    dtsTarefas: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    iml: TImageList;
    pmLegenda: TPopupMenu;
    Legenda1: TMenuItem;
    Panel1: TPanel;
    lblQuant: TLabel;
    BitBtn18: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn15: TBitBtn;
    Label1: TLabel;
    QComp: TADOQuery;
    dtsComp: TDataSource;
    QCompCOMP: TStringField;
    QCompVALOR: TFloatField;
    QCompPARA: TStringField;
    QCustoTMS: TADOQuery;
    QCustoTMSTMS_FRETE_PESO: TBCDField;
    QCustoTMSTMS_FRETE_VALOR: TBCDField;
    QCustoTMSTMS_PEDAGIO: TBCDField;
    QCustoTMSTMS_GRIS: TBCDField;
    QCustoTMSTMS_OUTROS: TBCDField;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    QCustoTMSTMS_IMPOSTO: TBCDField;
    Qini: TADOQuery;
    QiniCODARQUIVO: TBCDField;
    QiniCGCEMISSORCONHE: TBCDField;
    QImpFILIAL: TBCDField;
    QImpNM_TRANSP: TStringField;
    QImpNM_REM: TStringField;
    QImpNM_DES: TStringField;
    QImpVALOR: TBCDField;
    QImpID: TBCDField;
    QImpEMITENTE: TStringField;
    QImpNUMERO: TBCDField;
    QOcs: TADOQuery;
    dtsOcs: TDataSource;
    QNF: TADOQuery;
    dtsNF: TDataSource;
    QOcsSEQUENCIA: TBCDField;
    QOcsNRO_OCS: TBCDField;
    QOcsDT_COLETA: TDateTimeField;
    QOcsDT_ENTREGA: TDateTimeField;
    QOcsDATAINC: TDateTimeField;
    QOcsCODCON: TBCDField;
    QOcsFILIAL: TBCDField;
    QNFNOTFIS: TStringField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFPESCUB: TBCDField;
    QNFVLRMER: TBCDField;
    QNFORDCOM: TStringField;
    QImpCODMAN: TBCDField;
    QImpCOD_TRANS: TBCDField;
    QCompVL_TRA: TBCDField;
    Panel5: TPanel;
    DBGrid1: TJvDBGrid;
    Panel4: TPanel;
    JvDBGrid4: TJvDBGrid;
    dtsXML_carga: TDataSource;
    QXml_Carga: TADOQuery;
    QXml_CargaID: TBCDField;
    QXml_CargaEMITENTE: TStringField;
    QXml_CargaCARGA: TStringField;
    QXml_CargaMEDIDA: TStringField;
    QXml_CargaQUANT: TBCDField;
    JvDBGrid5: TJvDBGrid;
    QXml_nf: TADOQuery;
    dtsXml_nf: TDataSource;
    QXml_nfID: TBCDField;
    QXml_nfEMITENTE: TStringField;
    QXml_nfNF: TStringField;
    QXml_nfSERIE: TStringField;
    QXml_nfCHAVE: TStringField;
    QXml_nfVALOR: TBCDField;
    Label6: TLabel;
    edValorMerT: TJvCalcEdit;
    QImpdatainc: TDateField;
    QImpusuario: TStringField;
    QImpchave: TStringField;
    btnExcel: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    Label14: TLabel;
    QFornecedor: TADOQuery;
    dtsFornecedor: TDataSource;
    ledCliente: TJvDBLookupEdit;
    QFornecedorcnpj: TStringField;
    QFornecedornm_fornecedor: TStringField;
    QImpobs: TStringField;
    DBMemo1: TDBMemo;
    Label2: TLabel;

    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure btRelatorioClick(Sender: TObject);
    procedure Atualizar;
    procedure FormCreate(Sender: TObject);
    procedure QOcsAfterOpen(DataSet: TDataSet);
    procedure btnExcelClick(Sender: TObject);
    procedure QImpAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitImp_Cte: TfrmMonitImp_Cte;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmMonitImp_Cte.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QImp.First;
    2:
      QImp.Prior;
    3:
      QImp.Next;
    4:
      QImp.Last;
  end;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
  Screen.Cursor := crDefault;
end;

procedure TfrmMonitImp_Cte.Atualizar;
begin
  Panel2.Visible := true;
  Application.ProcessMessages;
  QImp.close;
  QImp.sql[10] := 'Where cte.datainc between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';

  if ledCliente.LookupValue <> '' then
    QImp.sql[11] := 'and substr(cte.emitente,1,8) = ' +
      QuotedStr(ledCliente.LookupValue)
  else
    QImp.sql[11] := '';
  // showmessage(Qimp.sql.text);
  QImp.open;
  Panel2.Visible := false;
  QImp.First;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
end;

procedure TfrmMonitImp_Cte.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  memo1: TStringList;
  i: integer;
begin
  QImp.First;
  if not QImp.eof then
  begin
    self.Tag := 0;
    sarquivo := 'C:\SIM\Imp_XML_' + ApCarac(DateToStr(date)) + '.xls';
    memo1 := TStringList.Create;
    memo1.Add('  <HTML>');
    memo1.Add('    <HEAD>');
    memo1.Add('      <TITLE>IW - Intecom</TITLE>');
    memo1.Add(
      '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
    memo1.Add(
      '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo1.Add(
      '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
    memo1.Add('         </STYLE>');
    memo1.Add('    </HEAD>');
    memo1.Add('    <BODY <Font Color="#004080">');
    memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

    memo1.Add('</th></font></Center>');
    memo1.Add('</tr>');
    memo1.Add('</B></font>');
    memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
    memo1.Add('<TBODY>');
    memo1.Add('<tr>');
    for i := 0 to DBGrid1.Columns.Count - 1 do
    begin
      memo1.Add('<th><FONT class=texto1>' + DBGrid1.Columns[i].Title.Caption
        + '</th>');
    end;
    memo1.Add('</tr>');
    QImp.First;
    while not QImp.eof do
    begin
      for i := 0 to DBGrid1.Columns.Count - 1 do
        memo1.Add('<th><FONT class=texto2>' +
          QImp.fieldbyname(DBGrid1.Columns[i].FieldName).AsString + '</th>');
      QImp.Next;
      memo1.Add('<tr>');
    end;
    memo1.Add('</TBODY>');
    memo1.Add('</table>');
    memo1.SaveToFile(sarquivo);
    showmessage('Planilha salva em ' + sarquivo);
    self.Tag := 1;
  end;
end;

procedure TfrmMonitImp_Cte.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('Voc� n�o escolheu a Data Inicial');
    exit;
  end;

  if copy(dtFinal.Text, 1, 2) = '  ' then
  begin
    showmessage('Voc� n�o escolheu a Data Final');
    exit;
  end;

  if dtFinal.date < dtInicial.date then
  begin
    showmessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
  self.Cursor := crHourGlass;
  Atualizar;
  self.Cursor := crDefault;
end;

procedure TfrmMonitImp_Cte.DBGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + DBGrid1.Columns.Items
    [DBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QImp.Locate(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName,
    Retorno, [loPartialKey]) then
    showmessage(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmMonitImp_Cte.DBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  QImp.close;
  if Pos('order by', QImp.sql.Text) > 0 then
    QImp.sql.Text := copy(QImp.sql.Text, 1, Pos('order by', QImp.sql.Text) - 1)
      + 'order by ' + Column.FieldName
  else
    QImp.sql.Text := QImp.sql.Text + ' order by ' + Column.FieldName;
  QImp.open;
  // Muda a cor da coluna do grid
  for icount := 0 to DBGrid1.Columns.Count - 1 do
    DBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;

end;

procedure TfrmMonitImp_Cte.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QImp.close;
  Qini.close;
  QComp.close;
  QCustoTMS.close;
  QXml_Carga.close;
  QXml_nf.close;
  QFornecedor.close;
end;

procedure TfrmMonitImp_Cte.FormCreate(Sender: TObject);
begin
  QFornecedor.open;
  dtInicial.date := date - 1;
  dtFinal.date := date;
  QOcs.open;
  self.Tag := 0;
  btRelatorioClick(Sender);
  self.Tag := 1;
end;

procedure TfrmMonitImp_Cte.QImpAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
  Screen.Cursor := crDefault;
  if self.Tag > 0 then
  begin
    QOcs.close;
    QOcs.Parameters[0].value := QImpID.AsInteger;
    QOcs.Parameters[1].value := QImpEMITENTE.AsString;
    QOcs.open;
    QXml_Carga.close;
    QXml_Carga.Parameters[0].value := QImpID.AsInteger;
    QXml_Carga.Parameters[1].value := QImpEMITENTE.AsString;
    QXml_Carga.open;
    QXml_nf.close;
    QXml_nf.Parameters[0].value := QImpID.AsInteger;
    QXml_nf.Parameters[1].value := QImpEMITENTE.AsString;
    QXml_nf.open;
    edValorMerT.value := 0;
    while not QXml_nf.eof do
    begin
      edValorMerT.value := edValorMerT.value + QXml_nfVALOR.value;
      QXml_nf.Next;
    end;
  end;
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add('call sp_xml_cte_integra(:1,:2)');
  dtmDados.IQuery1.Parameters[0].value := QImpNUMERO.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := QImpEMITENTE.AsString;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.Close;
end;

procedure TfrmMonitImp_Cte.QOcsAfterOpen(DataSet: TDataSet);
begin
  // edPesoKg.value := 0;
  // edPesoCub.value := 0;
  // edValorMer.value := 0;
  QNF.close;
  QNF.Parameters[0].value := QOcsCODCON.AsInteger;
  QNF.Parameters[1].value := QOcsFILIAL.AsInteger;
  QNF.open;
  { while not QNF.eof do
    begin
    edPesoKg.value := edPesoKg.value + QNFPESOKG.Value;
    edPesoCub.value := edPesoCub.value + QNFPESCUB.value;
    edValorMer.value := edValorMer.value + QNFVLRMER.value;
    QNF.next;
    end; }
end;

end.
