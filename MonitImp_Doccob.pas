unit MonitImp_Doccob;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvMemoryDataset, Menus, ImgList, ActnList, ADODB, JvToolEdit,
  StdCtrls, Mask, JvExMask, JvBaseEdits, Buttons, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, System.ImageList, System.Actions, JvDBLookup,
  Vcl.DBCtrls;

type
  TfrmMonitImp_Doccob = class(TForm)
    QImp: TADOQuery;
    dtsImp: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    iml: TImageList;
    pmLegenda: TPopupMenu;
    Legenda1: TMenuItem;
    Panel1: TPanel;
    lblQuant: TLabel;
    BitBtn18: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn15: TBitBtn;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    QCTe: TADOQuery;
    dtsCTE: TDataSource;
    Panel5: TPanel;
    DBGrid1: TJvDBGrid;
    btnExcel: TBitBtn;
    Panel2: TPanel;
    Label14: TLabel;
    QFornecedor: TADOQuery;
    dtsFornecedor: TDataSource;
    ledCliente: TJvDBLookupEdit;
    QImpCODARQUIVO: TFMTBCDField;
    QImpIDENTIFICACAO: TStringField;
    QImpREMETENTE: TStringField;
    QImpDESTINATARIO: TStringField;
    QImpDATINC: TDateTimeField;
    QImpDATALT: TDateTimeField;
    QImpIND_CONTROLE: TFMTBCDField;
    QImpARQUIVO: TStringField;
    QImpTIPO_DOC: TStringField;
    QImpFATURA: TFMTBCDField;
    QImpDATA: TDateTimeField;
    QImpVCTO: TDateTimeField;
    QImpVALOR: TBCDField;
    QImpIND_CONTROLE_1: TFMTBCDField;
    QImpCODARQUIVO_1: TFMTBCDField;
    QCTeNUMERO: TFMTBCDField;
    QCTeSERIE: TStringField;
    QFornecedorREMETENTE: TStringField;
    QCF: TADOQuery;
    dtsCF: TDataSource;
    QCFfatura: TStringField;
    QImpCNPJ: TStringField;
    QCFcte_rec: TBCDField;
    QCFnr_doc: TBCDField;
    QCFcodclifor: TBCDField;
    Panel3: TPanel;
    JvDBGrid2: TJvDBGrid;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    btnRep: TBitBtn;
    QA: TADOQuery;
    QAcodman: TBCDField;
    QAcod_trans: TBCDField;
    QAcoddoc: TBCDField;
    QAvalor: TFloatField;
    QAnumero: TBCDField;
    QAfilial: TBCDField;
    QTDE: TADOQuery;
    QTDEtem: TBCDField;

    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure btRelatorioClick(Sender: TObject);
    procedure Atualizar;
    procedure FormCreate(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure QImpAfterScroll(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QCTeAfterScroll(DataSet: TDataSet);
    procedure btnRepClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitImp_Doccob: TfrmMonitImp_Doccob;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmMonitImp_Doccob.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QImp.First;
    2:
      QImp.Prior;
    3:
      QImp.Next;
    4:
      QImp.Last;
  end;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
  Screen.Cursor := crDefault;
end;

procedure TfrmMonitImp_Doccob.Atualizar;
begin
  Panel2.Visible := true;
  Application.ProcessMessages;
  QImp.close;
  QImp.sql[4] := 'and a.datinc between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';

  if ledCliente.LookupValue <> '' then
    QImp.sql[5] := 'and a.remetente = ' + QuotedStr(ledCliente.LookupValue)
  else
    QImp.sql[5] := '';

  QImp.open;
  Panel2.Visible := false;
  QImp.First;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
end;

procedure TfrmMonitImp_Doccob.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  memo1: TStringList;
  i: integer;
begin
  QImp.First;
  if not QImp.eof then
  begin
    self.Tag := 0;
    sarquivo := 'C:\SIM\Imp_Doccob' + ApCarac(DateToStr(date)) + '.xls';
    memo1 := TStringList.Create;
    memo1.Add('<HTML>');
    memo1.Add('  <HEAD>');
    memo1.Add('    <TITLE>IW - Intecom</TITLE>');
    memo1.Add(
      '       <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
    memo1.Add(
      '              .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo1.Add(
      '              .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
    memo1.Add('       </STYLE>');
    memo1.Add('  </HEAD>');
    memo1.Add('  <Table border=2 bordercolor="#005CB9" align=center>');
    memo1.Add('    <tr>');
    memo1.Add('      <td><FONT class=texto1>Remetente : ' +
      QImpREMETENTE.AsString + ' - Fatura : ' + QImpFATURA.AsString +
      '</Font></td>');
    memo1.Add('    </tr>');
    memo1.Add('  </Table>');
    memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
    memo1.Add('<TBODY>');
    memo1.Add('<tr>');
    for i := 0 to JvDBGrid2.Columns.Count - 1 do
    begin
      memo1.Add('<th><FONT class=texto2>' + JvDBGrid2.Columns[i].Title.Caption
        + '</th>');
    end;
    memo1.Add('</tr>');
    QCTe.First;
    while not QCTe.eof do
    begin
      for i := 0 to JvDBGrid2.Columns.Count - 1 do
        memo1.Add('<th><FONT class=texto2>' + QCTe.fieldbyname(JvDBGrid2.Columns
          [i].FieldName).AsString + '</th>');
      QCTe.Next;
      memo1.Add('<tr>');
    end;
    memo1.Add('</TBODY>');
    memo1.Add('</table>');
    memo1.SaveToFile(sarquivo);
    showmessage('Planilha salva em ' + sarquivo);
    self.Tag := 1;
  end;
end;

procedure TfrmMonitImp_Doccob.btnRepClick(Sender: TObject);
var
  tde, ok: String;
begin
  QCTe.First;
  while not QCTe.eof do
  begin
    if QCFfatura.IsNull then
    begin
      // CTE s� falta incluir a fatura
      if not QCFcte_rec.IsNull then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.Clear;
        dtmdados.IQuery1.sql.Add
          ('update tb_controle_custo set fatura = :0 where codclifor = :1 and cte_custo = :2');
        dtmdados.IQuery1.Parameters[0].value := QImpFATURA.AsString;
        dtmdados.IQuery1.Parameters[1].value := QCFcodclifor.AsInteger;
        dtmdados.IQuery1.Parameters[2].value := QCTeNUMERO.AsString;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.IQuery1.close;
      end
      else
      // N�o tem nada procura
      begin
        ok := 'S';
        QA.close;
        QA.Parameters[0].value := QCTeNUMERO.AsInteger;
        QA.Parameters[1].value := QImpCNPJ.AsString;
        QA.open;

        if QA.eof then
        begin
          showmessage('XML do CT-e do Parceiro n�o foi importado !');
          ok := 'N';
        end;

        if ok = 'S' then
        begin
          QTDE.close;
          QTDE.Parameters[0].value := QCTeNUMERO.AsInteger;
          QTDE.Parameters[1].value := QImpCNPJ.AsString;
          QTDE.open;
          if QTDEtem.value = 0 then
            tde := 'N'
          else
            tde := 'S';

          dtmdados.IQuery1.close;
          dtmdados.IQuery1.sql.Clear;
          dtmdados.IQuery1.sql.Add
            ('select * from tb_controle_custo a where a.codclifor = :0 and a.cte_rec = :1 and a.nr_doc = :2 and cte_custo is null and fatura is null and nvl(status,''N'') = ''N'' ');
          dtmdados.IQuery1.Parameters[0].value := QAcod_trans.AsInteger;
          dtmdados.IQuery1.Parameters[1].value := QAcoddoc.AsInteger;
          dtmdados.IQuery1.Parameters[2].value := QAcodman.AsInteger;
          dtmdados.IQuery1.open;
          if dtmdados.IQuery1.eof then
          begin
            dtmdados.IQuery1.close;
            dtmdados.IQuery1.sql.Clear;
            dtmdados.IQuery1.sql.Add
              ('update tb_controle_custo a set a.cte_custo = :0, a.valor_cobrado = :1, a.fl_tde = :2, a.fatura = :3 ');
            dtmdados.IQuery1.sql.Add
              ('where a.codclifor = :4 and a.cte_rec = :5 and a.nr_doc = :6');
            dtmdados.IQuery1.Parameters[0].value := QAnumero.AsInteger;
            dtmdados.IQuery1.Parameters[1].value := QAvalor.value;
            dtmdados.IQuery1.Parameters[2].value := tde;
            dtmdados.IQuery1.Parameters[3].value := QImpFATURA.AsString;
            dtmdados.IQuery1.Parameters[4].value := QAcod_trans.AsInteger;
            dtmdados.IQuery1.Parameters[5].value := QAcoddoc.AsInteger;
            dtmdados.IQuery1.Parameters[6].value := QAcodman.AsInteger;
            dtmdados.IQuery1.ExecSQL;
            dtmdados.IQuery1.close;
          end;
        end;
      end;
    end;
    QCTe.Next;
  end;
  QCTe.close;
  QCTe.open;
end;

procedure TfrmMonitImp_Doccob.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('Voc� n�o escolheu a Data Inicial');
    exit;
  end;

  if copy(dtFinal.Text, 1, 2) = '  ' then
  begin
    showmessage('Voc� n�o escolheu a Data Final');
    exit;
  end;

  if dtFinal.date < dtInicial.date then
  begin
    showmessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
  self.Cursor := crHourGlass;
  Atualizar;
  self.Cursor := crDefault;
end;

procedure TfrmMonitImp_Doccob.DBGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + DBGrid1.Columns.Items
    [DBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QImp.Locate(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName,
    Retorno, [loPartialKey]) then
    showmessage(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmMonitImp_Doccob.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F3) then
  begin
    if Application.Messagebox('Voc� Deseja Excluir Este DOCCOB ?',
      'Excluir DOCCOB', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add
        ('delete from tb_doccob_cte where ind_controle = :codarquivo_1');
      dtmdados.IQuery1.Parameters[0].value := QImpCODARQUIVO_1.AsInteger;
      dtmdados.IQuery1.ExecSQL;

      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add
        ('delete from tb_doccob_fat where codarquivo = :codarquivo_1');
      dtmdados.IQuery1.Parameters[0].value := QImpCODARQUIVO_1.AsInteger;
      dtmdados.IQuery1.ExecSQL;

      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add
        ('delete from tb_doccob_tra where codarquivo = :ind_controle_1');
      dtmdados.IQuery1.Parameters[0].value := QImpIND_CONTROLE_1.AsInteger;
      dtmdados.IQuery1.ExecSQL;

      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add
        ('delete from tb_doccob_unb where codarquivo = :codarquivo');
      dtmdados.IQuery1.Parameters[0].value := QImpCODARQUIVO.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;
      showmessage('DOCCOB Exclu�do !');

      QImp.close;
      QImp.open;

    end;
  end;

end;

procedure TfrmMonitImp_Doccob.DBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  QImp.close;
  if Pos('order by', QImp.sql.Text) > 0 then
    QImp.sql.Text := copy(QImp.sql.Text, 1, Pos('order by', QImp.sql.Text) - 1)
      + 'order by ' + Column.FieldName
  else
    QImp.sql.Text := QImp.sql.Text + ' order by ' + Column.FieldName;
  QImp.open;
  // Muda a cor da coluna do grid
  for icount := 0 to DBGrid1.Columns.Count - 1 do
    DBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;

end;

procedure TfrmMonitImp_Doccob.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QImp.close;
  QCTe.close;
end;

procedure TfrmMonitImp_Doccob.FormCreate(Sender: TObject);
begin
  QFornecedor.open;
  dtInicial.date := date - 1;
  dtFinal.date := date;
  self.Tag := 0;
  btRelatorioClick(Sender);
  self.Tag := 1;
end;

procedure TfrmMonitImp_Doccob.QCTeAfterScroll(DataSet: TDataSet);
begin
  QCF.close;
  QCF.Parameters[0].value := QImpCNPJ.AsString;
  QCF.Parameters[1].value := QCTeNUMERO.AsInteger;
  QCF.open;
end;

procedure TfrmMonitImp_Doccob.QImpAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  lblQuant.Caption := IntToStr(QImp.RecNo) + #13#10 +
    IntToStr(QImp.RecordCount);
  Screen.Cursor := crDefault;
  QCTe.close;
  QCTe.Parameters[0].value := QImpCODARQUIVO_1.AsInteger;
  QCTe.open;
end;

end.
