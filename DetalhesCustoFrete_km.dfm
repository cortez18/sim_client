object frmDetalhesCustoFrete_km: TfrmDetalhesCustoFrete_km
  Left = 0
  Top = 0
  Caption = 'Detalhes KM'
  ClientHeight = 359
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvDBGrid3: TJvDBGrid
    Left = 0
    Top = 0
    Width = 484
    Height = 359
    Align = alClient
    DataSource = dtskm
    DrawingStyle = gdsClassic
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'codcep'
        Title.Caption = 'CEP'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'codclifor'
        Title.Caption = 'Cod. Dest.'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRI'
        Title.Caption = 'Cidade'
        Width = 242
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ESTADO'
        Title.Caption = 'UF'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'km'
        Width = 52
        Visible = True
      end>
  end
  object dtskm: TDataSource
    DataSet = mdKm
    Left = 38
    Top = 32
  end
  object QKM: TADOQuery
    Active = True
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 45
      end
      item
        Name = '1'
        DataType = ftWideString
        Size = 2
        Value = '20'
      end
      item
        Name = '2'
        DataType = ftWideString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      
        'select * from (select distinct i.manifesto, i.filial, i.serie, l' +
        'pad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)),8,0) codcep, m.' +
        'descri, m.estado, (d.latitu ||'#39'+'#39'|| d.longit) geo, d.codclifor '
      
        'from tb_manitem i left join tb_conhecimento c on i.coddoc = c.nr' +
        '_conhecimento and i.fildoc = c.fl_empresa and i.tipdoc = '#39'C'#39' '
      
        'left join cyber.rodcli d on d.codclifor = (case when c.cod_redes' +
        'pacho > 0 then c.cod_redespacho else c.cod_destinatario end) '
      'left join cyber.rodmun m on m.codmun = d.codmun '
      'union all '
      
        'select distinct i.manifesto, i.filial, i.serie, lpad(fnc_remove_' +
        'caracter(REMOVE_ESPACO(d.codcep)),8,0) codcep, m.descri, m.estad' +
        'o, (d.latitu ||'#39'+'#39'|| d.longit) geo, d.codclifor '
      
        'from tb_manitem i left join cyber.rodord c on i.coddoc = c.codig' +
        'o and i.fildoc = c.codfil and i.tipdoc = '#39'O'#39' '
      
        'left join cyber.rodcli d on d.codclifor = (case when c.terent > ' +
        '0 then c.terent else c.coddes end) '
      'left join cyber.rodmun m on m.codmun = d.codmun) '
      
        'where manifesto = :0 and filial = :1 and serie = :2 and codclifo' +
        'r is not null order by 4 ')
    Left = 98
    Top = 96
    object QKMCODCEP: TStringField
      FieldName = 'CODCEP'
      ReadOnly = True
      Size = 8
    end
    object QKMDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
    object QKMESTADO: TStringField
      FieldName = 'ESTADO'
      ReadOnly = True
      Size = 2
    end
    object QKMKM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'KM'
      Calculated = True
    end
    object QKMGEO: TStringField
      FieldName = 'GEO'
      ReadOnly = True
      Size = 81
    end
    object QKMCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
  end
  object XMLDoc: TXMLDocument
    Left = 38
    Top = 96
    DOMVendorDesc = 'MSXML'
  end
  object mdKm: TJvMemoryData
    FieldDefs = <
      item
        Name = 'CODCEP'
        Attributes = [faReadonly]
        DataType = ftBCD
        Precision = 38
        Size = 4
      end
      item
        Name = 'DESCRI'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ESTADO'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 2
      end>
    DataSet = QKM
    Left = 152
    Top = 96
    object mdKmcodcep: TStringField
      FieldName = 'codcep'
      Size = 10
    end
    object mdkmdescri: TStringField
      FieldName = 'DESCRI'
      Size = 40
    end
    object mdKMESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object mdKmkm: TFloatField
      FieldName = 'km'
      DisplayFormat = '##,##0.00'
    end
    object mdKmgeo: TStringField
      FieldName = 'geo'
      Size = 80
    end
    object mdKmcodclifor: TIntegerField
      FieldName = 'codclifor'
    end
  end
end
