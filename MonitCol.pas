unit MonitCol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvFullColorSpaces, JvMemoryDataset, Menus, ImgList, ActnList,
  ADODB, StdCtrls, DBCtrls, ComCtrls, JvExStdCtrls, JvCombobox,
  JvFullColorCtrls, Mask, JvExMask, JvToolEdit, Buttons, ExtCtrls, JvExControls,
  JvGradient, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvExComCtrls, JvComCtrls,
  ComObj, ShellAPI, IniFiles, JvBaseEdits, JvMaskEdit;

Const
  InputBoxMessage = WM_USER + 200;

type
  TfrmMonitCol = class(TForm)
    Qtarefas: TADOQuery;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    JvPageControl1: TJvPageControl;
    TBColetas: TTabSheet;
    QNF: TADOQuery;
    dtsNF: TDataSource;
    dtsMonito: TDataSource;
    iml: TImageList;
    Panel1: TPanel;
    Qocorr: TADOQuery;
    dtsocorr: TDataSource;
    DBGrid1: TJvDBGrid;
    pnLegenda: TPanel;
    JvGradient8: TJvGradient;
    lblColetaHoje: TJvFullColorLabel;
    lblColetaAtrasada: TJvFullColorLabel;
    pmLegenda: TPopupMenu;
    Legenda1: TMenuItem;
    lblColetaPontual: TJvFullColorLabel;
    QCliente: TADOQuery;
    QClienteNM_CLI_OS: TStringField;
    SMS1: TJvFullColorLabel;
    SMS2: TJvFullColorLabel;
    SMS3: TJvFullColorLabel;
    SMS9: TJvFullColorLabel;
    SMSnulo: TJvFullColorLabel;
    QocorrCODIGO: TBCDField;
    QocorrDESCRI: TStringField;
    QColeta: TADOQuery;
    QColetaSEQUENCIA: TBCDField;
    QColetaDATAINC: TDateTimeField;
    QColetaPAGADOR: TBCDField;
    QColetaREMETENTE: TBCDField;
    QColetaMUN_REM: TStringField;
    QColetaCOD_MUN_REM: TBCDField;
    QColetaDESTINATARIO: TBCDField;
    QColetaMUN_DES: TStringField;
    QColetaCOD_MUN_DES: TBCDField;
    QColetaDT_COLETA_PREV: TDateTimeField;
    QColetaSOLICITACAO: TBCDField;
    QColetaSOLICITANTE: TStringField;
    QColetaEMAIL_SOLIC: TStringField;
    QColetaOBSERV: TStringField;
    QColetaFILIAL: TBCDField;
    QColetaCODLINHA: TStringField;
    QColetaDEALER: TBCDField;
    QColetaNRO_OCS: TBCDField;
    QColetaFL_EMAIL_ENV: TBCDField;
    QColetaDT_ENTREGA_PREV: TDateTimeField;
    QColetaITN_REM: TStringField;
    QColetaITN_DES: TStringField;
    QColetaDT_COLETA: TDateTimeField;
    QColetaDT_ENTREGA: TDateTimeField;
    QColetaDT_ENTREGA_CD: TDateTimeField;
    QColetaDT_SAIDA: TDateTimeField;
    QNFID: TBCDField;
    QNFCOD: TBCDField;
    QNFNOTFIS: TStringField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFVLRMER: TBCDField;
    QtarefasSEQUENCIA: TBCDField;
    QtarefasNRO_OCS: TBCDField;
    QtarefasDATAINC: TDateTimeField;
    QtarefasDT_COLETA_PREV: TStringField;
    QtarefasDT_COLETA: TStringField;
    QtarefasDT_ENTREGA_CD: TStringField;
    QtarefasDT_ENTREGA_PREV: TDateTimeField;
    QtarefasDT_ENTREGA: TDateTimeField;
    QtarefasDT_SAIDA: TDateTimeField;
    QtarefasNM_CLI: TStringField;
    QtarefasNM_REM: TStringField;
    QtarefasCID_REM: TStringField;
    QtarefasNM_DES: TStringField;
    QtarefasCID_DES: TStringField;
    QtarefasCODMAN: TBCDField;
    QtarefasTIPOCS: TStringField;
    Query1: TADOQuery;
    QtarefasSAIDA: TStringField;
    QtarefasDESTINATARIO: TBCDField;
    Qbaixa: TADOQuery;
    QbaixaSEQUENCIA: TBCDField;
    QbaixaNRO_OCS: TBCDField;
    QtarefasREMETENTE: TBCDField;
    QtarefasCODCON: TBCDField;
    QFilial: TADOQuery;
    QFilialFILIAL: TStringField;
    Panel2: TPanel;
    Label10: TLabel;
    cbCliente: TJvComboBox;
    Label7: TLabel;
    cbFilial: TJvComboBox;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    lblQuant: TLabel;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Panel3: TPanel;
    JvDBGrid1: TJvDBGrid;
    dtTrans1: TJvDateEdit;
    dtTrans2: TJvDateEdit;
    lblTtrans2: TLabel;
    lblTransf: TLabel;
    lblTransb1: TLabel;
    CBT2: TCheckBox;
    CBT1: TCheckBox;
    QtarefasDT_TRANSBORDO1: TDateTimeField;
    lblTransc: TLabel;
    QtarefasCODRMC: TBCDField;
    JvPageControl2: TJvPageControl;
    TabSheet1: TTabSheet;
    Obs: TRichEdit;
    QtarefasTRANSP_ROMA: TBCDField;
    QtarefasTRANSP_MAN: TBCDField;
    QtarefasDESTMA: TStringField;
    QtarefasDESTRO: TStringField;
    Panel5: TPanel;
    Panel6: TPanel;
    btnFechar: TBitBtn;
    mCte: TMemo;
    TabSheet2: TTabSheet;
    JvDBGrid2: TJvDBGrid;
    btnObs: TBitBtn;
    Label11: TLabel;
    Label23: TLabel;
    eddtcolreal: TJvDateEdit;
    cbCTe: TCheckBox;
    btnAlTa: TBitBtn;
    btnSaTa: TBitBtn;
    btnCancelar: TBitBtn;
    eddtdesreal: TJvDateEdit;
    Label4: TLabel;
    Label9: TLabel;
    dtsTarefas: TDataSource;
    QtarefasStatus: TStringField;
    Label2: TLabel;
    cbStatus: TJvComboBox;
    btnemail: TBitBtn;
    QNFDATNOT: TDateTimeField;
    QtarefasSOLICITACAO: TBCDField;
    Label3: TLabel;
    QtarefasFILIAL: TBCDField;
    btnListagem: TBitBtn;
    QtarefasDESTINO: TStringField;
    lblDestino: TLabel;
    cbTudo: TCheckBox;
    QColetaUSER_BAIXA: TStringField;
    QColetaDT_BAIXA: TDateTimeField;
    btnVer: TBitBtn;
    QtarefasESTADO: TStringField;
    Label15: TLabel;
    edOCS: TJvCalcEdit;
    Label16: TLabel;
    edCte: TJvCalcEdit;
    Label17: TLabel;
    edNF: TJvCalcEdit;
    Label18: TLabel;
    edMan: TJvCalcEdit;
    btnFiltrar: TBitBtn;
    Label6: TLabel;
    edSol: TJvCalcEdit;
    QtarefasNOMMOT: TStringField;
    btnExcComp: TBitBtn;
    edhrdesreal: TJvMaskEdit;
    hrTrans2: TJvMaskEdit;
    hrTrans1: TJvMaskEdit;
    EdHrcolreal: TJvMaskEdit;
    QtarefasSTATUS_SMS: TBCDField;
    QtarefasOST: TBCDField;
    NF: TJvMemoryData;
    NFREG: TIntegerField;
    NFID: TBCDField;
    NFCOD: TBCDField;
    NFNOTFIS: TStringField;
    NFQUANTI: TBCDField;
    NFPESOKG: TBCDField;
    NFVLRMER: TBCDField;
    NFDataNF: TDateField;
    QtarefasMONITORAMENTO: TStringField;
    QtarefasCOD_DES: TBCDField;
    QtarefasCODFIL: TBCDField;
    Sp_Monit: TADOStoredProc;
    QMonitoramento: TADOQuery;
    QMonitoramentoDESCR: TStringField;
    QMonitoramentoMOTIVO: TStringField;
    QMonitoramentoDATAINC: TDateTimeField;
    QMonitoramentoDT_MOTIVO: TDateTimeField;
    QMonitoramentoOBSERVACAO: TStringField;
    QMonitoramentoUSUARIO: TStringField;
    QMonitoramentoID_NF: TBCDField;
    cbOcoCol: TJvComboBox;
    QColetaOCORR_COL: TBCDField;
    QColetaOBS_MON: TStringField;
    QColetaOCORR_ENT: TBCDField;
    cbOcoEn: TJvComboBox;
    cbEdi: TCheckBox;

    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure botoes;
    procedure btnSaTaClick(Sender: TObject);
    procedure btnAlTaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure pnLegendaExit(Sender: TObject);
    procedure Legenda1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure cbClienteChange(Sender: TObject);
    procedure EdHrcolrealExit(Sender: TObject);
    procedure edhrdesrealExit(Sender: TObject);
    procedure cbTipoChange(Sender: TObject);
    procedure BaixaTodas;
    procedure cbFilialChange(Sender: TObject);
    procedure CBT2Click(Sender: TObject);
    procedure CBT1Click(Sender: TObject);
    procedure hrTrans1Exit(Sender: TObject);
    procedure btnObsClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure QtarefasCalcFields(DataSet: TDataSet);
    procedure QtarefasAfterOpen(DataSet: TDataSet);
    procedure QtarefasAfterScroll(DataSet: TDataSet);
    procedure cbStatusChange(Sender: TObject);
    procedure btnemailClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnListagemClick(Sender: TObject);
    procedure QColetaAfterOpen(DataSet: TDataSet);
    procedure Baixaroma;
    procedure btnVerClick(Sender: TObject);
    procedure QtarefasBeforeOpen(DataSet: TDataSet);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnExcCompClick(Sender: TObject);
    procedure JvDBGrid1CellClick(Column: TColumn);

  private
    var tipo : String;
    procedure InputBoxSetPasswordChar(var Msg: TMessage); message InputBoxMessage;
  public
    { Public declarations }
  end;

var
  frmMonitCol: TfrmMonitCol;
  quant : integer;
  sqlmaster : string;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmMonitCol.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
      1: QTarefas.First;
      2: QTarefas.Prior;
      3: QTarefas.Next;
      4: QTarefas.Last;
  end;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

procedure TfrmMonitCol.Baixaroma;
begin
  // text := QTarefas.sql;
  Qtarefas.First;
  while not Qtarefas.eof do
  begin
    QColeta.edit;
    if (copy(eddtcolreal.text,1,2) <> '  ') and (QColetadt_coleta.IsNull) then
    begin
      QColetadt_coleta.value  := eddtcolreal.Date + StrToTime(edHrcolReal.Text);
      QOcorr.locate('descri',cbOcoCol.Text,[]);
      QColetaOCORR_COL.Value  := QocorrCODIGO.value;
    end;
    if (copy(eddtdesreal.text,1,2) <> '  ') and (QColetaDT_ENTREGA.IsNull) then
    begin
      QColetaDT_ENTREGA.value := eddtdesreal.Date + StrToTime(edHrdesReal.Text);
      QColetaDT_SAIDA.value := eddtdesreal.Date + StrToTime(edHrdesReal.Text);
      QColetaUSER_BAIXA.Value := GLBUSER;
      QColetaDT_BAIXA.Value := date;
      QOcorr.locate('descri',cbOcoEn.Text,[]);
      QColetaOCORR_ENT.Value  := QocorrCODIGO.value;      
    end;
    QColeta.post;
    Qtarefas.next;
  end;
  cbTudo.Checked := false;
  panel1.visible := true;
  application.processMessages;
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.BaixaTodas;
begin
  if cbCTe.Checked = true then
  begin
    Qbaixa.close;
    Qbaixa.sql.clear;
    QBaixa.SQL.add('select DISTINCT sequencia, t.nro_ocs from tb_coleta t ');
    QBaixa.SQL.add('Where t.remetente = :0 and t.destinatario =:1 ');
    QBaixa.SQL.add('and trunc(t.datainc) = trunc(:2) ');
    QBaixa.SQL.add('and dt_saida is null ');
    QBaixa.Parameters[0].value := QTarefasREMETENTE.AsInteger;
    QBaixa.Parameters[1].value := QTarefasDESTINATARIO.AsInteger;
    QBaixa.Parameters[2].value := QTarefasDATAINC.AsDateTime;
    QBaixa.Open;
  end;
  //reg := Qbaixa.RecordCount;
  while not QBaixa.eof do
  begin
    QColeta.close;
    QColeta.Parameters[0].Value := QbaixaSEQUENCIA.AsInteger;
    QColeta.Open;
    QColeta.edit;
    if (copy(eddtcolreal.text,1,2) <> '  ') then
    begin
      if cbOcoCol.Text = ''   then
      begin
        ShowMessage('Deve escolher a ocorr�ncia de coleta');
        cbOcoCol.Setfocus;
        exit;
      end;
      if QColetadt_coleta.IsNull then
      begin
        QColetadt_coleta.value := eddtcolreal.Date + StrToTime(edHrcolReal.Text);
        QOcorr.locate('descri',cbOcoCol.Text,[]);
        QColetaOCORR_COL.Value  := QocorrCODIGO.value;
      end;
    end
    else
      QColetaDT_COLETA.clear;

    if (copy(eddtdesreal.text,1,2) <> '  ') and (QColetaDT_ENTREGA.IsNull) then
      QColetaDT_ENTREGA.value  := eddtdesreal.Date + StrToTime(edHrdesReal.Text)
    else
      QColetaDT_ENTREGA.clear;

    // mudan�a at� termos maior controle Data de Sa�da = Data de entrega
    if not QColetaDT_ENTREGA.IsNull then
    begin
      QColetaDT_SAIDA.value := QColetaDT_ENTREGA.Value;
      QColetaUSER_BAIXA.Value := GLBUSER;
      QColetaDT_BAIXA.Value := date;
      QOcorr.locate('descri',cbOcoEn.Text,[]);
      QColetaOCORR_ENT.Value  := QocorrCODIGO.value;
    end;

    QColeta.post;
    QBaixa.Next;
  end;
  cbCTe.Checked := false;
end;

procedure TfrmMonitCol.btnListagemClick(Sender: TObject);
var memo2: TStringList;
    i : Integer;
begin
  if FileExists('email.html') then
    DeleteFile('email.html');
  Memo2 := TStringList.Create;
  memo2.Add('<HTML><HEAD><TITLE>Monitoramento de Transporte</TITLE>');
  memo2.Add('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo2.Add('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto2 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto3 {	FONT: bold 14px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('</STYLE>');
  memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo2.Add('<P align=center class=titulo1>Monitoramento de Transporte</P>');
  Memo2.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo2.Add('<TBODY>');

  for i:= 0 to DBGrid1.Columns.Count - 1 do
  begin
    Memo2.Add('<th><FONT class=texto1>'+ DBGrid1.Columns[i].Title.Caption +'</th>');
  end;
  Memo2.Add('</tr>');
  QTarefas.First;
  while not QTarefas.Eof do
  begin
    for i:= 0 to DBGrid1.Columns.Count - 1 do
      Memo2.Add('<th><FONT class=texto2>'+ QTarefas.fieldbyname(DBGrid1.Columns[i].FieldName).AsString +'</th>');
    QTarefas.Next;
    Memo2.Add('<tr>');
  end;
  QTarefas.First;
  memo2.Add('</TABLE></TBODY><BR>');

  Memo2.SaveToFile('listagem.html');
  Memo2.Free;

  ShellExecute(Application.Handle, nil, PChar('listagem.html'), nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmMonitCol.botoes;
begin
  btnAlTa.enabled := not btnAlTa.enabled;
  btnSaTa.enabled := not btnSaTa.enabled;
  btnCancelar.Enabled:= not btnCancelar.Enabled;
end;

procedure TfrmMonitCol.btnAlTaClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  botoes;
  if (QTarefasTIPOCS.value = 'C') and (cbOcoCol.Enabled = true) then
    eddtColreal.SetFocus
  else
    eddtdesreal.SetFocus;
  QColeta.edit;
  tipo := 'I';
end;

procedure TfrmMonitCol.btnCancelarClick(Sender: TObject);
begin
  tipo := '';
  botoes;
  QColeta.Cancel;
end;

procedure TfrmMonitCol.btnemailClick(Sender: TObject);
var mail, memo2: TStringList;
    caminho : String;
begin
  Memo2 := TStringList.Create;
  memo2.Add('<HTML><HEAD><TITLE>Monitoramento de Transporte</TITLE>');
  memo2.Add('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo2.Add('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto2 {	FONT: bold 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto3 {	FONT: bold 14px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('</STYLE>');
  memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo2.Add('<P align=center class=titulo1>Monitoramento de Transporte</P>');
  // In�cio dos dados
  memo2.Add('<TBODY>');
  memo2.Add('<Table>');
  memo2.Add('  <TR bgColor=#ebebeb>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Origem</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Cidade Origem</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Destino</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Cidade Destino</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Data Prevista</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>N.F.</FONT></TD>');
  memo2.Add('    <TD align=middle width=150><FONT class=texto2>Data NF</FONT></TD>');
  memo2.Add('  </TR>');
  // ITENS
  QTarefas.first;
  while not Qtarefas.eof do
  begin
    memo2.Add('  <TR>');
    memo2.Add('    <TD align=left><FONT class=texto1>' + QtarefasNM_REM.AsString + '</FONT></TD>');
    memo2.Add('    <TD align=left><FONT class=texto1>' + QtarefasCID_REM.AsString + '</FONT></TD>');
    memo2.Add('    <TD align=left><FONT class=texto1>' + QtarefasNM_DES.AsString + '</FONT></TD>');
    memo2.Add('    <TD align=left><FONT class=texto1>' + QtarefasCID_DES.AsString + '</FONT></TD>');
    if QtarefasDT_COLETA.IsNull then
      memo2.Add('    <TD align=center><FONT class=texto1>' + QtarefasDT_COLETA_PREV.AsString + '</FONT></TD>')
    else
      memo2.Add('    <TD align=center><FONT class=texto1>' + QtarefasDT_ENTREGA_PREV.AsString + '</FONT></TD>');
    memo2.Add('    <TD align=right><FONT class=texto1>' + QNFNOTFIS.AsString + '</FONT></TD>');
    memo2.Add('    <TD align=right><FONT class=texto1>' + NFDataNF.AsString + '</FONT></TD>');
    memo2.Add('  </TR>');
    QTarefas.next;
  end;
  QTarefas.First;
  memo2.Add('</TABLE></TBODY><BR>');
  caminho := ExtractFileDir(GetCurrentDir);
  DeleteFile(caminho + '\email.html');
  Memo2.SaveToFile(caminho + '\email.html');
  Memo2.Free;

  mail := TStringList.Create;
  try
    mail.values['to'] := 'intecom@intecomlogistica.com.br'; ///AQUI VAI O EMAIL DO DESTINATARIO///
    mail.values['subject'] := 'Monitoramento de Transporte'; ///AQUI O ASSUNTO///
    mail.values['body'] := ''; ///AQUI O TEXTO NO CORPO DO EMAIL///
    mail.values['attachment0'] := caminho + '\email.html'; ////AQUI O ENDERE�O ONDE ENCONTRAM OS ARQUIVOS//
    mail.values['attachment1'] := ''; ///IDEM  - NO ATTACHMENT1    TEM QUE COLOCAR A SEQUNCIA DO EMAIL A QUAL DESEJA ENVIAR EXEMPLO: ATTACHMENT1
    sendEMail(Application.Handle, mail);
  finally
    mail.Free;
  end;
end;

procedure TfrmMonitCol.btnExcCompClick(Sender: TObject);
var caminho,arq,Retorno: string;
begin

  if QColetaFILIAL.Value = 1 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_1\'
  else if QColetaFILIAL.Value = 8 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_2\'
  else if QColetaFILIAL.Value = 9 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_9\'
  else if QColetaFILIAL.Value = 8 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_8\'
  else if QColetaFILIAL.Value = 4 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_4\';

  arq := caminho+QtarefasCODCON.AsString+'-1.jpg';

  if FileExists(arq) then
  begin
    PostMessage(Handle, InputBoxMessage, 0, 0);
    Retorno := InputBox('Senha', 'Digite a senha', '');
    if Retorno <> 'INT_130314' then
      Exit;
    DeleteFile(arq);
    ShowMessage('Documento Exclu�do !!');
  end
  else
    ShowMessage('Documento sem comprovante digitalizado !!');
end;

procedure TfrmMonitCol.btnFecharClick(Sender: TObject);
begin
  if alltrim(mcte.Lines.Text) <> '' then
  begin
    Obs.Lines.Add('------------------------------------' +#13+#10+ glbUser + ' - ' + DateTimeToStr(now));
    Obs.Lines.Add(mcte.Lines.Text);
  end;
  mCte.Clear;
  mCte.Visible := False;
  panel5.Visible := False;
  QColeta.edit;
  QColetaOBS_MON.Value := Obs.Text;
  QColeta.post;
end;

procedure TfrmMonitCol.btnFiltrarClick(Sender: TObject);
var ocs : string;
    i : Integer;
begin
  panel1.visible := true;
  application.processMessages;

  if cbCliente.Text <> '' then
    QTarefas.sql [3] := 'and nm_cli = '+ #39 + cbcliente.Text + #39
  else
    QTarefas.sql [3] := ' ';

  if cbfilial.Text <> '' then
    QTarefas.sql [4] := 'and filial = '+ #39 + copy(cbFilial.Text,1,1) + #39
  else
    QTarefas.sql [4] := ' ';

  if edOCS.value > 0 then
    QTarefas.sql [5] := 'and nro_ocs = '+ #39 + edOCS.text + #39
  else
    QTarefas.sql [5] := ' ';

  if edCte.value > 0 then
    QTarefas.sql [6] := 'and codcon = '+ #39 + edCte.text + #39
  else
    QTarefas.sql [6] := ' ';

  if edNF.value > 0 then
  begin
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.Clear;
    dtmdados.RQuery1.sql.add('select codocs from rodioc where notfis = :0');
    dtmdados.RQuery1.Parameters[0].value := edNF.text;
    dtmdados.RQuery1.open;
    if not dtmdados.RQuery1.Eof then
    begin
      i := 0;
      while not dtmdados.RQuery1.Eof do
      begin
        if i = 0 then
          ocs := dtmdados.RQuery1.FieldByName('codocs').AsString
        else
          ocs := dtmdados.RQuery1.FieldByName('codocs').AsString + ',' + ocs;
        i := i + 1;  
        dtmdados.RQuery1.Next;
      end;
      QTarefas.sql [7] := 'and nro_ocs in ('+ ocs + ')';
    end;
    dtmdados.RQuery1.close;
  end
  else
    QTarefas.sql [7] := ' ';

  if edMan.value > 0 then
    QTarefas.sql [8] := 'and codman = '+ #39 + edMan.text + #39
  else
    QTarefas.sql [8] := ' ';

  if edSol.value > 0 then
    QTarefas.sql [9] := 'and solicitacao = '+ #39 + edSol.text + #39
  else
    QTarefas.sql [9] := ' ';

  if cbEdi.Checked = true then
    QTarefas.sql [10] := 'and ocorr_edi > 0 '
  else
    QTarefas.sql [10] := ' ';

//qtarefas.SQL.savetofile('C:\Sistemas\SIM\sql.txt');

  Qtarefas.open;
  if QTarefas.eof then
    ShowMessage('Consulta N�o Encontrada !!')
  else
  begin
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
  end;
  panel1.visible := false;

end;

procedure TfrmMonitCol.btnObsClick(Sender: TObject);
begin
  panel5.Visible := true;
  mcte.Visible := true;
  mcte.SetFocus;
end;

procedure TfrmMonitCol.btnSaTaClick(Sender: TObject);
var tar,esc: Integer;
    parcial : String;
begin
  esc := 0;
  parcial := 'N';
  NF.First;
  while not NF.eof do
  begin
    if NFREG.value = 1 then    
      esc := 1 + esc;
    NF.Next;
  end;
  NF.First;
  if NF.RecordCount > esc then
    parcial := 'S'
  else
    parcial := 'N';
  if esc = 0 then
  begin
    ShowMessage('N�o foi escolhida nenhuma NF');
    exit;
  end;

  if (copy(eddtdesreal.text,1,2) = '  ') and (copy(eddtcolreal.text,1,2) = '  ') then
  begin
    ShowMessage('Deve ser escolhido alguma data de ocorr�ncia !!');
    exit;
  end;

  tar := QtarefasSEQUENCIA.AsInteger;
  if (copy(eddtdesreal.text,1,2) <> '  ') and (QTarefasCODCON.IsNull) then
  begin
    ShowMessage('Como pode dar baixa de entrega sem CT-e Emitido ?');
    exit;
  end;

  // Data de Chegada Destino
  if copy(eddtdesreal.text,1,2) <> '  ' then
  begin
    if eddtdesreal.date <  eddtcolreal.date then
    begin
      showmessage('A Data de Chegada do Destino n�o pode ser menor que a Coleta !');
      eddtcolreal.SetFocus;
      exit;
    end;
  end;

  if (copy(eddtdesreal.text,1,2) <> '  ') and (cbOcoEn.Text = '') then
  begin
    ShowMessage('N�o foi escolhido a ocorr�ncia de entrega ?');
    cbOcoEn.setfocus;
    exit;
  end;

  if (copy(eddtcolreal.text,1,2) = '  ') and (cbOcoCol.Text <> '') then
  begin
    ShowMessage('N�o foi escolhido a data de ocorr�ncia de coleta ?');
    cbOcoCol.setfocus;
    exit;
  end;

  if cbTudo.Checked = true then
  begin
    if Application.Messagebox('Voc� Deseja Baixar Tudo que est� na Tela ?',
    'Baixar Tudo', MB_YESNO + MB_ICONQUESTION) = IDNO then
    begin
      cbTudo.Checked := false;
      exit;
    end;
  end;

  if cbCTe.Checked = true then
    BaixaTodas
  else
  begin
    if cbTudo.Checked = true then
    begin
      if (copy(eddtcolreal.text,1,2) = '  ') and (not QtarefasDT_COLETA_PREV.IsNull)then
      begin
        showmessage('N�o foi digitada a Data de Coleta !');
        eddtcolreal.SetFocus;
        exit;
      end;

      if (copy(eddtcolreal.text,1,2) <> '  ') and (cbOcoCol.Text = '')then
      begin
        showmessage('Qual a Ocorr�ncia da Coleta ?');
        cbOcoCol.SetFocus;
        exit;
      end;
      BaixaRoma;
    end
    else
      try
        if (copy(eddtcolreal.text,1,2) <> '  ') then
        begin
          if cbOcoCol.Text = '' then
          begin
            ShowMessage('Deve escolher a ocorr�ncia de coleta');
            cbOcoCol.Setfocus;
            exit;
          end;
          QColetadt_coleta.value  := eddtcolreal.Date + StrToTime(edHrcolReal.Text);
          QOcorr.locate('descri',cbOcoCol.Text,[]);
          QColetaOCORR_COL.Value  := QocorrCODIGO.value;
        end
        else
          QColetaDT_COLETA.clear;

        if copy(eddtdesreal.text,1,2) <> '  ' then
        begin
          QColetaDT_ENTREGA.value  := eddtdesreal.Date + StrToTime(edHrdesReal.Text);
          // mudan�a at� termos maior controle Data de Sa�da = Data de entrega
          QColetaDT_SAIDA.value := QColetaDT_ENTREGA.Value;
          QColetaUSER_BAIXA.Value := GLBUSER;
          QColetaDT_BAIXA.Value := date;
          QOcorr.locate('descri',cbOcoEn.Text,[]);
          QColetaOCORR_ENT.Value  := QocorrCODIGO.value;
        end
        else
          QColetaDT_ENTREGA.clear;
        QColeta.post;

      except
        on e: Exception do
        begin
          ShowMessage(e.message);
        end;
      end;
  end;
  tipo := '';
  cbOcoCol.ItemIndex := -1;
  cbOcoEn.ItemIndex := -1;
  eddtcolreal.Clear;
  eddtdesreal.Clear;
  EdHrcolreal.text := '00:00';
  edhrdesreal.text := '00:00';
  botoes;
  QTarefas.close;
  QTarefas.open;
  QTarefas.Locate('sequencia',tar,[]);
end;

procedure TfrmMonitCol.btnVerClick(Sender: TObject);
var wb:Variant;
    caminho,arq : string;
begin
  if not QTarefascodcon.IsNull then
  begin
    if QtarefasCODFIL.Value = 1 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_1\'
    else if QtarefasCODFIL.Value = 2 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_2\'
    else if QtarefasCODFIL.Value = 9 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_9\'
    else if QtarefasCODFIL.Value = 8 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_8\'
    else if QtarefasCODFIL.Value = 4 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_4\'
    else if QtarefasCODFIL.Value = 6 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_6\';

    arq := caminho+QtarefasCODCON.AsString+'-1.jpg';

    if FileExists(arq) then
    begin
      wb := CreateOleObject('InternetExplorer.Application');
      wb.Visible := true;
      wb.Navigate(arq);
    end
    else
      ShowMessage('Documento sem comprovante digitalizado !!');
  end
  else
    ShowMessage('Nem Existe CT-e como pode ter comprovante !!');
end;

procedure TfrmMonitCol.cbClienteChange(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  if cbCliente.Text <> '' then
    QTarefas.sql [32] := 'and nm_cli = '+ #39 + cbcliente.Text + #39
  else
    QTarefas.sql [32] := ' ';
//qtarefas.SQL.savetofile('c:\sql.txt');
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.cbFilialChange(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  if cbfilial.Text <> '' then
    QTarefas.sql [34] := 'and filial = '+ #39 + copy(cbFilial.Text,1,1) + #39
  else
    QTarefas.sql [34] := ' ';
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.cbStatusChange(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  if cbStatus.Text <> '' then
  begin
    if cbStatus.text = 'Coletando' then
      QTarefas.sql [38] := 'and CODCON is null and CODRMC > 0 and tipocs = ''C'' ';
    if cbStatus.text = 'A Coletar' then
      QTarefas.sql [38] := 'and CODCON is null and CODRMC is null and tipocs = ''C'' ';
    if cbStatus.text = 'A Manifestar' then
      QTarefas.sql [38] := 'and CODCON > 0 and CODMAN is null ';
    if cbStatus.text = 'Em Transito' then
      QTarefas.sql [38] := 'and CODCON > 0 and CODMAN > 0 ';
    if cbStatus.text = 'Em Processo' then
      QTarefas.sql [38] := 'and CODCON is null and CODMAN is null and tipocs = ''P'' ';
  end
  else
    QTarefas.sql [38] := ' ';
  Qtarefas.open;
//qtarefas.SQL.savetofile('c:\sql.txt');
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.CBT1Click(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  if cbt1.Checked = true then
    QTarefas.sql [37] := 'and codrmc = '+ #39 + QtarefasCODRMC.AsString + #39
  else
    QTarefas.sql [37] := ' ';
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.CBT2Click(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  if cbt2.Checked = true then
    QTarefas.sql [35] := 'and codman = '+ #39 + QtarefasCODMAN.AsString + #39
  else
    QTarefas.sql [35] := ' ';
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.cbTipoChange(Sender: TObject);
begin
  panel1.visible := true;
  application.processMessages;
  QTarefas.sql [26] := '';
  QTarefas.sql [27] := '';
  QTarefas.sql [28] := '';
  QTarefas.sql [29] := '';
  QTarefas.sql [32] := ' ';

//qtarefas.SQL.savetofile('c:\sql.txt');
  Qtarefas.open;
  Qtarefas.First;
  quant := QTarefas.RecordCount;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  sqlmaster := Qtarefas.SQL.Text;
  panel1.visible := false;
end;

procedure TfrmMonitCol.DBGrid1DblClick(Sender: TObject);
var Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTarefas.Locate(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption + ' n�o localizado');
end;

procedure TfrmMonitCol.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var hoje : TdateTime;
begin
  if (Column.Field.FieldName = 'NRO_OCS') then
  begin
    if QTarefasSTATUS_SMS.Value = 1 then
    begin
      dbgrid1.canvas.Brush.Color := SMS1.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 2 then
    begin
      dbgrid1.canvas.Brush.Color := SMS2.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 3 then
    begin
      dbgrid1.canvas.Brush.Color := SMS3.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 9 then
    begin
      dbgrid1.canvas.Brush.Color := SMS9.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.IsNull then
    begin
      dbgrid1.canvas.Brush.Color := SMSnulo.LabelColor;
      dbgrid1.Canvas.Font.Color  := clblue;
    end;
  end;

  if (Column.Field.FieldName = 'CODCON') then
  begin
    if QTarefasOST.Value > 0 then
    begin
      dbgrid1.canvas.Brush.Color := $004080FF;
      //dbgrid1.Canvas.Font.Color  := clwhite;
    end;
  end;

 if (Column.Field.FieldName = 'DT_COLETA') and (QTarefasTIPOCS.value = 'C') then
  begin
    if QTarefasdt_coleta.IsNull then
      hoje := date
    else
      hoje := StrToDate(QTarefasdt_coleta.value);

    if not QTarefasDT_COLETA_PREV.isnull then
    begin
      if StrToDate(QTarefasDT_COLETA_PREV.value) < hoje then
      begin
        DBgrid1.canvas.Brush.Color := lblColetaAtrasada.labelColor;
        dbgrid1.Canvas.Font.Color  := clWhite;
      end
      else if StrToDate(QTarefasDT_COLETA_PREV.value) = hoje then
      begin
        dbgrid1.canvas.Brush.Color := lblColetaHoje.labelcolor;
        dbgrid1.Canvas.Font.Color  := clNavy;
      end
      else if StrToDate(QTarefasDT_COLETA_PREV.value) > hoje then
      begin
        dbgrid1.canvas.Brush.Color := lblColetaPontual.labelcolor;
        dbgrid1.Canvas.Font.Color  := clWhite;
        DBGrid1.Canvas.FillRect(Rect);
        DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      end;
    end;
    Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
  end;

  if (Column.Field.FieldName = 'DT_COLETA') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;

  if (Column.Field.FieldName = 'DT_COLETA_PREV') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;

  if (Column.Field.FieldName = 'DT_ENTREGA_CD') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;


  if (Column.Field.FieldName = 'DT_ENTREGA_PREV') then
  begin
    if QTarefasDT_ENTREGA.IsNull then
      hoje := date
    else
      hoje := QTarefasDT_ENTREGA.Value;

    if QTarefasDT_ENTREGA_PREV.Value < hoje then
    begin
      DBgrid1.canvas.Brush.Color := lblColetaAtrasada.labelColor;
      dbgrid1.Canvas.Font.Color  := clWhite;
    end
    else
    if QTarefasDT_ENTREGA_PREV.Value = hoje then
    begin
      dbgrid1.canvas.Brush.Color := lblColetaHoje.labelcolor;
      dbgrid1.Canvas.Font.Color  := clNavy;
    end
    else
    if QTarefasDT_ENTREGA_PREV.Value > hoje then
    begin
      dbgrid1.canvas.Brush.Color := lblColetaPontual.labelcolor;
      dbgrid1.Canvas.Font.Color  := clWhite;
    end;
  end;
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
end;

procedure TfrmMonitCol.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var Retorno: String;
begin
  if Key = VK_F3 then
  begin
    Retorno := InserirValor('Filtrar por ' + DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption, Caption);
    if Retorno = '' then
      Exit;

    panel1.visible := true;
    application.processMessages;
    QTarefas.sql [31] := 'where dt_saida is null ' +
                         ' and ' + DBgrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName +' = ' +
                         #39 + Retorno + #39;
  //  Qtarefas.sql.savetofile('c:\sql.txt');
    Qtarefas.open;
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
    panel1.visible := false;
  end;
  if Key = VK_F4 then
  begin
    panel1.visible := true;
    application.processMessages;
    QTarefas.sql [31] := 'where dt_saida is null  ' ;
    Qtarefas.open;
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
    panel1.visible := false;
  end;
end;

procedure TfrmMonitCol.DBGrid1TitleClick(Column: TColumn);
var icount : integer;
begin
  panel1.visible := true;
  application.processMessages;
  if Pos('order by', QTarefas.SQL.Text) > 0 then
    QTarefas.SQL.Text := Copy(QTarefas.SQL.Text, 1, Pos('order by', QTarefas.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QTarefas.SQL.Text := QTarefas.SQL.Text + ' order by ' + Column.FieldName;
  QTarefas.Open;
  //Muda a cor da coluna do grid
  for iCount := 0 to DBGrid1.Columns.Count - 1 do
    DBGrid1.Columns[iCount].Title.font.color := clNavy;
  column.Title.Font.Color:=clRed;
  panel1.visible := false;
end;

procedure TfrmMonitCol.EdHrcolrealExit(Sender: TObject);
var data : TdateTime;
begin
  data := (eddtcolreal.Date + StrToTime(edHrcolReal.Text));
  if DataHoje(data) = 'S' then
  begin
    ShowMessage('Data Maior que Agora ?');
    eddtcolreal.SetFocus;
  end;
end;

procedure TfrmMonitCol.edhrdesrealExit(Sender: TObject);
var data : TdateTime;
begin
  data := (eddtdesreal.Date + StrToTime(edHrdesReal.Text));
  if DataHoje(data) = 'S' then
  begin
    ShowMessage('Data Maior que Agora ?');
    eddtdesreal.SetFocus;
  end;
end;

procedure TfrmMonitCol.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QTarefas.close;
  QColeta.close;
  Qocorr.close;
  QNF.close;
end;

procedure TfrmMonitCol.FormCreate(Sender: TObject);
begin
  QCliente.open;
  cbCliente.Clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteNM_CLI_OS.value);
    QCliente.Next;
  end;

  QFilial.open;
  cbFilial.Clear;
  cbFilial.Items.add('');
  while not QFilial.eof do
  begin
    cbFilial.Items.add(QFilialFILIAL.value);
    QFilial.Next;
  end;

  dtmdados.rQuery1.close;
  dtmdados.rQuery1.SQL.Clear;
  dtmdados.rQuery1.SQL.add('select distinct nvl(f.nomeab, f.razsoc) nome , f.codclifor ');
  dtmdados.rQuery1.SQL.add('from rodrmc ro left join rodmot m on ro.codmot = m.codmot ');
  dtmdados.rQuery1.SQL.add('               left join rodcli f on m.codclifor = f.codclifor ');
  dtmdados.rQuery1.SQL.add('union ');
  dtmdados.rQuery1.SQL.add('select distinct nvl(f.nomeab, f.razsoc) nome , f.codclifor ');
  dtmdados.rQuery1.SQL.add('from rodman r  left join rodmot m on r.codmo1 = m.codmot ');
  dtmdados.rQuery1.SQL.add('               left join rodcli f on m.codclifor = f.codclifor ');
  dtmdados.rQuery1.SQL.add('order by 1 ');
  dtmdados.rQuery1.open;

  QOcorr.SQL[1] := 'Where tipo = ''C'' ';
  Qocorr.open;
  cbOcoCol.Clear;
  cbOcoCol.Items.add('');
  while not Qocorr.eof do
  begin
    cbOcoCol.Items.add(QocorrDESCRI.AsString);
    Qocorr.Next;
  end;

  QOcorr.close;
  QOcorr.SQL[1] := 'Where tipo = ''E'' ';
  Qocorr.open;
  cbOcoEn.Clear;
  cbOcoEn.Items.add('');
  while not Qocorr.eof do
  begin
    cbOcoEn.Items.add(QocorrDESCRI.AsString);
    Qocorr.Next;
  end;

  QOcorr.close;
  QOcorr.SQL[1] := 'Where tipo is not null ';
  Qocorr.open;
  QNF.open;
  sqlmaster := QTarefas.SQL.Text;
end;

procedure TfrmMonitCol.hrTrans1Exit(Sender: TObject);
var data : TdateTime;
begin
  data := (dtTrans1.Date + StrToTime(hrTrans1.Text));
  if DataHoje(data) = 'S' then
  begin
    ShowMessage('Data Maior que Agora ?');
    dtTrans1.SetFocus;
  end
  else
  begin
    if Application.Messagebox('Baixar Entrega de Transbordo 1 ?',
      'Baixar Entrega', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      dtmdados.rQuery1.close;
      dtmdados.rQuery1.SQL.Clear;
      dtmdados.rQuery1.sql.add('update tb_coleta set dt_transbordo1 = :0 ');
      dtmdados.rQuery1.sql.add('where sequencia = :1 ');
      dtmdados.rQuery1.Parameters[0].value := data;
      dtmdados.rQuery1.Parameters[1].value := QTarefasSEQUENCIA.AsInteger;
      dtmdados.rQuery1.ExecSQL;
    end;
    dtmdados.rQuery1.Close;
  end;
end;

procedure TfrmMonitCol.JvDBGrid1CellClick(Column: TColumn);
begin
  if (Column.Field = NFREG) then
  begin
    NF.edit;
    if (NFReg.IsNull) or (NFReg.Value = 0) then
    begin
      NFReg.value := 1;
    end
    else
    begin
      NFReg.value := 0;
    end;
    NF.Post;
  end;
end;

procedure TfrmMonitCol.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = NFREG) then
  begin
    JvDBGrid1.Canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if NFREG.Value = 1 then
      iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
  {Remove Horizontal}
   ShowScrollBar(JvDBGrid1.Handle,SB_HORZ,False);

  {Remove Vertical}
 //  ShowScrollBar(JvDBGrid1.Handle,SB_VERT,False);
end;

procedure TfrmMonitCol.Legenda1Click(Sender: TObject);
begin
  pnLegenda.Visible := True;
  pnLegenda.SetFocus;
end;

procedure TfrmMonitCol.QTarefasAfterScroll(DataSet: TDataSet);
begin
  lblTransc.caption := '';
  Screen.Cursor := crHourGlass;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
  QColeta.close;
  QColeta.Parameters[0].Value := QtarefasSEQUENCIA.AsInteger;
  QColeta.Open;
  if QtarefasTIPOCS.Value = 'P' then
  begin
    label11.Visible := false;
    cbOcoCol.Visible := false;
    label23.Visible := false;
    eddtcolreal.Visible := false;
    EdHrcolreal.Visible := false;
  end
  else
  begin
    eddtcolreal.Visible := true;
    EdHrcolreal.Visible := true;
    if not QtarefasDT_COLETA.IsNull then
    begin
      cbOcoCol.Enabled := false;
      eddtcolreal.Enabled := false;
      EdHrcolreal.Enabled := false;
    end
    else
    begin
      cbOcoCol.Enabled := true;
      eddtcolreal.Enabled := true;
      EdHrcolreal.Enabled := true;
    end;
    label11.Visible := true;
    label23.Visible := true;
    cbOcoCol.Visible := true;
    dtmdados.rQuery1.close;
    dtmdados.rQuery1.SQL.Clear;
    dtmdados.rQuery1.sql.add('select (m.nommot || '' - '' || f.nomeab || '' - '' || f.prtel2) coleta ');
    dtmdados.rQuery1.sql.add('from rodrmc ro left join rodmot m on ro.codmot = m.codmot left join rodvei v on ro.codvei = v.codvei ');
    dtmdados.rQuery1.sql.add('left join rodcli f on v.codpro = f.codclifor ');
    dtmdados.rQuery1.sql.add('where ro.codrmc = :0 and ro.codfil = :1 ');
    dtmdados.rQuery1.Parameters[0].value := QtarefasCODRMC.AsInteger;
    dtmdados.rQuery1.Parameters[1].value := QtarefasFILIAL.Asinteger;
    dtmdados.rQuery1.open;
    if not dtmdados.rQuery1.eof then
      lblTransc.caption := dtmdados.rQuery1.FieldByName('coleta').Value + '-> Destino : '+ QtarefasDESTMA.AsString
    else
      lblTransc.caption := '';
  end;
  if not QtarefasDT_TRANSBORDO1.IsNull then
  begin
    dtTrans1.Date := QtarefasDT_TRANSBORDO1.Value;
    hrTrans1.Text := TimeToStr(QtarefasDT_TRANSBORDO1.Value);
    dttrans1.Enabled := false;
    hrTrans1.Enabled := false;
    lblTransb1.font.Color := clgreen;
  end
  else
  begin
    dtTrans1.clear;
    hrTrans1.clear;
    dttrans1.Enabled := true;
    hrTrans1.Enabled := true;
    lblTransb1.font.Color := clBlue;
  end;
  if QtarefasCODCON.Value > 0 then
    cbCTe.Enabled := true
  else
    cbcte.Enabled := false;

  if QtarefasDESTINO.Value = QtarefasNM_DES.Value then
    lblDestino.Caption := ''
  else
    lblDestino.Caption := QtarefasDESTINO.Value;

  QMonitoramento.Close;
  QMonitoramento.sql[2] := 'Where sequencia = ' + QtarefasSEQUENCIA.AsString;
  QMonitoramento.Open;
  if QMonitoramento.eof then
  begin
    QMonitoramento.Close;
    QMonitoramento.sql[2] := 'Where id = ' + QuotedStr(QtarefasNRO_OCS.AsString);
    QMonitoramento.Open;
  end;
  QMonitoramento.Last;

  if not QMonitoramentoOBSERVACAO.IsNull then
    obs.Lines.Add(QMonitoramentoOBSERVACAO.Value);
  QMonitoramento.First;

  dtTrans1.Visible := false;
  hrTrans1.Visible := false;
  lbltransb1.Caption := '';
  lblTransf.Caption := QtarefasNOMMOT.AsString;
end;

procedure TfrmMonitCol.QtarefasBeforeOpen(DataSet: TDataSet);
begin
  QColeta.open;
end;

procedure TfrmMonitCol.pnLegendaExit(Sender: TObject);
begin
  pnLegenda.Visible := False;
end;

procedure TfrmMonitCol.QColetaAfterOpen(DataSet: TDataSet);
begin
  QNF.close;
  QNF.sql.clear;
  QNF.sql.Add('select id_ioc id, codocs cod, notfis, quanti, pesokg, vlrmer,datnot from rodioc ');
  QNF.sql.Add('where codocs = :0 and filocs = :1 ');
  QNF.Parameters[0].value := Qtarefasnro_ocs.AsInteger;
  QNF.Parameters[1].value := QtarefasFilial.AsInteger;
  QNF.open;

  NF.Close;
  NF.open;
  QNF.First;
  while not QNF.eof do
  begin
    NF.append;
    NFREG.Value := 1;
    NFID.value := QNFID.AsInteger;
    NFCOD.Value := QNFCOD.AsInteger;
    NFNOTFIS.value := QNFNOTFIS.AsString;
    NFQUANTI.value := QNFQUANTI.Value;
    NFPESOKG.value := QNFPESOKG.value;
    NFVLRMER.value := QNFVLRMER.value;
    NFDataNF.Value := QNFDATNOT.Value;
    NF.post;
    QNF.next;
  end;
  NF.First;  

  if tipo <> 'I' then
  begin
    eddtcolreal.date := QColetadt_coleta.Value;
    if not QColetadt_coleta.IsNull then
      EdHrcolreal.Text := TimeToStr(QColetadt_coleta.Value)
    else
      EdHrcolreal.text := '00:00';
    eddtdesreal.date := QColetaDT_ENTREGA.Value;
    if not QColetaDT_ENTREGA.IsNull then
      edhrdesreal.Text := TimeToStr(QColetaDT_ENTREGA.Value)
    else
      EdHrdesreal.text := '00:00';
  end;

  if QColetaOCORR_COL.value > 0 then
  begin
    QOcorr.locate('codigo',QColetaOCORR_COL.value,[]);
    cbOcoCol.ItemIndex := cbOcoCol.Items.IndexOf(QocorrDESCRI.value);
  end
  else
    cbOcoCol.itemindex := -1;

  obs.Clear;
  if not QColetaOBS_MON.IsNull then
    obs.Lines.Add(QColetaOBS_MON.Value);

end;

procedure TfrmMonitCol.QtarefasAfterOpen(DataSet: TDataSet);
begin
  QColeta.close;
  QColeta.Parameters[0].Value := QTarefasSEQUENCIA.AsInteger;
  QColeta.Open;
end;

procedure TfrmMonitCol.QtarefasCalcFields(DataSet: TDataSet);
begin
  if not QtarefasDT_COLETA_PREV.IsNull then
  begin
    if (QtarefasCODCON.Value = 0) and (QtarefasCODRMC.Value > 0) then
      // tem coleta mas n�o foi emitido CT-e
      QtarefasStatus.value := 'Coletando'
    else if (QtarefasCODCON.Value = 0) and (QtarefasCODRMC.Value = 0) then
      QtarefasStatus.value := 'A Coletar'
    else if (QtarefasCODCON.Value > 0) and (QtarefasCODMAN.Value = 0) then
      QtarefasStatus.value := 'A Manifestar'
    else
      QtarefasStatus.value := 'Em Transito';
  end
  // n�o tem coleta
  else
  begin
    if QtarefasCODCON.Value = 0 then
      QtarefasStatus.value := 'Em Processo'
    else if (QtarefasCODCON.Value > 0) and (QtarefasCODMAN.Value = 0) then
      QtarefasStatus.value := 'A Manifestar'
    else
      QtarefasStatus.value := 'Em Transito';
  end;
end;

procedure TfrmMonitCol.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

end.
