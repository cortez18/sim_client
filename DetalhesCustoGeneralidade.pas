unit DetalhesCustoGeneralidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, JvBaseEdits, Buttons, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, DB, ADODB, Vcl.DBCtrls;

type
  TfrmDetalhesCustoGeneralidade = class(TForm)
    QGener: TADOQuery;
    dtsGener: TDataSource;
    QGenerCTE: TFMTBCDField;
    QGenerMANIFESTO: TFMTBCDField;
    QGenerUSUARIO_INC: TStringField;
    QGenerTIPO: TStringField;
    QGenerVEICULO: TStringField;
    QGenerTABELA_CUSTO: TFMTBCDField;
    QGenerTABELA_RECEITA: TFMTBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDetalhesCustoGeneralidade: TfrmDetalhesCustoGeneralidade;

implementation

uses Dados, RelCusto_Generalidades;

{$R *.dfm}

procedure TfrmDetalhesCustoGeneralidade.FormActivate(Sender: TObject);
begin
  QGener.close;
  QGener.Parameters[0].value := frmRelCusto_Generalidades.mdGenerAutorizacao.AsInteger;
  QGener.Open;
end;

procedure TfrmDetalhesCustoGeneralidade.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QGener.close;
end;

end.
