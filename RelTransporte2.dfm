object frmRelTransporte2: TfrmRelTransporte2
  Left = 0
  Top = 0
  Caption = 'Rela'#231#227'o de Transportes'
  ClientHeight = 544
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 12
    Top = 8
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 187
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 12
    Top = 27
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 519
    Width = 772
    Height = 23
    Progress = 0
  end
  object Label5: TLabel
    Left = 380
    Top = 47
    Width = 57
    Height = 16
    Caption = 'Diret'#243'rio :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 1020
    Top = 6
    Width = 47
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 486
    Top = 28
    Width = 42
    Height = 13
    Caption = 'Destino :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 611
    Top = 8
    Width = 20
    Height = 13
    Caption = 'Filial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object dtInicial: TJvDateEdit
    Left = 78
    Top = 5
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 247
    Top = 5
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 1
  end
  object btRelatorio: TBitBtn
    Left = 362
    Top = 3
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 4
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 12
    Top = 46
    Width = 145
    Height = 21
    LookupDisplay = 'nr_cnpj_cpf'
    LookupField = 'NR_CNPJ_CPF'
    LookupSource = DataSource1
    TabOrder = 2
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 163
    Top = 47
    Width = 303
    Height = 21
    LookupDisplay = 'nm_cliente'
    LookupField = 'nr_cnpj_cpf'
    LookupSource = DataSource1
    TabOrder = 3
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object btnExcel: TBitBtn
    Left = 873
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Excel'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
      346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
      2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
      33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
      38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
      3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
      40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
      73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
      33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
      64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
      33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
      33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
      3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
      D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
      49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
      CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
      53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
      C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
      BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
      62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
      56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
      45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
      4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
      4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
      49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
      4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
      4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 5
    OnClick = btnExcelClick
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 73
    Width = 1013
    Height = 439
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'NM_CLI'
        Title.Caption = 'Cliente'
        Width = 244
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_CLI'
        Title.Caption = 'CNPJ Cliente'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_REM'
        Title.Caption = 'Remetente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_REM'
        Title.Caption = 'CNPJ_Remetente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_REM'
        Title.Caption = 'Cidade Remetente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_REM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_DES'
        Title.Caption = 'Cliente Destino'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_DES'
        Title.Caption = 'CNPJ Destino'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_DES'
        Title.Caption = 'Cidade Destino'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_DES'
        Title.Caption = 'UF Destino'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_EXP'
        Title.Caption = 'Expedidor'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_EXP'
        Title.Caption = 'CNPJ Expedidor'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_EXP'
        Title.Caption = 'Cidade Expedidor'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_EXP'
        Title.Caption = 'UF Expedidor'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_RED'
        Title.Caption = 'Redespacho'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_RED'
        Title.Caption = 'CNPJ Redespacho'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_RED'
        Title.Caption = 'Cidade Redespacho'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_RED'
        Title.Caption = 'UF Redespacho'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REGIAO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_CTRC'
        Title.Caption = 'Data CT-e'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CTRC'
        Title.Caption = 'CT-e'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_SERIE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_NOTA'
        Title.Caption = 'N'#186' NF'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_NF'
        Title.Caption = 'Valor NF'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_KG'
        Title.Caption = 'Peso Kg.'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_CUB'
        Title.Caption = 'Peso Cub.'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QT_VOLUME'
        Title.Caption = 'Volumes'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_FRETE'
        Title.Caption = 'Frete Peso'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_PEDAGIO'
        Title.Caption = 'Valor Ped'#225'gio'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_SEGURO'
        Title.Caption = 'AdValorem'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_GRIS'
        Title.Caption = 'Valor GRIS'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_OUTROS'
        Title.Caption = 'Valor Outros'
        Width = 79
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CAT'
        Title.Caption = 'Valor CAT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_DESPACHO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ADEME'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TOTAL'
        Title.Caption = 'Frete Total'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ALIQUOTA'
        Title.Caption = 'Aliquota'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_IMPOSTO'
        Title.Caption = 'ICMS/ISS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPERACAO'
        Title.Caption = 'Opera'#231#227'o'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_TIPO'
        Title.Caption = 'Ve'#237'culo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_FATURA'
        Title.Caption = 'Fatura'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_TRANSPORTADORA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IESTADUAL'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CFOP_CLESS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TIPCON'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'tipo_cte'
        Title.Caption = 'Tipo_CT-e'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_TABELA'
        Title.Caption = 'Tabela Venda'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_STATUS'
        Title.Caption = 'Status'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MANIFESTO'
        Title.Caption = 'Manifesto'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 224
    Top = 176
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 6
    Visible = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 1019
    Top = 47
    Width = 157
    Height = 495
    TabOrder = 8
  end
  object DriveComboBox1: TDriveComboBox
    Left = 1019
    Top = 22
    Width = 154
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 9
  end
  object Memo1: TMemo
    Left = 300
    Top = 244
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 10
    Visible = False
  end
  object JvDBLookupEdit1: TJvDBLookupEdit
    Left = 486
    Top = 47
    Width = 145
    Height = 21
    LookupDisplay = 'NR_CNPJ'
    LookupField = 'NR_CNPJ'
    LookupSource = DataSource2
    TabOrder = 11
    Text = ''
    OnCloseUp = JvDBLookupEdit1CloseUp
    OnExit = JvDBLookupEdit1CloseUp
  end
  object JvDBLookupEdit2: TJvDBLookupEdit
    Left = 645
    Top = 47
    Width = 303
    Height = 21
    LookupDisplay = 'NM_CLIENTE'
    LookupField = 'NR_CNPJ'
    LookupSource = DataSource2
    TabOrder = 12
    Text = ''
    OnCloseUp = JvDBLookupEdit1CloseUp
    OnExit = JvDBLookupEdit1CloseUp
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 7
    Top = 518
    Width = 190
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
    TabOrder = 13
  end
  object cbFilial: TJvComboBox
    Left = 645
    Top = 5
    Width = 169
    Height = 21
    Style = csDropDownList
    TabOrder = 14
    Text = ''
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    OnCalcFields = QCtrcCalcFields
    Parameters = <>
    SQL.Strings = (
      'select * from VW_itc_transporte')
    Left = 224
    Top = 236
    object QCtrcNM_CLI: TStringField
      FieldName = 'NM_CLI'
      Size = 50
    end
    object QCtrcCNPJ_CLI: TStringField
      FieldName = 'CNPJ_CLI'
      Size = 18
    end
    object QCtrcNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      Size = 50
    end
    object QCtrcNR_CNPJ_CPF_EXP: TStringField
      FieldName = 'NR_CNPJ_CPF_EXP'
    end
    object QCtrcNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      Size = 50
    end
    object QCtrcNR_CNPJ_CPF_DES: TStringField
      FieldName = 'NR_CNPJ_CPF_DES'
    end
    object QCtrcVL_NF: TBCDField
      FieldName = 'VL_NF'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_FRETE: TBCDField
      FieldName = 'VL_FRETE'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_SEGURO: TBCDField
      FieldName = 'VL_SEGURO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_GRIS: TBCDField
      FieldName = 'VL_GRIS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcCAT: TBCDField
      FieldName = 'CAT'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_ADEME: TBCDField
      FieldName = 'VL_ADEME'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_DESPACHO: TBCDField
      FieldName = 'VL_DESPACHO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QCtrcVL_TOTAL: TBCDField
      FieldName = 'VL_TOTAL'
      DisplayFormat = '##,##0.00'
      Precision = 9
      Size = 2
    end
    object QCtrcVL_ALIQUOTA: TFloatField
      FieldName = 'VL_ALIQUOTA'
      DisplayFormat = '##,##0.00'
    end
    object QCtrcVL_IMPOSTO: TFloatField
      FieldName = 'VL_IMPOSTO'
      DisplayFormat = '##,##0.00'
    end
    object QCtrcFL_TIPO: TWideStringField
      FieldName = 'FL_TIPO'
      FixedChar = True
    end
    object QCtrcCIDADE_EXP: TStringField
      FieldName = 'CIDADE_EXP'
      Size = 50
    end
    object QCtrcUF_EXP: TStringField
      FieldName = 'UF_EXP'
      FixedChar = True
      Size = 2
    end
    object QCtrcCIDADE_DES: TStringField
      FieldName = 'CIDADE_DES'
      Size = 50
    end
    object QCtrcUF_DES: TStringField
      FieldName = 'UF_DES'
      FixedChar = True
      Size = 2
    end
    object QCtrcNM_CLIENTE_RED: TStringField
      FieldName = 'NM_CLIENTE_RED'
      Size = 50
    end
    object QCtrcNR_CNPJ_CPF_RED: TStringField
      FieldName = 'NR_CNPJ_CPF_RED'
    end
    object QCtrcCIDADE_RED: TStringField
      FieldName = 'CIDADE_RED'
      Size = 50
    end
    object QCtrcUF_RED: TStringField
      FieldName = 'UF_RED'
      FixedChar = True
      Size = 2
    end
    object QCtrcPESO_KG: TBCDField
      FieldName = 'PESO_KG'
      DisplayFormat = '#,##0.0000'
      Precision = 32
    end
    object QCtrcQT_VOLUME: TBCDField
      FieldName = 'QT_VOLUME'
      Precision = 32
    end
    object QCtrcNR_FATURA: TBCDField
      FieldName = 'NR_FATURA'
      Precision = 32
      Size = 0
    end
    object QCtrcNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 5
    end
    object QCtrcCNPJ_TRANSPORTADORA: TStringField
      FieldName = 'CNPJ_TRANSPORTADORA'
    end
    object QCtrcIESTADUAL: TStringField
      FieldName = 'IESTADUAL'
    end
    object QCtrcNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
    end
    object QCtrcNR_CTRC: TBCDField
      FieldName = 'NR_CTRC'
      Precision = 32
    end
    object QCtrcDT_CTRC: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_CTRC'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QCtrcOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 200
    end
    object QCtrcCFOP_CLESS: TStringField
      FieldName = 'CFOP_CLESS'
      Size = 6
    end
    object QCtrcTIPCON: TStringField
      FieldName = 'TIPCON'
      FixedChar = True
      Size = 1
    end
    object QCtrctipo_cte: TStringField
      FieldKind = fkCalculated
      FieldName = 'tipo_cte'
      Size = 15
      Calculated = True
    end
    object QCtrcPESO_CUB: TBCDField
      FieldName = 'PESO_CUB'
      Precision = 32
    end
    object QCtrcVL_DESCARGA: TBCDField
      FieldName = 'VL_DESCARGA'
      Precision = 32
    end
    object QCtrcVL_ENTREGA: TBCDField
      FieldName = 'VL_ENTREGA'
      Precision = 32
    end
    object QCtrcVL_TDE: TBCDField
      FieldName = 'VL_TDE'
      Precision = 32
    end
    object QCtrcVL_ADICIONAL: TBCDField
      FieldName = 'VL_ADICIONAL'
      Precision = 32
    end
    object QCtrcDESCRICAO_ADICIONAL: TStringField
      FieldName = 'DESCRICAO_ADICIONAL'
      Size = 30
    end
    object QCtrcFATURA: TBCDField
      FieldName = 'FATURA'
      Precision = 32
    end
    object QCtrcCTE_ID: TStringField
      FieldName = 'CTE_ID'
      Size = 48
    end
    object QCtrcNR_TABELA: TBCDField
      FieldName = 'NR_TABELA'
      Precision = 32
      Size = 0
    end
    object QCtrcREGIAO: TStringField
      FieldName = 'REGIAO'
      Size = 12
    end
    object QCtrcFL_STATUS: TStringField
      FieldName = 'FL_STATUS'
      FixedChar = True
      Size = 1
    end
    object QCtrcNM_CLIENTE_REM: TStringField
      FieldName = 'NM_CLIENTE_REM'
      Size = 80
    end
    object QCtrcNR_CNPJ_CPF_REM: TStringField
      FieldName = 'NR_CNPJ_CPF_REM'
    end
    object QCtrcCIDADE_REM: TStringField
      FieldName = 'CIDADE_REM'
      Size = 40
    end
    object QCtrcUF_REM: TStringField
      FieldName = 'UF_REM'
      Size = 2
    end
    object QCtrcMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
    end
  end
  object navnavig: TDataSource
    DataSet = QCtrc
    Left = 272
    Top = 236
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClienteBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct c.codclifor cod_cliente, c.razsoc nm_cliente, c.' +
        'codcgc nr_cnpj_cpf '
      
        'from cyber.rodcli c left join tb_conhecimento P on p.cod_pagador' +
        ' = c.codclifor '
      
        'where p.fl_status = '#39'A'#39' and p.fl_empresa = :0 order by nm_client' +
        'e')
    Left = 56
    Top = 52
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 116
    Top = 52
  end
  object QDestino: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT distinct DESTINATARIO.Razsoc nm_cliente, DESTINATARIO.cod' +
        'cgc nr_cnpj'
      
        '       FROM rodcon C INNER JOIN rodcli DESTINATARIO ON C.coddes ' +
        '= DESTINATARIO.codclifor'
      '       where DESTINATARIO.codcgc is not null '
      ''
      'union'
      ''
      
        'SELECT distinct REDESPACHO.Razsoc nm_cliente, REDESPACHO.codcgc ' +
        'nr_cnpj_cpf'
      
        '      FROM rodcon C LEFT OUTER JOIN rodcli REDESPACHO ON C.codre' +
        'd = REDESPACHO.codclifor                             '
      '      where REDESPACHO.codcgc is not null ')
    Left = 252
    Top = 56
    object QDestinoNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QDestinoNR_CNPJ: TStringField
      FieldName = 'NR_CNPJ'
      ReadOnly = True
    end
  end
  object DataSource2: TDataSource
    DataSet = QDestino
    Left = 312
    Top = 56
  end
  object QSite: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct s.codfil, (s.codfil || '#39' - '#39' || substr(s.razsoc,' +
        '1,instr(s.razsoc,'#39' '#39')-1)|| '#39' - '#39' || c.descri) filial'
      
        'from tb_manifesto m left join cyber.rodfil s on m.filial = s.cod' +
        'fil'
      
        '                    left join cyber.rodmun c on s.codmun = c.cod' +
        'mun'
      'where s.ativa = '#39'S'#39
      'and m.status <> '#39'C'#39
      'order by 1')
    Left = 548
    object QSiteFILIAL: TStringField
      FieldName = 'FILIAL'
      ReadOnly = True
      Size = 106
    end
    object QSiteCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
  end
end
