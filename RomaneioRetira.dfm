object frmRomaneioRetira: TfrmRomaneioRetira
  Left = 0
  Top = 0
  Caption = 'Mapa de Separa'#231#227'o - Retira'
  ClientHeight = 560
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 8
    Top = 8
    Width = 794
    Height = 1123
    Margins.LeftMargin = 5.000000000000000000
    Margins.TopMargin = 5.000000000000000000
    Margins.RightMargin = 5.000000000000000000
    Margins.BottomMargin = 5.000000000000000000
    DataSource = dtsMapaItens
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      Left = 19
      Top = 19
      Width = 756
      Height = 135
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        AlignWithMargins = True
        Left = 311
        Top = 3
        Width = 103
        Height = 12
        Alignment = taCenter
        Caption = 'Romaneio e Carga - n'#186
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel2: TRLLabel
        Left = 3
        Top = 8
        Width = 62
        Height = 16
        Caption = 'INTECOM'
      end
      object RLLabel3: TRLLabel
        Left = 16
        Top = 30
        Width = 83
        Height = 12
        Caption = 'Transportadora :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel4: TRLLabel
        Left = 594
        Top = 30
        Width = 81
        Height = 12
        Caption = 'Data de emiss'#227'o :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel5: TRLLabel
        Left = 16
        Top = 52
        Width = 54
        Height = 12
        Caption = 'Motorista :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel6: TRLLabel
        Left = 220
        Top = 90
        Width = 140
        Height = 16
        Caption = 'PBR _______________'
      end
      object RLLabel7: TRLLabel
        Left = 376
        Top = 90
        Width = 156
        Height = 16
        Caption = 'CHEP ________________'
      end
      object RLLabel8: TRLLabel
        Left = 4
        Top = 118
        Width = 29
        Height = 16
        Caption = 'N.F.'
      end
      object RLLabel9: TRLLabel
        Left = 144
        Top = 118
        Width = 73
        Height = 16
        Caption = 'Destinat'#225'rio'
      end
      object RLLabel10: TRLLabel
        Left = 352
        Top = 118
        Width = 44
        Height = 16
        Caption = 'Cidade'
      end
      object RLLabel11: TRLLabel
        Left = 570
        Top = 118
        Width = 21
        Height = 16
        Caption = 'UF'
      end
      object RLLabel12: TRLLabel
        Left = 681
        Top = 118
        Width = 48
        Height = 16
        Caption = 'Volume'
      end
      object RLDBText1: TRLDBText
        Left = 120
        Top = 30
        Width = 75
        Height = 12
        DataField = 'transportadora'
        DataSource = dtsMapaCarga
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText3: TRLDBText
        Left = 88
        Top = 52
        Width = 62
        Height = 12
        DataField = 'MOTORISTA'
        DataSource = dtsMapaCarga
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText4: TRLDBText
        Left = 552
        Top = 52
        Width = 45
        Height = 12
        DataField = 'VEICULO'
        DataSource = dtsMapaCarga
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLLabel13: TRLLabel
        Left = 484
        Top = 52
        Width = 42
        Height = 12
        Caption = 'Ve'#237'culo :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel14: TRLLabel
        Left = 644
        Top = 52
        Width = 31
        Height = 12
        Caption = 'Placa :'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
      end
      object RLDBText5: TRLDBText
        Left = 688
        Top = 52
        Width = 34
        Height = 12
        DataField = 'PLACA'
        DataSource = dtsMapaCarga
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 625
        Top = 3
        Width = 84
        Height = 16
        Alignment = taRightJustify
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        Info = itPageNumber
        ParentFont = False
        Text = 'P'#225'gina :'
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 712
        Top = 3
        Width = 25
        Height = 21
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        Info = itLastPageNumber
        ParentFont = False
        Text = 'de '
      end
      object RLDBText11: TRLDBText
        Left = 693
        Top = 30
        Width = 33
        Height = 12
        DataField = 'DTINC'
        DataSource = dtsMapaCarga
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLLabel22: TRLLabel
        Left = 67
        Top = 118
        Width = 44
        Height = 16
        Caption = 'Pedido'
      end
    end
    object RLBand2: TRLBand
      Left = 19
      Top = 154
      Width = 756
      Height = 20
      object RLDBText6: TRLDBText
        Left = 67
        Top = 1
        Width = 44
        Height = 14
        DataField = 'NR_AUF'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText7: TRLDBText
        Left = 144
        Top = 1
        Width = 32
        Height = 14
        DataField = 'NAME'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText8: TRLDBText
        Left = 352
        Top = 1
        Width = 40
        Height = 14
        DataField = 'CIDADE'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText9: TRLDBText
        Left = 573
        Top = 1
        Width = 16
        Height = 14
        DataField = 'UF'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText10: TRLDBText
        Left = 681
        Top = 2
        Width = 58
        Height = 14
        Alignment = taRightJustify
        DataField = 'SEQ_TOUR'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText12: TRLDBText
        Left = 8
        Top = 1
        Width = 29
        Height = 14
        DataField = 'nr_nf'
        DataSource = dtsMapaItens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
    end
    object RLBand3: TRLBand
      Left = 19
      Top = 174
      Width = 756
      Height = 477
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      BeforePrint = RLBand3BeforePrint
      object RLLabel15: TRLLabel
        Left = 3
        Top = 12
        Width = 40
        Height = 16
        Caption = 'Total :'
      end
      object RLLabel16: TRLLabel
        Left = 3
        Top = 44
        Width = 80
        Height = 16
        Caption = 'Observa'#231#227'o :'
      end
      object RLLabel17: TRLLabel
        Left = 699
        Top = 6
        Width = 40
        Height = 16
        Alignment = taRightJustify
        Caption = 'Total :'
      end
      object RLDBRichText1: TRLDBRichText
        Left = 24
        Top = 66
        Width = 689
        Height = 16
        AutoSize = False
        Behavior = [beSiteExpander]
        DataField = 'OBSERVACAO'
        DataSource = dtsMapaCarga
      end
      object RLDraw1: TRLDraw
        Left = 3
        Top = 108
        Width = 750
        Height = 9
        DrawKind = dkLine
      end
      object RLMemo1: TRLMemo
        Left = 10
        Top = 123
        Width = 735
        Height = 212
        Behavior = [beSiteExpander]
        Lines.Strings = (
          
            'Declaro ter recebido e realizado a verifica'#231#227'o dos volumes const' +
            'antes  neste documento e os mesmos encontram-se em conformidade ' +
            'quanto a:'
          ''
          'Inviolabilidade:'
          '- Caixas sem avarias ou remendos, fechadas e com fita lacre.'
          'Quantidade:'
          
            '- Os volumes constantes est'#227'o de acordo com o descrito neste rom' +
            'aneio.'
          
            '- N'#227'o h'#225' rasuras ou anota'#231#245'es manuscritas neste romaneio no que ' +
            'se refere a tipo e volume.'
          'Notas-Fiscais e etiquetas de identifica'#231#227'o: '
          '-N'#227'o h'#225' rasuras. '
          'Identifica'#231#227'o de fr'#225'gil '
          
            '-Declaro estar ciente de que as caixas contendo etiqueta "FR'#193'GIL' +
            '" dever'#227'o ser transportadas com cuidados apropriados de modo que' +
            ' assegure sua preserva'#231#227'o .')
      end
      object RLLabel18: TRLLabel
        Left = 10
        Top = 359
        Width = 406
        Height = 16
        Caption = 'Motorista : ________________________________________________'
      end
      object RLLabel19: TRLLabel
        Left = 10
        Top = 427
        Width = 406
        Height = 16
        Caption = 'Conferente : ________________________________________________'
      end
      object RLLabel20: TRLLabel
        Left = 451
        Top = 359
        Width = 217
        Height = 16
        Caption = 'RG : __________________________'
      end
      object RLDBText2: TRLDBText
        Left = 164
        Top = 381
        Width = 79
        Height = 16
        DataField = 'MOTORISTA'
        DataSource = dtsMapaCarga
        Text = ''
      end
      object RLLabel23: TRLLabel
        Left = 451
        Top = 427
        Width = 217
        Height = 16
        Caption = 'RG : __________________________'
      end
    end
    object RLBand4: TRLBand
      Left = 19
      Top = 651
      Width = 756
      Height = 22
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = False
      Borders.DrawRight = True
      Borders.DrawBottom = True
      BeforePrint = RLBand4BeforePrint
      object RLLabel21: TRLLabel
        Left = 3
        Top = 3
        Width = 84
        Height = 15
        Caption = 'Impresso Por :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 701
        Top = 3
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
    end
  end
  object dtsMapaItens: TDataSource
    DataSet = frmGeraRoma.QMapaItens
    Left = 88
    Top = 264
  end
  object dtsMapaCarga: TDataSource
    DataSet = frmGeraRoma.qMapaCarga
    Left = 336
    Top = 216
  end
  object Query1: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <>
    Left = 156
    Top = 264
  end
  object QTotal: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 2344
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select sum(seq_tour) total from ('
      'select case'
      
        ' when c.leitcode = '#39'com embalagem'#39' then (select SUM(AUFPOS.MNG_B' +
        'EST_ORG / ARTLAG.MNG_EMB) from aufpos@Wmswebprd , artlag@Wmswebp' +
        'rd'
      
        '      where aufpos.nr_auf = p.nr_auf and aufpos.id_artikel = art' +
        'lag.id_artikel and aufpos.lager = artlag.lager and aufpos.lager ' +
        '= p.lager and aufpos.id_klient = p.id_klient)'
      
        ' when c.leitcode = '#39'sem embalagem'#39' then (select SUM(AUFPOS.MNG_B' +
        'EST_ORG) from aufpos@Wmswebprd'
      
        '      where aufpos.nr_auf = p.nr_auf and aufpos.lager = p.lager ' +
        'and aufpos.id_klient = p.id_klient)'
      ' when c.leitcode = '#39'Reembalado'#39' then'
      
        '   (select count(*) from itc_man_emb@cyberintecprd where pedido_' +
        'wms = p.nr_auf and cod_fil = :0)'
      ' else'
      '  seq_tour end seq_tour'
      
        'from auftraege p left join adressen c on c.id_eigner_2 = p.id_kl' +
        'ient and c.kl_eigner_1 = '#39'MAR'#39
      'Where p.PERCENT = :1)')
    Left = 148
    Top = 474
    object QTotalTOTAL: TBCDField
      FieldName = 'TOTAL'
      ReadOnly = True
      Precision = 32
    end
  end
end
