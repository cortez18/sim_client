unit RelFatxPrazo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB, DBCtrls, JvToolEdit, JvMaskEdit, ExtCtrls,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, Mask, JvExMask, JvDBLookup, Buttons,
  Gauges, ShellAPI;

type
  TfrmRelFatxPrazo = class(TForm)
    QFatura: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    Label1: TLabel;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    btnExcel: TBitBtn;
    edFatura: TJvMaskEdit;
    Label2: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    DBNavigator1: TDBNavigator;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    QFaturaNUMDUP: TBCDField;
    QFaturaDATEMI: TDateTimeField;
    QFaturaCODDOC: TBCDField;
    QFaturaDT_ENTREGA_PREV: TDateTimeField;
    QFaturaDT_ENTREGA: TDateTimeField;
    QFaturaRAZSOC: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelFatxPrazo: TfrmRelFatxPrazo;

implementation

uses Dados, Menu;

{$R *.dfm}

procedure TfrmRelFatxPrazo.btnExcelClick(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
begin
  memo1 := TStringList.Create;
  texto := 'Relat�rio de Faturas x Prazo de Entrega';
  memo1.Clear();
  memo1.Add(
    '<HTML><HEAD><TITLE>Relat�rio de Faturas x Prazo de Entrega</TITLE>');
  memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add('</STYLE>');
  memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.Add(
    '<table border=0><title>Relat�rio de Faturas x Prazo de Entrega</title></head><body>');
  memo1.Add('<table border=1 align=center>');
  memo1.Add(
    '<TABLE borderColor=#ebebeb cellSpacing=0 cellPadding=2 width="100%" border=1>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr bgColor=#ebebeb>');
  memo1.Add(
    '<TD align=middle width=150><FONT class=texto2>Data Emiss�o</FONT></TD>');
  memo1.Add('<TD align=middle width=50><FONT class=texto2>Fatura</FONT></TD>');
  memo1.Add('<TD align=middle width=150><FONT class=texto2>CT-e</FONT></TD>');
  memo1.Add(
    '<TD align=middle width=150><FONT class=texto2>Data Prevista</FONT></TD>');
  memo1.Add(
    '<TD align=middle width=150><FONT class=texto2>Data Entrega</FONT></TD>');
  memo1.Add(
    '<TD align=middle width=500><FONT class=texto2>Cliente</FONT></TD>');
  memo1.Add('</TR>');

  QFatura.First;
  Gauge1.MaxValue := QFatura.RecordCount;
  while not QFatura.Eof do
  begin
    memo1.Add('<TR>');
    memo1.Add('<TD align=middle width=50><FONT class=texto2>' +
      formatDateTime('dd-mm-yyyy', QFaturaDATEMI.value) + '</FONT></TD>');
    memo1.Add('<TD align=middle width=50><FONT class=texto2>' +
      QFaturaNUMDUP.AsString + '</FONT></TD>');
    memo1.Add('<TD align=middle width=50><FONT class=texto2>' +
      QFaturaCODDOC.AsString + '</FONT></TD>');
    if QFaturaDT_ENTREGA_PREV.isnull then
      memo1.Add(
        '<TD align=middle width=50><FONT class=texto2>&nbsp;</FONT></TD>')
    else
      memo1.Add('<TD align=middle width=500><FONT class=texto2>' +
        formatDateTime('dd-mm-yyyy', QFaturaDT_ENTREGA_PREV.value) +
        '</FONT></TD>');
    if QFaturaDT_ENTREGA.isnull then
      memo1.Add(
        '<TD align=middle width=50><FONT class=texto2>&nbsp;</FONT></TD>')
    else
      memo1.Add('<TD align=middle width=500><FONT class=texto2>' +
        formatDateTime('dd-mm-yyyy', QFaturaDT_ENTREGA.value) + '</FONT></TD>');
    memo1.Add('<TD align=left width=150><FONT class=texto2>' +
      QFaturaRAZSOC.AsString + '</FONT></TD>');
    memo1.Add('</TR>');
    QFatura.Next;
    Gauge1.AddProgress(1);
  end;
  memo1.Add('</tr>');
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('rel_fatxprazo.html');

  ShellExecute(Application.Handle, nil, PChar('rel_fatxprazo.html'), nil, nil,
    SW_SHOWNORMAL);

end;

procedure TfrmRelFatxPrazo.btRelatorioClick(Sender: TObject);
begin
  Panel1.Visible := true;
  Application.ProcessMessages;
  QFatura.close;

  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QFatura.SQL[7] := ' and d.datemi between to_date(''' + dtInicial.Text +
      ' 00:00:00'',''dd/mm/yy hh24:mi:ss'') and to_date(''' + dtFinal.Text +
      ' 23:59:59'',''dd/mm/yy hh24:mi:ss'')';

  if edFatura.Text <> '' then
    QFatura.SQL[8] := ' and d.numdup =  ' + #39 + edFatura.Text + #39;

  if Trim(ledIdCliente.LookupValue) <> '' then
    QFatura.SQL[9] := ' and d.codclifor = ''' + ledIdCliente.LookupValue + '''';
  // Qfatura.SQL.savetofile('c:\sim\sql.txt');
  QFatura.open;

  Panel1.Visible := false;
end;

procedure TfrmRelFatxPrazo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QFatura.close;
  QCliente.close;
end;

procedure TfrmRelFatxPrazo.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmRelFatxPrazo.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
