object frmMapaRetira: TfrmMapaRetira
  Left = 0
  Top = 0
  Caption = 'Mapa de Separa'#231#227'o - Retira'
  ClientHeight = 560
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 9
    Top = 0
    Width = 794
    Height = 1123
    Margins.LeftMargin = 5.000000000000000000
    Margins.TopMargin = 8.000000000000000000
    Margins.RightMargin = 8.000000000000000000
    Margins.BottomMargin = 5.000000000000000000
    DataSource = dtsmdvol
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      Left = 19
      Top = 30
      Width = 745
      Height = 35
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        Left = 3
        Top = 12
        Width = 16
        Height = 16
        Caption = '...'
      end
      object RLLabel2: TRLLabel
        AlignWithMargins = True
        Left = 263
        Top = 3
        Width = 184
        Height = 16
        Alignment = taCenter
        Caption = 'Mapa de Separa'#231#227'o - Retira N'#186
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 661
        Top = 3
        Width = 74
        Height = 16
        Alignment = taRightJustify
        Text = 'Data :'
      end
      object RLDBText6: TRLDBText
        Left = 448
        Top = 3
        Width = 75
        Height = 16
        DataField = 'nr_romaneio'
        DataSource = dtsmdvol
        Text = ''
      end
    end
    object RLBand3: TRLBand
      Left = 19
      Top = 197
      Width = 745
      Height = 103
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      BeforePrint = RLBand3BeforePrint
      object RLLabel14: TRLLabel
        AlignWithMargins = True
        Left = 247
        Top = 6
        Width = 113
        Height = 16
        Caption = 'Total de Volumes :'
      end
      object RLLabel16: TRLLabel
        AlignWithMargins = True
        Left = 538
        Top = 6
        Width = 78
        Height = 16
        Caption = 'Total Notas :'
      end
      object RLLabel17: TRLLabel
        AlignWithMargins = True
        Left = 4
        Top = 73
        Width = 442
        Height = 25
        Caption = 
          'Conferido por : ________________________________________________' +
          '__'
      end
    end
    object RLBand4: TRLBand
      Left = 19
      Top = 300
      Width = 745
      Height = 25
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLSystemInfo2: TRLSystemInfo
        Left = 261
        Top = 3
        Width = 136
        Height = 16
        Alignment = taRightJustify
        Info = itPageNumber
        Text = 'P'#225'gina :'
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 398
        Top = 3
        Width = 124
        Height = 16
        Info = itLastPageNumber
        Text = ' / '
      end
    end
    object RLGroup1: TRLGroup
      Left = 19
      Top = 65
      Width = 745
      Height = 132
      DataFields = 'nf;palet'
      object RLBand2: TRLBand
        Left = 0
        Top = 49
        Width = 745
        Height = 88
        AutoSize = True
        Borders.Sides = sdCustom
        Borders.DrawLeft = True
        Borders.DrawTop = False
        Borders.DrawRight = True
        Borders.DrawBottom = True
        BeforePrint = RLBand2BeforePrint
        object RLDBMemo1: TRLDBMemo
          Left = 84
          Top = 71
          Width = 653
          Height = 16
          Behavior = [beSiteExpander]
          DataField = 'volume'
          DataSource = dtsmdvol
        end
        object RLDBText7: TRLDBText
          Left = 6
          Top = 71
          Width = 32
          Height = 16
          DataField = 'palet'
          DataSource = dtsmdvol
          Text = ''
        end
        object RLLabel8: TRLLabel
          AlignWithMargins = True
          Left = 6
          Top = 2
          Width = 94
          Height = 16
          Caption = 'Notas Fiscais :'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLDBRichText1: TRLDBRichText
          Left = 104
          Top = 4
          Width = 630
          Height = 13
          AutoSize = False
          Behavior = [beSiteExpander]
          DataField = 'nf'
          DataSource = dtsmdvol
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel9: TRLLabel
          AlignWithMargins = True
          Left = 6
          Top = 24
          Width = 36
          Height = 16
          Caption = 'Pedido'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object RLDBRichText3: TRLDBRichText
          Left = 48
          Top = 24
          Width = 645
          Height = 13
          AutoSize = False
          Behavior = [beSiteExpander]
          DataField = 'pedido'
          DataSource = dtsmdvol
        end
        object RLLabel20: TRLLabel
          AlignWithMargins = True
          Left = 6
          Top = 49
          Width = 26
          Height = 16
          Caption = 'Palet'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object RLLabel11: TRLLabel
          AlignWithMargins = True
          Left = 88
          Top = 49
          Width = 38
          Height = 16
          Caption = 'Volume'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object RLLabel19: TRLLabel
          AlignWithMargins = True
          Left = 439
          Top = 49
          Width = 162
          Height = 16
          Caption = 'Total Volumes Para Este Destino :'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText3: TRLDBText
          Left = 607
          Top = 49
          Width = 23
          Height = 16
          DataField = 'tvol'
          DataSource = dtsmdvol
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          ParentFont = False
          Text = ''
        end
        object RLDraw1: TRLDraw
          Left = 3
          Top = 67
          Width = 739
          Height = 3
          DrawKind = dkLine
        end
      end
      object RLBand5: TRLBand
        Left = 0
        Top = 0
        Width = 745
        Height = 49
        BandType = btHeader
        Borders.Sides = sdCustom
        Borders.DrawLeft = True
        Borders.DrawTop = True
        Borders.DrawRight = True
        Borders.DrawBottom = False
        object RLLabel4: TRLLabel
          AlignWithMargins = True
          Left = 11
          Top = 30
          Width = 77
          Height = 16
          Caption = 'Transportador :'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText2: TRLDBText
          Left = 100
          Top = 30
          Width = 281
          Height = 16
          AutoSize = False
          DataField = 'transportadora'
          DataSource = dtsmdvol
          Text = ''
        end
        object RLLabel7: TRLLabel
          AlignWithMargins = True
          Left = 543
          Top = 34
          Width = 29
          Height = 12
          Caption = 'Rota :'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Cambria'
          Font.Style = []
          ParentFont = False
        end
        object RLDBText5: TRLDBText
          Left = 576
          Top = 34
          Width = 157
          Height = 12
          AutoSize = False
          DataField = 'ROTA'
          DataSource = dtsmdvol
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Cambria'
          Font.Style = []
          ParentFont = False
          Text = ''
        end
        object RLLabel3: TRLLabel
          AlignWithMargins = True
          Left = 11
          Top = 6
          Width = 82
          Height = 16
          Caption = 'Destinat'#225'rio :'
        end
        object RLDBText1: TRLDBText
          Left = 100
          Top = 6
          Width = 281
          Height = 16
          AutoSize = False
          DataField = 'destino'
          DataSource = dtsmdvol
          Text = ''
        end
      end
    end
  end
  object qMapa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 16
        Value = 4
      end>
    SQL.Strings = (
      
        'select distinct r.romaneio, k.name destinatario, r.cod_transp tr' +
        'ansportadora, p.id_tour rota, pedido_wms pedido, pallet_bip pale' +
        't'
      
        'from cyber.itc_man_emb r left join pickauf@wmswebprd v on v.nr_l' +
        'e_1 = r.codbar and r.pedido_wms = v.nr_auf'
      
        '                         left join auftraege@wmswebprd p on p.nr' +
        '_auf = v.nr_auf and p.id_klient = v.id_klient_auf '
      
        '                         left join spediteure@wmswebprd t on p.i' +
        'd_spediteur  = t.id_spediteur '
      
        '                         left join kunden@wmswebprd k on p.id_ku' +
        'nde_ware = k.id_kunde'
      'where r.romaneio = :0'
      'and r.cod_fil = :1'
      'and pallet_fim >= 2'
      'order by 5,6')
    Left = 95
    Top = 329
    object qMapaROMANEIO: TBCDField
      FieldName = 'ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qMapaDESTINATARIO: TStringField
      FieldName = 'DESTINATARIO'
      ReadOnly = True
      Size = 40
    end
    object qMapaTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      ReadOnly = True
      Size = 30
    end
    object qMapaROTA: TStringField
      FieldName = 'ROTA'
      ReadOnly = True
      Size = 12
    end
    object qMapaPALET: TBCDField
      FieldName = 'PALET'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qMapaPEDIDO: TStringField
      FieldName = 'PEDIDO'
      ReadOnly = True
    end
  end
  object QVolume: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 3
        Value = 1
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 16
        Value = 4
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '3'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct codbar volume'
      'from cyber.itc_man_emb r '
      'where r.romaneio = :0'
      'and r.cod_fil = :1'
      'and r.pallet_bip = :2'
      'and pedido_wms = :3'
      'order by 1')
    Left = 92
    Top = 378
    object QVolumeVOLUME: TStringField
      FieldName = 'VOLUME'
      ReadOnly = True
      Size = 30
    end
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 3
        Value = 1
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 16
        Value = 4
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct nr_nf'
      'from cyber.itc_man_emb r '
      'where r.romaneio = :0'
      'and r.cod_fil = :1'
      'and pedido_wms = :2'
      'order by 1')
    Left = 48
    Top = 378
    object QNFNR_NF: TBCDField
      FieldName = 'NR_NF'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object Mdvolumes: TJvMemoryData
    AutoCalcFields = False
    FieldDefs = <
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end
      item
        Name = 'destino'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'rota'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'nf'
        DataType = ftString
        Size = 300
      end
      item
        Name = 'pedido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'volume'
        DataType = ftString
        Size = 999999
      end
      item
        Name = 'transportadora'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'palet'
        DataType = ftInteger
      end>
    Left = 48
    Top = 328
    object Mdvolumesnr_romaneio: TIntegerField
      DisplayWidth = 15
      FieldName = 'nr_romaneio'
    end
    object Mdvolumesdestino: TStringField
      DisplayWidth = 50
      FieldName = 'destino'
      Size = 50
    end
    object Mdvolumesrota: TStringField
      FieldName = 'rota'
    end
    object Mdvolumesnf: TStringField
      DisplayWidth = 300
      FieldName = 'nf'
      Size = 300
    end
    object Mdvolumespedido: TStringField
      FieldName = 'pedido'
    end
    object Mdvolumesvolume: TStringField
      DisplayWidth = 999999
      FieldName = 'volume'
      Size = 999999
    end
    object Mdvolumestransportadora: TStringField
      FieldName = 'transportadora'
      Size = 30
    end
    object Mdvolumespalet: TIntegerField
      FieldName = 'palet'
    end
    object Mdvolumestvol: TIntegerField
      FieldName = 'tvol'
    end
  end
  object dtsmdvol: TDataSource
    DataSet = Mdvolumes
    Left = 168
    Top = 336
  end
end
