object frmMonitorCte: TfrmMonitorCte
  Left = 49
  Top = 0
  Caption = 'Monitoramento CT-e'
  ClientHeight = 531
  ClientWidth = 1032
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 118
    Width = 1032
    Height = 380
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 916
    object grMonitor: TDBGrid
      Left = 1
      Top = 1
      Width = 1030
      Height = 378
      Align = alClient
      DataSource = dsMonitor
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = grMonitorDrawColumnCell
      OnDblClick = grMonitorDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CTRC'
          Title.Alignment = taCenter
          Title.Caption = 'N'#250'mero'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CHAVE_CTE'
          Title.Caption = 'Chave'
          Width = 279
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DREGISTRO'
          Title.Alignment = taCenter
          Title.Caption = 'D. Registro'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'XMOTIVO_CTE'
          Title.Caption = 'Status'
          Width = 406
          Visible = True
        end>
    end
  end
  object mCte: TMemo
    Left = 20
    Top = 148
    Width = 856
    Height = 277
    Hint = 'Clique ESC para sair'
    Align = alCustom
    Color = clWhite
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 3
    Visible = False
    OnClick = mCteClick
    OnKeyDown = mCteKeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1032
    Height = 118
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 916
    object GroupBox1: TGroupBox
      Left = 593
      Top = 1
      Width = 438
      Height = 116
      Align = alClient
      Caption = 'Motivo'
      TabOrder = 0
      ExplicitLeft = 477
      ExplicitWidth = 554
      object edMotivo: TDBMemo
        Left = 2
        Top = 15
        Width = 434
        Height = 99
        Align = alClient
        DataField = 'CONFIGURACAO'
        DataSource = dsMonitor
        TabOrder = 0
        ExplicitLeft = 0
        ExplicitTop = 13
        ExplicitWidth = 483
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 592
      Height = 116
      Align = alLeft
      TabOrder = 1
      object gbData: TGroupBox
        Left = 1
        Top = 1
        Width = 590
        Height = 114
        Align = alClient
        Caption = ' Filtro  '
        TabOrder = 0
        ExplicitWidth = 474
        object Label4: TLabel
          Left = 102
          Top = 64
          Width = 6
          Height = 13
          Caption = #224
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label37: TLabel
          Left = 6
          Top = 14
          Width = 18
          Height = 13
          Caption = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object btnPesquisa: TBitBtn
          Left = 289
          Top = 26
          Width = 96
          Height = 27
          Hint = 'Filtro por Data'
          Caption = 'Pesquisar'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
            FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
            96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
            92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
            BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
            CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
            D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
            EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
            E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
            E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
            8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
            E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
            FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
            C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
            CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
            CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
            D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
            9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          TabStop = False
          OnClick = btnPesquisaClick
        end
        object dtInicial: TJvDateEdit
          Left = 9
          Top = 61
          Width = 87
          Height = 21
          Date = 41049.000000000000000000
          DateFormat = 'dd/mm/yyyy'
          DateFormatPreferred = pdCustom
          ShowNullDate = False
          TabOrder = 0
        end
        object dtFinal: TJvDateEdit
          Left = 115
          Top = 61
          Width = 88
          Height = 21
          ShowNullDate = False
          TabOrder = 1
        end
        object btnImprimir: TBitBtn
          Left = 98
          Top = 89
          Width = 87
          Height = 22
          Hint = 'Imprimir Dacte'
          Caption = 'Dacte'
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
            52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
            FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
            D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
            FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
            FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
            BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
            FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
            FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
            A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
            B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
            A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
            FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
            CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
            B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
            FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
            FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
            F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
            A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
            F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
            F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
            8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
            F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
            F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
            F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
            90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
            D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
            BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          TabStop = False
          OnClick = btnImprimirClick
        end
        object btnGerarCtrc: TBitBtn
          Left = 5
          Top = 89
          Width = 87
          Height = 22
          Hint = 'Enviar  CT-e Novamente para o SEFAZ'
          Caption = 'Enviar CT-e'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
            FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
            96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
            92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
            BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
            CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
            D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
            EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
            E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
            E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
            8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
            E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
            FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
            C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
            CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
            CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
            D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
            9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          TabStop = False
          OnClick = btnGerarCtrcClick
        end
        object cbTipo: TComboBox
          Left = 209
          Top = 61
          Width = 176
          Height = 21
          Style = csDropDownList
          TabOrder = 5
          Items.Strings = (
            'Todos'
            'Monitorando'
            'Definitivo com Sucesso'
            'Definitivo com Erro'
            'Erro Schema')
        end
        object btnEnviar: TBitBtn
          Left = 400
          Top = 61
          Width = 69
          Height = 22
          Hint = 'Enviar e-mail'
          Caption = 'E-mail'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF8E512BB06331BB7038BE773CC17B40C27E42C28045C3
            8247C38649C3864AC3874BC3874BC3874CBD8349AC74408E512BA35D31F8F3EE
            F5ECE4FBF5F0FBF7F1FBF7F3FBF8F4FCF9F5FCF9F5FCF9F6FCF9F7FCFAF7FCFA
            F7F7F1ECEAD9CCAB7642BE6F3CFCF9F5ECD0BCF9E4D6FEECDFFEEBDFFEEBDEFE
            EBDBFEEBDCFEEADDFDEADBFDE8D8F8E0CDEACBB3F3EBE3C78B50C27646FDFBF8
            F9E3D2ECCFB9F8E1D0FDE7D6F4D5BDE9BFA0E9BFA2F4D3BDFDE6D4F7DEC9EBCA
            B0F8DBC4F8F2ECC98C50C57D50FDFBF9FDE9D8F9E1D0EBCAB3ECC5A7E3B698F7
            E7DDF7E8DEE3B697ECC3A4EAC5A9F8DAC2FCDFC6F8F3EDC88D50C9865BFDFBF9
            FDE8D7FDE6D4EDC6ABDCAA89F9ECE3FFFBF9FFFCFAF9EEE6DCA887EDBF9CFCDB
            C0FCDBC0F8F3EDC88C50CC8E66FDFBF9FDE5D3F1CCB2E3B596F9EAE0FFF9F5FE
            F3EAFEF4EDFFFBF9F9EDE5E3B08DF0C19EFCD7B7F8F3EDC88C50D09670FDFBF9
            F1CDB1E3B596F9E9DEFEF7F1FDEDE1FEEFE4FEF1E7FEF3EAFFFAF7F9ECE3E2AE
            8AF0BC95F8F4ECC88C50D39D7BFBF6F2E3B496F9E8DCFEF5EEFDE9DAFDEADCFD
            ECDEFDEDE1FEEFE4FEF1E7FFFAF6F9EAE0E2AA85F1E4D9C88C50D7AB91FDFAF8
            FCF5F1FFFCF9FFFCF9FFFCF9FFFCF9FFFCFAFFFCFAFFFCFAFFFCFBFFFDFBFFFD
            FCFBF6F3F8EFEAAB7743C89A7CD5A484D09770CC8F64CC8E62CB8E60CA8C5DC9
            8B5BC88A58C78856C68653C58450C4824DC1834DB279488E512BFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          TabStop = False
          OnClick = btnEnviarClick
        end
        object cbFilial: TComboBox
          Left = 7
          Top = 29
          Width = 276
          Height = 21
          Style = csDropDownList
          TabOrder = 7
        end
        object btnStatus: TBitBtn
          Left = 439
          Top = 88
          Width = 33
          Height = 23
          Hint = 'Status Sefaz'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D58871D58871D58871D58
            87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF1D588756A2D6387BAC1D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D588756A2D6458CBE1D58
            87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF1D58874D97CA56A2D61D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D58873C80B156A2D61D58
            87FFFFFFFFFFFFFFFFFF5BA4B20D92C50291CA0290C90892C83E9BB8FFFFFFFF
            FFFFFFFFFF1D58872C6B9C56A2D61D5887FFFFFFFFFFFFFFFFFF43ABBE7DDBF2
            90E0F58EDFF4A3E5F609B9E50297CD59B6CAB5D3CDAABDA30681B1539ED42B6A
            9A1D5887FFFFFFFFFFFF56A9B585DCF32AC4EB22C2EB61D3F097E2F5B3EAF881
            E1ED2AC9DA17CCDC21A1CF56A2D63A7EB01D5887FFFFFFFFFFFF6DA9AE7ED9EF
            62D3F039C8EC3BC8EC6BD6F197E2F5D5F6F9A9ECF49DEBF289C7D056A2D64692
            C71D5887FFFFFFFFFFFF7DA9AA27B2D189DEF42FC5EB33C7EC67D5F092E1F5BD
            F0F6A0EAF277E3EC83D4DD428CC07DB7DF1D5887FFFFFFFFFFFF84AAA93AAFC6
            85DDF32AC4EB2AC4EB66D5F08BDFF4B2EEF5B3EEF57EE3EE4CD1E03579AC6EAF
            DB1D5887FFFFFFFFFFFF83AAAA49ABBC85DDF38BDFF486DEF4AAE7F7BDEDF99F
            EAF2C4F2F880E3EE73E0ED356E978FC1E4215F8F1D5887FFFFFF83AAAA5CAAB5
            0792C80090CA0092CB0093CC7CD9F1CCF4F9DFF8FBBEF1F7B5EEF5A0B6C286BC
            E22E72A41D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0295CD2D
            C3E9C0EDF89BE3F506B7E40599C0EFF7FB3684BB1D5887FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF7FA9AE52A5B626A2C5249FC35C9AA33465881D58
            871D58871D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          TabStop = False
          OnClick = btnStatusClick
        end
        object btnXML: TBitBtn
          Left = 190
          Top = 89
          Width = 110
          Height = 22
          Caption = 'Carreg. pelo XML'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
            9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
            C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
            9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
            E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
            AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
            DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
            F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
            D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
            F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
            DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
            F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
            E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          TabStop = False
          OnClick = btnXMLClick
        end
        object btnConsXML: TBitBtn
          Left = 306
          Top = 89
          Width = 127
          Height = 22
          Caption = 'Consulta pela Chave'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
            9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
            C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
            9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
            E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
            AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
            DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
            F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
            D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
            F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
            DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
            F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
            E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          TabStop = False
          OnClick = btnConsXMLClick
        end
        object BitBtn2: TBitBtn
          Left = 400
          Top = 11
          Width = 69
          Height = 22
          Hint = 'Averbar'
          Caption = 'ATM'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF8E512BB06331BB7038BE773CC17B40C27E42C28045C3
            8247C38649C3864AC3874BC3874BC3874CBD8349AC74408E512BA35D31F8F3EE
            F5ECE4FBF5F0FBF7F1FBF7F3FBF8F4FCF9F5FCF9F5FCF9F6FCF9F7FCFAF7FCFA
            F7F7F1ECEAD9CCAB7642BE6F3CFCF9F5ECD0BCF9E4D6FEECDFFEEBDFFEEBDEFE
            EBDBFEEBDCFEEADDFDEADBFDE8D8F8E0CDEACBB3F3EBE3C78B50C27646FDFBF8
            F9E3D2ECCFB9F8E1D0FDE7D6F4D5BDE9BFA0E9BFA2F4D3BDFDE6D4F7DEC9EBCA
            B0F8DBC4F8F2ECC98C50C57D50FDFBF9FDE9D8F9E1D0EBCAB3ECC5A7E3B698F7
            E7DDF7E8DEE3B697ECC3A4EAC5A9F8DAC2FCDFC6F8F3EDC88D50C9865BFDFBF9
            FDE8D7FDE6D4EDC6ABDCAA89F9ECE3FFFBF9FFFCFAF9EEE6DCA887EDBF9CFCDB
            C0FCDBC0F8F3EDC88C50CC8E66FDFBF9FDE5D3F1CCB2E3B596F9EAE0FFF9F5FE
            F3EAFEF4EDFFFBF9F9EDE5E3B08DF0C19EFCD7B7F8F3EDC88C50D09670FDFBF9
            F1CDB1E3B596F9E9DEFEF7F1FDEDE1FEEFE4FEF1E7FEF3EAFFFAF7F9ECE3E2AE
            8AF0BC95F8F4ECC88C50D39D7BFBF6F2E3B496F9E8DCFEF5EEFDE9DAFDEADCFD
            ECDEFDEDE1FEEFE4FEF1E7FFFAF6F9EAE0E2AA85F1E4D9C88C50D7AB91FDFAF8
            FCF5F1FFFCF9FFFCF9FFFCF9FFFCF9FFFCFAFFFCFAFFFCFAFFFCFBFFFDFBFFFD
            FCFBF6F3F8EFEAAB7743C89A7CD5A484D09770CC8F64CC8E62CB8E60CA8C5DC9
            8B5BC88A58C78856C68653C58450C4824DC1834DB279488E512BFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          TabStop = False
          OnClick = BitBtn2Click
        end
        object cbXml: TCheckBox
          Left = 400
          Top = 39
          Width = 149
          Height = 17
          Caption = 'Salvar o XML para An'#225'lise ?'
          TabOrder = 12
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 498
    Width = 1032
    Height = 33
    Align = alBottom
    TabOrder = 2
    ExplicitWidth = 916
    object Shape1: TShape
      Left = 9
      Top = 10
      Width = 15
      Height = 15
      Brush.Color = clYellow
      Pen.Style = psClear
      Shape = stCircle
    end
    object Shape2: TShape
      Left = 166
      Top = 10
      Width = 15
      Height = 15
      Brush.Color = clRed
      Pen.Style = psClear
      Shape = stCircle
    end
    object Shape3: TShape
      Left = 348
      Top = 11
      Width = 15
      Height = 15
      Brush.Color = clGreen
      Pen.Style = psClear
      Shape = stCircle
    end
    object Label1: TLabel
      Left = 31
      Top = 12
      Width = 72
      Height = 13
      Caption = 'Monitorando'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 188
      Top = 12
      Width = 107
      Height = 13
      Caption = 'Definitivo com Erro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 370
      Top = 13
      Width = 130
      Height = 13
      Caption = 'Definitivo com Sucesso'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbTotReg: TLabel
      Left = 767
      Top = 13
      Width = 91
      Height = 13
      Caption = 'Total de Registros:'
    end
    object Shape4: TShape
      Left = 556
      Top = 11
      Width = 15
      Height = 15
      Brush.Color = clNavy
      Pen.Style = psClear
      Shape = stCircle
    end
    object Label5: TLabel
      Left = 578
      Top = 13
      Width = 153
      Height = 13
      Caption = 'Enviado como conting'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object tmImprimeDacte: TTimer
    Interval = 10000
    Left = 512
    Top = 280
  end
  object qrMonitor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select cod_conhecimento, cte.nr_conhecimento ctrc,cte.cte_xmsg, ' +
        'cte.cte_xmotivo, cte.averba_protocolo,'
      
        '       nvl(cte.cte_data, dt_conhecimento) dregistro, cia.razsoc ' +
        'cia_name, cte.cte_chave chave_cte,'
      '       case when nvl(cte.cte_xmsg,'#39#39') = '#39#39' then'
      '         case'
      '           when cte.FL_STATUS = '#39'I'#39' then'
      '             '#39'INUTILIZADO'#39
      '           when cte.cte_status is not null then'
      '             '#39'ERRO / PROBLEMA'#39
      '           else'
      '             '#39'A PROCESSAR'#39
      '         end'
      '       else'
      '          case'
      '            when cte.cte_status = 100 then'
      '              cte.cte_xmotivo'
      '            when cte.cte_status = 101 then'
      '              '#39'Cancelado'#39
      '            when cte.cte_status = 135 then'
      '              '#39'Cancelado'#39
      '            else cte.cte_xmsg'
      '          end end xmotivo_cte,'
      '             case'
      '             when cte.FL_STATUS = '#39'I'#39' then '#39'INUTILIZADO'#39
      
        '               when cte.cte_status = 1  then '#39'CONVERTIDO COM SUC' +
        'ESSO'#39
      
        '               when cte.cte_status = 6  then '#39'VALIDADO COM SUCES' +
        'SO'#39
      
        '               when cte.cte_status = 99 then '#39'ERRO NA CONVERS'#195'O'#39 +
        ' || cte.cte_xmsg'
      
        '               when cte.cte_status = 94 then '#39'ERRO NA VALIDA'#199#195'O'#39 +
        '|| cte.cte_xmsg'
      '               when cte.cte_status = 101 then '#39'CT-e CANCELADO'#39
      '               when cte.cte_status = 135 then '#39'CT-e CANCELADO'#39
      
        '               when (cte.cte_prot is null or cte.cte_prot ='#39#39') a' +
        'nd (cte.cte_status is null or cte.cte_status = 0) then '#39'CRIANDO ' +
        'LOTE DE TRANSMISS'#195'O'#39
      '               when cte.cte_status = '#39'100'#39' then cte.cte_xmotivo'
      
        '               when (cte.cte_status is not null) then '#39'ERRO RETO' +
        'RNADO PELA SEFAZ '#39' || cte.cte_xmotivo'
      
        '               when cte.cte_prot is null then '#39'TRANSMITINDO LOTE' +
        #39
      
        '               when cte.cte_status = '#39'103'#39' or cte.cte_status = '#39 +
        '105'#39' then '#39'LOTE EM PROCESSAMENTO NA SEFAZ'#39
      
        '               when cte.cte_status = '#39'-11'#39' then '#39'OCORREU UM ERRO' +
        ' NA TRANSMISS'#195'O DO LOTE PARA A SEFAZ. '#201' NECESS'#193'RIO ENVIAR O DOCU' +
        'MENTO NOVAMENTE.'#39
      '               when cte.cte_status = '#39'104'#39'then '#39'LOTE PROCESSADO'#39
      
        '               when cte.cte_prot <> '#39' AND BAT.BAT_CSTAT = '#39'  the' +
        'n '#39'TRANSMITINDO LOTE'#39
      '               else'
      
        '           '#39'STATUS N'#195'O DEFINIDO. STATUS CTE: '#39' + CAST(cte.cte_st' +
        'atus as varchar(10)) + '#39'. STATUS BAT: '#39' + CAST(cte.cte_status as' +
        ' varchar(10)) || '#39'CTE REGISTRADA NA TABELA CTE'#39
      '       end configuracao,'
      
        '       cte.cte_status, coalesce(cte.cte_prot,'#39#39') cte_prot, cia.c' +
        'odfil cod_loja, cte.fl_contingencia, cte.cte_can_prot'
      
        'from tb_conhecimento cte left join cyber.rodfil cia on cia.codfi' +
        'l = cte.fl_empresa'
      'where fl_tipo = '#39'C'#39
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by 2')
    Left = 248
    Top = 176
    object qrMonitorCOD_CONHECIMENTO: TBCDField
      FieldName = 'COD_CONHECIMENTO'
      Precision = 32
      Size = 0
    end
    object qrMonitorCTRC: TBCDField
      FieldName = 'CTRC'
      Precision = 32
      Size = 0
    end
    object qrMonitorDREGISTRO: TDateTimeField
      FieldName = 'DREGISTRO'
    end
    object qrMonitorCIA_NAME: TStringField
      FieldName = 'CIA_NAME'
      Size = 40
    end
    object qrMonitorXMOTIVO_CTE: TStringField
      FieldName = 'XMOTIVO_CTE'
      ReadOnly = True
      Size = 300
    end
    object qrMonitorCONFIGURACAO: TStringField
      FieldName = 'CONFIGURACAO'
      ReadOnly = True
      Size = 1017
    end
    object qrMonitorCTE_STATUS: TBCDField
      FieldName = 'CTE_STATUS'
      Precision = 32
      Size = 0
    end
    object qrMonitorCTE_PROT: TStringField
      FieldName = 'CTE_PROT'
      ReadOnly = True
      Size = 30
    end
    object qrMonitorCOD_LOJA: TBCDField
      FieldName = 'COD_LOJA'
      Precision = 32
    end
    object qrMonitorCHAVE_CTE: TStringField
      FieldName = 'CHAVE_CTE'
      Size = 44
    end
    object qrMonitorCTE_XMSG: TStringField
      FieldName = 'CTE_XMSG'
      Size = 1000
    end
    object qrMonitorCTE_XMOTIVO: TStringField
      FieldName = 'CTE_XMOTIVO'
      Size = 300
    end
    object qrMonitorAVERBA_PROTOCOLO: TStringField
      FieldName = 'AVERBA_PROTOCOLO'
      Size = 50
    end
    object qrMonitorfl_contingencia: TStringField
      FieldName = 'fl_contingencia'
      Size = 1
    end
    object qrMonitorcte_can_prot: TStringField
      FieldName = 'cte_can_prot'
      Size = 50
    end
  end
  object dsMonitor: TDataSource
    AutoEdit = False
    DataSet = qrMonitor
    Left = 312
    Top = 176
  end
  object QSite: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct f.codcgc nr_cnpj_cpf, f.razsoc nm_loja, f.codfil' +
        ' cod_loja, m.estado ds_uf'
      
        'from cyber.rodfil f left join cyber.rodmun m on f.codmun = m.cod' +
        'mun'
      
        '                    right join tb_conhecimento c on c.fl_empresa' +
        ' = f.codfil and c.nr_serie = '#39'1'#39
      'where f.ativa = '#39'S'#39
      ''
      'order by 3')
    Left = 168
    Top = 176
    object QSiteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
    end
    object QSiteNM_LOJA: TStringField
      FieldName = 'NM_LOJA'
      Size = 50
    end
    object QSiteds_uf: TStringField
      FieldName = 'ds_uf'
      FixedChar = True
      Size = 2
    end
    object QSiteCOD_LOJA: TBCDField
      FieldName = 'COD_LOJA'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsSite: TDataSource
    AutoEdit = False
    DataSet = QSite
    Left = 168
    Top = 228
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*-cte.xml'
    Filter = 
      'Arquivos CTe (*-cte.xml)|*-cte.xml|Arquivos XML (*.xml)|*.xml|To' +
      'dos os Arquivos (*.*)|*.*'#39
    Title = 'Selecione o CTe'
    Left = 380
    Top = 176
  end
  object XMLDoc: TXMLDocument
    FileName = 'C:\Paulo\GISIS_mssql\Schemas\cteTiposBasico_v2.00.xsd'
    Left = 600
    Top = 180
    DOMVendorDesc = 'MSXML'
  end
end
