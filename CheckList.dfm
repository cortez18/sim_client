object frmChecklist: TfrmChecklist
  Left = 0
  Top = 0
  Caption = 'frmCheckList'
  ClientHeight = 612
  ClientWidth = 796
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 2
    Top = 8
    Width = 794
    Height = 1123
    Margins.LeftMargin = 5.000000000000000000
    Margins.RightMargin = 5.000000000000000000
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLMemo1: TRLMemo
      Left = 19
      Top = 41
      Width = 756
      Height = 16
      Align = faWidth
      Alignment = taJustify
    end
    object RLMemo2: TRLMemo
      Left = 19
      Top = 76
      Width = 756
      Height = 64
      Align = faWidth
      Alignment = taJustify
      Lines.Strings = (
        
          'Comprometo-me pela manuten'#231#227'o das condi'#231#245'es atuais do ve'#237'culo, e' +
          ' estou ciente de minhas responsabilidades por quaisquer danos ca' +
          'usados a ele, seja por neglig'#234'ncia, imprud'#234'ncia ou imper'#237'cia. Te' +
          'nho o dever de verificar as condi'#231#245'es do ve'#237'culo e preencher o C' +
          'heck List apropriado antes de cada viagem realizada, e tamb'#233'm pr' +
          'ovidenciar o que for necess'#225'rio durante ela para cumprir as norm' +
          'as e leis previstas no C'#243'digo Brasileiro de Tr'#226'nsito.')
    end
    object RLMemo3: TRLMemo
      Left = 19
      Top = 141
      Width = 756
      Height = 48
      Align = faWidth
      Alignment = taJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      Lines.Strings = (
        
          'Declaro ter pleno conhecimento de que o transporte de animais si' +
          'lvestres, sem licen'#231'a ou autoriza'#231#227'o do IBAMA, '#233' crime previsto ' +
          'na Lei 9.605/98, pun'#237'vel com pena de at'#233' um ano e meio de deten'#231 +
          #227'o, comprometo-me a n'#227'o realiza-lo, sob qualquer hip'#243'tese')
      ParentFont = False
    end
    object RLLabel1: TRLLabel
      Left = 19
      Top = 208
      Width = 138
      Height = 16
      Caption = 'CAVALO/CAMINH'#195'O :'
    end
    object RLLabel2: TRLLabel
      Left = 19
      Top = 244
      Width = 73
      Height = 16
      Caption = 'CARRETA :'
    end
    object RLLabel3: TRLLabel
      Left = 321
      Top = 208
      Width = 55
      Height = 16
      Caption = 'PLACA :'
    end
    object RLLabel4: TRLLabel
      Left = 321
      Top = 244
      Width = 55
      Height = 16
      Caption = 'PLACA :'
    end
    object RLLabel5: TRLLabel
      Left = 593
      Top = 208
      Width = 63
      Height = 16
      Caption = 'VIAGEM :'
    end
    object RLLabel6: TRLLabel
      Left = 593
      Top = 244
      Width = 64
      Height = 16
      Caption = 'ESCALA :'
    end
    object RLDraw1: TRLDraw
      Left = 19
      Top = 276
      Width = 756
      Height = 37
      Align = faWidth
    end
    object RLLabel7: TRLLabel
      Left = 167
      Top = 286
      Width = 452
      Height = 18
      Alignment = taCenter
      Caption = 'ITENS DE VERIFICA'#199#195'O (preencher utilizando legenda abaixo)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object RLLabel8: TRLLabel
      Left = 20
      Top = 319
      Width = 189
      Height = 16
      Caption = 'ITENS OBRIGAT'#211'RIO POR LEI'
    end
    object RLMemo4: TRLMemo
      Left = 19
      Top = 341
      Width = 244
      Height = 144
      Lines.Strings = (
        '____ Tac'#243'grafo aferido / funcionando bem'
        '____ Freios do ve'#237'culo'
        '____ Sist. Dire'#231#227'o ve'#237'culos'
        '____ Para-brisa / retrovisor'
        '____ Lanternas e far'#243'is'
        '____ Paralamas dianteiro / traseiro'
        '____ Para-choque dianteiro / traseiro'
        '____ Faixas refletivas'
        '____ Ve'#237'culo Equip. Tanque Supl.')
    end
    object RLMemo5: TRLMemo
      Left = 272
      Top = 341
      Width = 244
      Height = 128
      Lines.Strings = (
        '____ Macaco Hidr'#225'ulico'
        '____ Triangulo seguran'#231'a'
        '____ Chave roda'
        '____ Cabo chave roda'
        '____ Extintor de inc'#234'ndio'
        '____ Pneus'
        '____ Pneus de estepe'
        '____ Consta Inf. Tang. CRLV')
    end
    object RLMemo6: TRLMemo
      Left = 524
      Top = 341
      Width = 251
      Height = 144
      Lines.Strings = (
        '____ Discos tac'#243'grafo sub / em dia'
        '____ CRLV orig. val. p/ este ano'
        '____ Certif. Registro ANTT'
        '____ Alvar'#225' Sanit. Val. Este ano'
        '____ Adesivo ANTT laterais Veic.'
        '____ Inscri'#231#227'o de TARA / LOTA'#199#194'O'
        '____ Placas leg'#237'veis'
        '____ Arame / lacre placa traseira'
        '____ Autor. p/ conduzir ve'#237'culo')
    end
    object RLMemo7: TRLMemo
      Left = 19
      Top = 494
      Width = 756
      Height = 30
      Align = faWidth
      Alignment = taJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        
          'VE'#205'CULOS N'#195'O PODEM SAIR DE VIAGEM SEM AUTORIZA'#199#195'O DO SUPERVISOR ' +
          'DO MOTORISTA OU SUPERIOR, SE ALGUM DOS ITENS OBRIGAT'#211'RIOS POR LE' +
          'I ESTIVER MARCADO COM C'#211'DIGOS: 9, 10, 11, 12, 13, 14 OU 15.')
      ParentFont = False
    end
    object RLDraw2: TRLDraw
      Left = 19
      Top = 526
      Width = 756
      Height = 5
      Align = faWidth
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = False
      DrawKind = dkLine
      Pen.Width = 3
    end
    object RLLabel9: TRLLabel
      Left = 20
      Top = 533
      Width = 276
      Height = 16
      Caption = 'ITENS DE MANUTEN'#199#195'O E CONSERVA'#199#195'O'
    end
    object RLMemo8: TRLMemo
      Left = 19
      Top = 553
      Width = 244
      Height = 160
      Lines.Strings = (
        '____ Motor / N'#237'vel de '#243'leo'
        '____ Radiador / N'#237'vel '#225'gua'
        '____ L'#226'mpada do painel'
        '____ Tanque combust'#237'vel'
        '____ Caixa de Ferramentas'
        '____ Tampa de bateria'
        '____ Banco do motorista'
        '____ Banco passag. / sof'#225' cama'
        '____ Verifica'#231#227'o visual de Emiss'#227'o de'
        '        Fuma'#231'a no escapamento'#9)
    end
    object RLMemo9: TRLMemo
      Left = 272
      Top = 553
      Width = 244
      Height = 128
      Lines.Strings = (
        '____ Tapetes / Forros Portas'
        '____ Mangueira ar cabine'
        '____ Cortinas'
        '____ Radio / Auto falantes'
        '____ Funilaria / Lataria'
        '____ Grade dianteira'
        '____ Pintura e logomarcas'
        '____ Conserva'#231#227'o / Limpeza')
    end
    object RLMemo10: TRLMemo
      Left = 524
      Top = 553
      Width = 251
      Height = 128
      Lines.Strings = (
        '____ Escadas do ba'#250
        '____ Corr Amarra'#231#227'o rack'
        '____ Canos suporte rack'
        '____ Carrinho de entrega'
        '____ Autotrac / rastreador'
        '____ Manivela (carreta)'
        '____ Chave segredo (carreta)'
        '____ Pino rei (carreta) ')
    end
    object RLDraw3: TRLDraw
      Left = 19
      Top = 719
      Width = 756
      Height = 5
      Align = faWidth
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = False
      DrawKind = dkLine
      Pen.Width = 3
    end
    object RLLabel10: TRLLabel
      Left = 20
      Top = 726
      Width = 74
      Height = 16
      Caption = 'LEGENDA :'
    end
    object RLMemo11: TRLMemo
      Left = 19
      Top = 748
      Width = 244
      Height = 80
      Lines.Strings = (
        '1. OK'
        '2. SUJO'
        '3. VAZANDO'
        '4. TRINCADO'
        '5. RISCADO')
    end
    object RLMemo12: TRLMemo
      Left = 272
      Top = 748
      Width = 244
      Height = 96
      Lines.Strings = (
        '6.  RASGADO'
        '7.  AMASSADO'
        '8.  N'#195'O SE APLICA'
        '9.  RUIM'
        '10. QUEBRADO'
        '')
    end
    object RLMemo13: TRLMemo
      Left = 520
      Top = 748
      Width = 251
      Height = 80
      Lines.Strings = (
        '11. N'#195'O POSSUI'
        '12. QUEIMADO'
        '13. FURADO'
        '14. VENCIDO'
        '15. N'#195'O FUNCIONA')
    end
    object RLDraw4: TRLDraw
      Left = 19
      Top = 834
      Width = 756
      Height = 5
      Align = faWidth
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = False
      DrawKind = dkLine
      Pen.Width = 3
    end
    object RLLabel11: TRLLabel
      Left = 22
      Top = 852
      Width = 746
      Height = 16
      Caption = 
        'Os problemas identificados foram informados para : _____________' +
        '__________________________________________________'
    end
    object RLLabel12: TRLLabel
      Left = 22
      Top = 892
      Width = 749
      Height = 16
      Caption = 
        'Observa'#231#245'es : __________________________________________________' +
        '____________________________________________'
    end
    object RLLabel13: TRLLabel
      Left = 25
      Top = 920
      Width = 746
      Height = 16
      Caption = 
        '________________________________________________________________' +
        '__________________________________________'
    end
    object RLDraw5: TRLDraw
      Left = 19
      Top = 946
      Width = 756
      Height = 5
      Align = faWidth
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = False
      DrawKind = dkLine
      Pen.Width = 3
    end
    object RLLabel14: TRLLabel
      Left = 206
      Top = 968
      Width = 363
      Height = 16
      Caption = '_____ de ______________________________ de __________'
    end
    object RLLabel15: TRLLabel
      Left = 30
      Top = 1028
      Width = 319
      Height = 16
      Caption = '_____________________________________________'
    end
    object RLLabel16: TRLLabel
      Left = 438
      Top = 1028
      Width = 319
      Height = 16
      Caption = '_____________________________________________'
    end
    object RLLabel17: TRLLabel
      Left = 438
      Top = 1050
      Width = 64
      Height = 16
      Caption = 'Supervisor'
    end
    object RLLabel18: TRLLabel
      Left = 678
      Top = 1050
      Width = 57
      Height = 16
      Caption = 'Matr'#237'cula'
    end
    object RLDBText1: TRLDBText
      Left = 30
      Top = 1050
      Width = 122
      Height = 16
      DataField = 'NM_FORNECEDOR'
      DataSource = frmConsManifesto.navnavig
    end
    object RLDBText2: TRLDBText
      Left = 387
      Top = 208
      Width = 47
      Height = 16
      DataField = 'PLACA'
      DataSource = frmConsManifesto.navnavig
    end
    object RLDBText3: TRLDBText
      Left = 662
      Top = 208
      Width = 79
      Height = 16
      DataField = 'MANIFESTO'
      DataSource = frmConsManifesto.navnavig
    end
    object RLDBText4: TRLDBText
      Left = 110
      Top = 244
      Width = 47
      Height = 16
      DataField = 'FROTA'
      DataSource = frmConsManifesto.navnavig
    end
  end
end
