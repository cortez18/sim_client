unit Rel_custoxrec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRel_custoxrec = class(TForm)
    QCusto: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    JvDBNavigator1: TJvDBNavigator;
    SP_custo: TADOStoredProc;
    QCustoDOC: TBCDField;
    QCustoFILIAL: TBCDField;
    QCustoCLIENTE: TBCDField;
    QCustoRECEITA: TBCDField;
    QCustoCUSTO: TBCDField;
    QCustoSTATUS: TStringField;
    QCustoTRANSP: TBCDField;
    QCustoMAN_ROM: TBCDField;
    QCustoDT_DOC: TDateTimeField;
    QCustoTIP_DOC: TStringField;
    QCustoOPERACAO: TStringField;
    QCustoFILDOC: TBCDField;
    QCustoCOD_TAB: TBCDField;
    QCustoNM_CLIENTE: TStringField;
    QCustoNM_TRANSP: TStringField;
    QCustoNR_NF: TBCDField;
    QCustoCIDADE: TStringField;
    QCustoESTADO: TStringField;
    QCustoLOCAL: TStringField;
    RGC: TRadioGroup;
    RGS: TRadioGroup;
    QCustofatura: TStringField;
    QCustoCTE_TRANSP: TFMTBCDField;
    SP_AUTO: TADOStoredProc;
    QCusto__: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    StringField1: TStringField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    StringField2: TStringField;
    BCDField6: TBCDField;
    StringField3: TStringField;
    BCDField7: TBCDField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    StringField5: TStringField;
    BCDField8: TBCDField;
    BCDField9: TBCDField;
    BCDField10: TBCDField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    FMTBCDField1: TFMTBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_custoxrec: TfrmRel_custoxrec;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_custoxrec.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\RelCusto_Receita' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCusto.First;
  Gauge1.MaxValue := QCusto.RecordCount;
  while not QCusto.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCusto.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCusto.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmRel_custoxrec.btRelatorioClick(Sender: TObject);
var arquivo : String;
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  QCusto.close;

  Panel1.Visible := true;
  Application.ProcessMessages;

  if RGS.ItemIndex = 0 then
  begin
    SP_AUTO.Parameters[0].Value := dtInicial.Text;
    SP_AUTO.Parameters[1].value := dtFinal.Text;
    SP_AUTO.ExecProc;
    //arquivo := SP_AUTO.Parameters[0].value;
    if copy(arquivo,1,4) = 'ERRO' then
    begin
      showmessage(arquivo);
    end
    else
    begin
      showmessage('Arquivo Gerado');
      ShellExecute(Application.Handle, nil, PChar('\\192.168.236.112\Pre_Faturas\'+arquivo), nil, nil,
       SW_SHOWNORMAL);
    end;
  end
  else
  begin
    SP_custo.Parameters[0].value := dtInicial.Text;
    SP_custo.Parameters[1].value := dtFinal.Text;
    if RGC.ItemIndex = 0 then
      SP_custo.Parameters[2].value := 0
    else
      SP_custo.Parameters[2].value := 1;

    if RGS.ItemIndex = 0 then
      SP_custo.Parameters[3].value := 'T'
    else if RGS.ItemIndex = 1 then
      SP_custo.Parameters[3].value := 'P'
    else if RGS.ItemIndex = 2 then
      SP_custo.Parameters[3].value := 'N'
    else if RGS.ItemIndex = 3 then
      SP_custo.Parameters[3].value := 'A'
    else if RGS.ItemIndex = 4 then
      SP_custo.Parameters[3].value := 'B'
    else
      SP_custo.Parameters[3].value := 'G';

    SP_custo.ExecProc;
    QCusto.open;
  end;

 { if RGS.ItemIndex = 0 then
    QCusto.SQL[8] := 'Where status in (''P'',''N'') '
  else if RGS.ItemIndex = 1 then
    QCusto.SQL[8] := 'Where status = ''P'' '
  else if RGS.ItemIndex = 2 then
    QCusto.SQL[8] := 'Where status = ''N'' '
  else if RGS.ItemIndex = 3 then
    QCusto.SQL[8] := 'Where status = ''A'' '
  else if RGS.ItemIndex = 4 then
    QCusto.SQL[8] := 'Where status = ''B'' '
  else
    QCusto.SQL[8] := 'Where status not in (''X'')'; }


  Panel1.Visible := false;
end;

procedure TfrmRel_custoxrec.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_custoxrec.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_custoxrec.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_custoxrec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCusto.close;
end;

procedure TfrmRel_custoxrec.FormCreate(Sender: TObject);
begin
  DriveComboBox1.Enabled := true;
  //QCusto.open;
end;

procedure TfrmRel_custoxrec.JvDBGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCusto.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_custoxrec.JvDBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  QCusto.close;
  if Pos('order by', QCusto.SQL.Text) > 0 then
  begin
    QCusto.SQL.Text := copy(QCusto.SQL.Text, 1, Pos('order by', QCusto.SQL.Text)
      - 1) + 'order by ' + Column.FieldName
  end
  else
    QCusto.SQL.Text := QCusto.SQL.Text + ' order by ' + Column.FieldName;
  QCusto.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
    JvDBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

end.
