unit CadTabelaVenda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, ActnList, JvExStdCtrls,
  JvToolEdit, JvBaseEdits, Mask, JvExMask, JvMaskEdit, Grids, ComObj, ShellApi,
  DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, JvMemo, JvEdit,
  System.Actions, JvDBControls, JvComponentBase, JvgExportComponents,
  Vcl.Samples.Gauges, Vcl.Themes;

type
  TfrmCadTabelaVenda = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    dtstabela: TDataSource;
    Detalhes: TTabSheet;
    dtsItem: TDataSource;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    Label6: TLabel;
    Label2: TLabel;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    CBUFO: TComboBox;
    CBLO: TComboBox;
    cbtra: TComboBox;
    edTrans: TJvMaskEdit;
    Qcadtabela: TADOQuery;
    QItemCOD_TAB: TBCDField;
    QItemNR_PESO_DE: TBCDField;
    QItemNR_PESO_ATE: TBCDField;
    QItemVL_RODOVIARIO: TBCDField;
    QItemDATAI: TDateTimeField;
    QItemDATAF: TDateTimeField;
    QItemVR_CTRC: TBCDField;
    QItemVR_EXCED: TBCDField;
    Label14: TLabel;
    cbUFD: TComboBox;
    cbLd: TComboBox;
    TabSheet1: TTabSheet;
    Label15: TLabel;
    cbPesqTransp: TComboBox;
    Label16: TLabel;
    cbPesqUFO: TComboBox;
    cbPesqLO: TComboBox;
    Label17: TLabel;
    cbPesqUFD: TComboBox;
    cbPesqLD: TComboBox;
    btProcurar: TBitBtn;
    btRetirarFiltro: TBitBtn;
    cbImposto: TCheckBox;
    cbPeso: TCheckBox;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir_: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Label20: TLabel;
    cbVeiculo: TComboBox;
    edVeiculo: TJvMaskEdit;
    btnDuplicar: TBitBtn;
    Label21: TLabel;
    cbCidade: TComboBox;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    Label1: TLabel;
    cbCidadeO: TComboBox;
    Label24: TLabel;
    cbVeiculoPesq: TComboBox;
    cbStatus: TCheckBox;
    Label28: TLabel;
    cbOperacao: TComboBox;
    Label29: TLabel;
    cbOperacaoPesq: TComboBox;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    QTabelaCOD_VENDA: TBCDField;
    QTabelaFL_STATUS: TStringField;
    QTabelaDS_UF: TStringField;
    QTabelaDS_UF_DES: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaVEICULO: TStringField;
    QTabelaOPERACAO: TStringField;
    QTabelaNM_CLIENTE: TStringField;
    QTabelaLOCAL: TStringField;
    QTabelaLOCAL_DES: TStringField;
    QcadtabelaCOD_VENDA: TBCDField;
    QcadtabelaCOD_CLIENTE: TBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TBCDField;
    QcadtabelaUSUARIO: TStringField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QcadtabelaCODMUN: TBCDField;
    QcadtabelaCODMUNO: TBCDField;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TBCDField;
    QcadtabelaVL_TDEP: TBCDField;
    QcadtabelaVL_TDEM: TBCDField;
    QcadtabelaFL_STATUS: TStringField;
    QcadtabelaOPERACAO: TStringField;
    QcadtabelaVL_TDEMAX: TBCDField;
    QItemCOD_VENDA: TBCDField;
    QItemUSUARIO: TStringField;
    QItemDATAINC: TDateTimeField;
    QItemUSUALT: TStringField;
    QItemDATAALT: TDateTimeField;
    QcadtabelaOBS: TStringField;
    Label27: TLabel;
    Label31: TLabel;
    cbRegiao: TComboBox;
    QcadtabelaREGIAO: TStringField;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    edDataI: TJvDateEdit;
    edDataF: TJvDateEdit;
    EdValor: TJvCalcEdit;
    edCtrc: TJvCalcEdit;
    edPesoi: TJvCalcEdit;
    edPesof: TJvCalcEdit;
    edExced: TJvCalcEdit;
    DBGrid1: TJvDBUltimGrid;
    Panel3: TPanel;
    DBNavigator1: TDBNavigator;
    btnInserirI: TBitBtn;
    btnsalvarI: TBitBtn;
    btnAlterarI: TBitBtn;
    btnExcluirI: TBitBtn;
    btnCancelarI: TBitBtn;
    QcadtabelaVL_OUTROS_P: TBCDField;
    QcadtabelaVL_SEG_BALSA: TBCDField;
    QcadtabelaVL_REDEP_FLUVIAL: TBCDField;
    QcadtabelaVL_AGEND: TBCDField;
    QcadtabelaVL_PALLET: TBCDField;
    QcadtabelaVL_TAS: TBCDField;
    QcadtabelaVL_DESPACHO: TBCDField;
    QcadtabelaVL_ENTREGA_PORTO: TBCDField;
    QcadtabelaVL_ALFAND: TBCDField;
    QcadtabelaVL_CANHOTO: TBCDField;
    QcadtabelaVL_FLUVIAL: TBCDField;
    QcadtabelaVL_FLUVIAL_M: TBCDField;
    QcadtabelaDT_INICIAL: TDateTimeField;
    QcadtabelaDT_FINAL: TDateTimeField;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    QcadtabelaVL_DEVOLUCAO: TBCDField;
    QItemVL_MINIMO: TBCDField;
    edValMin: TJvCalcEdit;
    QcadtabelaFL_RATEIO: TStringField;
    cbRateio: TCheckBox;
    QcadtabelaVL_FLUVMIN: TBCDField;
    QcadtabelaVL_AJUDA: TBCDField;
    GroupBox1: TGroupBox;
    QcadtabelaFL_FRETE: TStringField;
    QcadtabelaFL_PEDAGIO: TStringField;
    QcadtabelaFL_EXCEDENTE: TStringField;
    QcadtabelaFL_AD: TStringField;
    QcadtabelaFL_GRIS: TStringField;
    QcadtabelaFL_ENTREGA: TStringField;
    QcadtabelaFL_SEGURO: TStringField;
    QcadtabelaFL_TDE: TStringField;
    QcadtabelaFL_TR: TStringField;
    QcadtabelaFL_SEG_BALSA: TStringField;
    QcadtabelaFL_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_AGEND: TStringField;
    QcadtabelaFL_PALLET: TStringField;
    QcadtabelaFL_TAS: TStringField;
    QcadtabelaFL_DESPACHO: TStringField;
    QcadtabelaFL_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_ALFAND: TStringField;
    QcadtabelaFL_CANHOTO: TStringField;
    QcadtabelaFL_FLUVIAL: TStringField;
    QcadtabelaFL_DEVOLUCAO: TStringField;
    QcadtabelaFL_AJUDA: TStringField;
    QcadtabelaFL_CTE: TStringField;
    CbnfFrete: TCheckBox;
    CbnfPedagio: TCheckBox;
    CbnfExcedente: TCheckBox;
    CbnfGris: TCheckBox;
    CbnfSeguro: TCheckBox;
    CbnfTas: TCheckBox;
    CbnftaxaCte: TCheckBox;
    Label46: TLabel;
    CbnfTde: TCheckBox;
    CbnfTr: TCheckBox;
    CbnfSegBalsa: TCheckBox;
    CbnfRedepFluvial: TCheckBox;
    CbnfDespacho: TCheckBox;
    CbnfEntPorto: TCheckBox;
    CbnfTaxaAlfandega: TCheckBox;
    CbnfTaxaFluvial: TCheckBox;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    QcadtabelaFL_NF_FRETE: TStringField;
    QcadtabelaFL_NF_PEDAGIO: TStringField;
    QcadtabelaFL_NF_EXCEDENTE: TStringField;
    QcadtabelaFL_NF_AD: TStringField;
    QcadtabelaFL_NF_GRIS: TStringField;
    QcadtabelaFL_NF_ENTREGA: TStringField;
    QcadtabelaFL_NF_SEGURO: TStringField;
    QcadtabelaFL_NF_TDE: TStringField;
    QcadtabelaFL_NF_TR: TStringField;
    QcadtabelaFL_NF_SEG_BALSA: TStringField;
    QcadtabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_NF_AGEND: TStringField;
    QcadtabelaFL_NF_PALLET: TStringField;
    QcadtabelaFL_NF_TAS: TStringField;
    QcadtabelaFL_NF_DESPACHO: TStringField;
    QcadtabelaFL_NF_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_NF_ALFAND: TStringField;
    QcadtabelaFL_NF_CANHOTO: TStringField;
    QcadtabelaFL_NF_FLUVIAL: TStringField;
    QcadtabelaFL_NF_DEVOLUCAO: TStringField;
    QcadtabelaFL_NF_AJUDA: TStringField;
    QcadtabelaFL_NF_CTE: TStringField;
    QcadtabelaVL_REENT_MINIMA: TBCDField;
    btnControler: TBitBtn;
    RGFiltro: TRadioGroup;
    Label53: TLabel;
    cbCidOPesq: TComboBox;
    Label54: TLabel;
    cbCidDPesq: TComboBox;
    edUser: TJvEdit;
    edDtIns: TJvDateEdit;
    edUserA: TJvEdit;
    edDtAlt: TJvDateEdit;
    btnDcto: TBitBtn;
    btnVer: TBitBtn;
    OpenDialog: TOpenDialog;
    QcadtabelaDOC_CONTROLER: TStringField;
    Label55: TLabel;
    DBMemo1: TDBMemo;
    JvDBMaskEdit1: TJvDBMaskEdit;
    Panel1: TPanel;
    RBAtivo: TRadioButton;
    RbInativo: TRadioButton;
    Label60: TLabel;
    QcadtabelaREPROVADO: TStringField;
    QcadtabelaCONTROLER_REPR: TStringField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_DEDICADO: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    cbImpostoICMS: TCheckBox;
    QcadtabelaFL_IMPOSTO_ICMS: TStringField;
    QcadtabelaVL_TAXA_TRT: TFloatField;
    QcadtabelaVL_FRETE_VALOR: TFloatField;
    TabSheet3: TTabSheet;
    Panel9: TPanel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    LBLCanhoto: TLabel;
    btnAlterarGen: TBitBtn;
    btpostgen: TBitBtn;
    btcancelgen: TBitBtn;
    edpallet: TJvCalcEdit;
    edAjuda: TJvCalcEdit;
    edReentrega: TJvCalcEdit;
    edArmazem: TJvCalcEdit;
    edHoraParada: TJvCalcEdit;
    edcanhoto: TJvCalcEdit;
    QGenItem: TADOQuery;
    dsGenItem: TDataSource;
    QGenItemCOD_ID: TFMTBCDField;
    QGenItemVL_PERNOITE: TBCDField;
    QGenItemVL_VD: TBCDField;
    QGenItemVL_DIARIA: TBCDField;
    QGenItemVEICULO: TStringField;
    QGenItemCOD_VENDA: TFMTBCDField;
    Label37: TLabel;
    edAgendG: TJvCalcEdit;
    Qcadtabelavl_agendg: TBCDField;
    dtsCadTabela: TDataSource;
    QTabeladt_controler: TDateField;
    RBTodos: TRadioButton;
    Historico: TTabSheet;
    DBGrid2: TDBGrid;
    DBRichEdit1: TDBRichEdit;
    QAudit: TADOQuery;
    QAuditACAO: TStringField;
    QAuditDESCRICAO1: TStringField;
    QAuditMAQUINA: TStringField;
    QAuditUSUARIO: TStringField;
    QAuditDAT_INC: TDateTimeField;
    dtsAudit: TDataSource;
    Panel5: TPanel;
    DBGrid3: TDBGrid;
    DBRichEdit2: TDBRichEdit;
    QAuditI: TADOQuery;
    BCDField1: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    BCDField2: TBCDField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    DateTimeField1: TDateTimeField;
    dtsAuditI: TDataSource;
    Obs: TRichEdit;
    btnExcel: TBitBtn;
    JvgExportExcel1: TJvgExportExcel;
    QExcelTab: TADOQuery;
    JvDBGrid2: TJvDBGrid;
    dtsExcelTab: TDataSource;
    QExcelTabCOD_VENDA: TFMTBCDField;
    QExcelTabNM_CLIENTE: TStringField;
    QExcelTabFL_LOCAL: TStringField;
    QExcelTabDS_UF: TStringField;
    QExcelTabCIDADE_ORIGEM: TStringField;
    QExcelTabFL_LOCAL_DES: TStringField;
    QExcelTabDS_UF_DES: TStringField;
    QExcelTabFL_PESO: TStringField;
    QExcelTabCIDADE_DESTINO: TStringField;
    QExcelTabFL_IMPOSTO: TStringField;
    QExcelTabVEICULO: TStringField;
    QExcelTabFL_IMPOSTO_ICMS: TStringField;
    QExcelTabVL_FRETE_MINIMO: TBCDField;
    QExcelTabVL_PEDAGIO: TBCDField;
    QExcelTabVL_OUTROS: TBCDField;
    QExcelTabVL_OUTROS_P: TFloatField;
    QExcelTabVL_OUTROS_MINIMO: TBCDField;
    QExcelTabVL_EXCEDENTE: TBCDField;
    QExcelTabVL_AD: TFloatField;
    QExcelTabVL_GRIS: TFloatField;
    QExcelTabVL_GRIS_MINIMO: TFloatField;
    QExcelTabSEGURO_MINIMO: TFloatField;
    QExcelTabVL_TDE: TBCDField;
    QExcelTabVL_TR: TFloatField;
    QExcelTabVL_TDEP: TFloatField;
    QExcelTabVL_TDEM: TBCDField;
    QExcelTabFL_STATUS: TStringField;
    QExcelTabOPERACAO: TStringField;
    QExcelTabVL_TDEMAX: TBCDField;
    QExcelTabVL_SEG_BALSA: TFloatField;
    QExcelTabVL_REDEP_FLUVIAL: TFloatField;
    QExcelTabVL_AGEND: TBCDField;
    QExcelTabVL_PALLET: TBCDField;
    QExcelTabVL_TAS: TBCDField;
    QExcelTabVL_DESPACHO: TBCDField;
    QExcelTabVL_ENTREGA_PORTO: TFloatField;
    QExcelTabVL_ALFAND: TFloatField;
    QExcelTabVL_CANHOTO: TBCDField;
    QExcelTabVL_FLUVIAL: TBCDField;
    QExcelTabVL_DEVOLUCAO: TBCDField;
    QExcelTabFL_RATEIO: TStringField;
    QExcelTabVL_FLUVMIN: TBCDField;
    QExcelTabVL_AJUDA: TBCDField;
    QExcelTabVL_REENT_MINIMA: TBCDField;
    QExcelTabVL_ARMAZENAGEM: TBCDField;
    QExcelTabVL_DIARIA: TBCDField;
    QExcelTabVL_REENTREGA: TBCDField;
    QExcelTabVL_FRETE_VALOR: TFloatField;
    QExcelTabVL_TAXA_TRT: TFloatField;
    QExcelTabVL_AGENDG: TBCDField;
    Gauge1: TGauge;
    Label40: TLabel;
    QItemDIARIA: TBCDField;
    edDiariaF: TJvCalcEdit;
    Label41: TLabel;
    edDiaria48: TJvCalcEdit;
    QcadtabelaVL_DIARIA48: TBCDField;
    QcadtabelaVL_DEVOLUCAOP: TBCDField;
    QcadtabelaVL_REENTREGAP: TBCDField;
    QExcelTabVL_REENTREGAP: TBCDField;
    QExcelTabVL_DIARIA48: TBCDField;
    QExcelTabVL_DEVOLUCAOP: TBCDField;
    QItemRee: TADOQuery;
    dtsItemRee: TDataSource;
    QItemReeCOD_TAB: TFMTBCDField;
    QItemReeCOD_VENDA: TFMTBCDField;
    QItemReeNR_PESO_DE: TBCDField;
    QItemReeNR_PESO_ATE: TBCDField;
    QItemReeVL_FRETE: TBCDField;
    QItemReeDATAI: TDateTimeField;
    QItemReeDATAF: TDateTimeField;
    QItemReeVR_CTE: TBCDField;
    QItemReeVR_EXCED: TBCDField;
    QItemReeUSUARIO: TStringField;
    QItemReeDATAINC: TDateTimeField;
    QItemReeUSUALT: TStringField;
    QItemReeDATAALT: TDateTimeField;
    QItemReeVL_MINIMO: TBCDField;
    PageControl2: TPageControl;
    tsVeiculo: TTabSheet;
    tsReentrega: TTabSheet;
    Panel8: TPanel;
    Label65: TLabel;
    LBLDedicado: TLabel;
    Lbldiaria: TLabel;
    LBLPernoite: TLabel;
    eddedicadoGen: TJvCalcEdit;
    eddiariaGen: TJvCalcEdit;
    edpernoiteGen: TJvCalcEdit;
    cbveiculogen: TComboBox;
    JvDBGrid1: TJvDBGrid;
    Panel6: TPanel;
    DBNavigator2: TDBNavigator;
    btinserirgen: TBitBtn;
    btsalvargen: TBitBtn;
    btalterargen: TBitBtn;
    btexcluirgen: TBitBtn;
    btcancelagen: TBitBtn;
    JvDBUltimGrid1: TJvDBUltimGrid;
    Panel7: TPanel;
    edDataReeI: TJvDateEdit;
    eddataFRee: TJvDateEdit;
    edFreteRee: TJvCalcEdit;
    edCte: TJvCalcEdit;
    edPesoiRee: TJvCalcEdit;
    edPesofRee: TJvCalcEdit;
    edPesoExcRee: TJvCalcEdit;
    edVMinimoRee: TJvCalcEdit;
    Panel10: TPanel;
    DBNavigator3: TDBNavigator;
    btnInserirRee: TBitBtn;
    btnSalvarRee: TBitBtn;
    btnAlterarRee: TBitBtn;
    btnExcluirRee: TBitBtn;
    btnCancelarRee: TBitBtn;
    QcadtabelaVL_6X1: TBCDField;
    QcadtabelaQT6X1: TBCDField;
    QGenItemVL_PALETE: TBCDField;
    edPaleteGen: TJvCalcEdit;
    Label56: TLabel;
    tsAereo: TTabSheet;
    Panel11: TPanel;
    edDataAereoI: TJvDateEdit;
    edDataAereoF: TJvDateEdit;
    edAereoEntrega: TJvCalcEdit;
    edPesoAereoI: TJvCalcEdit;
    edPesoAereoF: TJvCalcEdit;
    edAereoExc: TJvCalcEdit;
    edAereoColeta: TJvCalcEdit;
    JvDBUltimGrid2: TJvDBUltimGrid;
    Panel12: TPanel;
    DBNavigator4: TDBNavigator;
    btnAereoI: TBitBtn;
    btnAereoS: TBitBtn;
    btnAereoA: TBitBtn;
    btnAereoE: TBitBtn;
    btnAereoC: TBitBtn;
    QAereo: TADOQuery;
    dtsAereo: TDataSource;
    QAereoCOD_TAB: TFMTBCDField;
    QAereoCOD_VENDA: TFMTBCDField;
    QAereoPESO_DE: TBCDField;
    QAereoPESO_ATE: TBCDField;
    QAereoVALOR_COLETA: TBCDField;
    QAereoVALOR_ENTREGA: TBCDField;
    QAereoDATAI: TDateTimeField;
    QAereoDATAF: TDateTimeField;
    QAereoVR_EXCED: TBCDField;
    QAereoUSUARIO: TStringField;
    QAereoDATAINC: TDateTimeField;
    QAereoUSUALT: TStringField;
    QAereoDATAALT: TDateTimeField;
    QTabelafl_aereo: TStringField;
    QTabelaFL_KM: TStringField;
    QcadtabelaVL_ARMAZENAGEM: TFMTBCDField;
    Label52: TLabel;
    edRMinimo: TJvCalcEdit;
    QExcelTabVL_6X1: TBCDField;
    QExcelTabQT6X1: TFMTBCDField;
    QcadtabelaVL_TDA: TBCDField;
    QcadtabelaVL_TDAP: TBCDField;
    QcadtabelaVL_TDAmin: TBCDField;
    QcadtabelaVL_TRTMIN: TBCDField;
    Label63: TLabel;
    Label71: TLabel;
    cbCte: TCheckBox;
    cbfluvial: TCheckBox;
    cbtas: TCheckBox;
    cbseguro: TCheckBox;
    cbgris: TCheckBox;
    cbexcedente: TCheckBox;
    cbpedagio: TCheckBox;
    cbfrete: TCheckBox;
    cbdespacho: TCheckBox;
    cbentport: TCheckBox;
    cbalfand: TCheckBox;
    cbredepFluvial: TCheckBox;
    cbsegBalsa: TCheckBox;
    cbtr: TCheckBox;
    cbtde: TCheckBox;
    PanelValores: TPanel;
    Label4: TLabel;
    edFreteM: TJvCalcEdit;
    Label22: TLabel;
    edTDE: TJvCalcEdit;
    Label35: TLabel;
    edred_fluv: TJvCalcEdit;
    Label64: TLabel;
    edtaxatrt: TJvCalcEdit;
    edTRTMin: TJvCalcEdit;
    Label43: TLabel;
    edfretepeso: TJvCalcEdit;
    Label62: TLabel;
    edFluvMin: TJvCalcEdit;
    Label44: TLabel;
    edTDEp: TJvCalcEdit;
    Label25: TLabel;
    edPedagio: TJvCalcEdit;
    Label5: TLabel;
    Label7: TLabel;
    edOutros: TJvCalcEdit;
    Label26: TLabel;
    edTDEm: TJvCalcEdit;
    Label36: TLabel;
    edagend: TJvCalcEdit;
    edentrega: TJvCalcEdit;
    Label3: TLabel;
    edTdeMax: TJvCalcEdit;
    Label30: TLabel;
    edOutrosP: TJvCalcEdit;
    Label33: TLabel;
    Label32: TLabel;
    edOutrosM: TJvCalcEdit;
    Label8: TLabel;
    ed_fluvial: TJvCalcEdit;
    Label9: TLabel;
    ed_fluv_min: TJvCalcEdit;
    Label10: TLabel;
    edExc: TJvCalcEdit;
    Label23: TLabel;
    edTR: TJvCalcEdit;
    Label18: TLabel;
    eddespacho: TJvCalcEdit;
    Label38: TLabel;
    edtas: TJvCalcEdit;
    Label11: TLabel;
    EdSeg: TJvCalcEdit;
    Label19: TLabel;
    edsegminimo: TJvCalcEdit;
    Label12: TLabel;
    EdGris: TJvCalcEdit;
    Label13: TLabel;
    EdGrisM: TJvCalcEdit;
    Label34: TLabel;
    edseg_balsa: TJvCalcEdit;
    Label59: TLabel;
    edTDAmin: TJvCalcEdit;
    edTDAp: TJvCalcEdit;
    Label58: TLabel;
    Label57: TLabel;
    edTDA: TJvCalcEdit;
    edDevolucao: TJvCalcEdit;
    Label61: TLabel;
    Label39: TLabel;
    edalfand: TJvCalcEdit;
    Label42: TLabel;
    ed6x1: TJvCalcEdit;
    edqt6x1: TJvCalcEdit;
    Label45: TLabel;
    PanelFiltro: TPanel;
    cbCliente: TComboBox;
    cbUFOF: TComboBox;
    cbUFDF: TComboBox;
    cbVeiculoF: TComboBox;
    cbOperacaoF: TComboBox;

    procedure btnFecharClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure btnExcluir_Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure Restaura;
    procedure RestauraI;
    procedure RestauraGen;
    procedure RestauraAereo;
    procedure btnInserirIClick(Sender: TObject);
    procedure btnsalvarIClick(Sender: TObject);
    procedure btnAlterarIClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure Limpadados;
    procedure LimpadadosGen;
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure btnExcluirIClick(Sender: TObject);
    procedure btnCancelarIClick(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    procedure btRetirarFiltroClick(Sender: TObject);
    procedure edExcedExit(Sender: TObject);
    procedure btnDuplicarClick(Sender: TObject);
    procedure cbUFDChange(Sender: TObject);
    procedure CBUFOChange(Sender: TObject);
    procedure edTDEChange(Sender: TObject);
    procedure edTDEpChange(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure PageControl1Change(Sender: TObject);
    procedure edOutrosChange(Sender: TObject);
    procedure edOutrosPChange(Sender: TObject);
    procedure cbfreteClick(Sender: TObject);
    procedure CbnfFreteClick(Sender: TObject);
    procedure cbpedagioClick(Sender: TObject);
    procedure CbnfPedagioClick(Sender: TObject);
    procedure cbexcedenteClick(Sender: TObject);
    procedure cbgrisClick(Sender: TObject);
    procedure cbseguroClick(Sender: TObject);
    procedure cbtasClick(Sender: TObject);
    procedure cbCteClick(Sender: TObject);
    procedure cbtdeClick(Sender: TObject);
    procedure cbtrClick(Sender: TObject);
    procedure cbsegBalsaClick(Sender: TObject);
    procedure cbredepFluvialClick(Sender: TObject);
    procedure cbdespachoClick(Sender: TObject);
    procedure cbentportClick(Sender: TObject);
    procedure cbalfandClick(Sender: TObject);
    procedure cbfluvialClick(Sender: TObject);
    procedure CbnfExcedenteClick(Sender: TObject);
    procedure CbnfGrisClick(Sender: TObject);
    procedure CbnfSeguroClick(Sender: TObject);
    procedure CbnfTasClick(Sender: TObject);
    procedure CbnftaxaCteClick(Sender: TObject);
    procedure CbnfTdeClick(Sender: TObject);
    procedure CbnfTrClick(Sender: TObject);
    procedure CbnfSegBalsaClick(Sender: TObject);
    procedure CbnfRedepFluvialClick(Sender: TObject);
    procedure CbnfDespachoClick(Sender: TObject);
    procedure CbnfEntPortoClick(Sender: TObject);
    procedure CbnfTaxaAlfandegaClick(Sender: TObject);
    procedure CbnfTaxaFluvialClick(Sender: TObject);
    procedure btnControlerClick(Sender: TObject);
    procedure cbPesqLOChange(Sender: TObject);
    procedure cbPesqLDChange(Sender: TObject);
    procedure btnDctoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure CBLOChange(Sender: TObject);
    procedure cbLdChange(Sender: TObject);
    procedure RbInativoClick(Sender: TObject);
    procedure RBAtivoClick(Sender: TObject);
    procedure QTabelaAfterOpen(DataSet: TDataSet);
    procedure btinserirgenClick(Sender: TObject);
    procedure btsalvargenClick(Sender: TObject);
    procedure btalterargenClick(Sender: TObject);
    procedure btexcluirgenClick(Sender: TObject);
    procedure btcancelagenClick(Sender: TObject);
    procedure btnAlterarGenClick(Sender: TObject);
    procedure btpostgenClick(Sender: TObject);
    procedure btcancelgenClick(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edDataFExit(Sender: TObject);
    procedure QItemAfterScroll(DataSet: TDataSet);
    procedure btnExcelClick(Sender: TObject);
    procedure btnInserirReeClick(Sender: TObject);
    procedure btnAlterarReeClick(Sender: TObject);
    procedure RestauraIRee;
    procedure btnCancelarReeClick(Sender: TObject);
    procedure btnSalvarReeClick(Sender: TObject);
    procedure btnExcluirReeClick(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edPesoExcReeExit(Sender: TObject);
    procedure edReentregaExit(Sender: TObject);
    procedure btnAereoIClick(Sender: TObject);
    procedure btnAereoAClick(Sender: TObject);
    procedure btnAereoEClick(Sender: TObject);
    procedure btnAereoCClick(Sender: TObject);
    procedure btnAereoSClick(Sender: TObject);
    procedure tsAereoShow(Sender: TObject);
    procedure tsAereoHide(Sender: TObject);
    procedure JvDBUltimGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edDataAereoFExit(Sender: TObject);
    procedure edPesofExit(Sender: TObject);
    procedure edPesoAereoFExit(Sender: TObject);
    procedure edTDAChange(Sender: TObject);
    procedure edTDApChange(Sender: TObject);
    procedure filtrar;
    procedure cbClienteChange(Sender: TObject);
    procedure cbUFOFChange(Sender: TObject);
    procedure cbUFDFChange(Sender: TObject);
    procedure cbVeiculoFChange(Sender: TObject);
    procedure cbOperacaoFChange(Sender: TObject);
  private
    quant, filtro, spot: Integer;
    tipo, sql, status, fdefaultStyleName: string;
  public
    { Public declarations }
  end;

var
  frmCadTabelaVenda: TfrmCadTabelaVenda;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadTabelaVenda.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTabelaVenda.btnNovoClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  Restaura;
  Limpadados;
  tipo := 'I';
  PageControl1.ActivePage := Detalhes;
  cbStatus.Checked := true;
  cbtra.SetFocus;
end;

procedure TfrmCadTabelaVenda.btnSalvarClick(Sender: TObject);
var
  cod, forn: Integer;
begin
  cod := 0;

  if alltrim(cbOperacao.Text) = '' then
  begin
    ShowMessage('A Opera��o n�o foi escolhida !');
    cbOperacao.SetFocus;
    Exit;
  end;

  if alltrim(cbtra.Text) = '' then
  begin
    ShowMessage('O Cliente n�o foi escolhido !');
    cbtra.SetFocus;
    Exit;
  end;

  if UpperCase(cbOperacao.Text) <> 'SPOT' then
  begin
    if not dtmDados.PodeInserir(name) then
    begin
      ShowMessage('Voc� N�o Tem Permiss�o Para Cadastrar este tipo de Opera��o');
      btnCancelarClick(Sender);
      exit;
    end;
  end;


  if Qcadtabela.Active = false then
  begin
    QTabela.Open;
    Qtabela.Last;
  end;

  forn := 0;
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select codclifor from cyber.rodcli where nomeab || '' - '' || codcgc = ' +
    #39 + cbtra.Text + #39);
  dtmDados.iQuery1.Open;
  forn := dtmDados.iQuery1.FieldByName('codclifor').value;

  if tipo = 'I' then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('select * from tb_tabelavenda f where fl_status = ''S'' ');
    dtmDados.iQuery1.sql.add('and cod_cliente = :0 and fl_local = :1 ');
    dtmDados.iQuery1.sql.add('and ds_uf = :2 and f.fl_local_des = :3 ');
    dtmDados.iQuery1.sql.add('and f.ds_uf_des = :4');
    dtmDados.iQuery1.sql.add('and nvl(f.veiculo,'' '') = :5');
    dtmDados.iQuery1.sql.add('and f.codmun = :6');
    dtmDados.iQuery1.sql.add('and f.codmunO = :7');
    dtmDados.iQuery1.sql.add('and f.operacao = :8');
    dtmDados.iQuery1.sql.add('and nvl(f.regiao,'' '') = :9');
    dtmDados.iQuery1.Parameters[0].value := forn;
    dtmDados.iQuery1.Parameters[1].value := copy(CBLO.Text, 1, 1);
    dtmDados.iQuery1.Parameters[2].value := CBUFO.Text;
    dtmDados.iQuery1.Parameters[3].value := copy(cbLd.Text, 1, 1);
    dtmDados.iQuery1.Parameters[4].value := cbUFD.Text;
    if cbVeiculo.Text <> '' then
      dtmDados.iQuery1.Parameters[5].value := cbVeiculo.Text
    else
      dtmDados.iQuery1.Parameters[5].value := '';

    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        dtmDados.iQuery1.Parameters[6].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[6].value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        dtmDados.iQuery1.Parameters[7].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[7].value := 0;
    dtmDados.iQuery1.Parameters[8].value := cbOperacao.Text;
    if cbRegiao.Text <> '' then
      dtmDados.iQuery1.Parameters[9].value := cbRegiao.Text
    else
      dtmDados.iQuery1.Parameters[9].value := ' ';
    dtmDados.iQuery1.Open;
    if not dtmDados.iQuery1.FieldByName('cod_venda').IsNull then
    begin
      ShowMessage
        ('Este Destino j� est� cadastrado para esta Cliente e Percurso !!');
      btnCancelarClick(Sender);
      QTabela.Locate('cod_venda', dtmDados.iQuery1.FieldByName('cod_venda')
        .value, []);
      dtmDados.iQuery1.Close;
    end;
  end
  else
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('select * from tb_tabelavenda f where fl_status = ''S'' ');
    dtmDados.iQuery1.sql.add('and cod_cliente = :0 and fl_local = :1 ');
    dtmDados.iQuery1.sql.add('and ds_uf = :2 and f.fl_local_des = :3 ');
    dtmDados.iQuery1.sql.add('and f.ds_uf_des = :4');
    dtmDados.iQuery1.sql.add('and nvl(f.veiculo,'' '') = :5');
    dtmDados.iQuery1.sql.add('and nvl(f.codmun,0) = :6');
    dtmDados.iQuery1.sql.add('and nvl(f.codmunO,0) = :7');
    dtmDados.iQuery1.sql.add('and f.operacao = :8');
    dtmDados.iQuery1.sql.add('and f.cod_venda <> :9');
    dtmDados.iQuery1.sql.add('and nvl(f.regiao,'' '') = :10');
    dtmDados.iQuery1.Parameters[0].value := forn;
    dtmDados.iQuery1.Parameters[1].value := copy(CBLO.Text, 1, 1);
    dtmDados.iQuery1.Parameters[2].value := CBUFO.Text;
    dtmDados.iQuery1.Parameters[3].value := copy(cbLd.Text, 1, 1);
    dtmDados.iQuery1.Parameters[4].value := cbUFD.Text;
    if cbVeiculo.Text <> '' then
      dtmDados.iQuery1.Parameters[5].value := cbVeiculo.Text
    else
      dtmDados.iQuery1.Parameters[5].value := ' ';
    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        dtmDados.iQuery1.Parameters[6].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[6].value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        dtmDados.iQuery1.Parameters[7].value := QCidadeCODMUN.AsInteger;
    end
    else
      dtmDados.iQuery1.Parameters[7].value := 0;
    dtmDados.iQuery1.Parameters[8].value := cbOperacao.Text;
    dtmDados.iQuery1.Parameters[9].value := QTabelaCOD_VENDA.AsInteger;
    if cbRegiao.Text <> '' then
      dtmDados.iQuery1.Parameters[10].value := cbRegiao.Text
    else
      dtmDados.iQuery1.Parameters[10].value := ' ';
    dtmDados.iQuery1.Open;
    if not dtmDados.iQuery1.FieldByName('cod_venda').IsNull then
    begin
      ShowMessage
        ('Este Destino j� est� cadastrado para esta Cliente e Percurso !!');
      btnCancelarClick(Sender);
      QTabela.Locate('cod_venda', dtmDados.iQuery1.FieldByName('cod_venda')
        .value, []);
      dtmDados.iQuery1.Close;
    end;
  end;

  try
    if tipo = 'I' then
    begin
      Qcadtabela.append;
      cbStatus.Checked := true;
    end
    else
    begin
      cod := QTabela.RecNo;
      Qcadtabela.edit;
    end;
    edTrans.Text := cbtra.Text;
    edVeiculo.Text := cbVeiculo.Text;
    QcadtabelaCOD_CLIENTE.value := forn;
    QcadtabelaFL_LOCAL.value := CBLO.Text;
    QcadtabelaDS_UF.value := CBUFO.Text;
    QcadtabelaFL_LOCAL_DES.value := cbLd.Text;
    QcadtabelaDS_UF_DES.value := cbUFD.Text;
    QcadtabelaVL_FRETE_MINIMO.value := edFreteM.value;
    QcadtabelaVL_PEDAGIO.value := edPedagio.value;
    QcadtabelaVL_OUTROS.value := edOutros.value;
    QcadtabelaVL_OUTROS_P.value := edOutrosP.value; // fluvial
    QcadtabelaVL_OUTROS_MINIMO.value := edOutrosM.value; // fluvial minimo
    QcadtabelaVL_EXCEDENTE.value := edExc.value;
    QcadtabelaVL_AD.value := EdSeg.value;
    QcadtabelaVL_GRIS.value := EdGris.value;
    QcadtabelaVL_GRIS_MINIMO.value := EdGrisM.value;
    QcadtabelaVL_TDE.value := edTDE.value;
    QcadtabelaVL_TDEP.value := edTDEp.value;
    QcadtabelaVL_TDEM.value := edTDEm.value;
    QcadtabelaVL_TR.value := edTR.value;
    QcadtabelaVL_TDEMAX.value := edTdeMax.value;
    QcadtabelaUSUARIO.value := GLBUSER;
    QcadtabelaMODAL.value := '';
    QcadtabelaVEICULO.value := cbVeiculo.Text;
    QcadtabelaVL_TDA.value := edTDA.value;
    QcadtabelaVL_TDAP.value := edTDAp.value;
    QcadtabelaVL_TDAMin.value := edTDAmin.value;
    // Implementa��o do projeto de generalidades
    QcadtabelaVL_ARMAZENAGEM.value := edArmazem.value;
    //QcadtabelaVL_DIARIA.value := edDiaria.value;
    QcadtabelaVL_REENTREGA.value := edReentrega.value;
    //QcadtabelaVL_DEDICADO.value := edDedicado.value;
    //QcadtabelaVL_PERNOITE.value := edPernoite.value;
    QcadtabelaVL_HORA_PARADA.value := edHoraParada.value;
    QcadtabelaVL_FRETE_VALOR.value := edfretepeso.value;
    QcadtabelaVL_TAXA_TRT.value := edtaxatrt.value;

    if cbCidade.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbUFD.Text;
      QCidade.Open;
      if QCidade.Locate('descri', cbCidade.Text, []) then
        QcadtabelaCODMUN.value := QCidadeCODMUN.AsInteger;
    end
    else
      QcadtabelaCODMUN.value := 0;

    if cbCidadeO.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := CBUFO.Text;
      QCidade.Open;
      if QCidade.Locate('descri', cbCidadeO.Text, []) then
        QcadtabelaCODMUNO.value := QCidadeCODMUN.AsInteger;
    end
    else
      QcadtabelaCODMUNO.value := 0;

    QcadtabelaDT_CADASTRO.value := date;
    QcadtabelaSITE.value := GLBFilial;
    QcadtabelaSEGURO_MINIMO.value := edsegminimo.value;
    QcadtabelaVL_DESPACHO.value := eddespacho.value;

    QcadtabelaVL_FLUVIAL.value := ed_fluvial.value;
    QcadtabelaVL_FLUVIAL_M.value := ed_fluv_min.value;
    QcadtabelaVL_SEG_BALSA.value := edseg_balsa.value;
    QcadtabelaVL_REDEP_FLUVIAL.value := edred_fluv.value;
    QcadtabelaVL_AGEND.value := edagend.value;
    QcadtabelaVL_PALLET.value := edpallet.value;
    QcadtabelaVL_TAS.value := edtas.value;
    QcadtabelaVL_ENTREGA_PORTO.value := edentrega.value;
    QcadtabelaVL_ALFAND.value := edalfand.value;
    QcadtabelaVL_CANHOTO.value := edcanhoto.value;
    QcadtabelaVL_TRTMIN.value := edtrtmin.value;
    QcadtabelaVL_FLUVMIN.value := edFluvMin.value;
    QcadtabelaVL_AJUDA.value := edAjuda.value;

    if cbImposto.Checked = true then
      QcadtabelaFL_IMPOSTO.value := 'S'
    else
      QcadtabelaFL_IMPOSTO.value := 'N';

    if cbImpostoICMS.Checked = true then
      QcadtabelaFL_IMPOSTO_ICMS.value := 'S'
    else
      QcadtabelaFL_IMPOSTO_ICMS.value := 'N';

    if cbPeso.Checked = true then
      QcadtabelaFL_PESO.value := 'S'
    else
      QcadtabelaFL_PESO.value := 'N';

    if cbStatus.Checked = true then
      QcadtabelaFL_STATUS.value := 'S'
    else
      QcadtabelaFL_STATUS.value := 'N';

    if cbRateio.Checked = true then
      QcadtabelaFL_RATEIO.value := 'S'
    else
      QcadtabelaFL_RATEIO.value := 'N';

    if cbfrete.Checked = true then
      QcadtabelaFL_FRETE.value := 'S'
    else
      QcadtabelaFL_FRETE.value := 'N';

    if cbpedagio.Checked = true then
      QcadtabelaFL_PEDAGIO.value := 'S'
    else
      QcadtabelaFL_PEDAGIO.value := 'N';

    if cbexcedente.Checked = true then
      QcadtabelaFL_EXCEDENTE.value := 'S'
    else
      QcadtabelaFL_EXCEDENTE.value := 'N';

    { if cbad.Checked = true then
      QcadtabelaFL_AD.value := 'S'
      else
      QcadtabelaFL_AD.value := 'N'; }

    if cbgris.Checked = true then
      QcadtabelaFL_GRIS.value := 'S'
    else
      QcadtabelaFL_GRIS.value := 'N';

    { if cbentrega.Checked = true then
      QcadtabelaFL_ENTREGA.value := 'S'
      else
      QcadtabelaFL_ENTREGA.value := 'N'; }

    if cbseguro.Checked = true then
      QcadtabelaFL_SEGURO.value := 'S'
    else
      QcadtabelaFL_SEGURO.value := 'N';

    if cbtde.Checked = true then
      QcadtabelaFL_TDE.value := 'S'
    else
      QcadtabelaFL_TDE.value := 'N';

    if cbtr.Checked = true then
      QcadtabelaFL_TR.value := 'S'
    else
      QcadtabelaFL_TR.value := 'N';

    if cbsegBalsa.Checked = true then
      QcadtabelaFL_SEG_BALSA.value := 'S'
    else
      QcadtabelaFL_SEG_BALSA.value := 'N';

    if cbredepFluvial.Checked = true then
      QcadtabelaFL_REDEP_FLUVIAL.value := 'S'
    else
      QcadtabelaFL_REDEP_FLUVIAL.value := 'N';

    if cbtas.Checked = true then
      QcadtabelaFL_TAS.value := 'S'
    else
      QcadtabelaFL_TAS.value := 'N';

    if cbdespacho.Checked = true then
      QcadtabelaFL_DESPACHO.value := 'S'
    else
      QcadtabelaFL_DESPACHO.value := 'N';

    if cbentport.Checked = true then
      QcadtabelaFL_ENTREGA_PORTO.value := 'S'
    else
      QcadtabelaFL_ENTREGA_PORTO.value := 'N';

    if cbalfand.Checked = true then
      QcadtabelaFL_ALFAND.value := 'S'
    else
      QcadtabelaFL_ALFAND.value := 'N';

    if cbfluvial.Checked = true then
      QcadtabelaFL_FLUVIAL.value := 'S'
    else
      QcadtabelaFL_FLUVIAL.value := 'N';

    if cbCte.Checked = true then
      QcadtabelaFL_CTE.value := 'S'
    else
      QcadtabelaFL_CTE.value := 'N';

    if CbnfFrete.Checked = true then
      QcadtabelaFL_NF_FRETE.value := 'S'
    else
      QcadtabelaFL_NF_FRETE.value := 'N';

    if CbnfPedagio.Checked = true then
      QcadtabelaFL_NF_PEDAGIO.value := 'S'
    else
      QcadtabelaFL_NF_PEDAGIO.value := 'N';

    if CbnfExcedente.Checked = true then
      QcadtabelaFL_NF_EXCEDENTE.value := 'S'
    else
      QcadtabelaFL_NF_EXCEDENTE.value := 'N';

    if CbnfGris.Checked = true then
      QcadtabelaFL_NF_GRIS.value := 'S'
    else
      QcadtabelaFL_NF_GRIS.value := 'N';

    if CbnfSeguro.Checked = true then
      QcadtabelaFL_NF_SEGURO.value := 'S'
    else
      QcadtabelaFL_NF_SEGURO.value := 'N';

    if CbnfTas.Checked = true then
      QcadtabelaFL_NF_TAS.value := 'S'
    else
      QcadtabelaFL_NF_TAS.value := 'N';

    if CbnftaxaCte.Checked = true then
      QcadtabelaFL_NF_CTE.value := 'S'
    else
      QcadtabelaFL_NF_CTE.value := 'N';

    if CbnfTde.Checked = true then
      QcadtabelaFL_NF_TDE.value := 'S'
    else
      QcadtabelaFL_NF_TDE.value := 'N';

    if CbnfTr.Checked = true then
      QcadtabelaFL_NF_TR.value := 'S'
    else
      QcadtabelaFL_NF_TR.value := 'N';

    if CbnfSegBalsa.Checked = true then
      QcadtabelaFL_NF_SEG_BALSA.value := 'S'
    else
      QcadtabelaFL_NF_SEG_BALSA.value := 'N';

    if CbnfRedepFluvial.Checked = true then
      QcadtabelaFL_NF_REDEP_FLUVIAL.value := 'S'
    else
      QcadtabelaFL_NF_REDEP_FLUVIAL.value := 'N';

    if CbnfDespacho.Checked = true then
      QcadtabelaFL_NF_DESPACHO.value := 'S'
    else
      QcadtabelaFL_NF_DESPACHO.value := 'N';

    if CbnfEntPorto.Checked = true then
      QcadtabelaFL_NF_ENTREGA_PORTO.value := 'S'
    else
      QcadtabelaFL_NF_ENTREGA_PORTO.value := 'N';

    if CbnfTaxaAlfandega.Checked = true then
      QcadtabelaFL_NF_ALFAND.value := 'S'
    else
      QcadtabelaFL_NF_ALFAND.value := 'N';

    if CbnfTaxaFluvial.Checked = true then
      QcadtabelaFL_NF_FLUVIAL.value := 'S'
    else
      QcadtabelaFL_NF_FLUVIAL.value := 'N';


    QcadtabelaOPERACAO.value := cbOperacao.Text;
    QcadtabelaOBS.value := Obs.Text;
    QcadtabelaREGIAO.value := cbRegiao.Text;
    QcadtabelaVL_REENT_MINIMA.value := edRMinimo.value;
    Qcadtabelavl_6x1.value := ed6x1.Value;
    Qcadtabelaqt6x1.AsInteger := edqt6x1.AsInteger;

    dtmDados.iQuery1.Close;
    Qcadtabela.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Qcadtabela.Close;
  QTabela.Close;
  QTabela.Open;
  Qcadtabela.Open;
  if tipo = 'A' then
    QTabela.RecNo := cod
  else
    QTabela.Last;
  Restaura;
  QItem.Close;
  QItem.Open;
  tipo := '';
end;

procedure TfrmCadTabelaVenda.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  PageControl1.ActivePage := Consulta;
  tipo := '';
end;

procedure TfrmCadTabelaVenda.btnCancelarIClick(Sender: TObject);
begin
  QItem.Cancel;
  RestauraI;
end;

procedure TfrmCadTabelaVenda.btnCancelarReeClick(Sender: TObject);
begin
  QItemRee.Cancel;
  RestauraIRee;
end;

procedure TfrmCadTabelaVenda.btnControlerClick(Sender: TObject);
var retorno : Integer;
begin
  if not dtmDados.PodeAlterar('frmCadTabelaControlerVenda') then
    Exit;
  if Application.Messagebox('Liberar esta Tabela Para Altera��o ?',
    'Liberar esta Tabela Para Altera��o', MB_YESNO + MB_ICONQUESTION) = IDYES
  then
  begin
    // retirado conforme solicita��o da Veronica em 27/12 e concordado com Debora
    {if filtro = 1 then
    begin
      if (Application.Messagebox('Liberar Todos da Tela ?',
        'Liberar esta Tabela Para Altera��o - Total', MB_YESNO + MB_ICONQUESTION)
        = IDYES) then
      begin
        while not QTabela.Eof do
        begin
          dtmDados.iQuery1.Close;
          dtmDados.iQuery1.sql.clear;
          dtmDados.iQuery1.sql.add
            ('update tb_tabelavenda set controler = null, dt_controler = null, ');
          dtmDados.iQuery1.sql.add
            ('dt_cadastro = sysdate, usuario = :0 where cod_venda = :1 ');
          dtmDados.iQuery1.Parameters[0].value := GLBUSER;
          dtmDados.iQuery1.Parameters[1].value := QTabelaCOD_VENDA.AsInteger;
          dtmDados.iQuery1.ExecSQL;
          dtmDados.iQuery1.Close;
          QTabela.Next;
        end;
      end;
    end
    else
    begin }
    Retorno := QTabelaCOD_VENDA.AsInteger;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('update tb_tabelavenda set controler = null, dt_controler = null, ');
      dtmDados.iQuery1.sql.add
        ('dt_cadastro = sysdate, usuario = :0 where cod_venda = :1 ');
      dtmDados.iQuery1.Parameters[0].value := GLBUSER;
      dtmDados.iQuery1.Parameters[1].value := QTabelaCOD_VENDA.AsInteger;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;
    //end;
    QTabela.Close;
    QTabela.Open;
    QTabela.Locate('cod_venda', Retorno, [loPartialKey])
  end;
end;

procedure TfrmCadTabelaVenda.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo, altera: string;
begin
  caminho := GLBControler;
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      arquivo := caminho + 'R_' + QcadtabelaCOD_VENDA.AsString +
        ExtractFileName(OpenDialog.FileName);
      // verifica se existe este arquivo j� gravado, sim renomeia
      if FileExists(arquivo) then
      begin
        altera := copy(arquivo, 1, length(arquivo) - 4) + 'A' +
          apcarac(DateToStr(now())) + '.pdf';
        RenameFile(arquivo, altera);
      end;
      CopyFile(Pchar(ori), Pchar(des), true);

      RenameFile(des, arquivo);

      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('update tb_tabelavenda set doc_controler = :c where cod_venda = :n');
      dtmDados.iQuery1.Parameters[0].value := 'R_' +
        QcadtabelaCOD_VENDA.AsString + ExtractFileName(OpenDialog.FileName);
      dtmDados.iQuery1.Parameters[1].value := QcadtabelaCOD_VENDA.AsInteger;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadTabelaVenda.btnDuplicarClick(Sender: TObject);
var
  cod, new: Integer;
begin
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit
    else
    // Pode cadastrar SPOT mas a tabela que vai duplicar n�o � SPOT
    begin
      if QTabelaOPERACAO.Value <> 'SPOT' then
        Exit;
    end;

  end;
  if Application.Messagebox('Voc� Deseja Duplicar Esta Tabela?',
    'Duplicar Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      cod := QTabelaCOD_VENDA.AsInteger;

      // QTabela.Close;
      // QTabela.SQL.Text := Copy(QTabela.SQL.Text, 1, Pos('order by', QTabela.SQL.Text) - 1) + ' order by cod_venda';
      // QTabela.Open;

      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('select * from tb_tabelavenda where cod_venda = :0');
      dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
      dtmDados.iQuery1.Open;

      QTabela.Locate('cod_venda', cod, []);
      Qcadtabela.append;
      QcadtabelaCOD_CLIENTE.value := dtmDados.iQuery1.FieldByName('COD_cliente')
        .AsInteger;
      QcadtabelaFL_LOCAL.value := dtmDados.iQuery1.FieldByName
        ('FL_LOCAL_DES').value;
      QcadtabelaDS_UF.value := QTabelaDS_UF_DES.value;
      QcadtabelaFL_LOCAL_DES.value := dtmDados.iQuery1.FieldByName
        ('FL_LOCAL').value;
      QcadtabelaDS_UF_DES.value := QTabelaDS_UF.value;
      QcadtabelaVL_FRETE_MINIMO.value := QTabelaVL_FRETE_MINIMO.value;
      QcadtabelaVL_PEDAGIO.value := dtmDados.iQuery1.FieldByName
        ('vl_pedagio').value;
      QcadtabelaVL_OUTROS.value := dtmDados.iQuery1.FieldByName
        ('vl_outros').value;
      QcadtabelaVL_OUTROS_P.value := dtmDados.iQuery1.FieldByName
        ('vl_outros_p').value;
      QcadtabelaVL_OUTROS_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('vl_outros_minimo').value;
      QcadtabelaVL_EXCEDENTE.value := dtmDados.iQuery1.FieldByName
        ('vl_excedente').value;
      QcadtabelaVL_AD.value := dtmDados.iQuery1.FieldByName('vl_ad').value;
      QcadtabelaVL_GRIS.value := dtmDados.iQuery1.FieldByName('vl_gris').value;
      QcadtabelaVL_GRIS_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('vl_gris_minimo').value;
      QcadtabelaUSUARIO.value := GLBUSER;
      QcadtabelaMODAL.value := '';
      QcadtabelaVEICULO.value := QTabelaVEICULO.value;
      QcadtabelaVL_ENTREGA.value := dtmDados.iQuery1.FieldByName
        ('VL_ENTREGA_PORTO').value;
      QcadtabelaDT_CADASTRO.value := date;
      QcadtabelaSITE.value := GLBFilial;
      QcadtabelaSEGURO_MINIMO.value := dtmDados.iQuery1.FieldByName
        ('SEGURO_MINIMO').value;
      QcadtabelaFL_IMPOSTO.value := dtmDados.iQuery1.FieldByName
        ('FL_IMPOSTO').value;
      QcadtabelaFL_PESO.value := dtmDados.iQuery1.FieldByName('FL_Peso').value;
      QcadtabelaCODMUN.value := dtmDados.iQuery1.FieldByName('codmun').value;
      QcadtabelaCODMUNO.value := dtmDados.iQuery1.FieldByName('codmunO').value;
      QcadtabelaVL_TDE.value := dtmDados.iQuery1.FieldByName('vl_tde').value;
      QcadtabelaVL_TR.value := dtmDados.iQuery1.FieldByName('vl_tr').value;
      QcadtabelaVL_TDEP.value := dtmDados.iQuery1.FieldByName('VL_TDEP').value;
      QcadtabelaVL_TDEM.value := dtmDados.iQuery1.FieldByName('VL_TDEM').value;
      QcadtabelaOPERACAO.value := QTabelaOPERACAO.value;
      QcadtabelaREGIAO.value := dtmDados.iQuery1.FieldByName('regiao').AsString;
      QcadtabelaVL_DESPACHO.value := dtmDados.iQuery1.FieldByName
        ('vl_despacho').value;
      QcadtabelaVL_DEVOLUCAO.value := dtmDados.iQuery1.FieldByName
        ('vl_devolucao').value;
      QcadtabelaFL_RATEIO.value := dtmDados.iQuery1.FieldByName
        ('fl_rateio').value;
      QcadtabelaVL_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('VL_FLUVIAL').value;
      QcadtabelaVL_FLUVIAL_M.value := dtmDados.iQuery1.FieldByName
        ('VL_FLUVIAL_M').value;
      QcadtabelaVL_TAS.value := dtmDados.iQuery1.FieldByName('VL_TAS').value;
      QcadtabelaVL_SEG_BALSA.value := dtmDados.iQuery1.FieldByName
        ('VL_SEG_BALSA').value;
      QcadtabelaVL_REDEP_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('VL_REDEP_FLUVIAL').value;
      QcadtabelaVL_AGEND.value := dtmDados.iQuery1.FieldByName
        ('VL_AGEND').value;
      QcadtabelaVL_PALLET.value := dtmDados.iQuery1.FieldByName
        ('VL_PALLET').value;
      QcadtabelaVL_ENTREGA_PORTO.value := dtmDados.iQuery1.FieldByName
        ('VL_ENTREGA_PORTO').value;
      QcadtabelaVL_ALFAND.value := dtmDados.iQuery1.FieldByName
        ('VL_ALFAND').value;
      QcadtabelaVL_CANHOTO.value := dtmDados.iQuery1.FieldByName
        ('VL_CANHOTO').value;
      QcadtabelaVL_FLUVMIN.value := dtmDados.iQuery1.FieldByName
        ('VL_FLUVMIN').value;
      QcadtabelaVL_AJUDA.value := dtmDados.iQuery1.FieldByName
        ('VL_AJUDA').value;
      QcadtabelaFL_FRETE.value := dtmDados.iQuery1.FieldByName
        ('FL_FRETE').value;
      QcadtabelaFL_PEDAGIO.value := dtmDados.iQuery1.FieldByName
        ('FL_PEDAGIO').value;
      QcadtabelaFL_EXCEDENTE.value := dtmDados.iQuery1.FieldByName
        ('FL_EXCEDENTE').value;
      QcadtabelaFL_GRIS.value := dtmDados.iQuery1.FieldByName('FL_GRIS').value;
      QcadtabelaFL_SEGURO.value := dtmDados.iQuery1.FieldByName
        ('FL_SEGURO').value;
      QcadtabelaFL_TDE.value := dtmDados.iQuery1.FieldByName('FL_TDE').value;
      QcadtabelaFL_TR.value := dtmDados.iQuery1.FieldByName('FL_TR').value;
      QcadtabelaFL_SEG_BALSA.value := dtmDados.iQuery1.FieldByName
        ('FL_SEG_BALSA').value;
      QcadtabelaFL_REDEP_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('FL_REDEP_FLUVIAL').value;
      QcadtabelaFL_AGEND.value := dtmDados.iQuery1.FieldByName
        ('FL_AGEND').value;
      QcadtabelaFL_PALLET.value := dtmDados.iQuery1.FieldByName
        ('FL_PALLET').value;
      QcadtabelaFL_TAS.value := dtmDados.iQuery1.FieldByName('FL_TAS').value;
      QcadtabelaFL_DESPACHO.value := dtmDados.iQuery1.FieldByName
        ('FL_DESPACHO').value;
      QcadtabelaFL_ENTREGA_PORTO.value := dtmDados.iQuery1.FieldByName
        ('FL_ENTREGA_PORTO').value;
      QcadtabelaFL_ALFAND.value := dtmDados.iQuery1.FieldByName
        ('FL_ALFAND').value;
      QcadtabelaFL_CANHOTO.value := dtmDados.iQuery1.FieldByName
        ('FL_CANHOTO').value;
      QcadtabelaFL_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('FL_FLUVIAL').value;
      QcadtabelaFL_AJUDA.value := dtmDados.iQuery1.FieldByName
        ('FL_AJUDA').value;
      QcadtabelaFL_CTE.value := dtmDados.iQuery1.FieldByName('FL_CTE').value;
      QcadtabelaFL_NF_FRETE.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_FRETE').value;
      QcadtabelaFL_NF_PEDAGIO.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_PEDAGIO').AsString;
      QcadtabelaFL_NF_EXCEDENTE.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_EXCEDENTE').AsString;
      QcadtabelaFL_NF_GRIS.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_GRIS').AsString;
      QcadtabelaFL_NF_SEGURO.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_SEGURO').AsString;
      QcadtabelaFL_NF_TAS.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_TAS').AsString;
      QcadtabelaFL_NF_CTE.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_CTE').AsString;
      QcadtabelaFL_NF_TDE.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_TDE').AsString;
      QcadtabelaFL_NF_TR.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_TR').AsString;
      QcadtabelaFL_NF_SEG_BALSA.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_SEG_BALSA').AsString;
      QcadtabelaFL_NF_REDEP_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_REDEP_FLUVIAL').AsString;
      QcadtabelaFL_NF_AGEND.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_AGEND').AsString;
      QcadtabelaFL_NF_PALLET.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_PALLET').AsString;
      QcadtabelaFL_NF_DESPACHO.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_DESPACHO').AsString;
      QcadtabelaFL_NF_ENTREGA_PORTO.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_ENTREGA_PORTO').AsString;
      QcadtabelaFL_NF_ALFAND.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_ALFAND').AsString;
      QcadtabelaFL_NF_CANHOTO.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_CANHOTO').AsString;
      QcadtabelaFL_NF_FLUVIAL.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_FLUVIAL').AsString;
      QcadtabelaFL_NF_AJUDA.value := dtmDados.iQuery1.FieldByName
        ('FL_NF_AJUDA').AsString;
      QcadtabelaVL_REENT_MINIMA.value := dtmDados.iQuery1.FieldByName
        ('VL_REENT_MINIMA').value;
      // Implementa��o do projeto generalidades
      QcadtabelaVL_ARMAZENAGEM.value := dtmDados.iQuery1.FieldByName
        ('VL_ARMAZENAGEM').AsFloat;
      QcadtabelaVL_REENTREGA.value := dtmDados.iQuery1.FieldByName
        ('VL_REENTREGA').value;
      QcadtabelaVL_HORA_PARADA.value := dtmDados.iQuery1.FieldByName
        ('VL_HORA_PARADA').value;
      QcadtabelaVL_6X1.value := dtmDados.iQuery1.FieldByName
        ('VL_6X1').value;
      QcadtabelaQT6X1.value := dtmDados.iQuery1.FieldByName
        ('QT6X1').value;

      QcadtabelaVL_TDA.value := dtmDados.iQuery1.FieldByName('vl_tda').value;
      QcadtabelaVL_TDAP.value := dtmDados.iQuery1.FieldByName('vl_tdap').value;
      QcadtabelaVL_TDAmin.value := dtmDados.iQuery1.FieldByName('vl_tdamin').value;
      QcadtabelaVL_TRTMIN.value := dtmDados.iQuery1.FieldByName('vl_trtmin').value;

      Qcadtabela.post;
      Qcadtabela.Close;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('select max(cod_venda) cod from tb_tabelavenda ');
      dtmDados.iQuery1.Open;
      new := dtmDados.iQuery1.FieldByName('cod').value;
      // generalidade
      QGenItem.Close;
      QGenItem.Parameters[0].value := cod;
      QGenItem.Open;
      QGenItem.First;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('insert into tb_tabelavenda_generalidades (cod_VENDA,vl_pernoite,vl_vd,vl_diaria,veiculo)');
      dtmDados.iQuery1.sql.add(' values ( :0, :1, :2, :3, :4)');
      while not QGenItem.Eof do
      begin
        dtmDados.iQuery1.Parameters[0].value := new;
        dtmDados.iQuery1.Parameters[1].value := QGenItemVL_PERNOITE.AsFloat;
        dtmDados.iQuery1.Parameters[2].value := QGenItemVL_VD.AsFloat;
        dtmDados.iQuery1.Parameters[3].value := QGenItemVL_DIARIA.AsFloat;
        dtmDados.iQuery1.Parameters[4].value := QGenItemVEICULO.AsString;
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
        QGenItem.next;
      end;
      dtmDados.iQuery1.Close;

      // fra��o
      QItem.Close;
      QItem.Parameters[0].value := cod;
      QItem.Open;
      QItem.First;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add('insert into tb_tabelavenda_item (cod_venda, ');
      dtmDados.iQuery1.sql.add('datai, dataf, vl_rodoviario, nr_peso_de, ');
      dtmDados.iQuery1.sql.add('nr_peso_ate, vr_ctrc, vr_exced) ');
      dtmDados.iQuery1.sql.add('values (:0, :1, :2, :3, :4, :5, :6, :7)');
      while not QItem.Eof do
      begin
        dtmDados.iQuery1.Parameters[0].value := new;
        dtmDados.iQuery1.Parameters[1].value := QItemDATAI.AsDateTime;
        dtmDados.iQuery1.Parameters[2].value := QItemDATAF.AsDateTime;
        dtmDados.iQuery1.Parameters[3].value := QItemVL_RODOVIARIO.AsFloat;
        dtmDados.iQuery1.Parameters[4].value := QItemNR_PESO_DE.AsFloat;
        dtmDados.iQuery1.Parameters[5].value := QItemNR_PESO_ATE.AsFloat;
        dtmDados.iQuery1.Parameters[6].value := QItemVR_CTRC.AsFloat;
        dtmDados.iQuery1.Parameters[7].value := QItemVR_EXCED.AsFloat;
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
        QItem.Next;
      end;
      dtmDados.iQuery1.Close;
      ShowMessage('Tabela Duplicada - Mude algum campo que n�o fique igual a duplicada por motivos de duplicidade');
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    Qcadtabela.Close;
    QTabela.Close;
    QTabela.sql.Text := sql;
    QTabela.Open;
    Qcadtabela.Open;
    QTabela.Last;
    QItem.Close;
    QItem.Open;
    tipo := '';
  end;
end;

procedure TfrmCadTabelaVenda.btalterargenClick(Sender: TObject);
begin
  // Implementa��o do projeto generalidades
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.alterar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  status := 'A';
  cbVeiculogen.ItemIndex := cbVeiculogen.Items.IndexOf(QGenItemVEICULO.AsString);
  edpernoiteGen.Text := QGenItemVL_PERNOITE.AsString;
  eddiariaGen.Text := QGenItemVL_DIARIA.AsString;
  eddedicadoGen.Text := QGenItemVL_VD.AsString;
  edPaleteGen.Value := QGenItemVL_PALETE.AsFloat;
  RestauraGen;
end;

procedure TfrmCadTabelaVenda.btcancelagenClick(Sender: TObject);
begin
  // Implementa��o do projeto generalidades
  RestauraGen;
  LimpadadosGen;
end;

procedure TfrmCadTabelaVenda.btcancelgenClick(Sender: TObject);
begin
  btnAlterarGen.Enabled := not btnAlterarGen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;
end;

procedure TfrmCadTabelaVenda.btnAlterarGenClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  status := 'A';

  btnAlterarGen.Enabled := not btnAlterarGen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;
end;

procedure TfrmCadTabelaVenda.btexcluirgenClick(Sender: TObject);
begin
  // Implementa��o do projeto generalidades
  if not dtmDados.PodeApagar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.apagar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('apagar').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  if Application.Messagebox('Voc� Deseja Apagar Este Registro?',
    'Apagar Registro', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_tabelavenda_generalidades where cod_id =:0');
    dtmDados.iQuery1.Parameters[0].value := QGenItemCOD_ID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end;
  LimpadadosGen;
end;

procedure TfrmCadTabelaVenda.btinserirgenClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  status := 'I';
  RestauraGen;
  LimpadadosGen;
end;

procedure TfrmCadTabelaVenda.btnAereoAClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.ALTERAR FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if QItemDATAF.value < date() then
    Exit;
  tipo := 'A';
  edDataAereoI.date := QAereoDATAI.value;
  edDataAereoF.date := QAereoDATAF.value;
  edAereoEntrega.value := QAereoVALOR_ENTREGA.Value;
  edPesoAereoI.value := QAereoPESO_DE.Value;
  edPesoAereoF.value := QAereoPESO_ATE.Value;
  edAereoColeta.value := QAereoVALOR_COLETA.Value;
  edAereoExc.value := QAereoVR_EXCED.Value;
  RestauraAereo;
  edDataAereoI.SetFocus;
end;

procedure TfrmCadTabelaVenda.btnAereoCClick(Sender: TObject);
begin
  QAereo.Cancel;
  RestauraAereo;
end;

procedure TfrmCadTabelaVenda.btnAereoEClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QAereo.Delete;
  end;
end;

procedure TfrmCadTabelaVenda.btnAereoIClick(Sender: TObject);
begin
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  tipo := 'I';
  RestauraAereo;
  edDataAereoI.SetFocus;
  edDataAereoI.Text := '';
  edDataAereoF.Text := '';
  edAereoEntrega.value := 0;
  edPesoAereoI.value := 0;
  edPesoAereoF.value := 0;
  edAereoColeta.value := 0;
  edAereoExc.value := 0;
end;

procedure TfrmCadTabelaVenda.btnAereoSClick(Sender: TObject);
var
  cod: Integer;
begin
  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    if (edDataAereoF.date - edDataAereoI.date) > 3 then
    begin
      ShowMessage
        ('Esta Tabelas SPOT o periodo de videncia n�o pode ser maior que 3 dias !!');
      Exit;
    end;
  end;

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_tabela_aereo(:0,:1,:2,:3,:4,''R'',:5) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QTabelaCOD_VENDA.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataAereoI.date;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataAereoF.date;
  dtmDados.iQuery1.Parameters.ParamByName('3').value := edPesoAereoI.value;
  dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoAereoF.value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('5').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('5').value := QAereoCOD_TAB.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Data e Peso digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_tabelavenda_aereo (cod_venda, ' +
        'datai, dataf, valor_coleta, peso_de, peso_ate, valor_entrega, vr_exced) ' +
        'values (:0, :1, :2, :3, :4, :5, :6, :7)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QTabelaCOD_VENDA.AsInteger;
    end
    else
    begin
      cod := QItemCOD_TAB.AsInteger;
      dtmDados.iQuery1.sql.add('update tb_tabelavenda_aereo set datai = :1, ' +
        'dataf = :2, valor_coleta = :3, peso_de = :4, peso_ate = :5, valor_entrega = :6, '
        + 'vr_exced = :7 where cod_tab = :0');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QAereoCOD_TAB.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataAereoI.date;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataAereoF.date;
    dtmDados.iQuery1.Parameters.ParamByName('3').value := edAereoColeta.value;
    dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoAereoI.value;
    dtmDados.iQuery1.Parameters.ParamByName('5').value := edPesoAereoF.value;
    dtmDados.iQuery1.Parameters.ParamByName('6').value := edAereoEntrega.value;
    dtmDados.iQuery1.Parameters.ParamByName('7').value := edAereoExc.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_tabelavenda set controler = null, dt_controler = null ');
    dtmDados.iQuery1.sql.add('where cod_venda = :0 ');
    dtmDados.iQuery1.Parameters[0].value := QAereoCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QAereo.Close;
  QAereo.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
  QAereo.Open;
  if tipo = 'A' then
    QAereo.Locate('cod_tab', cod, [])
  else
    QAereo.Last;
  tipo := '';
  RestauraAereo;
end;

procedure TfrmCadTabelaVenda.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.alterar FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  Restaura;
  PageControl1.ActivePage := Detalhes;
  cbtra.ItemIndex := cbtra.Items.IndexOf(edTrans.Text);
  cbtra.SetFocus;
  cbVeiculo.ItemIndex := cbVeiculo.Items.IndexOf(edVeiculo.Text);
  tipo := 'A';
end;

procedure TfrmCadTabelaVenda.dbgContratoTitleClick(Column: TColumn);
var
  icount: Integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.sql.Text) > 0 then
  begin
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('order by', QTabela.sql.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.sql.Text := QTabela.sql.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTabelaVenda.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QItemDATAF.Value < date() then
  begin
    DBGrid1.canvas.Brush.color := $006174FC;
    DBGrid1.canvas.font.color := clBlack;
  end
  else
  begin
    DBGrid1.canvas.Brush.color := clWindow;
    DBGrid1.canvas.font.color := clBlack;
  end;
  DBGrid1.DefaultDrawDataCell(Rect, DBGrid1.Columns[DataCol].field, State);
end;

procedure TfrmCadTabelaVenda.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePage := Consulta;
  if GLBUSER = 'PCORTEZ' then
  begin
    btnExcluir_.Visible := true;
  end;
end;

procedure TfrmCadTabelaVenda.btnExcluir_Click(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Esta Tabela?', 'Apagar Tabela',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_tabelavenda_item where cod_venda = :cn');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_tabelavenda where cod_venda = :cn');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QTabela.Close;
    QTabela.Open;
  end;
end;

procedure TfrmCadTabelaVenda.btnExcelClick(Sender: TObject);
var coluna, linha, linhag: integer;
    excel, sheet: variant;
    valor: string;
begin
  if filtro = 0 then
  begin
    showmessage('� necess�rio o filtro para exporta��o de tabelas');
    exit;
  end;
  try
    excel:=CreateOleObject('Excel.Application');
//excel.visible:=true;
    excel.Workbooks.add(1);
    //Excel.Workbooks[1].Sheets.Add;
    Excel.WorkBooks[1].WorkSheets[1].Name := 'Informa��es';
  except
    Application.MessageBox ('Vers�o do Ms-Excel Incompat�vel','Erro',MB_OK+MB_ICONEXCLAMATION);
  end;

  QTabela.First;
  Gauge1.Visible := true;
  Gauge1.MaxValue := QTabela.RecordCount;
  excel.Cells.Select;
  //Sheet := Excel.Workbooks[1].WorkSheets['Informa��es'];
  label40.Caption := 'Informa��es';
  try
    for linha:=0 to QTabela.RecordCount-1 do
    begin
      QExcelTab.Close;
      QExcelTab.Parameters[0].Value := QcadtabelaCOD_VENDA.AsInteger;
      QExcelTab.Open;
      for coluna:=1 to JvDBGrid2.Columns.Count do
      begin
        if QExcelTab.Fields[coluna-1].Visible = true then
        begin
          valor:= QExcelTab.Fields[coluna-1].DisplayName;
          if valor = 'Cliente' then
          begin
            excel.cells [linha+2,coluna].NumberFormat := '@';
            excel.cells [linha+2,coluna]:= QTabelaNM_CLIENTE.AsString;
          end
          else if valor = 'Cidade Origem' then
          begin
            excel.cells [linha+2,coluna].NumberFormat := '@';
            excel.cells [linha+2,coluna]:= cbCidadeO.Text;
          end
          else if valor = 'Cidade Destino' then
          begin
            excel.cells [linha+2,coluna].NumberFormat := '@';
            excel.cells [linha+2,coluna]:= cbCidade.Text;
          end
          else
          begin
            if QExcelTab.Fields[coluna-1].DataType = ftDateTime then
            begin
              excel.cells [linha+2,coluna].NumberFormat := 'dd/mm/aaaa';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsDateTime;
            end
            else if QExcelTab.Fields[coluna-1].DataType = ftString then
            begin
              excel.cells [linha+2,coluna].NumberFormat := '@';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsString;
            end
            else if QExcelTab.Fields[coluna-1].DataType = ftBCD then
            begin
              excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsFloat;
            end
            else if QExcelTab.Fields[coluna-1].DataType = ftFloat then
            begin
              excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsFloat;
            end
            else if QExcelTab.Fields[coluna-1].DataType = ftInteger then
            begin
              excel.cells [linha+2,coluna].NumberFormat := '#.###.##0';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsInteger;
            end
            else
            begin
              excel.cells [linha+2,coluna].NumberFormat := '@';
              excel.cells [linha+2,coluna]:= QExcelTab.Fields[coluna-1].AsString;
            end;
          end;
        end;
      end;
      Gauge1.AddProgress(1);
      QTabela.Next;
    end;
    for coluna:=1 to JvDBGrid2.Columns.Count do
    begin
      if QCadTabela.Fields[coluna-1].Visible = true then
      begin
        valor:= JvDBGrid2.Columns[coluna-1].Title.Caption;
        excel.cells[1,coluna]:=valor;
      end;
    end;
    // Generalidades
    QTabela.First;
    Gauge1.Progress := 0;
    linhag := 0;
    label40.Caption := 'Generalidades';
    Excel.Workbooks[1].Sheets.Add;
    Excel.WorkBooks[1].WorkSheets[1].Name := 'Generalidade';
    excel.Cells.Select;

    while not QTabela.eof do
    begin
      QGenItem.First;
      While not QGenItem.eof do
      begin
        for coluna:=1 to JvDBGrid1.Columns.Count do
        begin
          valor:= QGenItem.Fields[coluna-1].DisplayName;
          if QGenItem.Fields[coluna-1].DataType = ftDateTime then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := 'dd/mm/aaaa';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsDateTime;
          end
          else if QGenItem.Fields[coluna-1].DataType = ftString then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '@';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsString;
          end
          else if QGenItem.Fields[coluna-1].DataType = ftBCD then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0,00';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsFloat;
          end
          else if QGenItem.Fields[coluna-1].DataType = ftFloat then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0,00';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsFloat;
          end
          else if QGenItem.Fields[coluna-1].DataType = ftInteger then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsInteger;
          end
          else
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '@';
            excel.cells [linhag+2,coluna]:= QGenItem.Fields[coluna-1].AsString;
          end;
        end;
        linhag := linhag + 1;
        QGenItem.Next;
      end;
      Gauge1.AddProgress(1);
      QTabela.Next;
    end;
    for coluna:=1 to JvDBGrid1.Columns.Count do
    begin
      if QGenItem.Fields[coluna-1].Visible = true then
      begin
        valor:= JvDBGrid1.Columns[coluna-1].Title.Caption;
        excel.cells[1,coluna]:=valor;
      end;
    end;


    // Fra��o
    QTabela.First;
    Gauge1.Progress := 0;
    linhag := 0;
    label40.Caption := 'Fra��o';
    Excel.Workbooks[1].Sheets.Add;
    Excel.WorkBooks[1].WorkSheets[1].Name := 'Fra��o';
    excel.Cells.Select;
    while not QTabela.eof do
    begin
      QItem.First;
      While not QItem.eof do
      begin
        for coluna:=1 to DBGrid1.Columns.Count do
        begin
          valor:= QItem.Fields[coluna-1].DisplayName;
          if (valor = 'cod_venda') or (valor = 'cod_tab') then
          begin
            excel.cells [linhag+2,coluna].NumberFormat := '@';
            excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsString;
          end
          else
          begin
            if QItem.Fields[coluna-1].DataType = ftDateTime then
            begin
              excel.cells [linhag+2,coluna].NumberFormat := 'dd/mm/aaaa';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsDateTime;
            end
            else if QItem.Fields[coluna-1].DataType = ftString then
            begin
              excel.cells [linhag+2,coluna].NumberFormat := '@';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsString;
            end
            else if QItem.Fields[coluna-1].DataType = ftBCD then
            begin
              excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0,00';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsFloat;
            end
            else if QItem.Fields[coluna-1].DataType = ftFloat then
            begin
              excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0,00';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsFloat;
            end
            else if QItem.Fields[coluna-1].DataType = ftInteger then
            begin
              excel.cells [linhag+2,coluna].NumberFormat := '#.###.##0';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsInteger;
            end
            else
            begin
              excel.cells [linhag+2,coluna].NumberFormat := '@';
              excel.cells [linhag+2,coluna]:= QItem.Fields[coluna-1].AsString;
            end;
          end;
        end;
        linhag := linhag + 1;
        QItem.Next;
      end;
      Gauge1.AddProgress(1);
      QTabela.Next;
    end;
    for coluna:=1 to DBGrid1.Columns.Count do
    begin
      if QItem.Fields[coluna-1].Visible = true then
      begin
        valor:= DBGrid1.Columns[coluna-1].Title.Caption;
        excel.cells[1,coluna]:=valor;
      end;
    end;
    //Excel.WorkBooks[1].Sheets['PLAN3'].Name := 'Fra��o';
    excel.columns.AutoFit;
    excel.visible:=true;
  except
    Application.MessageBox ('Aconteceu um erro desconhecido durante a convers�o'+
    'da tabela para o Ms-Excel','Erro',MB_OK+MB_ICONEXCLAMATION);
  end;
  Gauge1.Visible := false;
  Label40.Caption := '';
end;

procedure TfrmCadTabelaVenda.btnExcluirIClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QItem.Delete;
  end;
end;

procedure TfrmCadTabelaVenda.btnExcluirReeClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QItemRee.Delete;
  end;
end;

procedure TfrmCadTabelaVenda.FormCreate(Sender: TObject);
begin
  //if Assigned(TStyleManager.ActiveStyle) then
  //  fdefaultStyleName := TStyleManager.ActiveStyle.Name;
{
   if Assigned(TStyleManager.ActiveStyle) and (TStyleManager.ActiveStyle.Name<>'Windows') then
   begin
     TStyleManager.TrySetStyle('Windows');
   end
   else
   begin
     TStyleManager.TrySetStyle(fdefaultStyleName); // whatever was in the project settings.
   end;
 }
  filtro := 0;
  QCliente.Open;
  sql := QTabela.sql.Text;
  cbtra.clear;
  cbCliente.clear;
  cbtra.Items.add('');
  cbCliente.Items.add('');
  while not QCliente.Eof do
  begin
    cbtra.Items.add(QClienteNM_CLIENTE.AsString);
    cbCliente.Items.add(QClienteNM_CLIENTE.AsString);
    QCliente.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct operacao from tb_operacao where fl_receita = ''S'' order by 1');
  dtmDados.iQuery1.Open;
  cbOperacao.clear;
  cbOperacaoF.clear;
  cbOperacaoPesq.clear;
  cbOperacao.Items.add('');
  cbOperacaoPesq.Items.add('');
  cbOperacaoF.Items.add('');
  while not dtmDados.iQuery1.Eof do
  begin
    cbOperacao.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    cbOperacaoPesq.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    cbOperacaoF.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    dtmDados.iQuery1.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('SELECT distinct descri FROM cyber.RODFRO order by 1');
  dtmDados.iQuery1.Open;
  cbVeiculo.clear;
  cbVeiculoPesq.clear;
  cbveiculogen.clear;
  cbveiculoF.clear;
  cbVeiculo.Items.add('');
  cbVeiculoPesq.Items.add('');
  cbveiculogen.Items.add('');
  cbveiculoF.Items.add('');
  while not dtmDados.iQuery1.Eof do
  begin
    cbVeiculo.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    cbVeiculoPesq.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    cbveiculogen.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    cbveiculoF.Items.add(dtmDados.iQuery1.FieldByName('descri').value);
    dtmDados.iQuery1.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct f.nomeab || '' - '' || f.codcgc nm_cliente from ');
  dtmDados.iQuery1.sql.add('tb_tabelavenda t inner join cyber.rodcli f ');
  dtmDados.iQuery1.sql.add('on f.codclifor = t.cod_cliente order by 1 ');
  dtmDados.iQuery1.Open;

  cbPesqTransp.clear;
  cbPesqTransp.Items.add('');
  While not dtmDados.iQuery1.Eof do
  begin
    cbPesqTransp.Items.add
      (alltrim(dtmDados.iQuery1.FieldByName('nm_cliente').value));
    dtmDados.iQuery1.Next;
  end;
  dtmDados.iQuery1.Close;

  //QTabela.Open;
  // Qtabela.First;
  //quant := QTabela.RecordCount;
  //lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadTabelaVenda.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QItemReeDATAF.Value < date() then
  begin
    JvDBUltimGrid1.canvas.Brush.color := $006174FC;
    JvDBUltimGrid1.canvas.font.color := clBlack;
  end
  else
  begin
    JvDBUltimGrid1.canvas.Brush.color := clWindow;
    JvDBUltimGrid1.canvas.font.color := clBlack;
  end;
  JvDBUltimGrid1.DefaultDrawDataCell(Rect, JvDBUltimGrid1.Columns[DataCol].field, State);

end;

procedure TfrmCadTabelaVenda.JvDBUltimGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QAereoDATAF.Value < date() then
  begin
    JvDBUltimGrid2.canvas.Brush.color := $006174FC;
    JvDBUltimGrid2.canvas.font.color := clBlack;
  end
  else
  begin
    JvDBUltimGrid2.canvas.Brush.color := clWindow;
    JvDBUltimGrid2.canvas.font.color := clBlack;
  end;
  JvDBUltimGrid2.DefaultDrawDataCell(Rect, JvDBUltimGrid2.Columns[DataCol].field, State);
end;

procedure TfrmCadTabelaVenda.Limpadados;
begin
  edFreteM.value := 0;
  edPedagio.value := 0;
  edOutros.value := 0;
  edOutrosP.value := 0;
  edOutrosM.value := 0;
  edExc.value := 0;
  EdSeg.value := 0;
  EdGris.value := 0;
  eddespacho.value := 0;
  EdGrisM.value := 0;
  edTDE.value := 0;
  edTR.value := 0;
  edTDEp.value := 0;
  edTDEm.value := 0;
  edTdeMax.value := 0;
  cbImposto.Checked := true;
  cbPeso.Checked := false;
  cbVeiculo.ItemIndex := -1;
  cbCidade.ItemIndex := -1;
  cbCidadeO.ItemIndex := -1;
  cbStatus.Checked := false;
  cbOperacao.ItemIndex := -1;
  Obs.clear;
  cbRegiao.ItemIndex := -1;
  ed_fluvial.value := 0;
  ed_fluv_min.value := 0;
  edseg_balsa.value := 0;
  edred_fluv.value := 0;
  edagend.value := 0;
  edpallet.value := 0;
  edtas.value := 0;
  edentrega.value := 0;
  edalfand.value := 0;
  edcanhoto.value := 0;
  edDataI.Text := '';
  edDataF.Text := '';
  cbRateio.Checked := false;
  edFluvMin.value := 0;
  edAjuda.value := 0;
  edRMinimo.value := 0;
  ed6x1.Value := 0;
  edqt6x1.value := 0;
  // edDtF.Clear;
  edArmazem.value := 0;
  //edDiaria.value := 0;
  edReentrega.value := 0;
  //edDedicado.value := 0;
  //edPernoite.value := 0;
  edHoraParada.value := 0;
  cbImpostoICMS.Checked := false;
  edfretepeso.value := 0;
  edtaxatrt.value := 0;
  edTDAp.value := 0;
  edTDAmin.value := 0;
  edTDA.value := 0;
end;

procedure TfrmCadTabelaVenda.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.ActivePage = TabSheet2) and (btnNovo.Enabled = false) then
    PageControl1.ActivePage := Detalhes;
end;

procedure TfrmCadTabelaVenda.QItemAfterScroll(DataSet: TDataSet);
begin
  QAuditi.Close;
  QAuditi.Parameters[0].Value := QItemCOD_TAB.AsInteger;
  QAuditi.open;
end;

procedure TfrmCadTabelaVenda.QTabelaAfterOpen(DataSet: TDataSet);
begin
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadTabelaVenda.QTabelaAfterScroll(DataSet: TDataSet);
begin

  Qcadtabela.Close;
  Qcadtabela.Parameters[0].value := QTabelaCOD_VENDA.value;
  Qcadtabela.Open;
  QItem.Close;
  QItem.Parameters[0].value := QTabelaCOD_VENDA.value;
  QItem.Open;

  QItemRee.Close;
  QItemRee.Parameters[0].value := QTabelaCOD_VENDA.value;
  QItemRee.Open;

  QGenItem.Close;
  QGenItem.Parameters[0].value := QTabelaCOD_VENDA.value;
  QGenItem.Open;

  QAudit.Close;
  QAudit.Parameters[0].value := QTabelaCOD_VENDA.value;
  QAudit.Open;


  if QcadtabelaCONTROLER.value <> '' then
  begin
    btnAlterar.Visible := false;
    btnAlterarI.Visible := false;
    btnAlterarRee.Visible := false;
    btnAlterarGen.Visible := false;
  end
  else
  begin
    btnAlterar.Visible := true;
    btnAlterarI.Visible := true;
    btnAlterarRee.Visible := true;
    btnAlterarGen.Visible := true;

  end;

  if tipo <> 'I' then
  begin
    CBLO.ItemIndex := CBLO.Items.IndexOf(QTabelaLOCAL.AsString);
    CBUFO.ItemIndex := CBUFO.Items.IndexOf(QTabelaDS_UF.AsString);
    cbLd.ItemIndex := cbLd.Items.IndexOf(QTabelaLOCAL_DES.AsString);
    cbUFD.ItemIndex := cbUFD.Items.IndexOf(QTabelaDS_UF_DES.AsString);
    edVeiculo.Text := QTabelaVEICULO.AsString;
    edTrans.Text := QTabelaNM_CLIENTE.AsString;
    edFreteM.value := QTabelaVL_FRETE_MINIMO.value;
    // Implementa��o do projeto de generalidades
    edpallet.value := QcadtabelaVL_PALLET.value;
    edAjuda.value := QcadtabelaVL_AJUDA.value;
    edReentrega.value := QcadtabelaVL_REENTREGA.value;
    edArmazem.value := QcadtabelaVL_ARMAZENAGEM.AsFloat;
    edHoraParada.value := QcadtabelaVL_HORA_PARADA.value;
    edcanhoto.value := QcadtabelaVL_CANHOTO.value;

    edRMinimo.value := QcadtabelaVL_REENT_MINIMA.value;
    ed6x1.Value := QcadtabelaVL_6X1.Value;
    edqt6x1.Value := Qcadtabelaqt6x1.value;

    edfretepeso.value := QcadtabelaVL_FRETE_VALOR.value;
    edtaxatrt.value := QcadtabelaVL_TAXA_TRT.value;
    edagend.value := QcadtabelaVL_AGEND.value;
    edPedagio.value := QcadtabelaVL_PEDAGIO.value;
    edOutros.value := QcadtabelaVL_OUTROS.value;
    edOutrosP.value := QcadtabelaVL_OUTROS_P.value;
    edOutrosM.value := QcadtabelaVL_OUTROS_MINIMO.value;
    edExc.value := QcadtabelaVL_EXCEDENTE.value;
    EdGrisM.value := QcadtabelaVL_GRIS_MINIMO.value;
    EdSeg.value := QcadtabelaVL_AD.value;
    EdGris.value := QcadtabelaVL_GRIS.value;
    eddespacho.value := QcadtabelaVL_DESPACHO.value;
    edsegminimo.value := QcadtabelaSEGURO_MINIMO.value;
    edTDE.value := QcadtabelaVL_TDE.value;
    edTDEp.value := QcadtabelaVL_TDEP.value;
    edTDEm.value := QcadtabelaVL_TDEM.value;
    edTR.value := QcadtabelaVL_TR.value;
    edTdeMax.value := QcadtabelaVL_TDEMAX.value;
    ed_fluvial.value := QcadtabelaVL_FLUVIAL.value;
    ed_fluv_min.value := QcadtabelaVL_FLUVIAL_M.value;
    edseg_balsa.value := QcadtabelaVL_SEG_BALSA.value;
    edred_fluv.value := QcadtabelaVL_REDEP_FLUVIAL.value;
    edtas.value := QcadtabelaVL_TAS.value;
    edentrega.value := QcadtabelaVL_ENTREGA_PORTO.value;
    edalfand.value := QcadtabelaVL_ALFAND.value;
    edTRTMin.value := QcadtabelaVL_TRTMIN.value;
    edFluvMin.value := QcadtabelaVL_FLUVMIN.value;

    edTDA.value := QcadtabelaVL_TDA.value;
    edTDAp.value := QcadtabelaVL_TDAP.value;
    edTDAmin.value := QcadtabelaVL_TDAmin.value;

    if edFreteM.value > 0 then
      edFreteM.Color := $00A9FEFA
    else
      edFreteM.Color := clWindow;

    if edPedagio.value > 0 then
      edPedagio.Color := $00A9FEFA
    else
      edPedagio.Color := clWindow;

    if edOutros.value > 0 then
      edOutros.Color := $00A9FEFA
    else
      edOutros.Color := clWindow;

    if edOutrosP.value > 0 then
      edOutrosP.Color := $00A9FEFA
    else
      edOutrosP.Color := clWindow;

    if edOutrosM.value > 0 then
      edOutrosM.Color := $00A9FEFA
    else
      edOutrosM.Color := clWindow;

    if edExc.value > 0 then
      edExc.Color := $00A9FEFA
    else
      edExc.Color := clWindow;

    if EdSeg.value > 0 then
      EdSeg.Color := $00A9FEFA
    else
      EdSeg.Color := clWindow;

    if EdGris.value > 0 then
      EdGris.Color := $00A9FEFA
    else
      EdGris.Color := clWindow;

    if EdGrisM.value > 0 then
      EdGrisM.Color := $00A9FEFA
    else
      EdGrisM.Color := clWindow;

    if eddespacho.value > 0 then
      eddespacho.Color := $00A9FEFA
    else
      eddespacho.Color := clWindow;

    if edsegminimo.value > 0 then
      edsegminimo.Color := $00A9FEFA
    else
      edsegminimo.Color := clWindow;
    if edTDE.value > 0 then
      edTDE.Color := $00A9FEFA
    else
      edTDE.Color := clWindow;
    if edTDEp.value > 0 then
      edTDEp.Color := $00A9FEFA
    else
      edTDEp.Color := clWindow;
    if edTDEm.value > 0 then
      edTDEm.Color := $00A9FEFA
    else
      edTDEm.Color := clWindow;
    if edTR.value > 0 then
      edTR.Color := $00A9FEFA
    else
      edTR.Color := clWindow;
    if edTdeMax.value > 0 then
      edTdeMax.Color := $00A9FEFA
    else
      edTdeMax.Color := clWindow;
    if ed_fluvial.value > 0 then
      ed_fluvial.Color := $00A9FEFA
    else
      ed_fluvial.Color := clWindow;
    if ed_fluv_min.value > 0 then
      ed_fluv_min.Color := $00A9FEFA
    else
      ed_fluv_min.Color := clWindow;
    if edseg_balsa.value > 0 then
      edseg_balsa.Color := $00A9FEFA
    else
      edseg_balsa.Color := clWindow;
    if edred_fluv.value > 0 then
      edred_fluv.Color := $00A9FEFA
    else
      edred_fluv.Color := clWindow;

    if edagend.value > 0 then
      edagend.Color := $00A9FEFA
    else
      edagend.Color := clWindow;

    if edpallet.value > 0 then
      edpallet.Color := $00A9FEFA
    else
      edpallet.Color := clWindow;
    if edtas.value > 0 then
      edtas.Color := $00A9FEFA
    else
      edtas.Color := clWindow;
    if edentrega.value > 0 then
      edentrega.Color := $00A9FEFA
    else
      edentrega.Color := clWindow;
    if edalfand.value > 0 then
      edalfand.Color := $00A9FEFA
    else
      edalfand.Color := clWindow;

    if edcanhoto.value > 0 then
      edcanhoto.Color := $00A9FEFA
    else
      edcanhoto.Color := clWindow;

    if edFluvMin.value > 0 then
      edFluvMin.Color := $00A9FEFA
    else
      edFluvMin.Color := clWindow;

    if edAjuda.value > 0 then
      edAjuda.Color := $00A9FEFA
    else
      edAjuda.Color := clWindow;

    if edRMinimo.value > 0 then
      edRMinimo.Color := $00A9FEFA
    else
      edRMinimo.Color := clWindow;

    if ed6x1.value > 0 then
      ed6x1.Color := $00A9FEFA
    else
      ed6x1.Color := clWindow;

    if edqt6x1.value > 0 then
      edqt6x1.Color := $00A9FEFA
    else
      edqt6x1.Color := clWindow;

    if edAgendG.value > 0 then
      edAgendG.Color := $00A9FEFA
    else
      edAgendG.Color := clWindow;

    if edDiaria48.value > 0 then
      edDiaria48.Color := $00A9FEFA
    else
      edDiaria48.Color := clWindow;

    if edHoraParada.value > 0 then
      edHoraParada.Color := $00A9FEFA
    else
      edHoraParada.Color := clWindow;

    if edTRTMin.value > 0 then
      edTRTMin.Color := $00A9FEFA
    else
      edTRTMin.Color := clWindow;

    if edTDA.value > 0 then
      edTDA.Color := $00A9FEFA
    else
      edTDA.Color := clWindow;

    if edTDAp.value > 0 then
      edTDAp.Color := $00A9FEFA
    else
      edTDAp.Color := clWindow;

    if edTDAmin.value > 0 then
      edTDAmin.Color := $00A9FEFA
    else
      edTDAmin.Color := clWindow;

    if QcadtabelaCODMUNO.value > 0 then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := QTabelaDS_UF.value;
      QCidade.Open;
      cbCidadeO.clear;
      cbCidadeO.Items.add('');
      while not QCidade.Eof do
      begin
        cbCidadeO.Items.add(QCidadeDESCRI.AsString);
        QCidade.Next;
      end;
      if QCidade.Locate('codmun', QcadtabelaCODMUNO.AsInteger, []) then
        cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(QCidadeDESCRI.AsString);
    end
    else
      cbCidadeO.ItemIndex := -1;
    if QcadtabelaCODMUN.value > 0 then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := QTabelaDS_UF_DES.value;
      QCidade.Open;
      cbCidade.clear;
      cbCidade.Items.add('');
      while not QCidade.Eof do
      begin
        cbCidade.Items.add(QCidadeDESCRI.AsString);
        QCidade.Next;
      end;
      if QCidade.Locate('codmun', QcadtabelaCODMUN.AsInteger, []) then
        cbCidade.ItemIndex := cbCidade.Items.IndexOf(QCidadeDESCRI.AsString);
    end
    else
      cbCidade.ItemIndex := -1;
    if QcadtabelaFL_IMPOSTO.value = 'S' then
      cbImposto.Checked := true
    else
      cbImposto.Checked := false;

    if QcadtabelaFL_IMPOSTO_ICMS.value = 'S' then
      cbImpostoICMS.Checked := true
    else
      cbImpostoICMS.Checked := false;

    if QcadtabelaFL_PESO.value = 'S' then
      cbPeso.Checked := true
    else
      cbPeso.Checked := false;

    if QcadtabelaFL_STATUS.value = 'S' then
      cbStatus.Checked := true
    else
      cbStatus.Checked := false;

    if QcadtabelaFL_RATEIO.value = 'S' then
      cbRateio.Checked := true
    else
      cbRateio.Checked := false;

    if QcadtabelaFL_FRETE.value = 'S' then
      cbfrete.Checked := true
    else
      cbfrete.Checked := false;

    if QcadtabelaFL_PEDAGIO.value = 'S' then
      cbpedagio.Checked := true
    else
      cbpedagio.Checked := false;

    if QcadtabelaFL_EXCEDENTE.value = 'S' then
      cbexcedente.Checked := true
    else
      cbexcedente.Checked := false;

    if QcadtabelaFL_GRIS.value = 'S' then
      cbgris.Checked := true
    else
      cbgris.Checked := false;

    if QcadtabelaFL_SEGURO.value = 'S' then
      cbseguro.Checked := true
    else
      cbseguro.Checked := false;

    if QcadtabelaFL_TDE.value = 'S' then
      cbtde.Checked := true
    else
      cbtde.Checked := false;

    if QcadtabelaFL_TR.value = 'S' then
      cbtr.Checked := true
    else
      cbtr.Checked := false;

    if QcadtabelaFL_SEG_BALSA.value = 'S' then
      cbsegBalsa.Checked := true
    else
      cbsegBalsa.Checked := false;

    if QcadtabelaFL_REDEP_FLUVIAL.value = 'S' then
      cbredepFluvial.Checked := true
    else
      cbredepFluvial.Checked := false;

    if QcadtabelaFL_TAS.value = 'S' then
      cbtas.Checked := true
    else
      cbtas.Checked := false;

    if QcadtabelaFL_DESPACHO.value = 'S' then
      cbdespacho.Checked := true
    else
      cbdespacho.Checked := false;

    if QcadtabelaFL_ENTREGA_PORTO.value = 'S' then
      cbentport.Checked := true
    else
      cbentport.Checked := false;

    if QcadtabelaFL_ALFAND.value = 'S' then
      cbalfand.Checked := true
    else
      cbalfand.Checked := false;

    if QcadtabelaFL_FLUVIAL.value = 'S' then
      cbfluvial.Checked := true
    else
      cbfluvial.Checked := false;

    if QcadtabelaFL_CTE.value = 'S' then
      cbCte.Checked := true
    else
      cbCte.Checked := false;

    if QcadtabelaFL_NF_FRETE.value = 'S' then
      CbnfFrete.Checked := true
    else
      CbnfFrete.Checked := false;

    if QcadtabelaFL_NF_PEDAGIO.value = 'S' then
      CbnfPedagio.Checked := true
    else
      CbnfPedagio.Checked := false;

    if QcadtabelaFL_NF_EXCEDENTE.value = 'S' then
      CbnfExcedente.Checked := true
    else
      CbnfExcedente.Checked := false;

    if QcadtabelaFL_NF_GRIS.value = 'S' then
      CbnfGris.Checked := true
    else
      CbnfGris.Checked := false;

    if QcadtabelaFL_NF_SEGURO.value = 'S' then
      CbnfSeguro.Checked := true
    else
      CbnfSeguro.Checked := false;

    if QcadtabelaFL_NF_TAS.value = 'S' then
      CbnfTas.Checked := true
    else
      CbnfTas.Checked := false;

    if QcadtabelaFL_NF_CTE.value = 'S' then
      CbnftaxaCte.Checked := true
    else
      CbnftaxaCte.Checked := false;

    if QcadtabelaFL_NF_TDE.value = 'S' then
      CbnfTde.Checked := true
    else
      CbnfTde.Checked := false;

    if QcadtabelaFL_NF_TR.value = 'S' then
      CbnfTr.Checked := true
    else
      CbnfTr.Checked := false;

    if QcadtabelaFL_NF_SEG_BALSA.value = 'S' then
      CbnfSegBalsa.Checked := true
    else
      CbnfSegBalsa.Checked := false;

    if QcadtabelaFL_NF_REDEP_FLUVIAL.value = 'S' then
      CbnfRedepFluvial.Checked := true
    else
      CbnfRedepFluvial.Checked := false;

    if QcadtabelaFL_NF_DESPACHO.value = 'S' then
      CbnfDespacho.Checked := true
    else
      CbnfDespacho.Checked := false;

    if QcadtabelaFL_NF_ENTREGA_PORTO.value = 'S' then
      CbnfEntPorto.Checked := true
    else
      CbnfEntPorto.Checked := false;

    if QcadtabelaFL_NF_ALFAND.value = 'S' then
      CbnfTaxaAlfandega.Checked := true
    else
      CbnfTaxaAlfandega.Checked := false;

    if QcadtabelaFL_NF_FLUVIAL.value = 'S' then
      CbnfTaxaFluvial.Checked := true
    else
      CbnfTaxaFluvial.Checked := false;

    cbOperacao.ItemIndex := cbOperacao.Items.IndexOf
      (QcadtabelaOPERACAO.AsString);
    cbRegiao.ItemIndex := cbRegiao.Items.IndexOf(QcadtabelaREGIAO.AsString);
    Obs.clear;
    Obs.Lines.add(QcadtabelaOBS.AsString);

    edUser.Text := QcadtabelaUSUARIO.AsString;
    edDtIns.date := QcadtabelaDT_CADASTRO.value;

    edUserA.Text := QcadtabelaCONTROLER.AsString;
    edDtAlt.date := QcadtabelaDT_CONTROLER.value;
    edAgendG.value := Qcadtabelavl_agendg.value;
    edDiaria48.Value := QcadtabelaVL_DIARIA48.Value;



    if QTabelaFL_KM.value = 'S' then
    begin
      JvDBUltimGrid2.Columns[3].Title.Caption := 'Valor KM';
      JvDBUltimGrid2.Columns[4].Title.Caption := 'KM Inicial';
      JvDBUltimGrid2.Columns[5].Title.Caption := 'KM Final';
    end
    else
    begin
      JvDBUltimGrid2.Columns[3].Title.Caption := 'Valor Entrega';
      JvDBUltimGrid2.Columns[4].Title.Caption := 'Peso Inicial';
      JvDBUltimGrid2.Columns[5].Title.Caption := 'Peso Final';
    end;

  end;
  if QTabelafl_aereo.AsString = 'S' then
    PageControl1.Pages[3].TabVisible:=true
  else
    PageControl1.Pages[3].TabVisible:=false;
end;

procedure TfrmCadTabelaVenda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
  QItem.Close;
  QCliente.Close;
  Qcadtabela.Close;
end;

procedure TfrmCadTabelaVenda.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadTabelaVenda.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QTabeladt_controler.IsNull then
  begin
    dbgContrato.canvas.Brush.color := clYellow;
    dbgContrato.canvas.font.color := clRed;
  end;

  if QTabelaFL_STATUS.value = 'N' then
  begin
    dbgContrato.canvas.Brush.color := clRed;
    dbgContrato.canvas.font.color := clWhite;
  end;

  dbgContrato.DefaultDrawDataCell(Rect, dbgContrato.Columns[DataCol]
    .field, State);
end;

procedure TfrmCadTabelaVenda.RBAtivoClick(Sender: TObject);
begin
  // QTabela.Close;
  // QTabela.SQL[12] := 'where t.fl_status = ''S''' ;
  // QTabela.Open;
  QTabela.Close;
  if Pos('fl_status = ', QTabela.sql.Text) > 0 then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('fl_status = ', QTabela.sql.Text) - 1) +
      'fl_status = ''S'' order by cod_venda ';
  //showmessage(qtabela.SQL.Text);
  QTabela.Open;
end;

procedure TfrmCadTabelaVenda.RbInativoClick(Sender: TObject);
begin
  QTabela.Close;
  if Pos('fl_status = ', QTabela.sql.Text) > 0 then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('fl_status = ', QTabela.sql.Text) - 1) +
      'fl_status = ''N'' order by cod_venda ';
  QTabela.Open;
end;

procedure TfrmCadTabelaVenda.RBTodosClick(Sender: TObject);
begin
  QTabela.Close;
  if Pos('fl_status = ', QTabela.sql.Text) > 0 then
    QTabela.sql.Text := copy(QTabela.sql.Text, 1,
      Pos('fl_status = ', QTabela.sql.Text) - 1) +
      'fl_status is not null order by cod_venda ';
  QTabela.Open;
end;

procedure TfrmCadTabelaVenda.LimpadadosGen;
begin
  edpernoiteGen.Text := '0';
  eddiariaGen.Text := '0';
  eddedicadoGen.Text := '0';
  cbveiculogen.ItemIndex := 0;
  edPaleteGen.Value := 0;
end;

procedure TfrmCadTabelaVenda.RestauraGen;
begin

  btinserirgen.Enabled := not btinserirgen.Enabled;
  btalterargen.Enabled := not btalterargen.Enabled;
  btexcluirgen.Enabled := not btexcluirgen.Enabled;
  btsalvargen.Enabled := not btsalvargen.Enabled;
  btcancelagen.Enabled := not btcancelagen.Enabled;
  if Panel8.Visible = true then
    Panel8.Visible := false
  else
    Panel8.Visible := true;
end;

procedure TfrmCadTabelaVenda.Restaura;
begin
  btnNovo.Enabled := not btnNovo.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if edTrans.Visible = true then
  begin
    edTrans.Visible := false;
    cbtra.Visible := true;
  end
  else
  begin
    edTrans.Visible := true;
    cbtra.Visible := false;
  end;

  if edVeiculo.Visible = true then
  begin
    edVeiculo.Visible := false;
    cbVeiculo.Visible := true;
  end
  else
  begin
    edVeiculo.Visible := true;
    cbVeiculo.Visible := false;
  end;
end;

procedure TfrmCadTabelaVenda.RestauraAereo;
begin
  btnAereoI.Enabled := not btnAereoI.Enabled;
  btnAereoA.Enabled := not btnAereoA.Enabled;
  btnAereoE.Enabled := not btnAereoE.Enabled;
  btnAereoS.Enabled := not btnAereoS.Enabled;
  btnAereoC.Enabled := not btnAereoC.Enabled;
  if edDataAereoI.Visible = true then
    edDataAereoI.Visible := false
  else
    edDataAereoI.Visible := true;
  if edDataAereoF.Visible = true then
    edDataAereoF.Visible := false
  else
    edDataAereoF.Visible := true;
  if edAereoColeta.Visible = true then
    edAereoColeta.Visible := false
  else
    edAereoColeta.Visible := true;
  if edPesoAereoI.Visible = true then
    edPesoAereoI.Visible := false
  else
    edPesoAereoI.Visible := true;
  if edPesoAereoF.Visible = true then
    edPesoAereoF.Visible := false
  else
    edPesoAereoF.Visible := true;
  if edAereoEntrega.Visible = true then
    edAereoEntrega.Visible := false
  else
    edAereoEntrega.Visible := true;
  if edAereoExc.Visible = true then
    edAereoExc.Visible := false
  else
    edAereoExc.Visible := true;
  if Panel11.Visible = true then
    Panel11.Visible := false
  else
    Panel11.Visible := true;
end;

procedure TfrmCadTabelaVenda.edDataAereoFExit(Sender: TObject);
begin
  if edDataAereoF.date < edDataAereoI.date then
  begin
    ShowMessage('A Data Final N�o pode ser Menor que a Data Inicial');
    edDataAereoF.SetFocus;
  end;
end;

procedure TfrmCadTabelaVenda.edDataFExit(Sender: TObject);
begin
  if edDataF.date < edDataI.date then
  begin
    ShowMessage('A Data Final N�o pode ser Menor que a Data Inicial');
    edDataF.SetFocus;
  end;
end;

procedure TfrmCadTabelaVenda.edExcedExit(Sender: TObject);
begin
  btnsalvarI.SetFocus;
end;

procedure TfrmCadTabelaVenda.edOutrosChange(Sender: TObject);
begin
  if edOutros.value > 0 then
    edOutrosP.value := 0;
end;

procedure TfrmCadTabelaVenda.edOutrosPChange(Sender: TObject);
begin
  if edOutrosP.value > 0 then
    edOutros.value := 0;
end;

procedure TfrmCadTabelaVenda.edPesoAereoFExit(Sender: TObject);
begin
  if edPesoAereof.Value < edPesoi.value then
  begin
    ShowMessage('O Peso Final N�o pode ser Menor que o Peso Inicial');
    edPesoAereof.SetFocus;
  end;
end;

procedure TfrmCadTabelaVenda.edPesoExcReeExit(Sender: TObject);
begin
  btnSalvarRee.setfocus;
end;

procedure TfrmCadTabelaVenda.edPesofExit(Sender: TObject);
begin
  if edPesof.Value < edPesoi.value then
  begin
    ShowMessage('O Peso Final N�o pode ser Menor que o Peso Inicial');
    edPesof.SetFocus;
  end;
end;

procedure TfrmCadTabelaVenda.edReentregaExit(Sender: TObject);
begin
  if QItemRee.RecordCount > 0 then
  begin
    ShowMessage('J� existe valor para reentrega preenchido, n�o pode ter as duas modalidades !');
    edReentrega.Value := 0;
    exit;
  end;
end;

procedure TfrmCadTabelaVenda.edTDAChange(Sender: TObject);
begin
  if edTDA.value > 0 then
    edTDAp.value := 0;
end;

procedure TfrmCadTabelaVenda.edTDApChange(Sender: TObject);
begin
  if edTDAp.value > 0 then
    edTDA.value := 0;
end;

procedure TfrmCadTabelaVenda.edTDEChange(Sender: TObject);
begin
  if edTDE.value > 0 then
    edTDEp.value := 0;
end;

procedure TfrmCadTabelaVenda.edTDEpChange(Sender: TObject);
begin
  if edTDEp.value > 0 then
    edTDE.value := 0;
end;

procedure TfrmCadTabelaVenda.filtrar;
begin
  Screen.Cursor := crHourGlass;
  QTabela.Close;
  QTabela.sql.clear;
  QTabela.sql.add
    ('select t.cod_venda , f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_cliente, ds_uf, ds_uf_des, veiculo, t.operacao, op.fl_km, t.fl_status, t.vl_frete_minimo, t.vl_ad, dt_controler, op.fl_aereo, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local_des');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add(', max(i.dataf) dataf ');
  QTabela.sql.add
    ('from tb_tabelavenda t inner join cyber.rodcli f on t.cod_cliente = f.codclifor ');
  QTabela.sql.add
    ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''S'' ');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add
      ('left join tb_tabelavenda_item i on i.cod_venda = t.cod_venda ');
  QTabela.sql.add('where f.nomeab is not null and fl_status = ''S'' ');

  if cbCliente.Text <> '' then
  begin
    QTabela.sql.add('and (f.nomeab || '' - '' || f.codcgc) = ' + #39 +
      cbCliente.Text + #39);
  end;
  if cbVeiculoF.Text <> '' then
  begin
    QTabela.sql.add('and t.veiculo = ' + #39 + cbVeiculoF.Text + #39);
  end;
  if cbUFOF.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf = ' + #39 + cbUFOF.Text + #39);
  end;
  if cbUFDF.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf_des = ' + #39 + cbUFDF.Text + #39);
  end;
  if cbOperacaoF.Text <> '' then
  begin
    QTabela.sql.add('and t.operacao = ' + #39 + (cbOperacaoF.Text) + #39);
  end;
  QTabela.sql.add('order by t.cod_venda ');

  //QTabela.sql.savetofile('c:\sim\sql.text');
  QTabela.Open;
  Screen.Cursor := crDefault;
  Qtabela.First;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadTabelaVenda.RestauraI;
begin
  btnInserirI.Enabled := not btnInserirI.Enabled;
  btnAlterarI.Enabled := not btnAlterarI.Enabled;
  btnExcluirI.Enabled := not btnExcluirI.Enabled;
  btnsalvarI.Enabled := not btnsalvarI.Enabled;
  btnCancelarI.Enabled := not btnCancelarI.Enabled;
  if edDataI.Visible = true then
    edDataI.Visible := false
  else
    edDataI.Visible := true;
  if edDataF.Visible = true then
    edDataF.Visible := false
  else
    edDataF.Visible := true;
  if EdValor.Visible = true then
    EdValor.Visible := false
  else
    EdValor.Visible := true;
  if edPesoi.Visible = true then
    edPesoi.Visible := false
  else
    edPesoi.Visible := true;
  if edPesof.Visible = true then
    edPesof.Visible := false
  else
    edPesof.Visible := true;
  if edCtrc.Visible = true then
    edCtrc.Visible := false
  else
    edCtrc.Visible := true;
  if edExced.Visible = true then
    edExced.Visible := false
  else
    edExced.Visible := true;
  if edDiariaF.Visible = true then
    edDiariaF.Visible := false
  else
    edDiariaF.Visible := true;
  if Panel4.Visible = true then
    Panel4.Visible := false
  else
    Panel4.Visible := true;
end;

procedure TfrmCadTabelaVenda.RestauraIRee;
begin
  btnInserirRee.Enabled := not btnInserirree.Enabled;
  btnAlterarRee.Enabled := not btnAlterarree.Enabled;
  btnExcluirree.Enabled := not btnExcluirree.Enabled;
  btnsalvarree.Enabled := not btnsalvarRee.Enabled;
  btnCancelarRee.Enabled := not btnCancelarRee.Enabled;
  if edDataReeI.Visible = true then
    edDataReeI.Visible := false
  else
    edDataReeI.Visible := true;
  if edDataFRee.Visible = true then
    edDataFRee.Visible := false
  else
    edDataFRee.Visible := true;
  if edVMinimoRee.Visible = true then
    edVMinimoRee.Visible := false
  else
    edVMinimoRee.Visible := true;
  if edFreteRee.Visible = true then
    edFreteRee.Visible := false
  else
    edFreteRee.Visible := true;
  if edPesoiRee.Visible = true then
    edPesoiRee.Visible := false
  else
    edPesoiRee.Visible := true;
  if edPesofRee.Visible = true then
    edPesofRee.Visible := false
  else
    edPesofRee.Visible := true;
  if edCte.Visible = true then
    edCte.Visible := false
  else
    edCte.Visible := true;
  if edPesoExcRee.Visible = true then
    edPesoExcRee.Visible := false
  else
    edPesoExcRee.Visible := true;
  if Panel7.Visible = true then
    Panel7.Visible := false
  else
    Panel7.Visible := true;
end;

procedure TfrmCadTabelaVenda.tsAereoHide(Sender: TObject);
begin
  QAereo.Close;
end;

procedure TfrmCadTabelaVenda.tsAereoShow(Sender: TObject);
begin
  QAereo.Close;
  QAereo.Parameters[0].value := QTabelaCOD_VENDA.value;
  QAereo.Open;
end;

procedure TfrmCadTabelaVenda.btnInserirIClick(Sender: TObject);
begin
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  tipo := 'I';
  RestauraI;
  edDataI.SetFocus;
  edDataI.Text := '';
  edDataF.Text := '';
  EdValor.value := 0;
  edPesoi.value := 0;
  edPesof.value := 0;
  edCtrc.value := 0;
  edExced.value := 0;
  edValMin.value := 0;
  edDiaria48.Value := 0;
end;

procedure TfrmCadTabelaVenda.btnInserirReeClick(Sender: TObject);
begin
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if not dtmDados.PodeInserir(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('INSERIR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;

  tipo := 'I';
  RestauraIRee;
  edDataReeI.SetFocus;
  edDataReeI.Text    := '';
  eddataFRee.Text    := '';
  edFreteRee.value   := 0;
  edPesoiRee.value   := 0;
  edPesofRee.value   := 0;
  edCte.value        := 0;
  edPesoExcRee.value := 0;
  edVMinimoRee.value := 0;
end;

procedure TfrmCadTabelaVenda.btnsalvarIClick(Sender: TObject);
var
  cod: Integer;
begin
  { if (EdValor.Value = 0) and (edExced.value = 0) then
    begin
    ShowMessage('Esta tabela n�o tem Valor ?');
    exit;
    end; }
  if QcadtabelaOPERACAO.value = 'SPOT' then
  begin
    if (edDataF.date - edDataI.date) > 3 then
    begin
      ShowMessage
        ('Esta Tabelas SPOT o periodo de videncia n�o pode ser maior que 3 dias !!');
      Exit;
    end;
  end;

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_tabela(:0,:1,:2,:3,:4,''R'',:5) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QTabelaCOD_VENDA.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataI.date;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataF.date;
  dtmDados.iQuery1.Parameters.ParamByName('3').value := edPesoi.value;
  dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesof.value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('5').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('5').value := QItemCOD_TAB.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Data e Peso digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_tabelavenda_item (cod_venda, ' +
        'datai, dataf, vl_rodoviario, nr_peso_de, nr_peso_ate, vr_ctrc, vr_exced, '
        + 'vl_minimo, diaria) values ( ' + ':0, :1, :2, :3, :4, :5, :6, :7, :8, :9)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QTabelaCOD_VENDA.AsInteger;
    end
    else
    begin
      cod := QItemCOD_TAB.AsInteger;
      dtmDados.iQuery1.sql.add('update tb_tabelavenda_item set datai = :1, ' +
        'dataf = :2, vl_rodoviario = :3, nr_peso_de = :4, nr_peso_ate =:5, vr_ctrc = :6, '
        + 'vr_exced = :7, vl_minimo = :8, diaria = :9 where cod_tab = :10');
      dtmDados.iQuery1.Parameters.ParamByName('10').value :=
        QItemCOD_TAB.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataI.date;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataF.date;
    dtmDados.iQuery1.Parameters.ParamByName('3').value := EdValor.value;
    dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoi.value;
    dtmDados.iQuery1.Parameters.ParamByName('5').value := edPesof.value;
    dtmDados.iQuery1.Parameters.ParamByName('6').value := edCtrc.value;
    dtmDados.iQuery1.Parameters.ParamByName('7').value := edExced.value;
    dtmDados.iQuery1.Parameters.ParamByName('8').value := edValMin.value;
    dtmDados.iQuery1.Parameters.ParamByName('9').value := edDiariaF.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_tabelavenda set controler = null, dt_controler = null ');
    dtmDados.iQuery1.sql.add('where cod_venda = :0 ');
    dtmDados.iQuery1.Parameters[0].value := QItemCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QItem.Close;
  QItem.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
  QItem.Open;
  if tipo = 'A' then
    QItem.Locate('cod_tab', cod, [])
  else
    QItem.Last;
  tipo := '';
  RestauraI;
end;

procedure TfrmCadTabelaVenda.btnSalvarReeClick(Sender: TObject);
var
  cod: Integer;
begin
  if edReentrega.Value > 0 then
  begin
    ShowMessage('J� existe valor para reentrega preenchido, n�o pode ter as duas modalidades !');
    exit;
  end;

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_tabela_ree(:0,:1,:2,:3,:4,''R'',:5) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QTabelaCOD_VENDA.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataReeI.date;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataFRee.date;
  dtmDados.iQuery1.Parameters.ParamByName('3').value := edPesoiRee.value;
  dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesofRee.value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('5').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('5').value := QItemReeCOD_TAB.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Data e Peso digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_tabelavenda_item_ree (cod_venda, ' +
        'datai, dataf, vl_frete, nr_peso_de, nr_peso_ate, vr_cte, vr_exced, '
        + 'vl_minimo) values ( ' + ':0, :1, :2, :3, :4, :5, :6, :7, :8)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QTabelaCOD_VENDA.AsInteger;
    end
    else
    begin
      cod := QItemCOD_TAB.AsInteger;
      dtmDados.iQuery1.sql.add('update tb_tabelavenda_item_ree set datai = :1, ' +
        'dataf = :2, vl_frete = :3, nr_peso_de = :4, nr_peso_ate =:5, vr_cte = :6, '
        + 'vr_exced = :7, vl_minimo = :8 where cod_tab = :9');
      dtmDados.iQuery1.Parameters.ParamByName('9').value :=
        QItemReeCOD_TAB.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edDataReeI.date;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edDataFRee.date;
    dtmDados.iQuery1.Parameters.ParamByName('3').value := EdFreteRee.value;
    dtmDados.iQuery1.Parameters.ParamByName('4').value := edPesoiRee.value;
    dtmDados.iQuery1.Parameters.ParamByName('5').value := edPesofRee.value;
    dtmDados.iQuery1.Parameters.ParamByName('6').value := edCte.value;
    dtmDados.iQuery1.Parameters.ParamByName('7').value := edPesoExcRee.value;
    dtmDados.iQuery1.Parameters.ParamByName('8').value := edVMinimoRee.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_tabelavenda set controler = null, dt_controler = null ');
    dtmDados.iQuery1.sql.add('where cod_venda = :0 ');
    dtmDados.iQuery1.Parameters[0].value := QItemReeCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QItemRee.Close;
  QItemRee.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
  QItemRee.Open;
  if tipo = 'A' then
    QItemRee.Locate('cod_tab', cod, [])
  else
    QItemRee.Last;
  tipo := '';
  RestauraIRee;
end;

procedure TfrmCadTabelaVenda.btnVerClick(Sender: TObject);
var
  caminho: string;
begin
  if QcadtabelaDOC_CONTROLER.AsString <> '' then
  begin
    caminho := GLBControler + QcadtabelaDOC_CONTROLER.AsString;
    ShellExecute(Handle, nil, Pchar(caminho), nil, nil, SW_SHOWNORMAL);
  end
  else
    ShowMessage('Tabela Sem Documenta��o Vinculada !!');
end;

procedure TfrmCadTabelaVenda.btpostgenClick(Sender: TObject);
begin
  // Implementa��o do projeto generalidades, salva a edi��o na tabela tb_tabelavenda
  if status = 'A' then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_tabelavenda set vl_pallet =:0 , vl_ajuda =:1, vl_reentrega =:2, vl_armazenagem =:3,');
    dtmDados.iQuery1.sql.add
      (' vl_hora_parada =:4, VL_CANHOTO =:5, vl_agendg = :6, vl_diaria48 = :7, VL_REENT_MINIMA = :8 ');
    dtmDados.iQuery1.sql.add('where cod_venda = :9');
    dtmDados.iQuery1.Parameters[0].value := edpallet.value;
    dtmDados.iQuery1.Parameters[1].value := edAjuda.value;
    dtmDados.iQuery1.Parameters[2].value := edReentrega.value;
    dtmDados.iQuery1.Parameters[3].value := edArmazem.value;
    dtmDados.iQuery1.Parameters[4].value := edHoraParada.value;
    dtmDados.iQuery1.Parameters[5].value := edcanhoto.value;
    dtmDados.iQuery1.Parameters[6].value := edAgendG.value;
    dtmDados.iQuery1.Parameters[7].value := edDiaria48.value;
    dtmDados.iQuery1.Parameters[8].value := edRMinimo.value;
    dtmDados.iQuery1.Parameters[9].value := QTabelaCOD_VENDA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
  end;

  status := '';

  btnAlterarGen.Enabled := not btnAlterarGen.Enabled;
  btpostgen.Enabled := not btpostgen.Enabled;
  btcancelgen.Enabled := not btcancelgen.Enabled;

end;

procedure TfrmCadTabelaVenda.btProcurarClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QTabela.Close;
  QTabela.sql.clear;
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add('select * from( ');
  QTabela.sql.add
    ('select t.cod_venda , f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_cliente, ds_uf, ds_uf_des, veiculo, t.operacao, op.fl_km, t.fl_status, t.vl_frete_minimo, t.vl_ad, dt_controler, op.fl_aereo, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local, ');
  QTabela.sql.add
    ('CASE WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ELSE ''INTERIOR'' end as local_des');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add(', max(i.dataf) dataf ');
  QTabela.sql.add
    ('from tb_tabelavenda t inner join cyber.rodcli f on t.cod_cliente = f.codclifor ');
  QTabela.sql.add
    ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''S'' ');
  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add
      ('left join tb_tabelavenda_item i on i.cod_venda = t.cod_venda ');
  QTabela.sql.add('where f.nomeab is not null');

  if cbPesqTransp.Text <> '' then
  begin
    QTabela.sql.add('and (f.nomeab || '' - '' || f.codcgc) = ' + #39 +
      cbPesqTransp.Text + #39);
  end;
  if cbVeiculoPesq.Text <> '' then
  begin
    QTabela.sql.add('and t.veiculo = ' + #39 + cbVeiculoPesq.Text + #39);
  end;
  if cbPesqUFO.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf = ' + #39 + cbPesqUFO.Text + #39);
  end;
  if cbPesqUFD.Text <> '' then
  begin
    QTabela.sql.add('and t.ds_uf_des = ' + #39 + cbPesqUFD.Text + #39);
  end;
  if cbPesqLO.Text <> '' then
  begin
    QTabela.sql.add('and t.fl_local = ' + #39 + copy(cbPesqLO.Text, 1,
      1) + #39);
    if cbCidOPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidOPesq.Text, []);
      QTabela.sql.add('and t.codmuno = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbPesqLD.Text <> '' then
  begin
    QTabela.sql.add('and t.fl_local_des = ' + #39 + copy(cbPesqLD.Text, 1,
      1) + #39);
    if cbCidDPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidDPesq.Text, []);
      QTabela.sql.add('and t.codmun = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbOperacaoPesq.Text <> '' then
  begin
    QTabela.sql.add('and t.operacao = ' + #39 + (cbOperacaoPesq.Text) + #39);
  end;

  if RGFiltro.ItemIndex = 1 then
    QTabela.sql.add('and t.dt_controler is null and t.fl_status = ''S'' ')
  else if RGFiltro.ItemIndex = 2 then
    QTabela.sql.add('and t.reprovado is not null ');

  if RGFiltro.ItemIndex = 3 then
    QTabela.sql.add
      ('group by t.cod_venda , f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf), ds_uf, ds_uf_des, vl_frete_minimo, vl_ad, veiculo, t.fl_local, t.fl_local_des, op.fl_km, t.operacao, t.fl_status, dt_controler, op.fl_aereo ');


  if RGFiltro.ItemIndex = 3 then
  begin
    QTabela.sql.add(') where trunc(dataf) < trunc(sysdate)');
  end;

  if RBAtivo.Checked = true then
    QTabela.sql.add('and fl_status = ''S'' ')
  else if RBInAtivo.Checked = true then
    QTabela.sql.add('and fl_status = ''N'' ');
  QTabela.sql.add('order by t.cod_venda ');

  //QTabela.sql.savetofile('c:\sim\sql.text');
  QTabela.Open;
  Screen.Cursor := crDefault;
  if QTabela.Eof then
  begin
    ShowMessage('N�o Encontrado Nenhum Resultado Para Esta Pesquisa !!');
    filtro := 0;
    btnExcel.Visible := false;
  end
  else
  begin
    PageControl1.ActivePageIndex := 0;
    filtro := 1;
    btnExcel.Visible := true;
  end;
end;

procedure TfrmCadTabelaVenda.btRetirarFiltroClick(Sender: TObject);
begin
  cbPesqTransp.ItemIndex := -1;
  cbPesqUFO.ItemIndex := -1;
  cbPesqLO.ItemIndex := -1;
  cbPesqUFD.ItemIndex := -1;
  cbPesqLD.ItemIndex := -1;
  QTabela.Close;
  QTabela.sql.Text := sql;
  QTabela.Open;
  PageControl1.ActivePageIndex := 0;
  sql := '';
  filtro := 0;
  btnExcel.Visible := false;
end;

procedure TfrmCadTabelaVenda.btsalvargenClick(Sender: TObject);
begin
  // Implementa��o do projeto generalidades, salva a edi��o e insert na tabela generalidades
  if (status = 'I') AND ((edpernoiteGen.Text <> '') or (eddiariaGen.Text <> '')
    or (eddedicadoGen.Text <> '')) then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('insert into tb_tabelavenda_generalidades ( cod_VENDA,vl_pernoite,vl_vd,vl_diaria,veiculo, vl_palete)');
    dtmDados.iQuery1.sql.add(' values ( :0, :1, :2, :3, :4, :5)');
    dtmDados.iQuery1.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
    dtmDados.iQuery1.Parameters[1].value := edpernoiteGen.Value;
    dtmDados.iQuery1.Parameters[2].value := eddedicadoGen.value;
    dtmDados.iQuery1.Parameters[3].value := eddiariaGen.value;
    dtmDados.iQuery1.Parameters[4].value := cbveiculogen.Text;
    dtmDados.iQuery1.Parameters[5].value := edPaleteGen.value;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end
  else if (status = 'A') AND ((edpernoiteGen.Text <> '') or
    (eddiariaGen.Text <> '') or (eddedicadoGen.Text <> '')) then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('update tb_tabelavenda_generalidades set vl_pernoite =:0');
    dtmDados.iQuery1.sql.add
      (',vl_vd =:1,vl_diaria =:2,veiculo =:3, vl_palete = :4 where cod_id =:5');
    dtmDados.iQuery1.Parameters[0].value := edpernoiteGen.Value;
    dtmDados.iQuery1.Parameters[1].value := eddedicadoGen.value;
    dtmDados.iQuery1.Parameters[2].value := eddiariaGen.value;
    dtmDados.iQuery1.Parameters[3].value := cbveiculogen.Text;
    dtmDados.iQuery1.Parameters[4].value := edPaleteGen.value;
    dtmDados.iQuery1.Parameters[5].value := QGenItemCOD_ID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QGenItem.Close;
    QGenItem.Open;
  end;

  status := '';
  RestauraGen;
  LimpadadosGen;
end;

procedure TfrmCadTabelaVenda.cbalfandClick(Sender: TObject);
begin
  if CbnfTaxaAlfandega.Checked then
    cbalfand.Checked := false
end;

procedure TfrmCadTabelaVenda.cbClienteChange(Sender: TObject);
begin
  filtrar;
end;

procedure TfrmCadTabelaVenda.cbCteClick(Sender: TObject);
begin
  if CbnftaxaCte.Checked then
    cbCte.Checked := false
end;

procedure TfrmCadTabelaVenda.cbdespachoClick(Sender: TObject);
begin
  if CbnfDespacho.Checked then
    cbdespacho.Checked := false
end;

procedure TfrmCadTabelaVenda.cbentportClick(Sender: TObject);
begin
  if CbnfEntPorto.Checked then
    cbentport.Checked := false
end;

procedure TfrmCadTabelaVenda.cbexcedenteClick(Sender: TObject);
begin
  if CbnfExcedente.Checked then
    cbexcedente.Checked := false
end;

procedure TfrmCadTabelaVenda.cbfluvialClick(Sender: TObject);
begin
  if CbnfTaxaFluvial.Checked then
    cbfluvial.Checked := false
end;

procedure TfrmCadTabelaVenda.cbfreteClick(Sender: TObject);
begin
  if CbnfFrete.Checked then
    cbfrete.Checked := false
end;

procedure TfrmCadTabelaVenda.cbgrisClick(Sender: TObject);
begin
  if CbnfGris.Checked then
    cbgris.Checked := false
end;

procedure TfrmCadTabelaVenda.cbLdChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbLd.Text, 1, 1));
  QCidade.Open;
  cbCidade.clear;
  cbCidade.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidade.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.CBLOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := CBUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(CBLO.Text, 1, 1));
  QCidade.Open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.CbnfDespachoClick(Sender: TObject);
begin
  if cbdespacho.Checked then
    CbnfDespacho.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfEntPortoClick(Sender: TObject);
begin
  if cbentport.Checked then
    CbnfEntPorto.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfExcedenteClick(Sender: TObject);
begin
  if cbexcedente.Checked then
    CbnfExcedente.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfFreteClick(Sender: TObject);
begin
  if cbfrete.Checked then
    CbnfFrete.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfGrisClick(Sender: TObject);
begin
  if cbgris.Checked then
    CbnfGris.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfPedagioClick(Sender: TObject);
begin
  if cbpedagio.Checked then
    CbnfPedagio.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfRedepFluvialClick(Sender: TObject);
begin
  if cbredepFluvial.Checked then
    CbnfRedepFluvial.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfSegBalsaClick(Sender: TObject);
begin
  if cbsegBalsa.Checked then
    CbnfSegBalsa.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfSeguroClick(Sender: TObject);
begin
  if cbseguro.Checked then
    CbnfSeguro.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfTasClick(Sender: TObject);
begin
  if cbtas.Checked then
    CbnfTas.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfTaxaAlfandegaClick(Sender: TObject);
begin
  if cbalfand.Checked then
    CbnfTaxaAlfandega.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnftaxaCteClick(Sender: TObject);
begin
  if cbCte.Checked then
    CbnftaxaCte.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfTaxaFluvialClick(Sender: TObject);
begin
  if cbfluvial.Checked then
    CbnfTaxaFluvial.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfTdeClick(Sender: TObject);
begin
  if cbtde.Checked then
    CbnfTde.Checked := false
end;

procedure TfrmCadTabelaVenda.CbnfTrClick(Sender: TObject);
begin
  if cbtr.Checked then
    CbnfTr.Checked := false
end;

procedure TfrmCadTabelaVenda.cbOperacaoFChange(Sender: TObject);
begin
  if cbOperacaoF.Text <> '' then
    filtrar;
end;

procedure TfrmCadTabelaVenda.cbpedagioClick(Sender: TObject);
begin
  if CbnfPedagio.Checked then
    cbpedagio.Checked := false
end;

procedure TfrmCadTabelaVenda.cbPesqLDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLD.Text, 1, 1));
  QCidade.Open;
  cbCidDPesq.clear;
  cbCidDPesq.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidDPesq.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.cbPesqLOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLO.Text, 1, 1));
  QCidade.Open;
  cbCidOPesq.clear;
  cbCidOPesq.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidOPesq.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.cbredepFluvialClick(Sender: TObject);
begin
  if CbnfRedepFluvial.Checked then
    cbredepFluvial.Checked := false
end;

procedure TfrmCadTabelaVenda.cbsegBalsaClick(Sender: TObject);
begin
  if CbnfSegBalsa.Checked then
    cbsegBalsa.Checked := false
end;

procedure TfrmCadTabelaVenda.cbseguroClick(Sender: TObject);
begin
  if CbnfSeguro.Checked then
    cbseguro.Checked := false
end;

procedure TfrmCadTabelaVenda.cbtasClick(Sender: TObject);
begin
  if CbnfTas.Checked then
    cbtas.Checked := false
end;

procedure TfrmCadTabelaVenda.cbtdeClick(Sender: TObject);
begin
  if CbnfTde.Checked then
    cbtde.Checked := false
end;

procedure TfrmCadTabelaVenda.cbtrClick(Sender: TObject);
begin
  if CbnfTr.Checked then
    cbtr.Checked := false
end;

procedure TfrmCadTabelaVenda.cbUFDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbLd.Text, 1, 1));
  QCidade.Open;
  cbCidade.clear;
  cbCidade.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidade.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.cbUFDFChange(Sender: TObject);
begin
  filtrar;
end;

procedure TfrmCadTabelaVenda.CBUFOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := CBUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(CBLO.Text, 1, 1));
  QCidade.Open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda.cbUFOFChange(Sender: TObject);
begin
  filtrar;
end;

procedure TfrmCadTabelaVenda.cbVeiculoFChange(Sender: TObject);
begin
  filtrar;
end;

procedure TfrmCadTabelaVenda.btnAlterarIClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.ALTERAR FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if QItemDATAF.value < date() then
    Exit;
  tipo := 'A';
  edDataI.date := QItemDATAI.value;
  edDataF.date := QItemDATAF.value;
  EdValor.value := QItemVL_RODOVIARIO.value;
  edPesoi.value := QItemNR_PESO_DE.value;
  edPesof.value := QItemNR_PESO_ATE.value;
  edCtrc.value := QItemVR_CTRC.value;
  edExced.value := QItemVR_EXCED.value;
  edValMin.value := QItemVL_MINIMO.value;
  edDiariaF.value := QItemDiaria.value;
  RestauraI;
  edDataI.SetFocus;
end;

procedure TfrmCadTabelaVenda.btnAlterarReeClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
  begin
    // N�o pode acessar tabelas normais, verifica se pode SPOT
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('SELECT A.inserir FROM tb_PERMISSAO_sis A, tb_TELA_sis T WHERE upper(T.NAME) = upper(''ReceitaSPOT'') AND T.IDTELA = A.IDTELA '+
      'AND A.COD_usuario= '+ #39 + IntToStr(glbcoduser) + #39);
    dtmDados.iQuery1.Open;
    spot := dtmDados.iQuery1.FieldByName('ALTERAR').AsInteger;
    dtmDados.iQuery1.Close;
    if spot = 0 then
      Exit;
  end;
  if QcadtabelaCONTROLER.value <> '' then
    Exit;
  if QItemReeDATAF.value < date() then
    Exit;
  tipo := 'A';
  RestauraIRee;
  edDataReeI.SetFocus;
  edDataReeI.date    := QItemReeDATAI.value;
  eddataFRee.date    := QItemReeDATAF.value;
  edFreteRee.value   := QItemReeVL_FRETE.value;
  edPesoiRee.value   := QItemReeNR_PESO_DE.value;
  edPesofRee.value   := QItemReeNR_PESO_ATE.value;
  edCte.value        := QItemReeVR_CTE.value;
  edPesoExcRee.value := QItemReeVR_EXCED.value;
  edVMinimoRee.value := QItemReeVL_MINIMO.value;

end;

procedure TfrmCadTabelaVenda.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
