unit RelSIMxRodopar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls,
  JvExGrids, JvStringGrid;

type
  TfrmRelSIMxRodopar = class(TForm)
    QRodopar: TADOQuery;
    navnavig: TDataSource;
    btRelatorio: TBitBtn;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    JvDBNavigator1: TJvDBNavigator;
    QRodoparNR_CONHECIMENTO: TBCDField;
    QRodoparFL_EMPRESA: TBCDField;
    QRodoparVL_TOTAL: TFloatField;
    QRodoparCODCON: TBCDField;
    QRodoparTOTFRE: TBCDField;
    QRodoparDT_CONHECIMENTO: TDateTimeField;
    QRodoparNOMEAB: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelSIMxRodopar: TfrmRelSIMxRodopar;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelSIMxRodopar.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_SIM_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QRodopar.First;
  Gauge1.MaxValue := QRodopar.RecordCount;
  while not QRodopar.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QRodopar.fieldbyname
        (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QRodopar.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmRelSIMxRodopar.btRelatorioClick(Sender: TObject);
var
  d1, d2: string;
begin
  d1 := formatdatetime('dd/mm/yy', dtInicial.date) + ' 00:00:01';
  d2 := formatdatetime('dd/mm/yy', dtFinal.date) + ' 23:59:59';
  Panel1.Visible := true;
  Application.ProcessMessages;
  QRodopar.close;
  QRodopar.SQL[3] := 'Where sim.dt_conhecimento between to_date(' + #39 + d1 +
    #39 + ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + d2 + #39 +
    ',''dd/mm/yy hh24:mi:ss'')';
  QRodopar.open;
  Panel1.Visible := false;
end;

procedure TfrmRelSIMxRodopar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QRodopar.close;
end;

procedure TfrmRelSIMxRodopar.FormCreate(Sender: TObject);
begin
  dtInicial.date := date - 1;
  dtFinal.date := date;
end;

procedure TfrmRelSIMxRodopar.JvDBGrid1TitleClick(Column: TColumn);
begin
  if Pos('order by', QRodopar.SQL.Text) > 0 then
    QRodopar.SQL.Text := Copy(QRodopar.SQL.Text, 1,
      Pos('order by', QRodopar.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QRodopar.SQL.Text := QRodopar.SQL.Text + ' order by ' + Column.FieldName;
  QRodopar.open;
end;

end.
