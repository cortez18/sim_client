unit PortariaSai;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, Buttons, JvGradient, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExControls, JvEnterTab, ImgList,
  JvMaskEdit, DBCtrls, JvExStdCtrls, JvCombobox, REST.types,
  RLRichText, RLReport, DateUtils, IniFiles, System.ImageList, IPPeerClient,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, System.JSON;

type
  TfrmPortariaSai = class(TForm)
    Panel3: TPanel;
    JvGradient1: TJvGradient;
    Label1: TLabel;
    btnGerar: TBitBtn;
    Panel2: TPanel;
    QSaida1: TADOQuery;
    iml: TImageList;
    Label11: TLabel;
    Label4: TLabel;
    QTransp: TADOQuery;
    btnImprimir: TBitBtn;
    Qmanifesto: TADOQuery;
    dtsManifesto: TDataSource;
    lblqt: TLabel;
    Label7: TLabel;
    edKM: TJvCalcEdit;
    Label2: TLabel;
    edPlaca: TJvMaskEdit;
    Label6: TLabel;
    edTransp: TJvMaskEdit;
    edMotor: TJvMaskEdit;
    Label8: TLabel;
    Label9: TLabel;
    edCfp: TJvMaskEdit;
    edRG: TJvMaskEdit;
    edCNH: TJvMaskEdit;
    Label10: TLabel;
    Memo1: TMemo;
    SPSaida: TADOStoredProc;
    Label13: TLabel;
    cbTipo: TComboBox;
    Label5: TLabel;
    edFrota: TJvMaskEdit;
    Label3: TLabel;
    edManifesto: TJvCalcEdit;
    Panel1: TPanel;
    dbgManifesto: TJvDBUltimGrid;
    mdTemp: TJvMemoryData;
    mdTempManifesto: TIntegerField;
    mdTempInserida: TIntegerField;
    mdTempreg: TIntegerField;
    dsTemp: TDataSource;
    lblKm: TLabel;
    edPlaca1: TJvMaskEdit;
    QmanifestoNR_ROMANEIO: TBCDField;
    QmanifestoMOTORISTA: TStringField;
    QmanifestoPLACA: TStringField;
    QmanifestoVEICULO: TStringField;
    QmanifestoDTINC: TDateTimeField;
    QmanifestoTRANSPORTADORA: TStringField;
    QTranspCODIGO: TStringField;
    QSaida1REG: TBCDField;
    QSaida1TIPO: TStringField;
    QSaida1FL_EMPRESA: TBCDField;
    QSaida1PLACA: TStringField;
    QSaida1COD_TRANSP: TBCDField;
    QSaida1COD_MOTORISTA: TBCDField;
    QSaida1DATA: TDateTimeField;
    QSaida1KM: TBCDField;
    QSaida1MANIFESTO: TBCDField;
    QSaida1OBS: TStringField;
    QSaida1USUARIO: TStringField;
    QSaida1MOTIVO: TStringField;
    QSaida1NM_TRANSP: TStringField;
    QSaida1NM_MOTOR: TStringField;
    QSaida1NR_CPF: TStringField;
    QSaida1NR_RG: TStringField;
    QSaida1NR_CNH: TStringField;
    QSaida1DT_SAIDA: TDateTimeField;
    QMan: TADOQuery;
    dtsMan: TDataSource;
    QManNR_ROMANEIO: TBCDField;
    QManMOTORISTA: TStringField;
    QManPLACA: TStringField;
    QManVEICULO: TStringField;
    QManDTINC: TDateTimeField;
    QManTRANSPORTADORA: TStringField;
    QPallet: TADOQuery;
    QSMS: TADOQuery;
    QPalletCODEAN: TStringField;
    QEnvia: TADOQuery;
    QEnviaNM_EMP_DEST_TA: TStringField;
    QEnviaNR_QUANTIDADE: TFloatField;
    QEnviaNR_NF: TStringField;
    QEnviaFL_TEMPO_PER: TStringField;
    QEnviaDS_EMAIL: TStringField;
    mdTempserie: TStringField;
    QManSERMAN: TStringField;
    mdTempvalido: TStringField;
    QManMDFEAUT: TStringField;
    QPalletSERIE: TStringField;
    QPalletCODMAN: TBCDField;
    ADOQPallet_CTE: TADOQuery;
    ADOQPallet_CTECODCGC: TStringField;
    Sp_SaidaSMS: TADOStoredProc;
    QArquivoBaruel: TADOQuery;
    QArquivoBaruelNOTFIS: TStringField;
    QArquivoBaruelSERIEN: TStringField;
    QArquivoBaruelCODCGC: TStringField;
    QArquivoBaruelDATAINC: TDateTimeField;
    QArquivoBaruelOPERACAO: TStringField;
    QArquivoBaruelNR_MANIFESTO: TBCDField;
    QArquivoBaruelSERIE: TStringField;
    QArquivoBaruelR: TADOQuery;
    Qverificaromaneio: TADOQuery;
    QUpdateRomaneio: TADOQuery;
    QArquivoBaruelRNR_ROMANEIO: TBCDField;
    QArquivoBaruelRNR_AUF: TStringField;
    QArquivoBaruelRNR_AUF_TA: TIntegerField;
    QArquivoBaruelRNAME: TStringField;
    QArquivoBaruelRSUCHBEGRIFF: TStringField;
    ADOQPallet_CTECODCLIFOR: TBCDField;
    QAverba: TADOQuery;
    QAverbaCTE_CHAVE: TStringField;
    QAverbaDT_CONHECIMENTO: TDateTimeField;
    QAverbaCOD_CONHECIMENTO: TBCDField;
    QAverbaFL_EMPRESA: TBCDField;
    QXML: TADOQuery;
    QXMLCTE_CHAVE: TStringField;
    QXMLDT_EMISSAO: TDateTimeField;
    Qemail: TADOQuery;
    QemailEMAIL: TStringField;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    QSMSCODCGC: TStringField;
    QSMSUSUARIO: TStringField;
    QSMSPEDIDO: TFMTBCDField;
    QSMSNR_CTRC: TFMTBCDField;
    QSMSSERIE: TStringField;
    QSMSDESTINO: TStringField;
    QSMSENDERE: TStringField;
    QSMSNUMERO: TFMTBCDField;
    QSMSCODCEP: TStringField;
    QSMSTELCEL: TStringField;
    QSMSREG: TFMTBCDField;
    QPortal: TADOQuery;
    QPortalCODCGC: TStringField;
    QPortalUSUARIO: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnGerarClick(Sender: TObject);
    procedure edManifestoExit(Sender: TObject);
    procedure cbTipoExit(Sender: TObject);
    procedure edPlaca1Exit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbgManifestoCellClick(Column: TColumn);
    procedure dbgManifestoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure verificafrota(placa: string);
    procedure edKMExit(Sender: TObject);
    procedure edMotorExit(Sender: TObject);
    procedure QManBeforeOpen(DataSet: TDataSet);
    procedure enviar_email;
    procedure QmanifestoBeforeOpen(DataSet: TDataSet);
    // procedure Button1Click(Sender: TObject);
  private
    manifestos: string;
  public
    { Public declarations }
  end;

var
  frmPortariaSai: TfrmPortariaSai;
  reg: Integer;
  arq: TIniFile;
  localarq: string;

implementation

uses Dados, menu, funcoes, uMensagensSms, Controle_pallet, ATM,
  Portariasai_impressao;

{$R *.dfm}

procedure TfrmPortariaSai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // md.Close;
  Action := caFree;
  frmPortariaSai := nil;
  QSaida1.close;
  QTransp.close;
  Qmanifesto.close;
end;

procedure TfrmPortariaSai.FormCreate(Sender: TObject);
begin
  cbTipo.Clear;
  cbTipo.Items.add('');
  cbTipo.Items.add('Romaneio');
  cbTipo.Items.add('Vazio');
  cbTipo.Items.add('Embalagem');
  cbTipo.Items.add('Devolu��o');
  cbTipo.Items.add('Manifesto');
  cbTipo.Items.add('Coleta/Devolu��o');

  arq := TIniFile.Create(ExtractFilePath(Application.exeName) + '\SIM.ini');
  localarq := arq.ReadString('ARQ', 'caminho', '');
end;

procedure TfrmPortariaSai.QManBeforeOpen(DataSet: TDataSet);
begin
  QMan.Parameters[0].value := glbFilial;
end;

procedure TfrmPortariaSai.QmanifestoBeforeOpen(DataSet: TDataSet);
begin
  if glbFilial = 1 then
  begin
    Qmanifesto.Parameters[0].value := 'MAR';
    Qmanifesto.Parameters[2].value := 'MAR';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 4 then
  begin
    Qmanifesto.Parameters[0].value := 'PBA';
    Qmanifesto.Parameters[2].value := 'PBA';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 16 then
  begin
    Qmanifesto.Parameters[0].value := 'MAR';
    Qmanifesto.Parameters[2].value := 'MAR';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 12 then
  begin
    Qmanifesto.Parameters[0].value := 'PBA';
    Qmanifesto.Parameters[2].value := 'PBA';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 9 then
  begin
    Qmanifesto.Parameters[0].value := 'BTM';
    Qmanifesto.Parameters[2].value := 'BTM';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 20 then
  begin
    Qmanifesto.Parameters[0].value := 'PBA';
    Qmanifesto.Parameters[2].value := 'PBA';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end
  else if glbFilial = 21 then
  begin
    Qmanifesto.Parameters[0].value := 'ETM';
    Qmanifesto.Parameters[2].value := 'ETM';
    Qmanifesto.Parameters[1].value := glbFilial;
    Qmanifesto.Parameters[3].value := glbFilial;
  end;
end;


procedure TfrmPortariaSai.verificafrota(placa: string);
begin
  {dtmdados.WQuery1.close;
  dtmdados.WQuery1.sql.Clear;
  dtmdados.WQuery1.sql.add
    ('select p.* from tb_veiculo v left join tb_veiculo_pneu p on v.nr_veiculo = p.veiculo where p.pneu is null and v.nr_placa = :0');
  dtmdados.WQuery1.Parameters[0].value := placa;
  dtmdados.WQuery1.open;
  if not dtmdados.WQuery1.Eof then
  begin
    ShowMessage('Para esta frota n�o foram cadastrados todos os Pneus');
  end;  }
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.add
    ('select km from tb_portaria where motivo = ''E'' and dt_saida is null and placa = :1 ');
  dtmdados.IQuery1.Parameters[0].value := placa;
  dtmdados.IQuery1.open;
  if not dtmdados.IQuery1.Eof then
    lblKm.Caption := dtmdados.IQuery1.FieldByName('km').AsString;
  dtmdados.IQuery1.close;
end;

procedure TfrmPortariaSai.btnGerarClick(Sender: TObject);
var
  protocolo, data, hora, sData, sCaminho, resultado, rec: String;
  inic, fim, a: Integer;
  arquivo, para, arqxml, r: String;
  mensagem, xx, cc, cnpj : TStrings;
  MS: TMemoryStream;
  i, f : Integer;
  jv: TJSONValue;
  jsonObj: TJSONObject;
  men, retorno : String;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  manifestos := '';

  if cbTipo.Text = '' then
  begin
    ShowMessage('N�o foi Escolhido o Tipo de Sa�da !!');
    Panel3.SetFocus;
    cbTipo.SetFocus;
    Exit;
  end;

  if edPlaca.Text = '' then
  begin
    ShowMessage('N�o foi Digitado a Placa !!');
    Panel3.SetFocus;
    edPlaca1.SetFocus;
    Exit;
  end;

  {if (edKM.value = 0) and (edKM.Visible = true) then
  begin
    ShowMessage('Informe a kilometragem do ve�culo');
    Panel2.SetFocus;
    edKM.SetFocus;
    Exit;
  end; }

  if Application.Messagebox('Voc� Deseja Gerar Esta Sa�da ?', 'Gerar Sa�da',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if (cbTipo.Text = 'Manifesto') then
    begin
      // verifica se tem pallet a controlar
      QPallet.close;
      QPallet.Parameters[0].value := glbFilial;
      QPallet.Parameters[1].value := glbFilial;
      QPallet.Parameters[2].value := QManNR_ROMANEIO.AsInteger;
      QPallet.Parameters[3].value := QManSERMAN.value;
      QPallet.Parameters[4].value := 'M';
      QPallet.open;

      // procura os CT-es do Manifesto selecionado (Implementa��o Hugo)
      ADOQPallet_CTE.close;
      ADOQPallet_CTE.Parameters[0].value :=  glbFilial;
      ADOQPallet_CTE.Parameters[1].value :=  glbFilial;
      ADOQPallet_CTE.Parameters[2].value := QManNR_ROMANEIO.AsInteger;
      ADOQPallet_CTE.Parameters[3].value := QManSERMAN.value;
      ADOQPallet_CTE.Parameters[4].value := 'M';
      ADOQPallet_CTE.open;

      if not QPalletCODEAN.IsNull then
      begin
        frmMenu.Tag := 2;
        try
          Application.CreateForm(TfrmControle_pallet, frmControle_pallet);
          // Carregar o combobox com os CT-es do manifesto selecionado (implementa��o Hugo)
          while not frmPortariaSai.ADOQPallet_CTE.Eof do
          begin
            frmControle_pallet.cbcte.Items.add
              (frmPortariaSai.ADOQPallet_CTECODCGC.AsString);
            ADOQPallet_CTE.Next;
          end;
          frmControle_pallet.ShowModal;
        finally
          frmControle_pallet.Free;
        end;
        frmMenu.Tag := 0;
      end;
    end
    else if (cbTipo.Text = 'Romaneio') then
    begin
      // verifica se tem pallet a controlar
      QPallet.close;
      QPallet.Parameters[0].value := glbFilial;
      QPallet.Parameters[1].value := glbFilial;
      QPallet.Parameters[2].value := QmanifestoNR_ROMANEIO.AsInteger;
      QPallet.Parameters[3].value := '0';
      QPallet.Parameters[4].value := 'R';
      QPallet.open;

      // procura os CT-es do Manifesto selecionado (Implementa��o Hugo)
      ADOQPallet_CTE.close;
      ADOQPallet_CTE.Parameters[0].value :=  glbFilial;
      ADOQPallet_CTE.Parameters[1].value :=  glbFilial;
      ADOQPallet_CTE.Parameters[2].value := QmanifestoNR_ROMANEIO.AsInteger;
      ADOQPallet_CTE.Parameters[3].value := '0';
      ADOQPallet_CTE.Parameters[4].value := 'R';
      ADOQPallet_CTE.open;

      if not QPalletCODEAN.IsNull then
      begin
        frmMenu.Tag := 2;
        try
          Application.CreateForm(TfrmControle_pallet, frmControle_pallet);
          // Carregar o combobox com os CT-es do manifesto selecionado (implementa��o Hugo)
          while not frmPortariaSai.ADOQPallet_CTE.Eof do
          begin
            frmControle_pallet.cbcte.Items.add
              (frmPortariaSai.ADOQPallet_CTECODCGC.AsString);
            ADOQPallet_CTE.Next;
          end;
          frmControle_pallet.ShowModal;
        finally
          frmControle_pallet.Free;
        end;
        frmMenu.Tag := 0;
      end;
    end;

    // n�o foi colocado quant de pallet
    if self.Tag = 1 then
      Exit;

    if dbgManifesto.Visible = true then
    begin
      mdTemp.First;
      while not mdTemp.Eof do
      begin
        if mdTempInserida.value = 1 then
        begin
          if manifestos = '' then
            manifestos := mdTempManifesto.AsString
          else
            manifestos := manifestos + ' / ' + mdTempManifesto.AsString;
          SPSaida.Parameters[0].value := mdTempManifesto.value;
          SPSaida.Parameters[1].value := cbTipo.Text;
          SPSaida.Parameters[2].value := edTransp.Text;
          SPSaida.Parameters[3].value := edMotor.Text;
          SPSaida.Parameters[4].value := glbFilial;
          SPSaida.Parameters[5].value := edKM.value;
          SPSaida.Parameters[6].value := Memo1.Text;
          SPSaida.Parameters[7].value := GLBUSER;
          SPSaida.Parameters[8].value := ApCarac(edPlaca.Text);
          SPSaida.Parameters[9].value := mdTempserie.value;
          SPSaida.ExecProc;
          if cbTipo.Text = 'Manifesto' then
          begin
            {if mdTempserie.value = '1' then
            begin
              Sp_SaidaSMS.Parameters[0].value := mdTempManifesto.value;
              Sp_SaidaSMS.Parameters[1].value := glbFilial;
              Sp_SaidaSMS.Parameters[2].value := GLBUSER;
              Sp_SaidaSMS.ExecProc;
            end; }
            if mdTempserie.value = 'U' then
            begin
              dtmdados.IQuery1.close;
              dtmdados.IQuery1.sql.Clear;
              dtmdados.IQuery1.sql.add
                ('update cyber.tb_coleta set dt_saida_portaria = sysdate ');
              dtmdados.IQuery1.sql.add('where sequencia in ');
              dtmdados.IQuery1.sql.add
                ('(select distinct a.sequencia from vw_monitor_abr a ');
              dtmdados.IQuery1.sql.add
                ('where a.codman = :0 and a.filial = :1 ');
              dtmdados.IQuery1.sql.add('and a.serman = ''U'') ');
              dtmdados.IQuery1.Parameters[0].value := mdTempManifesto.value;
              dtmdados.IQuery1.Parameters[1].value := glbFilial;
              dtmdados.IQuery1.ExecSQL;
              dtmdados.IQuery1.close;
            end;
          end;
        end;
        mdTemp.Next;
      end;
    end
    else
    begin
      SPSaida.Parameters[0].value := edManifesto.value;
      SPSaida.Parameters[1].value := cbTipo.Text;
      SPSaida.Parameters[2].value := edTransp.Text;
      SPSaida.Parameters[3].value := edMotor.Text;
      SPSaida.Parameters[4].value := glbFilial;
      SPSaida.Parameters[5].value := edKM.value;
      SPSaida.Parameters[6].value := Memo1.Text;
      SPSaida.Parameters[7].value := GLBUSER;
      SPSaida.Parameters[8].value := ApCarac(edPlaca.Text);
      SPSaida.Parameters[9].value := '';
      SPSaida.ExecProc;
    end;
    ShowMessage('Sa�da Registrada');
    Application.CreateForm(TfrmPortariasai_impressao, frmPortariasai_impressao);
    frmPortariasai_impressao.Caption := manifestos;
    frmPortariasai_impressao.RLReport1.Print;
    btnImprimir.Enabled := true;
    frmPortariasai_impressao.free;
    btnGerar.Enabled := false;

    if cbTipo.Text = 'Manifesto' then
    begin
      mdTemp.First;
      while not mdTemp.Eof do
      begin
        // Retirado a comunica��o pelo sistema e criado um servi�o
        {if mdTempInserida.value = 1 then
        begin

          //Enviar para Portal do Transportador para baixa no celular
          QPortal.Close;
          QPortal.Parameters[0].value := mdTempManifesto.AsInteger;
          QPortal.Parameters[1].value := glbFilial;
          QPortal.Parameters[2].value := mdTempserie.AsString;
          QPortal.open;
          if QPortal.RecordCount > 0 then
          begin
            try
              while not QPortal.eof do
              begin
                try
                  RESTRequest1.Params.Clear;
                  RESTRequest1.Params.Add;
                  RESTRequest1.Params[0].Kind := pkHTTPHEADER;
                  RESTRequest1.Params[0].name := 'apikey';
                  RESTRequest1.Params[0].Value := '78asd4546d4sa687e1d1xzlcknhwyhuWMKPSJDpox8213njdOWnxxipW58547';

                  RESTRequest1.Params.Add;
                  RESTRequest1.Params[1].Kind := pkREQUESTBODY;
                  RESTRequest1.Params[1].name := 'Auth_busca';
                  RESTRequest1.Params[1].Value := 'bb68093b11f2476491a8558a9a248a70';

                  RESTRequest1.Params.Add;
                  RESTRequest1.Params[2].Kind := pkREQUESTBODY;
                  RESTRequest1.Params[2].name := 'cnpj';
                  RESTRequest1.Params[2].Value := QPOrtalCODCGC.AsString;

                  RESTRequest1.Params.Add;
                  RESTRequest1.Params[3].Kind := pkREQUESTBODY;
                  RESTRequest1.Params[3].name := 'usuario';
                  RESTRequest1.Params[3].Value := QPortalUSUARIO.AsString;
                  a := 3;
                  QSMS.Close;
                  QSMS.Parameters[0].value := mdTempManifesto.AsInteger;
                  QSMS.Parameters[1].value := glbFilial;
                  QSMS.Parameters[2].value := mdTempserie.AsString;
                  QSMS.Parameters[3].value := QPortalCODCGC.AsString;
                  QSMS.open;
                  while not QSMS.eof do
                  begin
                    r := inttostr(i);
                    a := a + 1;
                    RESTRequest1.Params.Add;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][nfe]';
                    RESTRequest1.Params[a].Value := QSMSNR_CTRC.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][serie]';
                    RESTRequest1.Params[a].Value := QSMSSERIE.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][nome_cliente]';
                    RESTRequest1.Params[a].Value := QSMSDESTINO.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][destino_endereco]';
                    RESTRequest1.Params[a].Value := QSMSENDERE.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][numero_entrega]';
                    RESTRequest1.Params[a].Value := QSMSNUMERO.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkGETorPOST;
                    RESTRequest1.Params[a].name := 'dados['+r+'][cep_entrega]';
                    RESTRequest1.Params[a].Value := QSMSCODCEP.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][celular]';
                    RESTRequest1.Params[a].Value := QSMSTELCEL.AsString;
                    RESTRequest1.Params.Add;
                    a := a + 1;
                    RESTRequest1.Params[a].Kind := pkREQUESTBODY;
                    RESTRequest1.Params[a].name := 'dados['+r+'][pedido]';
                    RESTRequest1.Params[a].Value := QSMSPEDIDO.AsString;
                    QSms.Next;
                    i := i + 1;
                  end;

                  RESTRequest1.Execute;
                  if RestResponse1.StatusCode > 200 then
                  begin
                    jv := RESTResponse1.JSONValue;

                    jsonObj := TJSONObject.ParseJSONValue(RESTResponse1.Content) as TJSONObject;
                    retorno := jsonObj.GetValue('success').Value;
                    men     := jsonObj.GetValue('message').Value;

                    dtmDados.IQuery1.close;
                    dtmDados.IQuery1.sql.clear;
                    dtmDados.IQuery1.sql.add
                      ('update tb_portaria set retorno_portal = retorno_portal || '' - '' || :0 where reg = :1 ');
                    dtmDados.IQuery1.Parameters[0].value := men;
                    dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                    dtmDados.IQuery1.ExecSQL;
                    dtmDados.IQuery1.close;
                  end
                  else
                  begin
                    retorno := RESTResponse1.Content;

                    dtmDados.IQuery1.close;
                    dtmDados.IQuery1.sql.clear;
                    dtmDados.IQuery1.sql.add
                      ('update tb_portaria set portal = ''S'', retorno_portal = retorno_portal || '' - '' || :0 where reg = :1 ');
                    dtmDados.IQuery1.Parameters[0].value := retorno;
                    dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                    dtmDados.IQuery1.ExecSQL;
                    dtmDados.IQuery1.close;
                  end;
                except
                  on e: Exception do
                  begin
                    showmessage('Erro: ' + e.Message + ' Retorno: ' + RESTResponse1.Content);
                  end
                end;
                QPortal.Next;
              end;
              finally

            end;
          end;
        end;  }

        // Projeto 33 enviar os xmls do nossos CT-es ao transportador
        if mdTempInserida.value = 1 then
        begin
          xx := TStringList.Create;
          cc := TStringList.Create;
          mensagem := TStringList.Create;
          // email
          QEmail.Close;
          QEmail.Parameters[0].Value := mdTempManifesto.AsInteger;
          QEmail.Parameters[1].Value := mdTempserie.AsString;
          QEmail.Parameters[2].Value := glbFilial;
          QEmail.Open;
          while not QEmail.eof do
          begin
            cc.Add(QemailEMAIL.asstring);
            QEmail.Next;
          end;
          QEmail.Close;

          para := 'mensageiro@intecomlogistica.com.br';
          //cc.Add('paulo.cortez@intecomlogistica.com.br');

          QXML.Close;
          QXML.Parameters[0].Value := mdTempManifesto.AsInteger;
          QXML.Parameters[1].Value := mdTempserie.AsString;
          QXML.Parameters[2].Value := glbFilial;
          QXML.Open;
          while not QXML.eof do
          begin
            sData := FormatDateTime('YYYY', QXMLDT_EMISSAO.value) +
                     FormatDateTime('MM', QXMLDT_EMISSAO.value);
            sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sData + '\CTe\';
            arquivo := scaminho + QXMLCTE_CHAVE.asstring + '-cte.xml';
            arqxml :=  QXMLCTE_CHAVE.asstring + '-cte.xml';
            xx.Add(arquivo); //frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar +'\'+ QXMLCTE_CHAVE.asstring+'-cte.xml');

            MS := TMemoryStream.Create;
            try
              MS.LoadFromFile(arquivo);
              frmMenu.ACBrMail1.AddAttachment(MS, arqXML);
            finally
              MS.Free;
            end;

            Qxml.Next;
          end;
          mensagem.add('Estamos anexando os CT-e�s do Manifesto ' + mdTempManifesto.AsString);
          Mensagem.Add(' ');
          Mensagem.Add('>>> Sistema SIM - TI Intecom/IW <<<');


          frmMenu.ACBrCTe1.EnviarEmail(para //Para
                                       , 'CT-e�s do Manifesto ' + mdTempManifesto.AsString //edtEmailAssunto.Text
                                       , mensagem //mmEmailMsg.Lines
                                       , cc //Lista com emails que ser�o enviado c�pias - TStrings
                                       , xx // Lista de anexos - TStrings
                                        );
          xx.Free;
          mensagem.Free;
          QXML.Close;
        end;

        mdTemp.Next;
      end;
      FreeAndNil(frmMensagemSms);
      QSMS.close;
    end;



    // Processo de averba��o com ATM
    { if cbtipo.text = 'Manifesto' then
      begin
      QAverba.close;
      QAverba.Parameters[0].value := mdTempMANIFESTO.AsInteger;
      QAverba.Parameters[1].value := GlbFilial;
      QAverba.Parameters[2].value := mdTempSERIE.value;
      QAverba.Open;
      while not QAverba.eof do
      begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.SQL.add('select COALESCE(user_ws,'''') user_ws, pass_ws, code_ws from tb_seguradora ');
      dtmdados.IQuery1.SQL.add('where fl_empresa = :0 ');
      dtmdados.IQuery1.Parameters[0].value := QAverbaFL_EMPRESA.AsInteger;
      dtmdados.IQuery1.open;
      if dtmdados.IQuery1.FieldByName('user_ws').value <> '' then
      begin
      rec := QAverbaCTE_CHAVE.asstring+ '-cte.xml';
      sData := FormatDateTime('YYYY', QAverbaDT_CONHECIMENTO.value) +
      FormatDateTime('MM', QAverbaDT_CONHECIMENTO.value);
      sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sData + '\CTe\';

      frmMenu.ACBrCTe1.Conhecimentos.Clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + rec);
      frmMenu.ACBrCTe1.Consultar;

      resultado := ATM.GetATMWebSvrPortType.averbaCTe20(
      dtmdados.iQuery1.FieldByName('user_ws').value,
      dtmdados.iQuery1.FieldByName('pass_ws').value,
      dtmdados.iQuery1.FieldByName('code_ws').value,
      frmMenu.ACBrCTe1.Conhecimentos.Items[0].XML
      );
      resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
      resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);
      if Pos('<PROTOCOLONUMERO>',resultado) > 0 then
      begin
      inic := Pos('<PROTOCOLONUMERO>',resultado)+17;
      fim  := Pos('</PROTOCOLONUMERO>',resultado);
      a := fim - inic;
      protocolo := copy(resultado,inic,a);

      inic := Pos('<DATA>',resultado)+6;
      fim  := Pos('</DATA>',resultado);
      a := fim - inic;
      DATA := copy(resultado,inic,a);

      inic := Pos('<HORA>',resultado)+6;
      fim  := Pos('</HORA>',resultado);
      a := fim - inic;
      HORA := copy(resultado,inic,a);
      data := BuscaTroca(data,'-','/');

      frmMenu.prc_averba.Parameters[0].value := protocolo;
      frmMenu.prc_averba.Parameters[1].value := StrToDateTime(data + ' ' + hora);
      frmMenu.prc_averba.Parameters[2].value := QAverbaCOD_CONHECIMENTO.Value;
      frmMenu.prc_averba.ExecProc;
      end;
      end;
      QAverba.next;
      end;
      end; }
    mdTemp.close;
  end;
end;

procedure TfrmPortariaSai.btnImprimirClick(Sender: TObject);
begin
  // RLReport1.Preview;
  QEnvia.close;
  QEnvia.Parameters[0].value := 1;
  QEnvia.Parameters[1].value := glbFilial;
  QEnvia.open;
  if not QEnvia.Eof then
  begin
    enviar_email;
  end
  else
    QEnvia.close;
end;

procedure TfrmPortariaSai.cbTipoExit(Sender: TObject);
begin
  if (cbTipo.Text = 'Manifesto') then
  begin
    // edPlaca1.Enabled := false;
    edManifesto.Visible := false;
    dbgManifesto.Visible := true;
    dbgManifesto.Enabled := true;
    edPlaca.Enabled := false;
    edTransp.Enabled := false;
    edMotor.Enabled := true;
    edCfp.Enabled := false;
    edRG.Enabled := false;
    edCNH.Enabled := false;
    edPlaca.color := $00E6E6E6;
    edTransp.color := $00E6E6E6;
    edMotor.color := clWhite;
    edCfp.color := $00E6E6E6;
    edRG.color := $00E6E6E6;
    edCNH.color := $00E6E6E6;
    // QManifesto.Close;
    // QManifesto.sql [5] := 'and m.fl_empresa = ' + inttostr(glbemp) + 'and ds_modal not in (''RETIRA'',''ENTREGA'') ';
    // QManifesto.Open;
    mdTemp.close;
    mdTemp.open;
    QMan.open;
    while not QMan.Eof do
    begin
      mdTemp.append;
      mdTempManifesto.value := QManNR_ROMANEIO.AsInteger;
      mdTempserie.value := QManSERMAN.value;
      if (QManSERMAN.value = '1') and (QManMDFEAUT.value = 'N') then
        mdTempvalido.value := 'N'
      else
        mdTempvalido.value := 'S';
      QMan.Next;
    end;
    QMan.close;
    reg := 0;
    dbgManifesto.SetFocus;
  end
  else if (cbTipo.Text = 'Romaneio') then
  begin
    edPlaca1.Enabled := false;
    edManifesto.Visible := false;
    dbgManifesto.Visible := true;
    dbgManifesto.Enabled := true;
    edPlaca.Enabled := false;
    edTransp.Enabled := false;
    edMotor.Enabled := true;
    edCfp.Enabled := false;
    edRG.Enabled := false;
    edCNH.Enabled := false;
    edPlaca.color := $00E6E6E6;
    edTransp.color := $00E6E6E6;
    edMotor.color := clWhite;
    edCfp.color := $00E6E6E6;
    edRG.color := $00E6E6E6;
    edCNH.color := $00E6E6E6;
    Qmanifesto.close;
    // QManifesto.sql [5] := 'and m.fl_empresa = ' + inttostr(glbemp) + 'and ds_modal not in (''RETIRA'',''ENTREGA'') ';
    Qmanifesto.open;
    mdTemp.close;
    mdTemp.open;
    Qmanifesto.open;
    while not Qmanifesto.Eof do
    begin
      mdTemp.append;
      mdTempManifesto.value := QmanifestoNR_ROMANEIO.AsInteger;
      mdTempvalido.value := 'S';
      Qmanifesto.Next;
    end;
    Qmanifesto.close;
    reg := 0;
    dbgManifesto.SetFocus;
  end
  else
  begin
    edPlaca1.Enabled := true;
    edManifesto.Clear;
    if cbTipo.Text = 'COLETA' then
    begin
      Label1.Visible := true;
      edManifesto.Visible := true;
      dbgManifesto.Visible := false;
      dbgManifesto.Enabled := false;
    end
    else if cbTipo.Text = 'RETIRA' then
    begin
      // label1.caption := 'Retira';
      edManifesto.Visible := false;
      dbgManifesto.Enabled := true;
      dbgManifesto.Visible := true;
      Qmanifesto.close;
      Qmanifesto.sql[5] := 'and ds_modal = ''RETIRA'' ';
      Qmanifesto.open;
      mdTemp.close;
      mdTemp.open;
      Qmanifesto.open;
      while not Qmanifesto.Eof do
      begin
        mdTemp.append;
        mdTempManifesto.value := QmanifestoNR_ROMANEIO.AsInteger;
        mdTempvalido.value := 'S';
        Qmanifesto.Next;
      end;
      Qmanifesto.close;
      reg := 0;
      dbgManifesto.SetFocus;
    end
    else
    begin
      edManifesto.Enabled := false;
      dbgManifesto.Enabled := false;
      dbgManifesto.Visible := false;
      edManifesto.color := $00E6E6E6;
    end;
    edPlaca.Enabled := false;
    edTransp.Enabled := false;
    edMotor.Enabled := true;
    edRG.Enabled := false;
    edCNH.Enabled := false;
    //
    if (cbTipo.Text <> 'VAZIO') then
    begin
      edCfp.Enabled := false;
      edCfp.color := $00E6E6E6;
    end
    else
    begin
      edCfp.Enabled := true;
      edCfp.color := clWhite;
    end;
    //
    edPlaca.color := $00E6E6E6;
    edTransp.color := $00E6E6E6;
    edMotor.color := clWhite;
    edRG.color := $00E6E6E6;
    edCNH.color := $00E6E6E6;
    if (cbTipo.Text = 'COLETA') then
      edManifesto.SetFocus
    else if (cbTipo.Text = 'RETIRA') then
      dbgManifesto.SetFocus
    else
      edPlaca1.SetFocus;
  end;
  btnGerar.Enabled := true;
end;

procedure TfrmPortariaSai.dbgManifestoCellClick(Column: TColumn);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  if mdTempvalido.value = 'S' then
  begin
    mdTemp.edit;
    if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
    begin
      reg := reg + 1;
      mdTempreg.value := reg;
      mdTempInserida.value := 1;
      //
      if (cbTipo.Text = 'Manifesto') then
      begin
        QMan.close;
        QMan.sql[8] := 'and m.manifesto = ' + mdTempManifesto.AsString;
        QMan.sql[9] := 'and m.serie = ' + QuotedStr(mdTempserie.AsString);
        QMan.open;
        if not QMan.Eof then
        begin
          edPlaca.Text := edPlaca.Text; // QmanPLACA.value;
          edTransp.Text := QManTRANSPORTADORA.value;
          edMotor.Text := QManMOTORISTA.value;
          btnGerar.Enabled := true;
          if Label1.Caption = 'Retira' then
            edPlaca1.SetFocus
          else
            Memo1.SetFocus;
        end;
      end
      else if (cbTipo.Text = 'Romaneio') then
      begin
        Qmanifesto.close;
        Qmanifesto.sql[5] := 'and r.nr_romaneio = ' + mdTempManifesto.AsString;
        Qmanifesto.sql[13] := 'and r.nr_romaneio = ' + mdTempManifesto.AsString;
        Qmanifesto.open;

        if not Qmanifesto.Eof then
        begin
          QSaida1.close;
          QSaida1.Parameters[0].value := QmanifestoPLACA.value;
          QSaida1.open;
          if QSaida1.Eof then
          begin
            ShowMessage('Esta Placa ' + QuotedStr(QmanifestoPLACA.value) +
              ' N�o Teve Entrada !!');
            abort;
          end;
          if (edPlaca.Text <> '') and (edPlaca.Text <> QmanifestoPLACA.value)
          then
          begin
            ShowMessage('Este Romaneio � de Placa Diferente');
            mdTempInserida.value := 0;
            Exit;
          end;
          edPlaca.Text := QmanifestoPLACA.value;
          edTransp.Text := QSaida1NM_TRANSP.value;
          QmanifestoTRANSPORTADORA.value;
          edMotor.Text := QmanifestoMOTORISTA.value;
          btnGerar.Enabled := true;
          if Label1.Caption = 'Retira' then
            edPlaca1.SetFocus
          else
            Memo1.SetFocus;
        end;
      end
      else
      begin
        dtmdados.WQuery1.close;
        dtmdados.WQuery1.sql.Clear;
        dtmdados.WQuery1.sql.add
          ('select * from tb_portaria where manifesto = :0 and motivo = ''S'' ');
        dtmdados.WQuery1.Parameters[0].value := edManifesto.Text;
        dtmdados.WQuery1.open;
        if dtmdados.WQuery1.Eof then
        begin
          ShowMessage(Label1.Caption + ' N�o Encontrada !!');
          Panel3.SetFocus;
          edManifesto.SetFocus;
          Exit;
        end
        else
        begin
          ShowMessage(Label1.Caption + ' J� Teve Sa�da !!');
          Panel3.SetFocus;
          edManifesto.SetFocus;
          Exit;
        end
      end;
    end
    else
    begin
      mdTempInserida.value := 0;
    end;
    mdTemp.post;
  end;
end;

procedure TfrmPortariaSai.dbgManifestoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdTempInserida) then
  begin
    dbgManifesto.Canvas.FillRect(Rect);
    iml.Draw(dbgManifesto.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdTempInserida.value = 1 then
      iml.Draw(dbgManifesto.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgManifesto.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
  if mdTempvalido.value = 'N' then
  begin
    dbgManifesto.Canvas.Brush.color := clRed;
    dbgManifesto.Canvas.Font.color := clWhite;
    dbgManifesto.DefaultDrawDataCell(Rect, dbgManifesto.columns[DataCol]
      .Field, State);
  end
end;

procedure TfrmPortariaSai.edKMExit(Sender: TObject);
begin
  if (edKM.value > 0) then
  begin
    if edKM.value < StrToInt(lblKm.Caption) then
    begin
      ShowMessage('A kilometragem de Sa�da est� menor que na Entrada');
      Panel2.SetFocus;
      edKM.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmPortariaSai.edManifestoExit(Sender: TObject);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;
  if edManifesto.Text <> '' then
  begin
    if (Label1.Caption = 'Romaneio') or (Label1.Caption = 'Retira') then
    begin
      Qmanifesto.close;
      QManifesto.SQL[14] := 'where nr_romaneio = '+ QuotedStr(edManifesto.Text);
      Qmanifesto.open;
      if not Qmanifesto.Eof then
      begin
        edPlaca.Text := QmanifestoPLACA.value;
        edTransp.Text := QmanifestoTRANSPORTADORA.value;
        edMotor.Text := QmanifestoMOTORISTA.value;
        // edCfp.Text := QmanifestoNR_CNPJ_CPF.value;
        // edRG.text := QmanifestoDS_IE.Value;
        // edFrota.Text := QmanifestoFROTA.AsString;

        btnGerar.Enabled := true;
        if Label1.Caption = 'Retira' then
          edPlaca1.SetFocus
        else
          Memo1.SetFocus;
      end
      else
      begin
        dtmdados.WQuery1.close;
        dtmdados.WQuery1.sql.Clear;
        dtmdados.WQuery1.sql.add
          ('select * from tb_portaria where manifesto = :0 and motivo = ''S'' ');
        dtmdados.WQuery1.Parameters[0].value := edManifesto.Text;
        dtmdados.WQuery1.open;
        if dtmdados.WQuery1.Eof then
        begin
          ShowMessage(Label1.Caption + ' N�o Encontrado !!');
          Panel3.SetFocus;
          edManifesto.SetFocus;
          Exit;
        end
        else
        begin
          ShowMessage(Label1.Caption + ' J� Teve Sa�da !!');
          Panel3.SetFocus;
          edManifesto.SetFocus;
          Exit;
        end
      end;
    end;
    { else
      begin
      dtmdados.WQuery1.close;
      dtmdados.WQuery1.sql.clear;
      dtmdados.WQuery1.sql.add('select nm_fornecedor, cod_terc_ta, t.nm_contato ');
      dtmdados.WQuery1.sql.add('  from tb_tarefas t ');
      dtmdados.WQuery1.sql.add('  left join tb_fornecedor f on t.cod_terc_ta = f.cod_fornecedor');
      dtmdados.WQuery1.sql.add(' where t.cod_ta = :0 ');
      dtmdados.WQuery1.sql.add('   and t.fl_empresa = :1');
      //
      dtmdados.WQuery1.Parameters[0].value := edManifesto.text;
      dtmdados.WQuery1.Parameters[1].value := glbemp;
      dtmdados.WQuery1.open;
      if not dtmdados.WQuery1.eof then
      begin
      edTransp.Text := dtmdados.WQuery1.FieldByName('nm_fornecedor').AsString;
      edMotor.Text  := dtmdados.WQuery1.FieldByName('nm_contato').AsString;
      btnGerar.Enabled := true;
      //panel3.SetFocus;
      edPlaca1.SetFocus;
      end
      else
      begin
      ShowMessage(label1.caption + ' N�o Encontrada !!');
      //panel3.SetFocus;
      edmanifesto.setfocus;
      exit;
      end;
      end; }
  end
  else
    edManifesto.SetFocus;
end;

procedure TfrmPortariaSai.edMotorExit(Sender: TObject);
begin
  Memo1.SetFocus;
end;

procedure TfrmPortariaSai.edPlaca1Exit(Sender: TObject);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;
  if edPlaca1.Text <> '' then
  begin
    QSaida1.close;
    QSaida1.Parameters[0].value := edPlaca1.Text;
    QSaida1.open;
    if QSaida1.Eof then
    begin
      ShowMessage('Esta Placa N�o Teve Entrada !!');
      edPlaca1.Clear;
      edPlaca1.SetFocus;
    end
    else
    begin
      if (QSaida1NM_TRANSP.Text <> edTransp.Text) and (cbTipo.Text = 'COLETA')
      then
      begin
        ShowMessage('Esta Placa N�o � da Transportadora Escalada!!');
        edPlaca1.Clear;
        edPlaca1.SetFocus;
        Exit;
      end;
      edPlaca.Text := QSaida1PLACA.value;
      edTransp.Text := QSaida1NM_TRANSP.value;
      edMotor.Text := QSaida1NM_MOTOR.value;
      edCfp.Text := QSaida1NR_CPF.value;
      edRG.Text := QSaida1NR_RG.value;
      edCNH.Text := QSaida1NR_CNH.value;

      btnGerar.Enabled := true;
      Memo1.SetFocus;
    end;
    verificafrota(edPlaca1.Text);
  end
  else
    edPlaca1.SetFocus;
end;

procedure TfrmPortariaSai.enviar_email;
var
  mail, memo2: TStringList;
  tempo: Integer;
  email, email2: string;
  nPosicao, nContador: Integer;
begin
  tempo := StrToInt(ApCarac(QEnviaFL_TEMPO_PER.AsString));
  memo2 := TStringList.Create;
  memo2.add('<HTML><HEAD><TITLE></TITLE>');
  memo2.add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo2.add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo2.add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo2.add(
    '       .texto3 {	FONT: 08px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.add('</STYLE>');
  memo2.add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo2.add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo2.add('<table border=0><title></title></head><body>');
  memo2.add('<table border=1>');
  memo2.add('<tr>');
  memo2.add(
    '<th colspan=4><img src="http://iwlog.com.br/logo.jpg"><FONT class=texto3></font></th>');
  memo2.add('</tr><tr>');
  memo2.add('<th colspan=4><FONT class=titulo1>MANIFESTO DE SA�DA</th></font>');
  memo2.add('</tr><tr>');
  memo2.add(
    '<th height=70>TRANSPORTADORA&nbsp;&nbsp;<FONT class=texto1>INTECOM LOG�STICA</TH>');
  memo2.add('<th> DATA&nbsp;&nbsp;' + DateToStr(date) + '</th>');
  memo2.add('</tr><tr>');
  memo2.add('<th>DESTINAT�RIO :&nbsp;&nbsp;' + QEnviaNM_EMP_DEST_TA.value
    + '</TH>');
  memo2.add('<th>Previs�o de Chegada :&nbsp;&nbsp;<FONT class=texto1>' +
    DateTimeToStr(IncMinute(Now, tempo)) + '</TH>');
  memo2.add('</TR><TR FONT class=texto2>');
  memo2.add('<TH>N� NF.</th>');
  memo2.add('<th>Quantidade</th>');

  // ITENS
  QEnvia.First;
  while not QEnvia.Eof do
  begin
    memo2.add('<TR FONT class=texto4>');
    memo2.add('<th>' + QEnviaNR_NF.AsString + '</Th>');
    memo2.add('<Th>' + QEnviaNR_QUANTIDADE.AsString + '</Th>');
    memo2.add('</TR>');
    QEnvia.Next;
  end;
  memo2.add('</table>');
  memo2.SaveToFile('envia' + GLBUSER + '.html');
  memo2.Free;

  { Lista := TStringList.Create;
    iRetorno := ExtractStrings([';'],[' '],PChar(QEnviaDS_EMAIL.AsString),Lista);
    sProcura := 'a;00:1A:73:DE:02:6A;10.60.0.3;loja;1500;200;'; }
  email2 := QEnviaDS_EMAIL.AsString;
  nPosicao := 0;
  nContador := 1;
  While nContador < Length(email2) do
  begin
    nPosicao := Pos(';', email2); // ,nContador);
    if email2 <> '' then
    begin
      email := copy(email2, nContador - 1, nPosicao);
      mail := TStringList.Create;
      try
        mail.values['to'] := email;
        /// AQUI VAI O EMAIL DO DESTINATARIO///
        mail.values['subject'] := 'EXPEDI��O VE�CULO CD INTECOM BARUERI';
        /// AQUI O ASSUNTO///
        mail.values['body'] := '';
        /// AQUI O TEXTO NO CORPO DO EMAIL///
        mail.values['attachment0'] := 'envia' + GLBUSER + '.html';
        /// /AQUI O ENDERE�O ONDE ENCONTRAM OS ARQUIVOS//
        mail.values['attachment1'] := '';
        /// IDEM  - NO ATTACHMENT1    TEM QUE COLOCAR A SEQUNCIA DO EMAIL A QUAL DESEJA ENVIAR EXEMPLO: ATTACHMENT1
        sendEMail(Application.Handle, mail);
      finally
        mail.Free;
      end;
      // nContador := nPosicao;
      email2 := copy(email2, nPosicao + 1, Length(email2) - nPosicao);
    end;
    nContador := 2;
  end;

  { mail := TStringList.Create;
    try
    mail.values['to'] := QEnviaDS_EMAIL.AsString; ///AQUI VAI O EMAIL DO DESTINATARIO///
    mail.values['subject'] := 'EXPEDI��O VE�CULO CD INTECOM BARUERI' ; ///AQUI O ASSUNTO///
    mail.values['body'] := ''; ///AQUI O TEXTO NO CORPO DO EMAIL///
    mail.values['attachment0'] := 'envia' + Glbuser + '.html'; ////AQUI O ENDERE�O ONDE ENCONTRAM OS ARQUIVOS//
    mail.values['attachment1'] := ''; ///IDEM  - NO ATTACHMENT1    TEM QUE COLOCAR A SEQUNCIA DO EMAIL A QUAL DESEJA ENVIAR EXEMPLO: ATTACHMENT1
    sendEMail(Application.Handle, mail);
    finally
    mail.Free;
    end;
    FreeAndNil(Lista); }
end;

end.
