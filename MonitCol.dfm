object frmMonitCol: TfrmMonitCol
  Left = 0
  Top = 0
  Caption = 'Monitoramento de Transporte'
  ClientHeight = 720
  ClientWidth = 1272
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvPageControl1: TJvPageControl
    Left = 0
    Top = 0
    Width = 1272
    Height = 353
    ActivePage = TBColetas
    Align = alTop
    TabOrder = 0
    object TBColetas: TTabSheet
      Caption = 'Coletas'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1264
        Height = 325
        Hint = 'Clique com o bot'#227'o direito para ver a legenda'
        Align = alClient
        DataSource = dtsTarefas
        ParentShowHint = False
        PopupMenu = pmLegenda
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        OnDblClick = DBGrid1DblClick
        OnKeyDown = DBGrid1KeyDown
        OnTitleClick = DBGrid1TitleClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        UseXPThemes = False
        Columns = <
          item
            Expanded = False
            FieldName = 'NRO_OCS'
            Title.Alignment = taCenter
            Title.Caption = 'OCS'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SOLICITACAO'
            Title.Alignment = taCenter
            Title.Caption = 'Solicita'#231#227'o'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODRMC'
            Title.Alignment = taCenter
            Title.Caption = 'Roman.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_CLI'
            Title.Alignment = taCenter
            Title.Caption = 'Cliente'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 123
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCON'
            Title.Alignment = taCenter
            Title.Caption = 'CT-e/OST'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODMAN'
            Title.Alignment = taCenter
            Title.Caption = 'Manif.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_REM'
            Title.Alignment = taCenter
            Title.Caption = 'Origem'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 163
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CID_REM'
            Title.Alignment = taCenter
            Title.Caption = 'Cidade Origem'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 116
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_DES'
            Title.Alignment = taCenter
            Title.Caption = 'Destino'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 139
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CID_DES'
            Title.Alignment = taCenter
            Title.Caption = 'Cidade Destino'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 135
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ESTADO'
            Title.Caption = 'UF'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAINC'
            Title.Alignment = taCenter
            Title.Caption = 'DT OCS'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_COLETA_PREV'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Prev.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_COLETA'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Real'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA_CD'
            Title.Alignment = taCenter
            Title.Caption = 'Entr. CDC'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SAIDA'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Sa'#237'da'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA_PREV'
            Title.Alignment = taCenter
            Title.Caption = 'Dest. Prev.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Status'
            Title.Alignment = taCenter
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 447
        Top = 117
        Width = 361
        Height = 41
        Caption = 'Ordenando .......'
        Color = clLime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        Visible = False
      end
    end
  end
  object pnLegenda: TPanel
    Left = 485
    Top = 148
    Width = 226
    Height = 181
    TabOrder = 1
    Visible = False
    OnExit = pnLegendaExit
    object JvGradient8: TJvGradient
      Left = 1
      Top = 1
      Width = 20
      Height = 179
      Align = alLeft
      StartColor = 8404992
      EndColor = clBtnFace
      ExplicitHeight = 181
    end
    object lblColetaHoje: TJvFullColorLabel
      Left = 8
      Top = 48
      Width = 167
      Height = 17
      LabelColor = 67758079
      Brush.Color = 649215
      Shape = stRoundRect
      Caption = 'Coleta para ser entregue hoje'
      AutoSize = True
    end
    object lblColetaAtrasada: TJvFullColorLabel
      Left = 8
      Top = 8
      Width = 100
      Height = 17
      LabelColor = 71305697
      Brush.Color = 4196833
      Shape = stRoundRect
      Caption = 'Coleta Atrasada'
      AutoSize = True
      Color = clBtnFace
      ParentColor = False
    end
    object lblColetaPontual: TJvFullColorLabel
      Left = 8
      Top = 28
      Width = 92
      Height = 17
      LabelColor = 67143427
      Brush.Color = 34563
      Shape = stRoundRect
      Caption = 'Coleta Pontual'
      AutoSize = True
    end
    object SMS1: TJvFullColorLabel
      Left = 8
      Top = 68
      Width = 103
      Height = 17
      LabelColor = 79896628
      Brush.Color = 12787764
      Shape = stRoundRect
      Caption = 'SMS - Retornado'
      AutoSize = True
      Color = clBtnFace
      ParentColor = False
    end
    object SMS2: TJvFullColorLabel
      Left = 8
      Top = 88
      Width = 149
      Height = 17
      LabelColor = 67146737
      Brush.Color = 37873
      Shape = stRoundRect
      Caption = 'SMS - Problema no Trajeto'
      AutoSize = True
      Color = clBtnFace
      ParentColor = False
    end
    object SMS3: TJvFullColorLabel
      Left = 8
      Top = 108
      Width = 152
      Height = 17
      LabelColor = 83836811
      Brush.Color = 16727947
      Shape = stRoundRect
      Caption = 'SMS - Problema na Entrega'
      AutoSize = True
    end
    object SMS9: TJvFullColorLabel
      Left = 8
      Top = 128
      Width = 100
      Height = 17
      LabelColor = 67108864
      Brush.Color = clBlack
      Shape = stRoundRect
      Caption = 'SMS - Indefinido'
      AutoSize = True
    end
    object SMSnulo: TJvFullColorLabel
      Left = 8
      Top = 148
      Width = 112
      Height = 17
      LabelColor = 83885834
      Brush.Color = 16776970
      Shape = stRoundRect
      Caption = 'SMS - N'#227'o Enviado'
      AutoSize = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 353
    Width = 1272
    Height = 208
    Align = alTop
    TabOrder = 2
    object Label10: TLabel
      Left = 9
      Top = 1
      Width = 33
      Height = 13
      Caption = 'Cliente'
    end
    object Label7: TLabel
      Left = 312
      Top = 1
      Width = 20
      Height = 13
      Caption = 'Filial'
    end
    object lblQuant: TLabel
      Left = 1127
      Top = 5
      Width = 57
      Height = 25
      Hint = 'Quantidade de registros'
      Alignment = taCenter
      AutoSize = False
      ParentShowHint = False
      ShowHint = True
      Layout = tlCenter
    end
    object Label2: TLabel
      Left = 476
      Top = 1
      Width = 31
      Height = 13
      Caption = 'Status'
    end
    object lblDestino: TLabel
      Left = 807
      Top = 37
      Width = 232
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 744
      Top = 2
      Width = 21
      Height = 13
      Caption = 'OCS'
    end
    object Label16: TLabel
      Left = 873
      Top = 1
      Width = 23
      Height = 13
      Caption = 'CT-e'
    end
    object Label17: TLabel
      Left = 928
      Top = 1
      Width = 13
      Height = 13
      Caption = 'NF'
    end
    object Label18: TLabel
      Left = 983
      Top = 1
      Width = 47
      Height = 13
      Caption = 'Manifesto'
    end
    object Label6: TLabel
      Left = 808
      Top = 2
      Width = 50
      Height = 13
      Caption = 'Solicita'#231#227'o'
    end
    object cbCliente: TJvComboBox
      Left = 9
      Top = 14
      Width = 236
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 20
      ItemHeight = 0
      TabOrder = 0
    end
    object cbFilial: TJvComboBox
      Left = 311
      Top = 14
      Width = 146
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 0
      TabOrder = 1
      Items.Strings = (
        ''
        'C-COLETA'
        'P-PROVINDA')
    end
    object BitBtn15: TBitBtn
      Tag = 1
      Left = 1045
      Top = 5
      Width = 40
      Height = 25
      Action = acPrimeiroApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6591800C6511800C6511000FF00FF00FF00FF00FF00
        FF00FF00FF00B5301000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800E7AE7B00DE965A00C6591800FF00FF00FF00FF00FF00
        FF00B5381000C6613100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400DE966300CE611800FF00FF00FF00FF00BD49
        1000CE693900DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00DE9E6B00CE691800FF00FF00C6591800D671
        3900DE9E6B00DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7A67300CE712900CE691800D6864200E7A6
        7B00DE966300DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7B68C00D6793900DE9E6B00E7AE8400DEA6
        7300DE966300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00EFBE9C00D6864200E7A67300E7B68C00E7A6
        7B00DE9E6B00E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFB68C00DE8E5200D6864200DE966300E7B6
        9400E7AE7B00E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFBE9400DE965A00FF00FF00D6864200DE9E
        6300E7B68C00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300F7CFB500EFBE9C00E79E6300FF00FF00FF00FF00D686
        4A00DE9E6B00E7B69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400F7CFB500EFC7A500E7A67300FF00FF00FF00FF00FF00
        FF00DE8E4A00DE8E4A00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn16: TBitBtn
      Tag = 2
      Left = 1085
      Top = 5
      Width = 40
      Height = 25
      Action = acAnteriorApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B530
        1000B5301000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5381000C659
        3100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD491000CE693100DE96
        6300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00C6591800D6713900DE9E6B00DE9E
        6B00BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE712900CE692100D6864A00E7AE7B00DE9E6300DEA6
        7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200DE965A00E7B68C00E7A67300DE9E6300E7A6
        7B00C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200E79E6B00EFBE9400E7AE8400DEA67300E7AE
        8400CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DE8E5200E7A67300EFBE9C00E7AE8400E7B6
        8C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00DE965A00E7A67300EFBE9C00EFBE
        9400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE966300E7A67B00EFBE
        9C00DE864A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE9E6300E796
        6300DE965A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6B00E79E6300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn17: TBitBtn
      Tag = 3
      Left = 1186
      Top = 5
      Width = 38
      Height = 25
      Action = acProximoApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6491000B5411000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6591800CE713900BD411000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE611800DEA67300CE713900BD491000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE692100E7AE7B00E7A67B00D6713900BD511000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6713100E7AE8400DEA67300E7AE7B00D6793900C651
        1800B5411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200E7B69400E7A67300E7A67300E7AE8400D679
        4200C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200EFBE9C00E7AE8400E7AE8400E7B68C00D68E
        4A00C6611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7966300EFC7A500EFB69400EFBE9C00DE966300CE71
        2900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A66B00EFC7AD00EFC7A500E7A67300D6864200FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A67B00EFCFAD00EFB68400DE966300FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn18: TBitBtn
      Tag = 4
      Left = 1224
      Top = 5
      Width = 40
      Height = 25
      Action = acUltimoApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6511800FF00FF00FF00FF00FF00FF00FF00FF00BD41
        1000BD381000B5381000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800D6864200C6591800FF00FF00FF00FF00FF00FF00BD49
        1000D6864A00DE965A00B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400D6864200C6591800FF00FF00FF00FF00C651
        1000D6865200DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00E7AE8400D6864200C6611800FF00FF00C659
        1800D68E5200DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7AE7B00E7B68C00D6864A00CE611800CE61
        1800DE965A00DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7AE7B00E7AE8400E7B68C00DE9E6300CE69
        1800DEA67300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00E7B68400E7B68C00EFB69400DE9E6B00CE71
        2900E7AE8400E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFBE9400EFBE9C00DE9E6B00D6793900D679
        3900E7A67300E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFC7A500E7A67B00DE8E4A00FF00FF00DE86
        4200E7AE7B00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFB68C00DE966300FF00FF00FF00FF00DE8E
        5200E7AE8400EFB69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFA67B00E7A67300FF00FF00FF00FF00FF00FF00DE96
        5A00E7B68C00EFBE9C00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6300DE966300DE965A00DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object JvPageControl2: TJvPageControl
      Left = 8
      Top = 36
      Width = 1264
      Height = 169
      ActivePage = TabSheet1
      TabOrder = 6
      object TabSheet1: TTabSheet
        Caption = 'Observa'#231#227'o'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label11: TLabel
          Left = 328
          Top = 0
          Width = 91
          Height = 13
          Caption = 'Ocorr'#234'ncias Coleta'
        end
        object Label23: TLabel
          Left = 328
          Top = 46
          Width = 85
          Height = 13
          Caption = 'Data Coleta Real.'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object Label4: TLabel
          Left = 649
          Top = 46
          Width = 111
          Height = 13
          Caption = 'Data Destino Realizada'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object Label9: TLabel
          Left = 649
          Top = 1
          Width = 98
          Height = 13
          Caption = 'Ocorr'#234'ncias Entrega'
        end
        object cbOcoCol: TJvComboBox
          Left = 328
          Top = 19
          Width = 298
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 11
        end
        object Obs: TRichEdit
          Left = 0
          Top = 0
          Width = 301
          Height = 138
          ReadOnly = True
          TabOrder = 0
        end
        object btnObs: TBitBtn
          Left = 307
          Top = 1
          Width = 15
          Height = 16
          Hint = 'IncluirObs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnObsClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0041924E003D8F49003A8C44003689400032873C002F84
            37002C813300287F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF004999580045965300419950007DC28F0096D0A60096CFA60078BE
            8900368D42002C813400297F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00519F61004D9C5D0064B47800A8DBB50087CC980066BC7D0064BA7C0086CB
            9800A5D9B40058AA6B002C813400297F3000FF00FF00FF00FF00FF00FF0059A6
            6B0056A366006AB97D00A8DBB20060BC77005CBA730059B8700059B56F0058B5
            6F005BB77400A5D9B3005AAA6C002C823400297F3000FF00FF00FF00FF005DA9
            700053AB6800AADDB40064C179005FBE710060BC7700FFFFFF00FFFFFF0059B8
            700058B56E005CB77400A6DAB400388F43002C823400FF00FF00FF00FF0061AC
            75008ACC980089D396006BC67A0063C1700055AB6500FFFFFF00FFFFFF0059B8
            700059B870005BB9720085CC97007BBE8D0030853900FF00FF00FF00FF0065AF
            7A00A9DDB3007DCF8A0075CC8100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF0059B8700067BE7D009CD4AB0034883D00FF00FF00FF00FF0069B2
            7E00B6E2BE008BD597007AC98600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF0059B8700069C17E009DD4AA00388B4200FF00FF00FF00FF006DB5
            8300ACDDB600A6DFAF0081CB8C007CC986006EBD7900FFFFFF00FFFFFF005BAC
            6A0060BC77005CBA73008BD1990080C592003C8E4700FF00FF00FF00FF0070B8
            870085C79700D2EED70095D9A0008AD394007FC88900FFFFFF00FFFFFF0079CD
            85006BC37C006FC77E00ACDFB500459E570040914C00FF00FF00FF00FF0073BA
            8A0070B88700AADAB700D8F1DC0092D89D0088CD930084CC8E008BD496008AD4
            950083D28E00AFE0B7006BB97D004898560044945100FF00FF00FF00FF00FF00
            FF0073BB8B0070B88700AFDCBB00DCF2E000B6E4BD009BDBA50096D9A000A5DF
            AF00C0E8C50079C28A00509E5F004C9B5B00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0073BB8B0071B8870094CEA400C3E6CB00CFEBD400C9E9CE00AFDD
            B8006DB97F0058A5690054A16500FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF0074BB8B0071B988006EB684006AB3800067B17C0063AE
            770060AB73005CA86E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          Layout = blGlyphBottom
        end
        object eddtcolreal: TJvDateEdit
          Left = 328
          Top = 63
          Width = 94
          Height = 21
          DateFormat = 'dd/mm/yyyy'
          TabOrder = 2
        end
        object cbCTe: TCheckBox
          Left = 328
          Top = 90
          Width = 225
          Height = 17
          Caption = 'Todo Conhecimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object btnAlTa: TBitBtn
          Left = 449
          Top = 113
          Width = 71
          Height = 25
          Hint = 'Alterar Tarefa'
          Caption = 'Alterar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnAlTaClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00007D21EB037B1EFF00791504FF00FF00FF00FF00FF00FF00FF00FF001340
            58FF15425EFF25699CFF2C76B4FF3B8BBAADFF00FF00FF00FF00FF00FF00FF00
            FF0001832BEB43A15FFF007B1FCC00791906FF00FF00FF00FF00FF00FF001242
            59FF5D9CD4FFA6CFF5FFA9CFECFF488BC1FF219752FF1B9149FD158F43FD0F8B
            3BFD3A9F5EFF80C196FF46A362FF007D1FD100791907FF00FF00FF00FF001E6D
            93FFCBE3F9FF61AAECFF4098E8FF1567C2FF299B5BFF90CAA9FF8DC8A5FF8AC6
            A1FF88C59EFF6AB685FF82C297FF48A566FF007D21D700791B09FF00FF001E6D
            93FFC8E1F2FFD1E7FAFF347DB5FF3199C3FF319F63FF94CDADFF6FBA8EFF6BB8
            89FF66B685FF61B380FF67B582FF83C298FF3CA05CFF007F25F9FF00FF002063
            98202689B9FFB0CBE1FF67A9C8FF60DCF5FF37A36BFF96CEB0FF94CDADFF91CB
            AAFF90CBA8FF74BC90FF8AC7A1FF46A568FF078735FB01832D01FF00FF00FF00
            FF00FF00FF002689B9FFBEE6F2FFB3F4FCFF3DA56FFF37A46FFF34A269FF309D
            63FF55AF7CFF91CBAAFF4FAB74FF178F45FB118B3D01FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF002790BFFFC3EDF8FFB3F4FCFF60DCF5FF44D6F4FF8EEE
            FAFF34A16DFF5AB381FF289857FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DCF5FF44D6
            F4FF3EA976FF319F65FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DC
            F5FF44D6F4FF8EEEFAFF5DB4E6FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4
            FCFF68D9F5FF6FCFF3FF599DD0FF73ABDDFF4F91C9FFFF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3ED
            F8FFA8E2F8FF6CAEDDFFA5CFF4FFA5CFF4FFBDDBF7FF5393CBF7FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBA
            E4FFA7D4F4FFC5E1F8FFCCE3F9FFCCE3F9FFBDDBF7FF4F90C9FDFF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF0050A8D9FF6AA5D8FFC9E1F7FFCBE3F8FF4295CAFF3182C2AEFF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF002FBAE4094FAADBEA5093CAFD4E90C8FF2F9DD2DF35A4DE19FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
        object btnSaTa: TBitBtn
          Left = 555
          Top = 113
          Width = 71
          Height = 25
          Hint = 'Salvar Tarefa'
          Caption = 'Salvar'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = btnSaTaClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000BA6A368FB969
            35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
            32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
            ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
            F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
            B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
            88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
            B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
            C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
            B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
            88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
            BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
            F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
            BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
            76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
            C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
            77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
            C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
            77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
            C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
            65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
            C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
            E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
            CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
            EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
            D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
            F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
            D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
            F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
            D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
            F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
            3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
            39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
        end
        object btnCancelar: TBitBtn
          Left = 555
          Top = 72
          Width = 71
          Height = 25
          Hint = 'Cancelar Tarefa'
          Caption = 'Cancelar'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = btnCancelarClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
            EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
            F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
            F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
            F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
            F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
            FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
            FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
            FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
            FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
            FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
            FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
            FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
            FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
            FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
            FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
            FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
            FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
            FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
            FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
            FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
            FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
            FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
            FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
        object eddtdesreal: TJvDateEdit
          Left = 648
          Top = 63
          Width = 94
          Height = 21
          DateFormat = 'dd/mm/yyyy'
          TabOrder = 7
        end
        object cbTudo: TCheckBox
          Left = 328
          Top = 113
          Width = 115
          Height = 17
          Caption = 'Toda Tela'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
        end
        object edhrdesreal: TJvMaskEdit
          Left = 766
          Top = 63
          Width = 43
          Height = 21
          EditMask = '!90:00;1;_'
          MaxLength = 5
          TabOrder = 9
          Text = '00:00'
        end
        object EdHrcolreal: TJvMaskEdit
          Left = 437
          Top = 63
          Width = 44
          Height = 21
          EditMask = '!90:00;1;_'
          MaxLength = 5
          TabOrder = 10
          Text = '00:00'
        end
        object cbOcoEn: TJvComboBox
          Left = 649
          Top = 19
          Width = 298
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 12
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Ocorr'#234'ncias'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object JvDBGrid2: TJvDBGrid
          Left = 0
          Top = 0
          Width = 1256
          Height = 141
          Align = alClient
          DataSource = dtsMonito
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = JvDBGrid1DrawColumnCell
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DESCR'
              Title.Caption = 'Ocorr'#234'ncia'
              Width = 262
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAINC'
              Title.Caption = 'Data'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOTIVO'
              Title.Caption = 'Motivo'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DT_MOTIVO'
              Title.Caption = 'Data da Ocorr'#234'ncia'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Caption = 'Usu'#225'rio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_NF'
              Title.Caption = 'NF'
              Visible = False
            end>
        end
      end
    end
    object Panel5: TPanel
      Left = 19
      Top = 50
      Width = 424
      Height = 137
      TabOrder = 7
      Visible = False
      object Panel6: TPanel
        Left = 1
        Top = 114
        Width = 422
        Height = 22
        Align = alBottom
        TabOrder = 0
        object btnFechar: TBitBtn
          Left = 398
          Top = 1
          Width = 23
          Height = 20
          Align = alRight
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          TabStop = False
          OnClick = btnFecharClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
            EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
            F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
            F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
            F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
            F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
            FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
            FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
            FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
            FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
            FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
            FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
            FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
            FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
            FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
            FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
            FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
            FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
            FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
            FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
            FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
            FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
            FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
            FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        end
      end
      object mCte: TMemo
        Left = 1
        Top = 1
        Width = 422
        Height = 113
        Align = alClient
        Color = clWhite
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
    end
    object cbStatus: TJvComboBox
      Left = 475
      Top = 14
      Width = 130
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 0
      TabOrder = 8
      Items.Strings = (
        ''
        'A Coletar'
        'Coletando'
        'Em Processo'
        'A Manifestar'
        'Em Transito'
        '')
    end
    object edOCS: TJvCalcEdit
      Left = 743
      Top = 15
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 9
      DecimalPlacesAlwaysShown = False
    end
    object edCte: TJvCalcEdit
      Left = 872
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 10
      DecimalPlacesAlwaysShown = False
    end
    object edNF: TJvCalcEdit
      Left = 927
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 11
      DecimalPlacesAlwaysShown = False
    end
    object edMan: TJvCalcEdit
      Left = 982
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 12
      DecimalPlacesAlwaysShown = False
    end
    object edSol: TJvCalcEdit
      Left = 807
      Top = 15
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 13
      DecimalPlacesAlwaysShown = False
    end
    object cbEdi: TCheckBox
      Left = 624
      Top = 13
      Width = 61
      Height = 17
      Caption = 'Com EDI '
      TabOrder = 14
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 561
    Width = 1272
    Height = 159
    Align = alClient
    TabOrder = 3
    object lblTtrans2: TLabel
      Left = 460
      Top = 96
      Width = 423
      Height = 16
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTransf: TLabel
      Left = 460
      Top = 44
      Width = 66
      Height = 13
      Caption = '                      '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTransb1: TLabel
      Left = 460
      Top = 74
      Width = 423
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lblTransc: TLabel
      Left = 460
      Top = 17
      Width = 423
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 353
      Top = 132
      Width = 173
      Height = 13
      Caption = 'F3 = Filtrar        F4 = Tirar Filtro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object JvDBGrid1: TJvDBGrid
      Left = 9
      Top = 11
      Width = 267
      Height = 142
      TabStop = False
      DataSource = dtsNF
      Options = [dgTitles, dgColumnResize, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = JvDBGrid1CellClick
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      AutoAppend = False
      AutoSort = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      AutoSizeRows = False
      RowsHeight = 17
      TitleRowHeight = 17
      BooleanEditor = False
      UseXPThemes = False
      Columns = <
        item
          Expanded = False
          FieldName = 'REG'
          Title.Alignment = taCenter
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = 10746620
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NOTFIS'
          Title.Alignment = taCenter
          Title.Caption = 'N. F.'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTI'
          Title.Alignment = taCenter
          Title.Caption = 'Volume'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PESOKG'
          Title.Alignment = taCenter
          Title.Caption = 'Peso'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLRMER'
          Title.Alignment = taCenter
          Title.Caption = 'Valor'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 63
          Visible = True
        end>
    end
    object dtTrans1: TJvDateEdit
      Left = 1039
      Top = 61
      Width = 94
      Height = 21
      DateFormat = 'dd/mm/yyyy'
      TabOrder = 1
      Visible = False
    end
    object dtTrans2: TJvDateEdit
      Left = 1039
      Top = 94
      Width = 94
      Height = 21
      DateFormat = 'dd/mm/yyyy'
      TabOrder = 2
      Visible = False
    end
    object CBT2: TCheckBox
      Left = 282
      Top = 42
      Width = 170
      Height = 18
      Caption = 'Transportador Distrib./Transf. :'
      TabOrder = 3
      OnClick = CBT2Click
    end
    object CBT1: TCheckBox
      Left = 282
      Top = 15
      Width = 170
      Height = 18
      Caption = 'Transportador Coleta :'
      TabOrder = 4
      OnClick = CBT1Click
    end
    object btnemail: TBitBtn
      Left = 282
      Top = 121
      Width = 60
      Height = 25
      Hint = 'Enviar e-mail'
      Caption = 'e-mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      TabStop = False
      OnClick = btnemailClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF8E512BB06331BB7038BE773CC17B40C27E42C28045C3
        8247C38649C3864AC3874BC3874BC3874CBD8349AC74408E512BA35D31F8F3EE
        F5ECE4FBF5F0FBF7F1FBF7F3FBF8F4FCF9F5FCF9F5FCF9F6FCF9F7FCFAF7FCFA
        F7F7F1ECEAD9CCAB7642BE6F3CFCF9F5ECD0BCF9E4D6FEECDFFEEBDFFEEBDEFE
        EBDBFEEBDCFEEADDFDEADBFDE8D8F8E0CDEACBB3F3EBE3C78B50C27646FDFBF8
        F9E3D2ECCFB9F8E1D0FDE7D6F4D5BDE9BFA0E9BFA2F4D3BDFDE6D4F7DEC9EBCA
        B0F8DBC4F8F2ECC98C50C57D50FDFBF9FDE9D8F9E1D0EBCAB3ECC5A7E3B698F7
        E7DDF7E8DEE3B697ECC3A4EAC5A9F8DAC2FCDFC6F8F3EDC88D50C9865BFDFBF9
        FDE8D7FDE6D4EDC6ABDCAA89F9ECE3FFFBF9FFFCFAF9EEE6DCA887EDBF9CFCDB
        C0FCDBC0F8F3EDC88C50CC8E66FDFBF9FDE5D3F1CCB2E3B596F9EAE0FFF9F5FE
        F3EAFEF4EDFFFBF9F9EDE5E3B08DF0C19EFCD7B7F8F3EDC88C50D09670FDFBF9
        F1CDB1E3B596F9E9DEFEF7F1FDEDE1FEEFE4FEF1E7FEF3EAFFFAF7F9ECE3E2AE
        8AF0BC95F8F4ECC88C50D39D7BFBF6F2E3B496F9E8DCFEF5EEFDE9DAFDEADCFD
        ECDEFDEDE1FEEFE4FEF1E7FFFAF6F9EAE0E2AA85F1E4D9C88C50D7AB91FDFAF8
        FCF5F1FFFCF9FFFCF9FFFCF9FFFCF9FFFCFAFFFCFAFFFCFAFFFCFBFFFDFBFFFD
        FCFBF6F3F8EFEAAB7743C89A7CD5A484D09770CC8F64CC8E62CB8E60CA8C5DC9
        8B5BC88A58C78856C68653C58450C4824DC1834DB279488E512BFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnListagem: TBitBtn
      Left = 1136
      Top = 127
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Listagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      TabStop = False
      OnClick = btnListagemClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object btnVer: TBitBtn
      Left = 1144
      Top = 6
      Width = 96
      Height = 25
      Hint = 'Ver Comprovante desta NF'
      Caption = 'Comprovante'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = btnVerClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
        9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
        C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
        9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
        E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
        AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
        DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
        F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
        D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
        F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
        DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
        F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
        E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnFiltrar: TBitBtn
      Left = 943
      Top = 6
      Width = 88
      Height = 25
      Caption = 'Filtrar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      TabStop = False
      OnClick = btnFiltrarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
        9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
        C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
        9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
        E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
        AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
        DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
        F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
        D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
        F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
        DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
        F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
        E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnExcComp: TBitBtn
      Left = 1042
      Top = 6
      Width = 96
      Height = 25
      Hint = 'Excluir Comprovante'
      Caption = 'Excluir Comprov.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnClick = btnExcCompClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95B0E3235CC20543
        BC1F59C186A6DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF8CABE12866CA2177E60579EA0164DD074FBE86A6DDC6A18CC38E68
        C08B66BE8864BB8561B9835FB47E5CB27C5AB17B58164BAE639DF4187FFF0076
        F80076EE0368E11E59C0C8926CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF0543BCAECDFEFFFFFFFFFFFFFFFFFF187FEF0543BCCA946EFFFFFF
        FFFFFFFFFFFEFFFFFDFEFEFDFEFEFCFEFEFCFEFEFC245DC28DB5F64D92FF1177
        FF2186FF408AEB245CC2CC976FFFFFFFFFFFFCFFFFFDFEFEFCFEFEFCFEFEFBFD
        FDFAFDFDFA95B0E13D76D28DB5F7B8D6FE72A8F52E6BCA94AFE2D19C73FFFFFF
        FEFEFCFEFEFCFEFEFCFDFDFBFDFDFBFDFDFAFDFDF8FBFBF993AEDF2A61C60543
        BC205AC15F6186FFFFFFD49E75FFFFFFFEFEFCFDFDFBFDFDFCFDFDFBFDFDF9FC
        FCF8FBF9F7FBF9F5FBF8F4FBF7F2FBF5F2FFFFFFB27C5AFFFFFFD5A076FFFFFF
        FDFDFCFDFDFBFDFDFAFCFCF9FCFBF7FBF9F5FBF8F4FBF7F3FBF5F2FAF3EFF8F2
        ECFFFFFFB57E5CFFFFFFD8A279FFFFFFFDFDFAFCFCFAFCFBF9FBFAF6FBF8F5FB
        F7F4FBF6F1F8F4EEF7F2EBF7F0EAF6ECE8FFFFFFB7815EFFFFFFD9A379FFFFFF
        FCFBF9FCFBF8FBF9F7FBF7F4FAF7F2F9F5F0F7F3EDF6EFEAF5EBE7F3EAE4F2E7
        DEFFFFFFBA8560FFFFFFDBA47AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBD8763FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFDDAD86E8B992E8B992E8B992E8B992E8B992E8B992E8
        B992E8B992E8B992E8B992E8B992E8B992E8B992C19170FFFFFFDBC3B6DEB492
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC79E80DBC3B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object hrTrans2: TJvMaskEdit
      Left = 1145
      Top = 92
      Width = 65
      Height = 21
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 10
      Text = '  :  '
      Visible = False
    end
    object hrTrans1: TJvMaskEdit
      Left = 1145
      Top = 60
      Width = 65
      Height = 21
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 11
      Text = '  :  '
      Visible = False
    end
  end
  object Qtarefas: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterOpen = QtarefasAfterOpen
    AfterScroll = QtarefasAfterScroll
    OnCalcFields = QtarefasCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select distinct sequencia, nro_ocs, datainc, remetente, codcon, ' +
        'filial, solicitacao, dt_coleta_prev, dt_coleta, dt_entrega_cd, d' +
        't_entrega_prev, dt_entrega, dt_saida, nm_cli, nm_rem, cid_rem,nm' +
        '_des, cid_des, codman, tipocs, saida, destinatario, dt_transbord' +
        'o1, codrmc, destino,transp_roma, transp_man, destma, destro, est' +
        'ado, nommot,status_sms, monitoramento, cod_des, ost, codfil from' +
        ' VW_MONITOR_ABR'
      'where dt_entrega is null'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by dt_entrega_prev')
    Left = 30
    Top = 104
    object QtarefasSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDATAINC: TDateTimeField
      Alignment = taCenter
      FieldName = 'DATAINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YY'
    end
    object QtarefasDT_COLETA_PREV: TStringField
      Alignment = taCenter
      FieldName = 'DT_COLETA_PREV'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_COLETA: TStringField
      Alignment = taCenter
      FieldName = 'DT_COLETA'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_ENTREGA_CD: TStringField
      Alignment = taCenter
      FieldName = 'DT_ENTREGA_CD'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_ENTREGA_PREV: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_ENTREGA_PREV'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QtarefasDT_ENTREGA: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_ENTREGA'
      ReadOnly = True
    end
    object QtarefasDT_SAIDA: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_SAIDA'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QtarefasNM_CLI: TStringField
      FieldName = 'NM_CLI'
      ReadOnly = True
    end
    object QtarefasNM_REM: TStringField
      FieldName = 'NM_REM'
      ReadOnly = True
    end
    object QtarefasCID_REM: TStringField
      FieldName = 'CID_REM'
      ReadOnly = True
      Size = 40
    end
    object QtarefasNM_DES: TStringField
      FieldName = 'NM_DES'
      ReadOnly = True
    end
    object QtarefasCID_DES: TStringField
      FieldName = 'CID_DES'
      ReadOnly = True
      Size = 40
    end
    object QtarefasCODMAN: TBCDField
      FieldName = 'CODMAN'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTIPOCS: TStringField
      FieldName = 'TIPOCS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QtarefasSAIDA: TStringField
      FieldName = 'SAIDA'
      ReadOnly = True
      Size = 8
    end
    object QtarefasDESTINATARIO: TBCDField
      FieldName = 'DESTINATARIO'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasCODCON: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDT_TRANSBORDO1: TDateTimeField
      FieldName = 'DT_TRANSBORDO1'
      ReadOnly = True
    end
    object QtarefasCODRMC: TBCDField
      FieldName = 'CODRMC'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTRANSP_ROMA: TBCDField
      FieldName = 'TRANSP_ROMA'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTRANSP_MAN: TBCDField
      FieldName = 'TRANSP_MAN'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDESTMA: TStringField
      FieldName = 'DESTMA'
      ReadOnly = True
      Size = 40
    end
    object QtarefasDESTRO: TStringField
      FieldName = 'DESTRO'
      ReadOnly = True
      Size = 40
    end
    object QtarefasStatus: TStringField
      FieldKind = fkCalculated
      FieldName = 'Status'
      Size = 15
      Calculated = True
    end
    object QtarefasSOLICITACAO: TBCDField
      FieldName = 'SOLICITACAO'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
    end
    object QtarefasESTADO: TStringField
      FieldName = 'ESTADO'
      ReadOnly = True
      Size = 2
    end
    object QtarefasNOMMOT: TStringField
      FieldName = 'NOMMOT'
      Size = 40
    end
    object QtarefasSTATUS_SMS: TBCDField
      FieldName = 'STATUS_SMS'
      Precision = 32
      Size = 0
    end
    object QtarefasOST: TBCDField
      FieldName = 'OST'
      Precision = 32
    end
    object QtarefasMONITORAMENTO: TStringField
      FieldName = 'MONITORAMENTO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QtarefasCOD_DES: TBCDField
      FieldName = 'COD_DES'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
  end
  object act: TActionList
    Images = dtmDados.IMBotoes
    Left = 144
    Top = 65534
    object acPrimeiroApanha: TAction
      Tag = 1
      Category = 'Apanha'
      Hint = 'Primeiro registro'
      ImageIndex = 3
      OnExecute = acPrimeiroApanhaExecute
    end
    object acAnteriorApanha: TAction
      Tag = 2
      Category = 'Apanha'
      Hint = 'Registro anterior'
      ImageIndex = 2
      OnExecute = acPrimeiroApanhaExecute
    end
    object acProximoApanha: TAction
      Tag = 3
      Category = 'Apanha'
      Hint = 'Pr'#243'ximo registro'
      ImageIndex = 0
      OnExecute = acPrimeiroApanhaExecute
    end
    object acUltimoApanha: TAction
      Tag = 4
      Category = 'Apanha'
      Hint = #218'ltimo registro'
      ImageIndex = 1
      OnExecute = acPrimeiroApanhaExecute
    end
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select id_ioc id, codocs cod, notfis, quanti, pesokg, vlrmer, da' +
        'tnot from rodioc')
    Left = 25
    Top = 619
    object QNFID: TBCDField
      FieldName = 'ID'
      Precision = 32
    end
    object QNFCOD: TBCDField
      FieldName = 'COD'
      Precision = 32
    end
    object QNFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QNFQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
    object QNFPESOKG: TBCDField
      FieldName = 'PESOKG'
      Precision = 14
    end
    object QNFVLRMER: TBCDField
      FieldName = 'VLRMER'
      Precision = 14
      Size = 2
    end
    object QNFDATNOT: TDateTimeField
      FieldName = 'DATNOT'
    end
  end
  object dtsNF: TDataSource
    DataSet = NF
    Left = 77
    Top = 619
  end
  object dtsMonito: TDataSource
    DataSet = QMonitoramento
    Left = 80
    Top = 292
  end
  object iml: TImageList
    Left = 28
    Bitmap = {
      494C010102000400E00210001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object Qocorr: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select id codigo, (id || '#39'-'#39' || descricao) descri from tb_ocorre' +
        'ncia'
      'where tipo = '#39'E'#39
      'order by descricao')
    Left = 28
    Top = 172
    object QocorrCODIGO: TBCDField
      FieldName = 'CODIGO'
      Precision = 32
    end
    object QocorrDESCRI: TStringField
      FieldName = 'DESCRI'
      Size = 60
    end
  end
  object dtsocorr: TDataSource
    DataSet = Qocorr
    Left = 80
    Top = 172
  end
  object pmLegenda: TPopupMenu
    Left = 80
    object Legenda1: TMenuItem
      Caption = 'Legenda'
      ImageIndex = 96
      OnClick = Legenda1Click
    end
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct c.nomeab nm_cli_os'
      'from tb_coleta t left join rodcli c on t.pagador = c.codclifor'
      'where t.dt_saida is null '
      'order by 1')
    Left = 168
    Top = 164
    object QClienteNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      ReadOnly = True
      Size = 50
    end
  end
  object QColeta: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    AfterOpen = QColetaAfterOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select t.*'
      'from tb_coleta t '
      'where sequencia = :0')
    Left = 168
    Top = 57
    object QColetaSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      Precision = 32
    end
    object QColetaDATAINC: TDateTimeField
      FieldName = 'DATAINC'
    end
    object QColetaPAGADOR: TBCDField
      FieldName = 'PAGADOR'
      Precision = 32
    end
    object QColetaREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      Precision = 32
    end
    object QColetaMUN_REM: TStringField
      FieldName = 'MUN_REM'
      Size = 100
    end
    object QColetaCOD_MUN_REM: TBCDField
      FieldName = 'COD_MUN_REM'
      Precision = 32
    end
    object QColetaDESTINATARIO: TBCDField
      FieldName = 'DESTINATARIO'
      Precision = 32
    end
    object QColetaMUN_DES: TStringField
      FieldName = 'MUN_DES'
      Size = 100
    end
    object QColetaCOD_MUN_DES: TBCDField
      FieldName = 'COD_MUN_DES'
      Precision = 32
    end
    object QColetaDT_COLETA_PREV: TDateTimeField
      FieldName = 'DT_COLETA_PREV'
    end
    object QColetaSOLICITACAO: TBCDField
      FieldName = 'SOLICITACAO'
      Precision = 32
    end
    object QColetaSOLICITANTE: TStringField
      FieldName = 'SOLICITANTE'
      Size = 100
    end
    object QColetaEMAIL_SOLIC: TStringField
      FieldName = 'EMAIL_SOLIC'
      Size = 240
    end
    object QColetaOBSERV: TStringField
      FieldName = 'OBSERV'
      Size = 3000
    end
    object QColetaFILIAL: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
    end
    object QColetaCODLINHA: TStringField
      FieldName = 'CODLINHA'
      Size = 6
    end
    object QColetaDEALER: TBCDField
      FieldName = 'DEALER'
      Precision = 32
    end
    object QColetaNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      Precision = 32
    end
    object QColetaFL_EMAIL_ENV: TBCDField
      FieldName = 'FL_EMAIL_ENV'
      Precision = 32
    end
    object QColetaDT_ENTREGA_PREV: TDateTimeField
      FieldName = 'DT_ENTREGA_PREV'
    end
    object QColetaITN_REM: TStringField
      FieldName = 'ITN_REM'
      Size = 3
    end
    object QColetaITN_DES: TStringField
      FieldName = 'ITN_DES'
      Size = 3
    end
    object QColetaDT_COLETA: TDateTimeField
      FieldName = 'DT_COLETA'
    end
    object QColetaDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
    end
    object QColetaDT_ENTREGA_CD: TDateTimeField
      FieldName = 'DT_ENTREGA_CD'
    end
    object QColetaDT_SAIDA: TDateTimeField
      FieldName = 'DT_SAIDA'
    end
    object QColetaUSER_BAIXA: TStringField
      FieldName = 'USER_BAIXA'
      Size = 30
    end
    object QColetaDT_BAIXA: TDateTimeField
      FieldName = 'DT_BAIXA'
    end
    object QColetaOCORR_COL: TBCDField
      FieldName = 'OCORR_COL'
      Precision = 32
      Size = 0
    end
    object QColetaOBS_MON: TStringField
      FieldName = 'OBS_MON'
      Size = 3000
    end
    object QColetaOCORR_ENT: TBCDField
      FieldName = 'OCORR_ENT'
      Precision = 32
      Size = 0
    end
  end
  object Query1: TADOQuery
    Connection = dtmDados.ConIntecom
    Parameters = <>
    Left = 28
    Top = 48
  end
  object Qbaixa: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 381
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 1004
      end
      item
        Name = '2'
        DataType = ftDateTime
        Size = -1
        Value = 41435d
      end>
    SQL.Strings = (
      'select DISTINCT sequencia, t.nro_ocs '
      'from tb_coleta t'
      'where t.remetente = :0 and t.destinatario =:1'
      'and trunc(t.datainc) = trunc(:2)'
      'and dt_saida is null')
    Left = 168
    Top = 104
    object QbaixaSEQUENCIA: TBCDField
      DisplayWidth = 15
      FieldName = 'SEQUENCIA'
      ReadOnly = True
      Precision = 32
    end
    object QbaixaNRO_OCS: TBCDField
      DisplayWidth = 13
      FieldName = 'NRO_OCS'
      ReadOnly = True
      Precision = 32
    end
  end
  object QFilial: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select DISTINCT (t.filial || '#39' - '#39' || e.nomeab) filial'
      'from tb_coleta t left join rodfil e on t.filial = e.codfil'
      'where t.dt_saida is null and nro_ocs is not null')
    Left = 168
    Top = 220
    object QFilialFILIAL: TStringField
      FieldName = 'FILIAL'
      ReadOnly = True
      Size = 63
    end
  end
  object dtsTarefas: TDataSource
    DataSet = Qtarefas
    Left = 80
    Top = 104
  end
  object NF: TJvMemoryData
    Capacity = 868
    FieldDefs = <
      item
        Name = 'REG'
        DataType = ftInteger
      end
      item
        Name = 'ID'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'COD'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'NOTFIS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'QUANTI'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'PESOKG'
        DataType = ftBCD
        Precision = 14
        Size = 4
      end
      item
        Name = 'VLRMER'
        DataType = ftBCD
        Precision = 14
        Size = 2
      end
      item
        Name = 'DataNF'
        DataType = ftDate
      end>
    DataSet = QNF
    Left = 132
    Top = 621
    object NFREG: TIntegerField
      FieldName = 'REG'
    end
    object NFID: TBCDField
      FieldName = 'ID'
      Precision = 32
    end
    object NFCOD: TBCDField
      FieldName = 'COD'
      Precision = 32
    end
    object NFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object NFQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
    object NFPESOKG: TBCDField
      FieldName = 'PESOKG'
      DisplayFormat = '#,##0.000'
      Precision = 14
    end
    object NFVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
    object NFDataNF: TDateField
      FieldName = 'DataNF'
    end
  end
  object Sp_Monit: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_TB_MONITORAMENTO'
    Parameters = <
      item
        DataType = ftInteger
        Value = 0
      end
      item
        DataType = ftDateTime
        Size = 16
        Value = 0d
      end
      item
        DataType = ftString
        Size = 40
        Value = Null
      end
      item
        DataType = ftInteger
        Value = 0
      end
      item
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        DataType = ftDateTime
        Size = -1
        Value = 0d
      end
      item
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        DataType = ftInteger
        Value = 0
      end
      item
        DataType = ftInteger
        Value = 0
      end>
    Left = 28
    Top = 348
  end
  object QMonitoramento: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select ocorrencia descr, motivo, datainc, dt_motivo, observacao,' +
        ' usuario, id_nf'
      'from tb_monitoramento  '
      'where id = '#39'24108'#39
      'order by datainc')
    Left = 28
    Top = 292
    object QMonitoramentoDESCR: TStringField
      FieldName = 'DESCR'
      Size = 50
    end
    object QMonitoramentoMOTIVO: TStringField
      FieldName = 'MOTIVO'
      Size = 50
    end
    object QMonitoramentoDATAINC: TDateTimeField
      FieldName = 'DATAINC'
    end
    object QMonitoramentoDT_MOTIVO: TDateTimeField
      FieldName = 'DT_MOTIVO'
    end
    object QMonitoramentoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object QMonitoramentoUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QMonitoramentoID_NF: TBCDField
      FieldName = 'ID_NF'
      Precision = 32
      Size = 0
    end
  end
end
