object frmRelCteControladoria: TfrmRelCteControladoria
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de Receita e Custos'
  ClientHeight = 531
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 53
    Width = 968
    Height = 437
    Align = alClient
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'NR_CONHECIMENTO'
        Title.Caption = 'N'#186' CTe'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FILIAL_CTE'
        Title.Caption = 'Filial'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_CTE'
        Title.Caption = 'DT_Cte'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLI'
        Title.Caption = 'Cliente'
        Width = 215
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_CLI'
        Title.Caption = 'CPNJ Cliente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_REM'
        Title.Caption = 'Remetente'
        Width = 274
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_REM'
        Title.Caption = 'Cidade Remetente'
        Width = 188
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_REM'
        Title.Caption = 'UF Rem.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_DES'
        Title.Caption = 'Destinat'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_DES'
        Title.Caption = 'CNPJ Destinat'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_DES'
        Title.Caption = 'Cidade Destinat'#225'rio'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_DES'
        Title.Caption = 'UF Dest.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_NF'
        Title.Caption = 'Valor NF'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_KG'
        Title.Caption = 'Peso KG'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_CUB'
        Title.Caption = 'Peso CUB'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QT_VOLUME'
        Title.Caption = 'Volume'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATNOT'
        Title.Caption = 'Data NF'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_NOTA'
        Title.Caption = 'N.F.'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CFOP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_FRETE'
        Title.Caption = 'Frete'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_PEDAGIO'
        Title.Caption = 'Ped'#225'gio'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_SEGURO'
        Title.Caption = 'Seguro'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_GRIS'
        Title.Caption = 'GRIS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_OUTROS'
        Title.Caption = 'Outros'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TAS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TX_CTE'
        Title.Caption = 'Taxa CTe'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TDE'
        Title.Caption = 'TDE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_SEG_BALSA'
        Title.Caption = 'Seguro Balsa'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_RED_FLUV'
        Title.Caption = 'Fluvial'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_AGENDA'
        Title.Caption = 'Agendamento'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_IMPOSTO'
        Title.Caption = 'Imposto'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TOTAL'
        Title.Caption = 'Total'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ALIQUOTA'
        Title.Caption = 'Aliquota'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_FATURA'
        Title.Caption = 'Fatura'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COD_TRANSP'
        Title.Caption = 'Cod. Transp.'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_TRANSP'
        Title.Caption = 'Transportadora'
        Width = 337
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_TRANSPORTADORA'
        Title.Caption = 'CNPJ Transportadora'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPERACAO'
        Width = 93
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OBSCON'
        Title.Caption = 'Observa'#231#227'o CT-e'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_TABELA'
        Title.Caption = 'Tabela Receita'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REGIAO'
        Title.Caption = 'Regi'#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_MANIFESTO'
        Title.Caption = 'Manifesto'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATAINC'
        Title.Caption = 'Data Manifesto'
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_ENTREGA'
        Title.Caption = 'Data Entrega'
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO_CALC'
        Title.Caption = 'Custo Calc. Distr.'
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CTE_CUSTO'
        Title.Caption = 'CTe Forn.'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO_COBRADO'
        Title.Caption = 'Custo Cobr. Distr.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_TDE'
        Title.Caption = 'TDE Cobrada'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_TOTAL_C'
        Title.Caption = 'Custo Calc. Gener.'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CTE_PAGAR'
        Title.Caption = 'CTe Forn. Gen.'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO_COBRADO'
        Title.Caption = 'Custo Forn. Gener.'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 440
    Top = 180
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 968
    Height = 53
    Align = alTop
    TabOrder = 2
    object Label10: TLabel
      Left = 12
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 115
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 224
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Cliente :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtInicial: TJvDateEdit
      Left = 12
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 0
    end
    object dtFinal: TJvDateEdit
      Left = 115
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object btRelatorio: TBitBtn
      Left = 691
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 2
      OnClick = btRelatorioClick
    end
    object ledIdCliente: TJvDBLookupEdit
      Left = 224
      Top = 27
      Width = 145
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'nr_cnpj_cpf'
      LookupField = 'NR_CNPJ_CPF'
      LookupSource = DataSource1
      TabOrder = 3
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object ledCliente: TJvDBLookupEdit
      Left = 375
      Top = 27
      Width = 303
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'nm_cliente'
      LookupField = 'NR_CNPJ_CPF'
      LookupSource = DataSource1
      TabOrder = 4
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object btnExcel: TBitBtn
      Left = 776
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Excel'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
        346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
        2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
        33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
        EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
        38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
        3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
        40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
        73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
        33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
        64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
        33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
        33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
        3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
        D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
        49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
        CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
        53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
        C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
        BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
        62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
        56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
        45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
        4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
        BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
        4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
        B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
        49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
        ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
        4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
        4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 5
      OnClick = btnExcelClick
    end
    object btnHTML: TBitBtn
      Left = 863
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Listagem'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF196B37196B37196B
        37196B37196B37FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF196B37288C5364BA8D95D2B264BA8D288C53196B37CD6E23C9651B
        C8601AC65918C25317C04E16BD4715BB4115B93E14206A3862BA8B60BA87FFFF
        FF60B98767BC8F1E6936D27735E4AF87E3AB81E1A87BDFA376DEA171DC9D6DDB
        9968DA9763317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B37D68443E7B590
        E0A374DE9E6EDC9A67DB9560D9915AD78D53D5894D48875C90D3B192D6B1FFFF
        FF65BC8C67BC8F1E6936DB8E53EABB99FCF6F2E1A679FCF5F1DD9D6BFCF6F2DA
        945EF7E9DE938E5D61AB8195D4B4BAE6D06ABB8F2D8F57605326E19762ECC1A1
        FCF7F3E5AD84FCF6F2E1A477FCF7F3DD9B69F8EBE0F0D3BEA8BAA05F956F4F8E
        66478458868654B73C13E2A06EEEC7A8FEFDFCFDF7F3FEFAF8E3AC81FCF7F4E0
        A374F9ECE3FAF2EBFDF8F4E3AE86FAF1EAD5894DDA9966BD4315E6A779F0CBB0
        FDF8F5EABA98FDF8F4E7B38CFDF8F5E3AA80F9EEE5EECDB4FDF8F5E5B38EFDF9
        F6D88F57DD9E6DC04E16EAAB80F2CFB5FCF4EEECBF9FFBF3EDFDF8F4FDF7F4FC
        F7F3F4DBC9E7B48EF7E6DAE3AD83F6E4D6DB9762DFA376C45918EAAB80F3D0B7
        EFC6A9EFC4A6EEC2A2ECBF9EEBBC98E9B893E8B48EE6B088E3AC81E2A77BE0A3
        74DE9E6EE2AA80C9621AEAAB80F3D0B7F3D0B7F3D0B7F2D0B7F1CEB3F0CBB0EF
        C9ACEEC6A8EDC2A3EBC09EEABB99E8B794E6B48FE4B089CD6E23EAAB80EAAB80
        EAAB80EAAB80EAAB80EAAB80E8A97CE6A477E2A070E29B6BE19762DD9059D98B
        52D88549D6803ED27735FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 6
      OnClick = btnHTMLClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 490
    Width = 968
    Height = 41
    Align = alBottom
    TabOrder = 3
    object Gauge1: TGauge
      Left = 205
      Top = 9
      Width = 755
      Height = 23
      Progress = 0
    end
    object JvDBNavigator1: TJvDBNavigator
      Left = 8
      Top = 7
      Width = 188
      Height = 25
      DataSource = navnavig
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 0
    end
  end
  object QCTe: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct *'
      'from vw_rel_cte_controladoria'
      'where nr_conhecimento > 0'
      'and nr_conhecimento = 2045'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 56
    Top = 104
    object QCTeNR_CONHECIMENTO: TFMTBCDField
      FieldName = 'NR_CONHECIMENTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeDT_CTE: TDateTimeField
      FieldName = 'DT_CTE'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCTeFILIAL_CTE: TFMTBCDField
      FieldName = 'FILIAL_CTE'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeNM_CLI: TStringField
      FieldName = 'NM_CLI'
      ReadOnly = True
      Size = 80
    end
    object QCTeCNPJ_CLI: TStringField
      FieldName = 'CNPJ_CLI'
      ReadOnly = True
    end
    object QCTeNM_CLIENTE_REM: TStringField
      FieldName = 'NM_CLIENTE_REM'
      ReadOnly = True
      Size = 80
    end
    object QCTeCIDADE_REM: TStringField
      FieldName = 'CIDADE_REM'
      ReadOnly = True
      Size = 40
    end
    object QCTeUF_REM: TStringField
      FieldName = 'UF_REM'
      ReadOnly = True
      Size = 2
    end
    object QCTeNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      ReadOnly = True
      Size = 80
    end
    object QCTeNR_CNPJ_CPF_DES: TStringField
      FieldName = 'NR_CNPJ_CPF_DES'
      ReadOnly = True
    end
    object QCTeCIDADE_DES: TStringField
      FieldName = 'CIDADE_DES'
      ReadOnly = True
      Size = 40
    end
    object QCTeUF_DES: TStringField
      FieldName = 'UF_DES'
      ReadOnly = True
      Size = 2
    end
    object QCTeVL_NF: TFMTBCDField
      FieldName = 'VL_NF'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTePESO_KG: TFMTBCDField
      FieldName = 'PESO_KG'
      ReadOnly = True
      DisplayFormat = '#,##0.000'
      Precision = 38
      Size = 4
    end
    object QCTePESO_CUB: TFMTBCDField
      FieldName = 'PESO_CUB'
      ReadOnly = True
      DisplayFormat = '#,##0.000'
      Precision = 38
      Size = 4
    end
    object QCTeQT_VOLUME: TFMTBCDField
      FieldName = 'QT_VOLUME'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QCTeDATNOT: TDateTimeField
      FieldName = 'DATNOT'
      ReadOnly = True
    end
    object QCTeNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
      ReadOnly = True
    end
    object QCTeCFOP: TStringField
      FieldName = 'CFOP'
      ReadOnly = True
      Size = 5
    end
    object QCTeVL_FRETE: TFMTBCDField
      FieldName = 'VL_FRETE'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_PEDAGIO: TFMTBCDField
      FieldName = 'VL_PEDAGIO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_SEGURO: TFMTBCDField
      FieldName = 'VL_SEGURO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_GRIS: TFMTBCDField
      FieldName = 'VL_GRIS'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_OUTROS: TFMTBCDField
      FieldName = 'VL_OUTROS'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeTAS: TFMTBCDField
      FieldName = 'TAS'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_TX_CTE: TFMTBCDField
      FieldName = 'VL_TX_CTE'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_TDE: TFMTBCDField
      FieldName = 'VL_TDE'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_SEG_BALSA: TFMTBCDField
      FieldName = 'VL_SEG_BALSA'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_RED_FLUV: TFMTBCDField
      FieldName = 'VL_RED_FLUV'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_AGENDA: TFMTBCDField
      FieldName = 'VL_AGENDA'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 38
      Size = 4
    end
    object QCTeVL_IMPOSTO: TFloatField
      FieldName = 'VL_IMPOSTO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
    end
    object QCTeVL_TOTAL: TFloatField
      FieldName = 'VL_TOTAL'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
    end
    object QCTeVL_ALIQUOTA: TFloatField
      FieldName = 'VL_ALIQUOTA'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
    end
    object QCTeNR_FATURA: TFMTBCDField
      FieldName = 'NR_FATURA'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QCTeOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
    object QCTeOBSCON: TStringField
      FieldName = 'OBSCON'
      ReadOnly = True
      Size = 500
    end
    object QCTeNR_TABELA: TFMTBCDField
      FieldName = 'NR_TABELA'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeREGIAO: TStringField
      FieldName = 'REGIAO'
      ReadOnly = True
      Size = 12
    end
    object QCTeNR_MANIFESTO: TFMTBCDField
      FieldName = 'NR_MANIFESTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCTeDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCTeNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 80
    end
    object QCTeCNPJ_TRANSPORTADORA: TStringField
      FieldName = 'CNPJ_TRANSPORTADORA'
      ReadOnly = True
    end
    object QCTeCOD_TRANSP: TFMTBCDField
      FieldName = 'COD_TRANSP'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QCTeCTE_CUSTO: TFMTBCDField
      FieldName = 'CTE_CUSTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeVALOR_COBRADO: TBCDField
      FieldName = 'VALOR_COBRADO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object QCTeCUSTO_CALC: TBCDField
      FieldName = 'CUSTO_CALC'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object QCTeFL_TDE: TStringField
      FieldName = 'FL_TDE'
      ReadOnly = True
      Size = 1
    end
    object QCTeVLR_TOTAL_C: TBCDField
      FieldName = 'VLR_TOTAL_C'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 16
      Size = 2
    end
    object QCTeNR_CTE_PAGAR: TFMTBCDField
      FieldName = 'NR_CTE_PAGAR'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QCTeCUSTO_COBRADO: TBCDField
      FieldName = 'CUSTO_COBRADO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 16
      Size = 2
    end
    object QCTeSERVICO: TStringField
      FieldName = 'SERVICO'
      ReadOnly = True
      Size = 50
    end
  end
  object navnavig: TDataSource
    DataSet = QCTe
    Left = 116
    Top = 104
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct c.codclifor cod_cliente, c.razsoc nm_cliente, c.' +
        'codcgc nr_cnpj_cpf '
      
        'from cyber.rodcli c left join tb_conhecimento P on p.cod_pagador' +
        ' = c.codclifor '
      'where p.fl_status = '#39'A'#39' '
      'order by nm_cliente')
    Left = 56
    Top = 52
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 116
    Top = 52
  end
end
