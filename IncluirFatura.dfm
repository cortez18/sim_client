object frmIncluirFatura: TfrmIncluirFatura
  Left = 332
  Top = 256
  BorderStyle = bsSingle
  Caption = 'Emitir Fatura'
  ClientHeight = 487
  ClientWidth = 1124
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poMainFormCenter
  Scaled = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1124
    Height = 101
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 1016
    object JvGradient1: TJvGradient
      Left = 1
      Top = 1
      Width = 1122
      Height = 20
      Align = alTop
      Style = grVertical
      StartColor = 16245453
      EndColor = clBtnFace
      ExplicitWidth = 789
    end
    object Label1: TLabel
      Left = 784
      Top = 7
      Width = 30
      Height = 13
      Caption = 'Fatura'
      Transparent = True
    end
    object Label10: TLabel
      Left = 7
      Top = 7
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 104
      Top = 7
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 225
      Top = 7
      Width = 32
      Height = 13
      Caption = 'Cliente'
      Transparent = True
    end
    object Label7: TLabel
      Left = 53
      Top = 50
      Width = 47
      Height = 13
      Caption = 'Opera'#231#227'o'
      Transparent = True
    end
    object Label15: TLabel
      Left = 335
      Top = 50
      Width = 34
      Height = 13
      Caption = 'Regi'#227'o'
      Transparent = True
    end
    object Label9: TLabel
      Left = 515
      Top = 50
      Width = 62
      Height = 13
      Caption = 'Tipo de CT-e'
      Transparent = True
    end
    object Label16: TLabel
      Left = 670
      Top = 48
      Width = 89
      Height = 16
      Caption = 'Tipo de Servi'#231'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object btFatura: TBitBtn
      Left = 987
      Top = 63
      Width = 101
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
        FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
        96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
        92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
        BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
        CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
        D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
        EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
        E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
        E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
        8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
        E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
        FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
        C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
        CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
        CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
        D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
        9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 0
      TabStop = False
      OnClick = btFaturaClick
    end
    object btVerificar: TBitBtn
      Left = 880
      Top = 63
      Width = 101
      Height = 25
      Caption = 'Carregar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF0051A6
        58B84FA356AEFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00418E4517408D43EC3F8B42B3FF00FF0054AB5CB369B3
        6FFF67B16DFF4FA456AEFF00FF004C9F52FFFF00FF00FF00FF00FF00FF00FF00
        FF00459449FF43924817429046F15CA160FF569C5AFF3F8C42B856AD5FEC71B8
        77FF83BF89FF68B16EFF50A457FF4EA255FFFF00FF00FF00FF00FF00FF00FF00
        FF0046964BFF4A974EFF5FA462FF71B075FF579E5BFF408D44AE58B0611757AE
        5FF171B877FF74B87BFF80BD86FF50A457FFFF00FF00FF00FF00FF00FF00FF00
        FF0048994DFF73B377FF63A968FF5AA15EFF429147AEFF00FF00FF00FF0058B0
        61175CB064FF84C18AFF86C18BFF52A759FFFF00FF00FF00FF00FF00FF00FF00
        FF004A9B4FFF79B67EFF74B379FF45954AFFFF00FF00FF00FF00FF00FF005AB3
        63FF59B161FF57AE5FFF55AC5DFF54AA5BFFFF00FF00FF00FF00FF00FF00FF00
        FF004B9E51FF4A9C50FF48994EFF47974CFF45954AFFFF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0062BF
        6DFF61BD6CFF60BB6AFF5EB968FF5DB767FFFF00FF00FF00FF00FF00FF00FF00
        FF0055AB5DFF53A95BFF52A759FF50A457FF4EA255FFFF00FF00FF00FF00FF00
        FF0063BF6DFF97D19FFF98D0A0FF5FB969FFFF00FF00FF00FF00FF00FF00FF00
        FF0057AE5FFF8CC692FF87C38DFF57AA5EFF50A55717FF00FF00FF00FF0065C3
        70AE7ECA87FF8ECD98FF97D19FFF60BC6BFFFF00FF00FF00FF00FF00FF00FF00
        FF0059B161FF8CC793FF7DBF85FF72B979FF52A759F150A5571767C673AE80CD
        8AFF9ED6A7FF83CC8CFF69C273FF62BE6CFFFF00FF00FF00FF00FF00FF00FF00
        FF005AB364FF59B162FF70BB78FF8DC794FF73BA7AFF52A85AEC68C774B881CE
        8BFF86CF90FF65C371F164C26F1763C06EFFFF00FF00FF00FF00FF00FF00FF00
        FF005CB666FFFF00FF0059B162AE71BB79FF6FB977FF54AA5CB3FF00FF0068C7
        74B367C673EC66C57217FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0059B262AE58AF60B8FF00FF00}
      TabOrder = 1
      TabStop = False
      OnClick = btVerificarClick
    end
    object dtInicial: TJvDateEdit
      Left = 7
      Top = 23
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 2
    end
    object dtFinal: TJvDateEdit
      Left = 104
      Top = 23
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 3
    end
    object ledIdCliente: TJvDBLookupEdit
      Left = 225
      Top = 23
      Width = 149
      Height = 21
      LookupDisplay = 'NR_CNPJ_CPF'
      LookupField = 'COD_CLIENTE'
      LookupSource = dtsClientes
      CharCase = ecUpperCase
      TabOrder = 4
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object ledCliente: TJvDBLookupEdit
      Left = 380
      Top = 23
      Width = 345
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'NM_CLIENTE'
      LookupField = 'COD_CLIENTE'
      LookupSource = dtsClientes
      CharCase = ecUpperCase
      TabOrder = 5
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object cbOperacao: TJvDBLookupEdit
      Left = 53
      Top = 66
      Width = 267
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'OPERACAO'
      LookupField = 'OPERACAO'
      LookupSource = dtsOperacao
      CharCase = ecUpperCase
      TabOrder = 6
      Text = ''
    end
    object edFatura: TEdit
      Left = 785
      Top = 23
      Width = 70
      Height = 24
      TabStop = False
      Color = 15329769
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
    end
    object cbT: TCheckBox
      Left = 16
      Top = 68
      Width = 13
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 9
      OnClick = cbTClick
    end
    object Panel1: TPanel
      Left = 726
      Top = 22
      Width = 52
      Height = 21
      Caption = 'Regra'
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 10
      Visible = False
      OnClick = Panel1Click
    end
    object btnListagem: TBitBtn
      Left = 1184
      Top = 63
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Listagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      TabStop = False
      OnClick = btnListagemClick
    end
    object RGTipo: TRadioGroup
      Left = 888
      Top = 10
      Width = 265
      Height = 37
      Caption = 'Tipo'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Normal'
        'Especial')
      TabOrder = 12
    end
    object btnRelat: TBitBtn
      Left = 1098
      Top = 63
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Relat'#243'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      TabStop = False
      OnClick = btnRelatClick
    end
    object btnSKF: TBitBtn
      Left = 1184
      Top = 21
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Rel. SKF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 14
      TabStop = False
      OnClick = btnSKFClick
    end
    object cbRegiao: TJvComboBox
      Left = 335
      Top = 66
      Width = 145
      Height = 21
      TabOrder = 7
      Text = ''
      Items.Strings = (
        ''
        'Norte'
        'Nordeste'
        'Centro-Oeste'
        'Sudeste'
        'Sul')
    end
    object cbTipo: TJvComboBox
      Left = 515
      Top = 66
      Width = 145
      Height = 21
      TabOrder = 15
      Text = ''
      Items.Strings = (
        ''
        'Normal'
        'Complementar'
        'Devolu'#231#227'o'
        'Reentrega')
    end
    object cbServico: TComboBox
      Left = 670
      Top = 66
      Width = 204
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 328
    Width = 1124
    Height = 159
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 1016
    object Label2: TLabel
      Left = 425
      Top = 120
      Width = 102
      Height = 16
      Caption = 'Total Selecionado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 15
      Top = 120
      Width = 124
      Height = 16
      Caption = 'Data de Vencimento :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 657
      Top = 120
      Width = 52
      Height = 16
      Caption = 'Desconto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label5: TLabel
      Left = 830
      Top = 120
      Width = 63
      Height = 16
      Caption = 'Total Geral'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCtrc: TLabel
      Left = 278
      Top = 6
      Width = 4
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 7
      Top = 5
      Width = 76
      Height = 16
      Caption = 'Observa'#231#227'o :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edTotal: TJvCalcEdit
      Left = 537
      Top = 116
      Width = 110
      Height = 27
      Color = clYellow
      DisplayFormat = '##,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edVcto: TJvDateEdit
      Left = 151
      Top = 116
      Width = 121
      Height = 27
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
      OnExit = edVctoExit
    end
    object btnImprimir: TBitBtn
      Left = 283
      Top = 116
      Width = 101
      Height = 30
      Caption = 'Imprimir'
      Enabled = False
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 2
    end
    object edDesc: TJvCalcEdit
      Left = 717
      Top = 116
      Width = 88
      Height = 27
      Color = clYellow
      DisplayFormat = '##,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      Visible = False
      DecimalPlacesAlwaysShown = False
      OnExit = edDescExit
    end
    object edTT: TJvCalcEdit
      Left = 899
      Top = 116
      Width = 110
      Height = 27
      Color = clYellow
      DisplayFormat = '##,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 4
      DecimalPlacesAlwaysShown = False
    end
    object EdObs: TJvMemo
      Left = 7
      Top = 27
      Width = 1002
      Height = 58
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 255
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnKeyDown = JvMemo1KeyDown
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 101
    Width = 1124
    Height = 227
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 2
    ExplicitWidth = 1016
    object dbgFatura: TJvDBUltimGrid
      Left = 1
      Top = 1
      Width = 1122
      Height = 225
      Align = alClient
      Ctl3D = False
      DataSource = ds
      DrawingStyle = gdsClassic
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentCtl3D = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -9
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dbgFaturaCellClick
      OnDrawColumnCell = dbgFaturaDrawColumnCell
      OnTitleClick = dbgFaturaTitleClick
      TitleArrow = True
      AutoSizeColumns = True
      SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'reg'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBtnFace
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ctrc'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'CT-e'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clRed
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dt_cte'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cliente'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 174
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'expedidor'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Expedidor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'destinatario'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Destinat'#225'rio'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'consignatario'
          Title.Alignment = taCenter
          Title.Caption = 'Consignat'#225'rio'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'valor'
          Title.Alignment = taCenter
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'operacao'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'Opera'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 171
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cfop'
          Title.Alignment = taCenter
          Title.Caption = 'CFOP'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'status'
          Title.Caption = 'Status'
          Width = 77
          Visible = True
        end>
    end
    object JvMemo1: TJvMemo
      Left = 320
      Top = 64
      Width = 441
      Height = 201
      Hint = 'Clique <ESC> para fechar'
      Color = 8454143
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      OnKeyDown = JvMemo1KeyDown
    end
    object Panel8: TPanel
      Left = 495
      Top = 162
      Width = 303
      Height = 51
      Caption = 'Gerando.....'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      Visible = False
    end
  end
  object JvEnterAsTab1: TJvEnterAsTab
    Left = 400
    Top = 232
  end
  object ds: TDataSource
    DataSet = mdTemp
    Left = 564
    Top = 280
  end
  object mdTemp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'reg'
        DataType = ftInteger
      end>
    Left = 504
    Top = 280
    object mdTempreg: TIntegerField
      FieldName = 'reg'
    end
    object mdTempctrc: TIntegerField
      FieldName = 'ctrc'
    end
    object mdTempcliente: TStringField
      FieldName = 'cliente'
      Size = 50
    end
    object mdTempexpedidor: TStringField
      FieldName = 'expedidor'
      Size = 50
    end
    object mdTempdestinatario: TStringField
      FieldName = 'destinatario'
      Size = 50
    end
    object mdTempconsignatario: TStringField
      FieldName = 'consignatario'
      Size = 50
    end
    object mdTempvalor: TFloatField
      FieldName = 'valor'
      DisplayFormat = '###,##0.00'
    end
    object mdTempoperacao: TStringField
      FieldName = 'operacao'
      Size = 50
    end
    object mdTempFL_TIPO: TStringField
      FieldName = 'FL_TIPO'
      FixedChar = True
      Size = 1
    end
    object mdTempserie: TStringField
      FieldName = 'serie'
    end
    object mdTempgnre: TStringField
      FieldName = 'gnre'
    end
    object mdTempcfop: TIntegerField
      FieldName = 'cfop'
    end
    object mdTemppago: TIntegerField
      FieldName = 'pago'
    end
    object mdTempRegra: TIntegerField
      FieldName = 'Regra'
    end
    object mdTempdt_cte: TDateField
      FieldName = 'dt_cte'
      DisplayFormat = 'dd/mm/yy'
    end
    object mdTempstatus: TStringField
      FieldName = 'status'
    end
  end
  object QCTRC: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select * from vw_faturar where fl_empresa = :0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by nr_conhecimento')
    Left = 500
    Top = 216
    object QCTRCNR_CONHECIMENTO: TBCDField
      FieldName = 'NR_CONHECIMENTO'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object QCTRCDT_CONHECIMENTO: TDateTimeField
      FieldName = 'DT_CONHECIMENTO'
      ReadOnly = True
    end
    object QCTRCNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      ReadOnly = True
      Size = 50
    end
    object QCTRCNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      ReadOnly = True
      Size = 50
    end
    object QCTRCVL_TOTAL: TBCDField
      FieldName = 'VL_TOTAL'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 9
      Size = 2
    end
    object QCTRCNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QCTRCOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
    end
    object QCTRCNM_CLIENTE_CON: TStringField
      FieldName = 'NM_CLIENTE_CON'
      ReadOnly = True
      Size = 50
    end
    object QCTRCCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCTRCCNPJLOJA: TStringField
      FieldName = 'CNPJLOJA'
    end
    object QCTRCNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 5
    end
    object QCTRCCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object QCTRCNATOPE: TBCDField
      FieldName = 'NATOPE'
      Precision = 32
    end
    object QCTRCNMGNRE: TStringField
      FieldName = 'NMGNRE'
      Size = 80
    end
    object QCTRCPAGOU: TBCDField
      FieldName = 'PAGOU'
      ReadOnly = True
      Precision = 32
    end
    object QCTRCUFCL: TStringField
      FieldName = 'UFCL'
      ReadOnly = True
      Size = 2
    end
    object QCTRCUFRE: TStringField
      FieldName = 'UFRE'
      ReadOnly = True
      Size = 2
    end
    object QCTRCUFDE: TStringField
      FieldName = 'UFDE'
      ReadOnly = True
      Size = 2
    end
    object QCTRCITNLIN: TStringField
      FieldName = 'ITNLIN'
      Size = 6
    end
    object QCTRCTIPCON: TStringField
      FieldName = 'TIPCON'
      Size = 1
    end
    object QCTRCFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object QCTRCLimite: TBCDField
      FieldName = 'Limite'
    end
    object QCTRCprazo_pgto: TBCDField
      FieldName = 'prazo_pgto'
    end
    object QCTRCdia_semana: TBCDField
      FieldName = 'dia_semana'
    end
    object QCTRCPRAZO_ESPECIAL: TStringField
      FieldName = 'PRAZO_ESPECIAL'
      Size = 1
    end
    object QCTRCCNPJ_CLIENTE: TStringField
      FieldName = 'CNPJ_CLIENTE'
    end
    object QCTRCFL_TIPO_2: TStringField
      FieldName = 'FL_TIPO_2'
      Size = 1
    end
    object QCTRCfl_fatura: TStringField
      FieldName = 'fl_fatura'
      Size = 1
    end
  end
  object QOperacao: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QOperacaoBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct c.operacao'
      'from vw_faturar c'
      'where c.fl_empresa = :0'
      'and c.cod_cliente = :1'
      ''
      ''
      ''
      ''
      ''
      ''
      'order by 1')
    Left = 500
    Top = 164
    object QOperacaoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 200
    end
  end
  object dtsOperacao: TDataSource
    DataSet = QOperacao
    Left = 560
    Top = 164
  end
  object QClientes: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClientesBeforeOpen
    Parameters = <
      item
        Name = '1'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct w.cod_cliente, w.nm_cliente, w.cnpj_cliente nr_c' +
        'npj_cpf'
      'from vw_faturar w '
      'where w.fl_empresa = :1'
      'order by 2')
    Left = 340
    Top = 172
    object QClientesCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      Precision = 32
      Size = 0
    end
    object QClientesNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      Size = 50
    end
    object QClientesNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
    end
  end
  object dtsClientes: TDataSource
    DataSet = QClientes
    Left = 408
    Top = 164
  end
  object iml: TImageList
    Left = 349
    Top = 236
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object SPFatura: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_GRAVA_FATURA'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VCODCLI'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VVCTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VTOTAL'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VUSUA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VEMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VDESC'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VOBS'
        DataType = ftString
        Size = 255
        Value = ''
      end>
    Left = 784
    Top = 260
  end
  object SPITens: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_GRAVA_ITENS_FATURA'
    Parameters = <
      item
        Name = 'VFAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VUSUA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VEMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VCTE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end>
    Left = 784
    Top = 324
  end
  object QRelat: TADOQuery
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 7
        Value = '5013177'
      end>
    SQL.Strings = (
      'select * from VW_itc_transporte'
      'where fatura = :0')
    Left = 224
    Top = 164
    object QRelatNR_CTRC: TBCDField
      FieldName = 'NR_CTRC'
      Precision = 32
    end
    object QRelatDT_CTRC: TDateTimeField
      FieldName = 'DT_CTRC'
    end
    object QRelatNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 3
    end
    object QRelatNM_CLI: TStringField
      FieldName = 'NM_CLI'
      Size = 80
    end
    object QRelatCNPJ_CLI: TStringField
      FieldName = 'CNPJ_CLI'
    end
    object QRelatNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_EXP: TStringField
      FieldName = 'NR_CNPJ_CPF_EXP'
    end
    object QRelatCIDADE_EXP: TStringField
      FieldName = 'CIDADE_EXP'
      Size = 40
    end
    object QRelatUF_EXP: TStringField
      FieldName = 'UF_EXP'
      Size = 2
    end
    object QRelatNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_DES: TStringField
      FieldName = 'NR_CNPJ_CPF_DES'
    end
    object QRelatCIDADE_DES: TStringField
      FieldName = 'CIDADE_DES'
      Size = 40
    end
    object QRelatUF_DES: TStringField
      FieldName = 'UF_DES'
      Size = 2
    end
    object QRelatNM_CLIENTE_RED: TStringField
      FieldName = 'NM_CLIENTE_RED'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_RED: TStringField
      FieldName = 'NR_CNPJ_CPF_RED'
    end
    object QRelatCIDADE_RED: TStringField
      FieldName = 'CIDADE_RED'
      Size = 40
    end
    object QRelatUF_RED: TStringField
      FieldName = 'UF_RED'
      Size = 2
    end
    object QRelatVL_NF: TBCDField
      FieldName = 'VL_NF'
      Precision = 32
    end
    object QRelatPESO_KG: TBCDField
      FieldName = 'PESO_KG'
      Precision = 32
    end
    object QRelatQT_VOLUME: TBCDField
      FieldName = 'QT_VOLUME'
      Precision = 32
    end
    object QRelatNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
    end
    object QRelatVL_FRETE: TBCDField
      FieldName = 'VL_FRETE'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_SEGURO: TBCDField
      FieldName = 'VL_SEGURO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_GRIS: TBCDField
      FieldName = 'VL_GRIS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_TOTAL: TBCDField
      FieldName = 'VL_TOTAL'
      DisplayFormat = '##,##0.00'
      Precision = 14
      Size = 2
    end
    object QRelatCAT: TBCDField
      FieldName = 'CAT'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ADEME: TBCDField
      FieldName = 'VL_ADEME'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_DESPACHO: TBCDField
      FieldName = 'VL_DESPACHO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_DESCARGA: TBCDField
      FieldName = 'VL_DESCARGA'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ENTREGA: TBCDField
      FieldName = 'VL_ENTREGA'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_TDE: TBCDField
      FieldName = 'VL_TDE'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ADICIONAL: TBCDField
      FieldName = 'VL_ADICIONAL'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatDESCRICAO_ADICIONAL: TStringField
      FieldName = 'DESCRICAO_ADICIONAL'
      Size = 30
    end
    object QRelatVL_ALIQUOTA: TBCDField
      FieldName = 'VL_ALIQUOTA'
      DisplayFormat = '##,##0.00'
      Precision = 8
    end
    object QRelatVL_IMPOSTO: TBCDField
      FieldName = 'VL_IMPOSTO'
      DisplayFormat = '##,##0.00'
      Precision = 14
      Size = 2
    end
    object QRelatFL_TIPO: TStringField
      FieldName = 'FL_TIPO'
      Size = 100
    end
    object QRelatNR_FATURA: TBCDField
      FieldName = 'NR_FATURA'
      Precision = 32
    end
    object QRelatCNPJ_TRANSPORTADORA: TStringField
      FieldName = 'CNPJ_TRANSPORTADORA'
    end
    object QRelatIESTADUAL: TStringField
      FieldName = 'IESTADUAL'
      Size = 16
    end
    object QRelatOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 200
    end
    object QRelatCFOP_CLESS: TStringField
      FieldName = 'CFOP_CLESS'
      Size = 6
    end
    object QRelatTIPCON: TStringField
      FieldName = 'TIPCON'
      FixedChar = True
      Size = 1
    end
    object QRelatFATURA: TBCDField
      FieldName = 'FATURA'
      Precision = 32
    end
    object QRelatPESO_CUB: TBCDField
      FieldName = 'PESO_CUB'
      Precision = 32
    end
    object QRelatCTE_ID: TStringField
      FieldName = 'CTE_ID'
      Size = 48
    end
    object QRelatOBSCON: TStringField
      FieldName = 'OBSCON'
      Size = 500
    end
  end
  object dtsRelat: TDataSource
    DataSet = QRelat
    Left = 272
    Top = 164
  end
  object QSKF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select i.numdup, c.codcon, c.sercon, c.ctedta, c.totfre, c.icmvl' +
        'r, c.bascal, c.cte_id, c.natope, c.codrem, c.alicot, i.fildoc, e' +
        '.estado'
      
        'from cyber.rodidu i left join cyber.rodcon c on i.coddoc = c.cod' +
        'con and i.fildoc = c.codfil and i.serdoc = c.sercon'
      
        '                    left join cyber.rodfil f on f.codfil = c.cod' +
        'fil'
      
        '                    left join cyber.rodmun e on e.codmun = f.cod' +
        'mun'
      'where i.numdup = :0'
      'and i.tipdoc = '#39'CTRC'#39)
    Left = 784
    Top = 168
    object QSKFNUMDUP: TBCDField
      FieldName = 'NUMDUP'
      Precision = 32
    end
    object QSKFCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QSKFSERCON: TStringField
      FieldName = 'SERCON'
      Size = 3
    end
    object QSKFTOTFRE: TBCDField
      FieldName = 'TOTFRE'
      Precision = 14
      Size = 2
    end
    object QSKFICMVLR: TBCDField
      FieldName = 'ICMVLR'
      Precision = 14
      Size = 2
    end
    object QSKFBASCAL: TBCDField
      FieldName = 'BASCAL'
      Precision = 14
      Size = 2
    end
    object QSKFCTE_ID: TStringField
      FieldName = 'CTE_ID'
      Size = 48
    end
    object QSKFNATOPE: TBCDField
      FieldName = 'NATOPE'
      Precision = 32
    end
    object QSKFCODREM: TBCDField
      FieldName = 'CODREM'
      Precision = 32
    end
    object QSKFALICOT: TBCDField
      FieldName = 'ALICOT'
      Precision = 8
    end
    object QSKFFILDOC: TBCDField
      FieldName = 'FILDOC'
      Precision = 32
    end
    object QSKFCTEDTA: TDateTimeField
      FieldName = 'CTEDTA'
    end
    object QSKFESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select notfis'
      'from cyber.rodnfc n'
      'where n.codcon = :0 and n.sercon = :1 and n.codfil = :2')
    Left = 828
    Top = 168
    object QNFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
  end
  object QPrazo: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select prazo_pgto, dia_semana'
      'from tb_parametro'
      'where codcli = :0')
    Left = 900
    Top = 168
    object QPrazoprazo_pgto: TBCDField
      FieldName = 'prazo_pgto'
    end
    object QPrazodis_semana: TBCDField
      FieldName = 'dis_semana'
    end
  end
  object QRegra: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 7
        Value = '5013177'
      end>
    SQL.Strings = (
      
        'select count(*) tem from tb_parametro where codcli = :0 and fatu' +
        'ramento = '#39'S'#39)
    Left = 228
    Top = 260
    object QRegraTEM: TFMTBCDField
      FieldName = 'TEM'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
  end
  object QServico: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select descr_servico from crm_tb_tiposerv order by 1')
    Left = 968
    Top = 166
    object QServicoDESCR_SERVICO: TStringField
      FieldName = 'DESCR_SERVICO'
      Size = 30
    end
  end
end
