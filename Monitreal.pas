unit MonitReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ImgList, JvToolEdit, DBCtrls, JvComponentBase,
  JvBalloonHint, ActnList, ADODB, StdCtrls, JvExStdCtrls, JvRichEdit, ComObj,
  JvDBRichEdit, JvBaseEdits, JvDBControls, JvCombobox, Mask, JvExMask,
  JvMaskEdit, ExtCtrls, Buttons, JvDBUltimGrid, ComCtrls, JvExComCtrls,
  JvComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvExControls, JvGradient,
  JvFullColorSpaces, Menus, JvXPCore, JvXPButtons, JvFullColorCtrls, inifiles,
  JvMemoryDataset, ShellAPI;

Const
  InputBoxMessage = WM_USER + 200;

type
  TfrmMonitReal = class(TForm)
    Qtarefas: TADOQuery;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    JvPageControl1: TJvPageControl;
    TBColetas: TTabSheet;
    QNF: TADOQuery;
    dtsNF: TDataSource;
    iml: TImageList;
    Panel1: TPanel;
    Qocorr: TADOQuery;
    dtsocorr: TDataSource;
    DBGrid1: TJvDBGrid;
    pnLegenda: TPanel;
    JvGradient8: TJvGradient;
    lblColetaHoje: TJvFullColorLabel;
    lblColetaAtrasada: TJvFullColorLabel;
    pmLegenda: TPopupMenu;
    Legenda1: TMenuItem;
    lblColetaPontual: TJvFullColorLabel;
    QCliente: TADOQuery;
    QClienteNM_CLI_OS: TStringField;
    SMS1: TJvFullColorLabel;
    SMS2: TJvFullColorLabel;
    SMS3: TJvFullColorLabel;
    SMS9: TJvFullColorLabel;
    SMSnulo: TJvFullColorLabel;
    QocorrCODIGO: TBCDField;
    QocorrDESCRI: TStringField;
    QColeta: TADOQuery;
    QColetaSEQUENCIA: TBCDField;
    QColetaDATAINC: TDateTimeField;
    QColetaPAGADOR: TBCDField;
    QColetaREMETENTE: TBCDField;
    QColetaMUN_REM: TStringField;
    QColetaCOD_MUN_REM: TBCDField;
    QColetaDESTINATARIO: TBCDField;
    QColetaMUN_DES: TStringField;
    QColetaCOD_MUN_DES: TBCDField;
    QColetaDT_COLETA_PREV: TDateTimeField;
    QColetaSOLICITACAO: TBCDField;
    QColetaSOLICITANTE: TStringField;
    QColetaEMAIL_SOLIC: TStringField;
    QColetaOBSERV: TStringField;
    QColetaFILIAL: TBCDField;
    QColetaCODLINHA: TStringField;
    QColetaDEALER: TBCDField;
    QColetaNRO_OCS: TBCDField;
    QColetaFL_EMAIL_ENV: TBCDField;
    QColetaDT_ENTREGA_PREV: TDateTimeField;
    QColetaITN_REM: TStringField;
    QColetaITN_DES: TStringField;
    QColetaDT_COLETA: TDateTimeField;
    QColetaDT_ENTREGA: TDateTimeField;
    QColetaDT_ENTREGA_CD: TDateTimeField;
    QColetaDT_SAIDA: TDateTimeField;
    QNFID: TBCDField;
    QNFCOD: TBCDField;
    QNFNOTFIS: TStringField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFVLRMER: TBCDField;
    QtarefasSEQUENCIA: TBCDField;
    QtarefasNRO_OCS: TBCDField;
    QtarefasDATAINC: TDateTimeField;
    QtarefasDT_COLETA_PREV: TStringField;
    QtarefasDT_COLETA: TStringField;
    QtarefasDT_ENTREGA_CD: TStringField;
    QtarefasDT_ENTREGA_PREV: TDateTimeField;
    QtarefasDT_SAIDA: TDateTimeField;
    QtarefasNM_CLI: TStringField;
    QtarefasNM_REM: TStringField;
    QtarefasCID_REM: TStringField;
    QtarefasNM_DES: TStringField;
    QtarefasCID_DES: TStringField;
    QtarefasCODMAN: TBCDField;
    QtarefasTIPOCS: TStringField;
    QtarefasSAIDA: TStringField;
    QtarefasDESTINATARIO: TBCDField;
    QtarefasREMETENTE: TBCDField;
    QtarefasCODCON: TBCDField;
    QFilial: TADOQuery;
    QFilialFILIAL: TStringField;
    Panel2: TPanel;
    Label10: TLabel;
    cbCliente: TJvComboBox;
    Label7: TLabel;
    cbFilial: TJvComboBox;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    lblQuant: TLabel;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Panel3: TPanel;
    JvDBGrid1: TJvDBGrid;
    QtarefasDT_TRANSBORDO1: TDateTimeField;
    QtarefasCODRMC: TBCDField;
    QtarefasTRANSP_ROMA: TBCDField;
    QtarefasTRANSP_MAN: TBCDField;
    QtarefasDESTMA: TStringField;
    QtarefasDESTRO: TStringField;
    dtsTarefas: TDataSource;
    Label2: TLabel;
    btnemail: TBitBtn;
    QNFDATNOT: TDateTimeField;
    QtarefasSOLICITACAO: TBCDField;
    QtarefasDESTINO: TStringField;
    lblDestino: TLabel;
    btnVer: TBitBtn;
    Label3: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    edOCS: TJvCalcEdit;
    Label4: TLabel;
    edCte: TJvCalcEdit;
    Label5: TLabel;
    edNF: TJvCalcEdit;
    Label6: TLabel;
    edMan: TJvCalcEdit;
    Label9: TLabel;
    edSol: TJvCalcEdit;
    QtarefasESTADO: TStringField;
    QtarefasNOMMOT: TStringField;
    btnExcComp: TBitBtn;
    QtarefasSTATUS_SMS: TBCDField;
    QtarefasOST: TBCDField;
    QtarefasFILIAL: TBCDField;
    QtarefasMONITORAMENTO: TStringField;
    QtarefasCOD_DES: TBCDField;
    QtarefasCODFIL: TBCDField;
    dtsColeta: TDataSource;
    QColetaOBS_MON: TStringField;
    QColetaUSER_BAIXA: TStringField;
    QColetaOCORR_COL: TBCDField;
    QColetaOCORR_ENT: TBCDField;
    btnFiltrar: TBitBtn;
    Label12: TLabel;
    edocorrCol: TJvMaskEdit;
    Label13: TLabel;
    edocorrEnt: TJvMaskEdit;
    Label14: TLabel;
    JvDBMaskEdit3: TJvDBMaskEdit;
    Label1: TLabel;
    JvDBRichEdit1: TJvDBRichEdit;
    Label8: TLabel;
    lblTransc: TLabel;
    Label15: TLabel;
    lblTransf: TLabel;
    QtarefasDT_ENTREGA: TStringField;

    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure pnLegendaExit(Sender: TObject);
    procedure Legenda1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QColetaAfterScroll(DataSet: TDataSet);
    procedure QtarefasAfterOpen(DataSet: TDataSet);
    procedure QtarefasAfterScroll(DataSet: TDataSet);
    procedure btnemailClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnVerClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnExcCompClick(Sender: TObject);

  private
    procedure InputBoxSetPasswordChar(var Msg: TMessage); message InputBoxMessage;
  public
    { Public declarations }
  end;

var
  frmMonitReal: TfrmMonitReal;
  quant : integer;
  sqlmaster : string;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmMonitReal.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
      1: QTarefas.First;
      2: QTarefas.Prior;
      3: QTarefas.Next;
      4: QTarefas.Last;
  end;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

procedure TfrmMonitReal.btnemailClick(Sender: TObject);
var memo2: TStringList;
    i : Integer;
begin
  if FileExists('email.html') then
    DeleteFile('email.html');
  Memo2 := TStringList.Create;
  memo2.Add('<HTML><HEAD><TITLE>Monitoramento de Transporte - Realizado</TITLE>');
  memo2.Add('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo2.Add('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto2 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('       .texto3 {	FONT: bold 14px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add('</STYLE>');
  memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo2.Add('<P align=center class=titulo1>Monitoramento de Transporte</P>');
  Memo2.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo2.Add('<TBODY>');

  for i:= 0 to DBGrid1.Columns.Count - 1 do
  begin
    Memo2.Add('<th><FONT class=texto1>'+ DBGrid1.Columns[i].Title.Caption +'</th>');
  end;
  Memo2.Add('</tr>');
  QTarefas.First;
  while not QTarefas.Eof do
  begin
    for i:= 0 to DBGrid1.Columns.Count - 1 do
      Memo2.Add('<th><FONT class=texto2>'+ QTarefas.fieldbyname(DBGrid1.Columns[i].FieldName).AsString +'</th>');
    QTarefas.Next;
    Memo2.Add('<tr>');
  end;
  QTarefas.First;
  memo2.Add('</TABLE></TBODY><BR>');

  Memo2.SaveToFile('listagem.html');
  Memo2.Free;

  ShellExecute(Application.Handle, nil, PChar('listagem.html'), nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmMonitReal.btnExcCompClick(Sender: TObject);
var caminho,arq,Retorno: string;
begin

  if QColetaFILIAL.Value = 1 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_1\'
  else if QColetaFILIAL.Value = 8 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_2\'
  else if QColetaFILIAL.Value = 9 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_9\'
  else if QColetaFILIAL.Value = 8 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_8\'
  else if QColetaFILIAL.Value = 4 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_4\';

  arq := caminho+QtarefasCODCON.AsString+'-1.jpg';

  if FileExists(arq) then
  begin
    PostMessage(Handle, InputBoxMessage, 0, 0);
    Retorno := InputBox('Senha', 'Digite a senha', '');
    if Retorno <> 'INT_130314' then
      Exit;
    DeleteFile(arq);
    ShowMessage('Documento Exclu�do !!');
  end
  else
    ShowMessage('Documento sem comprovante digitalizado !!');
end;

procedure TfrmMonitReal.btnFiltrarClick(Sender: TObject);
var ocs : string;
    i : Integer;
begin
  panel1.Caption := 'Filtrando .......';
  panel1.visible := true;
  application.processMessages;

  if copy(dtinicial.Text,1,2) <> '  ' then
    QTarefas.sql [1] := 'Where dt_saida between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QTarefas.sql [1] := 'Where dt_saida is not null';

  if cbCliente.Text <> '' then
    QTarefas.sql [2] := 'and nm_cli = '+ #39 + cbcliente.Text + #39
  else
    QTarefas.sql [2] := ' ';

  if cbfilial.Text <> '' then
    QTarefas.sql [3] := 'and filial = '+ #39 + copy(cbFilial.Text,1,1) + #39
  else
    QTarefas.sql [3] := ' ';

  if edOCS.value > 0 then
    QTarefas.sql [4] := 'and nro_ocs = '+ #39 + edOCS.text + #39
  else
    QTarefas.sql [4] := ' ';

  if edCte.value > 0 then
    QTarefas.sql [5] := 'and codcon = '+ #39 + edCte.text + #39
  else
    QTarefas.sql [5] := ' ';

 { if edNF.value > 0 then
  begin
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.Clear;
    dtmdados.RQuery1.sql.add('select distinct nro_ocs from vw_monitor_real where notfis = :0');
    dtmdados.RQuery1.Parameters[0].value := edNF.text;
    dtmdados.RQuery1.open;
    if not dtmdados.RQuery1.Eof then
    begin
      i := 0;
      while not dtmdados.RQuery1.Eof do
      begin
        if i = 0 then
          ocs := dtmdados.RQuery1.FieldByName('nro_ocs').AsString
        else
          ocs := dtmdados.RQuery1.FieldByName('nro_ocs').AsString + ',' + ocs;
        i := i + 1;  
        dtmdados.RQuery1.Next;
      end;
      QTarefas.sql [6] := 'and nro_ocs in ('+ ocs + ') ';
    end
    else
      QTarefas.sql [6] := 'and nro_ocs = -987654321 ';
    dtmdados.RQuery1.close;
  end
  else
    QTarefas.sql [6] := ' ';   }

  if edNF.value > 0 then
  begin
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.Clear;
    dtmdados.RQuery1.sql.add('select codocs from rodioc where notfis = :0');
    dtmdados.RQuery1.Parameters[0].value := edNF.text;
    dtmdados.RQuery1.open;
    if not dtmdados.RQuery1.Eof then
    begin
      i := 0;
      while not dtmdados.RQuery1.Eof do
      begin
        if i = 0 then
          ocs := dtmdados.RQuery1.FieldByName('codocs').AsString
        else
          ocs := dtmdados.RQuery1.FieldByName('codocs').AsString + ',' + ocs;
        i := i + 1;  
        dtmdados.RQuery1.Next;
      end;
      QTarefas.sql [6] := 'and nro_ocs in ('+ ocs + ')';
    end;
    dtmdados.RQuery1.close;
  end
  else
    QTarefas.sql [6] := ' ';

  if edMan.value > 0 then
    QTarefas.sql [7] := 'and codman = '+ #39 + edMan.text + #39
  else
    QTarefas.sql [7] := ' ';

  if edSol.value > 0 then
    QTarefas.sql [9] := 'and solicitacao = '+ #39 + edSol.text + #39
  else
    QTarefas.sql [9] := ' ';

  Qtarefas.open;
  if QTarefas.eof then
    ShowMessage('Consulta N�o Encontrada !!')
  else
  begin
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
  end;
  panel1.visible := false;
end;

procedure TfrmMonitReal.btnVerClick(Sender: TObject);
var wb:Variant;
    caminho,arq : string;
begin
  if QtarefasCODFIL.Value = 1 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_1\'
  else if QtarefasCODFIL.Value = 2 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_2\'
  else if QtarefasCODFIL.Value = 9 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_9\'
  else if QtarefasCODFIL.Value = 8 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_8\'
  else if QtarefasCODFIL.Value = 4 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_4\'
  else if QtarefasCODFIL.Value = 6 then
    caminho := '\\192.168.236.112\Comprovantes3\Filial_6\';

  arq := caminho+QtarefasCODCON.AsString+'-1.jpg';

  if FileExists(arq) then
  begin
    wb := CreateOleObject('InternetExplorer.Application');
    wb.Visible := true;
    wb.Navigate(arq);
  end
  else
    ShowMessage('Documento sem comprovante digitalizado !!');
end;

procedure TfrmMonitReal.DBGrid1DblClick(Sender: TObject);
var Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTarefas.Locate(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption + ' n�o localizado');
end;

procedure TfrmMonitReal.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var hoje : TdateTime;
begin
  if (Column.Field.FieldName = 'NRO_OCS') then
  begin
    if QTarefasSTATUS_SMS.Value = 1 then
    begin
      dbgrid1.canvas.Brush.Color := SMS1.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 2 then
    begin
      dbgrid1.canvas.Brush.Color := SMS2.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 3 then
    begin
      dbgrid1.canvas.Brush.Color := SMS3.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.Value = 9 then
    begin
      dbgrid1.canvas.Brush.Color := SMS9.LabelColor;
      dbgrid1.Canvas.Font.Color  := clwhite;
    end;
    if QTarefasSTATUS_SMS.IsNull then
    begin
      dbgrid1.canvas.Brush.Color := SMSnulo.LabelColor;
      dbgrid1.Canvas.Font.Color  := clblue;
    end;
  end;

  if (Column.Field.FieldName = 'CODCON') then
  begin
    if QTarefasOST.Value > 0 then
    begin
      dbgrid1.canvas.Brush.Color := $004080FF;
      //dbgrid1.Canvas.Font.Color  := clwhite;
    end;
  end;  

  if (Column.Field.FieldName = 'DT_COLETA') and (QTarefasTIPOCS.value = 'C') then
  begin
    if QTarefasdt_coleta.IsNull then
      hoje := date
    else
      hoje := StrToDate(QTarefasdt_coleta.value);

    if not QTarefasDT_COLETA_PREV.isnull then
    begin
      if StrToDate(QTarefasDT_COLETA_PREV.value) < hoje then
      begin
        DBgrid1.canvas.Brush.Color := lblColetaAtrasada.labelColor;
        dbgrid1.Canvas.Font.Color  := clWhite;
      end
      else if StrToDate(QTarefasDT_COLETA_PREV.value) = hoje then
      begin
        dbgrid1.canvas.Brush.Color := lblColetaHoje.labelcolor;
        dbgrid1.Canvas.Font.Color  := clNavy;
      end
      else if StrToDate(QTarefasDT_COLETA_PREV.value) > hoje then
      begin
        dbgrid1.canvas.Brush.Color := lblColetaPontual.labelcolor;
        dbgrid1.Canvas.Font.Color  := clWhite;
        DBGrid1.Canvas.FillRect(Rect);
        DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      end;
    end;
    Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
  end;

  if (Column.Field.FieldName = 'DT_COLETA') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;

  if (Column.Field.FieldName = 'DT_COLETA_PREV') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;

  if (Column.Field.FieldName = 'DT_ENTREGA_CD') and (QTarefasTIPOCS.value <> 'C') then
  begin
    dbgrid1.canvas.Brush.Color := clSilver;
  end;


  if (Column.Field.FieldName = 'DT_ENTREGA') then
  begin
    if QTarefasDT_ENTREGA.IsNull then
      hoje := date
    else
      hoje := StrToDate(QTarefasDT_ENTREGA.Value);

    if QTarefasDT_ENTREGA_PREV.Value < hoje then
    begin
      DBgrid1.canvas.Brush.Color := lblColetaAtrasada.labelColor;
      dbgrid1.Canvas.Font.Color  := clWhite;
    end
    else
    if QTarefasDT_ENTREGA_PREV.Value = hoje then
    begin
      dbgrid1.canvas.Brush.Color := lblColetaHoje.labelcolor;
      dbgrid1.Canvas.Font.Color  := clNavy;
    end
    else
    if QTarefasDT_ENTREGA_PREV.Value > hoje then
    begin
      dbgrid1.canvas.Brush.Color := lblColetaPontual.labelcolor;
      dbgrid1.Canvas.Font.Color  := clWhite;
    end;
  end;          
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
end;

procedure TfrmMonitReal.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var Retorno: String;
begin
  if Key = VK_F3 then
  begin
    Retorno := InserirValor('Filtrar por ' + DBGrid1.Columns.Items[DBGrid1.SelectedIndex].Title.Caption, Caption);
    if Retorno = '' then
      Exit;
    panel1.Caption := 'Filtrando .......';
    panel1.visible := true;
    application.processMessages;
    QTarefas.sql [31] := 'where dt_saida is not null ' +
                         ' and ' + DBgrid1.Columns.Items[DBGrid1.SelectedIndex].FieldName +' = ' +
                         #39 + Retorno + #39;
    //Qtarefas.sql.savetofile('c:\sql.txt');
    Qtarefas.open;
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
    panel1.visible := false;
  end;
  if Key = VK_F4 then
  begin
    panel1.Caption := 'Filtrando .......';
    panel1.visible := true;
    application.processMessages;
    QTarefas.sql [31] := 'where dt_saida is not null  ' ;
    Qtarefas.open;
    Qtarefas.First;
    quant := QTarefas.RecordCount;
    lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
    sqlmaster := Qtarefas.SQL.Text;
    panel1.visible := false;
  end;
end;

procedure TfrmMonitReal.DBGrid1TitleClick(Column: TColumn);
var icount : integer;
begin
  panel1.Caption := 'Ordenando .......';
  panel1.visible := true;
  application.processMessages;
  if Pos('order by', QTarefas.SQL.Text) > 0 then
    QTarefas.SQL.Text := Copy(QTarefas.SQL.Text, 1, Pos('order by', QTarefas.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QTarefas.SQL.Text := QTarefas.SQL.Text + ' order by ' + Column.FieldName;
  QTarefas.Open;
  //Muda a cor da coluna do grid
  for iCount := 0 to DBGrid1.Columns.Count - 1 do
    DBGrid1.Columns[iCount].Title.font.color := clNavy;
  column.Title.Font.Color:=clRed;
  panel1.visible := false;
end;

procedure TfrmMonitReal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QTarefas.close;
  QColeta.close;
  Qocorr.close;
  QNF.close;
end;

procedure TfrmMonitReal.FormCreate(Sender: TObject);
begin
  QCliente.open;
  cbCliente.Clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteNM_CLI_OS.value);
    QCliente.Next;
  end;

  QFilial.open;
  cbFilial.Clear;
  cbFilial.Items.add('');
  while not QFilial.eof do
  begin
    cbFilial.Items.add(QFilialFILIAL.value);
    QFilial.Next;
  end;

  dtmdados.rQuery1.close;
  dtmdados.rQuery1.SQL.Clear;
  dtmdados.rQuery1.SQL.add('select distinct nvl(f.nomeab, f.razsoc) nome , f.codclifor ');
  dtmdados.rQuery1.SQL.add('from rodrmc ro left join rodmot m on ro.codmot = m.codmot ');
  dtmdados.rQuery1.SQL.add('               left join rodcli f on m.codclifor = f.codclifor ');
  dtmdados.rQuery1.SQL.add('union ');
  dtmdados.rQuery1.SQL.add('select distinct nvl(f.nomeab, f.razsoc) nome , f.codclifor ');
  dtmdados.rQuery1.SQL.add('from rodman r  left join rodmot m on r.codmo1 = m.codmot ');
  dtmdados.rQuery1.SQL.add('               left join rodcli f on m.codclifor = f.codclifor ');
  dtmdados.rQuery1.SQL.add('order by 1 ');
  dtmdados.rQuery1.open;

  QColeta.open;
  Qocorr.open;
  sqlmaster := QTarefas.SQL.Text;
end;

procedure TfrmMonitReal.Legenda1Click(Sender: TObject);
begin
  pnLegenda.Visible := True;
  pnLegenda.SetFocus;
end;

procedure TfrmMonitReal.QTarefasAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  lblQuant.Caption := IntToStr(QTarefas.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
  QColeta.close;
  QColeta.Parameters[0].Value := QtarefasSEQUENCIA.AsInteger;
  QColeta.Open;
  dtmdados.rQuery1.close;
  dtmdados.rQuery1.SQL.Clear;
  dtmdados.rQuery1.sql.add('select (m.nommot || '' - '' || f.nomeab || '' - '' || f.prtel2) coleta ');
  dtmdados.rQuery1.sql.add('from rodrmc ro left join rodmot m on ro.codmot = m.codmot ');
  dtmdados.rQuery1.sql.add('left join rodcli f on m.codclifor = f.codclifor ');
  dtmdados.rQuery1.sql.add('where ro.codrmc = :0 ');
  dtmdados.rQuery1.Parameters[0].value := QtarefasCODRMC.AsInteger;
  dtmdados.rQuery1.open;
  if not dtmdados.rQuery1.eof then
    lblTransc.caption := dtmdados.rQuery1.FieldByName('coleta').Value + '-> Destino : '+ QtarefasDESTMA.AsString
  else
    lblTransc.caption := '';

  if QtarefasDESTINO.Value = QtarefasNM_DES.Value then
    lblDestino.Caption := ''
  else
    lblDestino.Caption := QtarefasDESTINO.Value;  

  lblTransf.Caption := QtarefasNOMMOT.asstring;
end;

procedure TfrmMonitReal.pnLegendaExit(Sender: TObject);
begin
  pnLegenda.Visible := False;
end;

procedure TfrmMonitReal.QColetaAfterScroll(DataSet: TDataSet);
begin
  QNF.close;
  QNF.sql.clear;
  QNF.sql.Add('select id_ioc id, codocs cod, notfis, quanti, pesokg, vlrmer,datnot from rodioc ');
  QNF.sql.Add('where codocs = :0 and filocs = :1 ');
  QNF.Parameters[0].value := Qtarefasnro_ocs.AsInteger;
  QNF.Parameters[1].value := QtarefasFilial.AsInteger;
  QNF.open;

  if QColetaOCORR_COL.AsInteger > 0 then
  begin
    QOcorr.close;
    Qocorr.Parameters[0].value := QColetaOCORR_COL.AsInteger;
    Qocorr.Open;
    edocorrCol.Text := QocorrDESCRI.value;
  end
  else
    edocorrCol.Clear;

  if QColetaOCORR_ENT.AsInteger > 0 then
  begin
    QOcorr.close;
    Qocorr.Parameters[0].value := QColetaOCORR_ENT.AsInteger;
    Qocorr.Open;
    edocorrEnt.Text := QocorrDESCRI.value;
  end
  else
    edocorrEnt.clear;

end;

procedure TfrmMonitReal.QtarefasAfterOpen(DataSet: TDataSet);
begin
  QColeta.close;
  QColeta.Parameters[0].Value := QTarefasSEQUENCIA.AsInteger;
  QColeta.Open;
end;

procedure TfrmMonitReal.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

end.
