object frmAlteracao_transp: TfrmAlteracao_transp
  Left = 397
  Top = 233
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Altera'#231#227'o de transportadora'
  ClientHeight = 440
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 408
    Width = 404
    Height = 32
    Align = alBottom
    TabOrder = 2
    object btvoltarsp: TBitBtn
      Left = 4
      Top = 1
      Width = 105
      Height = 27
      Caption = 'Fechar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
        EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
        F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
        F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
        F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
        F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
        FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
        FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
        FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
        FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
        FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
        FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
        FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
        FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
        FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
        FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
        FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
        FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
        FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
        FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
        FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
        FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      TabOrder = 0
      OnClick = btvoltarspClick
    end
  end
  object PNSP: TPanel
    Left = 0
    Top = 59
    Width = 404
    Height = 349
    Align = alClient
    TabOrder = 0
    ExplicitTop = 0
    ExplicitWidth = 401
    ExplicitHeight = 377
    object LBLcount: TLabel
      Left = 328
      Top = 287
      Width = 5
      Height = 19
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 18
      Height = 14
      Caption = 'UF:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 106
      Top = 16
      Width = 46
      Height = 14
      Caption = 'Cliente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbUF: TComboBox
      Left = 36
      Top = 14
      Width = 57
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = cbUFChange
      Items.Strings = (
        ''
        'AC'
        'AL'
        'AM'
        'AP'
        'BA'
        'CE'
        'DF'
        'ES'
        'EX'
        'GO'
        'MA'
        'MG'
        'MS'
        'MT'
        'PA'
        'PB'
        'PE'
        'PI'
        'PR'
        'RJ'
        'RN'
        'RO'
        'RR'
        'RS'
        'SC'
        'SE'
        'SP'
        'TO')
    end
    object cbcliente: TComboBox
      Left = 167
      Top = 14
      Width = 213
      Height = 21
      TabOrder = 1
      OnChange = cbclienteChange
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 41
      Width = 397
      Height = 330
      ActivePage = TabSheet1
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Cadastrados'
        object DBGrid1: TDBGrid
          Left = 3
          Top = 0
          Width = 383
          Height = 299
          DataSource = dtsCadastrado
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ORT'
              Title.Caption = 'Cidade'
              Width = 143
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_2'
              Title.Caption = 'Transportadora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUCHBEGRIFF'
              Visible = False
            end>
        end
      end
      object tbalterar: TTabSheet
        Caption = 'Alterar'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label4: TLabel
          Left = 128
          Top = 222
          Width = 138
          Height = 13
          Caption = 'Munic'#237'pios selecionados:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 128
          Top = 237
          Width = 99
          Height = 14
          Caption = 'Transportadora:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 4
          Width = 380
          Height = 210
          Caption = 'Sele'#231#227'o de Municipios'
          TabOrder = 5
          object Button3: TButton
            Left = 179
            Top = 28
            Width = 27
            Height = 32
            Caption = '>>'
            TabOrder = 0
            OnClick = Button3Click
          end
        end
        object LBmunicipios: TListBox
          Left = 5
          Top = 28
          Width = 168
          Height = 179
          ItemHeight = 13
          ScrollWidth = 6
          TabOrder = 0
        end
        object Button1: TButton
          Left = 179
          Top = 74
          Width = 27
          Height = 32
          Caption = '>'
          TabOrder = 1
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 179
          Top = 171
          Width = 27
          Height = 32
          Caption = '<'
          Enabled = False
          TabOrder = 2
          OnClick = Button2Click
        end
        object Button4: TButton
          Left = 179
          Top = 125
          Width = 27
          Height = 32
          Caption = '<<'
          Enabled = False
          TabOrder = 3
          OnClick = Button4Click
        end
        object lbmunicselecionados: TListBox
          Left = 212
          Top = 30
          Width = 163
          Height = 179
          ItemHeight = 13
          ScrollWidth = 6
          TabOrder = 4
        end
        object btalterarsp: TBitBtn
          Left = 3
          Top = 220
          Width = 105
          Height = 42
          Caption = 'Alterar'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000008400000084000000840000008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000008400000084000000840000008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C0000840000008400000084000000840000FF00FF0000840000008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000FF00FF00FF00FF00FF00FF00008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C000084000000840000FF00FF00FF00FF0000840000FF00FF00FF00FF00FF00
            FF00008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C000084000000840000FF00FF00008400000084000000840000FF00FF00FF00
            FF00FF00FF000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C0000840000008400000084000000840000008400000084000000840000FF00
            FF00FF00FF000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000008400000084000000840000008400000084
            0000FF00FF000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000008400000084000000840000008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00008400000084000000840000008400000084000000840000008400000084
            0000008400000084000000840000FF00FF00FF00FF00FF00FF00FF00FF00A59C
            9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
            9C00A59C9C00A59C9C00A59C9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          ParentFont = False
          TabOrder = 6
          OnClick = btalterarspClick
        end
        object CBtranspSP: TComboBox
          Left = 128
          Top = 254
          Width = 185
          Height = 21
          Style = csDropDownList
          TabOrder = 7
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 404
    Height = 59
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 962
    object RadioGroup1: TRadioGroup
      Left = 1
      Top = 1
      Width = 402
      Height = 57
      Align = alClient
      Caption = 'Vers'#227'o WMS'
      Columns = 2
      Items.Strings = (
        'Vers'#227'o 9'
        'Vers'#227'o 11')
      TabOrder = 0
      OnClick = RadioGroup1Click
      ExplicitHeight = 56
    end
  end
  object XMLTransp: TXMLDocument
    Options = [doNodeAutoCreate, doAttrNull, doAutoPrefix, doNamespaceDecl, doAutoSave]
    Left = 32
    Top = 124
    DOMVendorDesc = 'MSXML'
  end
  object ADOQTranspaltera: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select id_spediteur from wms_custom.SPEDITEURE where prio = 1 or' +
        'der by 1')
    Left = 328
    Top = 60
    object ADOQTranspalteraID_SPEDITEUR: TStringField
      FieldName = 'ID_SPEDITEUR'
      Size = 12
    end
  end
  object ADOQupdateCLESS: TADOQuery
    Connection = dtmDados.ConWmsWeb
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'update ADRESSEN set NAME_2 =:0 , SUCHBEGRIFF =:1  where PLZ_ORT ' +
        '=:2 and ID_EIGNER_1 =:3'
      ''
      '')
    Left = 332
    Top = 4
  end
  object adoqmunicipios: TADOQuery
    Connection = dtmDados.ConWMSV11
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct(ort) from ADRESSEN where ID_EIGNER_1 =:0 and PLZ' +
        '_ORT =:1 order by ort')
    Left = 336
    Top = 168
    object adoqmunicipiosORT: TStringField
      FieldName = 'ORT'
      ReadOnly = True
      Size = 30
    end
  end
  object adoqupdateSPcless: TADOQuery
    Connection = dtmDados.ConWMSV11
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'update ADRESSEN set NAME_2 =:0 , SUCHBEGRIFF =:1  where PLZ_ORT ' +
        '=:2 and ID_EIGNER_1 =:3'
      ''
      ''
      ''
      '')
    Left = 336
    Top = 208
  end
  object AdoconsultaID: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT DESCRI, ESTADO, CODIBG FROM CYBER.RODMUN where wms.TiraAc' +
        'ento(descri) = wms.TiraAcento(:0) and estado =:1')
    Left = 28
    Top = 172
    object AdoconsultaIDDESCRI: TStringField
      FieldName = 'DESCRI'
      Size = 40
    end
    object AdoconsultaIDESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object AdoconsultaIDCODIBG: TStringField
      FieldName = 'CODIBG'
      Size = 10
    end
  end
  object AdoAlteraTranspPedido: TADOQuery
    Connection = dtmDados.ConWMSV11
    Parameters = <
      item
        Name = '0'
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ' update auftraege set id_spediteur =:0 '
      ' where ID_KLIENT = '#39'CLESS'#39' and stat = 25 '
      'and id_empfaenger_ware =:1')
    Left = 332
    Top = 260
  end
  object Qcliente: TADOQuery
    Connection = dtmDados.ConWMSV11
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select id_klient, name, suchbegriff from klienten where lager = ' +
        #39'MAR'#39'  and id_klient = '#39'CLESS'#39)
    Left = 336
    Top = 116
    object QclienteID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      Size = 12
    end
    object QclienteNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object QclienteSUCHBEGRIFF: TStringField
      FieldName = 'SUCHBEGRIFF'
      Size = 30
    end
  end
  object QUpdateAlterTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '4'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'update alter_transp set transportadora =:0, codibge =:1  where'
      'cliente =:2 and uf =:3 and municipio =:4 '
      '')
    Left = 60
    Top = 224
  end
  object QAlterTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select * from alter_transp where cliente =:0 and uf =:1 and muni' +
        'cipio =:2')
    Left = 28
    Top = 224
    object QAlterTranspCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 10
    end
    object QAlterTranspTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
    end
    object QAlterTranspUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object QAlterTranspMUNICIPIO: TStringField
      FieldName = 'MUNICIPIO'
      Size = 50
    end
  end
  object QInsertAlterTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '4'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'insert into alter_transp (cliente, transportadora, uf, municipio' +
        ', codibge) values (:0 , :1, :2, :3, :4)')
    Left = 84
    Top = 224
  end
  object QCadastrado: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 5
        Value = 'CLESS'
      end
      item
        Name = '1'
        DataType = ftString
        Size = 2
        Value = 'SP'
      end>
    SQL.Strings = (
      'select distinct (ort), NAME_2, SUCHBEGRIFF'
      'from ADRESSEN'
      'where ID_EIGNER_1 = :0'
      'and PLZ_ORT = :1'
      'order by ort')
    Left = 336
    Top = 312
    object QCadastradoORT: TStringField
      FieldName = 'ORT'
      ReadOnly = True
      Size = 30
    end
    object QCadastradoNAME_2: TStringField
      FieldName = 'NAME_2'
      ReadOnly = True
      Size = 40
    end
    object QCadastradoSUCHBEGRIFF: TStringField
      FieldName = 'SUCHBEGRIFF'
      ReadOnly = True
      Size = 30
    end
  end
  object dtsCadastrado: TDataSource
    DataSet = QCadastrado
    Left = 105
    Top = 76
  end
  object Cliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select nvl(cliente,'#39' '#39') cliente, nvl(base,'#39' '#39') base, nvl(site,'#39' ' +
        #39') site'
      'from dash_cliente c '
      'where cliente like ('#39'CLESS%'#39')')
    Left = 32
    Top = 32
    object ClienteCLIENTE: TStringField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Size = 10
    end
    object ClienteBASE: TStringField
      FieldName = 'BASE'
      ReadOnly = True
      Size = 10
    end
    object ClienteSITE: TStringField
      FieldName = 'SITE'
      ReadOnly = True
      Size = 10
    end
  end
end
