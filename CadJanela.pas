unit CadJanela;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, ImgList, ADODB, ActnList, JvExStdCtrls,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, Mask,
  JvExMask, JvToolEdit, JvMaskEdit, System.ImageList, System.Actions;

type
  TfrmCadJanela = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QCadastro: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Panel1: TPanel;
    Label16: TLabel;
    btProcurar: TBitBtn;
    QCadastroID: TBCDField;
    QCadastroHORA: TStringField;
    QCadastroFL_STATUS: TStringField;
    QCadastroDATAINC: TDateTimeField;
    QCadastroUSERINC: TBCDField;
    QCadastroUSEALT: TBCDField;
    QCadastroDATAALT: TDateTimeField;
    QCadastroFL_EMPRESA: TBCDField;
    edCliente: TJvMaskEdit;
    edJanela: TJvMaskEdit;
    QCadastroCLIENTE: TStringField;
    cbTransp: TComboBox;
    QTransp: TADOQuery;
    QTranspNM_TRANSP: TStringField;
    QCadastroTRANSPORTADOR: TStringField;
    CheckBox1: TCheckBox;
    cbCliente: TComboBox;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure btProcurarClick(Sender: TObject);
    procedure QCadastroBeforeOpen(DataSet: TDataSet);
    procedure edClienteExit(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadJanela: TfrmCadJanela;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadJanela.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadJanela.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  edCliente.clear;
  QTransp.open;
  cbTransp.clear;
  cbTransp.Items.add('');
  while not QTransp.eof do
  begin
    cbTransp.Items.add(QTranspNM_TRANSP.Value);
    QTransp.Next;
  end;
  QTransp.Close;
  edJanela.clear;
  edCliente.SetFocus;
end;

procedure TfrmCadJanela.btnSalvarClick(Sender: TObject);
begin

  if ApCarac(edCliente.text) = '' then
  begin
    ShowMessage('Cliente N�o Cadastrado !!');
    edCliente.SetFocus;
  end;

  if cbTransp.text = '' then
  begin
    ShowMessage('Fornecedor N�o Cadastrado !!');
    cbTransp.SetFocus;
  end;

  if ApCarac(edJanela.text) = '' then
  begin
    ShowMessage('Hor�rio N�o Cadastrado !!');
    edJanela.SetFocus;
  end;

  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select nvl(max(id),0) + 1 id from tb_janela_hora');
      dtmDados.IQuery1.open;

      QCadastro.append;
      QCadastroID.Value := dtmDados.IQuery1.FieldByName('id').Value;
      QCadastroFL_EMPRESA.Value := GLBFilial;
    end
    else
    begin
      QCadastro.edit;
      QCadastroUSEALT.Value := GLBCodUser;
      QCadastroDATAALT.Value := now;
    end;
    QCadastroCLIENTE.Value := ApCarac(edCliente.text);
    QCadastroTRANSPORTADOR.Value := cbTransp.text;
    QCadastroHORA.Value := edJanela.text;
    QCadastroUSERINC.Value := GLBCodUser;
    if CheckBox1.Checked then
      QCadastroFL_STATUS.Value := 'A'
    else
      QCadastroFL_STATUS.Value := 'I';
    QCadastro.post;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QCadastro.Close;
  QCadastro.open;
end;

procedure TfrmCadJanela.btProcurarClick(Sender: TObject);
begin
  if cbCliente.text <> '' then
  begin
    QCadastro.locate('Cliente', cbCliente.text, []);
  end
  else
  begin
    QCadastro.Close;
    QCadastro.open;
  end;
end;

procedure TfrmCadJanela.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QCadastroFL_STATUS.Value = 'I' then
  begin
    dbgContrato.canvas.Brush.Color := clRed;
  end;
  dbgContrato.DefaultDrawDataCell(Rect, dbgContrato.columns[DataCol]
    .field, State);
end;

procedure TfrmCadJanela.dbgContratoTitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QCadastro.locate(dbgContrato.columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadJanela.edClienteExit(Sender: TObject);
begin
  if ApCarac(edCliente.text) <> '' then
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select * from cyber.rodcli cl where fnc_cnpj(cl.codcgc) = :0 ');
    dtmDados.IQuery1.Parameters[0].Value := edCliente.text;
    dtmDados.IQuery1.open;
    if dtmDados.IQuery1.eof then
    begin
      ShowMessage('Cliente N�o Cadastrado');
      edCliente.text := '';
      edCliente.SetFocus;
    end;
    dtmDados.IQuery1.Close;
  end;
end;

procedure TfrmCadJanela.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QCadastro.Cancel;
end;

procedure TfrmCadJanela.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  QTransp.open;
  cbTransp.clear;
  cbTransp.Items.add('');
  while not QTransp.eof do
  begin
    cbTransp.Items.add(QTranspNM_TRANSP.Value);
    QTransp.Next;
  end;
  QTransp.Close;
  cbTransp.ItemIndex := cbTransp.Items.IndexOf(QCadastroTRANSPORTADOR.AsString);
  edCliente.text := QCadastroCLIENTE.AsString;
  edJanela.text := QCadastroHORA.AsString;
  if QCadastroFL_STATUS.Value = 'A' then
    CheckBox1.Checked := true
  else
    CheckBox1.Checked := false;
  edCliente.SetFocus;
end;

procedure TfrmCadJanela.FormCreate(Sender: TObject);
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select distinct cliente from tb_janela_hora where fl_empresa =:0 order by 1');
  dtmDados.IQuery1.Parameters[0].Value := GLBFilial;
  dtmDados.IQuery1.open;
  cbCliente.clear;
  cbCliente.Items.add('');
  while not dtmDados.IQuery1.eof do
  begin
    cbCliente.Items.add(dtmDados.IQuery1.FieldByName('cliente').Value);
    dtmDados.IQuery1.Next;
  end;
  dtmDados.IQuery1.Close;
  QCadastro.open;
  quant := QCadastro.RecordCount;
  lblQuant.Caption := IntToStr(QCadastro.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadJanela.QCadastroBeforeOpen(DataSet: TDataSet);
begin
  QCadastro.Parameters[0].Value := GLBFilial;
end;

procedure TfrmCadJanela.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCadastro.Close;
end;

procedure TfrmCadJanela.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
    pnBotao.Visible := false
  else
    pnBotao.Visible := true;
end;

procedure TfrmCadJanela.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QCadastro.First;
    2:
      QCadastro.Prior;
    3:
      QCadastro.Next;
    4:
      QCadastro.Last;
  end;
  lblQuant.Caption := IntToStr(QCadastro.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
