�
 TFRMCADTABELACONTROLER 0b�  TPF0TfrmCadTabelaControlerfrmCadTabelaControlerLeft� Top� BorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaption$   Aprovação da Tabela de Frete CustoClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TPageControlPageControl1Left Top Width�Height�
ActivePageDetalhesAlignalClientTabOrder  	TTabSheetConsultaCaptionConsultaExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TJvDBUltimGriddbgContratoLeft Top Width�Height�AlignalClient
DataSource	dtstabelaDrawingStyle
gdsClassicOptionsdgTitlesdgIndicator
dgColLines
dgRowLinesdgTabsdgCancelOnExitdgTitleClickdgTitleHotTrack TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickdbgContratoDblClickOnTitleClickdbgContratoTitleClickAlternateRowColorclSilverAlternateRowFontColorclBlack"SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldName	COD_CUSTOTitle.AlignmenttaCenterTitle.Caption   CódigoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width&Visible	 Expanded	FieldNameDT_CADASTROTitle.AlignmenttaCenterTitle.CaptionDataTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthCVisible	 Expanded	FieldNameNM_FORNECEDORTitle.AlignmenttaCenterTitle.CaptionTransportadorTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthYVisible	 Expanded	FieldNameLOCALTitle.AlignmenttaCenterTitle.CaptionOrigemTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthGVisible	 	AlignmenttaCenterExpanded	FieldNameDS_UFTitle.AlignmenttaCenterTitle.Caption	UF OrigemTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width;Visible	 Expanded	FieldName	LOCAL_DESTitle.AlignmenttaCenterTitle.CaptionDestinoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthDVisible	 	AlignmenttaCenterExpanded	FieldName	DS_UF_DESTitle.AlignmenttaCenterTitle.Caption
UF DestinoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVL_FRETE_MINIMOTitle.AlignmenttaCenterTitle.Caption   Frete MínimoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthRVisible	 Expanded	FieldNameVL_ADTitle.AlignmenttaCenterTitle.CaptionSeguroTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVEICULOTitle.AlignmenttaCenterTitle.Caption   VeículoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthUVisible	 Expanded	FieldNameOPERACAOTitle.AlignmenttaCenterTitle.Caption
   OperaçãoTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameUSUARIOTitle.AlignmenttaCenterTitle.Caption   UsuárioTitle.Color��� Title.Font.CharsetANSI_CHARSETTitle.Font.ColorclNavyTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthOVisible	     	TTabSheetDetalhesCaptionDetalhes
ImageIndexExplicitLeftExplicitTop ExplicitWidth ExplicitHeight  TLabelLabel3LeftbTopHWidth-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel6LeftTop� Width.HeightCaptionDestinoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2Left	TopWidthVHeightCaptionTransportadorFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel14LeftTopdWidth,HeightCaptionOrigemFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel20Left	Top3Width-HeightCaption   VeículoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel21Left� Top� Width,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel1Left� TopdWidth,HeightCaptionCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28LeftTop� Width=HeightCaption
   OperaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4LeftTopWidthMHeightCaption   Frete MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel33Left=TopWidth_HeightCaptionHrs ExcedentesFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7Left� TopWidth'HeightCaptionOutrosFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftgTopWidth4HeightCaption   PedágioFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8LeftTop;WidthHHeightCaptionTx Fluvial %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9LeftgTop;Width_HeightCaptionTx. Fluv. MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel23Left� Top;WidthHeightCaptionTRTFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel31Left>Top;Width+HeightCaptionTRT %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel32Left�Top;WidthJHeightCaption
TRT MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10Left�TopmWidth@HeightCaption	ExcedenteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel22LeftTopmWidthHeightCaptionTDEFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel25LeftgTopmWidth+HeightCaptionTDE %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel26Left� TopmWidthJHeightCaption   TDE MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel30Left=TopmWidthNHeightCaption   TDE MáximoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11LeftTop�Width,HeightCaptionSeguroFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel19LeftgTop�WidthZHeightCaption   Seguro mínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel12Left� Top�WidthHeightCaptionGrisFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel13Left=Top�WidthFHeightCaption   Gris MínimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel18Left�Top�WidthUHeightCaptionValor SinergiaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel27LeftTop>WidthKHeightCaption   ObservaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel15LeftkTop>WidthEHeightCaption	ReprovadoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel36LeftTop�Width2HeightCaptionSuframaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel41Left�Top�Width+HeightCaptionTDA %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel42LeftiTop
Width7HeightCaptionTDA Min.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel43LefteTop�Width;HeightCaptionTDA Max.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37Left�Top
Width_HeightCaption% CT-e ReceitaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel39Left� Top�WidthZHeightCaption   Tx. Portuária %Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel40Left<Top�Width^HeightCaptionTx. Port. MinimoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel49Left>Top	Width2HeightCaptionPernoiteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel47Left� TopWidth$HeightCaption   DiáriaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel50Left^Top� Width-HeightCaption   RegiãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel35Left^TopdWidth-HeightCaption   RegiãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TJvMaskEditedTransLeft	TopWidthFHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrderText      TJvMaskEdit	edVeiculoLeft	TopGWidthFHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrderText      TPanelPanel1LefteTop Width5Height1ColorclSilverTabOrder TJvDBUltimGridDBGrid1LeftTopWidth3Height/AlignalClient
DataSourcedtsItemDrawingStyle
gdsClassicOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExitdgTitleClickdgTitleHotTrack TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style AlternateRowColorclMoneyGreen"SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldNameDATAITitle.AlignmenttaCenterTitle.CaptionData InicialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthJVisible	 Expanded	FieldNameDATAFTitle.AlignmenttaCenterTitle.Caption
Data FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style WidthCVisible	 Expanded	FieldNameVL_RODOVIARIOTitle.AlignmenttaCenterTitle.CaptionFreteTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width7Visible	 Expanded	FieldNameVR_CTRCTitle.AlignmenttaCenterTitle.CaptionTx CT-eTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width8Visible	 Expanded	FieldName
NR_PESO_DETitle.AlignmenttaCenterTitle.CaptionPeso InicialTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameNR_PESO_ATETitle.AlignmenttaCenterTitle.Caption
Peso FinalTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	 Expanded	FieldNameVR_EXCEDTitle.AlignmenttaCenterTitle.CaptionPeso Exced.Title.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Width?Visible	 Expanded	FieldName	VL_MINIMOTitle.AlignmenttaCenterTitle.Caption   Vl. MínimoTitle.ColorclGrayTitle.Font.CharsetANSI_CHARSETTitle.Font.ColorclWhiteTitle.Font.Height�Title.Font.NameTahomaTitle.Font.Style Visible	     	TCheckBox	cbImpostoLeft
Top� WidthvHeightCaptionImposto InclusoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TCheckBoxcbPesoLeft� Top� Width� HeightCaptionAcima Peso Exced.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBoxcbStatusLeftbTopWidth>HeightCaptionAtivoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TJvDBMaskEditJvDBMaskEdit1LeftTopvWidth-Height	DataFieldDS_UF
DataSource	dtstabelaReadOnly	TabOrderEditMask      TJvDBMaskEditJvDBMaskEdit2LeftTop� Width-Height	DataField	DS_UF_DES
DataSource	dtstabelaReadOnly	TabOrderEditMask      TJvDBMaskEditJvDBMaskEdit3Left� TopvWidthyHeight	DataFieldCIDO
DataSource	dtstabelaReadOnly	TabOrderEditMask      TJvDBMaskEditJvDBMaskEdit4Left� Top� WidthyHeight	DataFieldCIDD
DataSource	dtstabelaReadOnly	TabOrder	EditMask      TJvDBMaskEditJvDBMaskEdit5LeftTop� Width2Height	DataFieldOPERACAO
DataSource	dtstabelaReadOnly	TabOrder
EditMask      TJvDBMaskEditJvDBMaskEdit6Left<Top� WidthlHeight	DataField	LOCAL_DES
DataSource	dtstabelaReadOnly	TabOrderEditMask      TJvDBMaskEditJvDBMaskEdit7Left<TopvWidthlHeight	DataFieldLOCAL
DataSource	dtstabelaReadOnly	TabOrderEditMask      TJvCalcEditedFreteMLeftTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edPedagioLeftgTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedOutrosLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedHELeft=TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edOutrosRLeftTopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edOutrosMLeftgTopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTRLeft� TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTRpLeft>TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTRmLeft�TopLWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedExcLeft�Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTdeMaxLeft=Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTDEmLeft� Top~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTDEpLeftgTop~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedTDELeftTop~WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edEntregaLeft�Top�WidthAHeightHint7Valor cobrado quando na viagem entrega em outra entregaDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdGrisMLeft=Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdGrisLeft� Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedsegminimoLeftgTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditEdSegLeftTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvMemoObsLeftTopRWidthRHeight9	MaxLength,TabOrder   TJvEditedUserLeft�Top7WidthyHeight
BevelInnerbvNone
BevelOuterbvNoneBorderStylebsNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder!Text      TJvDateEditedDtInsLeftTop7WidthAHeightBorderStylebsNone
BevelInnerbvNone
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	
ShowButtonShowNullDateTabOrder"  TJvMemoobsReproLeftkTopRWidthRHeight9	MaxLength�TabOrder#  TJvCalcEdit	edSuframaLeftTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder$DecimalPlacesAlwaysShown  TJvCalcEditedTDALeft�Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder%DecimalPlacesAlwaysShown  TJvCalcEditedTDAMinLeftgTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder&DecimalPlacesAlwaysShown  TJvCalcEditedTDAMaxLeftgTop�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder'DecimalPlacesAlwaysShown  TJvCalcEdit
edPreceitaLeft�TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder(DecimalPlacesAlwaysShown  TJvCalcEditedPortoLeft� Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder)DecimalPlacesAlwaysShown  TJvCalcEdit
edPortoMinLeft=Top�WidthAHeightDisplayFormat,0.00
ShowButtonTabOrder*DecimalPlacesAlwaysShown  TJvCalcEdit
edPernoiteLeft>TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder+VisibleDecimalPlacesAlwaysShown  TJvCalcEditedDiariaLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder,VisibleDecimalPlacesAlwaysShown  	TComboBoxcbRegiaoLeft^Top� Width� HeightTabOrder-Items.Strings       1ª Região   2ª Região   3ª Região   4ª Região   5ª Região   6ª Região   7ª Região   8ª Região   9ª Região   	TComboBox	cbRegiaoOLeft^TopvWidth� HeightTabOrder.Items.Strings       1ª Região   2ª Região   3ª Região   4ª Região   5ª Região   6ª Região   7ª Região   8ª Região   9ª Região    	TTabSheet	TabSheet1CaptionGeneralidades
ImageIndex TPanelPanel9Left Top Width�HeightIAlignalTop	BevelKindbkSoftTabOrder  TLabelLabel16LeftlTopWidth5HeightCaptionAjudanteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel17Left	TopWidthGHeightCaption   PaletizaçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel24LeftTopWidthXHeightCaptionArmazenagemFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel29Left|TopWidth>HeightCaption	Hr ParadaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel34Left� TopWidth@HeightCaption	ReentregaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
LBLCanhotoLeft�TopWidthQHeightCaptionDev. CanhotoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TJvCalcEditedPaletLeft	TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrder DecimalPlacesAlwaysShown  TJvCalcEditedAjudaLeftlTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEditedReentregaLeft� TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edArmazemLeftTopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit
edHrParadaLeft|TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown  TJvCalcEdit	edCanhotoLeft�TopWidthAHeightDisplayFormat,0.00
ShowButtonTabOrderDecimalPlacesAlwaysShown   	TJvDBGrid	JvDBGrid1Left TopIWidth�HeightOAlignalClient
DataSource	dsGenItemTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style "SelectColumnsDialogStrings.CaptionSelect columnsSelectColumnsDialogStrings.OK&OK-SelectColumnsDialogStrings.NoSelectionWarning$At least one column must be visible!EditControls 
RowsHeightTitleRowHeightColumnsExpanded	FieldName	COD_CUSTOVisible Expanded	FieldNameCOD_IDVisible Expanded	FieldNameVEICULOTitle.AlignmenttaCenterTitle.Caption   VeículoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldNameVL_PERNOITETitle.AlignmenttaCenterTitle.CaptionValor PernoiteTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldNameVL_VDTitle.AlignmenttaCenterTitle.CaptionValor DedicadoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	 Expanded	FieldName	VL_DIARIATitle.AlignmenttaCenterTitle.Caption   Valor DiáriaTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Width� Visible	      TPanelPanel2Left Top�Width�HeightAlignalBottomTabOrder TLabellblQuantLeftUTopWidth%HeightHintQuantidade de registros	AlignmenttaCenterAutoSizeParentShowHintShowHint	LayouttlCenter  TBitBtn	btnFecharLeft4TopWidthHHeightCaption&Fechar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � �   �� ��<L��:I��8G��5E��4C��2B����  ��� � � � � � � � � �   ��#��DS��$)��������		����3C����  ��� � � � � �   ��%��JX��BG������;;������33������05��4D����  ��� � � �   ��O]��27��������������;;��99����������������3C��  ��� � � �   ��R_��((��GG��������������������������44����5E��  ��� � � �   ��Ub��,,��))��HH������������������::������8H��  ��� � � �   ��Wd��00��--��KK������������������==������<K��  ��� � � �   ��Zg��33��PP��������������������������>>����?N��  ��� � � �   ��[h��CG��������������LL��JJ��������������*/��BQ��  ��� � � �   ��&+��]j��X[������RR��//��,,��KK������HL��IW��#��  ��� � � � � �   ��&+��]j��CG��44��22��00��--��8<��O]��%��  ��� � � � � � � � � �   ��&+��\i��[h��Zg��Xe��Vc��Ta��"'��  ��� � � � � � � � � � � � � �   ��  ��  ��  ��  ��  ��  ��  ��� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � TabOrder OnClickbtnFecharClick  TBitBtn
btnAprovarLeft TopWidthHHeightHintAprovar TabelaCaptionAprovar
Glyph.Data
:  6  BM6      6   (                   �  �          ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� �#~}z�s�p�q�q}l#��� ��� ��� ��� ��� ��� ��� �!S��B�R��ʚ��ӫ��ҫ��ǖ�=�L�n�pS��� ��� ��� ��� ��� �+S� �m����۵��̘�f�}�d�|��˘��ٴ�f�}�l�qS��� ��� ��� �6"�/�r��۲�`�w�\�s�Y�p�Y�o�X�o�[�t��ٳ�i��q�q"��� ��� �=~L�d��ݴ�d�y�_�q�uŅ������͙�V�l�X�n�\�t��ڴ�A�N�w~��� ��� �Bۑҟ��Ԛ�d�t�yɇ��������������˖�W�m�[�r��̗��ǚ�x���� ��� �B��ܯ�p��sʀ����������������������͖�[�q�g�}��ׯ�z���� ��� &�K��ݱ�r̀�f�s��������c�p�������������Й�i�~��׮����� ��� -�Tەס��כ�i�v�d�o�a�n�a�o�a�o��������������љ��Ν�� ���� ��� 4�Y~W�p����m�z�h�r�e�p�c�n�b�n�c�q����o�~��ߵ�H�^��&~��� ��� 9�\"4�U�ΐ����m�z�j�v�h�r�h�t�h�u�k�y��ߴ�vĉ��-��-"��� ��� ��� ;�^S4�U�ΐ�����؝�w΃�w΃��؝����xȋ��2��6S��� ��� ��� ��� ��� =�`S6�Y�Y�t��ף��ܮ��ܮ��֡�P�j��B��BS��� ��� ��� ��� ��� ��� ��� @�b#;�^}9�[�1�T�-�R�+�R�+�R}(�N#��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ParentShowHintShowHint	TabOrderOnClickbtnAprovarClick  TBitBtnBitBtn15TagLeftTopWidth(HeightActionacPrimeiroApanha
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �a �Y �Q �Q � � � � � � � � �0 �8 � � � � � � � � � � � � �i �{ ޖZ �Y � � � � � � �8 �a1 �8 � � � � � � � � � � � � �q) 箄 ޖc �a � � � � �I �i9 ޖc �A � � � � � � � � � � � � �y9 綌 ޞk �i � � �Y �q9 ޞk ޞk �I � � � � � � � � � � � � ކB ﶔ �s �q) �i ֆB �{ ޖc ޞs �Q � � � � � � � � � � � � ގR ﾜ 綌 �y9 ޞk 箄 ަs ޖc ަs �Y � � � � � � � � � � � � ޖZ �ǜ ﾜ ֆB �s 綌 �{ ޞk �{ �a � � � � � � � � � � � � �c �ǥ ﶌ ގR ֆB ޖc 綔 �{ 箄 �i � � � � � � � � � � � � �s �ϭ ﾔ ޖZ � � ֆB ޞc 綌 綌 �q) � � � � � � � � � � � � �s �ϵ ﾜ �c � � � � ֆJ ޞk 綔 �y9 � � � � � � � � � � � � ﮄ �ϵ �ǥ �s � � � � � � ގJ ގJ ކB � � � � � � � � � � � � ﮄ ﮄ �{ �s � � � � � � � � ގR ގR � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrder  TBitBtnBitBtn16TagLeft*TopWidth(HeightActionacAnteriorApanha
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �0 �0 � � � � � � � � � � � � � � � � � � � � � � � � � � �8 �Y1 �8 � � � � � � � � � � � � � � � � � � � � � � � � �I �i1 ޖc �A � � � � � � � � � � � � � � � � � � � � � � �Y �q9 ޞk ޞk �A � � � � � � � � � � � � � � � � � � �q) �i! ֆJ �{ ޞc ަs �Q � � � � � � � � � � � � � � � � � � ֆB ޖZ 綌 �s ޞc �{ �Y � � � � � � � � � � � � � � � � � � ގR �k ﾔ 箄 ަs 箄 �a � � � � � � � � � � � � � � � � � � � � ގR �s ﾜ 箄 綌 �q) � � � � � � � � � � � � � � � � � � � � � � ޖZ �s ﾜ ﾔ �y9 � � � � � � � � � � � � � � � � � � � � � � � � ޖc �{ ﾜ ކJ � � � � � � � � � � � � � � � � � � � � � � � � � � ޞc �c ޖZ � � � � � � � � � � � � � � � � � � � � � � � � � � � � �k �c � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrder  TBitBtnBitBtn17TagLeft}TopWidth&HeightActionacProximoApanha
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �I �A � � � � � � � � � � � � � � � � � � � � � � � � � � � � �Y �q9 �A � � � � � � � � � � � � � � � � � � � � � � � � � � �a ަs �q9 �I � � � � � � � � � � � � � � � � � � � � � � � � �i! �{ �{ �q9 �Q � � � � � � � � � � � � � � � � � � � � � � �q1 箄 ަs �{ �y9 �Q �A � � � � � � � � � � � � � � � � � � ֆB 綔 �s �s 箄 �yB �Y � � � � � � � � � � � � � � � � � � ގR ﾜ 箄 箄 綌 ֎J �a � � � � � � � � � � � � � � � � � � �c �ǥ ﶔ ﾜ ޖc �q) � � � � � � � � � � � � � � � � � � � � �k �ǭ �ǥ �s ֆB � � � � � � � � � � � � � � � � � � � � � � �{ �ϭ ﶄ ޖc � � � � � � � � � � � � � � � � � � � � � � � � ﮄ �{ �s � � � � � � � � � � � � � � � � � � � � � � � � � � ﮄ �{ � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrder  TBitBtnBitBtn18TagLeft� TopWidth(HeightActionacUltimoApanha
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �a �Q � � � � � � � � �A �8 �8 �8 � � � � � � � � � � � � �i ֆB �Y � � � � � � �I ֆJ ޖZ �8 � � � � � � � � � � � � �q) 箄 ֆB �Y � � � � �Q ֆR ޖc �A � � � � � � � � � � � � �y9 綌 箄 ֆB �a � � �Y ֎R ޞk �I � � � � � � � � � � � � ކB ﶔ �{ 綌 ֆJ �a �a ޖZ ޞs �Q � � � � � � � � � � � � ގR ﾜ �{ 箄 綌 ޞc �i ަs ަs �Y � � � � � � � � � � � � ޖZ �ǜ 綄 綌 ﶔ ޞk �q) 箄 �{ �a � � � � � � � � � � � � �c �ǥ ﾔ ﾜ ޞk �y9 �y9 �s 箄 �i � � � � � � � � � � � � �s �ϭ �ǥ �{ ގJ � � ކB �{ 綌 �q) � � � � � � � � � � � � �s �ϭ ﶌ ޖc � � � � ގR 箄 ﶔ �y9 � � � � � � � � � � � � ﮄ �{ �s � � � � � � ޖZ 綌 ﾜ ކB � � � � � � � � � � � � ﮄ �{ � � � � � � � � �c ޖc ޖZ ގR � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrder  TBitBtnbtnDctoLeft�TopWidth`HeightHintIncluir DocumentoCaption	DocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������mq�PUم���������������������������������������������=B�������������������������������������������������OU����������������������������������������������w}�z�崸��������el�v|䛠����������������������LU�������fn�-8�FP�]e�ry姫����������������������mu痜����`i����������������������������������������S^�`j�������������������������������������������]h�lv�������������������������������������������t�������������������������������������������iv�������������������������������������������s����������������������������������������������x������������������������������������������������������������������������������������������������������������������������������
ParentFontParentShowHintShowHint	TabOrderOnClickbtnDctoClick  TBitBtnbtnVerLeft/TopWidth>HeightHintVer o DocumentoCaptionPDFFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
:  6  BM6      6   (                  �  �          ���������������������������������������������������������������������������������]�Z����������������������������������������"f�b�"g�[����������������������������������2r�+m�U�����_�������������������������������D�<y�d�ŝ��f��e�������������ݲ�ٮ�֩�ӥР{͜v���u�̫��v��0p�(k����������佛Ḗ�ɮ������������ߺ�Ǩ����A}�9w��������������â�з�������ؽ�ֻ������߻����K������������������Ȩ�������������ؽ�׻������͟{�������������������ή����������������پ�׽���ӥ�������������������Ӵ������������������������ת��������������������׹������������������������۰��������������������۽����������������������ϵ߶�����������������������۾����������������ս���㼚�������������������������ܿ�ٻ�ָ�Ӵ�ϯ�˫�Ʀ������������������������������������������������������������������
ParentFontParentShowHintShowHint	TabOrderVisibleOnClickbtnVerClick  TBitBtnbtnDesativarLeftxTopWidthHHeightHintDesativar TabelaCaption	Desativar
Glyph.Data
:  6  BM6      6   (                                   � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ?T�#:P�}8S��5Q��0K��.N��+L�}'H�#� � � � � � � � � � � � � � FX�SBU��<R��uz����������qx��3M��+J��)K�S� � � � � � � � � � MZ�SIY��\e������~���[c��Y]��}�������Q]��+J��)K�S� � � � � � T_�"Sa��ak������T_��P\��MY��NY��LV��PV������T`��*J��)K�"� � � � X`�~KV������Vd��Rf��MY��MY��MY��MY��LX��RZ������4P��*J�~� � � � \b�ہ���~���]s��MY��MY��MY��MY��MY��MY��O[��{���u{��.K��� � � � _c������p���h���������������������������MY��\f������2P��� � � � di���������p���������������������������MY��^j������6O��� � � � gj�ۥ�������w���T_��T_��T_��T_��T_��T_��cw������x��:S��� � � � ji�~}�����������~���u���l���l���l���l���cy������>O��>T�~� � � � ll�"ji�壧�������������z������~���v�������cn��EW��AV�"� � � � � � ml�Sji������������������������������p{��L[��HX�S� � � � � � � � � � ml�Sjj�掓������������������fp��S^��P]�S� � � � � � � � � � � � � � mm�#kj�}hj��cd��ad��]c��[c�}Xb�#� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � ParentShowHintShowHint	TabOrderOnClickbtnDesativarClick  TBitBtnbtnListagemLeft�TopWidthKHeightCaptionListagem
Glyph.Data
:  6  BM6      6   (                                   � � � � ֞r�әn�іh�Βc�ˎ^�Ɋ[�ǇV�ÄR�ÄR�ÄR�ÄR�ÄR�ÄR��wB�� � � � סu�������������������������������������������������Ŋ]�� � � � ٤z������Ҿ������ӿ��������������ǭ�����������������ƌ_�� � � � ݨ~������к��л��л��л��л��ѽ��͵��͵��͵��͵�����Ɗ\�� � � � ߪ�������η������л��������������Ϻ�����������������ȍ_�� � ���ج������һ���­��ǰ��̳��̳��η��Ǭ��Ǭ��Ȱ��Ȯ�����ĆT�������j���������������������������������Ǭ�����������������ƆU����������������������������������ɮ��ɰ��Ǭ��ɰ��Ȱ��̵�����ȊY������������������������������������������Ǭ�����������������ˏ_����������������������������������æ��æ��æ��æ��æ��æ�����Γd�������������������������������������������������������������їj����������������������������������˓��Ɏ��Ɖ�~Ä�z���v�|�����ԛo�������������������������������������������������������������נt���������������������������������緓�浐�䲌�⯈�଄�ݩ��ܥ}�ڣzʺ�������������������������������� � � � � � � � � � � � � � � � ������l�������뵵�󲲲а������%� � � � � � � � � � � � � � � � TabOrder	OnClickbtnListagemClick   TDataSource	dtstabelaDataSetQTabelaLeft� Top�   TDataSourcedtsItemDataSetQItemLeft�Top�   TActionListactImagesdtmDados.IMBotoesLeft`Top�  TActionacPrimeiroApanhaTagCategoryApanhaHintPrimeiro registro
ImageIndex	OnExecuteacPrimeiroApanhaExecute  TActionacAnteriorApanhaTagCategoryApanhaHintRegistro anterior
ImageIndex	OnExecuteacPrimeiroApanhaExecute  TActionacProximoApanhaTagCategoryApanhaHint   Próximo registro
ImageIndex 	OnExecuteacPrimeiroApanhaExecute  TActionacUltimoApanhaTagCategoryApanhaHint   Último registro
ImageIndex	OnExecuteacPrimeiroApanhaExecute   	TADOQueryQTabela
ConnectiondtmDados.ConIntecom
CursorTypectStatic	AfterOpenQTabelaAfterScrollAfterScrollQTabelaAfterScroll
Parameters SQL.StringsFselect t.*, f.nomeab || ' - ' ||nvl(f.codcgc, f.codcpf) nm_fornecedor,CASE'   WHEN t.fl_local = 'C' THEN 'CAPITAL'ELSE   'INTERIOR'end as local,CASE+   WHEN t.fl_local_des = 'C' THEN 'CAPITAL'ELSE   'INTERIOR'.end as local_des, d.descri cidd, o.descri cidoOfrom tb_custofrac t inner join cyber.rodcli f on t.cod_fornecedor = f.codcliforC                    left join cyber.rodmun d on t.codmun = d.codmunD                    left join cyber.rodmun o on t.codmuno = o.codmunwhere t.controler is nulland t.fl_status = 'S'order by cod_custo LeftdTop�  	TBCDFieldQTabelaCOD_CUSTO	FieldName	COD_CUSTO	Precision   	TBCDFieldQTabelaCOD_FORNECEDOR	FieldNameCOD_FORNECEDOR	Precision   TStringFieldQTabelaFL_LOCAL	FieldNameFL_LOCAL	FixedChar	Size  TStringFieldQTabelaDS_UF	FieldNameDS_UFSize  	TBCDFieldQTabelaVL_FRETE_MINIMO	FieldNameVL_FRETE_MINIMODisplayFormat#,##0.00	PrecisionSize  	TBCDFieldQTabelaVL_PEDAGIO	FieldName
VL_PEDAGIODisplayFormat#,##0.00	PrecisionSize  	TBCDFieldQTabelaVL_OUTROS	FieldName	VL_OUTROSDisplayFormat#,##0.00	PrecisionSize  	TBCDFieldQTabelaVL_OUTROS_REAIS	FieldNameVL_OUTROS_REAISDisplayFormat	#,##0.00%	PrecisionSize  	TBCDFieldQTabelaVL_OUTROS_MINIMO	FieldNameVL_OUTROS_MINIMODisplayFormat#,##0.00	PrecisionSize  	TBCDFieldQTabelaVL_EXCEDENTE	FieldNameVL_EXCEDENTEDisplayFormat#,##0.00	PrecisionSize  TFloatFieldQTabelaVL_AD	FieldNameVL_ADDisplayFormat	#,##0.00%  TFloatFieldQTabelaVL_GRIS	FieldNameVL_GRISDisplayFormat	#,##0.00%  TFloatFieldQTabelaVL_GRIS_MINIMO	FieldNameVL_GRIS_MINIMODisplayFormat#,##0.00  TDateTimeFieldQTabelaDT_CADASTRO	FieldNameDT_CADASTRO  TStringFieldQTabelaMODAL	FieldNameMODAL  TStringFieldQTabelaNM_FORNECEDOR	FieldNameNM_FORNECEDORSize2  TStringFieldQTabelaLOCAL	FieldNameLOCALSize  TStringFieldQTabelaFL_LOCAL_DES	FieldNameFL_LOCAL_DES	FixedChar	Size  TStringFieldQTabelaDS_UF_DES	FieldName	DS_UF_DES	FixedChar	Size  TStringFieldQTabelaLOCAL_DES	FieldName	LOCAL_DESSize  	TBCDFieldQTabelaVL_ENTREGA	FieldName
VL_ENTREGA	PrecisionSize  TStringFieldQTabelaFL_IMPOSTO	FieldName
FL_IMPOSTO	FixedChar	Size  TFloatFieldQTabelaSEGURO_MINIMO	FieldNameSEGURO_MINIMODisplayFormat	#,##0.00%  TStringFieldQTabelaFL_PESO	FieldNameFL_PESO	FixedChar	Size  	TBCDFieldQTabelaSITE	FieldNameSITE	Precision Size   TStringFieldQTabelaUSUARIO	FieldNameUSUARIO  	TBCDFieldQTabelaCODMUN	FieldNameCODMUN	Precision Size   	TBCDFieldQTabelaCODMUNO	FieldNameCODMUNO	Precision Size   	TBCDFieldQTabelaVL_TDE	FieldNameVL_TDE	PrecisionSize  	TBCDFieldQTabelaVL_TR	FieldNameVL_TR	PrecisionSize  	TBCDFieldQTabelaVL_TDEP	FieldNameVL_TDEP	PrecisionSize  	TBCDFieldQTabelaVL_TDEM	FieldNameVL_TDEM	PrecisionSize  TStringFieldQTabelaVEICULO	FieldNameVEICULO  	TBCDFieldQTabelaDIAS	FieldNameDIAS	Precision Size   TStringFieldQTabelaFL_STATUS	FieldName	FL_STATUSSize  TStringFieldQTabelaOPERACAO	FieldNameOPERACAOSize  	TBCDFieldQTabelaVL_TDEMAX	FieldName	VL_TDEMAX	PrecisionSize  TStringFieldQTabelaCONTROLER	FieldName	CONTROLER  TDateTimeFieldQTabelaDT_CONTROLER	FieldNameDT_CONTROLER  TStringFieldQTabelaCIDD	FieldNameCIDDSize(  TStringFieldQTabelaCIDO	FieldNameCIDOSize(  TStringField
QTabelaOBS	FieldNameOBSSize,  	TBCDFieldQTabelaVL_TRP	FieldNameVL_TRP	PrecisionSize  	TBCDFieldQTabelaVL_TRM	FieldNameVL_TRM	PrecisionSize  	TBCDFieldQTabelaVL_HE	FieldNameVL_HE	PrecisionSize  	TBCDFieldQTabelavl_sufra	FieldNamevl_sufra  	TBCDFieldQTabelavl_vd	FieldNamevl_vd  	TBCDFieldQTabelavl_tdc	FieldNamevl_tdc  	TBCDFieldQTabelavl_porto	FieldNamevl_porto  	TBCDFieldQTabelavl_portomin	FieldNamevl_portomin  	TBCDFieldQTabelavl_tda	FieldNamevl_tda  	TBCDFieldQTabelavl_tdamin	FieldName	vl_tdamin  	TBCDFieldQTabelavl_tdamax	FieldName	vl_tdamax  TStringFieldQTabelaDOC_CONTROLER	FieldNameDOC_CONTROLERSize�   TStringFieldQTabelaREPROVADODisplayWidth�	FieldName	REPROVADOSize�  TStringFieldQTabelaCONTROLER_REPR	FieldNameCONTROLER_REPRSize  	TBCDFieldQTabelaVL_ARMAZENAGEM	FieldNameVL_ARMAZENAGEM	PrecisionSize  	TBCDFieldQTabelaVL_AJUDANTES	FieldNameVL_AJUDANTES	PrecisionSize  	TBCDFieldQTabelaVL_PALETIZACAO	FieldNameVL_PALETIZACAO	PrecisionSize  	TBCDFieldQTabelaVL_DIARIA	FieldName	VL_DIARIA	PrecisionSize  	TBCDFieldQTabelaVL_REENTREGA	FieldNameVL_REENTREGA	PrecisionSize  	TBCDFieldQTabelaVL_PERNOITE	FieldNameVL_PERNOITE	PrecisionSize  	TBCDFieldQTabelaVL_HORA_PARADA	FieldNameVL_HORA_PARADA	PrecisionSize  TStringFieldQTabelaREGIAO	FieldNameREGIAOSize2  	TBCDFieldQTabelavl_preceita	FieldNamevl_preceita   	TADOQueryQItem
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custoDataTypeftStringSizeValue0  SQL.Strings1select distinct t.*,t.ROWID || '' as id_registro from tb_custofrac_item twhere t.cod_custo = :cod_custo+order by datai,dataf,nr_peso_de,nr_peso_ate Left�Top�  	TBCDFieldQItemCOD_TAB	FieldNameCOD_TAB	Precision   	TBCDFieldQItemCOD_CUSTO	FieldName	COD_CUSTO	Precision   	TBCDFieldQItemNR_PESO_DE	FieldName
NR_PESO_DEDisplayFormat
##,##0.000	Precision
  	TBCDFieldQItemNR_PESO_ATE	FieldNameNR_PESO_ATEDisplayFormat
##,##0.000	Precision
  	TBCDFieldQItemVL_RODOVIARIO	FieldNameVL_RODOVIARIODisplayFormat
###,##0.00	PrecisionSize  TDateTimeField
QItemDATAI	FieldNameDATAI  TDateTimeField
QItemDATAF	FieldNameDATAF  TStringFieldQItemID_REGISTRO	FieldNameID_REGISTROReadOnly	Size  	TBCDFieldQItemVR_CTRC	FieldNameVR_CTRCReadOnly	DisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQItemVR_EXCED	FieldNameVR_EXCEDReadOnly	DisplayFormat#0.0000	Precision
  	TBCDFieldQItemVL_MINIMO	FieldName	VL_MINIMODisplayFormat
###,##0.00   	TADOQuery
Qcadtabela
ConnectiondtmDados.ConIntecom
CursorTypectStatic	AfterOpenQcadtabelaAfterOpen
ParametersName	cod_custo
AttributespaSigned DataType	ftInteger	Precision
Value  SQL.Stringsselect *from tb_custofracwhere cod_custo = :cod_custo Left� Top�  	TBCDFieldQcadtabelaCOD_CUSTO	FieldName	COD_CUSTO	Precision   	TBCDFieldQcadtabelaCOD_FORNECEDOR	FieldNameCOD_FORNECEDOR	Precision Size   TStringFieldQcadtabelaFL_LOCAL	FieldNameFL_LOCAL	FixedChar	Size  TStringFieldQcadtabelaDS_UF	FieldNameDS_UFSize  TStringFieldQcadtabelaFL_LOCAL_DES	FieldNameFL_LOCAL_DES	FixedChar	Size  TStringFieldQcadtabelaDS_UF_DES	FieldName	DS_UF_DESSize  	TBCDFieldQcadtabelaVL_FRETE_MINIMO	FieldNameVL_FRETE_MINIMO	PrecisionSize  	TBCDFieldQcadtabelaVL_PEDAGIO	FieldName
VL_PEDAGIO	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS	FieldName	VL_OUTROS	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS_REAIS	FieldNameVL_OUTROS_REAIS	PrecisionSize  	TBCDFieldQcadtabelaVL_OUTROS_MINIMO	FieldNameVL_OUTROS_MINIMO	PrecisionSize  	TBCDFieldQcadtabelaVL_EXCEDENTE	FieldNameVL_EXCEDENTE	PrecisionSize  TFloatFieldQcadtabelaVL_AD	FieldNameVL_AD  TFloatFieldQcadtabelaVL_GRIS	FieldNameVL_GRIS  TFloatFieldQcadtabelaVL_GRIS_MINIMO	FieldNameVL_GRIS_MINIMO  	TBCDFieldQcadtabelaVL_ENTREGA	FieldName
VL_ENTREGA	PrecisionSize  TFloatFieldQcadtabelaSEGURO_MINIMO	FieldNameSEGURO_MINIMO  TStringFieldQcadtabelaFL_PESO	FieldNameFL_PESO	FixedChar	Size  TStringFieldQcadtabelaFL_IMPOSTO	FieldName
FL_IMPOSTO	FixedChar	Size  TStringFieldQcadtabelaMODAL	FieldNameMODAL  TStringFieldQcadtabelaVEICULO	FieldNameVEICULO  	TBCDFieldQcadtabelaSITE	FieldNameSITE	Precision Size   TDateTimeFieldQcadtabelaDT_CADASTRO	FieldNameDT_CADASTRO  TStringFieldQcadtabelaUSUARIO	FieldNameUSUARIO  	TBCDFieldQcadtabelaCODMUN	FieldNameCODMUN	Precision Size   	TBCDFieldQcadtabelaCODMUNO	FieldNameCODMUNO	Precision Size   	TBCDFieldQcadtabelaVL_TDE	FieldNameVL_TDE	PrecisionSize  	TBCDFieldQcadtabelaVL_TR	FieldNameVL_TR	PrecisionSize  	TBCDFieldQcadtabelaVL_TDEP	FieldNameVL_TDEP	PrecisionSize  	TBCDFieldQcadtabelaVL_TDEM	FieldNameVL_TDEM	PrecisionSize  	TBCDFieldQcadtabelaDIAS	FieldNameDIAS	Precision Size   TStringFieldQcadtabelaFL_STATUS	FieldName	FL_STATUSSize  TStringFieldQcadtabelaOPERACAO	FieldNameOPERACAOSize  	TBCDFieldQcadtabelaVL_TDEMAX	FieldName	VL_TDEMAX	PrecisionSize  TStringFieldQcadtabelaCONTROLER	FieldName	CONTROLER  TDateTimeFieldQcadtabelaDT_CONTROLER	FieldNameDT_CONTROLER  TStringFieldQcadtabelaDOC_CONTROLER	FieldNameDOC_CONTROLERSize�   TStringFieldQcadtabelaOBS	FieldNameOBSSize,  	TBCDFieldQcadtabelaVL_TRP	FieldNameVL_TRP	PrecisionSize  	TBCDFieldQcadtabelaVL_TRM	FieldNameVL_TRM	PrecisionSize  	TBCDFieldQcadtabelaVL_HE	FieldNameVL_HE	PrecisionSize  TStringFieldQcadtabelareprovado	FieldName	reprovadoSize�  TStringFieldQcadtabelacontroler_repr	FieldNamecontroler_repr  	TBCDFieldQcadtabelaVL_SUFRA	FieldNameVL_SUFRA	PrecisionSize  	TBCDFieldQcadtabelaVL_VD	FieldNameVL_VD	PrecisionSize  	TBCDFieldQcadtabelaVL_PORTO	FieldNameVL_PORTO	PrecisionSize  	TBCDFieldQcadtabelaVL_PORTOMIN	FieldNameVL_PORTOMIN	PrecisionSize  	TBCDFieldQcadtabelaVL_TDC	FieldNameVL_TDC	PrecisionSize  	TBCDFieldQcadtabelaVL_TDA	FieldNameVL_TDA	PrecisionSize  	TBCDFieldQcadtabelaVL_TDAMIN	FieldName	VL_TDAMIN	PrecisionSize  	TBCDFieldQcadtabelaVL_TDAMAX	FieldName	VL_TDAMAX	PrecisionSize  	TBCDFieldQcadtabelaVL_ARMAZENAGEM	FieldNameVL_ARMAZENAGEM	PrecisionSize  	TBCDFieldQcadtabelaVL_AJUDANTES	FieldNameVL_AJUDANTES	PrecisionSize  	TBCDFieldQcadtabelaVL_PALETIZACAO	FieldNameVL_PALETIZACAO	PrecisionSize  	TBCDFieldQcadtabelaVL_DIARIA	FieldName	VL_DIARIA	PrecisionSize  	TBCDFieldQcadtabelaVL_REENTREGA	FieldNameVL_REENTREGA	PrecisionSize  	TBCDFieldQcadtabelaVL_PERNOITE	FieldNameVL_PERNOITE	PrecisionSize  	TBCDFieldQcadtabelaVL_HORA_PARADA	FieldNameVL_HORA_PARADA	PrecisionSize  TStringFieldQcadtabelaREGIAO	FieldNameREGIAOSize2  TStringFieldQcadtabelaregiaoo	FieldNameregiaooSize2   TOpenDialog
OpenDialogLeft�Topt  TIdSMTPIdSMTP1Hostsmtp.iwlogistica.com.brPassword
iwlog@2014PortKSASLMechanisms Usernameiwlogistica@iwlogistica.com.brLeftLTop(  
TIdMessage
IdMessage1AttachmentEncodingMIMEBccList CharSetus-asciiCCList ContentType	text/htmlEncodingmeMIMEFromListAddressiwlogistica@iwlogistica.com.brNameControladoria IntecomText6Controladoria Intecom <iwlogistica@iwlogistica.com.br>Domainiwlogistica.com.brUseriwlogistica  From.Addressiwlogistica@iwlogistica.com.br	From.NameControladoria Intecom	From.Text6Controladoria Intecom <iwlogistica@iwlogistica.com.br>From.Domainiwlogistica.com.br	From.Useriwlogistica
Recipients ReplyTo ConvertPreamble	LeftLTopX  	TADOQueryqtelas
ConnectiondtmDados.ConIntecom
CursorTypectStatic
Parameters SQL.Stringsselect t.descricao, m.email from tb_tela_sis t inner join tb_cadmensageiro m "on  t.descricao = m.servico where name = 'frmCadTabelaControler' and m.status = '1' LeftLTop�  TStringFieldqtelasDESCRICAO	FieldName	DESCRICAOSize2  TStringFieldqtelasEMAIL	FieldNameEMAILSize�   	TADOQueryQGenItem
ConnectiondtmDados.ConIntecom
CursorTypectStatic
ParametersName	cod_custoDataType	ftIntegerValue   SQL.Strings=select * from tb_custofrac_generalidades where cod_custo = :0 Left� Top  TFMTBCDFieldQGenItemCOD_ID	FieldNameCOD_ID	Precision&Size   TFMTBCDFieldQGenItemCOD_CUSTO	FieldName	COD_CUSTO	Precision&Size  	TBCDFieldQGenItemVL_PERNOITE	FieldNameVL_PERNOITEDisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQGenItemVL_VD	FieldNameVL_VDDisplayFormat
###,##0.00	PrecisionSize  	TBCDFieldQGenItemVL_DIARIA	FieldName	VL_DIARIADisplayFormat
###,##0.00	PrecisionSize  TStringFieldQGenItemVEICULO	FieldNameVEICULOSize   TDataSource	dsGenItemDataSetQGenItemLeft� Top0   