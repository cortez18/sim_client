unit Rel_Averbacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRel_Averbacao = class(TForm)
    QCusto: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    JvDBNavigator1: TJvDBNavigator;
    Label2: TLabel;
    CBO: TComboBox;
    QCustofl_empresa: TBCDField;
    QCustonumerocte: TBCDField;
    QCustoufori: TStringField;
    QCustouffim: TStringField;
    QCustoemissaocte: TDateField;
    QCustoplaca: TStringField;
    QCustonometom: TStringField;
    QCustovalorcarga: TFloatField;
    QCustodestinatario: TStringField;
    QCustoaverba_protocolo: TStringField;
    QCustoaverba_data: TDateField;
    RadioGroup1: TRadioGroup;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_Averbacao: TfrmRel_Averbacao;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_Averbacao.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  if not QCusto.Eof then
  begin
    sarquivo := DirectoryListBox1.Directory + '\Rel_Averbacao' +
      ApCarac(DateToStr(date)) + '.xls';
    Memo1 := TStringList.Create;
    Memo1.Add('  <HTML>');
    Memo1.Add('    <HEAD>');
    Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
    Memo1.Add(
      '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
    Memo1.Add(
      '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    Memo1.Add(
      '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
    Memo1.Add('         </STYLE>');
    Memo1.Add('    </HEAD>');
    Memo1.Add('    <BODY <Font Color="#004080">');
    Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

    Memo1.Add('</th></font></Center>');
    Memo1.Add('</tr>');
    Memo1.Add('</B></font>');
    Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
    Memo1.Add('<TBODY>');
    Memo1.Add('<tr>');
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
    begin
      Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
        + '</th>');
    end;
    Memo1.Add('</tr>');
    QCusto.First;
    Gauge1.MaxValue := QCusto.RecordCount;
    while not QCusto.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        Memo1.Add('<th><FONT class=texto2>' + QCusto.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
      Gauge1.AddProgress(1);
      QCusto.Next;
      Memo1.Add('<tr>');
    end;
    Memo1.Add('</TBODY>');
    Memo1.Add('</table>');
    Memo1.SaveToFile(sarquivo);
    showmessage('Planilha salva em ' + sarquivo);
  end;
end;

procedure TfrmRel_Averbacao.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;
  QCusto.close;
  if RadioGroup1.ItemIndex = 0 then
    QCusto.SQL[1] := 'Where c.averba_data between to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtInicial.date) + ' 00:00:01' + #39 +
      ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtFinal.date) + ' 23:59:59' + #39 +
      ',''dd/mm/yy hh24:mi:ss'')'
  else
    QCusto.SQL[1] := 'Where c.emissaocte between to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtInicial.date) + ' 00:00:01' + #39 +
      ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtFinal.date) + ' 23:59:59' + #39 +
      ',''dd/mm/yy hh24:mi:ss'')';
  if CBO.Text <> 'TODAS' then
    QCusto.SQL[2] := 'and c.fl_empresa = ' +
      QuotedStr(alltrim(copy(CBO.Text, 1, Pos('-', CBO.Text) - 1)))
  else
    QCusto.SQL[2] := ' ';
  QCusto.open;
  Panel1.Visible := false;
end;

procedure TfrmRel_Averbacao.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Averbacao.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Averbacao.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Averbacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCusto.close;
end;

procedure TfrmRel_Averbacao.FormCreate(Sender: TObject);
begin
  DriveComboBox1.Enabled := true;
  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.Clear;
  dtmdados.iQuery1.SQL.Add
    ('select distinct (fl_empresa ||''-''||f.nomeab) nm from tb_conhecimento c left join cyber.rodfil f on c.fl_empresa = f.codfil');
  dtmdados.iQuery1.open;
  CBO.Clear;
  CBO.Items.Add('TODAS');
  while not dtmdados.iQuery1.Eof do
  begin
    CBO.Items.Add(dtmdados.iQuery1.fieldbyname('nm').Value);
    dtmdados.iQuery1.Next;
  end;
  dtmdados.iQuery1.close;
  CBO.ItemIndex := 0;
end;

procedure TfrmRel_Averbacao.JvDBGrid1DblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCusto.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_Averbacao.JvDBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  QCusto.close;
  if Pos('order by', QCusto.SQL.Text) > 0 then
  begin
    QCusto.SQL.Text := copy(QCusto.SQL.Text, 1, Pos('order by', QCusto.SQL.Text)
      - 1) + 'order by ' + Column.FieldName
  end
  else
    QCusto.SQL.Text := QCusto.SQL.Text + ' order by ' + Column.FieldName;
  QCusto.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
    JvDBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

end.
