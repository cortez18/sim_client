unit Sobre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExControls, JvScrollText, ExtCtrls,
  ImgList, StdCtrls, GIFImg, jpeg, JvExExtCtrls, JvSecretPanel;

type
  TfrmSobre = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    JvSecretPanel1: TJvSecretPanel;
    procedure JvXPButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Image1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSobre: TfrmSobre;

implementation

uses Menu, CadTelas, Dados;

{$R *.dfm}

procedure TfrmSobre.FormCreate(Sender: TObject);
begin
  JvSecretPanel1.Active := true;
end;

procedure TfrmSobre.Image1Click(Sender: TObject);
begin

  // Verifica se o usu�rio � do TI
  dtmDados.qryAcesso.Close;
  dtmDados.qryAcesso.SQL.clear;
  dtmDados.qryAcesso.SQL.Add
    ('select * from tb_usuarios u where u.fl_status = ''A'' and u.cod_usuario = '
    + #39 + IntToStr(GLBCodUser) + #39' and u.cod_dpto = 3 ');
  dtmDados.qryAcesso.Open;
  if not dtmDados.qryAcesso.eof then
  begin
    try
      Application.CreateForm(TfrmCadTelas, frmCadTelas);
      frmCadTelas.ShowModal;
    finally
      frmCadTelas.Free;
    end;
  end;
end;

procedure TfrmSobre.JvXPButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSobre.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  JvSecretPanel1.Active := false;
  Action := caFree;
  frmSobre := nil;
end;

end.
