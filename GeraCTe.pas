unit GeraCTe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, Data.DB, ACBrSocket, ACBrCEP, ACBrBase, ACBrDFe,
  System.ImageList, Vcl.ImgList, JvMemoryDataset, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.Buttons, JvBaseEdits, JvExStdCtrls, JvMemo, Vcl.Mask, JvExMask,
  JvToolEdit, JvMaskEdit, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, Vcl.ComCtrls, Vcl.ExtCtrls, pcnConversao, JvDBLookup,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, IPPeerClient,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, REST.Types,
  Vcl.Themes;

type
  TfrmGeraCTe = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblqt: TLabel;
    mdTemp: TJvMemoryData;
    mdTempOCS: TIntegerField;
    mdTempInserida: TIntegerField;
    dsTemp: TDataSource;
    iml: TImageList;
    QVeiculo: TADOQuery;
    dtsVeiculo: TDataSource;
    pnManifesto: TPanel;
    Label5: TLabel;
    Panel5: TPanel;
    dstTarefa: TDataSource;
    Panel8: TPanel;
    lblDescricao: TLabel;
    edValor: TEdit;
    cbPesquisa: TComboBox;
    QTarefa: TADOQuery;
    qrRota: TADOQuery;
    dsRota: TDataSource;
    QDestino: TADOQuery;
    pnTarefas: TPanel;
    pgTarefas: TPageControl;
    tbRota: TTabSheet;
    dgRota: TJvDBUltimGrid;
    tbTarefa: TTabSheet;
    dbgColeta: TJvDBUltimGrid;
    qrCalcula_Frete: TADOQuery;
    ckRota: TCheckBox;
    qrRotaROTA: TStringField;
    qrRotaCODSETCL: TBCDField;
    qrCalcula_FreteCUB: TBCDField;
    qrCalcula_FretePESO: TBCDField;
    qrCalcula_FreteQTD: TBCDField;
    qrCalcula_FreteVALOR: TBCDField;
    mdTempdestino: TStringField;
    btnCancelaCTe: TBitBtn;
    QVeiculoDESCRI: TStringField;
    QVeiculoKG: TBCDField;
    QVeiculoCUB: TBCDField;
    QGerados: TADOQuery;
    dtsGerados: TDataSource;
    btnGerar: TBitBtn;
    Label58: TLabel;
    cbOperacao: TComboBox;
    Label9: TLabel;
    Label11: TLabel;
    cbMotorista: TComboBox;
    Label4: TLabel;
    cbplaca: TComboBox;
    dtsTrans: TDataSource;
    QTransp: TADOQuery;
    edVeiculo: TJvMaskEdit;
    QVeiculoPLACA: TStringField;
    tbCte: TTabSheet;
    dgCtrc: TJvDBUltimGrid;
    Panel7: TPanel;
    btnSefaz: TBitBtn;
    btnStatus: TBitBtn;
    MStat: TJvMemo;
    QSite: TADOQuery;
    QSiteSITE: TStringField;
    mdTempfilial: TIntegerField;
    QMotor: TADOQuery;
    QMotorNOME: TStringField;
    QVeiculoSITRAS: TStringField;
    QVeiculoCODPRO: TBCDField;
    cbFiltro: TCheckBox;
    Label32: TLabel;
    Label33: TLabel;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QVeiculoCasa: TADOQuery;
    QVeiculoCasaPLACA: TStringField;
    QVeiculoCasaDESCRI: TStringField;
    QVeiculoCasaKG: TBCDField;
    QVeiculoCasaCUB: TBCDField;
    QVeiculoCasaSITRAS: TStringField;
    QVeiculoCasaCODPRO: TBCDField;
    Label34: TLabel;
    Obs: TJvMemo;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    QCons: TADOQuery;
    QDestinoNRO_OCS: TBCDField;
    QDestinoRAZSOC: TStringField;
    QDestinoMUN_DES: TStringField;
    QDestinoTIPO: TStringField;
    QDestinoPAGADOR: TBCDField;
    QDestinoDESTINATARIO: TBCDField;
    mdTemppagador: TIntegerField;
    mdTempcod_destino: TIntegerField;
    mdTemptipo: TStringField;
    QCliente: TADOQuery;
    QClienteRAZSOC: TStringField;
    QClientePAGADOR: TBCDField;
    dtsCliente: TDataSource;
    QDestinoFILIAL: TBCDField;
    QDestinoSEQUENCIA: TBCDField;
    dbCliente: TDBLookupComboBox;
    QTarefaNRO_OCS: TBCDField;
    QTarefaRAZSOC: TStringField;
    QTarefaMUN_DES: TStringField;
    QTarefaTIPO: TStringField;
    QTarefaPAGADOR: TBCDField;
    QTarefaDESTINATARIO: TBCDField;
    QTarefaFILIAL: TBCDField;
    QTarefaCLIENTE: TStringField;
    QTarefaREMETENTE: TStringField;
    QTarefaSEQUENCIA: TBCDField;
    mdTempReg: TIntegerField;
    dgTarefa: TJvDBUltimGrid;
    btnVoltaTarefa: TBitBtn;
    btnCarregar_Tarefas: TBitBtn;
    Panel4: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    edTotPesoTarefa: TJvCalcEdit;
    edTotValorTarefa: TJvCalcEdit;
    edTotFreteTarefa: TJvCalcEdit;
    edTotCubadoTarefa: TJvCalcEdit;
    edTotVolumeTarefa: TJvCalcEdit;
    btnNovoCTe: TBitBtn;
    SPCalculo: TADOStoredProc;
    QTarefaCOD_MUN_DES: TBCDField;
    QTarefaFL_CTE: TBCDField;
    SP_TB_COLETA: TADOStoredProc;
    SPCte: TADOStoredProc;
    cbTrans: TComboBox;
    QGeradosCOD_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO_COMP: TBCDField;
    QGeradosDT_CONHECIMENTO: TDateTimeField;
    QGeradosDT_CONHECIMENTO_COMP: TDateTimeField;
    QGeradosCOD_PAGADOR: TBCDField;
    QGeradosCOD_REMETENTE: TBCDField;
    QGeradosCOD_DESTINATARIO: TBCDField;
    QGeradosCOD_REDESPACHO: TBCDField;
    QGeradosVL_DECLARADO_NF: TFloatField;
    QGeradosNR_QTDE_VOL: TFloatField;
    QGeradosNR_PESO_KG: TFloatField;
    QGeradosNR_PESO_CUB: TFloatField;
    QGeradosVL_FRETE_PESO: TFloatField;
    QGeradosVL_AD: TFloatField;
    QGeradosVL_GRIS: TFloatField;
    QGeradosVL_TR: TFloatField;
    QGeradosVL_TDE: TFloatField;
    QGeradosVL_OUTROS: TFloatField;
    QGeradosVL_PEDAGIO: TFloatField;
    QGeradosVL_FLUVIAL: TFloatField;
    QGeradosVL_EXCED: TFloatField;
    QGeradosVL_DESPACHO: TFloatField;
    QGeradosVL_BALSA: TFloatField;
    QGeradosVL_REDFLU: TFloatField;
    QGeradosVL_AGEND: TFloatField;
    QGeradosVL_PALLET: TFloatField;
    QGeradosVL_PORTO: TFloatField;
    QGeradosVL_ALFAND: TFloatField;
    QGeradosVL_CANHOTO: TFloatField;
    QGeradosVL_REENT: TFloatField;
    QGeradosVL_DEVOL: TFloatField;
    QGeradosVL_BASE: TFloatField;
    QGeradosVL_ALIQUOTA: TFloatField;
    QGeradosVL_IMPOSTO: TFloatField;
    QGeradosVL_TOTAL: TFloatField;
    QGeradosNM_USUARIO: TStringField;
    QGeradosOPERACAO: TBCDField;
    QGeradosDS_TIPO_MERC: TStringField;
    QGeradosDS_TIPO_FRETE: TStringField;
    QGeradosDS_OBS: TStringField;
    QGeradosDS_PLACA: TStringField;
    QGeradosMOTORISTA: TBCDField;
    QGeradosFL_STATUS: TStringField;
    QGeradosFL_TIPO: TStringField;
    QGeradosCODMUNO: TBCDField;
    QGeradosCODMUND: TBCDField;
    QGeradosCFOP: TStringField;
    QGeradosNR_FATURA: TBCDField;
    QGeradosNR_TABELA: TBCDField;
    QGeradosPEDIDO: TStringField;
    QGeradosNR_MANIFESTO: TBCDField;
    QGeradosFL_EMPRESA: TBCDField;
    QGeradosNR_RPS: TBCDField;
    QGeradosDS_CANCELAMENTO: TStringField;
    QGeradosNR_SERIE: TStringField;
    QGeradosVL_IMPOSTO_GNRE: TFloatField;
    QGeradosIMP_DACTE: TStringField;
    QGeradosFL_CONTINGENCIA: TStringField;
    QGeradosFRETE_IMPOSTO: TFloatField;
    QGeradosSEGURO_IMPOSTO: TFloatField;
    QGeradosGRIS_IMPOSTO: TFloatField;
    QGeradosOUTRAS_IMPOSTO: TFloatField;
    QGeradosFLUVIAL_IMPOSTO: TFloatField;
    QGeradosPEDAGIO_IMPOSTO: TFloatField;
    QGeradosCTE_DATA: TDateTimeField;
    QGeradosCTE_CHAVE: TStringField;
    QGeradosCTE_PROT: TStringField;
    QGeradosCTE_XMOTIVO: TStringField;
    QGeradosCTE_RECIBO: TStringField;
    QGeradosCTE_STATUS: TBCDField;
    QGeradosCTE_XMSG: TStringField;
    QGeradosFL_DDR: TStringField;
    QGeradosCTE_CAN_PROT: TStringField;
    QGeradosCTE_CHAVE_CTE_R: TStringField;
    QGeradosVL_TAS: TFloatField;
    QGeradosCLIENTE: TStringField;
    QGeradosNM_ORIGEM: TStringField;
    QGeradosNM_DESTINO: TStringField;
    Label28: TLabel;
    edQtPallet: TJvCalcEdit;
    edCteIni: TJvCalcEdit;
    edCteFim: TJvCalcEdit;
    Label29: TLabel;
    Label30: TLabel;
    btnLimpar_lote: TBitBtn;
    Label31: TLabel;
    edVol: TJvCalcEdit;
    Label41: TLabel;
    edPes: TJvCalcEdit;
    Label46: TLabel;
    edNF: TJvCalcEdit;
    Label47: TLabel;
    edCub: TJvCalcEdit;
    cbRateio: TCheckBox;
    mdTarefa: TJvMemoryData;
    mdTarefafl_cte: TIntegerField;
    Label49: TLabel;
    edAjudante: TJvCalcEdit;
    QTarefaCOD_MUN_O: TBCDField;
    QCidadeESTADO: TStringField;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label15: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label23: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label38: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    Label48: TLabel;
    Label45: TLabel;
    Label50: TLabel;
    edagend: TJvCalcEdit;
    edpallet: TJvCalcEdit;
    edentrega: TJvCalcEdit;
    edalfand: TJvCalcEdit;
    edcanhoto: TJvCalcEdit;
    edred_fluv: TJvCalcEdit;
    edFreteM: TJvCalcEdit;
    edPedagio: TJvCalcEdit;
    edOutros: TJvCalcEdit;
    edfluvial: TJvCalcEdit;
    edExc: TJvCalcEdit;
    edTR: TJvCalcEdit;
    eddespacho: TJvCalcEdit;
    edTDE: TJvCalcEdit;
    edtas: TJvCalcEdit;
    EdSeg: TJvCalcEdit;
    EdGris: TJvCalcEdit;
    edseg_balsa: TJvCalcEdit;
    edImposto: TJvCalcEdit;
    edAliq: TJvCalcEdit;
    edBase: TJvCalcEdit;
    edTotal: TJvCalcEdit;
    edCFOP: TJvMaskEdit;
    edTabela: TJvMaskEdit;
    edFretePeso: TJvCalcEdit;
    edTxCte: TJvCalcEdit;
    edVlAjudante: TJvCalcEdit;
    Panel6: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label73: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    edAgendNF: TJvCalcEdit;
    edPalletNF: TJvCalcEdit;
    edPortoNF: TJvCalcEdit;
    edAlfanNF: TJvCalcEdit;
    edCanhotoNF: TJvCalcEdit;
    edRedNF: TJvCalcEdit;
    edFreteMNF: TJvCalcEdit;
    edPedagioNF: TJvCalcEdit;
    edOutrosNF: TJvCalcEdit;
    edFluvialNF: TJvCalcEdit;
    edExcedNF: TJvCalcEdit;
    edTRNF: TJvCalcEdit;
    edDespachoNF: TJvCalcEdit;
    edTDENF: TJvCalcEdit;
    edTASNF: TJvCalcEdit;
    edSegNF: TJvCalcEdit;
    edGrisNF: TJvCalcEdit;
    edBalsaNF: TJvCalcEdit;
    edTTNF: TJvCalcEdit;
    edFretePesoNF: TJvCalcEdit;
    edTaxaNF: TJvCalcEdit;
    edAjudaNF: TJvCalcEdit;
    JvDBGrid1: TJvDBGrid;
    QNF: TADOQuery;
    dtsQNF: TDataSource;
    QNFNOTFIS: TStringField;
    QNFNOTNFE: TStringField;
    QCity: TADOQuery;
    QCityCODMUN: TBCDField;
    QCityDESCRI: TStringField;
    QCityESTADO: TStringField;
    QRaz: TADOQuery;
    QTranspTRANSP: TStringField;
    cbOS: TCheckBox;
    ACBrCEP1: TACBrCEP;
    mdTarefadestino: TIntegerField;
    QNFCFOP: TStringField;
    cbUFO: TComboBox;
    CBUF: TComboBox;
    cbCidadeO: TComboBox;
    cbCidadeD: TComboBox;
    Query1: TADOQuery;
    QOperacao: TADOQuery;
    QOperacaoCOD: TStringField;
    QOperacaoOPERACAO: TStringField;
    tsTrip: TTabSheet;
    dbgtrip: TJvDBUltimGrid;
    Qtrip: TADOQuery;
    Qtripfilial: TBCDField;
    Q_Dados_trip: TADOQuery;
    dtsTrip: TDataSource;
    QOcs_trip: TADOQuery;
    QOcs_tripSEQUENCIA: TFMTBCDField;
    QtripID_TRIP: TStringField;
    Q_Dados_tripPESO: TFMTBCDField;
    Q_Dados_tripVALOR: TFMTBCDField;
    Q_Dados_tripTRIP: TStringField;
    Q_Dados_tripCUB: TFMTBCDField;
    QPGR_M: TADOQuery;
    SP_PGR: TADOStoredProc;
    QPGR_MSUMAVLRMER: TFMTBCDField;
    QPGR_MPAGADOR: TFMTBCDField;
    QPGR_MID_REGRA: TFMTBCDField;
    QPGR_MRAZSOC: TStringField;
    Label70: TLabel;
    edVlAereo: TJvCalcEdit;
    QTarefaAEREO: TStringField;
    QTarefaORDER_CLI: TStringField;
    QTarefaNF: TStringField;
    Label71: TLabel;
    edKm: TJvCalcEdit;
    QKM: TADOQuery;
    QKMCODCLIFOR: TBCDField;
    QKMGEO: TStringField;
    QKMDESCRI: TStringField;
    QKMCODCEP: TStringField;
    XMLDoc: TXMLDocument;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Label72: TLabel;
    Label74: TLabel;
    edTDA: TJvCalcEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgColetaCellClick(Column: TColumn);
    procedure dbgColetaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnGerarClick(Sender: TObject);
    procedure btnCarregar_TarefasClick(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCarregaClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure rgTarefasClick(Sender: TObject);
    procedure btnCancelaCTeClick(Sender: TObject);
    procedure mdTempAfterOpen(DataSet: TDataSet);
    procedure tbTarefaShow(Sender: TObject);
    procedure btnVoltaTarefaClick(Sender: TObject);
    procedure dstTarefaDataChange(Sender: TObject; Field: TField);
    procedure ckRotaClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure btnNovoCTeClick(Sender: TObject);
    procedure cbOperacaoChange(Sender: TObject);
    procedure btnStatusClick(Sender: TObject);
    procedure MStatKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnSefazClick(Sender: TObject);
    procedure cbplacaChange(Sender: TObject);
    procedure mdTempFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure cbFiltroClick(Sender: TObject);
    procedure mdTempAfterPost(DataSet: TDataSet);
    procedure cbPesquisaChange(Sender: TObject);
    procedure cbUFOExit(Sender: TObject);
    procedure cbCidadeOExit(Sender: TObject);
    procedure cbUFExit(Sender: TObject);
    procedure cbCidadedExit(Sender: TObject);
    procedure edDataChange(Sender: TObject);
    procedure dgTarefaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QTarefaAfterScroll(DataSet: TDataSet);
    procedure Limpavalores;
    procedure CarregaCor;
    procedure tbCteShow(Sender: TObject);
    procedure tbCteHide(Sender: TObject);
    procedure QGeradosBeforeOpen(DataSet: TDataSet);
    procedure cbTransExit(Sender: TObject);
    procedure edCteFimExit(Sender: TObject);
    procedure edCteIniExit(Sender: TObject);
    procedure btnLimpar_loteClick(Sender: TObject);
    procedure dgCtrcDblClick(Sender: TObject);
    procedure tsTripShow(Sender: TObject);
    procedure dbgtripCellClick(Column: TColumn);
    procedure dbgtripDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QtripBeforeOpen(DataSet: TDataSet);
    procedure Panel10Click(Sender: TObject);
    procedure Panel11Click(Sender: TObject);
    procedure Panel12Click(Sender: TObject);

  private
    { Private declarations }
    iRecDelete, v_codmun, v_codmunD, paga: Integer;

    bInicio: Boolean;
    contmarg, tipo_transporte: String;
    cli, ori, des, GeraCTe, fdefaultStyleName: String;

    procedure CarregaRomaneio();
    procedure CarregaTarefas();
    function RetornaTarefasSelecionadas(): String;
    procedure calculakm;
    procedure escolhidotipo(tipo: String);


  public
    { Public declarations }

    procedure CarregaBtn();
  end;

var
  frmGeraCTe: TfrmGeraCTe;
  fTotPesoRomaneio, receita: real;
  dife, alterado: string;
  reg: Integer;

implementation

uses Dados, menu, funcoes, uLimpaRomaneio, Dacte_Cte_2, AltCte;

{$R *.dfm}

procedure TfrmGeraCTe.btnCancelaCTeClick(Sender: TObject);
begin
  btnCancelaCTe.Enabled := false;
  pnManifesto.Enabled := false;
  btnGerar.Enabled := false;
  btnNovoCTe.Enabled := true;
  btnVoltaTarefa.Enabled := true;
  btnCarregar_Tarefas.Enabled := true;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.SQL.clear;
  dtmdados.RQuery1.SQL.Add
    ('update tb_coleta set user_cte = null where doc is null and user_cte = :0');
  dtmdados.RQuery1.Parameters[0].value := GLBUSER;
  dtmdados.RQuery1.ExecSQL;
  dtmdados.RQuery1.close;

  cbOperacao.ItemIndex := -1;
  cbTrans.Clear;
  cbMotorista.Clear;
  edVeiculo.Clear;
  CBUF.Clear;
  cbUFO.Clear;
  cbCidadeO.Clear;
  cbCidadeD.Clear;

  Qtarefa.close;

  // CarregaRomaneioManifesto();
end;

procedure TfrmGeraCTe.btnCarregaClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraCTe.btnCarregar_TarefasClick(Sender: TObject);
begin
  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  CBUF.ItemIndex := -1;
  cbCidadeO.ItemIndex := -1;
  cbUFO.ItemIndex := -1;
  cbCidadeD.ItemIndex := -1;
  Limpavalores;
  CarregaBtn();
end;

procedure TfrmGeraCTe.btnNovoCTeClick(Sender: TObject);
var
  pag, reg: Integer;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  btnCarregar_TarefasClick(Sender);

  if QTarefa.RecordCount = 0 then
    Exit;

  if cbOS.Checked = false then
    ShowMessage
      ('Ser� Gerado 1 CT-e para Cada Linha, Analise se � isto mesmo o que deseja.');

  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  CBUF.ItemIndex := -1;
  cbCidadeO.ItemIndex := -1;
  cbUFO.ItemIndex := -1;
  cbCidadeD.ItemIndex := -1;

  edVeiculo.clear;
  // cbUf.Visible := false;
  // cbCidaded.Visible := false;

  btnVoltaTarefa.Enabled := false;
  btnCarregar_Tarefas.Enabled := false;
  pnManifesto.Enabled := true;
  btnCancelaCTe.Enabled := true;
  btnNovoCTe.Enabled := false;
  Obs.clear;
  QTransp.open;
  cbTrans.clear;
  while not QTransp.eof do
  begin
    cbTrans.Items.Add(QTranspTRANSP.value);
    QTransp.next;
  end;
  if QTransp.locate('TRANSP', 'INTECOM SERVICOS DE LOGISTICA LTDA | 1145', []) then
  cbTrans.ItemIndex := cbTrans.Items.IndexOf
    ('INTECOM SERVICOS DE LOGISTICA LTDA | 1145');

  QTarefa.DisableControls;
  QTarefa.First;
  pag := QTarefaPAGADOR.AsInteger;
  reg := 0;
  { des := QTarefaCOD_MUN_DES.AsInteger;
    while not (QTarefa.eof) do
    begin
    if (QTarefaPAGADOR.AsInteger <> pag) then
    begin
    reg := QTarefaSEQUENCIA.AsInteger;
    end;

    if (QTarefaCOD_MUN_DES.AsInteger <> des) and (des > 0)then
    begin
    des := 0;
    end;

    QTarefa.Next;
    end;

    if reg > 0 then
    begin
    QTarefa.Locate('SEQUENCIA',reg,[]);
    ShowMessage('Existem Clientes Pagadores Diferentes para a Emiss�o do CT-e !!');
    btnCancelaCTeClick(Sender);
    exit;
    end; }

  QTarefa.First;
  while not(QTarefa.eof) do
  begin
    SP_TB_COLETA.Parameters[0].value := QTarefaSEQUENCIA.AsInteger;
    SP_TB_COLETA.Parameters[1].value := GLBUSER;
    SP_TB_COLETA.ExecProc;
    QTarefa.next;
  end;
  QTarefa.EnableControls;
  QTarefa.First;

  QCidade.close;
  QCidade.Parameters[0].value := QTarefaCOD_MUN_O.AsInteger;
  QCidade.open;

  cbUFO.ItemIndex := cbUFO.Items.IndexOf(QCidadeESTADO.value);

  cbCidadeO.clear;
  cbCidadeO.Items.Add('');
  while not QCidade.eof do
  begin
    cbCidadeO.Items.Add(QCidadeDESCRI.value);
    QCidade.next;
  end;
  cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(QCidadeDESCRI.value);
  QCidade.locate('descri', cbCidadeO.text, []);
  v_codmun := QCidadeCODMUN.AsInteger;

  // if des > 0 then
  // begin
  QCons.close;
  QCons.SQL.clear;
  QCons.SQL.Add('select estado from cyber.rodmun where codmun = :0');
  QCons.Parameters[0].value := QTarefaCOD_MUN_DES.AsInteger;
  QCons.open;
  CBUF.ItemIndex := CBUF.Items.IndexOf(QCons.FieldByName('estado').value);

  QCidade.close;
  QCidade.Parameters[0].value := QTarefaCOD_MUN_DES.AsInteger;;
  // QCons.FieldByName('estado').value;
  QCidade.open;
  cbCidadeD.clear;
  cbCidadeD.Items.Add('');
  while not QCidade.eof do
  begin
    cbCidadeD.Items.Add(QCidadeDESCRI.value);
    QCidade.next;
  end;
  cbCidadeD.ItemIndex := cbCidadeD.Items.IndexOf(QTarefaMUN_DES.AsString);
  v_codmunD := QTarefaCOD_MUN_DES.AsInteger;
  QCons.close;
  // end;

  QMotor.close;
  QMotor.Parameters[0].value := 1145; //copy(QTranspTRANSP.value,Pos('|', QTranspTRANSP.value) + 1, 10);
  QMotor.open;

  cbMotorista.clear;
  while not QMotor.eof do
  begin
    cbMotorista.Items.Add(QMotorNOME.value);
    QMotor.next;
  end;
  QMotor.close;
 // QTransp.close;
  cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('146 - IMPORTA��O');
  QVeiculoCasa.close;
  QVeiculoCasa.open;
  cbplaca.clear;
  cbplaca.Items.Add('');
  while not QVeiculoCasa.eof do
  begin
    cbplaca.Items.Add(QVeiculoCasaPLACA.value);
    QVeiculoCasa.next;
  end;
  cbplaca.ItemIndex := cbplaca.Items.IndexOf('XXX-9999');

  QVeiculoCasa.locate('placa', cbplaca.text, []);
  edVeiculo.text := QVeiculoCasaDESCRI.value;

  if cbOperacao.CanFocus then
    cbOperacao.SetFocus;
end;

procedure TfrmGeraCTe.btnSefazClick(Sender: TObject);
var
  reg: Integer;
  sData, sCaminho, sMDFe, arquivo, lote, envio, nm_loja, sArquivo: string;
  stStreamNF: TStringStream;
  email, smtp, senhaemail, porta, efiscal, SQL, chave: string;
  SSL: Boolean;
  cc, mensagem, anexo: TStrings;
  Info: TextFile;
begin
  anexo := TStringList.Create;
  mensagem := TStringList.Create;
  cc := TStringList.Create;

  if QGeradosFL_TIPO.value = 'C' then
  begin
    SQL := QGerados.SQL.text;
    btnLimpar_lote.Enabled := false;
    btnStatus.Enabled := false;

    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.Add
      ('select ambiente_cte, certificado, logo, pdf_cte, ');
    dtmdados.IQuery1.SQL.Add('schemma, proxy, porta, user_proxy, pass_proxy ');
    dtmdados.IQuery1.SQL.Add
      ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
    dtmdados.IQuery1.Parameters[0].value := GLBFilial;
    dtmdados.IQuery1.open;

    GlbCteAmb := dtmdados.IQuery1.FieldByName('ambiente_cte').AsString;
    frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
    sData := FormatDateTime('YYYY', QGeradosDT_CONHECIMENTO.value) +
      FormatDateTime('MM', QGeradosDT_CONHECIMENTO.value);
    sCaminho := frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCTe + '\' + sData
      + '\CTe\';
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyHost :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPort :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyUser :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPass :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
    dtmdados.IQuery1.close;

    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.Add('select * from tb_empresa where codfil = :0 ');
    dtmdados.IQuery1.Parameters[0].value := GLBFilial;
    dtmdados.IQuery1.open;
    smtp := dtmdados.IQuery1.FieldByName('smtp').value;
    porta := dtmdados.IQuery1.FieldByName('porta').value;
    senhaemail := dtmdados.IQuery1.FieldByName('senha').value;
    email := dtmdados.IQuery1.FieldByName('email').value;
    efiscal := dtmdados.IQuery1.FieldByName('efiscal').value;
    envio := dtmdados.IQuery1.FieldByName('efiscal').AsString;
    if dtmdados.IQuery1.FieldByName('reqaut').value = 'S' then
      SSL := true
    else
      SSL := false;
    lote := '';
    dtmdados.IQuery1.close;
    // por lote
    if (edCteIni.value > 0) and (edCteFim.value > 0) then
    begin
      lote := 'S';
      QGerados.close;
      QGerados.SQL[10] := 'And nr_conhecimento between ' + #39 +
        IntToStr(edCteIni.AsInteger) + #39 + ' and ' + #39 +
        IntToStr(edCteFim.AsInteger) + #39;
      // para simular envio por lote
      if GLBUSER = 'PCORTEZ' then
        QGerados.SQL[11] := ''
      else
        QGerados.SQL[11] := 'And r.nm_usuario = ' + QuotedStr(GLBUSER);
      // showmessage(Qgerados.SQL.Text);
      QGerados.open;
      if Application.Messagebox('Enviar para o Sefaz agora ?', 'Enviar Sefaz',
        MB_YESNO + MB_ICONQUESTION) = IDYES then
      begin

        sData := GLBUSER + FormatDateTime('dd/mm/yy - hh:mm:ss', now);

        while not QGerados.eof do
        begin

          if (QGeradosCTE_STATUS.value = 204)
            or (QGeradosCTE_STATUS.value = 609) then
          begin
            frmMenu.ACBrCte1.WebServices.Consulta.CTeChave :=
              QGeradosCTE_CHAVE.AsString;
            frmMenu.ACBrCte1.WebServices.Consulta.Executar;
            dtmdados.IQuery1.close;
            dtmdados.IQuery1.SQL.clear;
            dtmdados.IQuery1.SQL.Add
              ('update tb_conhecimento set cte_prot = :0, ');
            dtmdados.IQuery1.SQL.Add
              ('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
            dtmdados.IQuery1.SQL.Add
              ('cte_xmsg = ''Autorizado o uso do CT-e'' ');
            dtmdados.IQuery1.SQL.Add('where cte_chave = :1 ');
            dtmdados.IQuery1.Parameters[0].value :=
              frmMenu.ACBrCte1.WebServices.Consulta.Protocolo;
            dtmdados.IQuery1.Parameters[1].value := QGeradosCTE_CHAVE.AsString;
            dtmdados.IQuery1.ExecSQL;
            dtmdados.IQuery1.close;

            dtmdados.IQuery1.close;
            dtmdados.IQuery1.SQL.clear;
            dtmdados.IQuery1.SQL.Add('update cyber.rodcon set ctepro = :0 ');
            dtmdados.IQuery1.SQL.Add('where cte_id = :1 ');
            dtmdados.IQuery1.Parameters[0].value :=
              frmMenu.ACBrCte1.WebServices.Consulta.Protocolo;
            dtmdados.IQuery1.Parameters[1].value := QGeradosCTE_CHAVE.AsString;
            dtmdados.IQuery1.ExecSQL;
            dtmdados.IQuery1.close;
          end
          else
          begin
            reg := QGeradosCOD_CONHECIMENTO.AsInteger;
            btnSefaz.Enabled := false;
            frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
            btnSefaz.Enabled := true;
          end;

          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('update tb_conhecimento set lote_sefaz = :0 ');
          dtmdados.IQuery1.SQL.Add('where cod_conhecimento = :1 ');
          dtmdados.IQuery1.Parameters[0].value := sData;
          dtmdados.IQuery1.Parameters[1].value := QGeradosCOD_CONHECIMENTO.AsInteger;
          dtmdados.IQuery1.ExecSQL;
          dtmdados.IQuery1.close;

          QGerados.next;
        end;
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.SQL.clear;
        dtmdados.IQuery1.SQL.Add
          ('select cod_conhecimento from tb_conhecimento ');
        dtmdados.IQuery1.SQL.Add('where lote_sefaz = :0 ');
        dtmdados.IQuery1.Parameters[0].value := sData;
        dtmdados.IQuery1.open;
        while not dtmdados.IQuery1.eof do
        begin
          anexo := TStringList.Create;
          cc := TStringList.Create;
          mensagem := TStringList.Create;
          frmMenu.QrCteEletronico.close;
          frmMenu.QrCteEletronico.Parameters[0].value :=
            dtmdados.IQuery1.FieldByName('COD_CONHECIMENTO').AsInteger;
          frmMenu.QrCteEletronico.open;
          frmMenu.QNF.close;
          frmMenu.QNF.Parameters[0].value :=
            frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
          frmMenu.QNF.Parameters[1].value :=
            frmMenu.qrCteEletronicoNR_SERIE.value;
          frmMenu.QNF.Parameters[2].value :=
            frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
          frmMenu.QNF.open;
          try
            frmMenu.ACBrCTeDACTeRL1.MostraPreview := false;
            frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;
            frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
            frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
            frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
            frmMenu.ACBrCTeDACTeRL1.MostraPreview := true;
          finally
          end;

          if GlbCteAmb = 'P' then
          begin
            Query1.close;
            Query1.SQL.clear;
            Query1.SQL.Add
              ('select email from cyber.rodctc e left join tb_conhecimento c on c.cod_pagador = e.codclifor ');
            Query1.SQL.Add
              ('where situac = ''A'' and c.cod_conhecimento = :0 and email is not null');
            Query1.Parameters[0].value :=
              frmMenu.qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
            Query1.open;
            while not Query1.eof do
            begin
              cc.Add(Query1.FieldByName('email').AsString);
              Query1.next;
            end;
            Query1.close;
          end;

          // chamado 16495 foi retirado 5932
          // GNRE
          if (frmMenu.qrCteEletronicoCFOP.AsString = '6932') then
            cc.Add('fiscal@intecomlogistica.com.br');

          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add
            ('select distinct razsoc from cyber.rodfil f ');
          dtmdados.RQuery1.SQL.Add('where codfil = :0');
          dtmdados.RQuery1.Parameters[0].value :=
            frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
          dtmdados.RQuery1.open;

          mensagem.Add('A ' + dtmdados.RQuery1.FieldByName('razsoc').AsString +
            ' emitiu o CT-e Anexo');
          mensagem.Add(' ');
          mensagem.Add('>>> Sistema SIM - TI Intecom <<<');

          try
            sData := FormatDateTime('YYYY',
              frmMenu.qrCteEletronicoEMISSAOCTE.value) +
              FormatDateTime('MM', frmMenu.qrCteEletronicoEMISSAOCTE.value);
            arquivo := frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar +
              sData + '\CTe\' + frmMenu.qrCteEletronicoCTE_CHAVE.AsString +
              '-cte.xml';
            frmMenu.ACBrCte1.Conhecimentos.clear;
            frmMenu.ACBrCte1.Conhecimentos.LoadFromFile(arquivo);
            frmMenu.ACBrCte1.Conhecimentos.Items[0].EnviarEmail(efiscal // Para
              , 'CT-e' // edtEmailAssunto.Text
              , mensagem // mmEmailMsg.Lines
              , true // Enviar PDF junto
              , cc // nil //Lista com emails que ser�o enviado c�pias - TStrings
              , nil // Lista de anexos - TStrings
              );
          except
            on E: Exception do
          end;
          anexo.Free;
          cc.Free;
          mensagem.Free;
          dtmdados.IQuery1.next;
        end;
      end;
    end
    else
    // POR CT-E
    begin
      reg := QGeradosCOD_CONHECIMENTO.AsInteger;

      if QGeradosCTE_STATUS.value = 204 then
      begin
        // frmMenu.ACBrCTe1.Configuracoes.WebServices.UF := glb;
        // verifica ambiente do cte
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.SQL.clear;
        dtmdados.IQuery1.SQL.Add
          ('select ambiente_cte, certificado, logo, pdf, ');
        dtmdados.IQuery1.SQL.Add
          ('schemma, proxy, porta, user_proxy, pass_proxy ');
        dtmdados.IQuery1.SQL.Add
          ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
        dtmdados.IQuery1.Parameters[0].value := GLBFilial;
        dtmdados.IQuery1.open;

        if dtmdados.IQuery1.FieldByName('ambiente_cte').AsString = 'H' then
        begin
          frmMenu.ACBrCte1.Configuracoes.WebServices.ambiente := taHomologacao;
        end
        else
        begin
          frmMenu.ACBrCte1.Configuracoes.WebServices.ambiente := taProducao;
        end;
        frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie :=
          ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
        dtmdados.IQuery1.close;

        frmMenu.ACBrCte1.WebServices.Consulta.CTeChave :=
          QGeradosCTE_CHAVE.AsString;
        frmMenu.ACBrCte1.WebServices.Consulta.Executar;
        if frmMenu.ACBrCte1.WebServices.Consulta.Protocolo <> '' then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('update tb_conhecimento set cte_prot = :0, ');
          dtmdados.IQuery1.SQL.Add
            ('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
          dtmdados.IQuery1.SQL.Add
            ('cte_xmsg = ''Autorizado o uso do CTE-e...'', cte_data = :1 ');
          dtmdados.IQuery1.SQL.Add('where cte_chave = :2 ');
          dtmdados.IQuery1.Parameters[0].value :=
            frmMenu.ACBrCte1.WebServices.Consulta.Protocolo;
          dtmdados.IQuery1.Parameters[1].value :=
            frmMenu.ACBrCte1.WebServices.Consulta.DhRecbto;
          dtmdados.IQuery1.Parameters[2].value := QGeradosCTE_CHAVE.AsString;
          dtmdados.IQuery1.ExecSQL;
          dtmdados.IQuery1.close;

          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add('update cyber.rodcon set ctepro = :0 ');
          dtmdados.IQuery1.SQL.Add('where cte_id = :1 ');
          dtmdados.IQuery1.Parameters[0].value :=
            frmMenu.ACBrCte1.WebServices.Consulta.Protocolo;
          dtmdados.IQuery1.Parameters[1].value := QGeradosCTE_CHAVE.AsString;
          dtmdados.IQuery1.ExecSQL;
          dtmdados.IQuery1.close;

          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select ambiente_cte, pdf from tb_controle where doc = ''MDFE'' and filial = :0 ');
          dtmdados.IQuery1.Parameters[0].value := GLBFilial;
          dtmdados.IQuery1.open;

          sData := FormatDateTime('YYYY', QGeradosDT_CONHECIMENTO.value) +
            FormatDateTime('MM', QGeradosDT_CONHECIMENTO.value);
          sMDFe := QGeradosCTE_CHAVE.AsString + '-cte.xml';
          frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCTe :=
            IncludeTrailingPathDelimiter
            (ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString));
          frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar :=
            IncludeTrailingPathDelimiter
            (ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString));

          frmMenu.QCtexml.close;
          frmMenu.QCtexml.SQL[1] := 'Where cod_conhecimento = ' + '' +
            QGeradosCOD_CONHECIMENTO.AsString + '';
          frmMenu.QCtexml.open;
          if not frmMenu.QCtexml.eof then
          begin
            stStreamNF := TStringStream.Create(frmMenu.QCteXmlxml.value);
            frmMenu.ACBrCte1.Conhecimentos.clear;
            frmMenu.ACBrCte1.Conhecimentos.LoadFromString
              (frmMenu.QCteXmlxml.AsString);
          end
          else
          begin
            frmMenu.ACBrCte1.Conhecimentos.clear;
            frmMenu.ACBrCte1.Conhecimentos.LoadFromFile(sCaminho + sMDFe);
          end;
          frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;

          frmMenu.ACBrCte1.Conhecimentos.Imprimir;
          frmMenu.ACBrCte1.Conhecimentos.Imprimir;
          frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
          frmMenu.QCtexml.close;
        end;
      end
      else
      begin
        btnSefaz.Enabled := false;
        frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
        btnSefaz.Enabled := true;
        try
          frmMenu.QrCteEletronico.close;
          frmMenu.QrCteEletronico.Parameters[0].value :=
            QGeradosCOD_CONHECIMENTO.AsInteger;
          frmMenu.QrCteEletronico.open;
          // se tem protocolo imprimi
          if not frmMenu.qrCteEletronicoPROTOCOLO.IsNull then
          begin
            frmMenu.QNF.close;
            frmMenu.QNF.Parameters[0].value :=
              frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
            frmMenu.QNF.Parameters[1].value :=
              frmMenu.qrCteEletronicoNR_SERIE.value;
            frmMenu.QNF.Parameters[2].value :=
              frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
            frmMenu.QNF.open;
            frmMenu.ACBrCTeDACTeRL1.MostraPreview := false;
            frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;
            frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
            frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
            frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
            frmMenu.ACBrCTeDACTeRL1.MostraPreview := true;
            //Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
            //frmDacte_Cte_2.sChaveCte :=
            //  frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
            //frmDacte_Cte_2.rlDacte.Print();
            //frmDacte_Cte_2.rlDacte.Print();
            //frmDacte_Cte_2.rlDacte.SaveToFile
            //  (frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar + '\' +
            //  frmMenu.qrCteEletronicoCTE_CHAVE.AsString + '-cte.pdf');
          end;
        finally
          //frmDacte_Cte_2.Free;
        end;

        if GlbCteAmb = 'P' then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select email from cyber.rodctc e left join tb_conhecimento c on c.cod_pagador = e.codclifor ');
          dtmdados.IQuery1.SQL.Add
            ('where situac = ''A'' and c.cod_conhecimento = :0 and email is not null');
          dtmdados.IQuery1.Parameters[0].value :=
            frmMenu.qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
          dtmdados.IQuery1.open;
          while not dtmdados.IQuery1.eof do
          begin
            cc.Add(dtmdados.IQuery1.FieldByName('email').AsString);
            dtmdados.IQuery1.next;
          end;
          dtmdados.IQuery1.close;
        end;

        // chamado 16495 foi retirado 5932
        if (frmMenu.qrCteEletronicoCFOP.AsString = '6932') then
          cc.Add('fiscal@intecomlogistica.com.br');

        dtmdados.RQuery1.close;
        dtmdados.RQuery1.SQL.clear;
        dtmdados.RQuery1.SQL.Add('select distinct razsoc from cyber.rodfil f ');
        dtmdados.RQuery1.SQL.Add('where codfil = :0');
        dtmdados.RQuery1.Parameters[0].value :=
          frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
        dtmdados.RQuery1.open;

        mensagem.Add('A ' + dtmdados.RQuery1.FieldByName('razsoc').AsString +
          ' emitiu o CT-e Anexo');
        mensagem.Add(' ');
        mensagem.Add('>>> Sistema SIM - TI Intecom/IW <<<');

        //sArquivo := '\\192.168.236.112\sim\Atualizador\logemailcte.txt';

        //AssignFile(Info, sArquivo);
        //if not FileExists(sArquivo) then
        //  Rewrite(Info)
        //else
        //  Append(Info);

        try
          sData := FormatDateTime('YYYY',
            frmMenu.qrCteEletronicoEMISSAOCTE.value) +
            FormatDateTime('MM', frmMenu.qrCteEletronicoEMISSAOCTE.value);
          arquivo := frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar + sData
            + '\CTe\' + frmMenu.qrCteEletronicoCTE_CHAVE.AsString + '-cte.xml';
          frmMenu.ACBrCte1.Conhecimentos.clear;
          frmMenu.ACBrCte1.Conhecimentos.LoadFromFile(arquivo);
          frmMenu.ACBrCte1.Conhecimentos.Items[0].EnviarEmail(envio // Para
            , 'CT-e' // edtEmailAssunto.Text
            , mensagem // mmEmailMsg.Lines
            , true // Enviar PDF junto
            , cc // nil //Lista com emails que ser�o enviado c�pias - TStrings
            , nil // Lista de anexos - TStrings
            );
        //  WriteLn(Info, 'Enviado', formatdatetime('dd/mm/yy',frmMenu.qrCteEletronicoEMISSAOCTE.value),
        //    RetbranE(frmMenu.qrCteEletronicoNUMEROCTE.AsString, 10),
        //    RetbranE(frmMenu.qrCteEletronicoFL_EMPRESA.Asstring, 3),
        //    RetbranE(frmMenu.qrCteEletronicoNOMETOM.Asstring, 80));
        except
          on E: Exception do
        //  WriteLn(Info, 'Erro', formatdatetime('dd/mm/yy',frmMenu.qrCteEletronicoEMISSAOCTE.value),
        //    RetbranE(frmMenu.qrCteEletronicoNUMEROCTE.AsString, 10),
        //    RetbranE(frmMenu.qrCteEletronicoFL_EMPRESA.Asstring, 3),
        //    RetbranE(frmMenu.qrCteEletronicoNOMETOM.Asstring, 80));
        end;
        //closefile(Info);

      end;
      anexo.Free;
      cc.Free;
      mensagem.Free;

    end;

    btnLimpar_lote.Enabled := true;
    if lote = 'S' then
      btnLimpar_loteClick(Sender);
    btnStatus.Enabled := true;
    btnSefaz.Enabled := true;
    QGerados.close;
    QGerados.SQL.clear;
    QGerados.SQL.Add(SQL);
    QGerados.open;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrCte1.WebServices.StatusServico.Executar;
  MStat.Lines.text :=
    UTF8Encode(frmMenu.ACBrCte1.WebServices.StatusServico.RetWS);
  MStat.Visible := true;
  MStat.clear;
  MStat.Lines.Add('Status Servi�o');
  MStat.Lines.Add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrCte1.WebServices.StatusServico.tpAmb));
  MStat.Lines.Add('verAplic: ' + frmMenu.ACBrCte1.WebServices.
    StatusServico.verAplic);
  MStat.Lines.Add('cStat: ' +
    IntToStr(frmMenu.ACBrCte1.WebServices.StatusServico.cStat));
  MStat.Lines.Add('xMotivo: ' + frmMenu.ACBrCte1.WebServices.
    StatusServico.xMotivo);
  MStat.Lines.Add('cUF: ' +
    IntToStr(frmMenu.ACBrCte1.WebServices.StatusServico.cUF));
  MStat.Lines.Add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrCte1.WebServices.StatusServico.DhRecbto));
  MStat.Lines.Add('tMed: ' +
    IntToStr(frmMenu.ACBrCte1.WebServices.StatusServico.TMed));
  MStat.Lines.Add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrCte1.WebServices.StatusServico.dhRetorno));
  MStat.Lines.Add('xObs: ' + frmMenu.ACBrCte1.WebServices.StatusServico.xObs);
end;

procedure TfrmGeraCTe.btnVoltaTarefaClick(Sender: TObject);
begin
  self.tag := 1;
  iRecDelete := QTarefaSEQUENCIA.AsInteger;

  mdTemp.locate('Reg', QTarefaSEQUENCIA.AsInteger, []);

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempOCS.AsInteger;
  qrCalcula_Frete.Parameters[1].value := mdTemppagador.AsInteger;
  qrCalcula_Frete.open;

  edTotCubadoTarefa.value := edTotCubadoTarefa.value -
    qrCalcula_Frete.FieldByName('cub').AsFloat;
  edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
    ('peso').AsFloat;
  edTotValorTarefa.value := edTotValorTarefa.value - qrCalcula_Frete.FieldByName
    ('valor').AsFloat;
  edTotVolumeTarefa.value := edTotVolumeTarefa.value -
    qrCalcula_Frete.FieldByName('qtd').AsFloat;

  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.post;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.SQL.clear;
  dtmdados.RQuery1.SQL.Add
    ('update tb_coleta set user_cte = null, fl_cte = null ');
  dtmdados.RQuery1.SQL.Add('where doc is null and user_cte = :0');
  dtmdados.RQuery1.SQL.Add('and sequencia = :1');
  dtmdados.RQuery1.Parameters[0].value := GLBUSER;
  dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
  dtmdados.RQuery1.ExecSQL;
  dtmdados.RQuery1.close;

  CarregaBtn();
  self.tag := 0;
end;

procedure TfrmGeraCTe.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a, b: String;
  orig1, dest1: String;
  i, x, j, k: Integer;
  arquivokm, km: String;
  EntryNode, EntryType :IXmlNode;
begin
  x := 0;

  // pegar o cep do aeroporto da entrega como base de km
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select (latitude ||''+''|| longitude) posicao from tb_uf where estado = :0 ');
  dtmDados.IQuery1.Parameters[0].value := cbUF.text;
  dtmDados.IQuery1.Open;
  cep := BuscaTroca(dtmDados.IQuery1.FieldByName('posicao').value, ',', '.');
  dtmDados.IQuery1.close;

  QKm.Close;
  QKm.Parameters[0].Value := QTarefaDESTINATARIO.AsInteger;
  QKm.Open;

  if QKM.RecordCount > 0 then
  begin
    while not QKM.eof do
    begin
      if x = 0 then
      begin
        orig := cep;
        orig1 := orig;
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := BuscaTroca(QKMGEO.AsString, ',', '.')
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        dest1 := dest;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end;
      RestClient1.BaseURL := XMLUrl;
      RESTRequest1.Accept := 'text/xml';
      RESTRequest1.Execute;
      if RestResponse1.StatusCode = 200 then
      begin
        arquivokm := RESTResponse1.Content;
        XMLDoc.LoadFromXML(RESTResponse1.Content);
        XMLDoc.Active := true;
        EntryNode := XMLDoc.DocumentElement;
        EntryType := EntryNode.ChildNodes.First;
        for i := 0 to EntryNode.ChildNodes.Count -1 do
        begin
          if (EntryType.NodeName = 'row') then
          begin
            for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
              for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1) do
              begin
                a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName;
                if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName = 'distance' then
                begin
                  edkm.Value := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                    [j].ChildNodes[k].ChildNodes['text'].Text) + edkm.Value;
                end;
              end;
            end;
          end;
          EntryType := EntryType.NextSibling;
        end;
      end;
      XMLDoc.Active := false;
      x := x + 1;
      QKM.next;
    end;

    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';

    RestClient1.BaseURL := XMLUrl;
    RESTRequest1.Execute;
    if RestResponse1.StatusCode = 200 then
    begin
      arquivokm := RESTResponse1.Content;
      XMLDoc.LoadFromXML(RESTResponse1.Content);
      XMLDoc.Active := true;
      EntryNode := XMLDoc.DocumentElement;
      EntryType := EntryNode.ChildNodes.First;
      for i := 0 to EntryNode.ChildNodes.Count -1 do
      begin
        if (EntryType.NodeName = 'row') then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName = 'distance' then
              begin
                edkm.Value := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].Text) + edkm.Value;
              end;
            end;
          end;
        end;
        EntryType := EntryType.NextSibling;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

procedure TfrmGeraCTe.btnGerarClick(Sender: TObject);
var
  man, dest, erropgr: Integer;
  regman, v_tde: String;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  if cbMotorista.text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    cbMotorista.SetFocus;
    Exit;
  end;

  if cbplaca.text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    // cbPlaca.SetFocus;
    Exit;
  end;

  if cbOperacao.text = '' then
  begin
    ShowMessage('Escolha o Modal');
    cbOperacao.SetFocus;
    Exit;
  end;

  if cbCidadeO.text = '' then
  begin
    ShowMessage('Selecione a Cidade de Origem');
    cbCidadeO.SetFocus;
  end;

  if cbCidadeD.text = '' then
  begin
    ShowMessage('Selecione a Cidade de Destino');
    cbCidadeD.SetFocus;
  end;

  if edTotal.value <= 0 then
  begin
    btnGerar.Enabled := false;
    Exit;
  end;

  if (v_codmun = v_codmunD) then
  begin
    ShowMessage('Origem e Destino para Mesma Cidade deve ser NFs');
    Exit;
  end;


  erropgr := 0;
  // verifica��o das regras do PGR
  Screen.Cursor := crHourGlass;

  mdTarefa.close;
  mdTarefa.open;
  QTarefa.DisableControls;
  QTarefa.First;
  dest := 0;
  while not QTarefa.eof do
  begin
    SP_PGR.Parameters[2].Value := QTarefaSEQUENCIA.AsInteger;
    SP_PGR.ExecProc;
    if SP_PGR.Parameters[0].Value <> ' ' then
    begin
      showmessage(SP_PGR.Parameters[0].Value);
      erropgr := 1
    end;
    QTarefa.next;
  end;
  QTarefa.First;
  QTarefa.EnableControls;

  if erropgr = 0 then
  begin
    if Application.Messagebox('Gerar CT-e agora ?', 'Gerar CT-e',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      // geracte := 'S';

      mdTarefa.close;
      mdTarefa.open;
      QTarefa.DisableControls;
      QTarefa.First;
      dest := 0;
      while not QTarefa.eof do
      begin

        dtmdados.IQuery1.close;
        dtmdados.IQuery1.SQL.clear;
        dtmdados.IQuery1.SQL.Add
          ('select par_1, par_3 from tb_parametro where codcli = :0');
        dtmdados.IQuery1.Parameters[0].value := QTarefaPAGADOR.AsInteger;
        dtmdados.IQuery1.open;

        if not dtmdados.IQuery1.eof then
        begin

          v_tde := dtmdados.IQuery1.FieldByName('par_1').value;
          if v_tde = 'S' then
          begin
            ShowMessage('Verifique se Existe CFOP na NF !!');
          end;

          if (dtmdados.IQuery1.FieldByName('par_3').value = 'S') and
            (QNF.RecordCount > 1) then
          begin
            ShowMessage
              ('Para este Cliente N�o � permitido mais de 1 NF por CT-e');
            btnCancelaCTeClick(Sender);
            Exit;
          end;

        end
        else
          v_tde := 'N';

        // Verifica se o destinat�rio � para 1 NF por CT-e
        if QTarefaPAGADOR.AsInteger = 152 then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select distinct count(i.notfis) tem from tb_parametro_destinatario p, cyber.rodioc i where p.codcli = :0 and p.nf = ''S'' and i.codocs = :1 and i.filocs = :2 ');
          dtmdados.IQuery1.Parameters[0].value := QTarefaDESTINATARIO.AsInteger;
          dtmdados.IQuery1.Parameters[1].value := QTarefaNRO_OCS.AsInteger;
          dtmdados.IQuery1.Parameters[2].value := QTarefaFILIAL.AsInteger;
          dtmdados.IQuery1.open;
          if dtmdados.IQuery1.FieldByName('tem').value > 1 then
          begin
            ShowMessage
              ('Para este Destinat�rio N�o � permitido mais de 1 NF por CT-e - OCS : ' + QTarefaNRO_OCS.AsString);
            //btnCancelaCTeClick(Sender);
            Exit;
          end;
        end;

        // Verifica se o destinat�rio � para 1 Order por CT-e
        if QTarefaPAGADOR.AsInteger = 152 then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select distinct count(i.ordcom) tem from tb_parametro_destinatario p, cyber.rodioc i where p.codcli = :0 and p.order_cli = ''S'' and i.codocs = :1 and i.filocs = :2 ');
          dtmdados.IQuery1.Parameters[0].value := QTarefaDESTINATARIO.AsInteger;
          dtmdados.IQuery1.Parameters[1].value := QTarefaNRO_OCS.AsInteger;
          dtmdados.IQuery1.Parameters[2].value := QTarefaFILIAL.AsInteger;
          dtmdados.IQuery1.open;
          if dtmdados.IQuery1.FieldByName('tem').value > 1 then
          begin
            ShowMessage
              ('Para este Destinat�rio N�o � permitido mais de 1 Order por CT-e - OCS : ' + QTarefaNRO_OCS.AsString);
            //btnCancelaCTeClick(Sender);
            Exit;
          end;
        end;

        if not mdTarefa.locate('fl_cte', QTarefaFL_CTE.AsInteger, []) then
        begin
          mdTarefa.append;
          mdTarefafl_cte.value := QTarefaFL_CTE.AsInteger;
          mdTarefadestino.value := QTarefaDESTINATARIO.AsInteger;
          mdTarefa.post;
        end;

        if cbRateio.Checked = true then
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = :0 ');
          dtmdados.RQuery1.SQL.Add('where sequencia = :1');
          dtmdados.RQuery1.Parameters[0].value := GLBUSER;
          dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end
        else
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
          dtmdados.RQuery1.SQL.Add('where sequencia = :1');
          dtmdados.RQuery1.Parameters[0].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end;
        dest := QTarefaDESTINATARIO.AsInteger;
        QTarefa.next;
      end;

      QTarefa.First;
      QTarefa.EnableControls;
      // cliente n�o tem TDE na primeira NF
      if v_tde = 'N' then

      begin
        Screen.Cursor := crHourGlass;
        mdTarefa.First;
        while not mdTarefa.eof do
        begin
          QTarefa.locate('fl_cte', mdTarefafl_cte.value, []);
          if edTotal.value > 0 then
          begin
            SPCte.Parameters[1].value := mdTarefafl_cte.AsInteger;
            SPCte.Parameters[2].value := RemoveChar(cbOperacao.text);
            SPCte.Parameters[3].value := v_codmun;
            SPCte.Parameters[4].value := v_codmunD;
            SPCte.Parameters[5].value := GLBUSER;
            SPCte.Parameters[6].value := RemoveChar(cbMotorista.text);
            SPCte.Parameters[7].value := cbplaca.text;
            SPCte.Parameters[8].value := edFretePeso.value;
            SPCte.Parameters[9].value := edPedagio.value;
            SPCte.Parameters[10].value := edOutros.value;
            SPCte.Parameters[11].value := edfluvial.value;
            SPCte.Parameters[12].value := edExc.value;
            SPCte.Parameters[13].value := edTR.value;
            SPCte.Parameters[14].value := eddespacho.value;
            SPCte.Parameters[15].value := edTDE.value;
            SPCte.Parameters[16].value := edtas.value;
            SPCte.Parameters[17].value := EdSeg.value;
            SPCte.Parameters[18].value := EdGris.value;
            SPCte.Parameters[19].value := edseg_balsa.value;
            SPCte.Parameters[20].value := edred_fluv.value;
            SPCte.Parameters[21].value := edagend.value;
            SPCte.Parameters[22].value := edpallet.value;
            SPCte.Parameters[23].value := edentrega.value;
            SPCte.Parameters[24].value := edalfand.value;
            SPCte.Parameters[25].value := edcanhoto.value;
            SPCte.Parameters[26].value := edBase.value;
            SPCte.Parameters[27].value := edAliq.value;
            SPCte.Parameters[28].value := edImposto.value;
            SPCte.Parameters[29].value := edTotal.value;
            SPCte.Parameters[30].value := edCFOP.text;
            SPCte.Parameters[31].value := StrToInt(edTabela.text);
            SPCte.Parameters[32].value := GLBFilial;
            // Projeto 88 incluir a Order na Obaserva��o
            if QTarefaORDER_CLI.AsString = 'S' then
            begin
              dtmdados.IQuery1.close;
              dtmdados.IQuery1.SQL.clear;
              dtmdados.IQuery1.SQL.Add
                ('select distinct i.ordcom from cyber.rodioc i where i.codocs = :1 and i.filocs = :2 ');
              dtmdados.IQuery1.Parameters[0].value := QTarefaNRO_OCS.AsInteger;
              dtmdados.IQuery1.Parameters[1].value := QTarefaFILIAL.AsInteger;
              dtmdados.IQuery1.open;
              SPCte.Parameters[33].value := Obs.text + 'Order n� : ' + dtmdados.IQuery1.FieldByName('ordcom').AsString;
              dtmdados.IQuery1.close;
            end
            else
              SPCte.Parameters[33].value := Obs.text;
            SPCte.Parameters[34].value := edTxCte.value;
            SPCte.Parameters[35].value := edFreteM.value;
            SPCte.Parameters[36].value := edVlAjudante.value;
            // NFS
            SPCte.Parameters[37].value := edPedagioNF.value;
            SPCte.Parameters[38].value := edFluvialNF.value;
            SPCte.Parameters[39].value := edExcedNF.value;
            SPCte.Parameters[40].value := edTRNF.value;
            SPCte.Parameters[41].value := edDespachoNF.value;
            SPCte.Parameters[42].value := edTDENF.value;
            SPCte.Parameters[43].value := edTASNF.value;
            SPCte.Parameters[44].value := edSegNF.value;
            SPCte.Parameters[45].value := edGrisNF.value;
            SPCte.Parameters[46].value := edBalsaNF.value;
            SPCte.Parameters[47].value := edRedNF.value;
            SPCte.Parameters[48].value := edAgendNF.value;
            SPCte.Parameters[49].value := edPalletNF.value;
            SPCte.Parameters[50].value := edPortoNF.value;
            SPCte.Parameters[51].value := edAlfanNF.value;
            SPCte.Parameters[52].value := edCanhotoNF.value;
            SPCte.Parameters[53].value := edTaxaNF.value;
            SPCte.Parameters[54].value := edFretePesoNF.value + edFreteMNF.value;
            SPCte.Parameters[55].value := edAjudaNF.value;
            SPCte.Parameters[56].value := edVlAereo.value;
            SPCte.Parameters[57].value := edkm.value;
            SPCte.Parameters[58].value := edTDA.value;
            SPCte.Parameters[59].value := copy(QTranspTRANSP.value, Pos('|', QTranspTRANSP.value) + 1, 10);
            SPCte.ExecProc;
            man := SPCte.Parameters[0].value;
            if regman = '' then
              regman := IntToStr(man)
            else
              regman := regman + ' - ' + IntToStr(man);
          end;
          mdTarefa.next;
        end;
        mdTarefa.close;
        QTarefa.close;
        GeraCTe := '';
        // limpa o campo rateio da tb_coleta
        if cbRateio.Checked = true then
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
          dtmdados.RQuery1.SQL.Add('where rateio = :0');
          dtmdados.RQuery1.Parameters[0].value := GLBUSER;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end;
        Application.ProcessMessages;
        Screen.Cursor := crDefault;
        ShowMessage('CT-e N�(s) ' + regman + ' Gerado(s)');
        alterado := '';
        mdTemp.close;
        mdTemp.open;
      end
      else
      begin
        Screen.Cursor := crHourGlass;
        QTarefa.First;
        while not QTarefa.eof do
        begin
          // QTarefa.locate('fl_cte',mdTarefafl_cte.value,[]);
          if edTotal.value > 0 then
          begin
            SPCte.Parameters[1].value := QTarefaFL_CTE.AsInteger;
            SPCte.Parameters[2].value := RemoveChar(cbOperacao.text);
            SPCte.Parameters[3].value := v_codmun;
            SPCte.Parameters[4].value := v_codmunD;
            SPCte.Parameters[5].value := GLBUSER;
            SPCte.Parameters[6].value := RemoveChar(cbMotorista.text);
            SPCte.Parameters[7].value := cbplaca.text;
            SPCte.Parameters[8].value := edFretePeso.value;
            SPCte.Parameters[9].value := edPedagio.value;
            SPCte.Parameters[10].value := edOutros.value;
            SPCte.Parameters[11].value := edfluvial.value;
            SPCte.Parameters[12].value := edExc.value;
            SPCte.Parameters[13].value := edTR.value;
            SPCte.Parameters[14].value := eddespacho.value;
            SPCte.Parameters[15].value := edTDE.value;
            SPCte.Parameters[16].value := edtas.value;
            SPCte.Parameters[17].value := EdSeg.value;
            SPCte.Parameters[18].value := EdGris.value;
            SPCte.Parameters[19].value := edseg_balsa.value;
            SPCte.Parameters[20].value := edred_fluv.value;
            SPCte.Parameters[21].value := edagend.value;
            SPCte.Parameters[22].value := edpallet.value;
            SPCte.Parameters[23].value := edentrega.value;
            SPCte.Parameters[24].value := edalfand.value;
            SPCte.Parameters[25].value := edcanhoto.value;
            SPCte.Parameters[26].value := edBase.value;
            SPCte.Parameters[27].value := edAliq.value;
            SPCte.Parameters[28].value := edImposto.value;
            SPCte.Parameters[29].value := edTotal.value;
            SPCte.Parameters[30].value := edCFOP.text;
            SPCte.Parameters[31].value := StrToInt(edTabela.text);
            SPCte.Parameters[32].value := GLBFilial;
            // Projeto 88 incluir a Order na Obaserva��o
            if QTarefaORDER_CLI.AsString = 'S' then
            begin
              dtmdados.IQuery1.close;
              dtmdados.IQuery1.SQL.clear;
              dtmdados.IQuery1.SQL.Add
                ('select distinct i.ordcom from cyber.rodioc i where i.codocs = :1 and i.filocs = :2 ');
              dtmdados.IQuery1.Parameters[0].value := QTarefaNRO_OCS.AsInteger;
              dtmdados.IQuery1.Parameters[1].value := QTarefaFILIAL.AsInteger;
              dtmdados.IQuery1.open;
              SPCte.Parameters[33].value := Obs.text + 'Order n� : ' + dtmdados.IQuery1.FieldByName('ordcom').AsString;
              dtmdados.IQuery1.close;
            end
            else
              SPCte.Parameters[33].value := Obs.text;
            SPCte.Parameters[34].value := edTxCte.value;
            SPCte.Parameters[35].value := edFreteM.value;
            SPCte.Parameters[36].value := edAjudante.value;
            // NFS
            SPCte.Parameters[37].value := edPedagioNF.value;
            SPCte.Parameters[38].value := edFluvialNF.value;
            SPCte.Parameters[39].value := edExcedNF.value;
            SPCte.Parameters[40].value := edTRNF.value;
            SPCte.Parameters[41].value := edDespachoNF.value;
            SPCte.Parameters[42].value := edTDENF.value;
            SPCte.Parameters[43].value := edTASNF.value;
            SPCte.Parameters[44].value := edSegNF.value;
            SPCte.Parameters[45].value := edGrisNF.value;
            SPCte.Parameters[46].value := edBalsaNF.value;
            SPCte.Parameters[47].value := edRedNF.value;
            SPCte.Parameters[48].value := edAgendNF.value;
            SPCte.Parameters[49].value := edPalletNF.value;
            SPCte.Parameters[50].value := edPortoNF.value;
            SPCte.Parameters[51].value := edAlfanNF.value;
            SPCte.Parameters[52].value := edCanhotoNF.value;
            SPCte.Parameters[53].value := edTaxaNF.value;
            SPCte.Parameters[54].value := edFretePesoNF.value + edFreteMNF.value;
            SPCte.Parameters[55].value := edAjudaNF.value;
            SPCte.Parameters[56].value := edVlAereo.value;
            SPCte.Parameters[57].value := edkm.value;
            SPCte.Parameters[58].value := edTDA.value;
            SPCte.Parameters[59].value := copy(QTranspTRANSP.value, Pos('|', QTranspTRANSP.value) + 1, 10);
            SPCte.ExecProc;
            man := SPCte.Parameters[0].value;
            if regman = '' then
              regman := IntToStr(man)
            else
              regman := regman + ' - ' + IntToStr(man);
          end;
          QTarefa.next;
        end;
        mdTarefa.close;
        QTarefa.close;
        GeraCTe := '';
        // limpa o campo rateio da tb_coleta
        if cbRateio.Checked = true then
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
          dtmdados.RQuery1.SQL.Add('where rateio = :0');
          dtmdados.RQuery1.Parameters[0].value := GLBUSER;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end;
        Application.ProcessMessages;
        Screen.Cursor := crDefault;
        ShowMessage('CT-e N�(s) ' + regman + ' Gerado(s)');
        alterado := '';
        mdTemp.close;
        mdTemp.open;
      end;
      PageControl1.ActivePageIndex := 1;
    end;
    Limpavalores;
    pnManifesto.Enabled := false;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmGeraCTe.btnLimpar_loteClick(Sender: TObject);
begin
  btnLimpar_lote.Enabled := true;
  btnStatus.Enabled := true;
  edCteIni.value := 0;
  edCteFim.value := 0;
  QGerados.close;
  QGerados.SQL[10] := '';
  QGerados.open;
end;

procedure TfrmGeraCTe.CarregaBtn;
var
  starefa, nm: string;
  i: Integer;
begin
  btnCarregar_Tarefas.Enabled := false;
  self.Cursor := crHourGlass;
  starefa := '';
  starefa := RetornaTarefasSelecionadas();

  if (starefa = '') then
    starefa := QuotedStr('0');
  self.tag := 1;
  QTarefa.close;
  QTarefa.SQL[5] := 'where sequencia in (' + starefa + ')';
  QTarefa.open;

  i := 1;

  mdTemp.First;
  QTarefa.DisableControls;
  QTarefa.First;

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select par_3 from tb_parametro where codcli = :');
  dtmdados.IQuery1.Parameters[0].value := QTarefaPAGADOR.AsInteger;
  dtmdados.IQuery1.open;

  if (dtmdados.IQuery1.FieldByName('par_3').value = 'S') then
  begin
    cbOS.Checked := false;
    cbOS.Enabled := false;
  end
  else
    cbOS.Enabled := true;

  // Projeto 88 -> Se o destinat�rio for por Order ou por NF
  if (QTarefaORDER_CLI.AsString = 'S') or (QTarefaNF.AsString = 'S') then
  begin
    cbOS.Checked := false;
    cbOS.Enabled := false;
  end
  else
    cbOS.Enabled := true;

  dtmdados.IQuery1.close;

  nm := QTarefaCLIENTE.AsString + QTarefaREMETENTE.AsString +
    QTarefaDESTINATARIO.AsString + QTarefaRAZSOC.AsString;
  if not QTarefa.eof then
  begin
    while not QTarefa.eof do
    begin
      // agrupa OS e clientes e destinat�rios
      if (cbOS.Checked = true) then
      begin
        if (QTarefaCLIENTE.AsString + QTarefaREMETENTE.AsString +
          QTarefaDESTINATARIO.AsString + QTarefaRAZSOC.AsString) = nm then
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add
            ('update tb_coleta set fl_cte = :0 where sequencia = :1');
          dtmdados.RQuery1.Parameters[0].value := i;
          dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end
        else
        begin
          i := i + 1;
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add
            ('update tb_coleta set fl_cte = :0 where sequencia = :1');
          dtmdados.RQuery1.Parameters[0].value := i;
          dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
          nm := QTarefaCLIENTE.AsString + QTarefaREMETENTE.AsString +
            QTarefaDESTINATARIO.AsString + QTarefaRAZSOC.AsString;
        end;
      end
      else
      // Cada OS uma linha = 1 CT-e
      begin
        i := i + 1;
        dtmdados.RQuery1.close;
        dtmdados.RQuery1.SQL.clear;
        dtmdados.RQuery1.SQL.Add
          ('update tb_coleta set fl_cte = :0 where sequencia = :1');
        dtmdados.RQuery1.Parameters[0].value := i;
        dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
        dtmdados.RQuery1.ExecSQL;
        dtmdados.RQuery1.close;
      end;
      QTarefa.next;
    end;
  end;
  QTarefa.close;
  QTarefa.SQL[5] := 'where sequencia in (' + starefa + ')';
  QTarefa.open;
  QTarefa.First;
  QTarefa.EnableControls;
  self.tag := 0;
  self.Cursor := crDefault;
  btnCarregar_Tarefas.Enabled := true;
  btnNovoCTe.Enabled := true;
end;

procedure TfrmGeraCTe.CarregaCor;
begin

  if edFreteM.value > 0 then
    edFreteM.Color := $00A9FEFA
  else
    edFreteM.Color := clWindow;
  if edPedagio.value > 0 then
    edPedagio.Color := $00A9FEFA
  else
    edPedagio.Color := clWindow;
  if edOutros.value > 0 then
    edOutros.Color := $00A9FEFA
  else
    edOutros.Color := clWindow;
  if edExc.value > 0 then
    edExc.Color := $00A9FEFA
  else
    edExc.Color := clWindow;
  if EdSeg.value > 0 then
    EdSeg.Color := $00A9FEFA
  else
    EdSeg.Color := clWindow;
  if EdGris.value > 0 then
    EdGris.Color := $00A9FEFA
  else
    EdGris.Color := clWindow;
  if eddespacho.value > 0 then
    eddespacho.Color := $00A9FEFA
  else
    eddespacho.Color := clWindow;
  if edTDE.value > 0 then
    edTDE.Color := $00A9FEFA
  else
    edTDE.Color := clWindow;
  if edTR.value > 0 then
    edTR.Color := $00A9FEFA
  else
    edTR.Color := clWindow;
  if edfluvial.value > 0 then
    edfluvial.Color := $00A9FEFA
  else
    edfluvial.Color := clWindow;
  if edseg_balsa.value > 0 then
    edseg_balsa.Color := $00A9FEFA
  else
    edseg_balsa.Color := clWindow;
  if edred_fluv.value > 0 then
    edred_fluv.Color := $00A9FEFA
  else
    edred_fluv.Color := clWindow;
  if edagend.value > 0 then
    edagend.Color := $00A9FEFA
  else
    edagend.Color := clWindow;
  if edpallet.value > 0 then
    edpallet.Color := $00A9FEFA
  else
    edpallet.Color := clWindow;
  if edtas.value > 0 then
    edtas.Color := $00A9FEFA
  else
    edtas.Color := clWindow;
  if edentrega.value > 0 then
    edentrega.Color := $00A9FEFA
  else
    edentrega.Color := clWindow;
  if edalfand.value > 0 then
    edalfand.Color := $00A9FEFA
  else
    edalfand.Color := clWindow;
  if edcanhoto.value > 0 then
    edcanhoto.Color := $00A9FEFA
  else
    edcanhoto.Color := clWindow;
  if edBase.value > 0 then
    edBase.Color := $00A9FEFA
  else
    edBase.Color := clWindow;
  if edAliq.value > 0 then
    edAliq.Color := $00A9FEFA
  else
    edAliq.Color := clWindow;
  if edImposto.value > 0 then
    edImposto.Color := $00A9FEFA
  else
    edImposto.Color := clWindow;
  if edTotal.value > 0 then
    edTotal.Color := $00A9FEFA
  else
    edTotal.Color := clWindow;
  if edTxCte.value > 0 then
    edTxCte.Color := $00A9FEFA
  else
    edTxCte.Color := clWindow;
  if edFretePeso.value > 0 then
    edFretePeso.Color := $00A9FEFA
  else
    edFretePeso.Color := clWindow;
  if edVlAereo.value > 0 then
    edVlAereo.Color := $00A9FEFA
  else
    edVlAereo.Color := clWindow;

  if edTDA.value > 0 then
    edTDA.Color := $00A9FEFA
  else
    edTDA.Color := clWindow;
  // NFS
  if edFreteMNF.value > 0 then
    edFreteMNF.Enabled := false
  else
    edFreteMNF.Enabled := true;
  if edPedagioNF.value > 0 then
    edPedagioNF.Enabled := false
  else
    edPedagioNF.Enabled := true;

  if edExcedNF.value > 0 then
    edExcedNF.Enabled := false
  else
    edExcedNF.Enabled := true;
  if edSegNF.value > 0 then
    edSegNF.Enabled := false
  else
    edSegNF.Enabled := true;
  if edGrisNF.value > 0 then
    edGrisNF.Enabled := false
  else
    edGrisNF.Enabled := true;
  if edDespachoNF.value > 0 then
    edDespachoNF.Enabled := false
  else
    edDespachoNF.Enabled := true;
  if edTDENF.value > 0 then
    edTDENF.Enabled := false
  else
    edTDENF.Enabled := true;
  if edTRNF.value > 0 then
    edTRNF.Enabled := false
  else
    edTRNF.Enabled := true;
  if edFluvialNF.value > 0 then
    edFluvialNF.Enabled := false
  else
    edFluvialNF.Enabled := true;
  if edBalsaNF.value > 0 then
    edBalsaNF.Enabled := false
  else
    edBalsaNF.Enabled := true;
  if edRedNF.value > 0 then
    edRedNF.Enabled := false
  else
    edRedNF.Enabled := true;
  if edAgendNF.value > 0 then
    edAgendNF.Enabled := false
  else
    edAgendNF.Enabled := true;
  if edPalletNF.value > 0 then
    edPalletNF.Enabled := false
  else
    edPalletNF.Enabled := true;
  if edTASNF.value > 0 then
    edTASNF.Enabled := false
  else
    edTASNF.Enabled := true;

  if edPortoNF.value > 0 then
    edPortoNF.Enabled := false
  else
    edPortoNF.Enabled := true;

  if edAlfanNF.value > 0 then
    edAlfanNF.Enabled := false
  else
    edAlfanNF.Enabled := true;
  if edCanhotoNF.value > 0 then
    edCanhotoNF.Enabled := false
  else
    edCanhotoNF.Enabled := true;

  if edTTNF.value > 0 then
    edTTNF.Enabled := false
  else
    edTTNF.Enabled := true;
  if edTaxaNF.value > 0 then
    edTaxaNF.Enabled := false
  else
    edTaxaNF.Enabled := true;
  if edFretePesoNF.value > 0 then
    edFretePesoNF.Enabled := false
  else
    edFretePesoNF.Enabled := true;

end;

procedure TfrmGeraCTe.CarregaRomaneio();
begin
  CarregaTarefas();
  mdTemp.close;
  mdTemp.open;
  // carrega em temp tarefas n�o romaneadas
  QDestino.First;
  while not QDestino.eof do
  begin
    mdTemp.append;
    mdTempInserida.value := 0;
    mdTempOCS.value := QDestinoNRO_OCS.AsInteger;
    mdTempdestino.value := QDestinoRAZSOC.value;
    mdTempfilial.value := QDestinoFILIAL.AsInteger;
    mdTemptipo.value := QDestinoTIPO.value;
    mdTemppagador.value := QDestinoPAGADOR.AsInteger;
    mdTempcod_destino.value := QDestinoDESTINATARIO.AsInteger;
    mdTempReg.value := QDestinoSEQUENCIA.AsInteger;
    mdTemp.post;
    QDestino.next;
  end;
  QDestino.close;
  CarregaBtn();
  mdTemp.First;
end;

procedure TfrmGeraCTe.CarregaTarefas();
begin
  if (ckRota.Checked = false) then
  begin
    QDestino.close;
    QDestino.SQL[4] := 'and filial in(' + IntToStr(GLBFilial) + ')';
    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTempOCS.value := QDestinoNRO_OCS.AsInteger;
      mdTempdestino.value := QDestinoRAZSOC.value;
      mdTempfilial.value := QDestinoFILIAL.AsInteger;
      mdTemptipo.value := QDestinoTIPO.value;
      mdTemppagador.value := QDestinoPAGADOR.AsInteger;
      mdTempcod_destino.value := QDestinoDESTINATARIO.AsInteger;
      mdTempReg.value := QDestinoSEQUENCIA.AsInteger;
      QDestino.next;
    end;
    mdTemp.First;
  end
  else
  begin
    QDestino.close;
    QDestino.SQL[4] := 'and filial in(' + IntToStr(GLBFilial) + ')';
    if (qrRotaROTA.AsString = 'SEM ROTA') then
    begin
      QDestino.SQL[5] := ' and nvl(d.codsetcl,0) = 0 ';
    end
    else
    begin
      QDestino.SQL[5] := ' and codsetcl = :1 ';
      QDestino.Parameters[0].value := qrRotaCODSETCL.AsString;
    end;

    QDestino.open;
    QDestino.First;
    mdTemp.close;
    mdTemp.open;
    while not QDestino.eof do
    begin
      mdTemp.append;
      mdTempOCS.value := QDestinoNRO_OCS.AsInteger;
      mdTempdestino.value := QDestinoRAZSOC.value;
      mdTempfilial.value := QDestinoFILIAL.AsInteger;
      mdTemptipo.value := QDestinoTIPO.value;
      mdTemppagador.value := QDestinoPAGADOR.AsInteger;
      mdTempcod_destino.value := QDestinoDESTINATARIO.AsInteger;
      mdTempReg.value := QDestinoSEQUENCIA.AsInteger;
      QDestino.next;
    end;
    mdTemp.First;
  end;
end;

procedure TfrmGeraCTe.cbOperacaoChange(Sender: TObject);
var
  rateio, cod: String;
begin
  if cbOperacao.text <> '' then
  begin
    //Projeto 88
    // Verificar se os destinat�rios s�o de a�reo
    cod := alltrim(Copy(cbOperacao.text, Pos('-', cbOperacao.text) + 1, 30));
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.Add('select fl_aereo, fl_km from tb_operacao where operacao = ' +
              QuotedStr(cod));
    dtmdados.IQuery1.open;
    if dtmdados.IQuery1.FieldByName('fl_aereo').value = 'S' then
    begin
      QTarefa.DisableControls;
      QTarefa.First;
      while not QTarefa.eof do
      begin
        if QTarefaAEREO.AsString = 'N' then
        begin
          ShowMessage('Este Destinat�rio somente opera��o A�reo');
          cbOperacao.SetFocus;
          exit;
        end;
        QTarefa.Next;
      end;
      if dtmdados.IQuery1.FieldByName('fl_km').value = 'S' then
      begin
        edKm.Visible := true;
        label71.Visible := true;
        edKm.value := 0;
        calculakm;
        if edkm.Value = 0 then
        begin
          ShowMessage('N�o encontrado o KM percorrido, favor verificar com a mesa frete o cadastro');
          exit;
        end;
      end
      else
      begin
        edKm.Visible := false;
        label71.Visible := false;
      end;
    end
    else
    begin
      edKm.Visible := false;
      label71.Visible := false;
    end;
    dtmdados.IQuery1.close;

    if cbRateio.Checked = true then
    begin
      QTarefa.DisableControls;
      QTarefa.First;
      while not QTarefa.eof do
      begin
        if cod = 'DISTRIBUI��O KM' then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select nvl(codcep,'''') codcep from cyber.rodcli a where codclifor = ' +
            QuotedStr(mdTempcod_destino.AsString));
          dtmdados.IQuery1.open;
          if dtmdados.IQuery1.FieldByName('codcep').value <> '' then
          begin
            try
              ACBrCEP1.ChaveAcesso := frmMenu.ACBrCte1.Configuracoes.Certificados.
                NumeroSerie;
              ACBrCEP1.BuscarPorCEP(dtmdados.IQuery1.FieldByName('codcep').value);

              if ACBrCEP1.Enderecos.Count < 1 then
              begin
                ShowMessage
                  ('Nenhum Endere�o encontrado no CEP deste Destino, favor corrigir teremos problemas no pagamento');
                // exit;
              end;
            except
              On E: Exception do
              begin
                ShowMessage(E.Message);
              end;
            end;
          end;
        end;
        dtmdados.RQuery1.close;
        dtmdados.RQuery1.SQL.clear;
        dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = :0 ');
        dtmdados.RQuery1.SQL.Add('where sequencia = :1');
        dtmdados.RQuery1.Parameters[0].value := GLBUSER;
        dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
        dtmdados.RQuery1.ExecSQL;
        dtmdados.RQuery1.close;
        QTarefa.next;
      end;
      QTarefa.EnableControls;
    end
    else
    begin
      dtmdados.RQuery1.close;
      dtmdados.RQuery1.SQL.clear;
      dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
      dtmdados.RQuery1.SQL.Add('where sequencia = :0');
      dtmdados.RQuery1.Parameters[0].value := QTarefaSEQUENCIA.AsInteger;
      dtmdados.RQuery1.ExecSQL;
      dtmdados.RQuery1.close;
    end;
    SPCalculo.Parameters[51].value := QTarefaFL_CTE.value;
    cod := Copy(cbOperacao.text, 1, Pos('-', cbOperacao.text) - 1);
    SPCalculo.Parameters[52].value := cod;
    SPCalculo.Parameters[53].value := v_codmun;
    SPCalculo.Parameters[54].value := v_codmunD;
    SPCalculo.Parameters[55].value := GLBUSER;
    SPCalculo.Parameters[56].value := edVeiculo.text;
    SPCalculo.Parameters[57].value := edQtPallet.value;
    if cbRateio.Checked = true then
      SPCalculo.Parameters[58].value := 'S'
    else
      SPCalculo.Parameters[58].value := 'N';
    SPCalculo.Parameters[59].value := edAjudante.value;
    SPCalculo.Parameters[60].value := QTarefaNRO_OCS.value;
    SPCalculo.Parameters[61].value := edKm.value;
    SPCalculo.ExecProc;

    edFreteM.value := SPCalculo.Parameters[0].value;
    edPedagio.value := SPCalculo.Parameters[1].value;
    edOutros.value := SPCalculo.Parameters[2].value;
    edfluvial.value := SPCalculo.Parameters[3].value;
    edExc.value := SPCalculo.Parameters[4].value;
    edTR.value := SPCalculo.Parameters[5].value;
    eddespacho.value := SPCalculo.Parameters[6].value;
    edTDE.value := SPCalculo.Parameters[7].value;
    edtas.value := SPCalculo.Parameters[8].value;
    EdSeg.value := SPCalculo.Parameters[9].value;
    EdGris.value := SPCalculo.Parameters[10].value;
    edseg_balsa.value := SPCalculo.Parameters[11].value;
    edred_fluv.value := SPCalculo.Parameters[12].value;
    edagend.value := SPCalculo.Parameters[13].value;
    edpallet.value := SPCalculo.Parameters[14].value;
    edentrega.value := SPCalculo.Parameters[15].value;
    edalfand.value := SPCalculo.Parameters[16].value;
    edcanhoto.value := SPCalculo.Parameters[17].value;
    edBase.value := SPCalculo.Parameters[18].value;
    edAliq.value := SPCalculo.Parameters[19].value;
    edImposto.value := SPCalculo.Parameters[20].value;
    edTotal.value := SPCalculo.Parameters[21].value;
    edCFOP.text := SPCalculo.Parameters[22].value;
    edTabela.text := IntToStr(SPCalculo.Parameters[23].value);
    edFretePeso.value := SPCalculo.Parameters[24].value;
    edTxCte.value := SPCalculo.Parameters[25].value;
    rateio := SPCalculo.Parameters[26].value;
    edVlAjudante.value := SPCalculo.Parameters[27].value;
    edVlAereo.value := SPCalculo.Parameters[28].value;
    edTDA.Value := SPCalculo.Parameters[50].value;

    // NFS
    edFreteMNF.value := SPCalculo.Parameters[29].value;
    edPedagioNF.value := SPCalculo.Parameters[30].value;
    edFluvialNF.value := SPCalculo.Parameters[31].value;
    edExcedNF.value := SPCalculo.Parameters[32].value;
    edTRNF.value := SPCalculo.Parameters[33].value;
    edDespachoNF.value := SPCalculo.Parameters[34].value;
    edTDENF.value := SPCalculo.Parameters[35].value;
    edTASNF.value := SPCalculo.Parameters[36].value;
    edSegNF.value := SPCalculo.Parameters[37].value;
    edGrisNF.value := SPCalculo.Parameters[38].value;
    edBalsaNF.value := SPCalculo.Parameters[39].value;
    edRedNF.value := SPCalculo.Parameters[40].value;
    edAgendNF.value := SPCalculo.Parameters[41].value;
    edPalletNF.value := SPCalculo.Parameters[42].value;
    edPortoNF.value := SPCalculo.Parameters[43].value;
    edAlfanNF.value := SPCalculo.Parameters[44].value;
    edCanhotoNF.value := SPCalculo.Parameters[45].value;
    edFretePesoNF.value := SPCalculo.Parameters[46].value;
    edTaxaNF.value := SPCalculo.Parameters[47].value;
    edAjudaNF.value := SPCalculo.Parameters[48].value;
    edTTNF.value := SPCalculo.Parameters[49].value;

    if (cbRateio.Checked = false) and (rateio = 'S') then
      ShowMessage('Esta Tabela Tem Rateio, mas n�o foi escolhido !!');

    if (edAjudante.value > 0) and (edVlAjudante.value = 0) then
      ShowMessage
        ('Esta Tabela N�o Tem Valor de Ajudante ou N�o est� marcado no CRM !!');

    CarregaCor;

    if edTotal.value > 0 then
      btnGerar.Enabled := true;
  end;
end;

procedure TfrmGeraCTe.cbPesquisaChange(Sender: TObject);
begin
  cbFiltro.Checked := false;
  mdTemp.Filtered := false;
  if cbPesquisa.ItemIndex = 1 then
    dbCliente.Visible := true
  else
    dbCliente.Visible := false;
end;

procedure TfrmGeraCTe.cbplacaChange(Sender: TObject);
var
  rateio, cod: String;
begin
  btnGerar.Enabled := false;

  if v_codmunD = 0 then
  begin
    ShowMessage('N�o foi escolhido o Destino');
    CBUF.SetFocus;
  end;

  if cbplaca.text <> '' then
  begin
    if copy(cbTrans.text, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.locate('placa', cbplaca.text, []);
      edVeiculo.text := QVeiculoCasaDESCRI.value;
    end
    else
    begin
      QVeiculo.locate('placa', cbplaca.text, []);
      edVeiculo.text := QVeiculoDESCRI.value;
    end;

    if cbRateio.Checked = true then
    begin
      dtmdados.RQuery1.close;
      dtmdados.RQuery1.SQL.clear;
      dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = :0 ');
      dtmdados.RQuery1.SQL.Add('where sequencia = :1');
      dtmdados.RQuery1.Parameters[0].value := GLBUSER;
      dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
      dtmdados.RQuery1.ExecSQL;
      dtmdados.RQuery1.close;
    end
    else
    begin
      dtmdados.RQuery1.close;
      dtmdados.RQuery1.SQL.clear;
      dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
      dtmdados.RQuery1.SQL.Add('where sequencia = :0');
      dtmdados.RQuery1.Parameters[0].value := QTarefaSEQUENCIA.AsInteger;
      dtmdados.RQuery1.ExecSQL;
      dtmdados.RQuery1.close;
    end;

    SPCalculo.Parameters[51].value := QTarefaFL_CTE.value;
    cod := Copy(cbOperacao.text, 1, Pos('-', cbOperacao.text) - 1);
    SPCalculo.Parameters[52].value := cod;
    SPCalculo.Parameters[53].value := v_codmun;
    SPCalculo.Parameters[54].value := v_codmunD;
    SPCalculo.Parameters[55].value := GLBUSER;
    SPCalculo.Parameters[56].value := edVeiculo.text;
    SPCalculo.Parameters[57].value := edQtPallet.value;
    if cbRateio.Checked = true then
      SPCalculo.Parameters[58].value := 'S'
    else
      SPCalculo.Parameters[58].value := 'N';
    SPCalculo.Parameters[59].value := edAjudante.value;
    SPCalculo.Parameters[60].value := QTarefaNRO_OCS.value;
    SPCalculo.Parameters[61].value := edKm.value;
    SPCalculo.ExecProc;

    edFreteM.value := SPCalculo.Parameters[0].value;
    edPedagio.value := SPCalculo.Parameters[1].value;
    edOutros.value := SPCalculo.Parameters[2].value;
    edfluvial.value := SPCalculo.Parameters[3].value;
    edExc.value := SPCalculo.Parameters[4].value;
    edTR.value := SPCalculo.Parameters[5].value;
    eddespacho.value := SPCalculo.Parameters[6].value;
    edTDE.value := SPCalculo.Parameters[7].value;
    edtas.value := SPCalculo.Parameters[8].value;
    EdSeg.value := SPCalculo.Parameters[9].value;
    EdGris.value := SPCalculo.Parameters[10].value;
    edseg_balsa.value := SPCalculo.Parameters[11].value;
    edred_fluv.value := SPCalculo.Parameters[12].value;
    edagend.value := SPCalculo.Parameters[13].value;
    edpallet.value := SPCalculo.Parameters[14].value;
    edentrega.value := SPCalculo.Parameters[15].value;
    edalfand.value := SPCalculo.Parameters[16].value;
    edcanhoto.value := SPCalculo.Parameters[17].value;
    edBase.value := SPCalculo.Parameters[18].value;
    edAliq.value := SPCalculo.Parameters[19].value;
    edImposto.value := SPCalculo.Parameters[20].value;
    edTotal.value := SPCalculo.Parameters[21].value;
    edCFOP.text := SPCalculo.Parameters[22].value;
    edTabela.text := IntToStr(SPCalculo.Parameters[23].value);
    edFretePeso.value := SPCalculo.Parameters[24].value;
    edTxCte.value := SPCalculo.Parameters[25].value;
    rateio := SPCalculo.Parameters[26].value;
    edVlAjudante.value := SPCalculo.Parameters[27].value;
    edVlAereo.value := SPCalculo.Parameters[28].value;
    edTDA.Value :=  SPCalculo.Parameters[50].value;

    // NFS
    edFreteMNF.value := SPCalculo.Parameters[29].value;
    edPedagioNF.value := SPCalculo.Parameters[30].value;
    edFluvialNF.value := SPCalculo.Parameters[31].value;
    edExcedNF.value := SPCalculo.Parameters[32].value;
    edTRNF.value := SPCalculo.Parameters[33].value;
    edDespachoNF.value := SPCalculo.Parameters[34].value;
    edTDENF.value := SPCalculo.Parameters[35].value;
    edTASNF.value := SPCalculo.Parameters[36].value;
    edSegNF.value := SPCalculo.Parameters[37].value;
    edGrisNF.value := SPCalculo.Parameters[38].value;
    edBalsaNF.value := SPCalculo.Parameters[39].value;
    edRedNF.value := SPCalculo.Parameters[40].value;
    edAgendNF.value := SPCalculo.Parameters[41].value;
    edPalletNF.value := SPCalculo.Parameters[42].value;
    edPortoNF.value := SPCalculo.Parameters[43].value;
    edAlfanNF.value := SPCalculo.Parameters[44].value;
    edCanhotoNF.value := SPCalculo.Parameters[45].value;
    edFretePesoNF.value := SPCalculo.Parameters[46].value;
    edTaxaNF.value := SPCalculo.Parameters[47].value;
    edAjudaNF.value := SPCalculo.Parameters[48].value;
    edTTNF.value := SPCalculo.Parameters[49].value;

    if (cbRateio.Checked = false) and (rateio = 'S') then
      ShowMessage('Esta Tabela Tem Rateio, mas n�o foi escolhido !!');

    if (edAjudante.value > 0) and (edVlAjudante.value = 0) then
      ShowMessage
        ('Esta Tabela N�o Tem Valor de Ajudante ou N�o est� marcado no CRM !!');

    CarregaCor;

    if edTotal.value > 0 then
      btnGerar.Enabled := true;
  end;
end;

procedure TfrmGeraCTe.cbTransExit(Sender: TObject);
begin
  QTransp.open;
  QTransp.locate('transp', cbTrans.text, []);
  QMotor.close;
  QMotor.Parameters[0].value := copy(QTranspTRANSP.value,
    Pos('|', QTranspTRANSP.value) + 1, 10);
  QMotor.open;
  cbMotorista.clear;
  while not QMotor.eof do
  begin
    cbMotorista.Items.Add(QMotorNOME.value);
    QMotor.next;
  end;
  QMotor.close;

  cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('IMPORTA��O');

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select nvl(fl_viagem,''N'') fl_viagem from tb_operacao where operacao = :0');
  dtmdados.IQuery1.Parameters[0].value := cbOperacao.text;
  dtmdados.IQuery1.open;

  if copy(QTranspTRANSP.value, 1, 7) = 'INTECOM' then
  begin
    QVeiculoCasa.close;
    QVeiculoCasa.open;
    cbplaca.clear;
    cbplaca.Items.Add('');
    while not QVeiculoCasa.eof do
    begin
      cbplaca.Items.Add(QVeiculoCasaPLACA.value);
      QVeiculoCasa.next;
    end;
    // marca que n�o controla custo
    contmarg := 'N';
  end
  else
  begin
    QVeiculo.close;
    if dtmdados.IQuery1.FieldByName('fl_viagem').value = 'N' then
    begin
      QVeiculo.SQL[3] := '';
      QVeiculo.SQL[12] := '';
    end;
    dtmdados.IQuery1.close;
    QVeiculo.Parameters[0].value := copy(QTranspTRANSP.value,
      Pos('|', QTranspTRANSP.value) + 1, 10);
    QVeiculo.open;
    cbplaca.clear;
    cbplaca.Items.Add('');
    while not QVeiculo.eof do
    begin
      cbplaca.Items.Add(QVeiculoPLACA.value);
      QVeiculo.next;
    end;
  end;
end;

procedure TfrmGeraCTe.cbUFExit(Sender: TObject);
begin
  QCity.close;
  QCity.Parameters[0].value := CBUF.text;
  QCity.open;
  cbCidadeD.clear;
  cbCidadeD.Items.Add('');
  while not QCity.eof do
  begin
    cbCidadeD.Items.Add(QCityDESCRI.value);
    QCity.next;
  end;
  QCity.close;
end;

procedure TfrmGeraCTe.cbUFOExit(Sender: TObject);
begin
  QCity.close;
  QCity.Parameters[0].value := cbUFO.text;
  QCity.open;
  cbCidadeO.clear;
  cbCidadeO.Items.Add('');
  while not QCity.eof do
  begin
    cbCidadeO.Items.Add(QCityDESCRI.value);
    QCity.next;
  end;
  QCity.close;
end;

procedure TfrmGeraCTe.cbCidadedExit(Sender: TObject);
begin
  QCity.close;
  QCity.Parameters[0].value := CBUF.text;
  QCity.open;
  QCity.locate('descri', cbCidadeD.text, []);
  v_codmunD := QCityCODMUN.AsInteger;
  cbplaca.SetFocus;
end;

procedure TfrmGeraCTe.cbCidadeOExit(Sender: TObject);
begin
  QCidade.locate('descri', cbCidadeO.text, []);
  v_codmun := QCidadeCODMUN.AsInteger;
end;

procedure TfrmGeraCTe.cbFiltroClick(Sender: TObject);
var
  ini, fim: Integer;
begin
  pgTarefas.ActivePageIndex := 1;

  if cbFiltro.Checked = true then
    mdTemp.Filtered := true
  else
    mdTemp.Filtered := false;
  mdTemp.First;
  ini := mdTemp.RecNo - 1;
  mdTemp.last;
  fim := mdTemp.RecNo;
  Label32.caption := IntToStr(fim - ini) + ' Registros';
  mdTemp.First;
end;

procedure TfrmGeraCTe.ckRotaClick(Sender: TObject);
begin
  if (ckRota.Checked = false) then
  begin
    // tbRota.TabVisible := false;
    pgTarefas.ActivePageIndex := 1;
  end
  else
  begin
    pgTarefas.ActivePageIndex := 0;
    // tbRota.TabVisible := True;
  end;
  // CarregaTarefas();
end;

procedure TfrmGeraCTe.dbgColetaCellClick(Column: TColumn);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select count(*) qt from cyber.rodioc a where codocs =:0 and filocs =:1 and a.notnfe is null');
  dtmdados.IQuery1.Parameters[0].value := mdTempOCS.AsInteger;
  dtmdados.IQuery1.Parameters[1].value := mdTempfilial.AsInteger;
  dtmdados.IQuery1.open;

  if dtmdados.IQuery1.FieldByName('qt').value > 0 then
  begin
    ShowMessage('Esta OCS Tem Nota Fiscal sem a Chave');
    Exit;
  end;

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempOCS.AsInteger;
  qrCalcula_Frete.Parameters[1].value := mdTemppagador.AsInteger;
  qrCalcula_Frete.open;
  //
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    // mdTempreg.value := reg;
    mdTempInserida.value := 1;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value +
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value + qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value +
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    // edTotFreteTarefa.Value  := edTotFreteTarefa.Value + qrCalcula_Frete.FieldByName('frete').AsFloat;
  end
  else
  begin
    mdTempInserida.value := 0;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value -
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value -
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    // edTotFreteTarefa.Value  := edTotFreteTarefa.Value - qrCalcula_Frete.FieldByName('frete').AsFloat;
  end;
  mdTemp.post;
end;

procedure TfrmGeraCTe.dbgColetaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgColeta.Canvas.FillRect(Rect);
      iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraCTe.dbgtripCellClick(Column: TColumn);
var qtd : Integer;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  Q_dados_trip.close;
  Q_dados_trip.Parameters[0].value := mdTempdestino.AsString;
  Q_dados_trip.open;
  qtd := 0;
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select count(box) qtd from (select distinct box from dashboard.skf_tb_trip tr where id_trip = :0)');
  dtmdados.IQuery1.Parameters[0].value := mdTempdestino.AsString;
  dtmdados.IQuery1.open;
  qtd := dtmdados.IQuery1.FieldByName('qtd').AsInteger;
  //
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    // mdTempreg.value := reg;
    mdTempInserida.value := 1;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value + Q_Dados_tripCUB.AsFloat;
    //  qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value + Q_Dados_tripPESO.AsFloat;
    //qrCalcula_Frete.FieldByName('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value + Q_Dados_tripVALOR.AsFloat;
    //  qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value + qtd
    // edTotFreteTarefa.Value  := edTotFreteTarefa.Value + qrCalcula_Frete.FieldByName('frete').AsFloat;
  end
  else
  begin
    mdTempInserida.value := 0;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value - Q_Dados_tripCUB.AsFloat;
    //  qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value - Q_Dados_tripPESO.AsFloat;
    //qrCalcula_Frete.FieldByName('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value - Q_Dados_tripVALOR.AsFloat;
    //  qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value - qtd;
   //   qrCalcula_Frete.FieldByName('qtd').AsFloat;
    // edTotFreteTarefa.Value  := edTotFreteTarefa.Value - qrCalcula_Frete.FieldByName('frete').AsFloat;
  end;
  mdTemp.post;
end;

procedure TfrmGeraCTe.dbgtripDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgTrip.Canvas.FillRect(Rect);
      iml.Draw(dbgtrip.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgtrip.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgtrip.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraCTe.dgCtrcDblClick(Sender: TObject);
begin
  if not dtmdados.PodeInserir('frmGeraCTe') then
    Exit;
  // if QGeradosCTE_STATUS.value > 100 then
  // begin
  try
    Application.CreateForm(TfrmAltCTE, frmAltCTE);
    frmAltCTE.QCTE.Parameters[0].value := QGeradosCOD_CONHECIMENTO.AsInteger;
    frmAltCTE.QCTE.open;
    frmAltCTE.ShowModal;
  finally
    frmAltCTE.Free;
  end;
  // end;
end;

procedure TfrmGeraCTe.dgTarefaDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if TestaParaPar(QTarefaFL_CTE.AsInteger) = true then
  begin
    dgTarefa.Canvas.Brush.Color := clYellow;
    cli := QTarefaCLIENTE.AsString;
    ori := QTarefaREMETENTE.AsString;
    des := QTarefaDESTINATARIO.AsString;
  end
  else
  begin
    dgTarefa.Canvas.Brush.Color := clMoneyGreen;
    cli := QTarefaCLIENTE.AsString;
    ori := QTarefaREMETENTE.AsString;
    des := QTarefaDESTINATARIO.AsString;
  end;
  dgTarefa.DefaultDrawDataCell(Rect, dgTarefa.columns[DataCol].Field, State);
end;

procedure TfrmGeraCTe.dstTarefaDataChange(Sender: TObject; Field: TField);
begin
  btnVoltaTarefa.Enabled := QTarefa.RecordCount > 0;
end;

procedure TfrmGeraCTe.edCteFimExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteFim.value > 0 then
  begin
    if edCteFim.value < edCteIni.value then
    begin
      ShowMessage('O CT-e Final n�o pode ser menor que o Inicial');
      edCteFim.SetFocus;
      Exit;
    end;
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteFim.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteFim.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe.edCteIniExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteIni.value > 0 then
  begin
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteIni.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteIni.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe.edDataChange(Sender: TObject);
begin
  CarregaTarefas;
end;

procedure TfrmGeraCTe.edValorExit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
  i: Integer;
  svalor: string;
begin
  pgTarefas.ActivePageIndex := 1;
  if (ALLTRIM(edValor.text) = '') then
    Exit;

  i := cbPesquisa.ItemIndex;
  svalor := edValor.text;
  if cbPesquisa.ItemIndex <> -1 then
  begin
    qrPesquisa := TADOQuery.Create(Nil);
    with qrPesquisa, SQL do
    begin
      Connection := dtmdados.ConIntecom;
      if (cbPesquisa.ItemIndex = 0) then // OCS
      begin
        Add('select nro_ocs  ');
        Add('from cyber.tb_coleta ');
        Add('where datainc > to_date(''01/09/2015'',''dd/mm/yyyy'') ');
        Add('and doc is null and tipo = ''OCS'' ');
        Add('and filial in(' + IntToStr(GLBFilial) + ')');
        Add('and nro_ocs = :0 ');
        Parameters[0].value := edValor.text;
      end
      else if (cbPesquisa.ItemIndex = 2) then // Filial
      begin
        Add('select nro_ocs  ');
        Add('from cyber.tb_coleta ');
        Add('where datainc > to_date(''01/09/2015'',''dd/mm/yyyy'') ');
        Add('and doc is null and tipo = ''OCS'' ');
        Add('and filial = :0');
        Add('and filial in(' + IntToStr(GLBFilial) + ')');
        Parameters[0].value := edValor.text;
      end
      else if (cbPesquisa.ItemIndex = 3) then // Nota Fiscal
      begin
        Add('select codocs nro_ocs  ');
        Add('from cyber.rodioc ');
        Add('where datnot > to_date(''01/09/2015'',''dd/mm/yyyy'') ');
        Add('and filocs = :0');
        Add('and notfis = (' + (QuotedStr(edValor.text)) + ')');
        Parameters[0].value := GLBFilial;
      end;
      open;
    end;
    if (qrPesquisa.RecordCount = 0) then
    begin
      if (cbPesquisa.ItemIndex = 0) then
        ShowMessage('OCS n�o Encontrada !!')
      else if (cbPesquisa.ItemIndex = 2) then
        ShowMessage('Filial n�o Encontrada !!')
      else if (cbPesquisa.ItemIndex = 3) then
        ShowMessage('Nota Fiscal n�o Encontrada !!');
      FreeAndNil(qrPesquisa);
      Exit;
    end
    else
    begin
      if not mdTemp.locate('ocs', qrPesquisa.FieldByName('nro_ocs').AsString, [])
      then
      begin
        if (cbPesquisa.ItemIndex = 0) then
          ShowMessage('OCS n�o Encontrada !!')
        else if (cbPesquisa.ItemIndex = 2) then
          ShowMessage('Filial n�o Encontrada !!');
      end
      else if (cbPesquisa.ItemIndex = 2) then
        mdTemp.SortOnFields('filial');
    end;
  end;
  cbPesquisa.ItemIndex := i;
  edValor.text := svalor;
end;

procedure TfrmGeraCTe.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self.tag := 1;
  if QTarefa.Active = true then
  begin
    QTarefa.First;
    if not QTarefa.eof then
    begin
      SP_TB_COLETA.Parameters[0].value := 0;
      SP_TB_COLETA.Parameters[1].value := GLBUSER;
      SP_TB_COLETA.ExecProc;
    end;
  end;
  self.tag := 0;
  QVeiculo.close;
  QDestino.close;
  QTarefa.close;
  mdTemp.close;
  QGerados.close;
  Action := caFree;
end;

procedure TfrmGeraCTe.FormCreate(Sender: TObject);
begin
  bInicio := true;
  tipo_transporte := '';

  // Limpa sujeita da tb_coleta preso no usuario
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.SQL.clear;
  dtmdados.RQuery1.SQL.Add
    ('update tb_coleta set user_cte = null, fl_cte = null ');
  dtmdados.RQuery1.SQL.Add('where doc is null and user_cte = :0');
  dtmdados.RQuery1.Parameters[0].value := GLBUSER;
  dtmdados.RQuery1.ExecSQL;
  dtmdados.RQuery1.close;

  QSite.close;
  QSite.Parameters[0].value := GLBUSER;
  QSite.open;

  qrRota.close;
  qrRota.SQL[6] := 'and a.filial in(' + IntToStr(GLBFilial) + ')';
  qrRota.SQL[15] := 'and a.filial in(' + IntToStr(GLBFilial) + ')';
  qrRota.open;
  qrRota.First;

  // Carrega as opera��es
  QOperacao.Close;
  QOperacao.Open;
  cbOperacao.clear;
  cbOperacao.Items.add('');
  while not QOperacao.eof do
  begin
    cbOperacao.Items.add
      (UpperCase(QOperacaoCOD.value));
    QOperacao.next;
  end;
  QOperacao.close;

  reg := 0;
  PageControl1.TabIndex := 0;

  QVeiculo.open;
  // eddata.date := date-30;
  QCliente.close;
  QCliente.SQL[4] := 'And a.filial in(' + IntToStr(GLBFilial) + ')';
  QCliente.open;

  cbPesquisa.clear;
  cbPesquisa.Items.Add('OCS');
  cbPesquisa.Items.Add('Cliente');
  cbPesquisa.Items.Add('Filial');
  cbPesquisa.Items.Add('Nota Fiscal');
  cbPesquisa.Items.Add('');

  //inibir aba trip
  if glbtrip = 'S' then
    tsTrip.TabVisible := true
  else
    tsTrip.TabVisible := false;
end;

procedure TfrmGeraCTe.Limpavalores;
begin
  edFreteM.value := 0;
  edPedagio.value := 0;
  edOutros.value := 0;
  edfluvial.value := 0;
  edExc.value := 0;
  edTR.value := 0;
  eddespacho.value := 0;
  edTDE.value := 0;
  edtas.value := 0;
  EdSeg.value := 0;
  EdGris.value := 0;
  edseg_balsa.value := 0;
  edred_fluv.value := 0;
  edagend.value := 0;
  edpallet.value := 0;
  edentrega.value := 0;
  edalfand.value := 0;
  edcanhoto.value := 0;
  edBase.value := 0;
  edAliq.value := 0;
  edImposto.value := 0;
  edTotal.value := 0;
  edCFOP.text := '';
  edTDA.Value := 0;
end;

procedure TfrmGeraCTe.mdTempAfterOpen(DataSet: TDataSet);
begin
  mdTemp.First;
end;

procedure TfrmGeraCTe.mdTempAfterPost(DataSet: TDataSet);
begin
  Label32.caption := IntToStr(mdTemp.RecordCount) + ' Registros';
end;

procedure TfrmGeraCTe.mdTempFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  // OCS
  // OST
  // Cliente
  // Filial
  if cbPesquisa.ItemIndex = 0 then
    Accept := mdTemp['tipo'] = 'OCS'
  else if cbPesquisa.ItemIndex = 2 then
    Accept := mdTemp['filial'] = ALLTRIM(edValor.text)
  else if cbPesquisa.ItemIndex = 1 then
    Accept := mdTemp['pagador'] = QClientePAGADOR.AsInteger;
end;

procedure TfrmGeraCTe.MStatKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    MStat.clear;
    MStat.Visible := false;
  end;
end;

procedure TfrmGeraCTe.escolhidotipo(tipo: String);
begin
  if Application.Messagebox('Este � o Tipo do CT-e que quer Gerar ?', 'Gerar CT-e',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Panel9.Visible := false;
    showmessage(tipo);
  end;
end;


procedure TfrmGeraCTe.Panel10Click(Sender: TObject);
begin
  escolhidotipo(panel10.Caption)
end;

procedure TfrmGeraCTe.Panel11Click(Sender: TObject);
begin
  escolhidotipo(panel11.Caption)
end;

procedure TfrmGeraCTe.Panel12Click(Sender: TObject);
begin
  escolhidotipo(panel12.Caption)
end;

procedure TfrmGeraCTe.QGeradosBeforeOpen(DataSet: TDataSet);
begin
  QGerados.Parameters[0].value := GLBFilial;
end;

procedure TfrmGeraCTe.QTarefaAfterScroll(DataSet: TDataSet);
var
  rateio: String;
begin
  QNF.close;
  QNF.Parameters[0].value := QTarefaNRO_OCS.AsInteger;
  QNF.Parameters[1].value := QTarefaFILIAL.AsInteger;
  QNF.open;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.SQL.clear;
  dtmdados.RQuery1.SQL.Add
    ('select nvl(sum(pesokg),0) p, nvl(sum(vlrmer),0) nf, nvl(sum(quanti),0) v, nvl(sum(pescub)*300,0) c ');
  dtmdados.RQuery1.SQL.Add
    ('from cyber.rodioc where codocs = :0 and filocs = :1 ');
  dtmdados.RQuery1.Parameters[0].value := QTarefaNRO_OCS.AsInteger;
  dtmdados.RQuery1.Parameters[1].value := QTarefaFILIAL.AsInteger;
  dtmdados.RQuery1.open;
  edVol.value := dtmdados.RQuery1.FieldByName('v').value;
  edPes.value := dtmdados.RQuery1.FieldByName('p').value;
  edCub.value := dtmdados.RQuery1.FieldByName('c').value;
  edNF.value := dtmdados.RQuery1.FieldByName('nf').value;
  dtmdados.RQuery1.close;

  if (self.tag = 0) then
  begin

    if GeraCTe = '' then
    begin
      if QTarefaCOD_MUN_DES.AsInteger > 0 then
      begin
        dtmdados.RQuery1.close;
        dtmdados.RQuery1.SQL.clear;
        dtmdados.RQuery1.SQL.Add('select c.descri cidade, c.estado ');
        dtmdados.RQuery1.SQL.Add('from rodmun c ');
        dtmdados.RQuery1.SQL.Add('where codmun = :0');
        dtmdados.RQuery1.Parameters[0].value := QTarefaCOD_MUN_DES.AsInteger;
        dtmdados.RQuery1.open;

        CBUF.ItemIndex := CBUF.Items.IndexOf
          (dtmdados.RQuery1.FieldByName('estado').value);

        QCidade.close;
        QCidade.Parameters[0].value := QTarefaCOD_MUN_DES.AsInteger;
        // dtmdados.RQuery1.FieldByName('estado').value;
        QCidade.open;
        cbCidadeD.clear;
        cbCidadeD.Items.Add('');
        while not QCidade.eof do
        begin
          cbCidadeD.Items.Add(QCidadeDESCRI.value);
          QCidade.next;
        end;
        cbCidadeD.ItemIndex := cbCidadeD.Items.IndexOf(QTarefaMUN_DES.AsString);
        dtmdados.RQuery1.close;
        v_codmunD := QTarefaCOD_MUN_DES.AsInteger;
      end;

      QCidade.close;
      QCidade.Parameters[0].value := QTarefaCOD_MUN_O.AsInteger;
      QCidade.open;

      cbUFO.ItemIndex := cbUFO.Items.IndexOf(QCidadeESTADO.value);

      cbCidadeO.clear;
      cbCidadeO.Items.Add('');
      while not QCidade.eof do
      begin
        cbCidadeO.Items.Add(QCidadeDESCRI.value);
        QCidade.next;
      end;
      cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(QCidadeDESCRI.value);
      QCidade.locate('descri', cbCidadeO.text, []);
      v_codmun := QCidadeCODMUN.AsInteger;
    end
    else
    begin
      dtmdados.RQuery1.close;
      dtmdados.RQuery1.SQL.clear;
      dtmdados.RQuery1.SQL.Add('select c.codmun ');
      dtmdados.RQuery1.SQL.Add('from rodmun c ');
      dtmdados.RQuery1.SQL.Add('where descri = :0 and estado = :1 ');
      dtmdados.RQuery1.Parameters[0].value := cbCidadeD.text;
      dtmdados.RQuery1.Parameters[1].value := CBUF.text;
      dtmdados.RQuery1.open;
      v_codmunD := dtmdados.RQuery1.FieldByName('codmun').value;
      dtmdados.RQuery1.close;

      dtmdados.RQuery1.SQL.clear;
      dtmdados.RQuery1.SQL.Add('select c.codmun ');
      dtmdados.RQuery1.SQL.Add('from rodmun c ');
      dtmdados.RQuery1.SQL.Add('where descri = :0 and estado = :1 ');
      dtmdados.RQuery1.Parameters[0].value := cbCidadeO.text;
      dtmdados.RQuery1.Parameters[1].value := cbUFO.text;
      dtmdados.RQuery1.open;
      v_codmun := dtmdados.RQuery1.FieldByName('codmun').value;
      dtmdados.RQuery1.close;
    end;
    Limpavalores;
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.SQL.clear;
    dtmdados.RQuery1.SQL.Add('select count(*) tem from cyber.tb_coleta co ');
    dtmdados.RQuery1.SQL.Add('where co.user_cte = :0 and co.doc is null');
    dtmdados.RQuery1.Parameters[0].value := GLBUSER;
    dtmdados.RQuery1.open;

    if dtmdados.RQuery1.FieldByName('tem').value > 0 then
    begin
      if edVeiculo.text <> '' then
      begin
        if cbRateio.Checked = true then
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = :0 ');
          dtmdados.RQuery1.SQL.Add('where sequencia = :1');
          dtmdados.RQuery1.Parameters[0].value := GLBUSER;
          dtmdados.RQuery1.Parameters[1].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end
        else
        begin
          dtmdados.RQuery1.close;
          dtmdados.RQuery1.SQL.clear;
          dtmdados.RQuery1.SQL.Add('update tb_coleta set rateio = null ');
          dtmdados.RQuery1.SQL.Add('where sequencia = :0');
          dtmdados.RQuery1.Parameters[0].value := QTarefaSEQUENCIA.AsInteger;
          dtmdados.RQuery1.ExecSQL;
          dtmdados.RQuery1.close;
        end;

        SPCalculo.Parameters[51].value := QTarefaFL_CTE.value;
        SPCalculo.Parameters[52].value := RemoveChar(cbOperacao.text);
        SPCalculo.Parameters[53].value := v_codmun;
        SPCalculo.Parameters[54].value := v_codmunD;
        SPCalculo.Parameters[55].value := GLBUSER;
        SPCalculo.Parameters[56].value := edVeiculo.text;
        SPCalculo.Parameters[57].value := edQtPallet.value;
        if cbRateio.Checked = true then
          SPCalculo.Parameters[58].value := 'S'
        else
          SPCalculo.Parameters[58].value := 'N';
        SPCalculo.Parameters[59].value := edAjudante.value;
        SPCalculo.Parameters[60].value := QTarefaNRO_OCS.value;
        SPCalculo.ExecProc;

        edFreteM.value := SPCalculo.Parameters[0].value;
        edPedagio.value := SPCalculo.Parameters[1].value;
        edOutros.value := SPCalculo.Parameters[2].value;
        edfluvial.value := SPCalculo.Parameters[3].value;
        edExc.value := SPCalculo.Parameters[4].value;
        edTR.value := SPCalculo.Parameters[5].value;
        eddespacho.value := SPCalculo.Parameters[6].value;
        edTDE.value := SPCalculo.Parameters[7].value;
        edtas.value := SPCalculo.Parameters[8].value;
        EdSeg.value := SPCalculo.Parameters[9].value;
        EdGris.value := SPCalculo.Parameters[10].value;
        edseg_balsa.value := SPCalculo.Parameters[11].value;
        edred_fluv.value := SPCalculo.Parameters[12].value;
        edagend.value := SPCalculo.Parameters[13].value;
        edpallet.value := SPCalculo.Parameters[14].value;
        edentrega.value := SPCalculo.Parameters[15].value;
        edalfand.value := SPCalculo.Parameters[16].value;
        edcanhoto.value := SPCalculo.Parameters[17].value;
        edBase.value := SPCalculo.Parameters[18].value;
        edAliq.value := SPCalculo.Parameters[19].value;
        edImposto.value := SPCalculo.Parameters[20].value;
        edTotal.value := SPCalculo.Parameters[21].value;
        edCFOP.text := SPCalculo.Parameters[22].value;
        edTabela.text := IntToStr(SPCalculo.Parameters[23].value);
        edFretePeso.value := SPCalculo.Parameters[24].value;
        edTxCte.value := SPCalculo.Parameters[25].value;
        rateio := SPCalculo.Parameters[26].value;
        edVlAjudante.value := SPCalculo.Parameters[27].value;
        edVlAereo.value := SPCalculo.Parameters[28].value;
        edTDA.Value :=  SPCalculo.Parameters[50].value;
        // NFS
        edFreteMNF.value := SPCalculo.Parameters[29].value;
        edPedagioNF.value := SPCalculo.Parameters[30].value;
        edFluvialNF.value := SPCalculo.Parameters[31].value;
        edExcedNF.value := SPCalculo.Parameters[32].value;
        edTRNF.value := SPCalculo.Parameters[33].value;
        edDespachoNF.value := SPCalculo.Parameters[34].value;
        edTDENF.value := SPCalculo.Parameters[35].value;
        edTASNF.value := SPCalculo.Parameters[36].value;
        edSegNF.value := SPCalculo.Parameters[37].value;
        edGrisNF.value := SPCalculo.Parameters[38].value;
        edBalsaNF.value := SPCalculo.Parameters[39].value;
        edRedNF.value := SPCalculo.Parameters[40].value;
        edAgendNF.value := SPCalculo.Parameters[41].value;
        edPalletNF.value := SPCalculo.Parameters[42].value;
        edPortoNF.value := SPCalculo.Parameters[43].value;
        edAlfanNF.value := SPCalculo.Parameters[44].value;
        edCanhotoNF.value := SPCalculo.Parameters[45].value;
        edFretePesoNF.value := SPCalculo.Parameters[46].value;
        edTaxaNF.value := SPCalculo.Parameters[47].value;
        edAjudaNF.value := SPCalculo.Parameters[48].value;
        edTTNF.value := SPCalculo.Parameters[49].value;


        if (cbRateio.Checked = false) and (rateio = 'S') then
          ShowMessage('Esta Tabela Tem Rateio, mas n�o foi escolhido !!');

        if (edAjudante.value > 0) and (edVlAjudante.value = 0) then
          ShowMessage
            ('Esta Tabela N�o Tem Valor de Ajudante ou N�o est� marcado no CRM !!');

        if edTotal.value > 0 then
          btnGerar.Enabled := true;
      end;
    end;
  end;
end;

procedure TfrmGeraCTe.QtripBeforeOpen(DataSet: TDataSet);
begin
  QTrip.Parameters[0].Value := GLBFilial;
end;

function TfrmGeraCTe.RetornaTarefasSelecionadas: String;
var
  sTarSelec: String;
  iCount, escolhido: Integer;
begin
  //relaciona as OCS da TRIP
  if pgtarefas.ActivePage = tsTrip then
  begin
    mdTemp.First;
    iCount := 0;
    // lista tarefas marcadas
    mdTemp.DisableControls;
    if not(QTarefa.Active) then
      QTarefa.open;
    // incluir tarefas j� existente
    QTarefa.First;
    while not QTarefa.eof do
    begin
      if (QTarefaSEQUENCIA.AsInteger <> iRecDelete) then
      begin
        if iCount = 0 then
          sTarSelec := QuotedStr(QTarefaSEQUENCIA.AsString)
        else
          sTarSelec := sTarSelec + ',' + QuotedStr(QTarefaSEQUENCIA.AsString);
        inc(iCount);
      end;
      QTarefa.next;
    end;
    iRecDelete := 0;
    escolhido := 0;
    while not mdTemp.eof do
    begin
      if mdTempInserida.value = 1 then
      begin
        // Agrupa por OCS
        {if cbOS.Checked = true then
        begin
          if escolhido = 0 then
            paga := mdTemppagador.AsInteger;
          escolhido := 1;
          if paga <> mdTemppagador.AsInteger then
          begin
            ShowMessage
              ('Esta OCS n�o � do mesmo pagador da(s) OCS(s) j� escolhida(s)');
            mdTemp.edit;
            mdTempInserida.value := 0;
            edTotCubadoTarefa.value := edTotCubadoTarefa.value -
              qrCalcula_Frete.FieldByName('cub').AsFloat;
            edTotPesoTarefa.value := edTotPesoTarefa.value -
              qrCalcula_Frete.FieldByName('peso').AsFloat;
            edTotValorTarefa.value := edTotValorTarefa.value -
              qrCalcula_Frete.FieldByName('valor').AsFloat;
            edTotVolumeTarefa.value := edTotVolumeTarefa.value -
              qrCalcula_Frete.FieldByName('qtd').AsFloat;
            mdTemp.post;
          end
          else
          begin
            if iCount = 0 then
              sTarSelec := QuotedStr(mdTempReg.AsString)
            else
              sTarSelec := sTarSelec + ',' + QuotedStr(mdTempReg.AsString);
          end;
        end
        else }
        // Cda OCS uma linha , n�o distingue pagador
        QOCS_trip.Close;
        QOCS_trip.Parameters[0].Value := mdTempdestino.AsString;
        QOCS_trip.Open;
        while not QOcs_trip.Eof do
        begin
          if iCount = 0 then
            sTarSelec := QuotedStr(QOcs_tripSEQUENCIA.AsString)
          else
            sTarSelec := sTarSelec + ',' + QuotedStr(QOcs_tripSEQUENCIA.AsString);
          QOCS_trip.next;
          inc(iCount);
        end;
        QOCS_trip.Close;
      end;
      mdTemp.next;
    end;
  end
  else
  begin
    mdTemp.First;
    iCount := 0;
    // lista tarefas marcadas
    mdTemp.DisableControls;
    if not(QTarefa.Active) then
      QTarefa.open;
    // incluir tarefas j� existente
    QTarefa.First;
    while not QTarefa.eof do
    begin
      if (QTarefaSEQUENCIA.AsInteger <> iRecDelete) then
      begin
        if iCount = 0 then
          sTarSelec := QuotedStr(QTarefaSEQUENCIA.AsString)
        else
          sTarSelec := sTarSelec + ',' + QuotedStr(QTarefaSEQUENCIA.AsString);
        inc(iCount);
      end;
      QTarefa.next;
    end;
    iRecDelete := 0;
    escolhido := 0;
    while not mdTemp.eof do
    begin
      if mdTempInserida.value = 1 then
      begin
        // Agrupa por OCS
        if cbOS.Checked = true then
        begin
          if escolhido = 0 then
            paga := mdTemppagador.AsInteger;
          escolhido := 1;
          if paga <> mdTemppagador.AsInteger then
          begin
            ShowMessage
              ('Esta OCS n�o � do mesmo pagador da(s) OCS(s) j� escolhida(s)');
            mdTemp.edit;
            mdTempInserida.value := 0;
            edTotCubadoTarefa.value := edTotCubadoTarefa.value -
              qrCalcula_Frete.FieldByName('cub').AsFloat;
            edTotPesoTarefa.value := edTotPesoTarefa.value -
              qrCalcula_Frete.FieldByName('peso').AsFloat;
            edTotValorTarefa.value := edTotValorTarefa.value -
              qrCalcula_Frete.FieldByName('valor').AsFloat;
            edTotVolumeTarefa.value := edTotVolumeTarefa.value -
              qrCalcula_Frete.FieldByName('qtd').AsFloat;
            mdTemp.post;
          end
          else
          begin
            if iCount = 0 then
              sTarSelec := QuotedStr(mdTempReg.AsString)
            else
              sTarSelec := sTarSelec + ',' + QuotedStr(mdTempReg.AsString);
          end;
        end
        else
        // Cda OCS uma linha , n�o distingue pagador
        begin
          if iCount = 0 then
            sTarSelec := QuotedStr(mdTempReg.AsString)
          else
            sTarSelec := sTarSelec + ',' + QuotedStr(mdTempReg.AsString);
        end;
        inc(iCount);
      end;
      mdTemp.next;
    end;
  end;
  mdTemp.EnableControls;
  result := sTarSelec;
end;

procedure TfrmGeraCTe.rgTarefasClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraCTe.TabSheet1Show(Sender: TObject);
begin
  // CarregaRomaneioManifesto();
  Limpavalores;

  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  CBUF.ItemIndex := -1;
  cbCidadeO.ItemIndex := -1;
  cbUFO.ItemIndex := -1;
  cbCidadeD.ItemIndex := -1;

  pnManifesto.Enabled := false;
end;

procedure TfrmGeraCTe.TabSheet2Show(Sender: TObject);
begin
  QGerados.close;
  QGerados.open;
end;

procedure TfrmGeraCTe.tbCteHide(Sender: TObject);
begin
  QGerados.close;
end;

procedure TfrmGeraCTe.tbCteShow(Sender: TObject);
begin
  QGerados.open;
end;

procedure TfrmGeraCTe.tbTarefaShow(Sender: TObject);
begin
  CarregaTarefas();
end;

procedure TfrmGeraCTe.tsTripShow(Sender: TObject);
begin
  mdTemp.Close;
  mdTemp.Open;
  QTrip.Close;
  QTrip.Open;
  while not QTrip.eof do
  begin
    mdTemp.append;
    mdTempInserida.value := 0;
    mdTempdestino.value := QtripID_TRIP.value;
    mdTemp.post;
    QTrip.Next;
  end;
end;

end.

