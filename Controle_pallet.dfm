object frmControle_pallet: TfrmControle_pallet
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Controle de Pallets'
  ClientHeight = 320
  ClientWidth = 255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 102
    Top = 92
    Width = 34
    Height = 19
    Caption = 'PBR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 92
    Top = 132
    Width = 44
    Height = 19
    Caption = 'CHEP'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 8
    Width = 91
    Height = 19
    Caption = 'Manifesto :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 52
    Width = 42
    Height = 19
    Caption = 'Dest.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblrazao: TLabel
    Left = 59
    Top = 33
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label5: TLabel
    Left = 16
    Top = 175
    Width = 120
    Height = 19
    Caption = 'DESCART'#193'VEL'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 67
    Top = 216
    Width = 69
    Height = 19
    Caption = 'OUTROS'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edPBR: TJvCalcEdit
    Left = 142
    Top = 88
    Width = 69
    Height = 27
    DecimalPlaces = 0
    DisplayFormat = '0'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ShowButton = False
    TabOrder = 0
    DecimalPlacesAlwaysShown = False
  end
  object edChep: TJvCalcEdit
    Left = 142
    Top = 129
    Width = 69
    Height = 27
    DecimalPlaces = 0
    DisplayFormat = '0'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ShowButton = False
    TabOrder = 1
    DecimalPlacesAlwaysShown = False
  end
  object btnConfirma: TBitBtn
    Left = 59
    Top = 265
    Width = 109
    Height = 37
    Caption = 'Confirma'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 4
    OnClick = btnConfirmaClick
  end
  object cbcte: TComboBox
    Left = 67
    Top = 52
    Width = 179
    Height = 22
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnChange = cbcteChange
  end
  object edDesc: TJvCalcEdit
    Left = 142
    Top = 172
    Width = 69
    Height = 27
    DecimalPlaces = 0
    DisplayFormat = '0'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ShowButton = False
    TabOrder = 2
    DecimalPlacesAlwaysShown = False
  end
  object edOutro: TJvCalcEdit
    Left = 142
    Top = 213
    Width = 69
    Height = 27
    DecimalPlaces = 0
    DisplayFormat = '0'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ShowButton = False
    TabOrder = 3
    DecimalPlacesAlwaysShown = False
  end
  object ADOQdes_razao: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select nomeab from cyber.rodcli where fnc_cnpj(codcgc) = fnc_cnp' +
        'j(:0)')
    Left = 196
    Top = 84
    object ADOQdes_razaoNOMEAB: TStringField
      FieldName = 'NOMEAB'
    end
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '3'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '4'
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '5'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '6'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'SELECT distinct c.codclifor, c.razsoc cliente'
      
        'FROM tb_manitem m inner join tb_conhecimento N on m.CODDOC = N.n' +
        'r_conhecimento and m.fildoc = n.fl_empresa '
      
        '                  left join cyber.RODCLI R on N.cod_destinatario' +
        ' = R.CODCLIFOR '
      
        '                  left join cyber.RODCLI C on N.cod_pagador = C.' +
        'CODCLIFOR '
      'WHERE m.filial = :0 and c.codean = '#39'PALLET'#39
      'and m.manifesto = :1'
      'and r.codcgc = :2'
      'union '
      'SELECT distinct r.codclifor, r.razsoc cliente'
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient'
      
        '                    left join kunden@wmswebprd d on d.id_kunde =' +
        ' p.id_kunde_ware  and p.id_klient = d.id_klient '
      'WHERE r.codean = '#39'PALLET'#39' '
      'and p.percent = :3'
      'and d.suchbegriff = :4'
      'union'
      'SELECT distinct r.codclifor, r.razsoc cliente'
      
        'FROM cyber.RODCLI R left join klienten@wmsv11 k on (replace(repl' +
        'ace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegriff'
      
        '                    left join auftraege@wmsv11 p on p.id_klient ' +
        '= k.id_klient'
      
        '                    left join kunden@wmsv11 d on d.id_kunde = p.' +
        'id_kunde_ware  and p.id_klient = d.id_klient'
      'WHERE r.codean = '#39'PALLET'#39
      'and p.percent = :5'
      'and d.suchbegriff = :6')
    Left = 196
    Top = 132
    object QClienteCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object QClienteCLIENTE: TStringField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Size = 80
    end
  end
  object QManifesto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '4'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 9212
      end
      item
        Name = '1'
        DataType = ftString
        Size = 6
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct tipo, nr_romaneio, motorista, placa, veiculo, dt' +
        'inc, transportadora, codfil'
      'from ('
      
        'select distinct '#39'R'#39' tipo, r.nr_romaneio, r.motorista, r.placa, r' +
        '.veiculo, r.dtinc, '
      't.name transportadora, 1 codfil'
      
        'from tb_romaneio@wmswebprd r  left join spediteure@wmswebprd t o' +
        'n to_char(r.cod_transp) = t.id_spediteur and t.lager = :3'
      'union all'
      
        'select distinct '#39'R'#39' tipo, r.nr_romaneio, r.motorista, r.placa, r' +
        '.veiculo, r.dtinc,'
      't.name transportadora, r.fl_empresa codfil'
      
        'from tb_romaneio_wms r  left join spediteure@wmswebprd t on to_c' +
        'har(r.cod_transp) = t.id_spediteur and t.lager = :4'
      'union all'
      
        'select distinct '#39'M'#39' tipo, m.manifesto nr_romaneio, nvl(mma.nomea' +
        'b, mma.nommot) motorista, v.numvei placa, f.descri veiculo,'
      'm.datainc dtinc, fma.nomeab transportadora, m.filial codfil'
      
        'from tb_manifesto m left join cyber.rodmot mma on m.motorista = ' +
        'mma.codmot'
      
        '                    left join cyber.rodcli fma on m.transp = fma' +
        '.codclifor'
      
        '                    left join cyber.rodvei v on v.numvei = m.pla' +
        'ca and v.situac = 1'
      
        '                    left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      'order by 1)'
      'where nr_romaneio = :0'
      'and tipo = :1'
      'and codfil = :2'
      'order by 1')
    Left = 20
    Top = 126
    object QManifestoNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
    end
    object QManifestoMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      ReadOnly = True
    end
    object QManifestoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QManifestoVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 100
    end
    object QManifestoDTINC: TDateTimeField
      FieldName = 'DTINC'
      ReadOnly = True
    end
    object QManifestoTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      ReadOnly = True
    end
    object QManifestoCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
  end
  object QQuant: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select pallet, quant'
      'from tb_controle_pallet pp '
      'where manifesto = :0'
      'and doc = :1'
      'and filial = :2')
    Left = 164
    Top = 12
    object QQuantPALLET: TStringField
      FieldName = 'PALLET'
    end
    object QQuantQUANT: TBCDField
      FieldName = 'QUANT'
      Precision = 32
      Size = 0
    end
  end
end
