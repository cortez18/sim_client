unit Ordem_Coleta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, ImgList, JvMemoryDataset,
  StdCtrls, ComCtrls, JvMaskEdit, JvDBLookup, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  ExtCtrls, ComObj, JvExControls, JvExStdCtrls, JvMemo, pcnConversao, RLReport,
  System.ImageList, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc,
  JvCombobox;

type
  TfrmOrdem_Coleta = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblqt: TLabel;
    mdTemp: TJvMemoryData;
    mdTempColeta: TIntegerField;
    mdTempInserida: TIntegerField;
    mdTempreg: TIntegerField;
    dsTemp: TDataSource;
    iml: TImageList;
    QVeiculo: TADOQuery;
    dtsVeiculo: TDataSource;
    mdTempnr_romaneio: TIntegerField;
    pnManifesto: TPanel;
    Label5: TLabel;
    Panel5: TPanel;
    edPesoLimiteVeiculo: TJvCalcEdit;
    Label13: TLabel;
    Label15: TLabel;
    edQtdCubagem: TJvCalcEdit;
    dstTarefa: TDataSource;
    Panel8: TPanel;
    lblDescricao: TLabel;
    edValor: TEdit;
    QTarefa: TADOQuery;
    edPercentCarga: TJvCalcEdit;
    Label2: TLabel;
    edQtdDestino: TJvCalcEdit;
    Label8: TLabel;
    QOCS: TADOQuery;
    pnTarefas: TPanel;
    pgTarefas: TPageControl;
    tbTarefa: TTabSheet;
    dbgColeta: TJvDBUltimGrid;
    qrCalcula_Frete: TADOQuery;
    qrCalcula_FreteCUB: TBCDField;
    qrCalcula_FretePESO: TBCDField;
    qrCalcula_FreteQTD: TBCDField;
    qrCalcula_FreteVALOR: TBCDField;
    mdTempdestino: TStringField;
    mdTempdata: TDateField;
    QVeiculoDESCRI: TStringField;
    QVeiculoKG: TBCDField;
    QVeiculoCUB: TBCDField;
    btnNovoManifesto: TBitBtn;
    btnGerarMan: TBitBtn;
    Label58: TLabel;
    cbOperacao: TComboBox;
    Label9: TLabel;
    cbTransp: TJvDBLookupEdit;
    Label11: TLabel;
    cbMotorista: TComboBox;
    Label4: TLabel;
    cbplaca: TComboBox;
    Label1: TLabel;
    cbPlaca_carreta: TComboBox;
    dtsTrans: TDataSource;
    QTransp: TADOQuery;
    QTranspNOME: TStringField;
    QTranspCODIGO: TBCDField;
    edVeiculo: TJvMaskEdit;
    QVeiculoPLACA: TStringField;
    tbOCR: TTabSheet;
    tsOrdemG: TTabSheet;
    dgCtrc: TJvDBUltimGrid;
    Panel7: TPanel;
    btnImprMan: TBitBtn;
    Panel9: TPanel;
    btnDescarga: TBitBtn;
    btnCanTransf: TBitBtn;
    btnGeraTransf: TBitBtn;
    SPOrdem: TADOStoredProc;
    Label27: TLabel;
    edCusto: TJvCalcEdit;
    QOCi: TADOQuery;
    dtsOCi: TDataSource;
    QCusto: TADOQuery;
    QCustoCUSTO: TBCDField;
    mdTempfilial: TIntegerField;
    QOCSNRO_OCS: TBCDField;
    QOCSFILIAL: TBCDField;
    QOCSDATAINC: TDateTimeField;
    QOCSNOMEAB: TStringField;
    QTarefaNRO_OCS: TBCDField;
    QTarefaFILIAL: TBCDField;
    QTarefaDATAINC: TDateTimeField;
    QTarefaREM: TStringField;
    QTarefaDES: TStringField;
    QTarefaCLI: TStringField;
    QTarefaUF: TStringField;
    QTarefaPESO: TBCDField;
    btnCarregar_Tarefas: TBitBtn;
    dgTarefa: TJvDBUltimGrid;
    Label16: TLabel;
    edTotVolumeTarefa: TJvCalcEdit;
    Label25: TLabel;
    edTotPesoTarefa: TJvCalcEdit;
    Label14: TLabel;
    edTotCubadoTarefa: TJvCalcEdit;
    Label26: TLabel;
    edTotValorTarefa: TJvCalcEdit;
    btnVoltaTarefa: TBitBtn;
    QOCa: TADOQuery;
    dtsOCa: TDataSource;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QOCaID_ORDEM: TBCDField;
    QOCaFILIAL: TBCDField;
    QOCaDATINC: TDateTimeField;
    QOCaREM: TStringField;
    QOCaDES: TStringField;
    QOCaCLI: TStringField;
    QOCaUF: TStringField;
    QOCaPESO: TBCDField;
    JvDBUltimGrid2: TJvDBUltimGrid;
    QOCr: TADOQuery;
    dtsOCr: TDataSource;
    QOCiCODOCS: TBCDField;
    QOCiNOTFIS: TStringField;
    QOCiPESOKG: TBCDField;
    QOCiPESCUB: TBCDField;
    QOCiPESCAL: TBCDField;
    QOCiVLRMER: TBCDField;
    QOCrID_ORDEM: TBCDField;
    QOCrFILIAL: TBCDField;
    QOCrDATINC: TDateTimeField;
    QOCrREM: TStringField;
    QOCrDES: TStringField;
    QOCrCLI: TStringField;
    QOCrUF: TStringField;
    QOCrTRANSP: TStringField;
    QOCrUSUINC: TStringField;
    edOC: TJvCalcEdit;
    Label3: TLabel;
    Label10: TLabel;
    dtInicial: TJvDateEdit;
    Label6: TLabel;
    dtFinal: TJvDateEdit;
    DBGItens: TJvDBUltimGrid;
    dtsIt: TDataSource;
    QIt: TADOQuery;
    QITCODOCS: TBCDField;
    QITNOTFIS: TStringField;
    QITPESOKG: TBCDField;
    QITPESCUB: TBCDField;
    QITPESCAL: TBCDField;
    QITVLRMER: TBCDField;
    QOCaRAZSOC: TStringField;
    Label7: TLabel;
    edOCa: TJvCalcEdit;
    Label12: TLabel;
    edNF: TJvCalcEdit;
    QItQUANTI: TBCDField;
    QOCiQUANTI: TBCDField;
    QOCaPLACA: TStringField;
    QOCrDESCRI: TStringField;
    QOCrCONFERIDO: TStringField;
    btnDescarregado: TBitBtn;
    QOCrDT_RECEB: TDateTimeField;
    QOCrPLACA: TStringField;
    QOCaREMETENTE: TBCDField;
    QOCrREMETENTE: TBCDField;
    TabSheet2: TTabSheet;
    JvDBUltimGrid3: TJvDBUltimGrid;
    QOc: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    DateTimeField1: TDateTimeField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    BCDField3: TBCDField;
    dtsOC: TDataSource;
    Label17: TLabel;
    edOCS: TJvCalcEdit;
    Panel3: TPanel;
    Label18: TLabel;
    Label21: TLabel;
    BitBtn1: TBitBtn;
    JvCalcEdit1: TJvCalcEdit;
    JvCalcEdit2: TJvCalcEdit;
    BitBtn2: TBitBtn;
    QOCaCODCON: TBCDField;
    Label45: TLabel;
    edData: TJvDateEdit;
    btnEstornar: TBitBtn;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    JvCalcEdit3: TJvCalcEdit;
    DBGrid2: TDBGrid;
    BitBtn3: TBitBtn;
    JvCalcEdit5: TJvCalcEdit;
    JvCalcEdit6: TJvCalcEdit;
    QM: TADOQuery;
    QOCSEQUENCIA: TBCDField;
    QOCPAGADOR: TBCDField;
    QOCREMETENTE: TBCDField;
    QOCNRO_OCS: TBCDField;
    QOCUSER_CTE: TStringField;
    QOCNR_OC: TBCDField;
    dtsQM: TDataSource;
    QMOPERACAO: TStringField;
    Label20: TLabel;
    cbom: TComboBox;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLDraw1: TRLDraw;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLSystemInfo1: TRLSystemInfo;
    RLBand2: TRLBand;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDraw2: TRLDraw;
    RLBand3: TRLBand;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLReport2: TRLReport;
    RLBand4: TRLBand;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw3: TRLDraw;
    RLDBText8: TRLDBText;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLSystemInfo2: TRLSystemInfo;
    RLBand5: TRLBand;
    RLDBText13: TRLDBText;
    RLDBText14: TRLDBText;
    RLBand6: TRLBand;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLDBText15: TRLDBText;
    RLDBText16: TRLDBText;
    RLDBText17: TRLDBText;
    TabSheet4: TTabSheet;
    dtsOCC: TDataSource;
    QOCC: TADOQuery;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    DateTimeField3: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    BCDField7: TBCDField;
    BCDField8: TBCDField;
    QOCCdatatu: TDateField;
    QOCCusuatu: TStringField;
    Label49: TLabel;
    edKm: TJvCalcEdit;
    QKM: TADOQuery;
    QKMDESCRI: TStringField;
    QKMCODCLIFOR: TBCDField;
    QKMGEO: TStringField;
    QKMCODCEP: TStringField;
    XMLDoc: TXMLDocument;
    QOCCNRO_OCS: TBCDField;
    JvDBGrid1: TJvDBGrid;
    QOCaOPERACAO: TStringField;
    QOCrCODOCS: TFMTBCDField;
    QOCrOPERACAO: TStringField;
    QOcCODOCS: TFMTBCDField;
    QOcOPERACAO: TStringField;
    QVeiculoCasa: TADOQuery;
    QVeiculoCasaPLACA: TStringField;
    QVeiculoCasaDESCRI: TStringField;
    QVeiculoCasaKG: TBCDField;
    QVeiculoCasaCUB: TBCDField;
    QVeiculoCasaSITRAS: TStringField;
    QVeiculoCasaCODPRO: TBCDField;
    QPGR_M: TADOQuery;
    SP_PGR: TADOStoredProc;
    QPGR_MID_REGRA: TFMTBCDField;
    QPGR_MCLIENTE: TFMTBCDField;
    QPGR_MRAZSOC: TStringField;
    QPGR_MVLRMER: TFMTBCDField;
    Label50: TLabel;
    cbIsca1: TJvComboBox;
    Label51: TLabel;
    cbIsca2: TJvComboBox;
    QIsca: TADOQuery;
    QIscaNM_ISCA: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgColetaCellClick(Column: TColumn);
    procedure dbgColetaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnCarregar_TarefasClick(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure tbOCSShow(Sender: TObject);
    procedure mdTempAfterOpen(DataSet: TDataSet);
    procedure tbTarefaShow(Sender: TObject);
    procedure btnVoltaTarefaClick(Sender: TObject);
    procedure dstTarefaDataChange(Sender: TObject; Field: TField);
    procedure cbTranspExit(Sender: TObject);
    procedure btnNovoManifestoClick(Sender: TObject);
    procedure btnGerarManClick(Sender: TObject);
    procedure cbOperacaoChange(Sender: TObject);
    procedure QOCiBeforeOpen(DataSet: TDataSet);
    procedure btnImprManClick(Sender: TObject);
    procedure cbplacaChange(Sender: TObject);
    procedure btnGeraTransfClick(Sender: TObject);
    procedure QOCSBeforeOpen(DataSet: TDataSet);
    procedure QOCaBeforeOpen(DataSet: TDataSet);
    procedure QOCrBeforeOpen(DataSet: TDataSet);
    procedure tsOrdemGShow(Sender: TObject);
    procedure QOCaAfterScroll(DataSet: TDataSet);
    procedure edOCaExit(Sender: TObject);
    procedure edNFExit(Sender: TObject);
    procedure edOCaEnter(Sender: TObject);
    procedure edNFEnter(Sender: TObject);
    procedure btnDescargaClick(Sender: TObject);
    procedure btnDescarregadoClick(Sender: TObject);
    procedure edValorEnter(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure QOcBeforeOpen(DataSet: TDataSet);
    procedure QOCrAfterScroll(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnCanTransfClick(Sender: TObject);
    procedure edDataChange(Sender: TObject);
    procedure btnEstornarClick(Sender: TObject);
    procedure JvCalcEdit5Exit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLReport2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure QOCCBeforeOpen(DataSet: TDataSet);
    procedure TabSheet4Show(Sender: TObject);
    procedure TabSheet4Hide(Sender: TObject);
    procedure QKMBeforeOpen(DataSet: TDataSet);
    procedure gravageo;
    procedure calculakm;
  private
    { Private declarations }
    iRecDelete: Integer;
    gerakm, lat, lon: String;
    bInicio: Boolean;

  var
    km: double;

    procedure CarregaTarefas();
    procedure LimpaCamposManifesto();
    function RetornaTarefasSelecionadas(): String;

  public
    { Public declarations }

    procedure CarregaBtn();
  end;

var
  frmOrdem_Coleta: TfrmOrdem_Coleta;

implementation

uses Dados, menu, funcoes, UrlMon;

{$R *.dfm}

procedure TfrmOrdem_Coleta.btnGerarManClick(Sender: TObject);
var
  man, icount, erropgr : Integer;
  roma: String;
begin

  if cbMotorista.Text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    cbMotorista.SetFocus;
    Exit;
  end;

  if cbplaca.Text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    cbplaca.SetFocus;
    Exit;
  end;

  if cbOperacao.Text = '' then
  begin
    ShowMessage('Escolha o Modal');
    cbOperacao.SetFocus;
    Exit;
  end;

  if edCusto.value = 0 then
  begin
    if Application.Messagebox
      ('O Custo desta coleta ficou ZERO, vai liberar assim ?', 'Liberar O.C.',
      MB_YESNO + MB_ICONQUESTION) = IDNO then
      Exit;
  end;

  // verifica��o das regras do PGR
  Screen.Cursor := crHourGlass;
  QTarefa.DisableControls;
  QTarefa.First;
  icount := 0;

  while not(QTarefa.eof) do
  begin
    if icount = 0 then
      roma := QTarefaNRO_OCS.asstring
    else
      roma := roma + ',' + QTarefaNRO_OCS.asstring;
    icount := icount + 1;
    QTarefa.next;
  end;

  QPGR_M.close;
  QPGR_M.sql [6] := 'and o.codocs in ('+ roma + ')';
  QPGR_M.Open;

  while not QPGR_M.eof do
  begin
    if QPGR_MID_REGRA.isnull then
    begin
      showmessage('Valor M�ximo Permitido do PGR Ultrapassado !');
      erropgr := 1;
    end;
    SP_PGR.Parameters[2].Value := QPGR_MID_REGRA.AsInteger;
    SP_PGR.Parameters[3].Value := cbIsca1.Text;
    SP_PGR.Parameters[4].Value := cbIsca2.Text;
    SP_PGR.Parameters[5].Value := QTranspCODIGO.AsInteger;
    SP_PGR.Parameters[6].Value := QPGR_MVLRMER.AsFloat;
    SP_PGR.ExecProc;
    if SP_PGR.Parameters[1].Value = 'S' then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      erropgr := 1;
    end
    else if SP_PGR.Parameters[0].value <> null then
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
    QPGR_M.Next;
  end;
  QTarefa.EnableControls;

  if erropgr > 0 then
   exit;

  if Application.Messagebox('Gerar Ordem de Coleta Agora ?', 'Gerar O.C.',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    QTarefa.DisableControls;
    QTarefa.First;
    icount := 0;
    while not(QTarefa.eof) do
    begin
      if icount = 0 then
        roma := QTarefaNRO_OCS.AsString
      else
        roma := roma + ',' + QTarefaNRO_OCS.AsString;
      inc(icount);
      QTarefa.Next;
    end;
    QTarefa.EnableControls;

    SPOrdem.Parameters[1].value := cbOperacao.Text;
    SPOrdem.Parameters[2].value := glbFilial;
    SPOrdem.Parameters[3].value := QTranspCODIGO.AsInteger;
    SPOrdem.Parameters[4].value := cbplaca.Text;
    SPOrdem.Parameters[5].value := GLBUSER;
    SPOrdem.Parameters[6].value := RemoveChar(cbMotorista.Text);
    SPOrdem.Parameters[7].value := roma;
    SPOrdem.Parameters[8].value := edCusto.value;
    SPOrdem.Parameters[9].value := cbPlaca_carreta.Text;
    SPOrdem.ExecProc;
    man := SPOrdem.Parameters[0].value;

    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Ordem de Coleta N�(s) ' + IntToStr(man) + ' Gerada');

    btnGerarMan.Enabled := false;

    mdTemp.Close;
    mdTemp.Open;

  end;
  QTarefa.Close;
  LimpaCamposManifesto();
  pnManifesto.Enabled := false;
  CarregaTarefas();
  PageControl1.ActivePageIndex := 1;
end;

procedure TfrmOrdem_Coleta.btnGeraTransfClick(Sender: TObject);
var
  vconf: String;
begin
  if Application.Messagebox('Receber O.C. Agora ?', 'Receber O.C.',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if not(InputQuery('Conferido Por', 'Confer�ncia de Descarga', vconf)) then
      Exit;
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('update tb_ordem set dt_receb = sysdate, conferido = :0 ');
    dtmdados.IQuery1.SQL.add('where id_ordem = :1');
    dtmdados.IQuery1.Parameters[0].value := vconf;
    dtmdados.IQuery1.Parameters[1].value := QOCaID_ORDEM.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
    QOCa.Close;
    QOCa.Open;
  end;
  PageControl1.ActivePageIndex := 2;
end;

procedure TfrmOrdem_Coleta.btnDescargaClick(Sender: TObject);
begin
  if not QOCa.IsEmpty then
    RLReport1.Preview;
end;

procedure TfrmOrdem_Coleta.btnDescarregadoClick(Sender: TObject);
begin
  if not QOCr.IsEmpty then
    RLReport2.Preview;
end;

procedure TfrmOrdem_Coleta.btnEstornarClick(Sender: TObject);
begin
  if not dtmdados.PodeApagar(name) then
    Exit;

  if Application.Messagebox('Estornar a Baixa de Entrega ?', 'Estornar a Baixa',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('update tb_ordem set dt_receb = null, datatu = sysdate, usuatu = :0 ');
    dtmdados.IQuery1.SQL.add('where id_ordem = :1');
    dtmdados.IQuery1.Parameters[0].value := GLBUSER;
    dtmdados.IQuery1.Parameters[1].value := QOCrID_ORDEM.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
    PageControl1.TabIndex := 1;
  end;
end;

procedure TfrmOrdem_Coleta.BitBtn1Click(Sender: TObject);
begin

  QOc.Close;

  if JvCalcEdit1.value > 0 then
    QOc.SQL[29] := 'And id_ordem = ' + QuotedStr(JvCalcEdit1.Text)
  else
    QOc.SQL[29] := '';

  if JvCalcEdit2.value > 0 then
    QOc.SQL[31] := 'And codocs = ' + QuotedStr(JvCalcEdit2.Text)
  else
    QOc.SQL[31] := '';

  QOc.Open;
end;

procedure TfrmOrdem_Coleta.BitBtn2Click(Sender: TObject);
begin
  QOc.Close;
  QOc.SQL[29] := '';
  QOc.SQL[31] := '';
  QOc.Open;
end;

procedure TfrmOrdem_Coleta.BitBtn3Click(Sender: TObject);
var
  men: string;
begin
  if not dtmdados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Alterar Este Registro ?', 'Alterar',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if JvCalcEdit6.value > 0 then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add('Update cyber.tb_coleta set nr_oc = ' +
        QuotedStr(apcarac(JvCalcEdit6.Text)) + ' Where doc = ' +
        QuotedStr(apcarac(JvCalcEdit3.Text)) + 'and filial = ' +
        QuotedStr(apcarac(JvCalcEdit5.Text)));
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
    end;
    if cbom.Text <> '' then
    begin
      if cbom.Text <> QMOPERACAO.value then
      begin
        dtmdados.IQuery1.Close;
        dtmdados.IQuery1.SQL.clear;
        dtmdados.IQuery1.SQL.add('Update tb_ordem set operacao = :0 ');
        dtmdados.IQuery1.SQL.add('Where id_ordem = ' +
          QuotedStr(apcarac(JvCalcEdit6.Text)));
        dtmdados.IQuery1.Parameters[0].value := cbom.Text;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.IQuery1.Close;
      end;
    end;

    ShowMessage('Ordem de Coleta Alterada');

    QM.Close;
    cbom.ItemIndex := -1;
    JvCalcEdit3.value := 0;
    JvCalcEdit5.value := 0;
    JvCalcEdit6.value := 0;
    BitBtn3.Enabled := false;
  end;
end;

procedure TfrmOrdem_Coleta.btnCanTransfClick(Sender: TObject);
var
  motivo: String;
begin
  if Application.Messagebox('Cancelar Ordem de Coleta ?', 'Cancelar O.C.',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    motivo := InputBox('Qual o Motivo do Cancelamento ? :',
      'Motivo do Cancelamento da O.C.', '');
    if motivo <> '' then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('update tb_ordem set fl_status = ''C'', usuatu = :0, motivo = :1, datatu = sysdate ');
      dtmdados.IQuery1.SQL.add('where id_ordem = :2');
      dtmdados.IQuery1.Parameters[0].value := GLBUSER;
      dtmdados.IQuery1.Parameters[1].value := motivo;
      dtmdados.IQuery1.Parameters[2].value := QOCaID_ORDEM.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;

      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('update cyber.tb_coleta set nr_oc = null where nr_oc = :0');
      dtmdados.IQuery1.Parameters[0].value := QOCaID_ORDEM.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
      QOCa.Close;
      QOCa.Open;
    end;
  end;
end;

procedure TfrmOrdem_Coleta.btnCarregar_TarefasClick(Sender: TObject);
begin
  CarregaBtn();
end;

procedure TfrmOrdem_Coleta.btnImprManClick(Sender: TObject);
var
  dt: String;
begin
  dt := copy(dtInicial.Text, 1, 2);
  if (dt = '  ') and (edOC.value = 0) and (edOCS.value = 0) then
  begin
    ShowMessage('Voc� n�o escolheu as Datas, OC ou OCS');
    dtInicial.SetFocus;
    Exit;
  end;
  QOCr.Close;

  if edOC.value > 0 then
    QOCr.SQL[29] := 'And id_ordem = ' + QuotedStr(apcarac(edOC.Text))
  else
    QOCr.SQL[29] := '';

  if dt <> '  ' then
    QOCr.SQL[30] := 'and datinc between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QOCr.SQL[30] := '';

  if edOCS.value > 0 then
    QOCr.SQL[31] := 'And codocs = ' + QuotedStr(apcarac(edOCS.Text))
  else
    QOCr.SQL[31] := '';

  QOCr.Open;
end;

procedure TfrmOrdem_Coleta.btnNovoManifestoClick(Sender: TObject);
begin
  pnManifesto.Enabled := True;
  btnNovoManifesto.Enabled := false;
  // btnGerarMan.Enabled         := True;

  // carregar as iscas
  QIsca.close;
  QIsca.Open;
  cbIsca1.clear;
  cbIsca2.clear;
  cbIsca1.Items.add('');
  cbIsca2.Items.add('');
  while not QIsca.eof do
  begin
    cbIsca1.Items.add(QIscaNM_ISCA.value);
    cbIsca2.Items.add(QIscaNM_ISCA.value);
    QIsca.next;
  end;

  if cbOperacao.CanFocus then
    cbOperacao.SetFocus;
end;

procedure TfrmOrdem_Coleta.btnVoltaTarefaClick(Sender: TObject);
begin
  iRecDelete := QTarefaNRO_OCS.AsInteger;

  mdTemp.Locate('Coleta', QTarefaNRO_OCS.AsInteger, []);

  qrCalcula_Frete.Close;
  qrCalcula_Frete.Parameters[0].value := mdTempColeta.AsInteger;
  qrCalcula_Frete.Parameters[1].value := mdTempfilial.AsInteger;
  qrCalcula_Frete.Open;

  edTotCubadoTarefa.value := edTotCubadoTarefa.value -
    qrCalcula_Frete.FieldByName('cub').AsFloat;
  edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
    ('peso').AsFloat;
  edTotValorTarefa.value := edTotValorTarefa.value - qrCalcula_Frete.FieldByName
    ('valor').AsFloat;
  edTotVolumeTarefa.value := edTotVolumeTarefa.value -
    qrCalcula_Frete.FieldByName('qtd').AsFloat;

  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.post;

  CarregaBtn();
end;

procedure TfrmOrdem_Coleta.CarregaBtn;
var
  starefa: string;
begin
  starefa := '';
  starefa := RetornaTarefasSelecionadas();

  if (starefa = '') then
    starefa := '0';

  QTarefa.Close;
  QTarefa.SQL[7] := 'and o.codocs in (' + starefa + ')';
  QTarefa.Parameters[0].value := glbFilial;
  QTarefa.Open;

  mdTemp.First;
end;

procedure TfrmOrdem_Coleta.CarregaTarefas();
begin
  QOCS.Close;
  QOCS.SQL[6] := 'and o.datinc > to_date(' + QuotedStr(edData.Text) +
    ',''dd/mm/yyyy'')';
  QOCS.Open;
  QOCS.First;
  mdTemp.Close;
  mdTemp.Open;
  while not QOCS.eof do
  begin
    mdTemp.append;
    mdTempColeta.value := QOCSNRO_OCS.AsInteger;
    mdTempdestino.value := QOCSNOMEAB.value;
    mdTempdata.value := QOCSDATAINC.value;
    mdTempfilial.value := QOCSFILIAL.AsInteger;
    QOCS.Next;
  end;
  mdTemp.First;
end;

procedure TfrmOrdem_Coleta.cbOperacaoChange(Sender: TObject);
var
  iroma, icount: Integer;
  roma: String;
  l1, l2: double;
begin
  if cbOperacao.Text <> '' then
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select fl_peso, fl_viagem, fl_mdfe, fl_margem, fl_km from tb_operacao where operacao = :0 ');
    dtmdados.IQuery1.Parameters[0].value := cbOperacao.Text;
    dtmdados.IQuery1.Open;
    QTransp.Close;
    QTransp.SQL[8] := 'and o.operacao = ' + QuotedStr(cbOperacao.Text);
    QTransp.Open;
    gerakm := dtmdados.IQuery1.FieldByName('fl_km').AsString;
    dtmdados.IQuery1.Close;
    // calcular km
    if gerakm = 'S' then
    begin
      Screen.Cursor := crHourGlass;
      QTarefa.DisableControls;
      QTarefa.First;
      icount := 0;
      iroma := 0;
      while not(QTarefa.eof) do
      begin
        if iroma <> QTarefaNRO_OCS.AsInteger then
        begin
          if icount = 0 then
            roma := QTarefaNRO_OCS.AsString
          else
            roma := roma + ',' + QTarefaNRO_OCS.AsString;
        end;
        icount := icount + 1;
        iroma := QTarefaNRO_OCS.AsInteger;
        QTarefa.Next;
      end;
      QTarefa.EnableControls;
      QKM.Close;
      QKM.SQL[4] := 'where nro_ocs in (' + roma + ')';
      // showmessage(qkm.sql.Text);
      // QKM.Parameters[0].value := roma;
      QKM.Open;
      km := 0;
      if not QKM.eof then
      begin
        While not QKM.eof do
        begin
          if apcarac(QKMGEO.value) = '' then
          begin
            lat := '';
            lon := '';
            gravageo;
            if lat <> '' then
            begin
              l1 := StrtoFloat(buscatroca(lat, '.', ','));
              l2 := StrtoFloat(buscatroca(lon, '.', ','));
              dtmdados.IQuery1.Close;
              dtmdados.IQuery1.SQL.clear;
              dtmdados.IQuery1.SQL.add
                ('update cyber.rodcli set latitu = :0, longit = :1 ');
              dtmdados.IQuery1.SQL.add('where codclifor = :2 ');
              dtmdados.IQuery1.Parameters[0].value := l1;
              dtmdados.IQuery1.Parameters[1].value := l2;
              dtmdados.IQuery1.Parameters[2].value := QKMCODCLIFOR.AsInteger;
              dtmdados.IQuery1.ExecSQL;
              dtmdados.IQuery1.Close;
            end;
          end;
          QKM.Next;
        end;
        QKM.First;
        calculakm;
        if km > 0 then
          edKm.value := km
        else
          edKm.value := 0;
      end;
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfrmOrdem_Coleta.gravageo;
var
  XMLUrl, cep, orig, dest, XMLFileName, a: String;
  i, j, k: Integer;
  t: double;
begin
  dest := apcarac(QKMDESCRI.AsString);
  XMLUrl := 'https://maps.google.com/maps/api/geocode/xml?sensor=false&address='
    + QKMDESCRI.value + '+' + QKMCODCEP.AsString + '+BR' +
    '&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
  XMLFileName := 'C:\sim\geo.xml';

  URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

  XMLDoc.FileName := 'C:\sim\geo.xml';
  XMLDoc.Active := True;
  i := 0;
  j := 0;
  k := 0;
  for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
  begin
    a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
    if a = 'result' then
    begin
      for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
        .ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
        for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
            [k].NodeName;
          if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k]
            .NodeName = 'location' then
          begin
            lat := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].ChildNodes['lat'].Text;
            lon := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].ChildNodes['lng'].Text;
          end;
        end;
      end;
    end;
  end;
  XMLDoc.Active := false;
end;

procedure TfrmOrdem_Coleta.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a, b: String;
  Met, i, x, j, k: Integer;
  Sec: TTime;
  t, klm: double;
  orig1, dest1: String;
begin
  x := 0;
  j := 0;
  t := 0;
  // pegar o cep da filial como ponto de partida
  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.add
    ('select (d.latitu ||''+''|| d.longit) cep from cyber.tb_coleta f left join cyber.rodcli d on d.codclifor = f.remetente ');
  dtmdados.IQuery1.SQL.add
    ('left join cyber.rodmun m on d.codmun = m.codmun where f.nro_ocs = :0 and f.filial = :1 ');
  dtmdados.IQuery1.Parameters[0].value := QTarefaNRO_OCS.AsInteger;
  // glbfilial;
  dtmdados.IQuery1.Parameters[1].value := glbFilial;
  dtmdados.IQuery1.Open;
  cep := dtmdados.IQuery1.FieldByName('cep').value;
  dtmdados.IQuery1.Close;

  if QKM.RecordCount > 0 then
  begin
    while not QKM.eof do
    begin
      if x = 0 then
      begin
        orig := cep;
        orig1 := orig;
        if apcarac(QKMGEO.value) <> '' then
          dest := QKMGEO.AsString
        else
          dest := apcarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if apcarac(QKMGEO.value) <> '' then
          dest := QKMGEO.AsString
        else
          dest := apcarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        dest1 := dest;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest; // ApCarac(QKMCODCEP.AsString)+'+'+QKMDESCRI.AsString;
      end;

      XMLFileName := 'C:\sim\temp.xml';

      URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

      XMLDoc.FileName := 'C:\sim\temp.xml';
      XMLDoc.Active := True;
      i := 0;
      j := 0;
      k := 0;
      for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
        if a = 'row' then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
              .ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName = 'distance' then
              begin
                // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
                // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

                km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].Text) + km;
                // edDistancia.Text := FloatToStr(t);

              end;
              b := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                ['status'].Text;
            end;
          end;
        end;
      end;
      // Memo1.Lines.Add(edit1.Text);
      // Memo1.Lines.Add('-----------------');
      XMLDoc.Active := false;
      x := x + 1;
      QKM.Next;
    end;
    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
    XMLFileName := 'C:\sim\temp.xml';
    URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

    XMLDoc.FileName := 'C:\sim\temp.xml';
    XMLDoc.Active := True;
    i := 0;
    j := 0;
    k := 0;
    for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
    begin
      a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
      if a = 'row' then
      begin
        for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
          for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].NodeName;
            if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k]
              .NodeName = 'distance' then
            begin
              // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
              // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

              km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
                .ChildNodes[k].ChildNodes['text'].Text) + km;
              // edDistancia.Text := FloatToStr(t);

            end;
            b := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              ['status'].Text;
          end;
        end;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

procedure TfrmOrdem_Coleta.cbplacaChange(Sender: TObject);
var
  ocs: String;
  icount: Integer;
begin
  btnGerarMan.Enabled := false;
  if cbplaca.Text <> '' then
  begin
    QVeiculo.Locate('placa', cbplaca.Text, []);
    edVeiculo.Text := QVeiculoDESCRI.value;
    edPesoLimiteVeiculo.value := QVeiculoKG.value;
    edQtdCubagem.value := QVeiculoCUB.value;

    if edPesoLimiteVeiculo.value > 0 then
      edPercentCarga.value :=
        (edTotPesoTarefa.value / edPesoLimiteVeiculo.value) * 100
    else
      edPercentCarga.value := 0;
    if edPercentCarga.value < 50 then
      edPercentCarga.Color := clred;

    if edPercentCarga.value > 70 then
      edPercentCarga.Color := $000080FF;

    QTarefa.DisableControls;
    QTarefa.First;
    icount := 0;
    while not(QTarefa.eof) do
    begin
      if icount = 0 then
        ocs := QTarefaNRO_OCS.AsString
      else
        ocs := ocs + ',' + QTarefaNRO_OCS.AsString;
      inc(icount);
      QTarefa.Next;
    end;
    QTarefa.EnableControls;

    QCusto.Close;
    QCusto.Parameters[0].value := cbplaca.Text;
    QCusto.Parameters[1].value := cbOperacao.Text;
    QCusto.Parameters[2].value := QTranspCODIGO.value;
    QCusto.Parameters[3].value := glbFilial;
    QCusto.Parameters[4].value := ocs;

    QCusto.Open;
    edCusto.value := QCustoCUSTO.value;
    QCusto.Close;

    btnGerarMan.Enabled := True;
  end;
end;

procedure TfrmOrdem_Coleta.cbTranspExit(Sender: TObject);
begin
  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.add
    ('select (codmot ||'' - ''||nommot) nome from cyber.rodmot where codclifor = :0 order by 1');
  dtmdados.IQuery1.Parameters[0].value := QTranspCODIGO.AsInteger;
  dtmdados.IQuery1.Open;
  cbMotorista.Clear;
  while not dtmdados.IQuery1.eof do
  begin
    cbMotorista.Items.add(dtmdados.IQuery1.FieldByName('nome').value);
    dtmdados.IQuery1.Next;
  end;
  dtmdados.IQuery1.Close;
  // carregar os ve�culos do transportador
  if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
  begin
    QVeiculoCasa.close;
    QVeiculoCasa.Open;
    cbPlaca.clear;
    cbPlaca.Items.add('');
    while not QVeiculoCasa.eof do
    begin
      cbPlaca.Items.add(QVeiculoCasaPLACA.value);
      QVeiculoCasa.next;
    end;
  end
  else
  begin
    QVeiculo.close;
    QVeiculo.Parameters[0].value := QTranspCODIGO.AsInteger;
    QVeiculo.Open;
    cbPlaca.clear;
    cbPlaca.Items.add('');
    while not QVeiculo.eof do
    begin
      cbPlaca.Items.add(QVeiculoPLACA.value);
      QVeiculo.next;
    end;

    cbPlaca_carreta.clear;
    dtmDados.RQuery1.close;
    dtmDados.RQuery1.sql.clear;
    dtmDados.RQuery1.sql.add
      ('select codvei, numvei placa_carreta, codpro from cyber.rodvei v where v.tipvei = 7 and v.codclifor = :0');
    dtmDados.RQuery1.Parameters[0].value := QTranspCODIGO.AsInteger;
    dtmDados.RQuery1.Open;
    cbPlaca_carreta.Items.add('');
    while not dtmDados.RQuery1.eof do
    begin
      cbPlaca_carreta.Items.add(dtmDados.RQuery1.FieldByName
        ('placa_carreta').value);
      dtmDados.RQuery1.next;
    end;
    dtmDados.RQuery1.close;

  end;


end;

procedure TfrmOrdem_Coleta.dbgColetaCellClick(Column: TColumn);
var
  reg: Integer;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;
  //
  qrCalcula_Frete.Close;
  qrCalcula_Frete.Parameters[0].value := mdTempColeta.AsInteger;
  qrCalcula_Frete.Parameters[1].value := mdTempfilial.AsInteger;
  qrCalcula_Frete.Open;
  //
  mdTemp.edit;
  reg := 0;
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    mdTempreg.value := reg;
    mdTempInserida.value := 1;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value +
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value + qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value +
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
  end
  else
  begin
    mdTempInserida.value := 0;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value -
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value -
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
  end;
  mdTemp.post;
end;

procedure TfrmOrdem_Coleta.dbgColetaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgColeta.Canvas.FillRect(Rect);
      iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmOrdem_Coleta.dstTarefaDataChange(Sender: TObject; Field: TField);
begin
  // btnVoltaTarefa.Enabled := QTarefa.RecordCount > 0;
end;

procedure TfrmOrdem_Coleta.edDataChange(Sender: TObject);
begin
  CarregaTarefas;
end;

procedure TfrmOrdem_Coleta.edNFEnter(Sender: TObject);
begin
  edOCa.clear;
end;

procedure TfrmOrdem_Coleta.edNFExit(Sender: TObject);
begin
  if edNF.Text <> '' then
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select o.codrmc from cyber.rodioc i left join cyber.rodocs o on i.codocs = o.codocs and i.filocs = o.filocs ');
    dtmdados.IQuery1.SQL.add
      ('where o.codrmc is not null and i.notfis = :0 and i.filocs = :1 ');
    dtmdados.IQuery1.SQL.add('union all ');
    dtmdados.IQuery1.SQL.add('select o.nr_oc codmrc ');
    dtmdados.IQuery1.SQL.add
      ('from cyber.rodioc i left join cyber.tb_coleta o on i.codocs = o.nro_ocs and i.filocs = o.filial ');
    dtmdados.IQuery1.SQL.add('where o.nr_oc is not null  ');
    dtmdados.IQuery1.SQL.add('and i.notfis = :2 and i.filocs = :3 ');
    dtmdados.IQuery1.Parameters[0].value := edNF.Text;
    dtmdados.IQuery1.Parameters[1].value := glbFilial;
    dtmdados.IQuery1.Parameters[2].value := edNF.Text;
    dtmdados.IQuery1.Parameters[3].value := glbFilial;
    dtmdados.IQuery1.Open;
    if not dtmdados.IQuery1.eof then
    begin
      if not QOCa.Locate('id_ordem', dtmdados.IQuery1.FieldByName('codrmc')
        .value, []) then
        ShowMessage('NF N�o encontrada')
      else
        QIt.Locate('notfis', edNF.Text, []);
    end
    else
      ShowMessage('NF N�o encontrada');
    dtmdados.IQuery1.Close;
  end;
end;

procedure TfrmOrdem_Coleta.edOCaEnter(Sender: TObject);
begin
  edNF.clear;
end;

procedure TfrmOrdem_Coleta.edOCaExit(Sender: TObject);
begin
  if edOCa.Text <> '' then
  begin
   { dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select o.codrmc from cyber.rodocs o where o.codocs = :0 and o.filocs = :1 ');
    dtmdados.IQuery1.Parameters[0].value := edOCa.Text;
    dtmdados.IQuery1.Parameters[1].value := glbFilial;
    dtmdados.IQuery1.Open;
    if not dtmdados.IQuery1.eof then
    begin  }
    if not QOca.locate('id_ordem;filial', VarArrayOf([apcarac(edOCa.Text),glbFilial]), []) then
    //  if not QOCa.Locate('id_ordem', dtmdados.IQuery1.FieldByName('codrmc')
    //    .value, []) then
        ShowMessage('OCS N�o encontrada');
    //end
    //else
    //  ShowMessage('OCS N�o encontrada');
    //dtmdados.IQuery1.Close;
  end;
end;

procedure TfrmOrdem_Coleta.edValorEnter(Sender: TObject);
begin
  edValor.clear;
end;

procedure TfrmOrdem_Coleta.edValorExit(Sender: TObject);
begin
  // pgTarefas.ActivePageIndex := 1;

  if (Alltrim(edValor.Text) = '') then
    Exit;

  if not mdTemp.Locate('coleta', edValor.Text, []) then
    ShowMessage('OCS n�o Encontrada !!');
end;

procedure TfrmOrdem_Coleta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QVeiculo.Close;
  QTarefa.Close;
  mdTemp.Close;
  QOCS.Close;
  QOCi.Close;
  QOCa.Close;
  QOCr.Close;
  QOc.Close;
  Action := caFree;
end;

procedure TfrmOrdem_Coleta.FormCreate(Sender: TObject);
begin
  bInicio := True;

  // Carrega as opera��es
  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.add
    ('select distinct operacao from (select dataf, operacao from (');
  dtmdados.IQuery1.SQL.add
    ('select distinct t.operacao, max(i.dataf) dataf');
  dtmdados.IQuery1.SQL.add
    ('from tb_custofrac_item i left join tb_custofrac t on i.cod_custo = t.cod_custo');
  dtmdados.IQuery1.SQL.add
    ('left join tb_operacao o on t.operacao = o.operacao');
  dtmdados.IQuery1.SQL.add
    ('where t.fl_status = ''S'' and o.fl_oc = ''S'' and o.fl_receita = ''N'' ');
  dtmdados.IQuery1.SQL.add
    ('group by t.operacao, t.operacao) where trunc(dataf) >= trunc(sysdate) ) ');
  dtmdados.IQuery1.SQL.add
    ('order by 1 ');
  dtmdados.IQuery1.Open;
  cbOperacao.clear;
  cbom.clear;
  cbOperacao.Items.add('');
  cbom.clear;
  cbom.Items.add('');
  while not dtmdados.IQuery1.eof do
  begin
    cbOperacao.Items.add
      (UpperCase(dtmdados.IQuery1.FieldByName('operacao').value));
    cbom.Items.add(UpperCase(dtmdados.IQuery1.FieldByName('operacao').value));
    dtmdados.IQuery1.Next;
  end;
  dtmdados.IQuery1.Close;
  pgTarefas.ActivePageIndex := 0;
  PageControl1.ActivePageIndex := 0;
  PageControl1.TabIndex := 0;
  QVeiculo.Open;
  edData.date := date - 10;
end;

procedure TfrmOrdem_Coleta.JvCalcEdit5Exit(Sender: TObject);
begin
  QM.Close;
  QM.Parameters[0].value := JvCalcEdit3.value;
  QM.Parameters[1].value := JvCalcEdit5.value;
  QM.Open;
  if not QM.eof then
    BitBtn3.Enabled := True
  else
    BitBtn3.Enabled := false;
end;

procedure TfrmOrdem_Coleta.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QOCaCODCON.value > 0 then
  begin
    JvDBUltimGrid1.Canvas.Brush.Color := clYellow;
    JvDBUltimGrid1.Canvas.Font.Color := clred;
  end;
  JvDBUltimGrid1.DefaultDrawDataCell(Rect, JvDBUltimGrid1.columns[DataCol]
    .Field, State);
end;

procedure TfrmOrdem_Coleta.LimpaCamposManifesto;
begin
  edTotPesoTarefa.value := 0;
  edQtdDestino.value := 0;
  edQtdDestino.Color := clSilver;
  edQtdCubagem.value := 0;
  edPercentCarga.value := 0;
  edCusto.value := 0;
  cbOperacao.ItemIndex := -1;
  QTransp.Close;
  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  cbPlaca_carreta.ItemIndex := -1;
  edVeiculo.clear;
  edPercentCarga.Color := clSilver;
end;

procedure TfrmOrdem_Coleta.mdTempAfterOpen(DataSet: TDataSet);
begin
  mdTemp.First;
end;

procedure TfrmOrdem_Coleta.QOCCBeforeOpen(DataSet: TDataSet);
begin
  QOCC.Parameters[0].value := glbFilial;
  QOCC.Parameters[1].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QOCiBeforeOpen(DataSet: TDataSet);
begin
  QOCi.Parameters[1].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QKMBeforeOpen(DataSet: TDataSet);
begin
  QKM.Parameters[0].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QOCaAfterScroll(DataSet: TDataSet);
begin
  QIt.Close;
  QIt.Parameters[0].value := QOCaID_ORDEM.AsInteger;
  QIt.Parameters[1].value := QOCaFILIAL.AsInteger;
  QIt.Open;
end;

procedure TfrmOrdem_Coleta.QOCaBeforeOpen(DataSet: TDataSet);
begin
  QOCa.Parameters[0].value := glbFilial;
  QOCa.Parameters[1].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QOcBeforeOpen(DataSet: TDataSet);
begin
  QOc.Parameters[0].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QOCrAfterScroll(DataSet: TDataSet);
begin
  QOCi.Close;
  QOCi.Parameters[0].value := QOCrID_ORDEM.AsInteger;
  QOCi.Parameters[1].value := QOCrFILIAL.AsInteger;
  QOCi.Open;
end;

procedure TfrmOrdem_Coleta.QOCrBeforeOpen(DataSet: TDataSet);
begin
  QOCr.Parameters[0].value := glbFilial;
end;

procedure TfrmOrdem_Coleta.QOCSBeforeOpen(DataSet: TDataSet);
begin
  QOCS.Parameters[0].value := glbFilial;
end;

function TfrmOrdem_Coleta.RetornaTarefasSelecionadas: String;
var
  sTarSelec: String;
  icount: Integer;
begin
  mdTemp.First;
  icount := 0;
  // lista tarefas marcadas
  mdTemp.DisableControls;
  if not(QTarefa.Active) then
    QTarefa.Open;
  // incluir tarefas j� existente
  QTarefa.First;
  while not QTarefa.eof do
  begin
    if (QTarefaNRO_OCS.AsInteger <> iRecDelete) then
    begin
      if icount = 0 then
        sTarSelec := QTarefaNRO_OCS.AsString
      else
        sTarSelec := sTarSelec + ',' + QTarefaNRO_OCS.AsString;
      inc(icount);
    end;
    QTarefa.Next;
  end;
  iRecDelete := 0;

  while not mdTemp.eof do
  begin
    if mdTempInserida.value = 1 then
    begin
      if icount = 0 then
        sTarSelec := mdTempColeta.AsString
      else
        sTarSelec := sTarSelec + ',' + mdTempColeta.AsString;
      inc(icount);
    end;
    mdTemp.Next;
  end;
  mdTemp.EnableControls;
  result := sTarSelec;
end;

procedure TfrmOrdem_Coleta.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel6.caption := 'Ordem de Coleta - Descarga N� : ' +
    QOCaID_ORDEM.AsString;
end;

procedure TfrmOrdem_Coleta.RLReport2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel19.caption := 'Ordem de Coleta - Descarga N� : ' +
    QOCrID_ORDEM.AsString;
end;

procedure TfrmOrdem_Coleta.TabSheet1Show(Sender: TObject);
begin
  pnManifesto.Enabled := false;

  edTotPesoTarefa.clear;
  edPesoLimiteVeiculo.clear;
  edPercentCarga.clear;
  edQtdCubagem.clear;
  edQtdDestino.clear;
  edQtdDestino.Color := clSilver;
end;

procedure TfrmOrdem_Coleta.TabSheet2Hide(Sender: TObject);
begin
  QOc.Close;
end;

procedure TfrmOrdem_Coleta.TabSheet2Show(Sender: TObject);
begin
  QOc.Open;
end;

procedure TfrmOrdem_Coleta.TabSheet4Hide(Sender: TObject);
begin
  QOCC.Close;
end;

procedure TfrmOrdem_Coleta.TabSheet4Show(Sender: TObject);
begin
  QOCC.Close;
  QOCC.Open;
end;

procedure TfrmOrdem_Coleta.tbOCSShow(Sender: TObject);
var
  starefa_1: String;
begin
  starefa_1 := RetornaTarefasSelecionadas();
  if (starefa_1 = '') then
    starefa_1 := '0';
  QTarefa.Close;
  QTarefa.SQL[2] := ' where v.codcon in (' + starefa_1 + ')' +
    ' and v.fl_empresa = ' + IntToStr(glbFilial);
  QTarefa.Open;
  mdTemp.First;
end;

procedure TfrmOrdem_Coleta.tbTarefaShow(Sender: TObject);
begin
  CarregaTarefas();
end;

procedure TfrmOrdem_Coleta.tsOrdemGShow(Sender: TObject);
begin
  QOCa.Close;
  QOCa.Open;
end;

end.
