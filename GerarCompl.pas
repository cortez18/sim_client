unit GerarCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvCombobox, JvCheckBox, JvEdit, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, ExtCtrls, ImgList, Buttons, DB,
  ADODB, JvExExtCtrls, JvRadioGroup, IniFiles, JvDBControls, DBCtrls, JvMemo,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls,
  pcnConversao, System.ImageList;

type
  TfrmGerarCompl = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label27: TLabel;
    lblEndPA: TLabel;
    Panel1: TPanel;
    Label30: TLabel;
    Label37: TLabel;
    JvEdit1: TJvEdit;
    edCTRC: TJvCalcEdit;
    edSerie: TJvEdit;
    btnGerar: TBitBtn;
    Panel5: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label40: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    JvDBCalcEdit1: TJvDBCalcEdit;
    JvDBCalcEdit2: TJvDBCalcEdit;
    JvDBCalcEdit3: TJvDBCalcEdit;
    JvDBCalcEdit4: TJvDBCalcEdit;
    JvDBCalcEdit5: TJvDBCalcEdit;
    JvDBCalcEdit6: TJvDBCalcEdit;
    JvDBCalcEdit7: TJvDBCalcEdit;
    JvDBCalcEdit8: TJvDBCalcEdit;
    JvDBCalcEdit9: TJvDBCalcEdit;
    JvDBCalcEdit10: TJvDBCalcEdit;
    JvDBCalcEdit11: TJvDBCalcEdit;
    JvDBCalcEdit12: TJvDBCalcEdit;
    JvDBCalcEdit13: TJvDBCalcEdit;
    JvDBCalcEdit14: TJvDBCalcEdit;
    JvDBCalcEdit15: TJvDBCalcEdit;
    JvDBCalcEdit16: TJvDBCalcEdit;
    JvDBCalcEdit17: TJvDBCalcEdit;
    JvDBCalcEdit18: TJvDBCalcEdit;
    JvDBCalcEdit19: TJvDBCalcEdit;
    JvDBCalcEdit20: TJvDBCalcEdit;
    JvDBCalcEdit21: TJvDBCalcEdit;
    JvDBCalcEdit22: TJvDBCalcEdit;
    edTotal: TJvCalcEdit;
    edTas: TJvCalcEdit;
    edSegBalsa: TJvCalcEdit;
    edoutros: TJvCalcEdit;
    edImposto: TJvCalcEdit;
    edAliquota: TJvCalcEdit;
    edpedagio: TJvCalcEdit;
    edBase: TJvCalcEdit;
    edSeguro: TJvCalcEdit;
    edGris: TJvCalcEdit;
    edRedFlu: TJvCalcEdit;
    edAgend: TJvCalcEdit;
    edPallet: TJvCalcEdit;
    edPorto: TJvCalcEdit;
    edAlfa: TJvCalcEdit;
    edCanhoto: TJvCalcEdit;
    edFrete: TJvCalcEdit;
    edFluvial: TJvCalcEdit;
    edExced: TJvCalcEdit;
    edTR: TJvCalcEdit;
    edDespacho: TJvCalcEdit;
    edTDE: TJvCalcEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label34: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel3: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label14: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit13: TDBEdit;
    JvMemo1: TJvMemo;
    iml: TImageList;
    Query1: TADOQuery;
    Query1NR_CNPJ_CPF: TStringField;
    Query1COD_CLIENTE: TIntegerField;
    Query1NM_CLIENTE: TStringField;
    Query1NM_FANTASIA: TStringField;
    Query1DS_CIDADE: TStringField;
    Query1DS_UF: TStringField;
    Query1DS_IE_RG: TStringField;
    QCTRC: TADOQuery;
    QCTRCCOD_CONHECIMENTO: TBCDField;
    QCTRCFL_EMPRESA: TBCDField;
    QCTRCCUFEMIT: TStringField;
    QCTRCCODUFORIGEM: TStringField;
    QCTRCUFORI: TStringField;
    QCTRCCODCTRC: TBCDField;
    QCTRCCTE_CHAVE: TStringField;
    QCTRCCFOP: TStringField;
    QCTRCNATOPERACAO: TStringField;
    QCTRCNR_SERIE: TStringField;
    QCTRCFORMAPAGTO: TStringField;
    QCTRCMODELOCTE: TStringField;
    QCTRCNUMEROCTE: TBCDField;
    QCTRCEMISSAOCTE: TDateTimeField;
    QCTRCTPIMP: TStringField;
    QCTRCTPEMIS: TStringField;
    QCTRCDIGVERFICHAVEACES: TStringField;
    QCTRCTPAMB: TStringField;
    QCTRCTPCTE: TStringField;
    QCTRCPROCEMI: TStringField;
    QCTRCVERPROC: TStringField;
    QCTRCCODMUNENVI: TStringField;
    QCTRCMUNENVI: TStringField;
    QCTRCUFENV: TStringField;
    QCTRCMODAL: TStringField;
    QCTRCTPSERV: TStringField;
    QCTRCCODMUNINI: TStringField;
    QCTRCMUNINI: TStringField;
    QCTRCUFINI: TStringField;
    QCTRCCMUNFIM: TStringField;
    QCTRCXMUNFIM: TStringField;
    QCTRCUFFIM: TStringField;
    QCTRCRETIRA: TStringField;
    QCTRCTOMA03: TStringField;
    QCTRCCNPJEMI: TStringField;
    QCTRCIEEMI: TStringField;
    QCTRCNOMEEMI: TStringField;
    QCTRCLGREMI: TStringField;
    QCTRCNROEMI: TBCDField;
    QCTRCBAIRROEMMI: TStringField;
    QCTRCCODMUNEMI: TStringField;
    QCTRCMUNEMI: TStringField;
    QCTRCCEPEMI: TStringField;
    QCTRCUFEMI: TStringField;
    QCTRCFONEEMI: TStringField;
    QCTRCCNPJTOM: TStringField;
    QCTRCIETOM: TStringField;
    QCTRCNOMETOM: TStringField;
    QCTRCFONETOM: TStringField;
    QCTRCLGRTOM: TStringField;
    QCTRCCOMPLTOM: TStringField;
    QCTRCNROTOM: TBCDField;
    QCTRCBAIRROTOM: TStringField;
    QCTRCCODMUNTOM: TStringField;
    QCTRCMUNTOM: TStringField;
    QCTRCCEPTOM: TStringField;
    QCTRCUFTOM: TStringField;
    QCTRCCODPAISTOM: TStringField;
    QCTRCNOMEPAISTOM: TStringField;
    QCTRCTIPOPESSOAREM: TStringField;
    QCTRCCNPJREM: TStringField;
    QCTRCIEREM: TStringField;
    QCTRCNOMEREM: TStringField;
    QCTRCFONEREM: TStringField;
    QCTRCLGRREM: TStringField;
    QCTRCCOMPLREM: TStringField;
    QCTRCNROREM: TBCDField;
    QCTRCBAIRROREM: TStringField;
    QCTRCCODMUNREM: TStringField;
    QCTRCMUNREM: TStringField;
    QCTRCCEPREM: TStringField;
    QCTRCUFREM: TStringField;
    QCTRCCODPAISREM: TStringField;
    QCTRCNOMEPAISREM: TStringField;
    QCTRCTIPOPESSOAEXP: TStringField;
    QCTRCCNPJEXP: TStringField;
    QCTRCIEEXP: TStringField;
    QCTRCNOMEEXP: TStringField;
    QCTRCFONEEXP: TStringField;
    QCTRCLGREXP: TStringField;
    QCTRCCOMPLEXP: TStringField;
    QCTRCNROEXP: TBCDField;
    QCTRCBAIRROEXP: TStringField;
    QCTRCCODMUNEXP: TStringField;
    QCTRCMUNEXP: TStringField;
    QCTRCCEPEXP: TStringField;
    QCTRCUFEXP: TStringField;
    QCTRCCODPAISEXP: TStringField;
    QCTRCNOMEPAISEXP: TStringField;
    QCTRCCNPJDES: TStringField;
    QCTRCTIPOPESSOA: TStringField;
    QCTRCIEDES: TStringField;
    QCTRCNOMEDES: TStringField;
    QCTRCFONEDES: TStringField;
    QCTRCLGRDES: TStringField;
    QCTRCCOMPLDES: TStringField;
    QCTRCNRODES: TBCDField;
    QCTRCBAIRRODES: TStringField;
    QCTRCCODMUNDES: TStringField;
    QCTRCMUNDES: TStringField;
    QCTRCCEPDES: TStringField;
    QCTRCUFDES: TStringField;
    QCTRCCODPAISDES: TStringField;
    QCTRCNOMEPAISDES: TStringField;
    QCTRCCNPJRED: TStringField;
    QCTRCIERED: TStringField;
    QCTRCNOMERED: TStringField;
    QCTRCFONERED: TStringField;
    QCTRCLGRRED: TStringField;
    QCTRCCOMPLRED: TStringField;
    QCTRCNRORED: TBCDField;
    QCTRCBAIRRORED: TStringField;
    QCTRCCODMUNRED: TStringField;
    QCTRCMUNRED: TStringField;
    QCTRCCEPRED: TStringField;
    QCTRCUFRED: TStringField;
    QCTRCCODPAISRED: TStringField;
    QCTRCNOMEPAISRED: TStringField;
    QCTRCVLTOTPRESTACAO: TFloatField;
    QCTRCVLRECEBPRESTACAO: TFloatField;
    QCTRCCOMPDESPACHO: TStringField;
    QCTRCCOMPDESPACHOVALOR: TFloatField;
    QCTRCCOMPGRIS: TStringField;
    QCTRCCOMPGRISVALOR: TFloatField;
    QCTRCCOMPADVALOREN: TStringField;
    QCTRCCOMPADVALORENVALOR: TFloatField;
    QCTRCCOMPPEDAGIO: TStringField;
    QCTRCCOMPPEDAGIOVALOR: TFloatField;
    QCTRCCOMPOUTRASTAXAS: TStringField;
    QCTRCCOMPOUTROSTAXASVALOR: TFloatField;
    QCTRCCOMPFLUVIAL: TStringField;
    QCTRCCOMPFLUVIALVALOR: TFloatField;
    QCTRCCOMPTR: TStringField;
    QCTRCCOMPTRVALOR: TFloatField;
    QCTRCCOMPTDE: TStringField;
    QCTRCCOMPTDEVALOR: TFloatField;
    QCTRCCOMPEXED: TStringField;
    QCTRCCOMPEXEDVALOR: TFloatField;
    QCTRCCOMPBALSA: TStringField;
    QCTRCCOMPBALSAVALOR: TFloatField;
    QCTRCCOMPREDFLU: TStringField;
    QCTRCCOMPREDFLUVALOR: TFloatField;
    QCTRCCOMPAGEND: TStringField;
    QCTRCCOMPAGENDVALOR: TFloatField;
    QCTRCCOMPPALLET: TStringField;
    QCTRCCOMPPALLETVALOR: TFloatField;
    QCTRCCOMPPORTO: TStringField;
    QCTRCCOMPPORTOVALOR: TFloatField;
    QCTRCCOMPALFA: TStringField;
    QCTRCCOMPALFAVALOR: TFloatField;
    QCTRCCOMPCANHOTO: TStringField;
    QCTRCCOMPCANHOTOVALOR: TFloatField;
    QCTRCCOMPREE: TStringField;
    QCTRCCOMPREEVALOR: TFloatField;
    QCTRCCOMPDEV: TStringField;
    QCTRCCOMPDEVVALOR: TFloatField;
    QCTRCCOMPTAS: TStringField;
    QCTRCCOMPTASVALOR: TFloatField;
    QCTRCCSTIMPOSTO: TStringField;
    QCTRCDESCCSTIMPOSTO: TStringField;
    QCTRCBASEIMPOSTO: TFloatField;
    QCTRCALIQIMPOSTO: TBCDField;
    QCTRCVALORIMMPOSTO: TBCDField;
    QCTRCVALORCARGA: TFloatField;
    QCTRCDESCRICAOCARGA: TStringField;
    QCTRCCODIGOUNIDADE: TStringField;
    QCTRCTIPOMEDIDA: TStringField;
    QCTRCQTDMEDIDA: TFloatField;
    QCTRCRNTRC: TStringField;
    QCTRCDPREV: TDateTimeField;
    QCTRCLOTA: TStringField;
    QCTRCPESOTOTAL: TFloatField;
    QCTRCCUBAGEM: TFloatField;
    QCTRCPESOCALCULO: TBCDField;
    QCTRCDS_TIPO_FRETE: TStringField;
    QCTRCOBSERVACAO: TStringField;
    QCTRCTIPO: TStringField;
    QCTRCFORMAPAGAMENTO: TStringField;
    QCTRCTOMADOR: TStringField;
    QCTRCPROTOCOLO: TStringField;
    QCTRCFL_CONTINGENCIA: TStringField;
    QCTRCUFORICALC: TStringField;
    QCTRCCIDORICALC: TStringField;
    QCTRCUFDESCALC: TStringField;
    QCTRCCIDDESCALC: TStringField;
    QCTRCMOTORISTA: TStringField;
    QCTRCCPF_MOT: TStringField;
    QCTRCNM_USUARIO: TStringField;
    QCTRCSEGURADORA: TStringField;
    QCTRCAPOLICE: TStringField;
    QCTRCRESPSEG: TBCDField;
    QCTRCFL_STATUS: TStringField;
    QCTRCPLACA: TStringField;
    QCTRCRENAVAM: TStringField;
    QCTRCLOTACAO: TStringField;
    QCTRCUFVEI: TStringField;
    QCTRCCTE_CHAVE_CTE_R: TStringField;
    QCTRCCTE_PROT: TStringField;
    QCTRCTP_VEI: TStringField;
    QCTRCCOMPFRETE_PESO: TStringField;
    QCTRCCOMPFRETE_PESOVALOR: TFloatField;
    QCTRCCOMPTXCTE: TStringField;
    QCTRCCOMPTXCTEVALOR: TFloatField;
    QCTRCCOMPVL_FRETE: TStringField;
    QCTRCCOMPVL_FRETEVALOR: TFloatField;
    Query2: TADOQuery;
    SpGerarCtrcComplementar: TADOStoredProc;
    dtsCte: TDataSource;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    btnSefaz: TBitBtn;
    btnStatus: TBitBtn;
    dgCtrc: TJvDBUltimGrid;
    MStat: TJvMemo;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    QGerados: TADOQuery;
    dtsGerados: TDataSource;
    QGeradosCOD_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO_COMP: TBCDField;
    QGeradosDT_CONHECIMENTO: TDateTimeField;
    QGeradosDT_CONHECIMENTO_COMP: TDateTimeField;
    QGeradosCOD_PAGADOR: TBCDField;
    QGeradosCOD_REMETENTE: TBCDField;
    QGeradosCOD_DESTINATARIO: TBCDField;
    QGeradosCOD_REDESPACHO: TBCDField;
    QGeradosVL_DECLARADO_NF: TFloatField;
    QGeradosNR_QTDE_VOL: TFloatField;
    QGeradosNR_PESO_KG: TFloatField;
    QGeradosNR_PESO_CUB: TFloatField;
    QGeradosVL_FRETE_PESO: TFloatField;
    QGeradosVL_AD: TFloatField;
    QGeradosVL_GRIS: TFloatField;
    QGeradosVL_TR: TFloatField;
    QGeradosVL_TDE: TFloatField;
    QGeradosVL_OUTROS: TFloatField;
    QGeradosVL_PEDAGIO: TFloatField;
    QGeradosVL_FLUVIAL: TFloatField;
    QGeradosVL_EXCED: TFloatField;
    QGeradosVL_DESPACHO: TFloatField;
    QGeradosVL_BALSA: TFloatField;
    QGeradosVL_REDFLU: TFloatField;
    QGeradosVL_AGEND: TFloatField;
    QGeradosVL_PALLET: TFloatField;
    QGeradosVL_PORTO: TFloatField;
    QGeradosVL_ALFAND: TFloatField;
    QGeradosVL_CANHOTO: TFloatField;
    QGeradosVL_REENT: TFloatField;
    QGeradosVL_DEVOL: TFloatField;
    QGeradosVL_BASE: TFloatField;
    QGeradosVL_ALIQUOTA: TFloatField;
    QGeradosVL_IMPOSTO: TFloatField;
    QGeradosVL_TOTAL: TFloatField;
    QGeradosNM_USUARIO: TStringField;
    QGeradosOPERACAO: TBCDField;
    QGeradosDS_TIPO_MERC: TStringField;
    QGeradosDS_TIPO_FRETE: TStringField;
    QGeradosDS_OBS: TStringField;
    QGeradosDS_PLACA: TStringField;
    QGeradosMOTORISTA: TBCDField;
    QGeradosFL_STATUS: TStringField;
    QGeradosFL_TIPO: TStringField;
    QGeradosCODMUNO: TBCDField;
    QGeradosCODMUND: TBCDField;
    QGeradosCFOP: TStringField;
    QGeradosNR_FATURA: TBCDField;
    QGeradosNR_TABELA: TBCDField;
    QGeradosPEDIDO: TStringField;
    QGeradosNR_MANIFESTO: TBCDField;
    QGeradosFL_EMPRESA: TBCDField;
    QGeradosNR_RPS: TBCDField;
    QGeradosDS_CANCELAMENTO: TStringField;
    QGeradosNR_SERIE: TStringField;
    QGeradosVL_IMPOSTO_GNRE: TFloatField;
    QGeradosIMP_DACTE: TStringField;
    QGeradosFL_CONTINGENCIA: TStringField;
    QGeradosFRETE_IMPOSTO: TFloatField;
    QGeradosSEGURO_IMPOSTO: TFloatField;
    QGeradosGRIS_IMPOSTO: TFloatField;
    QGeradosOUTRAS_IMPOSTO: TFloatField;
    QGeradosFLUVIAL_IMPOSTO: TFloatField;
    QGeradosPEDAGIO_IMPOSTO: TFloatField;
    QGeradosFL_TIPO_2: TStringField;
    QGeradosCTE_DATA: TDateTimeField;
    QGeradosCTE_CHAVE: TStringField;
    QGeradosCTE_PROT: TStringField;
    QGeradosCTE_XMOTIVO: TStringField;
    QGeradosCTE_RECIBO: TStringField;
    QGeradosCTE_STATUS: TBCDField;
    QGeradosCTE_XMSG: TStringField;
    QGeradosFL_DDR: TStringField;
    QGeradosCTE_CAN_PROT: TStringField;
    QGeradosCTE_CHAVE_CTE_R: TStringField;
    QGeradosVL_TAS: TFloatField;
    QGeradosCTE_DATA_CAN: TDateTimeField;
    QGeradosNR_LOTE: TBCDField;
    QGeradosVL_TXT_CTE: TFloatField;
    QGeradosVL_FRETE: TFloatField;
    QGeradosNR_CTE_ORIGEM: TBCDField;
    QGeradosOBS_TRANSPARENCIA: TStringField;
    QGeradosXML_VL_FRETE: TBCDField;
    QGeradosXML_VL_PEDAGIO: TBCDField;
    QGeradosXML_VL_OUTROS: TBCDField;
    QGeradosXML_VL_EXCEDENTE: TBCDField;
    QGeradosXML_VL_AD: TBCDField;
    QGeradosXML_VL_GRIS: TBCDField;
    QGeradosXML_VL_ENTREGA: TBCDField;
    QGeradosXML_VL_SEGURO: TBCDField;
    QGeradosXML_VL_TDE: TBCDField;
    QGeradosXML_VL_TR: TBCDField;
    QGeradosXML_VL_SEG_BALSA: TBCDField;
    QGeradosXML_VL_REDEP_FLUVIAL: TBCDField;
    QGeradosXML_VL_AGEND: TBCDField;
    QGeradosXML_VL_PALLET: TBCDField;
    QGeradosXML_VL_TAS: TBCDField;
    QGeradosXML_VL_DESPACHO: TBCDField;
    QGeradosXML_VL_ENTREGA_PORTO: TBCDField;
    QGeradosXML_VL_ALFAND: TBCDField;
    QGeradosXML_VL_CANHOTO: TBCDField;
    QGeradosXML_VL_FLUVIAL: TBCDField;
    QGeradosXML_VL_DEVOLUCAO: TBCDField;
    QGeradosXML_VL_AJUDA: TBCDField;
    QGeradosXML_VL_TX_CTE: TBCDField;
    QGeradosNF_VL_FRETE: TBCDField;
    QGeradosNF_VL_PEDAGIO: TBCDField;
    QGeradosNF_VL_EXCEDENTE: TBCDField;
    QGeradosNF_VL_AD: TBCDField;
    QGeradosNF_VL_GRIS: TBCDField;
    QGeradosNF_VL_ENTREGA: TBCDField;
    QGeradosNF_VL_SEGURO: TBCDField;
    QGeradosNF_VL_TDE: TBCDField;
    QGeradosNF_VL_TR: TBCDField;
    QGeradosNF_VL_SEG_BALSA: TBCDField;
    QGeradosNF_VL_REDEP_FLUVIAL: TBCDField;
    QGeradosNF_VL_AGEND: TBCDField;
    QGeradosNF_VL_PALLET: TBCDField;
    QGeradosNF_VL_TAS: TBCDField;
    QGeradosNF_VL_DESPACHO: TBCDField;
    QGeradosNF_VL_ENTREGA_PORTO: TBCDField;
    QGeradosNF_VL_ALFAND: TBCDField;
    QGeradosNF_VL_CANHOTO: TBCDField;
    QGeradosNF_VL_FLUVIAL: TBCDField;
    QGeradosNF_VL_DEVOLUCAO: TBCDField;
    QGeradosNF_VL_AJUDAR: TBCDField;
    QGeradosNF_VL_CTE: TBCDField;
    QGeradosNF_NR: TBCDField;
    QGeradosNF_VALORTT: TBCDField;
    QGeradosEMAIL_ENVIADO: TStringField;
    QGeradosLOTE_SEFAZ: TStringField;
    QGeradosNR_ROMANEIO: TBCDField;
    QGeradosCOD_EXPEDIDOR: TBCDField;
    QGeradosSERIE_MAN: TStringField;
    QGeradosFILIAL_MAN: TBCDField;
    QGeradosCOD_CST: TStringField;
    QGeradosCLIENTE: TStringField;
    QGeradosNM_ORIGEM: TStringField;
    QGeradosNM_DESTINO: TStringField;
    cbCusto: TCheckBox;
    Label13: TLabel;
    edCteIni: TJvCalcEdit;
    Label15: TLabel;
    edCteFim: TJvCalcEdit;
    btnLimpar_lote: TBitBtn;
    Label16: TLabel;
    cbServico: TComboBox;
    QServico: TADOQuery;
    QServicoDESCR_SERVICO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure edFreteChange(Sender: TObject);
    procedure somavalores;
    procedure btnGerarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvEdit1Exit(Sender: TObject);
    procedure QCTRCAfterOpen(DataSet: TDataSet);
    procedure edSerieExit(Sender: TObject);
    procedure edoutrosChange(Sender: TObject);
    procedure edpedagioChange(Sender: TObject);
    procedure edFluvialChange(Sender: TObject);
    procedure edExcedChange(Sender: TObject);
    procedure edTRChange(Sender: TObject);
    procedure edDespachoChange(Sender: TObject);
    procedure edTDEChange(Sender: TObject);
    procedure edGrisChange(Sender: TObject);
    procedure edSegBalsaChange(Sender: TObject);
    procedure edRedFluChange(Sender: TObject);
    procedure edAgendChange(Sender: TObject);
    procedure edPalletChange(Sender: TObject);
    procedure edPortoChange(Sender: TObject);
    procedure edAlfaChange(Sender: TObject);
    procedure edCanhotoChange(Sender: TObject);
    procedure edTasChange(Sender: TObject);
    procedure edSeguroChange(Sender: TObject);
    procedure edBaseChange(Sender: TObject);
    procedure edFreteExit(Sender: TObject);
    procedure edpedagioExit(Sender: TObject);
    procedure edoutrosExit(Sender: TObject);
    procedure edFluvialExit(Sender: TObject);
    procedure edExcedExit(Sender: TObject);
    procedure edTRExit(Sender: TObject);
    procedure edDespachoExit(Sender: TObject);
    procedure edTDEExit(Sender: TObject);
    procedure edGrisExit(Sender: TObject);
    procedure edSegBalsaExit(Sender: TObject);
    procedure edRedFluExit(Sender: TObject);
    procedure edAgendExit(Sender: TObject);
    procedure edPalletExit(Sender: TObject);
    procedure edPortoExit(Sender: TObject);
    procedure edAlfaExit(Sender: TObject);
    procedure edCanhotoExit(Sender: TObject);
    procedure edTasExit(Sender: TObject);
    procedure edSeguroExit(Sender: TObject);
    procedure btnSefazClick(Sender: TObject);
    procedure QGeradosBeforeOpen(DataSet: TDataSet);
    procedure btnStatusClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure btnLimpar_loteClick(Sender: TObject);
    procedure edCteIniExit(Sender: TObject);
    procedure edCteFimExit(Sender: TObject);

  private
  var
    new: Integer;
  public
    { Public declarations }
  end;

var
  frmGerarCompl: TfrmGerarCompl;
  codExped, codDest, codCon, codtab, codcliente: Integer;
  ufExp, ufDest, ufCon, cidExp, cidDest, cidCon, vcnpj, gerado, vcnpjdes,
    vcnpjcon, cnpjcliente: string;
  exp, dest, cons, anoos, st, nmcfop, imposto, isento, fl_tipo: string;

implementation

uses Dados, funcoes, Menu, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmGerarCompl.btnGerarClick(Sender: TObject);
var
  novo: Integer;
begin
  if edAliquota.value = 0 then
  begin
    ShowMessage('O Valor da Aliquota est� Zero ?');
    edAliquota.SetFocus;
    exit;
  end;

  if edTotal.value = 0 then
  begin
    ShowMessage('O Valor do Total est� Zero ?');
    exit;
  end;

  if cbServico.Text = '' then
  begin
    ShowMessage('Precisa Escolher um Tipo de Servi�o');
    cbServico.SetFocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Gerar Este CT-e?', 'Gerar CT-e',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    btnGerar.Enabled := false;
    SpGerarCtrcComplementar.Parameters[1].value := QCTRCNUMEROCTE.AsInteger;
    SpGerarCtrcComplementar.Parameters[2].value := QCTRCNR_SERIE.AsString;
    SpGerarCtrcComplementar.Parameters[3].value := edFrete.value;
    SpGerarCtrcComplementar.Parameters[4].value := edpedagio.value;
    SpGerarCtrcComplementar.Parameters[5].value := edoutros.value;
    SpGerarCtrcComplementar.Parameters[6].value := edFluvial.value;
    SpGerarCtrcComplementar.Parameters[7].value := edExced.value;
    SpGerarCtrcComplementar.Parameters[8].value := edTR.value;
    SpGerarCtrcComplementar.Parameters[9].value := edDespacho.value;
    SpGerarCtrcComplementar.Parameters[10].value := edTDE.value;
    SpGerarCtrcComplementar.Parameters[11].value := edTas.value;
    SpGerarCtrcComplementar.Parameters[12].value := edSeguro.value;
    SpGerarCtrcComplementar.Parameters[13].value := edGris.value;
    SpGerarCtrcComplementar.Parameters[14].value := edSegBalsa.value;
    SpGerarCtrcComplementar.Parameters[15].value := edRedFlu.value;
    SpGerarCtrcComplementar.Parameters[16].value := edAgend.value;
    SpGerarCtrcComplementar.Parameters[17].value := edPallet.value;
    SpGerarCtrcComplementar.Parameters[18].value := edPorto.value;
    SpGerarCtrcComplementar.Parameters[19].value := edAlfa.value;
    SpGerarCtrcComplementar.Parameters[20].value := edCanhoto.value;
    SpGerarCtrcComplementar.Parameters[21].value := edBase.value;
    SpGerarCtrcComplementar.Parameters[22].value := edAliquota.value;
    SpGerarCtrcComplementar.Parameters[23].value := edImposto.value;
    SpGerarCtrcComplementar.Parameters[24].value := edTotal.value;
    SpGerarCtrcComplementar.Parameters[25].value := QCTRCFL_EMPRESA.AsInteger;
    SpGerarCtrcComplementar.Parameters[26].value := alltrim(JvMemo1.Text);
    SpGerarCtrcComplementar.Parameters[27].value := GLBUSER;
    if cbCusto.Checked = true then
      SpGerarCtrcComplementar.Parameters[28].value := 'X'
    else
      SpGerarCtrcComplementar.Parameters[28].value := '';
    SpGerarCtrcComplementar.Parameters[29].value := cbServico.Text;
    SpGerarCtrcComplementar.ExecProc;
    novo := SpGerarCtrcComplementar.Parameters[0].value;
    ShowMessage('CT-e ' + IntToStr(novo) + ' Gerado com Sucesso');
    // GravaLog(name,IntToStr(novo),'Gerar CTRC Complementar');
    frmGerarCompl.JvEdit1.Text := retzero(IntToStr(novo), 6);
    frmGerarCompl.JvEdit1Exit(Sender);

  end;
  PageControl1.ActivePageIndex := 1;

end;

procedure TfrmGerarCompl.btnLimpar_loteClick(Sender: TObject);
begin
  btnLimpar_lote.Enabled := true;
  btnStatus.Enabled := true;
  edCteIni.value := 0;
  edCteFim.value := 0;
  QGerados.close;
  QGerados.SQL[6] := '';
  QGerados.SQL[7] := '';
  QGerados.open;
end;

procedure TfrmGerarCompl.btnSefazClick(Sender: TObject);
var
  reg: Integer;
  sData, lote: String;
begin
  reg := 0;
  if QGeradosFL_TIPO.value = 'C' then
  begin
    if (QGeradosCTE_STATUS.value = 204)
      or (QGeradosCTE_STATUS.value = 609) then
    begin
      frmMenu.ACBrCTe1.WebServices.Consulta.CTeChave :=
        QGeradosCTE_CHAVE.AsString;
      frmMenu.ACBrCTe1.WebServices.Consulta.Executar;
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.clear;
      dtmdados.iQuery1.SQL.add('update tb_conhecimento set cte_prot = :0, ');
      dtmdados.iQuery1.SQL.add
        ('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
      dtmdados.iQuery1.SQL.add('cte_xmsg = ''Autorizado o uso do CT-e'' ');
      dtmdados.iQuery1.SQL.add('where cte_chave = :1 ');
      dtmdados.iQuery1.Parameters[0].value :=
        frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
      dtmdados.iQuery1.Parameters[1].value := QGeradosCTE_CHAVE.AsString;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;

      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.clear;
      dtmdados.iQuery1.SQL.add('update cyber.rodcon set ctepro = :0 ');
      dtmdados.iQuery1.SQL.add('where cte_id = :1 ');
      dtmdados.iQuery1.Parameters[0].value :=
        frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
      dtmdados.iQuery1.Parameters[1].value := QGeradosCTE_CHAVE.AsString;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end
    else
    begin
      lote := '';
      // por lote
      if (edCteIni.value > 0) and (edCteFim.value > 0) then
      begin
        lote := 'S';
        QGerados.close;
        QGerados.SQL[6] := 'And nr_conhecimento between ' + #39 +
          IntToStr(edCteIni.AsInteger) + #39 + ' and ' + #39 +
          IntToStr(edCteFim.AsInteger) + #39;
        // para simular envio por lote
        if GLBUSER = 'PCORTEZ' then
          QGerados.SQL[7] := ''
        else
          QGerados.SQL[7] := 'And r.nm_usuario = ' + QuotedStr(GLBUSER);
        QGerados.open;
        if Application.Messagebox('Enviar para o Sefaz agora ?', 'Enviar Sefaz',
          MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
          sData := GLBUSER + FormatDateTime('dd/mm/yy - hh:mm:ss', now);
          while not QGerados.eof do
          begin
            reg := QGeradosCOD_CONHECIMENTO.AsInteger;
            btnSefaz.Enabled := false;
            frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
            btnSefaz.Enabled := true;

            dtmdados.IQuery1.close;
            dtmdados.IQuery1.SQL.clear;
            dtmdados.IQuery1.SQL.Add
              ('update tb_conhecimento set lote_sefaz = :0 ');
            dtmdados.IQuery1.SQL.Add('where cod_conhecimento = :1 ');
            dtmdados.IQuery1.Parameters[0].value := sData;
            dtmdados.IQuery1.Parameters[1].value := QGeradosCOD_CONHECIMENTO.AsInteger;
            dtmdados.IQuery1.ExecSQL;
            dtmdados.IQuery1.close;

            QGerados.next;
          end;
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select cod_conhecimento from tb_conhecimento ');
          dtmdados.IQuery1.SQL.Add('where lote_sefaz = :0 ');
          dtmdados.IQuery1.Parameters[0].value := sData;
          dtmdados.IQuery1.open;
          while not dtmdados.IQuery1.eof do
          begin
            frmMenu.QrCteEletronico.close;
            frmMenu.QrCteEletronico.Parameters[0].value :=
              dtmdados.IQuery1.FieldByName('COD_CONHECIMENTO').AsInteger;
            frmMenu.QrCteEletronico.open;
            frmMenu.QNF.close;
            frmMenu.QNF.Parameters[0].value :=
              frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
            frmMenu.QNF.Parameters[1].value :=
              frmMenu.qrCteEletronicoNR_SERIE.value;
            frmMenu.QNF.Parameters[2].value :=
              frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
            frmMenu.QNF.open;
            try
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := false;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := true;
            finally
              //frmDacte_Cte_2.Free;
            end;
            dtmdados.IQuery1.next;
          end;
        end;
      end
      else
      // POR CT-E
      begin
        begin
          reg := QGeradosCOD_CONHECIMENTO.AsInteger;
          frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
        end;
        try
          frmMenu.qrCteEletronico.close;
          frmMenu.qrCteEletronico.Parameters[0].value :=
            QGeradosCOD_CONHECIMENTO.AsString;
          frmMenu.qrCteEletronico.open;

          frmMenu.QNF.close;
          frmMenu.QNF.Parameters[0].value :=
            frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
          frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.value;
          frmMenu.QNF.Parameters[2].value :=
            frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
          frmMenu.QNF.open;

          frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
          frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
        finally
          //frmDacte_Cte_2.Free;
        end;
      end;
      QGerados.close;
      QGerados.open;
      QGerados.Locate('cod_conhecimento', reg, []);
    end;
  end;
end;

procedure TfrmGerarCompl.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrCTe1.WebServices.StatusServico.Executar;
  MStat.Lines.Text :=
    UTF8Encode(frmMenu.ACBrCTe1.WebServices.StatusServico.RetWS);
  MStat.Visible := true;
  MStat.clear;
  MStat.Lines.add('Status Servi�o');
  MStat.Lines.add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.tpAmb));
  MStat.Lines.add('verAplic: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.verAplic);
  MStat.Lines.add('cStat: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cStat));
  MStat.Lines.add('xMotivo: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.xMotivo);
  MStat.Lines.add('cUF: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cUF));
  MStat.Lines.add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRecbto));
  MStat.Lines.add('tMed: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.TMed));
  MStat.Lines.add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRetorno));
  MStat.Lines.add('xObs: ' + frmMenu.ACBrCTe1.WebServices.StatusServico.xObs);
end;

procedure TfrmGerarCompl.edAgendChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edAgendExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edAlfaChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edAlfaExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edBaseChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edCanhotoChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edCanhotoExit(Sender: TObject);
begin
  if (edCanhoto.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edCteFimExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteFim.value > 0 then
  begin
    if edCteFim.value < edCteIni.value then
    begin
      ShowMessage('O CT-e Final n�o pode ser menor que o Inicial');
      edCteFim.SetFocus;
      Exit;
    end;
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteFim.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteFim.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGerarCompl.edCteIniExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteIni.value > 0 then
  begin
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteIni.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteIni.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGerarCompl.edDespachoChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edDespachoExit(Sender: TObject);
begin
  if (edDespacho.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edExcedChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edExcedExit(Sender: TObject);
begin
  if (edExced.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edFluvialChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edFluvialExit(Sender: TObject);
begin
  if (edFluvial.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edFreteChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edFreteExit(Sender: TObject);
begin
  if (edFrete.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edGrisChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edGrisExit(Sender: TObject);
begin
  if (edGris.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edoutrosChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edoutrosExit(Sender: TObject);
begin
  if (edoutros.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edPalletChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edPalletExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edpedagioChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edpedagioExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edPortoChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edPortoExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edRedFluChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edRedFluExit(Sender: TObject);
begin
  if (edRedFlu.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edSegBalsaChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edSegBalsaExit(Sender: TObject);
begin
  if (edSegBalsa.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edSeguroChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edSeguroExit(Sender: TObject);
begin
  if (edSeguro.Text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCompl.edSerieExit(Sender: TObject);
begin
  if edCTRC.value > 0 then
  begin
    QCTRC.close;
    QCTRC.Parameters[0].value := edCTRC.value;
    QCTRC.Parameters[1].value := edSerie.Text;
    QCTRC.Parameters[2].value := GLBFilial;
    QCTRC.open;
    if QCTRC.IsEmpty then
    begin
      ShowMessage('CT-e n�o encontrado !!');
      edCTRC.SetFocus;
      exit;
    end;

    // btnImprimir.Enabled := true;
    // btnVisualizar.Enabled := true;
    btnGerar.Enabled := true;
    JvMemo1.clear;
    JvMemo1.Lines.add('Complemento do CT-e :' + edCTRC.Text);
    edAliquota.value := QCTRCALIQIMPOSTO.value;
    edFrete.SetFocus;
  end;
end;

procedure TfrmGerarCompl.edTasChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edTasExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edTDEChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edTDEExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.edTRChange(Sender: TObject);
begin
  // somavalores;
end;

procedure TfrmGerarCompl.edTRExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCompl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Query1.close;
  QCTRC.close;
  Query2.close;
end;

procedure TfrmGerarCompl.FormCreate(Sender: TObject);
begin
  self.caption := 'Gerar Conhecimento de Transportes Complementar - ' +
    glbnmfilial;
  imposto := 'S';
  QServico.Close;
  QServico.Open;
  cbServico.clear;
  cbServico.Items.add('');
  while not QServico.eof do
  begin
    cbServico.Items.add
      (UpperCase(QServicoDESCR_SERVICO.AsString));
    QServico.next;
  end;
  QServico.close;
  PageControl1.TabIndex := 0;
end;

procedure TfrmGerarCompl.JvEdit1Exit(Sender: TObject);
begin
  if JvEdit1.Text <> '' then
  begin
    QCTRC.close;
    QCTRC.Parameters[0].value := StrToInt(JvEdit1.Text);
    QCTRC.Parameters[1].value := edSerie.Text;
    QCTRC.Parameters[2].value := GLBFilial;
    QCTRC.open;
    if QCTRC.IsEmpty then
    begin
      ShowMessage('CT-e n�o encontrado !!');
      edCTRC.SetFocus;
      exit;
    end;
    gerado := 'S';
    new := QCTRCCOD_CONHECIMENTO.AsInteger;
  end;
end;

procedure TfrmGerarCompl.somavalores;
begin

  edTotal.Text := '';
  edBase.Text := '';
  edImposto.Text := '';

  if imposto = 'S' then
  begin
    edTotal.value := edFrete.value + edSeguro.value + edGris.value +
      edoutros.value + edFluvial.value + edExced.value + edImposto.value +
      edpedagio.value + edTR.value + edTDE.value + edDespacho.value +
      edSegBalsa.value + edRedFlu.value + edAgend.value + edPallet.value +
      edPorto.value + edAlfa.value + edCanhoto.value + edTas.value;
    edBase.value := edTotal.value;
  end
  else
  begin
    edTotal.value := edFrete.value + edSeguro.value + edGris.value +
      edoutros.value + edFluvial.value + edExced.value + edpedagio.value +
      edTR.value + edTDE.value + edDespacho.value + edSegBalsa.value +
      edRedFlu.value + edAgend.value + edPallet.value + edPorto.value +
      edAlfa.value + edCanhoto.value + edTas.value;
    edBase.value := edTotal.value;
  end;

  edImposto.value := (edBase.value / ((100 - edAliquota.value) / 100) *
    (edAliquota.value / 100));

  edBase.value := edBase.value + edImposto.value;
  edTotal.value := edBase.value;
end;

procedure TfrmGerarCompl.TabSheet2Hide(Sender: TObject);
begin
  QGerados.close;
end;

procedure TfrmGerarCompl.TabSheet2Show(Sender: TObject);
begin
  QGerados.open;
end;

procedure TfrmGerarCompl.QCTRCAfterOpen(DataSet: TDataSet);
begin
  if QCTRCCOMPFRETE_PESOVALOR.value > 0 then
    JvDBCalcEdit1.Color := $0080FFFF
  else
    JvDBCalcEdit1.Color := clWhite;
  if QCTRCCOMPPEDAGIOVALOR.value > 0 then
    JvDBCalcEdit2.Color := $0080FFFF
  else
    JvDBCalcEdit2.Color := clWhite;
  if QCTRCCOMPOUTROSTAXASVALOR.value > 0 then
    JvDBCalcEdit3.Color := $0080FFFF
  else
    JvDBCalcEdit3.Color := clWhite;
  if QCTRCCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit4.Color := $0080FFFF
  else
    JvDBCalcEdit4.Color := clWhite;
  if QCTRCCOMPEXEDVALOR.value > 0 then
    JvDBCalcEdit5.Color := $0080FFFF
  else
    JvDBCalcEdit5.Color := clWhite;
  if QCTRCCOMPTRVALOR.value > 0 then
    JvDBCalcEdit6.Color := $0080FFFF
  else
    JvDBCalcEdit6.Color := clWhite;
  if QCTRCCOMPDESPACHOVALOR.value > 0 then
    JvDBCalcEdit7.Color := $0080FFFF
  else
    JvDBCalcEdit7.Color := clWhite;
  if QCTRCCOMPTDEVALOR.value > 0 then
    JvDBCalcEdit8.Color := $0080FFFF
  else
    JvDBCalcEdit8.Color := clWhite;
  if QCTRCCOMPGRISVALOR.value > 0 then
    JvDBCalcEdit9.Color := $0080FFFF
  else
    JvDBCalcEdit9.Color := clWhite;
  if QCTRCCOMPBALSAVALOR.value > 0 then
    JvDBCalcEdit10.Color := $0080FFFF
  else
    JvDBCalcEdit10.Color := clWhite;
  if QCTRCCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit11.Color := $0080FFFF
  else
    JvDBCalcEdit11.Color := clWhite;
  if QCTRCCOMPAGENDVALOR.value > 0 then
    JvDBCalcEdit12.Color := $0080FFFF
  else
    JvDBCalcEdit12.Color := clWhite;
  if QCTRCCOMPPALLETVALOR.value > 0 then
    JvDBCalcEdit13.Color := $0080FFFF
  else
    JvDBCalcEdit13.Color := clWhite;
  if QCTRCCOMPPORTOVALOR.value > 0 then
    JvDBCalcEdit14.Color := $0080FFFF
  else
    JvDBCalcEdit14.Color := clWhite;
  if QCTRCCOMPALFAVALOR.value > 0 then
    JvDBCalcEdit15.Color := $0080FFFF
  else
    JvDBCalcEdit15.Color := clWhite;
  if QCTRCCOMPCANHOTOVALOR.value > 0 then
    JvDBCalcEdit16.Color := $0080FFFF
  else
    JvDBCalcEdit16.Color := clWhite;
  if QCTRCCOMPTASVALOR.value > 0 then
    JvDBCalcEdit17.Color := $0080FFFF
  else
    JvDBCalcEdit17.Color := clWhite;
  if QCTRCCOMPADVALORENVALOR.value > 0 then
    JvDBCalcEdit18.Color := $0080FFFF
  else
    JvDBCalcEdit18.Color := clWhite;

  if QCTRCBASEIMPOSTO.value > 0 then
    JvDBCalcEdit19.Color := $0080FFFF
  else
    JvDBCalcEdit19.Color := clWhite;
  if QCTRCALIQIMPOSTO.value > 0 then
    JvDBCalcEdit20.Color := $0080FFFF
  else
    JvDBCalcEdit20.Color := clWhite;
  if QCTRCVALORIMMPOSTO.value > 0 then
    JvDBCalcEdit21.Color := $0080FFFF
  else
    JvDBCalcEdit21.Color := clWhite;
  if QCTRCVLTOTPRESTACAO.value > 0 then
    JvDBCalcEdit22.Color := $0080FFFF
  else
    JvDBCalcEdit22.Color := clWhite;
end;

procedure TfrmGerarCompl.QGeradosBeforeOpen(DataSet: TDataSet);
begin
  QGerados.Parameters[0].value := GLBFilial;
end;

end.
