unit GeraCTe_dev;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, ImgList, JvMemoryDataset,
  StdCtrls, ComCtrls, JvMaskEdit, JvDBLookup, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  ExtCtrls, ComObj, JvExControls, JvExStdCtrls, JvMemo, pcnConversao,
  JvCombobox,
  jpeg, DBCtrls, JvDBControls, JvEdit, System.ImageList;

type
  TfrmGeraCTe_dev = class(TForm)
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblqt: TLabel;
    iml: TImageList;
    QVeiculo: TADOQuery;
    dtsVeiculo: TDataSource;
    pnManifesto: TPanel;
    Label5: TLabel;
    Panel5: TPanel;
    qrCalcula_Frete: TADOQuery;
    qrCalcula_FreteCUB: TBCDField;
    qrCalcula_FretePESO: TBCDField;
    qrCalcula_FreteQTD: TBCDField;
    qrCalcula_FreteVALOR: TBCDField;
    btnCancelaCTe: TBitBtn;
    QVeiculoDESCRI: TStringField;
    QVeiculoKG: TBCDField;
    QVeiculoCUB: TBCDField;
    QGerados: TADOQuery;
    dtsGerados: TDataSource;
    btnGerar: TBitBtn;
    Label58: TLabel;
    cbOperacao: TComboBox;
    Label9: TLabel;
    Label11: TLabel;
    cbMotorista: TComboBox;
    Label4: TLabel;
    cbplaca: TComboBox;
    dtsTrans: TDataSource;
    QTransp: TADOQuery;
    QTranspNOME: TStringField;
    QTranspCODIGO: TBCDField;
    edVeiculo: TJvMaskEdit;
    QVeiculoPLACA: TStringField;
    tbCte: TTabSheet;
    dgCtrc: TJvDBUltimGrid;
    Panel7: TPanel;
    btnSefaz: TBitBtn;
    btnStatus: TBitBtn;
    MStat: TJvMemo;
    QMotor: TADOQuery;
    QMotorNOME: TStringField;
    QVeiculoSITRAS: TStringField;
    QVeiculoCODPRO: TBCDField;
    Label33: TLabel;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QVeiculoCasa: TADOQuery;
    QVeiculoCasaPLACA: TStringField;
    QVeiculoCasaDESCRI: TStringField;
    QVeiculoCasaKG: TBCDField;
    QVeiculoCasaCUB: TBCDField;
    QVeiculoCasaSITRAS: TStringField;
    QVeiculoCasaCODPRO: TBCDField;
    Label34: TLabel;
    Obs: TJvMemo;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    QCons: TADOQuery;
    btnNovoCTe: TBitBtn;
    SPCalculo: TADOStoredProc;
    SP_TB_COLETA: TADOStoredProc;
    SPCte: TADOStoredProc;
    cbTrans: TComboBox;
    QGeradosCOD_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO_COMP: TBCDField;
    QGeradosDT_CONHECIMENTO: TDateTimeField;
    QGeradosDT_CONHECIMENTO_COMP: TDateTimeField;
    QGeradosCOD_PAGADOR: TBCDField;
    QGeradosCOD_REMETENTE: TBCDField;
    QGeradosCOD_DESTINATARIO: TBCDField;
    QGeradosCOD_REDESPACHO: TBCDField;
    QGeradosVL_DECLARADO_NF: TFloatField;
    QGeradosNR_QTDE_VOL: TFloatField;
    QGeradosNR_PESO_KG: TFloatField;
    QGeradosNR_PESO_CUB: TFloatField;
    QGeradosVL_FRETE_PESO: TFloatField;
    QGeradosVL_AD: TFloatField;
    QGeradosVL_GRIS: TFloatField;
    QGeradosVL_TR: TFloatField;
    QGeradosVL_TDE: TFloatField;
    QGeradosVL_OUTROS: TFloatField;
    QGeradosVL_PEDAGIO: TFloatField;
    QGeradosVL_FLUVIAL: TFloatField;
    QGeradosVL_EXCED: TFloatField;
    QGeradosVL_DESPACHO: TFloatField;
    QGeradosVL_BALSA: TFloatField;
    QGeradosVL_REDFLU: TFloatField;
    QGeradosVL_AGEND: TFloatField;
    QGeradosVL_PALLET: TFloatField;
    QGeradosVL_PORTO: TFloatField;
    QGeradosVL_ALFAND: TFloatField;
    QGeradosVL_CANHOTO: TFloatField;
    QGeradosVL_REENT: TFloatField;
    QGeradosVL_DEVOL: TFloatField;
    QGeradosVL_BASE: TFloatField;
    QGeradosVL_ALIQUOTA: TFloatField;
    QGeradosVL_IMPOSTO: TFloatField;
    QGeradosVL_TOTAL: TFloatField;
    QGeradosNM_USUARIO: TStringField;
    QGeradosOPERACAO: TBCDField;
    QGeradosDS_TIPO_MERC: TStringField;
    QGeradosDS_TIPO_FRETE: TStringField;
    QGeradosDS_OBS: TStringField;
    QGeradosDS_PLACA: TStringField;
    QGeradosMOTORISTA: TBCDField;
    QGeradosFL_STATUS: TStringField;
    QGeradosFL_TIPO: TStringField;
    QGeradosCODMUNO: TBCDField;
    QGeradosCODMUND: TBCDField;
    QGeradosCFOP: TStringField;
    QGeradosNR_FATURA: TBCDField;
    QGeradosNR_TABELA: TBCDField;
    QGeradosPEDIDO: TStringField;
    QGeradosNR_MANIFESTO: TBCDField;
    QGeradosFL_EMPRESA: TBCDField;
    QGeradosNR_RPS: TBCDField;
    QGeradosDS_CANCELAMENTO: TStringField;
    QGeradosNR_SERIE: TStringField;
    QGeradosVL_IMPOSTO_GNRE: TFloatField;
    QGeradosIMP_DACTE: TStringField;
    QGeradosFL_CONTINGENCIA: TStringField;
    QGeradosFRETE_IMPOSTO: TFloatField;
    QGeradosSEGURO_IMPOSTO: TFloatField;
    QGeradosGRIS_IMPOSTO: TFloatField;
    QGeradosOUTRAS_IMPOSTO: TFloatField;
    QGeradosFLUVIAL_IMPOSTO: TFloatField;
    QGeradosPEDAGIO_IMPOSTO: TFloatField;
    QGeradosCTE_DATA: TDateTimeField;
    QGeradosCTE_CHAVE: TStringField;
    QGeradosCTE_PROT: TStringField;
    QGeradosCTE_XMOTIVO: TStringField;
    QGeradosCTE_RECIBO: TStringField;
    QGeradosCTE_STATUS: TBCDField;
    QGeradosCTE_XMSG: TStringField;
    QGeradosFL_DDR: TStringField;
    QGeradosCTE_CAN_PROT: TStringField;
    QGeradosCTE_CHAVE_CTE_R: TStringField;
    QGeradosVL_TAS: TFloatField;
    QGeradosCLIENTE: TStringField;
    QGeradosNM_ORIGEM: TStringField;
    QGeradosNM_DESTINO: TStringField;
    edCTRC: TJvCalcEdit;
    edSerie: TJvEdit;
    Label30: TLabel;
    Label28: TLabel;
    QCTRC: TADOQuery;
    dtsCte: TDataSource;
    QCTRCCOD_CONHECIMENTO: TBCDField;
    QCTRCNR_CONHECIMENTO: TBCDField;
    QCTRCNR_CONHECIMENTO_COMP: TBCDField;
    QCTRCDT_CONHECIMENTO: TDateTimeField;
    QCTRCDT_CONHECIMENTO_COMP: TDateTimeField;
    QCTRCCOD_PAGADOR: TBCDField;
    QCTRCCOD_REMETENTE: TBCDField;
    QCTRCCOD_DESTINATARIO: TBCDField;
    QCTRCCOD_REDESPACHO: TBCDField;
    QCTRCVL_DECLARADO_NF: TFloatField;
    QCTRCNR_QTDE_VOL: TFloatField;
    QCTRCNR_PESO_KG: TFloatField;
    QCTRCNR_PESO_CUB: TFloatField;
    QCTRCVL_FRETE_PESO: TFloatField;
    QCTRCVL_AD: TFloatField;
    QCTRCVL_GRIS: TFloatField;
    QCTRCVL_TR: TFloatField;
    QCTRCVL_TDE: TFloatField;
    QCTRCVL_OUTROS: TFloatField;
    QCTRCVL_PEDAGIO: TFloatField;
    QCTRCVL_FLUVIAL: TFloatField;
    QCTRCVL_EXCED: TFloatField;
    QCTRCVL_DESPACHO: TFloatField;
    QCTRCVL_BALSA: TFloatField;
    QCTRCVL_REDFLU: TFloatField;
    QCTRCVL_AGEND: TFloatField;
    QCTRCVL_PALLET: TFloatField;
    QCTRCVL_PORTO: TFloatField;
    QCTRCVL_ALFAND: TFloatField;
    QCTRCVL_CANHOTO: TFloatField;
    QCTRCVL_REENT: TFloatField;
    QCTRCVL_DEVOL: TFloatField;
    QCTRCVL_BASE: TFloatField;
    QCTRCVL_ALIQUOTA: TFloatField;
    QCTRCVL_IMPOSTO: TFloatField;
    QCTRCVL_TOTAL: TFloatField;
    QCTRCNM_USUARIO: TStringField;
    QCTRCOPERACAO: TBCDField;
    QCTRCDS_TIPO_MERC: TStringField;
    QCTRCDS_TIPO_FRETE: TStringField;
    QCTRCDS_OBS: TStringField;
    QCTRCDS_PLACA: TStringField;
    QCTRCMOTORISTA: TBCDField;
    QCTRCFL_STATUS: TStringField;
    QCTRCFL_TIPO: TStringField;
    QCTRCCODMUNO: TBCDField;
    QCTRCCODMUND: TBCDField;
    QCTRCCFOP: TStringField;
    QCTRCNR_FATURA: TBCDField;
    QCTRCNR_TABELA: TBCDField;
    QCTRCPEDIDO: TStringField;
    QCTRCNR_MANIFESTO: TBCDField;
    QCTRCFL_EMPRESA: TBCDField;
    QCTRCNR_RPS: TBCDField;
    QCTRCDS_CANCELAMENTO: TStringField;
    QCTRCNR_SERIE: TStringField;
    QCTRCVL_IMPOSTO_GNRE: TFloatField;
    QCTRCIMP_DACTE: TStringField;
    QCTRCFL_CONTINGENCIA: TStringField;
    QCTRCFRETE_IMPOSTO: TFloatField;
    QCTRCSEGURO_IMPOSTO: TFloatField;
    QCTRCGRIS_IMPOSTO: TFloatField;
    QCTRCOUTRAS_IMPOSTO: TFloatField;
    QCTRCFLUVIAL_IMPOSTO: TFloatField;
    QCTRCPEDAGIO_IMPOSTO: TFloatField;
    QCTRCCTE_DATA: TDateTimeField;
    QCTRCCTE_CHAVE: TStringField;
    QCTRCCTE_PROT: TStringField;
    QCTRCCTE_XMOTIVO: TStringField;
    QCTRCCTE_RECIBO: TStringField;
    QCTRCCTE_STATUS: TBCDField;
    QCTRCCTE_XMSG: TStringField;
    QCTRCFL_DDR: TStringField;
    QCTRCCTE_CAN_PROT: TStringField;
    QCTRCCTE_CHAVE_CTE_R: TStringField;
    QCTRCVL_TAS: TFloatField;
    QCTRCCTE_DATA_CAN: TDateTimeField;
    QCTRCCLIENTE: TStringField;
    QCTRCORIGEM: TStringField;
    QCTRCDESTINO: TStringField;
    QCTRCNM_OPERACAO: TStringField;
    Panel1: TPanel;
    Panel4: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    JvDBCalcEdit1: TJvDBCalcEdit;
    JvDBCalcEdit2: TJvDBCalcEdit;
    JvDBCalcEdit3: TJvDBCalcEdit;
    JvDBCalcEdit4: TJvDBCalcEdit;
    dgTarefa: TJvDBUltimGrid;
    QNF: TADOQuery;
    dtsNF: TDataSource;
    QNFID_NF: TBCDField;
    QNFCODCON: TBCDField;
    QNFSERCON: TStringField;
    QNFNOTFIS: TStringField;
    QNFSERIEN: TStringField;
    QNFDATNOT: TDateTimeField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFPESCUB: TBCDField;
    QNFVLRMER: TBCDField;
    QNFDATINC: TDateTimeField;
    QNFNOTNFE: TStringField;
    QNFFL_EMPRESA: TBCDField;
    RGTipo: TRadioGroup;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QCidadeESTADO: TStringField;
    edUFo: TJvMaskEdit;
    edUFd: TJvMaskEdit;
    edCidadeO: TJvMaskEdit;
    edCidadeD: TJvMaskEdit;
    Panel3: TPanel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label15: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label23: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label38: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    Label48: TLabel;
    Label45: TLabel;
    edagend: TJvCalcEdit;
    edpallet: TJvCalcEdit;
    edentrega: TJvCalcEdit;
    edalfand: TJvCalcEdit;
    edcanhoto: TJvCalcEdit;
    edred_fluv: TJvCalcEdit;
    edFreteM: TJvCalcEdit;
    edPedagio: TJvCalcEdit;
    edOutros: TJvCalcEdit;
    edfluvial: TJvCalcEdit;
    edExc: TJvCalcEdit;
    edTR: TJvCalcEdit;
    eddespacho: TJvCalcEdit;
    edTDE: TJvCalcEdit;
    edtas: TJvCalcEdit;
    EdSeg: TJvCalcEdit;
    EdGris: TJvCalcEdit;
    edseg_balsa: TJvCalcEdit;
    edImposto: TJvCalcEdit;
    edAliq: TJvCalcEdit;
    edBase: TJvCalcEdit;
    edTotal: TJvCalcEdit;
    edCFOP: TJvMaskEdit;
    edTabela: TJvMaskEdit;
    edFretePeso: TJvCalcEdit;
    edTxCte: TJvCalcEdit;
    QCTRCREDESPACHO: TStringField;
    Predesp: TPanel;
    Label12: TLabel;
    cbredesp: TComboBox;
    BitBtn1: TBitBtn;
    QTranspRed: TADOQuery;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    QTranspRedCODCLIFOR: TBCDField;
    QTranspRedNOME: TStringField;
    QTranspRedCODCGC: TStringField;
    mdNF: TJvMemoryData;
    mdNFidNF: TBCDField;
    mdNFNotFis: TStringField;
    mdNFDatNot: TDateTimeField;
    mdNFQaunti: TBCDField;
    mdNFPesoKG: TBCDField;
    mdNFVlrMer: TBCDField;
    mdNFNOTNFE: TStringField;
    mdNFescol: TIntegerField;
    edVolume: TJvCalcEdit;
    edPesoKG: TJvCalcEdit;
    edPesoCub: TJvCalcEdit;
    edValorNF: TJvCalcEdit;
    Label41: TLabel;
    edCteIni: TJvCalcEdit;
    Label46: TLabel;
    edCteFim: TJvCalcEdit;
    btnLimpar_lote: TBitBtn;
    cbCusto: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGerarClick(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCancelaCTeClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure cbTranspExit(Sender: TObject);
    procedure btnNovoCTeClick(Sender: TObject);
    procedure cbOperacaoChange(Sender: TObject);
    procedure btnStatusClick(Sender: TObject);
    procedure btnSefazClick(Sender: TObject);
    procedure cbplacaChange(Sender: TObject);
    procedure Limpavalores;
    procedure CarregaCor;
    procedure tbCteShow(Sender: TObject);
    procedure tbCteHide(Sender: TObject);
    procedure edSerieExit(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure MStatKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTransExit(Sender: TObject);
    procedure dgTarefaDblClick(Sender: TObject);
    procedure cbredespChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBUltimGrid1CellClick(Column: TColumn);
    procedure btnLimpar_loteClick(Sender: TObject);
    procedure edCteIniExit(Sender: TObject);
    procedure edCteFimExit(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }

  end;

var
  frmGeraCTe_dev: TfrmGeraCTe_dev;

implementation

uses Dados, menu, funcoes, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmGeraCTe_dev.BitBtn1Click(Sender: TObject);
begin
  Predesp.Visible := false;
end;

procedure TfrmGeraCTe_dev.btnCancelaCTeClick(Sender: TObject);
begin
  btnCancelaCTe.Enabled := false;
  pnManifesto.Enabled := false;
  btnGerar.Enabled := false;
  btnNovoCTe.Enabled := false;
  QCTRC.close;
  QNF.close;
  RGTipo.ItemIndex := -1;
  Limpavalores;
  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  edUFo.clear;
  edCidadeO.clear;
  edUFd.clear;
  edCidadeD.clear;

  edVeiculo.clear;

  edCTRC.Enabled := true;
  edSerie.Enabled := true;
  RGTipo.Enabled := true;
end;

procedure TfrmGeraCTe_dev.btnNovoCTeClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  edCTRC.Enabled := false;
  edSerie.Enabled := false;
  RGTipo.Enabled := false;

  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  edUFo.clear;
  edCidadeO.clear;
  edUFd.clear;
  edCidadeD.clear;

  edVeiculo.clear;

  pnManifesto.Enabled := true;
  btnCancelaCTe.Enabled := true;
  btnNovoCTe.Enabled := false;
  Obs.clear;
  QTransp.open;
  cbTrans.clear;
  while not QTransp.eof do
  begin
    cbTrans.Items.Add(QTranspNOME.AsString);
    QTransp.next;
  end;
  QTransp.locate('codigo', 1145, []);
  cbTrans.ItemIndex := cbTrans.Items.IndexOf
    ('INTECOM SERVICOS DE LOGISTICA LTDA');

  if RGTipo.ItemIndex = 0 then
  begin
    QCidade.close;
    QCidade.Parameters[0].Value := QCTRCCODMUND.AsInteger;
    QCidade.open;
    edUFo.text := QCidadeESTADO.AsString;
    edCidadeO.text := QCidadeDESCRI.AsString;

    QCidade.close;
    QCidade.Parameters[0].Value := QCTRCCODMUNO.AsInteger;
    QCidade.open;
    edUFd.text := QCidadeESTADO.AsString;
    edCidadeD.text := QCidadeDESCRI.AsString;
  end
  else
  begin
    QCidade.close;
    QCidade.Parameters[0].Value := QCTRCCODMUNO.AsInteger;
    QCidade.open;
    edUFo.text := QCidadeESTADO.AsString;
    edCidadeO.text := QCidadeDESCRI.AsString;

    QCidade.close;
    QCidade.Parameters[0].Value := QCTRCCODMUND.AsInteger;
    QCidade.open;
    edUFd.text := QCidadeESTADO.AsString;
    edCidadeD.text := QCidadeDESCRI.AsString;
  end;

  QMotor.close;
  QMotor.Parameters[0].Value := QTranspCODIGO.AsInteger;
  QMotor.open;

  cbMotorista.clear;
  while not QMotor.eof do
  begin
    cbMotorista.Items.Add(QMotorNOME.AsString);
    QMotor.next;
  end;
  QMotor.close;
  QTransp.close;
  cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('146 - IMPORTA��O');
  QVeiculoCasa.close;
  QVeiculoCasa.open;
  cbplaca.clear;
  cbplaca.Items.Add('');
  while not QVeiculoCasa.eof do
  begin
    cbplaca.Items.Add(QVeiculoCasaPLACA.AsString);
    QVeiculoCasa.next;
  end;
  cbplaca.ItemIndex := cbplaca.Items.IndexOf('XXX-9999');

  QVeiculoCasa.locate('placa', cbplaca.text, []);
  edVeiculo.text := QVeiculoCasaDESCRI.AsString;

  if cbOperacao.CanFocus then
    cbOperacao.SetFocus;
  if RGTipo.ItemIndex = 0 then
    Obs.text := 'Devolu��o - CT-e :' + edCTRC.text
  else
    Obs.text := 'Reentrega - CT-e :' + edCTRC.text;
end;

procedure TfrmGeraCTe_dev.btnSefazClick(Sender: TObject);
var
  reg: Integer;
  sData, lote: String;
begin
  if QGeradosFL_TIPO.Value = 'C' then
  begin
    if (QGeradosCTE_STATUS.Value = 204)
    // or (QManifestoMDFE_STATUS.Value = 100)
      or (QGeradosCTE_STATUS.Value = 609) then
    begin
      frmMenu.ACBrCTe1.WebServices.Consulta.CTeChave :=
        QGeradosCTE_CHAVE.AsString;
      frmMenu.ACBrCTe1.WebServices.Consulta.Executar;
      dtmDados.iQuery1.close;
      dtmDados.iQuery1.SQL.clear;
      dtmDados.iQuery1.SQL.Add('update tb_conhecimento set cte_prot = :0, ');
      dtmDados.iQuery1.SQL.Add
        ('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
      dtmDados.iQuery1.SQL.Add('cte_xmsg = ''Autorizado o uso do CT-e'' ');
      dtmDados.iQuery1.SQL.Add('where cte_chave = :1 ');
      dtmDados.iQuery1.Parameters[0].Value :=
        frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
      dtmDados.iQuery1.Parameters[1].Value := QGeradosCTE_CHAVE.AsString;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.close;

      dtmDados.iQuery1.close;
      dtmDados.iQuery1.SQL.clear;
      dtmDados.iQuery1.SQL.Add('update cyber.rodcon set ctepro = :0 ');
      dtmDados.iQuery1.SQL.Add('where cte_id = :1 ');
      dtmDados.iQuery1.Parameters[0].Value :=
        frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
      dtmDados.iQuery1.Parameters[1].Value := QGeradosCTE_CHAVE.AsString;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.close;
    end
    else
    begin
      lote := '';
      // por lote
      if (edCteIni.value > 0) and (edCteFim.value > 0) then
      begin
        lote := 'S';
        QGerados.close;
        QGerados.SQL[6] := 'And nr_conhecimento between ' + #39 +
          IntToStr(edCteIni.AsInteger) + #39 + ' and ' + #39 +
          IntToStr(edCteFim.AsInteger) + #39;
        // para simular envio por lote
        if GLBUSER = 'PCORTEZ' then
          QGerados.SQL[7] := ''
        else
          QGerados.SQL[7] := 'And r.nm_usuario = ' + QuotedStr(GLBUSER);
        QGerados.open;
        if Application.Messagebox('Enviar para o Sefaz agora ?', 'Enviar Sefaz',
          MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
          sData := GLBUSER + FormatDateTime('dd/mm/yy - hh:mm:ss', now);
          while not QGerados.eof do
          begin
            reg := QGeradosCOD_CONHECIMENTO.AsInteger;
            btnSefaz.Enabled := false;
            frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
            btnSefaz.Enabled := true;

            dtmdados.IQuery1.close;
            dtmdados.IQuery1.SQL.clear;
            dtmdados.IQuery1.SQL.Add
              ('update tb_conhecimento set lote_sefaz = :0 ');
            dtmdados.IQuery1.SQL.Add('where cod_conhecimento = :1 ');
            dtmdados.IQuery1.Parameters[0].value := sData;
            dtmdados.IQuery1.Parameters[1].value := QGeradosCOD_CONHECIMENTO.AsInteger;
            dtmdados.IQuery1.ExecSQL;
            dtmdados.IQuery1.close;

            QGerados.next;
          end;
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.Add
            ('select cod_conhecimento from tb_conhecimento ');
          dtmdados.IQuery1.SQL.Add('where lote_sefaz = :0 ');
          dtmdados.IQuery1.Parameters[0].value := sData;
          dtmdados.IQuery1.open;
          while not dtmdados.IQuery1.eof do
          begin
            frmMenu.QrCteEletronico.close;
            frmMenu.QrCteEletronico.Parameters[0].value :=
              dtmdados.IQuery1.FieldByName('COD_CONHECIMENTO').AsInteger;
            frmMenu.QrCteEletronico.open;
            frmMenu.QNF.close;
            frmMenu.QNF.Parameters[0].value :=
              frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
            frmMenu.QNF.Parameters[1].value :=
              frmMenu.qrCteEletronicoNR_SERIE.value;
            frmMenu.QNF.Parameters[2].value :=
              frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
            frmMenu.QNF.open;
            try
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := false;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := true;
            finally
              //frmDacte_Cte_2.Free;
            end;
            dtmdados.IQuery1.next;
          end;
        end;
      end
      else
      // POR CT-E
      begin
        reg := QGeradosCOD_CONHECIMENTO.AsInteger;
        frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
        try
          frmMenu.qrCteEletronico.close;
          frmMenu.qrCteEletronico.Parameters[0].Value :=
            QGeradosCOD_CONHECIMENTO.AsString;
          frmMenu.qrCteEletronico.open;

          frmMenu.QNF.close;
          frmMenu.QNF.Parameters[0].Value :=
            frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
          frmMenu.QNF.Parameters[1].Value := frmMenu.qrCteEletronicoNR_SERIE.Value;
          frmMenu.QNF.Parameters[2].Value :=
            frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
          frmMenu.QNF.open;
          frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
          frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
        finally
          //frmDacte_Cte_2.Free;
        end;
      end;
    end;
    QGerados.close;
    QGerados.open;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe_dev.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrCTe1.WebServices.StatusServico.Executar;
  MStat.Lines.text :=
    UTF8Encode(frmMenu.ACBrCTe1.WebServices.StatusServico.RetWS);
  MStat.Visible := true;
  MStat.clear;
  MStat.Lines.Add('Status Servi�o');
  MStat.Lines.Add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.tpAmb));
  MStat.Lines.Add('verAplic: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.verAplic);
  MStat.Lines.Add('cStat: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cStat));
  MStat.Lines.Add('xMotivo: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.xMotivo);
  MStat.Lines.Add('cUF: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cUF));
  MStat.Lines.Add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRecbto));
  MStat.Lines.Add('tMed: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.TMed));
  MStat.Lines.Add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRetorno));
  MStat.Lines.Add('xObs: ' + frmMenu.ACBrCTe1.WebServices.StatusServico.xObs);
end;

procedure TfrmGeraCTe_dev.btnGerarClick(Sender: TObject);
var
  man, icount: Integer;
  regman, nf: String;
begin
  if cbMotorista.text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    cbMotorista.SetFocus;
    Exit;
  end;

  if cbplaca.text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    // cbPlaca.SetFocus;
    Exit;
  end;

  if cbOperacao.text = '' then
  begin
    ShowMessage('Escolha o Modal');
    cbOperacao.SetFocus;
    Exit;
  end;

  if Application.Messagebox('Gerar CT-e agora ?', 'Gerar CT-e',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;

    mdNF.DisableControls;
    mdNF.First;
    icount := 0;
    while not(mdNF.eof) do
    begin
      if (mdNFescol.AsInteger = 1) then
      begin
        if icount = 0 then
          nf := mdNFidNF.AsString
        else
          nf := nf + ',' + mdNFidNF.AsString;
        icount := icount + 1;
      end;
      mdNF.next;
    end;
    mdNF.EnableControls;

    if edTotal.Value > 0 then
    begin
      SPCte.Parameters[1].Value := QCTRCCOD_CONHECIMENTO.AsInteger;
      SPCte.Parameters[2].Value := RemoveChar(cbOperacao.text);
      if RGTipo.ItemIndex = 0 then
      begin
        SPCte.Parameters[3].Value := 'D';
        SPCte.Parameters[35].Value := nf;
      end
      else
      begin
        SPCte.Parameters[3].Value := 'C';
        SPCte.Parameters[35].Value := nf;
      end;
      SPCte.Parameters[4].Value := glbuser;
      SPCte.Parameters[5].Value := RemoveChar(cbMotorista.text);
      SPCte.Parameters[6].Value := cbplaca.text;
      SPCte.Parameters[7].Value := edFretePeso.Value;
      SPCte.Parameters[8].Value := edPedagio.Value;
      SPCte.Parameters[9].Value := edOutros.Value;
      SPCte.Parameters[10].Value := edfluvial.Value;
      SPCte.Parameters[11].Value := edExc.Value;
      SPCte.Parameters[12].Value := edTR.Value;
      SPCte.Parameters[13].Value := eddespacho.Value;
      SPCte.Parameters[14].Value := edTDE.Value;
      SPCte.Parameters[15].Value := edtas.Value;
      SPCte.Parameters[16].Value := EdSeg.Value;
      SPCte.Parameters[17].Value := EdGris.Value;
      SPCte.Parameters[18].Value := edseg_balsa.Value;
      SPCte.Parameters[19].Value := edred_fluv.Value;
      SPCte.Parameters[20].Value := edagend.Value;
      SPCte.Parameters[21].Value := edpallet.Value;
      SPCte.Parameters[22].Value := edentrega.Value;
      SPCte.Parameters[23].Value := edalfand.Value;
      SPCte.Parameters[24].Value := edcanhoto.Value;

      SPCte.Parameters[25].Value := edBase.Value;
      SPCte.Parameters[26].Value := edAliq.Value;
      SPCte.Parameters[27].Value := edImposto.Value;
      SPCte.Parameters[28].Value := edTotal.Value;
      SPCte.Parameters[29].Value := edCFOP.text;
      SPCte.Parameters[30].Value := StrToInt(edTabela.text);
      SPCte.Parameters[31].Value := GLBFilial;
      SPCte.Parameters[32].Value := 'M - ' + Obs.text;
      SPCte.Parameters[33].Value := edFreteM.Value;
      SPCte.Parameters[34].Value := StrToFloat(Label32.Caption);
      SPCte.ExecProc;
      man := SPCte.Parameters[0].Value;
      if regman = '' then
        regman := IntToStr(man)
      else
        regman := regman + ' - ' + IntToStr(man);
    end;

    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('CT-e N� ' + regman + ' Gerado');
    QCTRC.close;

    // CarregaTarefas();
  end;
  Limpavalores;
  pnManifesto.Enabled := false;
  edCTRC.Enabled := true;
  edSerie.Enabled := true;
  RGTipo.Enabled := true;
  Label29.Visible := false;
  Label32.Caption := '0';
  Label31.Caption := '';
  Label31.Visible := false;
  PageControl1.ActivePageIndex := 1;
end;

procedure TfrmGeraCTe_dev.btnLimpar_loteClick(Sender: TObject);
begin
  btnLimpar_lote.Enabled := true;
  btnStatus.Enabled := true;
  edCteIni.value := 0;
  edCteFim.value := 0;
  QGerados.close;
  QGerados.SQL[6] := '';
  QGerados.SQL[7] := '';
  QGerados.open;
end;

procedure TfrmGeraCTe_dev.CarregaCor;
begin
  if edFreteM.Value > 0 then
    edFreteM.Color := $0080FFFF
  else
    edFreteM.Color := clWhite;
  if edPedagio.Value > 0 then
    edPedagio.Color := $0080FFFF
  else
    edPedagio.Color := clWhite;
  if edOutros.Value > 0 then
    edOutros.Color := $0080FFFF
  else
    edOutros.Color := clWhite;
  if edOutros.Value > 0 then
    edOutros.Color := $0080FFFF
  else
    edOutros.Color := clWhite;
  if edExc.Value > 0 then
    edExc.Color := $0080FFFF
  else
    edExc.Color := clWhite;
  if EdSeg.Value > 0 then
    EdSeg.Color := $0080FFFF
  else
    EdSeg.Color := clWhite;
  if EdGris.Value > 0 then
    EdGris.Color := $0080FFFF
  else
    EdGris.Color := clWhite;
  if eddespacho.Value > 0 then
    eddespacho.Color := $0080FFFF
  else
    eddespacho.Color := clWhite;
  if edTDE.Value > 0 then
    edTDE.Color := $0080FFFF
  else
    edTDE.Color := clWhite;
  if edTR.Value > 0 then
    edTR.Color := $0080FFFF
  else
    edTR.Color := clWhite;
  if edfluvial.Value > 0 then
    edfluvial.Color := $0080FFFF
  else
    edfluvial.Color := clWhite;
  if edseg_balsa.Value > 0 then
    edseg_balsa.Color := $0080FFFF
  else
    edseg_balsa.Color := clWhite;
  if edred_fluv.Value > 0 then
    edred_fluv.Color := $0080FFFF
  else
    edred_fluv.Color := clWhite;
  if edagend.Value > 0 then
    edagend.Color := $0080FFFF
  else
    edagend.Color := clWhite;
  if edpallet.Value > 0 then
    edpallet.Color := $0080FFFF
  else
    edpallet.Color := clWhite;
  if edtas.Value > 0 then
    edtas.Color := $0080FFFF
  else
    edtas.Color := clWhite;
  if edentrega.Value > 0 then
    edentrega.Color := $0080FFFF
  else
    edentrega.Color := clWhite;
  if edalfand.Value > 0 then
    edalfand.Color := $0080FFFF
  else
    edalfand.Color := clWhite;
  if edcanhoto.Value > 0 then
    edcanhoto.Color := $0080FFFF
  else
    edcanhoto.Color := clWhite;
  if edBase.Value > 0 then
    edBase.Color := $0080FFFF
  else
    edBase.Color := clWhite;
  if edAliq.Value > 0 then
    edAliq.Color := $0080FFFF
  else
    edAliq.Color := clWhite;
  if edImposto.Value > 0 then
    edImposto.Color := $0080FFFF
  else
    edImposto.Color := clWhite;
  if edTotal.Value > 0 then
    edTotal.Color := $0080FFFF
  else
    edTotal.Color := clWhite;
  if edTxCte.Value > 0 then
    edTxCte.Color := $0080FFFF
  else
    edTxCte.Color := clWhite;
  if edFretePeso.Value > 0 then
    edFretePeso.Color := $0080FFFF
  else
    edFretePeso.Color := clWhite;
end;

procedure TfrmGeraCTe_dev.cbOperacaoChange(Sender: TObject);
var cod, nf : string;
    icount  : integer;
begin
  if cbOperacao.text <> '' then
  begin
    if copy(cbTrans.text, 1, 7) <> 'INTECOM' then
    begin
      QTransp.open;
      QTransp.locate('nome', cbTrans.text, []);
      QMotor.close;
      QMotor.Parameters[0].Value := QTranspCODIGO.AsInteger;
      QMotor.open;
      cbMotorista.clear;
      while not QMotor.eof do
      begin
        cbMotorista.Items.Add(QMotorNOME.AsString);
        QMotor.next;
      end;
      QMotor.close;

      cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('IMPORTA��O');

      QVeiculoCasa.close;
      QVeiculoCasa.open;
      cbplaca.clear;
      cbplaca.Items.Add('');
      while not QVeiculoCasa.eof do
      begin
        cbplaca.Items.Add(QVeiculoCasaPLACA.AsString);
        QVeiculoCasa.next;
      end;
      QVeiculoCasa.locate('placa', cbplaca.text, []);
      edVeiculo.text := QVeiculoCasaDESCRI.AsString;
      cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('IMPORTA��O');
    end;

    if edVeiculo.text <> '' then
    begin

      mdNF.DisableControls;
      mdNF.First;
      icount := 0;
      while not(mdNF.eof) do
      begin
        if (mdNFescol.AsInteger = 1) then
        begin
          if icount = 0 then
            nf := mdNFidNF.AsString
          else
            nf := nf + ',' + mdNFidNF.AsString;
          icount := icount + 1;
        end;
        mdNF.next;
      end;
      mdNF.EnableControls;

      SPCalculo.Parameters[26].Value := QCTRCCOD_CONHECIMENTO.Value;

      cod := Copy(cbOperacao.text, 1, Pos('-', cbOperacao.text) - 1);
      SPCalculo.Parameters[27].Value := cod;
      if RGTipo.ItemIndex = 0 then
      begin
        SPCalculo.Parameters[31].Value := 'D';
      end
      else
      begin
        SPCalculo.Parameters[31].Value := 'R';
      end;
      SPCalculo.Parameters[28].Value := QCTRCCODMUNO.Value;
      SPCalculo.Parameters[29].Value := QCTRCCODMUND.Value;
      SPCalculo.Parameters[30].Value := edVeiculo.text;
      SPCalculo.Parameters[32].Value := nf;
      SPCalculo.ExecProc;

      edFreteM.Value := SPCalculo.Parameters[0].Value;
      edPedagio.Value := SPCalculo.Parameters[1].Value;
      edOutros.Value := SPCalculo.Parameters[2].Value;
      edfluvial.Value := SPCalculo.Parameters[3].Value;
      edExc.Value := SPCalculo.Parameters[4].Value;
      edTR.Value := SPCalculo.Parameters[5].Value;
      eddespacho.Value := SPCalculo.Parameters[6].Value;
      edTDE.Value := SPCalculo.Parameters[7].Value;
      edtas.Value := SPCalculo.Parameters[8].Value;
      EdSeg.Value := SPCalculo.Parameters[9].Value;
      EdGris.Value := SPCalculo.Parameters[10].Value;
      edseg_balsa.Value := SPCalculo.Parameters[11].Value;
      edred_fluv.Value := SPCalculo.Parameters[12].Value;
      edagend.Value := SPCalculo.Parameters[13].Value;
      edpallet.Value := SPCalculo.Parameters[14].Value;
      edentrega.Value := SPCalculo.Parameters[15].Value;
      edalfand.Value := SPCalculo.Parameters[16].Value;
      edcanhoto.Value := SPCalculo.Parameters[17].Value;
      edBase.Value := SPCalculo.Parameters[18].Value;
      edAliq.Value := SPCalculo.Parameters[19].Value;
      edImposto.Value := SPCalculo.Parameters[20].Value;
      edTotal.Value := SPCalculo.Parameters[21].Value;
      edCFOP.text := SPCalculo.Parameters[22].Value;
      edTabela.text := IntToStr(SPCalculo.Parameters[23].Value);
      edFretePeso.Value := SPCalculo.Parameters[24].Value;
      edTxCte.Value := SPCalculo.Parameters[25].Value;

      CarregaCor;

      if edTotal.Value > 0 then
        btnGerar.Enabled := true;
    end;

  end;
end;

procedure TfrmGeraCTe_dev.cbplacaChange(Sender: TObject);
begin
  btnGerar.Enabled := false;

  if cbplaca.text <> '' then
  begin
    if copy(cbTrans.text, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.locate('placa', cbplaca.text, []);
      edVeiculo.text := QVeiculoCasaDESCRI.AsString;
    end
    else
    begin
      QVeiculo.locate('placa', cbplaca.text, []);
      edVeiculo.text := QVeiculoDESCRI.AsString;
    end;

    SPCalculo.Parameters[26].Value := QCTRCCOD_CONHECIMENTO.Value;
    SPCalculo.Parameters[27].Value := RemoveChar(cbOperacao.text);
    if RGTipo.ItemIndex = 0 then
    begin
      // SPCalculo.Parameters[26].value := QCTRCCODMUND.value;
      // SPCalculo.Parameters[27].value := QCTRCCODMUNO.value;
      SPCalculo.Parameters[31].Value := 'D';
    end
    else
    begin
      // SPCalculo.Parameters[26].value := QCTRCCODMUNO.value;
      // SPCalculo.Parameters[27].value := QCTRCCODMUND.value;
      SPCalculo.Parameters[31].Value := 'R';
    end;
    SPCalculo.Parameters[28].Value := QCTRCCODMUNO.Value;
    SPCalculo.Parameters[29].Value := QCTRCCODMUND.Value;
    SPCalculo.Parameters[30].Value := edVeiculo.text;

    SPCalculo.ExecProc;

    edFreteM.Value := SPCalculo.Parameters[0].Value;
    edPedagio.Value := SPCalculo.Parameters[1].Value;
    edOutros.Value := SPCalculo.Parameters[2].Value;
    edfluvial.Value := SPCalculo.Parameters[3].Value;
    edExc.Value := SPCalculo.Parameters[4].Value;
    edTR.Value := SPCalculo.Parameters[5].Value;
    eddespacho.Value := SPCalculo.Parameters[6].Value;
    edTDE.Value := SPCalculo.Parameters[7].Value;
    edtas.Value := SPCalculo.Parameters[8].Value;
    EdSeg.Value := SPCalculo.Parameters[9].Value;
    EdGris.Value := SPCalculo.Parameters[10].Value;
    edseg_balsa.Value := SPCalculo.Parameters[11].Value;
    edred_fluv.Value := SPCalculo.Parameters[12].Value;
    edagend.Value := SPCalculo.Parameters[13].Value;
    edpallet.Value := SPCalculo.Parameters[14].Value;
    edentrega.Value := SPCalculo.Parameters[15].Value;
    edalfand.Value := SPCalculo.Parameters[16].Value;
    edcanhoto.Value := SPCalculo.Parameters[17].Value;
    edBase.Value := SPCalculo.Parameters[18].Value;
    edAliq.Value := SPCalculo.Parameters[19].Value;
    edImposto.Value := SPCalculo.Parameters[20].Value;
    edTotal.Value := SPCalculo.Parameters[21].Value;
    edCFOP.text := SPCalculo.Parameters[22].Value;
    edTabela.text := IntToStr(SPCalculo.Parameters[23].Value);
    edFretePeso.Value := SPCalculo.Parameters[24].Value;
    edTxCte.Value := SPCalculo.Parameters[25].Value;

    CarregaCor;

    if edTotal.Value > 0 then
      btnGerar.Enabled := true;
  end;
end;

procedure TfrmGeraCTe_dev.cbredespChange(Sender: TObject);
begin
  { QTranspRed.open;
    QTranspRed.Locate('nome',cbTRans.text,[]);
    Label31.Caption := QTranspRedCODCGC.AsString+' - '+QTranspRedNOME.AsString;
    Label29.Visible := true;
    Label31.Visible := True;
    QTranspRed.close; }
  // if QuotedStr(cbredesp.text) = 'Nenhum' then
  if cbredesp.ItemIndex = 0 then
  begin
    Label32.Caption := '0,1';
    Label31.Caption := 'N�o haver� Redespacho para essa reentrega';
    Label29.Visible := true;
    Label31.Visible := true;
  end
  else
  begin
    QTranspRed.close;
    QTranspRed.SQL.clear;
    QTranspRed.SQL.Add('select distinct codclifor,razsoc nome, codcgc' + #13 +
      'from cyber.rodcli ' + #13 + 'where razsoc =' + QuotedStr(cbredesp.text) +
      'order by 2');
    QTranspRed.open;
    QTranspRed.Active := true;
    Label31.Caption := QTranspRed.FieldByName('CODCGC').AsString + ' - ' +
      QTranspRed.FieldByName('nome').AsString;
    Label29.Visible := true;
    Label31.Visible := true;
    Label32.Caption := QTranspRedCODCLIFOR.AsString;
  end;
end;

procedure TfrmGeraCTe_dev.cbTransExit(Sender: TObject);
begin
  QTransp.open;
  QTransp.locate('nome', cbTrans.text, []);
  if copy(QTranspNOME.Value, 1, 7) <> 'INTECOM' then
  begin
    QMotor.close;
    QMotor.Parameters[0].Value := QTranspCODIGO.AsInteger;
    QMotor.open;
    cbMotorista.clear;
    while not QMotor.eof do
    begin
      cbMotorista.Items.Add(QMotorNOME.AsString);
      QMotor.next;
    end;
    QMotor.close;

    cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('IMPORTA��O');

    dtmDados.iQuery1.close;
    dtmDados.iQuery1.SQL.clear;
    dtmDados.iQuery1.SQL.Add
      ('select nvl(fl_viagem,''N'') fl_viagem from tb_operacao where operacao = :0');
    dtmDados.iQuery1.Parameters[0].Value := cbOperacao.text;
    dtmDados.iQuery1.open;

    if copy(QTranspNOME.Value, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.close;
      QVeiculoCasa.open;
      cbplaca.clear;
      cbplaca.Items.Add('');
      while not QVeiculoCasa.eof do
      begin
        cbplaca.Items.Add(QVeiculoCasaPLACA.AsString);
        QVeiculoCasa.next;
      end;
      // marca que n�o controla custo
      // contmarg := 'N';
    end
    else
    begin
      QVeiculo.close;
      if dtmDados.iQuery1.FieldByName('fl_viagem').Value = 'S' then
      begin
        // QVeiculo.sql[3] := 'right join tb_custofrac t on (t.cod_fornecedor = v.codpro and f.descri = t.veiculo)';
        // QVeiculo.sql[12] := 'right join tb_custofrac t on (t.cod_fornecedor = v.codpro and f.descri = t.veiculo)';
      end
      else
      begin
        QVeiculo.SQL[3] := '';
        QVeiculo.SQL[12] := '';
      end;
      dtmDados.iQuery1.close;
      QVeiculo.Parameters[0].Value := QTranspCODIGO.AsInteger;
      QVeiculo.open;
      cbplaca.clear;
      cbplaca.Items.Add('');
      while not QVeiculo.eof do
      begin
        cbplaca.Items.Add(QVeiculoPLACA.AsString);
        QVeiculo.next;
      end;
    end;
  end;
  QTransp.close;
end;

procedure TfrmGeraCTe_dev.cbTranspExit(Sender: TObject);
begin
  if copy(QTranspNOME.Value, 1, 7) <> 'INTECOM' then
  begin
    QMotor.close;
    QMotor.Parameters[0].Value := QTranspCODIGO.AsInteger;
    QMotor.open;
    cbMotorista.clear;
    while not QMotor.eof do
    begin
      cbMotorista.Items.Add(QMotorNOME.AsString);
      QMotor.next;
    end;
    QMotor.close;

    cbMotorista.ItemIndex := cbMotorista.Items.IndexOf('IMPORTA��O');

    dtmDados.iQuery1.close;
    dtmDados.iQuery1.SQL.clear;
    dtmDados.iQuery1.SQL.Add
      ('select nvl(fl_viagem,''N'') fl_viagem from tb_operacao where operacao = :0');
    dtmDados.iQuery1.Parameters[0].Value := cbOperacao.text;
    dtmDados.iQuery1.open;

    if copy(QTranspNOME.Value, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.close;
      QVeiculoCasa.open;
      cbplaca.clear;
      cbplaca.Items.Add('');
      while not QVeiculoCasa.eof do
      begin
        cbplaca.Items.Add(QVeiculoCasaPLACA.AsString);
        QVeiculoCasa.next;
      end;
    end
    else
    begin
      QVeiculo.close;
      if dtmDados.iQuery1.FieldByName('fl_viagem').Value = 'S' then
      begin
        // QVeiculo.sql[3] := 'right join tb_custofrac t on (t.cod_fornecedor = v.codpro and f.descri = t.veiculo)';
        // QVeiculo.sql[12] := 'right join tb_custofrac t on (t.cod_fornecedor = v.codpro and f.descri = t.veiculo)';
      end
      else
      begin
        QVeiculo.SQL[3] := '';
        QVeiculo.SQL[12] := '';
      end;
      dtmDados.iQuery1.close;
      QVeiculo.Parameters[0].Value := QTranspCODIGO.AsInteger;
      QVeiculo.open;
      cbplaca.clear;
      cbplaca.Items.Add('');
      while not QVeiculo.eof do
      begin
        cbplaca.Items.Add(QVeiculoPLACA.AsString);
        QVeiculo.next;
      end;
    end;
  end;
end;

procedure TfrmGeraCTe_dev.dgTarefaDblClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar('frmGeraCTe_dev') then
  begin
    Exit;
  end
  else
  begin
    QTranspRed.open;
    cbredesp.Items.Add('Nenhum');
    while not QTranspRed.eof do
    begin
      cbredesp.Items.Add(QTranspRedNOME.AsString);
      QTranspRed.next;
    end;
    QTranspRed.close;
    Predesp.Visible := true;
  end;
end;

procedure TfrmGeraCTe_dev.edCteFimExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteFim.value > 0 then
  begin
    if edCteFim.value < edCteIni.value then
    begin
      ShowMessage('O CT-e Final n�o pode ser menor que o Inicial');
      edCteFim.SetFocus;
      Exit;
    end;
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteFim.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteFim.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe_dev.edCteIniExit(Sender: TObject);
var
  reg: Integer;
begin
  if edCteIni.value > 0 then
  begin
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    if not QGerados.locate('nr_conhecimento', edCteIni.value, []) then
    begin
      ShowMessage('O CT-e N�o existe nesta rela��o !!');
      edCteIni.SetFocus;
      Exit;
    end;
    QGerados.locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGeraCTe_dev.edSerieExit(Sender: TObject);
begin
  if edCTRC.Value > 0 then
  begin
    QCTRC.close;
    QCTRC.Parameters[0].Value := edCTRC.Value;
    QCTRC.Parameters[1].Value := edSerie.text;
    QCTRC.Parameters[2].Value := GLBFilial;
    QCTRC.open;
    if QCTRC.IsEmpty then
    begin
      ShowMessage('CT-e N�o Encontrado !!');
      edCTRC.SetFocus;
      Exit;
    end;
    QNF.close;
    QNF.Parameters[0].Value := edCTRC.Value;
    QNF.Parameters[1].Value := GLBFilial;
    QNF.open;
    mdNF.close;
    mdNF.open;
    edTotal.Value := 0;
    while not QNF.eof do
    begin
      mdNF.Append;
      mdNFidNF.Value := QNFID_NF.Value;
      mdNFNotFis.Value := QNFNOTFIS.Value;
      mdNFDatNot.Value := QNFDATNOT.Value;
      mdNFQaunti.Value := QNFQUANTI.Value;
      mdNFPesoKG.Value := QNFPESOKG.Value;
      mdNFVlrMer.Value := QNFVLRMER.Value;
      mdNFNOTNFE.Value := QNFNOTNFE.Value;
      mdNFescol.Value := 1;
      mdNF.post;
      edVolume.Value := edVolume.Value + QNFQUANTI.Value;
      edPesoKG.Value := edPesoKG.Value + QNFPESOKG.Value;
      edPesoCub.Value := edPesoCub.Value + QNFPESCUB.Value;
      edValorNF.Value := edValorNF.Value + QNFVLRMER.Value;
      QNF.next;
    end;
  end;
end;

procedure TfrmGeraCTe_dev.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QVeiculo.close;
  QCTRC.close;
  QGerados.close;
  Action := caFree;
end;

procedure TfrmGeraCTe_dev.FormCreate(Sender: TObject);
begin
  // Carrega as opera��es
  dtmDados.iQuery1.close;
  dtmDados.iQuery1.SQL.clear;
  dtmDados.iQuery1.SQL.Add
    ('select (id_operacao||''-''||operacao) cod, operacao from tb_operacao where fl_oc = ''N'' and fl_receita = ''S'' order by operacao ');
  dtmDados.iQuery1.open;
  cbOperacao.clear;
  cbOperacao.Items.Add('');
  while not dtmDados.iQuery1.eof do
  begin
    cbOperacao.Items.Add(UpperCase(dtmDados.iQuery1.FieldByName('cod').Value));
    dtmDados.iQuery1.next;
  end;
  dtmDados.iQuery1.close;

  QVeiculo.open;
  PageControl1.TabIndex := 0;
  Label29.Visible := false;
  Label31.Visible := false;
  Label32.Caption := '0';
end;

procedure TfrmGeraCTe_dev.JvDBUltimGrid1CellClick(Column: TColumn);
begin
  if (Column.Field = mdNFescol) then
  begin
    mdNF.edit;
    if mdNFescol.Value = 0 then
    begin
      mdNFescol.Value := 1;
      edVolume.Value := edVolume.Value + mdNFQaunti.Value;
      edPesoKG.Value := edPesoKG.Value + mdNFPesoKG.Value;
      edPesoCub.Value := edPesoCub.Value + 0;
      edValorNF.Value := edValorNF.Value + mdNFVlrMer.Value;
    end
    else
    begin
      mdNFescol.Value := 0;
      edVolume.Value := edVolume.Value - mdNFQaunti.Value;
      edPesoKG.Value := edPesoKG.Value - mdNFPesoKG.Value;
      edPesoCub.Value := edPesoCub.Value - 0;
      edValorNF.Value := edValorNF.Value - mdNFVlrMer.Value;
    end;
    mdNF.post;
  end;
end;

procedure TfrmGeraCTe_dev.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdNFescol) then
  begin
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    dtmDados.iml.Draw(JvDBUltimGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdNFescol.Value = 1 then
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    // JvDbgrid1.DefaultDrawDataCell(Rect, Jvdbgrid1.columns[datacol].field, State);
  end;
end;

procedure TfrmGeraCTe_dev.Limpavalores;
begin
  edFreteM.Value := 0;
  edPedagio.Value := 0;
  edOutros.Value := 0;
  edfluvial.Value := 0;
  edExc.Value := 0;
  edTR.Value := 0;
  eddespacho.Value := 0;
  edTDE.Value := 0;
  edtas.Value := 0;
  EdSeg.Value := 0;
  EdGris.Value := 0;
  edseg_balsa.Value := 0;
  edred_fluv.Value := 0;
  edagend.Value := 0;
  edpallet.Value := 0;
  edentrega.Value := 0;
  edalfand.Value := 0;
  edcanhoto.Value := 0;
  edBase.Value := 0;
  edAliq.Value := 0;
  edImposto.Value := 0;
  edTotal.Value := 0;
  edCFOP.text := '';
  edTabela.clear;
  edCTRC.clear;
  edSerie.clear;
  RGTipo.ItemIndex := -1;
end;

procedure TfrmGeraCTe_dev.MStatKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    MStat.clear;
    MStat.Visible := false;
  end;
end;

procedure TfrmGeraCTe_dev.RGTipoClick(Sender: TObject);
begin
  btnNovoCTe.Enabled := true;
  btnCancelaCTe.Enabled := true;
{
  //devolu��o
  if RGTipo.ItemIndex = 0 then
  begin
    edVolume.Value := 0;
    edPesoKG.Value := 0;
    edPesoCub.Value := 0;
    edValorNF.Value := 0;
    MDNF.First;
    while not MDNF.eof do
    begin
      mdNF.Edit;
      mdNFescol.Value := 1;
      mdNF.post;
      edVolume.Value := edVolume.Value + mdNFQaunti.Value;
      edPesoKG.Value := edPesoKG.Value + mdNFPESOKG.Value;
      edPesoCub.Value := edPesoCub.Value + 0;
      edValorNF.Value := edValorNF.Value + mdNFVLRMER.Value;
      mdNF.next;
    end;
    JvDBUltimGrid1.Enabled := false;
  end
  else     }
    JvDBUltimGrid1.Enabled := true;

end;

procedure TfrmGeraCTe_dev.TabSheet1Show(Sender: TObject);
begin
  Limpavalores;

  cbTrans.ItemIndex := -1;
  cbOperacao.ItemIndex := -1;

  cbMotorista.ItemIndex := -1;
  cbplaca.ItemIndex := -1;
  edUFo.clear;
  edCidadeO.clear;
  edUFo.clear;
  edCidadeD.clear;

  pnManifesto.Enabled := false;
end;

procedure TfrmGeraCTe_dev.TabSheet2Show(Sender: TObject);
begin
  QGerados.close;
  QGerados.open;
end;

procedure TfrmGeraCTe_dev.tbCteHide(Sender: TObject);
begin
  QGerados.close;
end;

procedure TfrmGeraCTe_dev.tbCteShow(Sender: TObject);
begin
  QGerados.open;
end;

end.
