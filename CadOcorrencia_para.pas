unit CadOcorrencia_para;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  System.ImageList, System.Actions, Vcl.ComCtrls;

type
  TfrmCadOcorrencia_para = class(TForm)
    dtsCliente: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QCliente: TADOQuery;
    iml: TImageList;
    pnGrid: TPanel;
    PageControl1: TPageControl;
    tsCliente: TTabSheet;
    dbgContrato: TJvDBUltimGrid;
    tsPara: TTabSheet;
    QClienteCLIENTE: TFMTBCDField;
    QClienteRAZSOC: TStringField;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QPara: TADOQuery;
    dtsPara: TDataSource;
    QParaDESCRICAO: TStringField;
    QParaCLIENTE: TFMTBCDField;
    QParaPARA: TFMTBCDField;
    QParaID_OCOR: TFMTBCDField;
    QParaID: TFMTBCDField;
    Panel1: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    pnBotao: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Panel2: TPanel;
    Edit4: TEdit;
    btnInserir: TBitBtn;
    dpPrimeiro: TAction;
    dpAnterior: TAction;
    dpProximo: TAction;
    dpUltimo: TAction;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure RestauraP;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QClienteAfterScroll(DataSet: TDataSet);
    procedure btnInserirClick(Sender: TObject);
    procedure dpPrimeiroExecute(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadOcorrencia_para: TfrmCadOcorrencia_para;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadOcorrencia_para.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadOcorrencia_para.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  RestauraP;
  tipo := 'A';
  Edit4.text := QParaPARA.AsString;
  Edit4.SetFocus;
end;

procedure TfrmCadOcorrencia_para.btnSalvarClick(Sender: TObject);
begin
  try
    if tipo = 'I' then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add('insert into tb_ocorrencia_para (id, cliente, id_ocor, para)');
      dtmdados.IQuery1.sql.Add('values (select seq_tb_ocorrencia_para.nextval, :0, :1, :2) ');
      dtmdados.IQuery1.Parameters[0].Value := QClienteCLIENTE.AsInteger;
      dtmdados.IQuery1.Parameters[0].Value := QParaID_OCOR.AsInteger;
      dtmdados.IQuery1.Parameters[0].Value := edit4.Text;
      dtmdados.IQuery1.ExecSQL;
    end
    else
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.sql.Add('update tb_ocorrencia_para set para = :0 ');
      dtmdados.IQuery1.sql.Add('where id = :1');
      dtmdados.IQuery1.Parameters[0].Value := edit4.Text;
      dtmdados.IQuery1.Parameters[1].Value := QParaID.AsInteger;
      dtmdados.IQuery1.ExecSQL;
    end

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QPara.Close;
  QPara.open;
end;

procedure TfrmCadOcorrencia_para.dpPrimeiroExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QPara.First;
    2:
      QPara.Prior;
    3:
      QPara.Next;
    4:
      QPara.Last;
  end;
  lblQuant.Caption := IntToStr(QPara.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

procedure TfrmCadOcorrencia_para.Edit2Exit(Sender: TObject);
begin
  if Edit2.Text <> '' then
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.Add('Select distinct razsoc from tb_tabelavenda t left join cyber.rodcli c on t.cod_cliente = c.codclifor where t.fl_status = ''S'' and codclifor = :0');
    dtmdados.IQuery1.Parameters[0].Value := Edit2.Text;
    dtmdados.IQuery1.Open;
    if not dtmdados.IQuery1.eof then
      edit1.Text := dtmdados.IQuery1.FieldByName('razsoc').AsString
    else
    begin
      edit1.Clear;
      showMessage('Cliente n�o encontrado !');
      edit2.clear;
    end;
    dtmdados.IQuery1.Close;
  end;
end;

procedure TfrmCadOcorrencia_para.btnCancelarClick(Sender: TObject);
begin
  RestauraP;
end;

procedure TfrmCadOcorrencia_para.BitBtn2Click(Sender: TObject);
begin
  Restaura;
end;

procedure TfrmCadOcorrencia_para.BitBtn3Click(Sender: TObject);
begin
  try
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.Add('insert into tb_ocorrencia_para (id, cliente, id_ocor, para)');
    dtmdados.IQuery1.sql.Add('select seq_tb_ocorrencia_para.nextval, :0, id, id ');
    dtmdados.IQuery1.sql.Add('from tb_ocorrencia p where p.situacao = ''A'' ');
    dtmdados.IQuery1.Parameters[0].Value := edit2.Text;
    dtmdados.IQuery1.ExecSQL;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  RestauraP;
  tipo := '';
  QCliente.Close;
  QCliente.open;
  QCliente.Last;
end;

procedure TfrmCadOcorrencia_para.BitBtn4Click(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  Edit2.text := QClienteCLIENTE.AsString;
  Edit1.Text := QClienteRAZSOC.AsString;
  Edit2.SetFocus;
end;

procedure TfrmCadOcorrencia_para.BitBtn9Click(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  Edit2.clear;
  Edit1.clear;
  Edit2.SetFocus;
end;

procedure TfrmCadOcorrencia_para.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  RestauraP;
  tipo := 'A';
  Edit4.text := QParaPARA.AsString;
  Edit4.SetFocus;
end;

procedure TfrmCadOcorrencia_para.FormCreate(Sender: TObject);
begin
  QCliente.open;
  quant := QCliente.RecordCount;
  Label1.Caption := IntToStr(QCliente.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadOcorrencia_para.QClienteAfterScroll(DataSet: TDataSet);
begin
  QPara.close;
  QPara.Parameters[0].Value := QClienteCLIENTE.AsInteger;
  QPara.Open;
end;

procedure TfrmCadOcorrencia_para.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCliente.Close;
  QPara.Close;
end;

procedure TfrmCadOcorrencia_para.Restaura;
begin
  BitBtn1.Enabled := not BitBtn1.Enabled;
  //btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  BitBtn2.Enabled := not BitBtn2.Enabled;
  BitBtn3.Enabled := not BitBtn3.Enabled;
  BitBtn4.Enabled := not BitBtn4.Enabled;

  if pnBotao.Visible then
    pnBotao.Visible := false
  else
    pnBotao.Visible := true;
end;

procedure TfrmCadOcorrencia_para.RestauraP;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if panel2.Visible then
    panel2.Visible := false
  else
    panel2.Visible := true;
end;

procedure TfrmCadOcorrencia_para.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QCliente.First;
    2:
      QCliente.Prior;
    3:
      QCliente.Next;
    4:
      QCliente.Last;
  end;
  label1.Caption := IntToStr(QCliente.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
