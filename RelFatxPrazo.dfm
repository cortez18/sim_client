object frmRelFatxPrazo: TfrmRelFatxPrazo
  Left = 0
  Top = 0
  Caption = 'Consulta Faturas x Prazo de Entrega'
  ClientHeight = 546
  ClientWidth = 836
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 293
    Top = 4
    Width = 32
    Height = 13
    Caption = 'Cliente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 212
    Top = 4
    Width = 36
    Height = 13
    Caption = 'Fatura :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 188
    Top = 520
    Width = 559
    Height = 23
    Progress = 0
  end
  object Label2: TLabel
    Left = 8
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 109
    Top = 4
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btRelatorio: TBitBtn
    Left = 753
    Top = 18
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 293
    Top = 20
    Width = 174
    Height = 21
    DropDownCount = 30
    LookupDisplay = 'NR_CNPJ_CPF'
    LookupField = 'COD_CLIENTE'
    LookupSource = dtsCliente
    TabOrder = 5
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 473
    Top = 20
    Width = 274
    Height = 21
    DropDownCount = 30
    LookupDisplay = 'NM_CLIENTE'
    LookupField = 'COD_CLIENTE'
    LookupSource = dtsCliente
    TabOrder = 6
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 56
    Width = 828
    Height = 458
    DataSource = navnavig
    DrawingStyle = gdsClassic
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    WordWrap = True
    Columns = <
      item
        Expanded = False
        FieldName = 'DATEMI'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NUMDUP'
        Title.Alignment = taCenter
        Title.Caption = 'Fatura'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODDOC'
        Title.Alignment = taCenter
        Title.Caption = 'CT-e'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_ENTREGA_PREV'
        Title.Alignment = taCenter
        Title.Caption = 'Data Prevista'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_ENTREGA'
        Title.Alignment = taCenter
        Title.Caption = 'Data Entrega'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 83
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAZSOC'
        Title.Alignment = taCenter
        Title.Caption = 'Cliente'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 358
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 261
    Top = 204
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    Visible = False
  end
  object btnExcel: TBitBtn
    Left = 753
    Top = 520
    Width = 75
    Height = 25
    Caption = 'Exportar'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF196B37196B37196B
      37196B37196B37FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF196B37288C5364BA8D95D2B264BA8D288C53196B37CD6E23C9651B
      C8601AC65918C25317C04E16BD4715BB4115B93E14206A3862BA8B60BA87FFFF
      FF60B98767BC8F1E6936D27735E4AF87E3AB81E1A87BDFA376DEA171DC9D6DDB
      9968DA9763317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B37D68443E7B590
      E0A374DE9E6EDC9A67DB9560D9915AD78D53D5894D48875C90D3B192D6B1FFFF
      FF65BC8C67BC8F1E6936DB8E53EABB99FCF6F2E1A679FCF5F1DD9D6BFCF6F2DA
      945EF7E9DE938E5D61AB8195D4B4BAE6D06ABB8F2D8F57605326E19762ECC1A1
      FCF7F3E5AD84FCF6F2E1A477FCF7F3DD9B69F8EBE0F0D3BEA8BAA05F956F4F8E
      66478458868654B73C13E2A06EEEC7A8FEFDFCFDF7F3FEFAF8E3AC81FCF7F4E0
      A374F9ECE3FAF2EBFDF8F4E3AE86FAF1EAD5894DDA9966BD4315E6A779F0CBB0
      FDF8F5EABA98FDF8F4E7B38CFDF8F5E3AA80F9EEE5EECDB4FDF8F5E5B38EFDF9
      F6D88F57DD9E6DC04E16EAAB80F2CFB5FCF4EEECBF9FFBF3EDFDF8F4FDF7F4FC
      F7F3F4DBC9E7B48EF7E6DAE3AD83F6E4D6DB9762DFA376C45918EAAB80F3D0B7
      EFC6A9EFC4A6EEC2A2ECBF9EEBBC98E9B893E8B48EE6B088E3AC81E2A77BE0A3
      74DE9E6EE2AA80C9621AEAAB80F3D0B7F3D0B7F3D0B7F2D0B7F1CEB3F0CBB0EF
      C9ACEEC6A8EDC2A3EBC09EEABB99E8B794E6B48FE4B089CD6E23EAAB80EAAB80
      EAAB80EAAB80EAAB80EAAB80E8A97CE6A477E2A070E29B6BE19762DD9059D98B
      52D88549D6803ED27735FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 3
    OnClick = btnExcelClick
  end
  object edFatura: TJvMaskEdit
    Left = 212
    Top = 20
    Width = 69
    Height = 21
    MaxLength = 6
    TabOrder = 4
    Text = ''
  end
  object dtInicial: TJvDateEdit
    Left = 8
    Top = 20
    Width = 90
    Height = 21
    Hint = 'Data do Servi'#231'o'
    ParentShowHint = False
    ShowHint = True
    ShowNullDate = False
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 109
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 1
  end
  object DBNavigator1: TDBNavigator
    Left = 6
    Top = 520
    Width = 176
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 9
  end
  object QFatura: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select i.numdup, d.datemi, i.coddoc, c.dt_entrega_prev, c.dt_ent' +
        'rega, cl.razsoc'
      
        'from cyber.roddup d left join cyber.rodidu i on i.numdup = d.num' +
        'dup and i.codfil = d.codfil'
      
        '                    left join vw_ocs_ost x on x.codcon = i.coddo' +
        'c and x.filcon = i.codfil'
      
        '                    left join cyber.tb_coleta c on c.nro_ocs = x' +
        '.cod_reg and c.filial = x.filcon'
      
        '                    left join cyber.rodcli cl on cl.codclifor = ' +
        'd.codclifor'
      'where d.codclifor > 0'
      ''
      ''
      ''
      'order by datemi desc')
    Left = 28
    Top = 208
    object QFaturaNUMDUP: TBCDField
      FieldName = 'NUMDUP'
      Precision = 32
    end
    object QFaturaDATEMI: TDateTimeField
      FieldName = 'DATEMI'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QFaturaCODDOC: TBCDField
      FieldName = 'CODDOC'
      Precision = 32
    end
    object QFaturaDT_ENTREGA_PREV: TDateTimeField
      FieldName = 'DT_ENTREGA_PREV'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QFaturaDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QFaturaRAZSOC: TStringField
      FieldName = 'RAZSOC'
      Size = 80
    end
  end
  object navnavig: TDataSource
    DataSet = QFatura
    Left = 24
    Top = 260
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct cl.codclifor cod_cliente, cl.razsoc nm_cliente, ' +
        'cl.codcgc nr_cnpj_cpf'
      'from cyber.rodcon c'
      'left join cyber.rodfra op on op.codfre = c.tabpad'
      'left join cyber.rodcli cl on cl.codclifor = c.codpag'
      'where c.situac in ('#39'D'#39','#39'E'#39')'
      'and c.cteaut = '#39'S'#39
      'and c.numdup is null'
      'order by 2')
    Left = 28
    Top = 104
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
    end
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 80
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
  end
  object dtsCliente: TDataSource
    DataSet = QCliente
    Left = 28
    Top = 148
  end
end
