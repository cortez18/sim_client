object frmCons_Fatura_rec: TfrmCons_Fatura_rec
  Left = 332
  Top = 256
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Consultar Faturas'
  ClientHeight = 619
  ClientWidth = 1365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 61
    Width = 1365
    Height = 558
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 1
    object DBSint: TJvDBUltimGrid
      Left = 1
      Top = 1
      Width = 1363
      Height = 556
      Align = alClient
      Ctl3D = False
      DataSource = dtsSint
      DrawingStyle = gdsClassic
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentCtl3D = False
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      OnDrawColumnCell = DBSintDrawColumnCell
      AutoSort = False
      TitleArrow = True
      AutoSizeColumns = True
      SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATINC'
          Title.Alignment = taCenter
          Title.Caption = 'Data Fatura'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 336
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NUMDUP'
          Title.Alignment = taCenter
          Title.Caption = 'Fatura'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 246
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATVEN'
          Title.Alignment = taCenter
          Title.Caption = 'Vencimento'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clRed
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 267
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TEM'
          Title.Alignment = taCenter
          Title.Caption = 'Tem Compr.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 258
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FALTAM'
          Title.Alignment = taCenter
          Title.Caption = 'Falta Compr.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 237
          Visible = True
        end>
    end
    object dbgFatura: TJvDBUltimGrid
      Left = 1
      Top = 1
      Width = 1363
      Height = 556
      Align = alClient
      Ctl3D = False
      DataSource = ds
      DrawingStyle = gdsClassic
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentCtl3D = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dbgFaturaCellClick
      OnDrawColumnCell = dbgFaturaDrawColumnCell
      AutoSort = False
      TitleArrow = True
      AutoSizeColumns = True
      SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATINC'
          Title.Alignment = taCenter
          Title.Caption = 'Data Fatura'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 69
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NUMDUP'
          Title.Alignment = taCenter
          Title.Caption = 'Fatura'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NR_CONHECIMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'CT-e'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clRed
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DT_CONHECIMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_CLIENTE'
          Title.Alignment = taCenter
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_CLIENTE_EXP'
          Title.Alignment = taCenter
          Title.Caption = 'Expedidor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 193
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_CLIENTE_DES'
          Title.Alignment = taCenter
          Title.Caption = 'Destinat'#225'rio'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 232
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_CLIENTE_CON'
          Title.Alignment = taCenter
          Title.Caption = 'Consignat'#225'rio'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 146
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VL_TOTAL'
          Title.Alignment = taCenter
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OPERACAO'
          Title.Alignment = taCenter
          Title.Caption = 'Opera'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 173
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cfop'
          Title.Alignment = taCenter
          Title.Caption = 'CFOP'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'status'
          Title.Alignment = taCenter
          Title.Caption = 'Status'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'CAMINHO'
          Title.Alignment = taCenter
          Title.Caption = 'Compr.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 41
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FL_COMPROVANTE'
          Title.Alignment = taCenter
          Title.Caption = 'Conf.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Width = 40
          Visible = True
        end>
    end
    object Panel8: TPanel
      Left = 338
      Top = 154
      Width = 447
      Height = 51
      Caption = 'Gerando.....'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      Visible = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1365
    Height = 61
    Align = alTop
    TabOrder = 0
    object JvGradient1: TJvGradient
      Left = 1
      Top = 1
      Width = 1363
      Height = 20
      Align = alTop
      Style = grVertical
      StartColor = 16245453
      EndColor = clBtnFace
      ExplicitLeft = 0
    end
    object Label1: TLabel
      Left = 6
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Fatura'
      Transparent = True
    end
    object Label10: TLabel
      Left = 88
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 182
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 359
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Cliente :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtInicial: TJvDateEdit
      Left = 86
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object dtFinal: TJvDateEdit
      Left = 182
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 2
    end
    object ledIdCliente: TJvDBLookupEdit
      Left = 359
      Top = 27
      Width = 149
      Height = 21
      LookupDisplay = 'NR_CNPJ_CPF'
      LookupField = 'COD_CLIENTE'
      LookupSource = dtsClientes
      CharCase = ecUpperCase
      TabOrder = 3
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object ledCliente: TJvDBLookupEdit
      Left = 523
      Top = 27
      Width = 345
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'NM_CLIENTE'
      LookupField = 'COD_CLIENTE'
      LookupSource = dtsClientes
      CharCase = ecUpperCase
      TabOrder = 4
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object edFatura: TEdit
      Left = 6
      Top = 27
      Width = 70
      Height = 21
      TabStop = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object btnListagem: TBitBtn
      Left = 1281
      Top = 3
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Listagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      TabStop = False
      OnClick = btnListagemClick
    end
    object btnRelat: TBitBtn
      Left = 1198
      Top = 31
      Width = 75
      Height = 25
      Hint = 'Listagem'
      Caption = 'Relat'#243'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      TabStop = False
      OnClick = btnRelatClick
    end
    object btRelatorio: TBitBtn
      Left = 1198
      Top = 3
      Width = 75
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 7
      OnClick = btRelatorioClick
    end
    object btnSKF: TBitBtn
      Left = 1281
      Top = 31
      Width = 74
      Height = 25
      Hint = 'Listagem'
      Caption = 'Rel. SKF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      TabStop = False
      OnClick = btnSKFClick
    end
    object RGD: TRadioGroup
      Left = 278
      Top = 5
      Width = 72
      Height = 53
      Caption = 'Data'
      ItemIndex = 0
      Items.Strings = (
        'Emiss'#227'o'
        'Vcto')
      TabOrder = 9
    end
    object RGS: TRadioGroup
      Left = 874
      Top = 3
      Width = 128
      Height = 53
      Caption = 'Situa'#231#227'o'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Aberto'
        'Pago')
      TabOrder = 10
    end
    object RGM: TRadioGroup
      Left = 1013
      Top = 3
      Width = 80
      Height = 53
      Caption = 'Modo'
      ItemIndex = 0
      Items.Strings = (
        'Anal'#237'tico'
        'Sint'#233'tico')
      TabOrder = 11
    end
    object cbXML: TCheckBox
      Left = 1099
      Top = 8
      Width = 82
      Height = 17
      Caption = 'Anexar XML'
      TabOrder = 12
    end
    object cbDacte: TCheckBox
      Left = 1099
      Top = 25
      Width = 95
      Height = 17
      Caption = 'Anexar DACTE'
      TabOrder = 13
    end
    object cbCompr: TCheckBox
      Left = 1099
      Top = 41
      Width = 95
      Height = 17
      Caption = 'Anexar Compr'
      TabOrder = 14
    end
  end
  object JvEnterAsTab1: TJvEnterAsTab
    Left = 400
    Top = 232
  end
  object ds: TDataSource
    DataSet = QCTRC
    Left = 548
    Top = 164
  end
  object QCTRC: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select distinct  c.nr_conhecimento, c.dt_conhecimento, re.razsoc' +
        ' nm_cliente_EXP, de.razsoc nm_cliente_DES,'
      
        '       c.vl_total, l.codcgc cnpjloja, cl.codclifor cod_cliente, ' +
        ' cl.razsoc nm_cliente, cl.codclifor, op.operacao operacao,'
      '       co.razsoc nm_cliente_CON, c.nr_serie, '
      '       case when c.cfop = 6932 then'
      
        '        (select count(*) from cyber.pagdoc pg where (to_char(c.n' +
        'r_conhecimento) = pg.numdoc and c.fl_empresa = pg.codfil and c.v' +
        'l_imposto = pg.vlrdoc and pg.situac = '#39'L'#39'))'
      
        '       else  0 end pagou, ccl.estado ufcl, cre.estado ufre, cde.' +
        'estado ufde, fa.datinc, fa.numdup, t.caminho, c.cte_chave cte_id' +
        ', c.fl_comprovante, c.cod_conhecimento'
      'from tb_conhecimento c '
      'left join tb_tabelavenda op on op.cod_venda = c.nr_tabela'
      'left join cyber.rodcli cl on cl.codclifor = c.cod_pagador'
      'left join cyber.rodmun ccl on ccl.codmun = cl.codmun'
      'left join cyber.rodcli re on re.codclifor = c.cod_remetente'
      'left join cyber.rodmun cre on cre.codmun = re.codmun'
      'left join cyber.rodcli de on de.codclifor = c.cod_destinatario'
      'left join cyber.rodmun cde on cde.codmun = de.codmun'
      
        'left join cyber.rodcli co on co.codclifor = nvl(c.cod_expedidor,' +
        ' c.cod_redespacho)'
      'left join cyber.rodfil l on (l.codfil = c.fl_empresa)'
      
        'left join cyber.rodidu fa on fa.coddoc = c.nr_conhecimento AND f' +
        'a.fildoc = c.fl_empresa and fa.tipdoc = '#39'CTRC'#39' left join cyber.r' +
        'oddup f on f.numdup = fa.numdup and f.codfil = fa.codfil left jo' +
        'in cyber.RECMEN p on p.numdup = to_char(f.numdup) and p.codclifo' +
        'r = f.codclifor and numpar = '#39'1'#39
      
        'left join cyber.tb_comprovantes t on (c.nr_conhecimento = t.nro_' +
        'doc and c.fl_empresa = t.filial_doc)'
      'where fa.codfil = :0'
      'and fa.numdup = '#39'1'#39
      ''
      ''
      'and c.fl_status = ('#39'A'#39')'
      'and c.cte_status = 100'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by 1')
    Left = 500
    Top = 164
    object QCTRCNR_CONHECIMENTO: TBCDField
      FieldName = 'NR_CONHECIMENTO'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object QCTRCDT_CONHECIMENTO: TDateTimeField
      FieldName = 'DT_CONHECIMENTO'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCTRCNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      ReadOnly = True
      Size = 50
    end
    object QCTRCNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      ReadOnly = True
      Size = 50
    end
    object QCTRCVL_TOTAL: TBCDField
      FieldName = 'VL_TOTAL'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 9
      Size = 2
    end
    object QCTRCNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QCTRCOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
    end
    object QCTRCNM_CLIENTE_CON: TStringField
      FieldName = 'NM_CLIENTE_CON'
      ReadOnly = True
      Size = 50
    end
    object QCTRCCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCTRCCNPJLOJA: TStringField
      FieldName = 'CNPJLOJA'
    end
    object QCTRCNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 5
    end
    object QCTRCCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object QCTRCPAGOU: TBCDField
      FieldName = 'PAGOU'
      ReadOnly = True
      Precision = 32
    end
    object QCTRCUFCL: TStringField
      FieldName = 'UFCL'
      ReadOnly = True
      Size = 2
    end
    object QCTRCUFRE: TStringField
      FieldName = 'UFRE'
      ReadOnly = True
      Size = 2
    end
    object QCTRCUFDE: TStringField
      FieldName = 'UFDE'
      ReadOnly = True
      Size = 2
    end
    object QCTRCDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCTRCNUMDUP: TBCDField
      FieldName = 'NUMDUP'
      ReadOnly = True
      Precision = 32
    end
    object QCTRCCAMINHO: TStringField
      FieldName = 'CAMINHO'
      ReadOnly = True
      Size = 500
    end
    object QCTRCCTE_ID: TStringField
      FieldName = 'CTE_ID'
      ReadOnly = True
      Size = 48
    end
    object QCTRCFL_COMPROVANTE: TStringField
      FieldName = 'FL_COMPROVANTE'
      ReadOnly = True
      Size = 1
    end
    object QCTRCCOD_CONHECIMENTO: TBCDField
      FieldName = 'COD_CONHECIMENTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object QClientes: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClientesBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct cl.codclifor cod_cliente, cl.razsoc nm_cliente, ' +
        'cl.codcgc nr_cnpj_cpf'
      
        'from tb_conhecimento c left join cyber.rodcli cl on cl.codclifor' +
        ' = c.cod_pagador'
      'where c.fl_empresa = 1'
      'and c.fl_status = '#39'A'#39
      'and c.cte_prot is not null'
      'and c.nr_fatura is null'
      '--and c.dt_conhecimento > '#39'01/01/2017'#39
      'order by 2')
    Left = 348
    Top = 164
    object QClientesCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      Precision = 32
      Size = 0
    end
    object QClientesNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      Size = 50
    end
    object QClientesNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
    end
  end
  object dtsClientes: TDataSource
    DataSet = QClientes
    Left = 408
    Top = 164
  end
  object iml: TImageList
    Left = 349
    Top = 236
    Bitmap = {
      494C010102000400FC0110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object QRelat: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 7
        Value = '5013177'
      end>
    SQL.Strings = (
      'select * from VW_itc_transporte'
      'where fatura = :0')
    Left = 224
    Top = 164
    object QRelatNR_CTRC: TBCDField
      FieldName = 'NR_CTRC'
      Precision = 32
    end
    object QRelatDT_CTRC: TDateTimeField
      FieldName = 'DT_CTRC'
    end
    object QRelatNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 3
    end
    object QRelatNM_CLI: TStringField
      FieldName = 'NM_CLI'
      Size = 80
    end
    object QRelatCNPJ_CLI: TStringField
      FieldName = 'CNPJ_CLI'
    end
    object QRelatNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_EXP: TStringField
      FieldName = 'NR_CNPJ_CPF_EXP'
    end
    object QRelatCIDADE_EXP: TStringField
      FieldName = 'CIDADE_EXP'
      Size = 40
    end
    object QRelatUF_EXP: TStringField
      FieldName = 'UF_EXP'
      Size = 2
    end
    object QRelatNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_DES: TStringField
      FieldName = 'NR_CNPJ_CPF_DES'
    end
    object QRelatCIDADE_DES: TStringField
      FieldName = 'CIDADE_DES'
      Size = 40
    end
    object QRelatUF_DES: TStringField
      FieldName = 'UF_DES'
      Size = 2
    end
    object QRelatNM_CLIENTE_RED: TStringField
      FieldName = 'NM_CLIENTE_RED'
      Size = 80
    end
    object QRelatNR_CNPJ_CPF_RED: TStringField
      FieldName = 'NR_CNPJ_CPF_RED'
    end
    object QRelatCIDADE_RED: TStringField
      FieldName = 'CIDADE_RED'
      Size = 40
    end
    object QRelatUF_RED: TStringField
      FieldName = 'UF_RED'
      Size = 2
    end
    object QRelatVL_NF: TBCDField
      FieldName = 'VL_NF'
      Precision = 32
    end
    object QRelatPESO_KG: TBCDField
      FieldName = 'PESO_KG'
      Precision = 32
    end
    object QRelatQT_VOLUME: TBCDField
      FieldName = 'QT_VOLUME'
      Precision = 32
    end
    object QRelatNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
    end
    object QRelatVL_FRETE: TBCDField
      FieldName = 'VL_FRETE'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_SEGURO: TBCDField
      FieldName = 'VL_SEGURO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_GRIS: TBCDField
      FieldName = 'VL_GRIS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_TOTAL: TBCDField
      FieldName = 'VL_TOTAL'
      DisplayFormat = '##,##0.00'
      Precision = 14
      Size = 2
    end
    object QRelatCAT: TBCDField
      FieldName = 'CAT'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ADEME: TBCDField
      FieldName = 'VL_ADEME'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_DESPACHO: TBCDField
      FieldName = 'VL_DESPACHO'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_DESCARGA: TBCDField
      FieldName = 'VL_DESCARGA'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ENTREGA: TBCDField
      FieldName = 'VL_ENTREGA'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_TDE: TBCDField
      FieldName = 'VL_TDE'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatVL_ADICIONAL: TBCDField
      FieldName = 'VL_ADICIONAL'
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QRelatDESCRICAO_ADICIONAL: TStringField
      FieldName = 'DESCRICAO_ADICIONAL'
      Size = 30
    end
    object QRelatVL_ALIQUOTA: TBCDField
      FieldName = 'VL_ALIQUOTA'
      DisplayFormat = '##,##0.00'
      Precision = 8
    end
    object QRelatVL_IMPOSTO: TBCDField
      FieldName = 'VL_IMPOSTO'
      DisplayFormat = '##,##0.00'
      Precision = 14
      Size = 2
    end
    object QRelatFL_TIPO: TStringField
      FieldName = 'FL_TIPO'
      Size = 100
    end
    object QRelatNR_FATURA: TBCDField
      FieldName = 'NR_FATURA'
      Precision = 32
    end
    object QRelatCNPJ_TRANSPORTADORA: TStringField
      FieldName = 'CNPJ_TRANSPORTADORA'
    end
    object QRelatIESTADUAL: TStringField
      FieldName = 'IESTADUAL'
      Size = 16
    end
    object QRelatOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 200
    end
    object QRelatCFOP_CLESS: TStringField
      FieldName = 'CFOP_CLESS'
      Size = 6
    end
    object QRelatTIPCON: TStringField
      FieldName = 'TIPCON'
      FixedChar = True
      Size = 1
    end
    object QRelatFATURA: TBCDField
      FieldName = 'FATURA'
      Precision = 32
    end
    object QRelatCTE_ID: TStringField
      FieldName = 'CTE_ID'
      Size = 48
    end
    object QRelatPESO_CUB: TBCDField
      FieldName = 'PESO_CUB'
      Precision = 32
    end
    object QRelatOBSCON: TStringField
      FieldName = 'OBSCON'
      Size = 500
    end
    object QRelatCODCIDADEDESTINO: TStringField
      FieldName = 'CODCIDADEDESTINO'
      Size = 10
    end
    object QRelatCODCIDADEORIGEM: TStringField
      FieldName = 'CODCIDADEORIGEM'
      Size = 10
    end
  end
  object dtsRelat: TDataSource
    DataSet = QRelat
    Left = 272
    Top = 164
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select notfis'
      'from cyber.rodnfc n'
      'where n.codcon = :0 and n.sercon = :1 and n.codfil = :2')
    Left = 828
    Top = 168
    object QNFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
  end
  object QSKF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    BeforeOpen = QCTRCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select i.numdup, c.codcon, c.sercon, c.ctedta, c.totfre, c.icmvl' +
        'r, c.bascal, c.cte_id, c.natope, c.codrem, c.alicot, i.fildoc, e' +
        '.estado'
      
        'from cyber.rodidu i left join cyber.rodcon c on i.coddoc = c.cod' +
        'con and i.fildoc = c.codfil and i.serdoc = c.sercon'
      
        '                    left join cyber.rodfil f on f.codfil = c.cod' +
        'fil'
      
        '                    left join cyber.rodmun e on e.codmun = f.cod' +
        'mun'
      'where i.numdup = :0'
      'and i.tipdoc = '#39'CTRC'#39)
    Left = 784
    Top = 168
    object QSKFNUMDUP: TBCDField
      FieldName = 'NUMDUP'
      Precision = 32
    end
    object QSKFCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QSKFSERCON: TStringField
      FieldName = 'SERCON'
      Size = 3
    end
    object QSKFTOTFRE: TBCDField
      FieldName = 'TOTFRE'
      Precision = 14
      Size = 2
    end
    object QSKFICMVLR: TBCDField
      FieldName = 'ICMVLR'
      Precision = 14
      Size = 2
    end
    object QSKFBASCAL: TBCDField
      FieldName = 'BASCAL'
      Precision = 14
      Size = 2
    end
    object QSKFCTE_ID: TStringField
      FieldName = 'CTE_ID'
      Size = 48
    end
    object QSKFNATOPE: TBCDField
      FieldName = 'NATOPE'
      Precision = 32
    end
    object QSKFCODREM: TBCDField
      FieldName = 'CODREM'
      Precision = 32
    end
    object QSKFALICOT: TBCDField
      FieldName = 'ALICOT'
      Precision = 8
    end
    object QSKFFILDOC: TBCDField
      FieldName = 'FILDOC'
      Precision = 32
    end
    object QSKFCTEDTA: TDateTimeField
      FieldName = 'CTEDTA'
    end
    object QSKFESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 40
    end
  end
  object QSint: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    BeforeOpen = QSintBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct f.datinc, f.numdup, f.datven, count(t.caminho) t' +
        'em, (count(c.codcon) - count(t.caminho)) faltam'
      'from cyber.rodcon c'
      
        'left join cyber.rodidu fa on fa.coddoc = c.codcon AND fa.fildoc ' +
        '= c.codfil '
      
        'left join cyber.roddup f on f.numdup = fa.numdup and f.codfil = ' +
        'fa.codfil '
      
        'left join cyber.RECMEN p on p.numdup = to_char(f.numdup) and p.c' +
        'odclifor = f.codclifor and numpar = '#39'1'#39
      
        'left join cyber.tb_comprovantes t on (c.codcon = t.nro_doc and c' +
        '.codfil = t.filial_doc)'
      'where fa.codfil = :0'
      'and fa.numdup = '#39'171216'#39
      'and fa.datinc ='
      'and c.codpag = 22874'
      'and c.situac in ('#39'D'#39','#39'E'#39')'
      'and c.cteaut = '#39'S'#39
      'AND fa.situac <> '#39'C'#39
      ''
      'group by f.datinc, f.numdup, f.datven')
    Left = 500
    Top = 232
    object QSintDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
    end
    object QSintNUMDUP: TBCDField
      FieldName = 'NUMDUP'
      ReadOnly = True
      Precision = 32
    end
    object QSintDATVEN: TDateTimeField
      FieldName = 'DATVEN'
      ReadOnly = True
    end
    object QSintTEM: TBCDField
      FieldName = 'TEM'
      ReadOnly = True
      Precision = 32
    end
    object QSintFALTAM: TBCDField
      FieldName = 'FALTAM'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsSint: TDataSource
    DataSet = QSint
    Left = 548
    Top = 232
  end
  object QConf: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClientesBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'update tb_conhecimento set fl_comprovante = :0 where cte_chave =' +
        ' :1')
    Left = 224
    Top = 236
  end
end
