unit MonAverba;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, JvExMask,
  JvToolEdit, Buttons, DBCtrls, SHDocVw, xmldom, XMLIntf, msxmldom, XMLDoc,
  InvokeRegistry, Rio, SOAPHTTPClient;

type
  TfrmMonAverba = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    grMonitor: TDBGrid;
    qrMonitor: TADOQuery;
    dsMonitor: TDataSource;
    mCte: TMemo;
    XMLDoc: TXMLDocument;
    btnATM: TBitBtn;
    qrMonitorCOD_CONHECIMENTO: TBCDField;
    qrMonitorNR_CONHECIMENTO: TBCDField;
    qrMonitorAVERBA_PROTOCOLO: TStringField;
    qrMonitorCTE_CHAVE: TStringField;
    lbTotReg: TLabel;
    qrMonitorcte_data: TDateTimeField;
    qrMonitorFL_EMPRESA: TBCDField;
    qrMonitoruser_ws: TStringField;
    qrMonitorpass_ws: TStringField;
    qrMonitorcode_ws: TStringField;
    procedure mCteClick(Sender: TObject);
    procedure mCteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    function TagToNameTag(ChildNode: IXMLNode; NomeTag: String): String;
    procedure btnATMClick(Sender: TObject);
    procedure qrMonitorBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure grMonitorDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisa();

  public
    { Public declarations }
    iEmpresa: integer;

  end;

var
  frmMonAverba: TfrmMonAverba;

implementation

uses Dados, menu, funcoes, pcnConversao, ATM;

{$R *.dfm}

procedure TfrmMonAverba.btnATMClick(Sender: TObject);
var
  sData, sCaminho, sCte: String;           //, data, hora  protocolo,
  //inic, fim, a: integer;
  //erro: Retorno;
  //datahora : TDatetime;
  //resultado : SuccessProcesso;
  destino,sArquivo : string;
begin
  while not qrMonitor.eof do
  begin
    try
      // frmMenu.ACBrCTe1.Configuracoes.WebServices.UF := QSiteds_UF.AsString;
      // verifica ambiente do cte
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('select ambiente_cte, certificado, logo, pdf_cte, ');
      dtmdados.IQuery1.sql.add
        ('schemma, proxy, porta, user_proxy, pass_proxy ');
      dtmdados.IQuery1.sql.add
        ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
      dtmdados.IQuery1.Parameters[0].value := GLBFilial;
      dtmdados.IQuery1.open;

      if dtmdados.IQuery1.FieldByName('ambiente_cte').AsString = 'H' then
      begin
        frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taHomologacao;
      end
      else
      begin
        frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taProducao;
      end;


      frmMenu.ACBrCTe1.Configuracoes.Certificados.NumeroSerie :=
        ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

      dtmdados.IQuery1.close;

      sData := FormatDateTime('YYYY', qrMonitorcte_data.value) +
        FormatDateTime('MM', qrMonitorcte_data.value);
      sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCte + '\' + sData
        + '\CTe\';
      sCte := qrMonitorCTE_CHAVE.AsString + '-cte.xml';
      sArquivo :=  sCaminho + sCte;
      frmMenu.ACBrCTe1.Conhecimentos.clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sArquivo);
      frmMenu.ACBrCTe1.Consultar;

      // gravar na pasta para averba��o
      destino := '\\192.168.236.112\cte\' +
          copy(frmMenu.ACBrCTe1.Conhecimentos.Items[0].CTe.infCTe.ID, 4, 44) +
          '-cte.xml';
      CopyFile(PChar(sArquivo), PChar(destino), True);

    except

    end;
    qrMonitor.next;
  end;
  Pesquisa;
end;

procedure TfrmMonAverba.FormCreate(Sender: TObject);
begin
  Pesquisa();
end;

procedure TfrmMonAverba.grMonitorDblClick(Sender: TObject);
var
  protocolo, sData, sCaminho, sCte: String;    //data, hora,
  //inic, fim, a: integer;
  erro: Retorno;
  datahora : TDatetime;
  resultado : SuccessProcesso;
begin
  try
    // verifica ambiente do cte
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add
      ('select ambiente_cte, certificado, logo, pdf_cte, ');
    dtmdados.IQuery1.sql.add
      ('schemma, proxy, porta, user_proxy, pass_proxy ');
    dtmdados.IQuery1.sql.add
      ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
    dtmdados.IQuery1.Parameters[0].value := GLBFilial;
    dtmdados.IQuery1.open;

    if dtmdados.IQuery1.FieldByName('ambiente_cte').AsString = 'H' then
    begin
      frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taHomologacao;
    end
    else
    begin
      frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taProducao;
    end;
    //frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCte :=
    //  IncludeTrailingPathDelimiter
    //  (ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));
    //frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar :=
    //  IncludeTrailingPathDelimiter
    ///  (ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));

    //frmMenu.ACBrCTe1.Configuracoes.Certificados.NumeroSerie :=
    //  ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

    // frmMenu.ACBrCte1.DACTe.Logo := ALLTRIM(dtmdados.iQuery1.FieldByName('logo').AsString);
    //frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSchemas :=
    //  ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
    dtmdados.IQuery1.close;

    // frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := true;

    sData := FormatDateTime('YYYY', qrMonitorcte_data.value) +
      FormatDateTime('MM', qrMonitorcte_data.value);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCte + '\' + sData
      + '\CTe\';
    sCte := qrMonitorCTE_CHAVE.AsString + '-cte.xml';

    frmMenu.ACBrCTe1.Conhecimentos.clear;
    frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + sCte);
    frmMenu.ACBrCTe1.Consultar;

    dtmdados.iQuery1.close;
    dtmdados.iQuery1.sql.clear;
    dtmdados.iQuery1.SQL.add('select COALESCE(user_ws,'''') user_ws, pass_ws, code_ws from tb_seguradora ');
    dtmdados.IQuery1.SQL.add('where fl_empresa = :0 and trunc(datafim) >= trunc(sysdate) ');
    dtmdados.IQuery1.Parameters[0].value := qrMonitorFL_EMPRESA.AsInteger;
    dtmdados.iQuery1.open;

    if qrMonitoruser_ws.value <> '' then
    begin
      erro := ATM.GetATMWebSvrPortType.averbaCTe
        (dtmDados.IQuery1.fieldbyname('user_ws').value,
        dtmDados.IQuery1.fieldbyname('pass_ws').value,
        dtmDados.IQuery1.fieldbyname('code_ws').value,
        frmMenu.ACBrCTe1.Conhecimentos.Items[0].XML);
        protocolo := resultado.protocolo;
        datahora := (resultado.dhAverbacao.AsDateTime);
      dtmdados.iQuery1.close;
      mcte.Visible := true;
      mcte.Clear;
      mcte.Lines.Add(erro.ToString);
      //resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
      //resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);

      if protocolo <> '' then
      begin
        {inic := Pos('<PROTOCOLONUMERO>', resultado) + 17;
        fim := Pos('</PROTOCOLONUMERO>', resultado);
        a := fim - inic;
        protocolo := copy(resultado, inic, a);

        inic := Pos('<DATA>', resultado) + 6;
        fim := Pos('</DATA>', resultado);
        a := fim - inic;
        data := copy(resultado, inic, a);

        inic := Pos('<HORA>', resultado) + 6;
        fim := Pos('</HORA>', resultado);
        a := fim - inic;
        hora := copy(resultado, inic, a);
        data := BuscaTroca(data, '-', '/');         }
        // dt := StrToDateTime(data + ' ' + hora);
        frmMenu.prc_averba.Parameters[0].value := protocolo;
        frmMenu.prc_averba.Parameters[1].value := datahora;
          //StrToDateTime(data + ' ' + hora);
        frmMenu.prc_averba.Parameters[2].value :=
          qrMonitorCOD_CONHECIMENTO.value;
        frmMenu.prc_averba.ExecProc;
      end;
    end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TfrmMonAverba.mCteClick(Sender: TObject);
begin
  mCte.Color := clWhite;
  mCte.Font.Style := [fsBold];
end;

procedure TfrmMonAverba.mCteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    mCte.clear;
    mCte.Visible := False;
  end;
  Pesquisa;
end;

procedure TfrmMonAverba.Pesquisa();
begin
  qrMonitor.close;
  qrMonitor.open;
  qrMonitor.First;
  lbTotReg.Caption := 'Total de Registros: ' + InttoStr(qrMonitor.RecordCount);
end;

procedure TfrmMonAverba.qrMonitorBeforeOpen(DataSet: TDataSet);
begin
  // showmessage(qrmonitor.SQL.Text);
  // QrMonitor.Parameters[0].value := glbfilial;
end;

function TfrmMonAverba.TagToNameTag(ChildNode: IXMLNode;
  NomeTag: String): String;
var
  I: integer;
begin
  Result := '';
  ChildNode.ChildNodes.First;
  if (ChildNode.NodeName = 'xs:element') then
  begin
    if ChildNode.GetAttributeNS('name', '') = NomeTag then
    begin
      if (ChildNode.ChildNodes.FindNode('xs:annotation') <> nil) then
      begin
        Result := ChildNode.ChildNodes.FindNode('xs:annotation')
          .ChildNodes.FindNode('xs:documentation').Text;
        Exit;
      end;
    end;
  end;
  for I := 0 to ChildNode.ChildNodes.Count - 1 do
  begin
    Result := TagToNameTag(ChildNode.ChildNodes.Nodes[I], NomeTag);
    if Result <> '' then
      Exit;
  end;
end;

end.
