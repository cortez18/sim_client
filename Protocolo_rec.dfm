object frmProtocolo_rec: TfrmProtocolo_rec
  Left = 0
  Top = 0
  Caption = 'Protocolo de Recebimento'
  ClientHeight = 494
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1184
    Height = 494
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Receber'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1176
        Height = 405
        Align = alClient
        DataSource = dtsPendente
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnTitleClick = JvDBGrid1TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'COD'
            Title.Caption = 'Reg'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RAZSOC'
            Title.Caption = 'Fornecedor'
            Width = 275
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_NOTA'
            Title.Caption = 'NF'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_NOTA'
            Title.Caption = 'DT NF'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_VENCIMENTO'
            Title.Caption = 'DT Vcto.'
            Width = 93
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_RECEBIMENTO'
            Title.Caption = 'DT Receb.'
            Width = 101
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_SOLCITACAO'
            Title.Caption = 'Pedido/Contrato'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USUARIO'
            Title.Caption = 'Usu'#225'rio'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FL_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Width = 500
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCLIFOR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DT_LANCAMENTO'
            Title.Caption = 'DT Lcto.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCGC'
            Visible = False
          end>
      end
      object Panel1: TPanel
        Left = 420
        Top = 180
        Width = 341
        Height = 41
        Caption = 'Gerando..............'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object Panel2: TPanel
        Left = 0
        Top = 405
        Width = 1176
        Height = 61
        Align = alBottom
        TabOrder = 2
        object Label10: TLabel
          Left = 360
          Top = 1
          Width = 76
          Height = 16
          Caption = 'Observa'#231#227'o :'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBNavigator1: TDBNavigator
          Left = 4
          Top = 2
          Width = 225
          Height = 39
          DataSource = dtsPendente
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
          TabOrder = 0
        end
        object btnGravar: TBitBtn
          Left = 248
          Top = 2
          Width = 101
          Height = 39
          Caption = 'Receber'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
            FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
            96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
            92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
            BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
            CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
            D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
            EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
            E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
            E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
            8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
            E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
            FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
            C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
            CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
            CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
            D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
            9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          TabOrder = 1
          TabStop = False
          OnClick = btnGravarClick
        end
        object Memo1: TMemo
          Left = 442
          Top = 0
          Width = 722
          Height = 53
          MaxLength = 250
          TabOrder = 2
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Recebido'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1176
        Height = 466
        Align = alClient
        DataSource = dtsRecebido
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnTitleClick = JvDBGrid2TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'COD'
            Title.Caption = 'Reg'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RAZSOC'
            Title.Caption = 'Fornecedor'
            Width = 275
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_NOTA'
            Title.Caption = 'NF'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_NOTA'
            Title.Caption = 'DT NF'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_VENCIMENTO'
            Title.Caption = 'DT Vcto.'
            Width = 93
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_RECEBIMENTO'
            Title.Caption = 'DT Receb.'
            Width = 101
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_SOLCITACAO'
            Title.Caption = 'Pedido/Contrato'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USUARIO'
            Title.Caption = 'Usu'#225'rio'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FL_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Width = 500
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCLIFOR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DT_LANCAMENTO'
            Title.Caption = 'DT Lcto.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCGC'
            Visible = False
          end>
      end
    end
  end
  object QPendente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select p.*, f.codclifor, f.razsoc, f.codcgc   '
      
        'from tb_protocolo_notas p left join cyber.rodcli f on p.forneced' +
        'or = f.codclifor '
      'where fl_status = '#39'E'#39
      
        'and cod not in (select cod from tb_protocolo_notas where fl_stat' +
        'us = '#39'R'#39')')
    Left = 40
    Top = 82
    object QPendenteCOD: TBCDField
      FieldName = 'COD'
      Precision = 32
      Size = 0
    end
    object QPendenteNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
      Size = 50
    end
    object QPendenteDT_NOTA: TDateTimeField
      FieldName = 'DT_NOTA'
    end
    object QPendenteFORNECEDOR: TStringField
      FieldName = 'FORNECEDOR'
      Size = 50
    end
    object QPendenteDT_VENCIMENTO: TDateTimeField
      FieldName = 'DT_VENCIMENTO'
    end
    object QPendenteDT_RECEBIMENTO: TDateTimeField
      FieldName = 'DT_RECEBIMENTO'
    end
    object QPendenteDT_LANCAMENTO: TDateTimeField
      FieldName = 'DT_LANCAMENTO'
    end
    object QPendenteDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
    end
    object QPendenteNR_SOLCITACAO: TStringField
      FieldName = 'NR_SOLCITACAO'
      Size = 50
    end
    object QPendenteUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QPendenteFL_STATUS: TStringField
      FieldName = 'FL_STATUS'
      Size = 1
    end
    object QPendenteOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 250
    end
    object QPendenteCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object QPendenteRAZSOC: TStringField
      FieldName = 'RAZSOC'
      Size = 80
    end
    object QPendenteCODCGC: TStringField
      FieldName = 'CODCGC'
    end
  end
  object dtsPendente: TDataSource
    DataSet = QPendente
    Left = 100
    Top = 84
  end
  object SPEntrada: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_GRAVA_PROTOCOLO'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VCOD'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'VCODFOR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VVCTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VDTNF'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VUSUA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VDTREC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'VPEDIDO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VNR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 40
    Top = 136
  end
  object QRecebido: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select p.*, f.codclifor, f.razsoc, f.codcgc   '
      
        'from tb_protocolo_notas p left join cyber.rodcli f on p.forneced' +
        'or = f.codclifor '
      
        'where cod in (select cod from tb_protocolo_notas where fl_status' +
        ' = '#39'R'#39')'
      'order by 1 desc, 11')
    Left = 200
    Top = 86
    object BCDField1: TBCDField
      FieldName = 'COD'
      Precision = 32
      Size = 0
    end
    object StringField1: TStringField
      FieldName = 'NR_NOTA'
      Size = 50
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DT_NOTA'
    end
    object StringField2: TStringField
      FieldName = 'FORNECEDOR'
      Size = 50
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'DT_VENCIMENTO'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DT_RECEBIMENTO'
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'DT_LANCAMENTO'
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'DT_ENTREGA'
    end
    object StringField3: TStringField
      FieldName = 'NR_SOLCITACAO'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'USUARIO'
    end
    object StringField5: TStringField
      FieldName = 'FL_STATUS'
      Size = 1
    end
    object StringField6: TStringField
      FieldName = 'OBSERVACAO'
      Size = 250
    end
    object BCDField2: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object StringField7: TStringField
      FieldName = 'RAZSOC'
      Size = 80
    end
    object StringField8: TStringField
      FieldName = 'CODCGC'
    end
  end
  object dtsRecebido: TDataSource
    DataSet = QRecebido
    Left = 260
    Top = 88
  end
end
