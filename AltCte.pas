unit AltCte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ImgList, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, JvComponentBase,
  JvBalloonHint, ADODB, ActnList, StdCtrls, Buttons, JvExStdCtrls, JvRichEdit,
  JvDBRichEdit, ComCtrls, JvExComCtrls, JvComCtrls, JvBaseEdits, JvDBControls,
  JvMaskEdit, DBCtrls, JvCombobox, Mask, JvExMask, JvToolEdit, ExtCtrls, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvExControls, JvGradient, Inifiles, ComObj;

type
  TfrmAltCTE = class(TForm)
    QNF: TADOQuery;
    dtsNF: TDataSource;
    Panel1: TPanel;
    JvPageControl2: TJvPageControl;
    TBDest: TTabSheet;
    Label1: TLabel;
    Panel2: TPanel;
    Label2: TLabel;
    Eddata: TJvDBDateEdit;
    JvPageControl3: TJvPageControl;
    TBOrigem: TTabSheet;
    Label13: TLabel;
    JvPageControl4: TJvPageControl;
    TBFinal: TTabSheet;
    QCTE: TADOQuery;
    dtsSolicitacao: TDataSource;
    EdCodOr: TJvDBCalcEdit;
    edCodDes: TJvDBCalcEdit;
    EdNmOri: TJvDBMaskEdit;
    edNmDes: TJvDBMaskEdit;
    JvGradient1: TJvGradient;
    JvGradient2: TJvGradient;
    JvGradient3: TJvGradient;
    Label14: TLabel;
    JvDBMaskEdit1: TJvDBMaskEdit;
    btnNFi: TBitBtn;
    btnNFa: TBitBtn;
    btnNFe: TBitBtn;
    btnNFs: TBitBtn;
    QCTECOD_CONHECIMENTO: TBCDField;
    QCTENR_CONHECIMENTO: TBCDField;
    QCTENR_CONHECIMENTO_COMP: TBCDField;
    QCTEDT_CONHECIMENTO: TDateTimeField;
    QCTEDT_CONHECIMENTO_COMP: TDateTimeField;
    QCTECOD_PAGADOR: TBCDField;
    QCTECOD_REMETENTE: TBCDField;
    QCTECOD_DESTINATARIO: TBCDField;
    QCTECOD_REDESPACHO: TBCDField;
    QCTEVL_DECLARADO_NF: TFloatField;
    QCTENR_QTDE_VOL: TFloatField;
    QCTENR_PESO_KG: TFloatField;
    QCTENR_PESO_CUB: TFloatField;
    QCTEVL_FRETE_PESO: TFloatField;
    QCTEVL_AD: TFloatField;
    QCTEVL_GRIS: TFloatField;
    QCTEVL_TR: TFloatField;
    QCTEVL_TDE: TFloatField;
    QCTEVL_OUTROS: TFloatField;
    QCTEVL_PEDAGIO: TFloatField;
    QCTEVL_FLUVIAL: TFloatField;
    QCTEVL_EXCED: TFloatField;
    QCTEVL_DESPACHO: TFloatField;
    QCTEVL_BALSA: TFloatField;
    QCTEVL_REDFLU: TFloatField;
    QCTEVL_AGEND: TFloatField;
    QCTEVL_PALLET: TFloatField;
    QCTEVL_PORTO: TFloatField;
    QCTEVL_ALFAND: TFloatField;
    QCTEVL_CANHOTO: TFloatField;
    QCTEVL_REENT: TFloatField;
    QCTEVL_DEVOL: TFloatField;
    QCTEVL_BASE: TFloatField;
    QCTEVL_ALIQUOTA: TFloatField;
    QCTEVL_IMPOSTO: TFloatField;
    QCTEVL_TOTAL: TFloatField;
    QCTENM_USUARIO: TStringField;
    QCTEOPERACAO: TBCDField;
    QCTEDS_TIPO_MERC: TStringField;
    QCTEDS_TIPO_FRETE: TStringField;
    QCTEDS_OBS: TStringField;
    QCTEDS_PLACA: TStringField;
    QCTEMOTORISTA: TBCDField;
    QCTEFL_STATUS: TStringField;
    QCTEFL_TIPO: TStringField;
    QCTECODMUNO: TBCDField;
    QCTECODMUND: TBCDField;
    QCTECFOP: TStringField;
    QCTENR_FATURA: TBCDField;
    QCTENR_TABELA: TBCDField;
    QCTEPEDIDO: TStringField;
    QCTENR_MANIFESTO: TBCDField;
    QCTEFL_EMPRESA: TBCDField;
    QCTENR_RPS: TBCDField;
    QCTEDS_CANCELAMENTO: TStringField;
    QCTENR_SERIE: TStringField;
    QCTEVL_IMPOSTO_GNRE: TFloatField;
    QCTEIMP_DACTE: TStringField;
    QCTEFL_CONTINGENCIA: TStringField;
    QCTEFRETE_IMPOSTO: TFloatField;
    QCTESEGURO_IMPOSTO: TFloatField;
    QCTEGRIS_IMPOSTO: TFloatField;
    QCTEOUTRAS_IMPOSTO: TFloatField;
    QCTEFLUVIAL_IMPOSTO: TFloatField;
    QCTEPEDAGIO_IMPOSTO: TFloatField;
    QCTEFL_TIPO_2: TStringField;
    QCTECTE_DATA: TDateTimeField;
    QCTECTE_CHAVE: TStringField;
    QCTECTE_PROT: TStringField;
    QCTECTE_XMOTIVO: TStringField;
    QCTECTE_RECIBO: TStringField;
    QCTECTE_STATUS: TBCDField;
    QCTECTE_XMSG: TStringField;
    QCTEFL_DDR: TStringField;
    QCTECTE_CAN_PROT: TStringField;
    QCTECTE_CHAVE_CTE_R: TStringField;
    QCTEVL_TAS: TFloatField;
    QCTECTE_DATA_CAN: TDateTimeField;
    QCTENR_LOTE: TBCDField;
    QCTEVL_TXT_CTE: TFloatField;
    QCTEVL_FRETE: TFloatField;
    QCTENR_CTE_ORIGEM: TBCDField;
    QCTEOBS_TRANSPARENCIA: TStringField;
    QCTEXML_VL_FRETE: TBCDField;
    QCTEXML_VL_PEDAGIO: TBCDField;
    QCTEXML_VL_OUTROS: TBCDField;
    QCTEXML_VL_EXCEDENTE: TBCDField;
    QCTEXML_VL_AD: TBCDField;
    QCTEXML_VL_GRIS: TBCDField;
    QCTEXML_VL_ENTREGA: TBCDField;
    QCTEXML_VL_SEGURO: TBCDField;
    QCTEXML_VL_TDE: TBCDField;
    QCTEXML_VL_TR: TBCDField;
    QCTEXML_VL_SEG_BALSA: TBCDField;
    QCTEXML_VL_REDEP_FLUVIAL: TBCDField;
    QCTEXML_VL_AGEND: TBCDField;
    QCTEXML_VL_PALLET: TBCDField;
    QCTEXML_VL_TAS: TBCDField;
    QCTEXML_VL_DESPACHO: TBCDField;
    QCTEXML_VL_ENTREGA_PORTO: TBCDField;
    QCTEXML_VL_ALFAND: TBCDField;
    QCTEXML_VL_CANHOTO: TBCDField;
    QCTEXML_VL_FLUVIAL: TBCDField;
    QCTEXML_VL_DEVOLUCAO: TBCDField;
    QCTEXML_VL_AJUDA: TBCDField;
    QCTEXML_VL_TX_CTE: TBCDField;
    QCTENF_VL_FRETE: TBCDField;
    QCTENF_VL_PEDAGIO: TBCDField;
    QCTENF_VL_EXCEDENTE: TBCDField;
    QCTENF_VL_AD: TBCDField;
    QCTENF_VL_GRIS: TBCDField;
    QCTENF_VL_ENTREGA: TBCDField;
    QCTENF_VL_SEGURO: TBCDField;
    QCTENF_VL_TDE: TBCDField;
    QCTENF_VL_TR: TBCDField;
    QCTENF_VL_SEG_BALSA: TBCDField;
    QCTENF_VL_REDEP_FLUVIAL: TBCDField;
    QCTENF_VL_AGEND: TBCDField;
    QCTENF_VL_PALLET: TBCDField;
    QCTENF_VL_TAS: TBCDField;
    QCTENF_VL_DESPACHO: TBCDField;
    QCTENF_VL_ENTREGA_PORTO: TBCDField;
    QCTENF_VL_ALFAND: TBCDField;
    QCTENF_VL_CANHOTO: TBCDField;
    QCTENF_VL_FLUVIAL: TBCDField;
    QCTENF_VL_DEVOLUCAO: TBCDField;
    QCTENF_VL_AJUDAR: TBCDField;
    QCTENF_VL_CTE: TBCDField;
    QCTENF_NR: TBCDField;
    QCTENF_VALORTT: TBCDField;
    QCTEEMAIL_ENVIADO: TStringField;
    QCTELOTE_SEFAZ: TStringField;
    QCTENR_ROMANEIO: TBCDField;
    QCTECOD_EXPEDIDOR: TBCDField;
    QCTESERIE_MAN: TStringField;
    QCTEFILIAL_MAN: TBCDField;
    QCTECLIENTE: TStringField;
    QCTENM_ORIGEM: TStringField;
    QCTENM_DESTINO: TStringField;
    QCTENM_REDESPACHO: TStringField;
    Label3: TLabel;
    JvDBCalcEdit1: TJvDBCalcEdit;
    JvDBMaskEdit2: TJvDBMaskEdit;
    JvDBMaskEdit3: TJvDBMaskEdit;
    QAlterar: TADOQuery;
    QAlterarCOD_CONHECIMENTO: TBCDField;
    QAlterarNR_CONHECIMENTO: TBCDField;
    QAlterarNR_CONHECIMENTO_COMP: TBCDField;
    QAlterarDT_CONHECIMENTO: TDateTimeField;
    QAlterarDT_CONHECIMENTO_COMP: TDateTimeField;
    QAlterarCOD_PAGADOR: TBCDField;
    QAlterarCOD_REMETENTE: TBCDField;
    QAlterarCOD_DESTINATARIO: TBCDField;
    QAlterarCOD_REDESPACHO: TBCDField;
    QAlterarVL_DECLARADO_NF: TFloatField;
    QAlterarNR_QTDE_VOL: TFloatField;
    QAlterarNR_PESO_KG: TFloatField;
    QAlterarNR_PESO_CUB: TFloatField;
    QAlterarVL_FRETE_PESO: TFloatField;
    QAlterarVL_AD: TFloatField;
    QAlterarVL_GRIS: TFloatField;
    QAlterarVL_TR: TFloatField;
    QAlterarVL_TDE: TFloatField;
    QAlterarVL_OUTROS: TFloatField;
    QAlterarVL_PEDAGIO: TFloatField;
    QAlterarVL_FLUVIAL: TFloatField;
    QAlterarVL_EXCED: TFloatField;
    QAlterarVL_DESPACHO: TFloatField;
    QAlterarVL_BALSA: TFloatField;
    QAlterarVL_REDFLU: TFloatField;
    QAlterarVL_AGEND: TFloatField;
    QAlterarVL_PALLET: TFloatField;
    QAlterarVL_PORTO: TFloatField;
    QAlterarVL_ALFAND: TFloatField;
    QAlterarVL_CANHOTO: TFloatField;
    QAlterarVL_REENT: TFloatField;
    QAlterarVL_DEVOL: TFloatField;
    QAlterarVL_BASE: TFloatField;
    QAlterarVL_ALIQUOTA: TFloatField;
    QAlterarVL_IMPOSTO: TFloatField;
    QAlterarVL_TOTAL: TFloatField;
    QAlterarNM_USUARIO: TStringField;
    QAlterarOPERACAO: TBCDField;
    QAlterarDS_TIPO_MERC: TStringField;
    QAlterarDS_TIPO_FRETE: TStringField;
    QAlterarDS_OBS: TStringField;
    QAlterarDS_PLACA: TStringField;
    QAlterarMOTORISTA: TBCDField;
    QAlterarFL_STATUS: TStringField;
    QAlterarFL_TIPO: TStringField;
    QAlterarCODMUNO: TBCDField;
    QAlterarCODMUND: TBCDField;
    QAlterarCFOP: TStringField;
    QAlterarNR_FATURA: TBCDField;
    QAlterarNR_TABELA: TBCDField;
    QAlterarPEDIDO: TStringField;
    QAlterarNR_MANIFESTO: TBCDField;
    QAlterarFL_EMPRESA: TBCDField;
    QAlterarNR_RPS: TBCDField;
    QAlterarDS_CANCELAMENTO: TStringField;
    QAlterarNR_SERIE: TStringField;
    QAlterarVL_IMPOSTO_GNRE: TFloatField;
    QAlterarIMP_DACTE: TStringField;
    QAlterarFL_CONTINGENCIA: TStringField;
    QAlterarFRETE_IMPOSTO: TFloatField;
    QAlterarSEGURO_IMPOSTO: TFloatField;
    QAlterarGRIS_IMPOSTO: TFloatField;
    QAlterarOUTRAS_IMPOSTO: TFloatField;
    QAlterarFLUVIAL_IMPOSTO: TFloatField;
    QAlterarPEDAGIO_IMPOSTO: TFloatField;
    QAlterarFL_TIPO_2: TStringField;
    QAlterarCTE_DATA: TDateTimeField;
    QAlterarCTE_CHAVE: TStringField;
    QAlterarCTE_PROT: TStringField;
    QAlterarCTE_XMOTIVO: TStringField;
    QAlterarCTE_RECIBO: TStringField;
    QAlterarCTE_STATUS: TBCDField;
    QAlterarCTE_XMSG: TStringField;
    QAlterarFL_DDR: TStringField;
    QAlterarCTE_CAN_PROT: TStringField;
    QAlterarCTE_CHAVE_CTE_R: TStringField;
    QAlterarVL_TAS: TFloatField;
    QAlterarCTE_DATA_CAN: TDateTimeField;
    QAlterarNR_LOTE: TBCDField;
    QAlterarVL_TXT_CTE: TFloatField;
    QAlterarVL_FRETE: TFloatField;
    QAlterarNR_CTE_ORIGEM: TBCDField;
    QAlterarOBS_TRANSPARENCIA: TStringField;
    QAlterarXML_VL_FRETE: TBCDField;
    QAlterarXML_VL_PEDAGIO: TBCDField;
    QAlterarXML_VL_OUTROS: TBCDField;
    QAlterarXML_VL_EXCEDENTE: TBCDField;
    QAlterarXML_VL_AD: TBCDField;
    QAlterarXML_VL_GRIS: TBCDField;
    QAlterarXML_VL_ENTREGA: TBCDField;
    QAlterarXML_VL_SEGURO: TBCDField;
    QAlterarXML_VL_TDE: TBCDField;
    QAlterarXML_VL_TR: TBCDField;
    QAlterarXML_VL_SEG_BALSA: TBCDField;
    QAlterarXML_VL_REDEP_FLUVIAL: TBCDField;
    QAlterarXML_VL_AGEND: TBCDField;
    QAlterarXML_VL_PALLET: TBCDField;
    QAlterarXML_VL_TAS: TBCDField;
    QAlterarXML_VL_DESPACHO: TBCDField;
    QAlterarXML_VL_ENTREGA_PORTO: TBCDField;
    QAlterarXML_VL_ALFAND: TBCDField;
    QAlterarXML_VL_CANHOTO: TBCDField;
    QAlterarXML_VL_FLUVIAL: TBCDField;
    QAlterarXML_VL_DEVOLUCAO: TBCDField;
    QAlterarXML_VL_AJUDA: TBCDField;
    QAlterarXML_VL_TX_CTE: TBCDField;
    QAlterarNF_VL_FRETE: TBCDField;
    QAlterarNF_VL_PEDAGIO: TBCDField;
    QAlterarNF_VL_EXCEDENTE: TBCDField;
    QAlterarNF_VL_AD: TBCDField;
    QAlterarNF_VL_GRIS: TBCDField;
    QAlterarNF_VL_ENTREGA: TBCDField;
    QAlterarNF_VL_SEGURO: TBCDField;
    QAlterarNF_VL_TDE: TBCDField;
    QAlterarNF_VL_TR: TBCDField;
    QAlterarNF_VL_SEG_BALSA: TBCDField;
    QAlterarNF_VL_REDEP_FLUVIAL: TBCDField;
    QAlterarNF_VL_AGEND: TBCDField;
    QAlterarNF_VL_PALLET: TBCDField;
    QAlterarNF_VL_TAS: TBCDField;
    QAlterarNF_VL_DESPACHO: TBCDField;
    QAlterarNF_VL_ENTREGA_PORTO: TBCDField;
    QAlterarNF_VL_ALFAND: TBCDField;
    QAlterarNF_VL_CANHOTO: TBCDField;
    QAlterarNF_VL_FLUVIAL: TBCDField;
    QAlterarNF_VL_DEVOLUCAO: TBCDField;
    QAlterarNF_VL_AJUDAR: TBCDField;
    QAlterarNF_VL_CTE: TBCDField;
    QAlterarNF_NR: TBCDField;
    QAlterarNF_VALORTT: TBCDField;
    QAlterarEMAIL_ENVIADO: TStringField;
    QAlterarLOTE_SEFAZ: TStringField;
    QAlterarNR_ROMANEIO: TBCDField;
    QAlterarCOD_EXPEDIDOR: TBCDField;
    QAlterarSERIE_MAN: TStringField;
    QAlterarFILIAL_MAN: TBCDField;
    Panel3: TPanel;
    btnAlTa: TBitBtn;
    btnSaTa: TBitBtn;
    btnCancelar: TBitBtn;
    btnFechar: TBitBtn;
    edRede: TJvCalcEdit;
    EdNMRede: TJvMaskEdit;
    QNFID_NF: TBCDField;
    QNFCODCON: TBCDField;
    QNFSERCON: TStringField;
    QNFNOTFIS: TStringField;
    QNFSERIEN: TStringField;
    QNFDATNOT: TDateTimeField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFPESCUB: TBCDField;
    QNFVLRMER: TBCDField;
    QNFDATINC: TDateTimeField;
    QNFNOTNFE: TStringField;
    QNFFL_EMPRESA: TBCDField;
    QNFESPECIE: TStringField;
    QNFORDCOM: TStringField;
    Panel4: TPanel;
    DBGNF: TJvDBGrid;
    btnAltChave: TBitBtn;
    btnSalvarCh: TBitBtn;
    btnIncluirNF: TBitBtn;
    SpNF: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botoes;
    procedure btnSaTaClick(Sender: TObject);
    procedure btnAlTaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure edRedeChange(Sender: TObject);
    procedure QCTEAfterOpen(DataSet: TDataSet);
    procedure btnAltChaveClick(Sender: TObject);
    procedure btnSalvarChClick(Sender: TObject);
    procedure btnIncluirNFClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    arChave: array [1 .. 500] of String;
    sCodigoBarras: String;
    sChaveNfe: String;
  end;

var
  frmAltCTE: TfrmAltCTE;

implementation

uses Dados, funcoes, Menu, CodigoBarras;

{$R *.dfm}

procedure TfrmAltCTE.botoes;
begin
  btnAlTa.enabled := not btnAlTa.enabled;
  btnSaTa.enabled := not btnSaTa.enabled;
  btnCancelar.enabled := not btnCancelar.enabled;

  edRede.Visible := not edRede.Visible;
  EdNMRede.Visible := not EdNMRede.Visible;

end;

procedure TfrmAltCTE.btnAlTaClick(Sender: TObject);
begin
  botoes;
  QAlterar.close;
  QAlterar.Parameters[0].value := QCTECOD_CONHECIMENTO.AsInteger;
  QAlterar.Open;
  QAlterar.edit;
  edRede.setfocus;
end;

procedure TfrmAltCTE.btnAltChaveClick(Sender: TObject);
var
  frmBarras: TfrmCodigoBarras;
  iContChave: integer;
begin
  iContChave := 1;
  arChave[iContChave] := '';
  btnAltChave.enabled := false;
  btnSalvarCh.enabled := true;
  frmBarras := TfrmCodigoBarras.Create_Chave(Self, 'frmOsTar');
  frmBarras.edCcliente.Caption := 'NOME: ' + QCTENM_ORIGEM.AsString;
  frmBarras.edNota.Caption := QNFNOTFIS.AsString;
  frmBarras.edSerie.Caption := QNFSERIEN.AsString;
  frmBarras.ShowModal();
  sChaveNfe := sCodigoBarras;
  arChave[iContChave] := sChaveNfe;
  inc(iContChave);

end;

procedure TfrmAltCTE.btnCancelarClick(Sender: TObject);
begin
  botoes;
  QAlterar.Cancel;
end;

procedure TfrmAltCTE.btnFecharClick(Sender: TObject);
begin
  close;
end;

procedure TfrmAltCTE.btnIncluirNFClick(Sender: TObject);
begin
  if QNF.RecordCount > 0 then
  begin
    ShowMessage('J� Existe NF, n�o tem porque incluir novamente !');
    exit;
  end;
  SpNF.Parameters[0].Value := QCTENR_CONHECIMENTO.AsInteger;
  SpNF.Parameters[1].Value := QCTEFL_EMPRESA.AsInteger;
  SpNF.ExecProc;
  QNF.close;
  QNF.Parameters[0].value := QCTENR_CONHECIMENTO.AsInteger;
  QNF.Parameters[1].value := QCTENR_SERIE.AsString;
  QNF.Parameters[2].value := QCTEFL_EMPRESA.AsInteger;
  QNF.Open;
end;

procedure TfrmAltCTE.btnSalvarChClick(Sender: TObject);
var
  reg: integer;
begin
  try
    if Application.Messagebox('Voc� Deseja Alterar esta Chave ?',
      'Alterar Chave', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      reg := QNFID_NF.AsInteger;
      QNF.edit;
      QNFNOTNFE.value := sChaveNfe;
      QNF.Post;
      QNF.close;
      QNF.Parameters[0].value := QCTENR_CONHECIMENTO.AsInteger;
      QNF.Parameters[1].value := QCTENR_SERIE.AsString;
      QNF.Parameters[2].value := QCTEFL_EMPRESA.AsInteger;
      QNF.Open;
      QNF.locate('id_nf', reg, []);
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  btnAltChave.enabled := true;
  btnSalvarCh.enabled := false;
end;

procedure TfrmAltCTE.btnSaTaClick(Sender: TObject);
begin
  try
    if Application.Messagebox('Voc� Deseja Alterar este CT-e ?', 'Alterar CT-e',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      QAlterarCOD_REDESPACHO.value := edRede.value;
    end;
    QAlterar.Post;
    QCTE.close;
    QCTE.Parameters[0].value := QAlterarCOD_CONHECIMENTO.AsInteger;
    QCTE.Open;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  botoes;
end;

procedure TfrmAltCTE.edRedeChange(Sender: TObject);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.add
    ('select razsoc from cyber.rodcli a where codclifor =:0 ');
  dtmdados.IQuery1.Parameters[0].value := edRede.value;
  dtmdados.IQuery1.Open;
  if dtmdados.IQuery1.eof then
  begin
    ShowMessage('C�digo n�o encontrado');
    edRede.setfocus;
  end
  else
    EdNMRede.Text := dtmdados.IQuery1.FieldByName('razsoc').value;
  dtmdados.IQuery1.close;
end;

procedure TfrmAltCTE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QAlterar.close;
  QCTE.close;
end;

procedure TfrmAltCTE.FormCreate(Sender: TObject);
begin
  // Verifica se o usu�rio � do TI
  dtmDados.qryAcesso.Close;
  dtmDados.qryAcesso.SQL.clear;
  dtmDados.qryAcesso.SQL.Add
    ('select * from tb_usuarios u where u.fl_status = ''A'' and u.cod_usuario = '
    + #39 + IntToStr(GLBCodUser) + #39' and u.cod_dpto = 3 ');
  dtmDados.qryAcesso.Open;
  if not dtmDados.qryAcesso.eof then
    btnIncluirNF.Visible := true
  else
    btnIncluirNF.Visible := false;
  dtmDados.qryAcesso.Close;
end;

procedure TfrmAltCTE.QCTEAfterOpen(DataSet: TDataSet);
begin
  QNF.close;
  QNF.Parameters[0].value := QCTENR_CONHECIMENTO.AsInteger;
  QNF.Parameters[1].value := QCTENR_SERIE.AsString;
  QNF.Parameters[2].value := QCTEFL_EMPRESA.AsInteger;
  QNF.Open;
end;

end.
