unit EdiDocCob;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, Buttons, ComCtrls,
  Gauges, ADODB, FileCtrl, JvExStdCtrls, JvListBox, JvDriveCtrls, JvCombobox;

type
  TfrmEdiDocCob = class(TForm)
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    btnImportar: TBitBtn;
    btnSair: TBitBtn;
    PB1: TGauge;
    Label5: TLabel;
    Label7: TLabel;
    QEDI: TADOQuery;
    JvDriveCombo1: TJvDriveCombo;
    JvDirectoryListBox1: TJvDirectoryListBox;
    FLB: TJvFileListBox;
    QOperacao: TADOQuery;
    QOperacaoOPERACAO: TStringField;
    QOperacaoCFOP: TStringField;
    QOperacaoCOD_OPERACAO: TIntegerField;
    QOperacaoEMPRESA: TIntegerField;
    QDoccob: TADOQuery;
    QTem: TADOQuery;
    QTemTEM: TBCDField;
    QDoccobCODCLIFOR: TBCDField;
    cbTodos: TCheckBox;
    Edit1: TEdit;
    procedure btnSairClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FLBClick(Sender: TObject);
    procedure processar(arquivo: String);
  private
  var
    erro: string;
  public
    { Public declarations }
  end;

var
  frmEdiDocCob: TfrmEdiDocCob;

implementation

uses Dados, funcoes, menu;

{$R *.dfm}

procedure TfrmEdiDocCob.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmEdiDocCob.FLBClick(Sender: TObject);
begin
  Label5.caption := FLB.FileName;
end;

procedure TfrmEdiDocCob.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QEDI.Close;
end;

procedure TfrmEdiDocCob.processar(arquivo: String);
var
  Txt: TextFile;
  Linha, dt, emit, fat: string;
  y, unb, unh, dem, des, dnf, tot: integer;
begin
  emit := '';
  dem := 0;
  btnSair.Enabled := false;
  { Assignfile(Txt,(label5.caption));
    Reset(Txt);
    y:=0;
    While not Eof(Txt) do
    begin
    Readln(Txt);
    y := y + 1;
    end;
    CloseFile(Txt); }
  Assignfile(Txt, (arquivo));
  Reset(Txt);
  PB1.MaxValue := y;
  y := 0;
  While not Eof(Txt) do
  begin
    y := 1 + y;
    Application.ProcessMessages;
    PB1.Progress := y;
    Readln(Txt, Linha);
    if alltrim(copy(Linha, 1, 1)) <> '' then
    begin
      if copy(Linha, 1, 3) = '000' then
      begin
        if copy(Linha, 84, 3) = 'COB' then
          erro := 'OK'
        else
          erro := '';

        QTem.Parameters[0].value := copy(Linha, 84, 12);
        QTem.Parameters[1].value := copy(Linha, 04, 35);
        QTem.Parameters[2].value := ExtractFileName(Label5.caption);
        QTem.Open;

        if (QTemTEM.value > 0) then
          erro := 'TEM';

        if erro = 'OK' then
        begin
          QEDI.Close;
          QEDI.SQL.Clear;
          QEDI.SQL.Add
            ('insert into tb_doccob_unb (identificacao, remetente, destinatario, arquivo'
            + ') values ( :0, :1, :2, :3)');
          QEDI.Parameters[0].value := copy(Linha, 84, 12);
          QEDI.Parameters[1].value := copy(Linha, 04, 35);
          QEDI.Parameters[2].value := copy(Linha, 39, 35);
          QEDI.Parameters[3].value := ExtractFileName(Label5.caption);
          QEDI.ExecSQL;
          dtmdados.IQuery1.Close;
          dtmdados.IQuery1.SQL.Clear;
          dtmdados.IQuery1.SQL.Add
            ('select max(codarquivo) cod from tb_doccob_unb');
          dtmdados.IQuery1.Open;
          unb := dtmdados.IQuery1.FieldByName('cod').value;
          dtmdados.IQuery1.Close;
        end;
      end;

      if (copy(Linha, 1, 3) = '551') and (erro = 'OK') then
      begin
        emit := copy(Linha, 04, 14);
        QEDI.Close;
        QEDI.SQL.Clear;
        QEDI.SQL.Add('insert into tb_doccob_tra (ind_controle, cnpj)' +
          'values ( :0, :1)');
        QEDI.Parameters[0].value := unb;
        QEDI.Parameters[1].value := copy(Linha, 04, 14);
        QEDI.ExecSQL;
        dtmdados.IQuery1.Close;
        dtmdados.IQuery1.SQL.Clear;
        dtmdados.IQuery1.SQL.Add
          ('select max(codarquivo) cod from tb_doccob_tra');
        dtmdados.IQuery1.Open;
        unh := dtmdados.IQuery1.FieldByName('cod').value;
        dtmdados.Query1.Close;
      end;

      if (copy(Linha, 1, 3) = '552') and (erro = 'OK') then
      begin
        fat := copy(Linha, 18, 10);
        QEDI.Close;
        QEDI.SQL.Clear;
        QEDI.SQL.Add
          ('insert into tb_doccob_fat (ind_controle, tipo_doc, fatura,' +
          'data, vcto, valor) values (:0, :1, :2, :3, :4, :5)');
        QEDI.Parameters[0].value := unh;
        QEDI.Parameters[1].value := copy(Linha, 14, 1);
        QEDI.Parameters[2].value := copy(Linha, 18, 10);
        QEDI.Parameters[3].value :=
          StrToDate(copy(Linha, 28, 02) + '/' + copy(Linha, 30, 02) + '/' +
          copy(Linha, 32, 04));
        QEDI.Parameters[4].value :=
          StrToDate(copy(Linha, 36, 02) + '/' + copy(Linha, 38, 02) + '/' +
          copy(Linha, 40, 04));
        QEDI.Parameters[5].value :=
          StrToFloat(copy(Linha, 44, 13) + ',' + copy(Linha, 57, 2));
        QEDI.ExecSQL;
        dtmdados.IQuery1.Close;
        dtmdados.IQuery1.SQL.Clear;
        dtmdados.IQuery1.SQL.Add
          ('select max(codarquivo) cod from tb_doccob_fat');
        dtmdados.IQuery1.Open;
        dem := dtmdados.IQuery1.FieldByName('cod').value;
        dtmdados.Query1.Close;
      end;

      if (copy(Linha, 1, 3) = '555') and (erro = 'OK') then
      begin
        QEDI.Close;
        QEDI.SQL.Clear;
        QEDI.SQL.Add
          ('insert into tb_doccob_cte (ind_controle, serie, numero, valor )' +
          'values (:0, :1, :2, :3)');
        QEDI.Parameters[0].value := dem;
        QEDI.Parameters[1].value := copy(Linha, 14, 05);
        QEDI.Parameters[2].value := copy(Linha, 19, 12);
        QEDI.Parameters[3].value :=
          StrToFloat(copy(Linha, 31, 13) + ',' + copy(Linha, 44, 2));
        QEDI.ExecSQL;

        QDoccob.Close;
        QDoccob.Parameters[0].value := copy(Linha, 19, 12);
        QDoccob.Parameters[1].value := copy(emit, 1, 8);
        QDoccob.Open;
        if not QDoccob.Eof then
        begin
          QEDI.Close;
          QEDI.SQL.Clear;
          QEDI.SQL.Add
            ('update tb_controle_custo set fatura = ltrim(:0,0) where codclifor = :1 and cte_custo = :2 and fatura is null');
          QEDI.Parameters[0].value := fat;
          QEDI.Parameters[1].value := QDoccobCODCLIFOR.AsInteger;
          QEDI.Parameters[2].value := copy(Linha, 19, 12);
          QEDI.ExecSQL;
          QEDI.Close;
        end;
      end;
    end;
  end;
  CloseFile(Txt);
end;

procedure TfrmEdiDocCob.btnImportarClick(Sender: TObject);
var
  i, y: integer;
  xml: TextFile;
begin
  y := 0;
  if cbTodos.checked = True then
  begin
    Edit1.text := FLB.FileName;
    FLB.ItemIndex := 0;
    PB1.MaxValue := FLB.Items.Count;
    for i := 0 to FLB.Items.Count - 1 do
    begin
      Assignfile(xml, FLB.FileName);
      Reset(xml);
      y := 1 + y;
      Application.ProcessMessages;
      PB1.Progress := y;
      Label5.caption := FLB.FileName;
      processar(FLB.FileName);
      CloseFile(xml);
      if erro = '' then
        RenameFile(FLB.FileName, FLB.FileName + '.OK')
      else
        RenameFile(FLB.FileName, FLB.FileName + '.DUP');
      FLB.ItemIndex := FLB.ItemIndex + 1;
      erro := '';
    end;
  end
  else
  begin
    processar(FLB.FileName);
    if erro = '' then
      RenameFile(FLB.FileName, FLB.FileName + '.OK')
    else
      RenameFile(FLB.FileName, FLB.FileName + '.DUP');
    erro := '';
  end;
  FLB.Update;

  btnSair.Enabled := True;
end;

end.
