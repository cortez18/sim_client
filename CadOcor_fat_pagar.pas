unit CadOcor_fat_pagar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, JvCombobox,
  System.ImageList, System.Actions;

type
  TfrmCadOcor_fat_pagar = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QOperacao: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    Edit1: TEdit;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    QOperacaoID: TBCDField;
    QOperacaoFATURA: TStringField;
    QOperacaoOCOR: TStringField;
    QOperacaoDESCRICAO: TStringField;
    cbOcor: TJvComboBox;
    edFatura: TEdit;
    QOperacaoDT_INC: TDateTimeField;
    QOperacaoUSER_INC: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure QOperacaoBeforeOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
  private
    tipo: String;
  public
    { Public declarations }
  end;

var
  frmCadOcor_fat_pagar: TfrmCadOcor_fat_pagar;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadOcor_fat_pagar.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadOcor_fat_pagar.btnInserirClick(Sender: TObject);
begin
  Restaura;
  tipo := 'I';
  cbOcor.ItemIndex := -1;
  Edit1.text := '';
  cbOcor.SetFocus;
end;

procedure TfrmCadOcor_fat_pagar.btnSalvarClick(Sender: TObject);
begin
  if UpperCase(cbOcor.text) = '' then
  begin
    ShowMessage('� Obrigat�rio a ocorr�ncia');
    cbOcor.SetFocus;
    exit;
  end;

  if Edit1.text = '' then
  begin
    ShowMessage('� Obrigat�rio a Descri��o da Ocorr�ncia');
    Edit1.SetFocus;
    exit;
  end;

  try
    if tipo = 'I' then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('select nvl(max(id),0) + 1 codigo from tb_ocor_fat_pagar');
      dtmdados.IQuery1.open;

      QOperacao.append;
      QOperacaoID.value := dtmdados.IQuery1.FieldByName('codigo').value;
    end
    else
    begin
      QOperacao.edit;
    end;
    QOperacaoDESCRICAO.value := UpperCase(Edit1.text);
    QOperacaoOCOR.value := UpperCase(cbOcor.text);
    QOperacaoFATURA.value := edFatura.text;
    QOperacaoUSER_INC.value := GLBUSER;
    QOperacao.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QOperacao.Close;
  QOperacao.open;
end;

procedure TfrmCadOcor_fat_pagar.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QOperacao.Cancel;
end;

procedure TfrmCadOcor_fat_pagar.btnAlterarClick(Sender: TObject);
begin
  Restaura;
  tipo := 'A';
  Edit1.text := QOperacaoDESCRICAO.value;
  cbOcor.ItemIndex := cbOcor.Items.IndexOf(QOperacaoOCOR.value);
  cbOcor.SetFocus;
end;

procedure TfrmCadOcor_fat_pagar.QOperacaoBeforeOpen(DataSet: TDataSet);
begin
  QOperacao.Parameters[0].value := edFatura.text;
end;

procedure TfrmCadOcor_fat_pagar.FormActivate(Sender: TObject);
begin
  QOperacao.open;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 +
    IntToStr(QOperacao.RecordCount);
end;

procedure TfrmCadOcor_fat_pagar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QOperacao.Close;
end;

procedure TfrmCadOcor_fat_pagar.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  // btnAlterar.enabled  := not btnAlterar.enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
    pnBotao.Visible := false
  else
    pnBotao.Visible := True;
end;

procedure TfrmCadOcor_fat_pagar.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QOperacao.First;
    2:
      QOperacao.Prior;
    3:
      QOperacao.Next;
    4:
      QOperacao.Last;
  end;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 +
    IntToStr(QOperacao.RecordCount);
  Screen.Cursor := crDefault;
end;

end.
