unit DetalhesCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, JvBaseEdits, Buttons, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, DB, ADODB;

type
  TfrmDetalhesCusto = class(TForm)
    Panel9: TPanel;
    JvDBGrid2: TJvDBGrid;
    QComp: TADOQuery;
    QCompPARA: TStringField;
    QCompVALOR: TBCDField;
    QCompDE: TStringField;
    QCompcusto: TFloatField;
    dtsComp: TDataSource;
    Panel1: TPanel;
    Label13: TLabel;
    edTotalCobr: TJvCalcEdit;
    edTotalTMS: TJvCalcEdit;
    btnFecharCtrc: TBitBtn;
    QCompDifer: TFloatField;
    edDifer: TJvCalcEdit;
    procedure btnFecharCtrcClick(Sender: TObject);
    procedure QCompCalcFields(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDetalhesCusto: TfrmDetalhesCusto;

implementation

{$R *.dfm}

uses RelCusto_frete_sim, dados, funcoes;

procedure TfrmDetalhesCusto.btnFecharCtrcClick(Sender: TObject);
begin
  close;
end;

procedure TfrmDetalhesCusto.FormActivate(Sender: TObject);
begin
  Self.caption := 'Detalhes do Custo Cobrado - ' + frmRelCusto_frete_sim.mdReprecte_custo.AsString;
  QComp.close;
  QComp.Parameters[0].value := frmRelCusto_frete_sim.mdRepreTransp.AsInteger;
  QComp.open;
  edTotalTMS.value := 0;
  edTotalCobr.Value := 0;
  edDifer.Value := 0;
  while not QComp.eof do
  begin
    edTotalCobr.Value := edTotalCobr.Value + QCompcusto.Value;
    edTotalTMS.value  := edTotalTMS.value + QCompVALOR.value;
    QComp.next;
  end;
  edDifer.value := edTotalCobr.Value - edTotalTMS.value;
end;

procedure TfrmDetalhesCusto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QComp.close;
end;

procedure TfrmDetalhesCusto.QCompCalcFields(DataSet: TDataSet);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.add('select distinct v.nome, v.valor from tb_cte_xml c left join tb_cte_xml_comp v on v.id = c.id ');
  dtmdados.IQuery1.sql.add('where numero = :0 and substr(v.emitente,1,8)  = :1 and v.nome = :2 ');
  dtmdados.IQuery1.Parameters[0].value := frmRelCusto_frete_sim.mdReprecte_custo.AsInteger;
  dtmdados.IQuery1.Parameters[1].value := copy(ApCarac(frmRelCusto_frete_sim.ledIdCliente.Text),1,8);// mdRepreTransp.AsInteger;
  dtmdados.IQuery1.Parameters[2].value := QCompDE.AsString;
  dtmdados.IQuery1.open;
  if not dtmdados.IQuery1.eof then
    QCompcusto.Value := dtmdados.IQuery1.FieldByName('valor').Value;
  dtmdados.IQuery1.close;

  QCompDifer.Value := QCompcusto.Value - QCompVALOR.Value;
end;

end.
