unit pre_fatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls,
  JvMemoryDataset;

type
  TfrmPre_fatura = class(TForm)
    Label10: TLabel;
    dtInicial: TJvDateEdit;
    btRelatorio: TBitBtn;
    Gauge1: TGauge;
    Panel1: TPanel;
    JvDBNavigator1: TJvDBNavigator;
    QFornecedor: TADOQuery;
    QFornecedorNM_FORNECEDOR: TStringField;
    QFornecedorNR_CNPJ_CPF: TStringField;
    dtsFornecedor: TDataSource;
    QFornecedorTRANSP: TBCDField;
    JvDBGrid2: TJvDBGrid;
    mdFatura: TJvMemoryData;
    mdFaturacodigo: TIntegerField;
    mdFaturatransportador: TStringField;
    mdFaturacnpj: TStringField;
    mdFaturaseleciona: TStringField;
    cbT: TCheckBox;
    Label11: TLabel;
    dtFinal: TJvDateEdit;
    BitBtn1: TBitBtn;
    SP_PRE: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbTClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid2CellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPre_fatura: TfrmPre_fatura;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmPre_fatura.BitBtn1Click(Sender: TObject);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.SQL.add('delete from TB_ENVIO_FATURA');
  dtmdados.IQuery1.ExecSQL;
  dtmdados.IQuery1.close;
  Gauge1.MaxValue := mdFatura.RecordCount;
  mdFatura.First;
  While not mdFatura.Eof do
  begin
    if mdFaturaseleciona.Value = 'S' then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.Clear;
      dtmdados.IQuery1.SQL.add('insert into TB_ENVIO_FATURA ( ');
      dtmdados.IQuery1.SQL.add('codigo, data) values (') ;
      dtmdados.IQuery1.SQL.add(':0, :1) ');
      dtmdados.IQuery1.Parameters[0].value := mdFaturacodigo.AsInteger;
      dtmdados.IQuery1.Parameters[1].value := dtInicial.Date;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;
    end;
    Gauge1.AddProgress(1);
    mdFatura.Next;
  end;
  SP_Pre.Prepared := true;
  SP_PRE.ExecProc;
  showmessage('Arquivos sendo processados e enviados por e-mail');
end;

procedure TfrmPre_fatura.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;

  QFornecedor.Close;
  QFornecedor.SQL[2] := ('where v.data between to_date(''' + dtInicial.text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text +
      ' 23:59'',''dd/mm/yy hh24:mi'')');
  //showmessage(QFornecedor.sql.Text);
  QFornecedor.Open;
  QFornecedor.First;
  mdFatura.close;
  mdFatura.open;
  While not QFornecedor.Eof do
  begin
    mdFatura.Append;
    mdFaturacodigo.Value := QFornecedorTRANSP.AsInteger;
    mdFaturatransportador.Value := QFornecedorNM_FORNECEDOR.AsString;
    mdFaturacnpj.Value := QFornecedorNR_CNPJ_CPF.AsString;
    mdFaturaseleciona.Value := 'S';
    mdfatura.Post;
    QFornecedor.Next;
  end;
  QFornecedor.close;
  Panel1.Visible := false;
end;

procedure TfrmPre_fatura.cbTClick(Sender: TObject);
begin
  Application.ProcessMessages;
  if cbT.Checked then
  begin
    mdfatura.First;
    while not mdFatura.Eof do
    begin
      mdFatura.edit;
      mdFaturaseleciona.Value := 'S';
      mdFatura.Post;
      mdFatura.Next;
    end;
  end
  else
  begin
    mdFatura.First;
    while not mdFatura.Eof do
    begin
      mdFatura.edit;
      mdFaturaseleciona.Value := 'N';
      mdFatura.Post;
      mdFatura.Next;
    end;
  end;
end;

procedure TfrmPre_fatura.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmPre_fatura.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmPre_fatura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QFornecedor.close;
end;

procedure TfrmPre_fatura.JvDBGrid2CellClick(Column: TColumn);
begin
  if (Column.Field = mdFaturaseleciona) then
  begin
    mdFatura.edit;
    if (mdFaturaseleciona.Value = 'N') then
      mdFaturaseleciona.Value := 'S'
    else
      mdFaturaseleciona.Value := 'N';
    mdFatura.Post;
  end;
end;

procedure TfrmPre_fatura.JvDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdFaturaseleciona) then
  begin
    JvDBGrid2.canvas.FillRect(Rect);
    dtmdados.iml.Draw(JvDBGrid2.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdFaturaseleciona.Value = 'S' then
      dtmdados.iml.Draw(JvDBGrid2.canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(JvDBGrid2.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;


end.
