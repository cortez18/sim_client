unit AlterarTransportes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, xmldom, XMLIntf, msxmldom, XMLDoc,
  IniFiles,
  DB, ADODB, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls;

type
  TfrmAlteracao_transp = class(TForm)
    XMLTransp: TXMLDocument;
    ADOQTranspaltera: TADOQuery;
    ADOQupdateCLESS: TADOQuery;
    adoqmunicipios: TADOQuery;
    adoqupdateSPcless: TADOQuery;
    ADOQTranspalteraID_SPEDITEUR: TStringField;
    adoqmunicipiosORT: TStringField;
    AdoconsultaID: TADOQuery;
    AdoAlteraTranspPedido: TADOQuery;
    Qcliente: TADOQuery;
    QclienteID_KLIENT: TStringField;
    QclienteNAME: TStringField;
    QclienteSUCHBEGRIFF: TStringField;
    QUpdateAlterTransp: TADOQuery;
    QAlterTransp: TADOQuery;
    QInsertAlterTransp: TADOQuery;
    QAlterTranspCLIENTE: TStringField;
    QAlterTranspTRANSPORTADORA: TStringField;
    QAlterTranspUF: TStringField;
    QAlterTranspMUNICIPIO: TStringField;
    AdoconsultaIDDESCRI: TStringField;
    AdoconsultaIDESTADO: TStringField;
    AdoconsultaIDCODIBG: TStringField;
    QCadastrado: TADOQuery;
    dtsCadastrado: TDataSource;
    QCadastradoORT: TStringField;
    QCadastradoNAME_2: TStringField;
    QCadastradoSUCHBEGRIFF: TStringField;
    Cliente: TADOQuery;
    ClienteCLIENTE: TStringField;
    ClienteBASE: TStringField;
    ClienteSITE: TStringField;
    PNSP: TPanel;
    LBLcount: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    cbUF: TComboBox;
    cbcliente: TComboBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    tbalterar: TTabSheet;
    Label4: TLabel;
    Label5: TLabel;
    GroupBox3: TGroupBox;
    Button3: TButton;
    LBmunicipios: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button4: TButton;
    lbmunicselecionados: TListBox;
    btalterarsp: TBitBtn;
    CBtranspSP: TComboBox;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    Panel2: TPanel;
    btvoltarsp: TBitBtn;
    procedure cbUFChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btvoltarspClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btalterarspClick(Sender: TObject);
    procedure cbclienteChange(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteracao_transp: TfrmAlteracao_transp;
  vXMLDoc: TXMLDocument;
  NodePai, NodeSec, NodeTmp: IXMLNode;
  infNo: IXMLNodeList;
  arq: TIniFile;
  arqLog: TextFile;
  localXml, localLog, logarq: String;

implementation

uses Dados, menu, funcoes;

{$R *.dfm}

procedure TfrmAlteracao_transp.btalterarspClick(Sender: TObject);
var
  i, j: integer;
  mun, teste: string;

begin
  adoqupdateSPcless.Close;
  adoqupdateSPcless.Parameters[0].Value := CBtranspSP.Text;
  adoqupdateSPcless.Parameters[1].Value := CBtranspSP.Text;
  adoqupdateSPcless.Parameters[2].Value := cbUF.Text;
  adoqupdateSPcless.Parameters[3].Value := cbcliente.Text;
  // inclus�o dos municipios SP
  if lbmunicselecionados.Count = 1 then
  begin
    adoqupdateSPcless.SQL[3] := 'and ort in (''' +
      lbmunicselecionados.items.Strings[0] + ''')';
    // AdoconsultaID.SQL[2]  := 'and ort in ('''+lbmunicselecionados.items.Strings[0]+''')';
    QAlterTransp.Close;
    QAlterTransp.Parameters[0].Value := cbcliente.Text;
    // QAlterTransp.Parameters[1].Value := CBtranspSP.text;
    QAlterTransp.Parameters[1].Value := cbUF.Text;
    QAlterTransp.Parameters[2].Value := lbmunicselecionados.items.Strings[0];
    QAlterTransp.Open;

    AdoconsultaID.Close;
    AdoconsultaID.Parameters[0].Value := lbmunicselecionados.items.Strings[0];
    AdoconsultaID.Parameters[1].Value := cbUF.Text;
    AdoconsultaID.Open;

    if (QAlterTransp.IsEmpty = true) then
    begin
      QInsertAlterTransp.Close;
      QInsertAlterTransp.Parameters[0].Value := cbcliente.Text;
      QInsertAlterTransp.Parameters[1].Value := CBtranspSP.Text;
      QInsertAlterTransp.Parameters[2].Value := cbUF.Text;
      QInsertAlterTransp.Parameters[3].Value :=
        lbmunicselecionados.items.Strings[0];
      QInsertAlterTransp.Parameters[4].Value := AdoconsultaIDCODIBG.AsString;
      QInsertAlterTransp.ExecSQL;
    end
    else
    begin
      QUpdateAlterTransp.Close;
      QUpdateAlterTransp.Parameters[0].Value := CBtranspSP.Text;
      QUpdateAlterTransp.Parameters[1].Value := AdoconsultaIDCODIBG.AsString;
      QUpdateAlterTransp.Parameters[2].Value := cbcliente.Text;
      QUpdateAlterTransp.Parameters[3].Value := cbUF.Text;
      QUpdateAlterTransp.Parameters[4].Value :=
        lbmunicselecionados.items.Strings[0];
      QUpdateAlterTransp.ExecSQL;
    end;
  end
  else
  begin
    for i := 0 to lbmunicselecionados.items.Count - 1 do
    begin
      if (lbmunicselecionados.Count - 1) <> i then
      begin
        mun := mun + '''' + lbmunicselecionados.items.Strings[i] + '''' + ',';
        QAlterTransp.Close;
        QAlterTransp.Parameters[0].Value := cbcliente.Text;
        // QAlterTransp.Parameters[1].Value := CBtranspSP.text;
        QAlterTransp.Parameters[1].Value := cbUF.Text;
        QAlterTransp.Parameters[2].Value :=
          lbmunicselecionados.items.Strings[i];
        QAlterTransp.Open;

        AdoconsultaID.Close;
        AdoconsultaID.Parameters[0].Value :=
          lbmunicselecionados.items.Strings[i];
        AdoconsultaID.Parameters[1].Value := cbUF.Text;
        AdoconsultaID.Open;

        if (QAlterTransp.IsEmpty = true) then
        begin
          QInsertAlterTransp.Close;
          QInsertAlterTransp.Parameters[0].Value := cbcliente.Text;
          QInsertAlterTransp.Parameters[1].Value := CBtranspSP.Text;
          QInsertAlterTransp.Parameters[2].Value := cbUF.Text;
          QInsertAlterTransp.Parameters[3].Value :=
            lbmunicselecionados.items.Strings[i];
          QInsertAlterTransp.Parameters[4].Value :=
            AdoconsultaIDCODIBG.AsString;
          QInsertAlterTransp.ExecSQL;
        end
        else
        begin
          QUpdateAlterTransp.Close;
          QUpdateAlterTransp.Parameters[0].Value := CBtranspSP.Text;
          QUpdateAlterTransp.Parameters[1].Value :=
            AdoconsultaIDCODIBG.AsString;
          QUpdateAlterTransp.Parameters[2].Value := cbcliente.Text;
          QUpdateAlterTransp.Parameters[3].Value := cbUF.Text;
          QUpdateAlterTransp.Parameters[4].Value :=
            lbmunicselecionados.items.Strings[i];
          QUpdateAlterTransp.ExecSQL;
        end;

      end
      else
      begin

        mun := mun + '''' + lbmunicselecionados.items.Strings[i] + '''';
        QAlterTransp.Close;
        QAlterTransp.Parameters[0].Value := cbcliente.Text;
        // QAlterTransp.Parameters[1].Value := CBtranspSP.text;
        QAlterTransp.Parameters[1].Value := cbUF.Text;
        QAlterTransp.Parameters[2].Value :=
          lbmunicselecionados.items.Strings[i];
        QAlterTransp.Open;

        AdoconsultaID.Close;
        AdoconsultaID.Parameters[0].Value :=
          lbmunicselecionados.items.Strings[i];
        AdoconsultaID.Parameters[1].Value := cbUF.Text;
        AdoconsultaID.Open;

        if (QAlterTransp.IsEmpty = true) then
        begin
          QInsertAlterTransp.Close;
          QInsertAlterTransp.Parameters[0].Value := cbcliente.Text;
          QInsertAlterTransp.Parameters[1].Value := CBtranspSP.Text;
          QInsertAlterTransp.Parameters[2].Value := cbUF.Text;
          QInsertAlterTransp.Parameters[3].Value :=
            lbmunicselecionados.items.Strings[i];
          QInsertAlterTransp.Parameters[4].Value :=
            AdoconsultaIDCODIBG.AsString;
          QInsertAlterTransp.ExecSQL;
        end
        else
        begin
          QUpdateAlterTransp.Close;
          QUpdateAlterTransp.Parameters[0].Value := CBtranspSP.Text;
          QUpdateAlterTransp.Parameters[1].Value :=
            AdoconsultaIDCODIBG.AsString;
          QUpdateAlterTransp.Parameters[2].Value := cbcliente.Text;
          QUpdateAlterTransp.Parameters[3].Value := cbUF.Text;
          QUpdateAlterTransp.Parameters[4].Value :=
            lbmunicselecionados.items.Strings[i];
          QUpdateAlterTransp.ExecSQL;
        end;
      end;
    end;
    // teste := adoqupdateSPcless.SQL.Text;
    adoqupdateSPcless.SQL[3] := 'and ort in (' + mun + ')';
    // AdoconsultaID.SQL[2] := 'and ort in ('+mun+')';
  end;
  teste := adoqupdateSPcless.SQL.Text;
  adoqupdateSPcless.ExecSQL;
  // gravar log com altera��o, data e usu�rio que realizou
  Append(arqLog);
  writeln(arqLog,
    '**************************************************************************************************************');
  writeln(arqLog, 'Foi alterado na UF SP para a Transportadora ' +
    CBtranspSP.Text + ' pelo usu�rio ' + GLBUSER + ' na data ' +
    DateTimeToStr(Date) + ' ' + TimeToStr(Time) + ' para o cliente ' +
    cbcliente.Text + ' os munic�pios: ' + mun + '');
  CloseFile(arqLog);
  // retorna os municipios para listbox 1
  // LBmunicipios.Items.Text := LBmunicipios.Items.Text + lbmunicselecionados.Items.Text;
  lbmunicselecionados.Clear;
  LBmunicipios.Clear;
  LBLcount.Caption := inttostr(lbmunicselecionados.Count);
  CBtranspSP.ItemIndex := 0;

  adoqmunicipios.Close;
  adoqmunicipios.Open;
  while not adoqmunicipios.Eof do
  Begin

    LBmunicipios.items.Add(adoqmunicipios.Fields.FieldByName('ort').AsString);
    adoqmunicipios.Next;
  End;

  ShowMessage('Transportadora do cliente ' + cbcliente.Text +
    ' para os munic�pios do(a) ' + cbUF.Text + ' alteradas com sucesso!');
  CBtranspSP.ItemIndex := -1;
  btalterarsp.Enabled := false;
  Button2.Enabled := false;
  Button4.Enabled := false;
end;

procedure TfrmAlteracao_transp.btvoltarspClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAlteracao_transp.Button1Click(Sender: TObject);
begin
  LBmunicipios.MoveSelection(lbmunicselecionados);
  LBLcount.Caption := inttostr(lbmunicselecionados.Count);
  btalterarsp.Enabled := true;
  if lbmunicselecionados.Count > 0 then
  begin
    btalterarsp.Enabled := true;
    Button2.Enabled := true;
    Button4.Enabled := true;
  end;

end;

procedure TfrmAlteracao_transp.Button2Click(Sender: TObject);
begin
  lbmunicselecionados.MoveSelection(LBmunicipios);
  LBLcount.Caption := inttostr(lbmunicselecionados.Count);
  if lbmunicselecionados.Count = 0 then
  begin
    btalterarsp.Enabled := false;
    Button2.Enabled := false;
    Button4.Enabled := false;
  end;
end;

procedure TfrmAlteracao_transp.Button3Click(Sender: TObject);
begin
  lbmunicselecionados.items.Text := lbmunicselecionados.items.Text +
    LBmunicipios.items.Text;
  LBmunicipios.Clear;
  LBLcount.Caption := inttostr(lbmunicselecionados.Count);
  btalterarsp.Enabled := true;
  if lbmunicselecionados.Count > 0 then
  begin
    btalterarsp.Enabled := true;
    Button2.Enabled := true;
    Button4.Enabled := true;
  end;
end;

procedure TfrmAlteracao_transp.Button4Click(Sender: TObject);
begin
  LBmunicipios.items.Text := LBmunicipios.items.Text +
    lbmunicselecionados.items.Text;
  lbmunicselecionados.Clear;
  LBLcount.Caption := inttostr(lbmunicselecionados.Count);
  btalterarsp.Enabled := false;
  if lbmunicselecionados.Count = 0 then
  begin
    btalterarsp.Enabled := false;
    Button2.Enabled := false;
    Button4.Enabled := false;
  end;
end;

procedure TfrmAlteracao_transp.cbclienteChange(Sender: TObject);
begin
  // Preenche o combobox com as transportadoras com cnpj  03857930000154 e se estiver ativo

  LBmunicipios.Clear;
  adoqmunicipios.Close;
  adoqmunicipios.Parameters[0].Value := cbcliente.Text;
  adoqmunicipios.Parameters[1].Value := cbUF.Text;
  adoqmunicipios.Open;

  while not adoqmunicipios.Eof do
  Begin
    LBmunicipios.items.Add(adoqmunicipios.Fields.FieldByName('ort').AsString);
    adoqmunicipios.Next;
  End;

  QCadastrado.Close;
  QCadastrado.Parameters[0].Value := cbcliente.Text;
  QCadastrado.Parameters[1].Value := cbUF.Text;
  QCadastrado.Open;

end;

procedure TfrmAlteracao_transp.cbUFChange(Sender: TObject);
begin
  cbcliente.ItemIndex := -1;
  LBmunicipios.Clear;
  lbmunicselecionados.Clear;
end;

procedure TfrmAlteracao_transp.FormCreate(Sender: TObject);
begin
  // Preenche o combobox com as transportadoras com cnpj  03857930000154 e se estiver ativo
  ADOQTranspaltera.Close;
  ADOQTranspaltera.Open;

  while not ADOQTranspaltera.Eof do
  Begin
    CBtranspSP.items.Add(ADOQTranspaltera.Fields.FieldByName('ID_SPEDITEUR')
      .AsString);
    ADOQTranspaltera.Next;
  End;
  // Preencher combobox dos clientes
  cliente.Close;
  cliente.Open;

  while not cliente.Eof do
  Begin
    cbcliente.items.Add(cliente.Fields.FieldByName('cliente').AsString);
    cliente.Next;
  End;

  arq := TIniFile.Create(ExtractFilePath(Application.exeName) + '\SIM.ini');
  localXml := arq.ReadString('XML', 'caminho', '');
  localLog := arq.ReadString('XML', 'log', '');

end;

procedure TfrmAlteracao_transp.FormShow(Sender: TObject);
begin
  XMLTransp.FileName := localXml;

  logarq := '' + localLog + 'LogTransp.txt';
  AssignFile(arqLog, '' + localLog + 'LogTransp.txt');
  if not FileExists(logarq) then
    Rewrite(arqLog)
  else
  begin
    Append(arqLog);
    CloseFile(arqLog);
  end;
end;

procedure TfrmAlteracao_transp.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex = 0 then
  begin
    ADOQupdateCLESS.Connection := dtmDados.ConWmsWeb;
    ADOQTranspaltera.Connection := dtmDados.ConWmsWeb;
    Qcliente.Connection := dtmDados.ConWmsWeb;
    adoqmunicipios.Connection := dtmDados.ConWmsWeb;
    adoqupdateSPcless.Connection := dtmDados.ConWmsWeb;
    AdoAlteraTranspPedido.Connection := dtmDados.ConWmsWeb;
    QCadastrado.Connection := dtmDados.ConWmsWeb;
  end
  else
  begin
    ADOQupdateCLESS.Connection := dtmDados.ConWMSV11;
    ADOQTranspaltera.Connection := dtmDados.ConWMSV11;
    Qcliente.Connection := dtmDados.ConWMSV11;
    adoqmunicipios.Connection := dtmDados.ConWMSV11;
    adoqupdateSPcless.Connection := dtmDados.ConWMSV11;
    AdoAlteraTranspPedido.Connection := dtmDados.ConWMSV11;
    QCadastrado.Connection := dtmDados.ConWMSV11;
  end;
end;

end.
