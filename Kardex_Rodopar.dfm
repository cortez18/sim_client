object frmKardex_Rodopar: TfrmKardex_Rodopar
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio Kardex - Rodopar'
  ClientHeight = 544
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 12
    Top = 6
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 519
    Width = 754
    Height = 23
    Progress = 0
  end
  object Label8: TLabel
    Left = 499
    Top = 100
    Width = 47
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object lblDescricao: TLabel
    Left = 188
    Top = 6
    Width = 38
    Height = 13
    Caption = 'Produto'
    Transparent = True
  end
  object Label10: TLabel
    Left = 480
    Top = 6
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 584
    Top = 6
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 324
    Top = 6
    Width = 74
    Height = 13
    Caption = 'Produto Cliente'
    Transparent = True
  end
  object btRelatorio: TBitBtn
    Left = 723
    Top = 12
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 1
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 12
    Top = 22
    Width = 170
    Height = 21
    LookupDisplay = 'NOMEAB'
    LookupField = 'CODCLIFOR'
    LookupSource = DataSource1
    TabOrder = 0
    Text = ''
  end
  object btnExcel: TBitBtn
    Left = 826
    Top = 12
    Width = 75
    Height = 25
    Caption = 'Excel'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
      346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
      2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
      33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
      38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
      3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
      40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
      73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
      33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
      64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
      33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
      33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
      3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
      D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
      49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
      CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
      53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
      C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
      BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
      62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
      56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
      45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
      4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
      4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
      49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
      4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
      4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btnExcelClick
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 49
    Width = 960
    Height = 464
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'CODOSA'
        Title.Caption = 'OS'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FILOSA'
        Title.Caption = 'Filial'
        Width = 39
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODPRO'
        Title.Caption = 'Produto'
        Width = 86
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REFERE'
        Title.Caption = 'Produto Cliente'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODCLIFOR'
        Title.Caption = 'Cliente'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODLOT'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'NOTFIS'
        Title.Caption = 'N.F. Ent.'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SERIEN'
        Title.Caption = 'S'#233'rie'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATDOC'
        Title.Caption = 'Data Doc.'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'TIPDOC'
        Title.Caption = 'E / S'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUANTI'
        Title.Caption = 'Quant.'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ANTFIS'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'SALFIS'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ANTFIN'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DATATU'
        Title.Caption = 'Data Movim.'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USUATU'
        Title.Caption = 'Usu'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUBLOT'
        Title.Caption = 'Sub Lote'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_IOS'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REFCLI'
        Title.Caption = 'NF. Cliente'
        Width = 56
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 224
    Top = 176
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 498
    Top = 136
    Width = 157
    Height = 469
    TabOrder = 5
    Visible = False
  end
  object DriveComboBox1: TDriveComboBox
    Left = 498
    Top = 116
    Width = 157
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 6
    Visible = False
  end
  object Memo1: TMemo
    Left = 300
    Top = 244
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Visible = False
  end
  object edProd: TEdit
    Left = 188
    Top = 22
    Width = 99
    Height = 21
    TabOrder = 8
    OnExit = edProdExit
  end
  object dtInicial: TJvDateEdit
    Left = 480
    Top = 22
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 9
  end
  object dtFinal: TJvDateEdit
    Left = 584
    Top = 22
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 10
  end
  object Edit1: TEdit
    Left = 324
    Top = 22
    Width = 99
    Height = 21
    TabOrder = 11
    OnExit = Edit1Exit
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 7
    Top = 518
    Width = 190
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
    TabOrder = 12
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select z.codosa, z.filosa, z.codpro, p.refere, z.codclifor, z.no' +
        'tfis, z.serien, z.entsai tipdoc,'
      
        'z.qtdade quanti, z.datatu, z.usuatu, z.id_ios, z.datdoc datemi, ' +
        'o.refcli'
      
        'from cyber.wmsraz z  left join cyber.wmspro p on z.codpro = p.co' +
        'digo'
      
        '                     left join cyber.wmsosa o on o.codosa = z.co' +
        'dosa'
      'where situac <> '#39'C'#39
      ''
      ''
      ''
      ''
      ''
      'order by 10')
    Left = 24
    Top = 124
    object QCtrcCODOSA: TBCDField
      FieldName = 'CODOSA'
      Precision = 32
    end
    object QCtrcFILOSA: TBCDField
      FieldName = 'FILOSA'
      Precision = 32
    end
    object QCtrcCODPRO: TBCDField
      FieldName = 'CODPRO'
      Precision = 32
    end
    object QCtrcREFERE: TStringField
      FieldName = 'REFERE'
      Size = 30
    end
    object QCtrcCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object QCtrcNOTFIS: TStringField
      FieldName = 'NOTFIS'
      Size = 15
    end
    object QCtrcSERIEN: TStringField
      FieldName = 'SERIEN'
      Size = 10
    end
    object QCtrcTIPDOC: TStringField
      FieldName = 'TIPDOC'
      FixedChar = True
      Size = 1
    end
    object QCtrcQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 14
    end
    object QCtrcDATATU: TDateTimeField
      FieldName = 'DATATU'
    end
    object QCtrcUSUATU: TStringField
      FieldName = 'USUATU'
    end
    object QCtrcID_IOS: TBCDField
      FieldName = 'ID_IOS'
      Precision = 32
    end
    object QCtrcREFCLI: TStringField
      FieldName = 'REFCLI'
      Size = 15
    end
    object QCtrcDATEMI: TDateTimeField
      FieldName = 'DATEMI'
    end
  end
  object navnavig: TDataSource
    DataSet = QCtrc
    Left = 80
    Top = 124
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct r.codclifor, c.nomeab'
      'from wmsraz r left join rodcli c on r.codclifor = c.codclifor'
      'order by 2')
    Left = 24
    Top = 80
    object QClienteCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object QClienteNOMEAB: TStringField
      FieldName = 'NOMEAB'
      ReadOnly = True
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 84
    Top = 80
  end
  object ADOQuery1: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select r.*, o.refcli, p.refere '
      
        'from wmsraz r left join wmsosa o on r.codosa = o.codosa and r.fi' +
        'losa = o.codfil left join wmspro p on p.codigo = r.codpro'
      'where r.codclifor > 0'
      ''
      ''
      ''
      ''
      ''
      'and o.situac <> '#39'C'#39' order by 1')
    Left = 24
    Top = 184
    object BCDField1: TBCDField
      FieldName = 'ID_RAZ'
      Precision = 32
    end
    object BCDField2: TBCDField
      FieldName = 'CODOSA'
      Precision = 32
    end
    object BCDField3: TBCDField
      FieldName = 'FILOSA'
      Precision = 32
    end
    object BCDField4: TBCDField
      FieldName = 'CODPRO'
      Precision = 32
    end
    object BCDField5: TBCDField
      FieldName = 'CODCLIFOR'
      Precision = 32
    end
    object StringField1: TStringField
      FieldName = 'CODLOT'
    end
    object StringField2: TStringField
      FieldName = 'NOTFIS'
      Size = 15
    end
    object StringField3: TStringField
      FieldName = 'SERIEN'
      Size = 10
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATDOC'
    end
    object StringField4: TStringField
      FieldName = 'ENTSAI'
      FixedChar = True
      Size = 1
    end
    object BCDField6: TBCDField
      FieldName = 'QTDADE'
      Precision = 16
    end
    object BCDField7: TBCDField
      FieldName = 'VALOR'
      Precision = 16
    end
    object BCDField8: TBCDField
      FieldName = 'ANTFIS'
      Precision = 16
    end
    object BCDField9: TBCDField
      FieldName = 'SALFIS'
      Precision = 16
    end
    object BCDField10: TBCDField
      FieldName = 'ANTFIN'
      Precision = 16
    end
    object BCDField11: TBCDField
      FieldName = 'SALFIN'
      Precision = 16
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'DATATU'
    end
    object StringField5: TStringField
      FieldName = 'USUATU'
    end
    object BCDField12: TBCDField
      FieldName = 'NUMVOL'
      Precision = 16
    end
    object BCDField13: TBCDField
      FieldName = 'ANTVOL'
      Precision = 16
    end
    object BCDField14: TBCDField
      FieldName = 'SALVOL'
      Precision = 16
    end
    object StringField6: TStringField
      FieldName = 'SUBLOT'
      Size = 30
    end
    object BCDField15: TBCDField
      FieldName = 'ID_IOS'
      Precision = 32
    end
    object StringField7: TStringField
      FieldName = 'REFCLI'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'REFERE'
      Size = 30
    end
  end
end
