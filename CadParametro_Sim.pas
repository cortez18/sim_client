unit CadParametro_Sim;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, JvExControls, JvDBLookup, DB, ImgList, ADODB, ActnList, Buttons,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, JvExStdCtrls, JvGroupBox, Menus, ComCtrls,
  JvExComCtrls, JvComCtrls, System.ImageList, System.Actions;

type
  TfrmCadParametro_Sim = class(TForm)
    QParametro: TADOQuery;
    dtsParametro: TDataSource;
    Label19: TLabel;
    edPdfCte: TEdit;
    Label10: TLabel;
    edSchema: TEdit;
    Label8: TLabel;
    edPdf: TEdit;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Label1: TLabel;
    edControladoria: TEdit;
    Label2: TLabel;
    edSeguradora: TEdit;
    QParametroDATAINC: TDateTimeField;
    QParametroUSUARIO: TStringField;
    QParametroPDF: TStringField;
    QParametroSCHEMMA: TStringField;
    QParametroPDF_CTE: TStringField;
    QParametroCONTROLADORIA: TStringField;
    QParametroSEGURADORA: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
  private
    quant: integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadParametro_Sim: TfrmCadParametro_Sim;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadParametro_Sim.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadParametro_Sim.btnSalvarClick(Sender: TObject);
begin
  try
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('update tb_parametro_sim set usuario =:0,  pdf =:1, ');
    dtmDados.IQuery1.SQL.add
      ('schemma = :2, pdf_cte = :3, controladoria = :4, seguradora = :5');
    dtmDados.IQuery1.Parameters[0].value := GLBUSER;
    dtmDados.IQuery1.Parameters[1].value := edPdf.text;
    dtmDados.IQuery1.Parameters[2].value := edSchema.text;
    dtmDados.IQuery1.Parameters[3].value := edPdfCte.text;
    dtmDados.IQuery1.Parameters[4].value := EdControladoria.text;
    dtmDados.IQuery1.Parameters[5].value := EdSeguradora.text;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QParametro.Close;
  QParametro.Open;
end;

procedure TfrmCadParametro_Sim.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QParametro.Cancel;
end;

procedure TfrmCadParametro_Sim.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  EdPdfCte.SetFocus;
end;

procedure TfrmCadParametro_Sim.FormCreate(Sender: TObject);
begin
  QParametro.Open;
  edPdfCte.Text        := QParametroPDF_CTE.AsString;
  edSchema.Text        := QParametroSCHEMMA.AsString;
  edPdf.Text           := QParametroPDF.AsString;
  edControladoria.Text := QParametroCONTROLADORIA.AsString;
  edSeguradora.Text    := QParametroSEGURADORA.AsString;
end;

procedure TfrmCadParametro_Sim.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QParametro.Close;
end;

procedure TfrmCadParametro_Sim.Restaura;
begin
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;
end;

end.
