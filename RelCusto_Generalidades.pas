unit RelCusto_Generalidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.FileCtrl, Vcl.StdCtrls,
  JvBaseEdits, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ExtCtrls,
  Vcl.Buttons, JvExStdCtrls, JvCombobox, JvMaskEdit, JvDBLookup, Vcl.Mask,
  JvExMask, JvToolEdit, Data.Win.ADODB, JvDBControls, Vcl.DBCtrls,
  System.ImageList, Vcl.ImgList, RLParser, RLReport, RLRichText,
  Vcl.Imaging.jpeg, JvMemoryDataset;

type
  TfrmRelCusto_Generalidades = class(TForm)
    dbggen: TJvDBGrid;
    Panel2: TPanel;
    Label8: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    edVlrPrev: TJvCalcEdit;
    btnImpr: TBitBtn;
    btnSair: TBitBtn;
    EdTransp: TEdit;
    edFatura: TEdit;
    Memo1: TMemo;
    edDesconto: TJvCalcEdit;
    edVcto: TJvDateEdit;
    edImp: TJvCalcEdit;
    cbTipo: TJvComboBox;
    edProcesso: TJvCalcEdit;
    edData: TJvDateEdit;
    Panel5: TPanel;
    Label16: TLabel;
    btnSalvarPrevia: TBitBtn;
    btnCancelaPrevia: TBitBtn;
    btnImprimirPrevia: TBitBtn;
    btnSairPrevia: TBitBtn;
    edPrevia: TEdit;
    btnCarregar: TBitBtn;
    Panel4: TPanel;
    Label24: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    btnGerarExcel: TBitBtn;
    QFornecedor: TADOQuery;
    dtsFornecedor: TDataSource;
    QFornecedorCODIGO: TFMTBCDField;
    QFornecedorNOME: TStringField;
    QFornecedorNR_CNPJ_CPF: TStringField;
    Panel3: TPanel;
    Label25: TLabel;
    Label36: TLabel;
    Label26: TLabel;
    cbCampo: TComboBox;
    edLocalizar: TEdit;
    cbOperador: TComboBox;
    btProcurar: TBitBtn;
    btRetirarFiltro: TBitBtn;
    cbFieldName: TComboBox;
    btnPrevia: TBitBtn;
    btnManifesto: TBitBtn;
    btnLimpaFiltro: TBitBtn;
    btnOco: TBitBtn;
    btnBO_2: TBitBtn;
    Panel8: TPanel;
    Label29: TLabel;
    Label30: TLabel;
    btnConfirma_rel: TBitBtn;
    btnImprimir: TBitBtn;
    btnExcel: TBitBtn;
    edcusto: TJvCalcEdit;
    edcalculado: TJvCalcEdit;
    JvDBNavigator2: TJvDBNavigator;
    JvDBMaskEdit1: TJvDBMaskEdit;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label23: TLabel;
    dtinicial: TJvDateEdit;
    dtfinal: TJvDateEdit;
    ledidcliente: TJvDBLookupEdit;
    ledcliente: TJvDBLookupEdit;
    cbTodos: TCheckBox;
    btrelatorio: TBitBtn;
    cbfilial: TJvComboBox;
    QGen: TADOQuery;
    dsGen: TDataSource;
    iml: TImageList;
    QSite: TADOQuery;
    QSiteFILIAL: TStringField;
    QSiteCODFIL: TBCDField;
    dtsSite: TDataSource;
    lbltransp: TLabel;
    QGenFL_STATUS: TStringField;
    QGenSERVICO: TStringField;
    QGenFILIAL: TFMTBCDField;
    QGenCLIENTE: TStringField;
    QGenTRANSPORTADORA: TStringField;
    QGenDESTINO: TStringField;
    QGenDESCRI: TStringField;
    QGenESTADO: TStringField;
    QGenNR_CONHECIMENTO: TFMTBCDField;
    QGenQUANTIDADE: TBCDField;
    QGenVLR_TOTAL_C: TBCDField;
    QGenVLR_TOTAL_R: TBCDField;
    QGenMARGEM: TFMTBCDField;
    QGenFATURA: TStringField;
    QGenNR_CTE_PAGAR: TFMTBCDField;
    QGenCUSTO_COBRADO: TBCDField;
    QGenUSER_FATURA: TStringField;
    QGenDT_FATURA: TDateTimeField;
    QRep: TADOQuery;
    dsRep: TDataSource;
    Label20: TLabel;
    edFat: TJvMaskEdit;
    RadioGroup1: TRadioGroup;
    QGenCODCLIFOR: TFMTBCDField;
    SP_Exporta: TADOStoredProc;
    MMNF: TMemo;
    QNF: TADOQuery;
    QNFNR_NF: TStringField;
    QGenAutorizacao: TBCDField;
    QGendata: TDateField;
    mdGener: TJvMemoryData;
    Label4: TLabel;
    Panel1: TPanel;
    mdGenerescol: TStringField;
    mdGenerData: TDateTimeField;
    mdGenerAutorizacao: TFMTBCDField;
    mdGenerServico: TStringField;
    mdGenerFilial: TFMTBCDField;
    mdGenerNr_conhecimento: TFMTBCDField;
    mdGenerQuantidade: TBCDField;
    mdGenerVlr_total_c: TBCDField;
    mdGenerVlr_total_r: TBCDField;
    mdGenerfatura: TStringField;
    mdGenerNr_cte_pagar: TFMTBCDField;
    mdGenerCusto_cobrado: TBCDField;
    mdGeneruser_fatura: TStringField;
    mdGenerDt_fatura: TDateTimeField;
    mdGenerCliente: TStringField;
    mdGenerCodclifor: TFMTBCDField;
    mdGenerTransportadora: TStringField;
    mdGenerDestino: TStringField;
    mdGenerDescri: TStringField;
    mdGenerEstado: TStringField;
    mdGenerMargem: TFMTBCDField;
    mdGenernotfis: TStringField;
    mdGenerfl_status: TStringField;
    edId: TJvCalcEdit;
    SP_Atualiza: TADOStoredProc;
    mdGenerliberacao: TStringField;
    mdGenernovo_custo: TBCDField;
    QGenliberacao: TStringField;
    QGennovo_custo: TBCDField;
    QGentabela_custo: TBCDField;
    QGentabela_receita: TBCDField;
    mdGenertabela_custo: TIntegerField;
    mdGenertabela_receita: TIntegerField;
    Label5: TLabel;
    edNFs: TJvCalcEdit;
    QRepDATA: TDateTimeField;
    QRepAUTORIZACAO: TFMTBCDField;
    QRepFL_STATUS: TStringField;
    QRepSERVICO: TStringField;
    QRepFILIAL: TFMTBCDField;
    QRepCTE_GEN: TFMTBCDField;
    QRepQUANTIDADE: TBCDField;
    QRepVLR_TOTAL_C: TBCDField;
    QRepVLR_TOTAL_R: TBCDField;
    QRepCNPJ_CLIENTE: TStringField;
    QRepFATURA: TStringField;
    QRepNR_CTE_PAGAR: TFMTBCDField;
    QRepCUSTO_COBRADO: TBCDField;
    QRepUSER_FATURA: TStringField;
    QRepDT_FATURA: TDateTimeField;
    QRepCLIENTE: TStringField;
    QRepCODCLIFOR: TFMTBCDField;
    QRepTRANSPORTADORA: TStringField;
    QRepMARGEM: TFMTBCDField;
    QRepMANIFESTO: TFMTBCDField;
    QRepLIBERACAO: TStringField;
    QRepNOVO_CUSTO: TBCDField;
    QRepVL_DESCONTO: TBCDField;
    QRepOBS_FATURA: TStringField;
    QRepVL_IMPOSTO_NFS: TBCDField;
    Label7: TLabel;
    edVctoM: TJvDateEdit;
    QRepCUSTO: TFMTBCDField;
    Sp_Custo: TADOStoredProc;
    QGenMANIFESTO: TFMTBCDField;
    mdGenerManifesto: TBCDField;
    QGenveiculo: TStringField;
    mdGenerveiculo: TStringField;
    QGenCTE_ORIGEM: TFMTBCDField;
    mdGenercte_origem: TIntegerField;
    QRepVL_PROCESSO: TBCDField;
    QGenintegra_rodopar: TStringField;
    mdGenerintegra_rodopar: TStringField;
    mdGenerxml: TStringField;
    QGenTIPO_TRANSPORTADOR: TStringField;
    mdGenertipo_transportdor: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure ledidclienteExit(Sender: TObject);
    procedure dbggenDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ledclienteExit(Sender: TObject);
    procedure btrelatorioClick(Sender: TObject);
    procedure dbggenCellClick(Column: TColumn);
    procedure cbTodosClick(Sender: TObject);
    procedure btnSalvarPreviaClick(Sender: TObject);
    procedure btnPreviaClick(Sender: TObject);
    procedure btnCancelaPreviaClick(Sender: TObject);
    procedure btnImprimirPreviaClick(Sender: TObject);
    procedure btnSairPreviaClick(Sender: TObject);
    procedure btnCarregarClick(Sender: TObject);
    procedure dbggenEditChange(Sender: TObject);
    procedure btnConfirma_relClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnImprClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure btnGerarExcelClick(Sender: TObject);
    procedure cbCampoChange(Sender: TObject);
    procedure cbOperadorExit(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    Function FindMemo(const Enc: String; Var Texto: TMemo): STRING;
    procedure btnLimpaFiltroClick(Sender: TObject);
    procedure Carregar;
    procedure edIdExit(Sender: TObject);
    procedure mdGenerAfterPost(DataSet: TDataSet);
    procedure mdGenerFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure mdGenerfaturaChange(Sender: TField);
    procedure mdGenerNr_cte_pagarChange(Sender: TField);
    procedure mdGenerCusto_cobradoChange(Sender: TField);
    procedure btnImprimirClick(Sender: TObject);
    procedure dbggenKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edNFsExit(Sender: TObject);
    procedure edDataExit(Sender: TObject);
    procedure edVctoMExit(Sender: TObject);
  private
    alterado : String;
    { Private declarations }
  public

    { Public declarations }
  end;

var
  frmRelCusto_Generalidades: TfrmRelCusto_Generalidades;
  filtro: String;

implementation

{$R *.dfm}

uses Dados, Menu, funcoes, LiberaPagto_Gen, DetalhesCustoGeneralidade,
  CadTabelaCustoFrac, CadTabelaVenda, Impressao_gener;

procedure TfrmRelCusto_Generalidades.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QFornecedor.Close;
  QGen.Close;
  QRep.Close;
 { dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.Add
    ('update crm_generalidades set fl_status = ''N'', user_fatura = null where ');
  dtmDados.IQuery1.SQL.Add
    ('(user_fatura =:0 or user_fatura is null) and fl_status = ''S'' and fatura is null and dt_fatura is null');
  dtmDados.IQuery1.Parameters[0].Value := GLBUSER;
  dtmDados.IQuery1.ExecSQL;  }
  mdGener.Close;
end;

procedure TfrmRelCusto_Generalidades.FormCreate(Sender: TObject);
begin
  // vari�vel para controlar o carregamento e altera��o
  alterado := '';
  mdGener.Open;
  // self.Height := Ajustaaltura(self.Height);
  Panel1.Left := trunc((self.Width / 2) - (Panel1.Width / 2));
  Panel1.Top := trunc((self.Height / 2) - (Panel1.Height / 2));
  QFornecedor.Open;
  cbfilial.Clear;
  cbfilial.Items.Add('Todas');
  QSite.Open;
  while not QSite.eof do
  begin
    cbfilial.Items.Add(QSiteFILIAL.AsString);
    QSite.next;
  end;
  QSite.Locate('codfil', GLBFilial, []);
  cbfilial.ItemIndex := cbfilial.Items.IndexOf(QSiteFILIAL.AsString);
  QSite.Close;
end;

procedure TfrmRelCusto_Generalidades.btnConfirma_relClick(Sender: TObject);
var
  tt, tc, base, sest, inss, vc: real;
  vcto: TdateTime;
  codtar, dia, filial: Integer;
  pfj, xml, fat: String;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if RadioGroup1.ItemIndex = 1 then
    Exit;

  if mdGenerFATURA.AsString = '' then
    Exit;

  mdGener.filtered := true;
  mdGener.First;

  vc := 0;
  tt := 0;
  tc := 0;
  xml := 'S';

  tag := 0;

  mdGener.first;
  filial := mdGenerFILIAL.asInteger;
  fat := mdGenerFATURA.AsString;
  while not mdGener.eof do
  begin
    if mdGenerCUSTO_COBRADO.Value = -99 then
      vc := 0
    else
      vc := mdGenerCUSTO_COBRADO.Value;
    tt := tt + vc;
    tc := tc + mdGenerVLR_TOTAL_C.Value;
    if mdGenerFILIAL.asInteger <> filial then
    begin
      ShowMessage
        ('Existem mais de 1 filial para este pagamento, favor limpar o campo fatura da filial e desmarcar !!');
      Exit;
    end;
    if mdGenerFATURA.AsString <> fat then
    begin
      ShowMessage
        ('Existem Lan�amentos de Faturas diferentes para este Pagamento !!');
      Exit;
    end;
    mdGener.next;
  end;
  EdTRansp.text := mdGenerTransportadora.AsString;

  Memo1.Clear;

  Panel2.Visible := true;
  btnSalvar.Enabled := true;
  btnCancelar.Enabled := true;
  edcusto.Value := tt;
  edVlrPrev.Value := tc; // edCalculado.value;
  edcalculado.Value := tc;
  edFatura.text := mdGenerFATURA.AsString;
  edDesconto.Value := 0;
  edData.Clear;

  if pfj = 'F' then
  begin
    base := Arredondar((edVlrPrev.Value * 0.20), 2);
    sest := Arredondar((base * 0.025), 2);
    inss := Arredondar((base * 0.11), 2);
    edImp.Value := sest + inss;
    Memo1.Lines.Add('Sest/Senat = ' + FloatToStrf(sest, ffnumber, 10, 2) +
      ' INSS = ' + FloatToStrf(inss, ffnumber, 10, 2));
  end
  else
  begin
    base := 0;
    sest := 0;
    inss := 0;
    edImp.Value := sest + inss;
  end;
  if edVlrPrev.Value <> edcalculado.Value then
    ShowMessage
      ('Favor enviar ao TIC esta tela, o valor a pagar est� diferente do valor calculado !!');
  edData.SetFocus;
end;

procedure TfrmRelCusto_Generalidades.btnExcelClick(Sender: TObject);
begin
  Panel4.Visible := true;
end;

procedure TfrmRelCusto_Generalidades.btnCancelaPreviaClick(Sender: TObject);
begin
  btnImprimirPrevia.Enabled := false;
  edPrevia.Clear;
  Panel5.Visible := false;
end;

procedure TfrmRelCusto_Generalidades.btnCancelarClick(Sender: TObject);
begin
  mdGener.filtered := true;
  Panel2.Visible := false;
end;

procedure TfrmRelCusto_Generalidades.btnCarregarClick(Sender: TObject);
begin
  if edPrevia.text <> '' then
  begin
    edFat.Text := edPrevia.text;
    if mdGener.RecordCount > 0 then
    begin
      if (Application.Messagebox
        ('Existem Dados Carregados, deseja ignora-los ?', 'Carregar Fatura',
        MB_YESNO + MB_ICONQUESTION) = IDYES) then
      begin
        carregar;
      end;
    end
    else
      carregar;
  end;
  panel5.Visible := false;
end;

procedure TfrmRelCusto_Generalidades.btnGerarExcelClick(Sender: TObject);
var
  sarquivo: String;
  Memo1: TStringList;
  i: Integer;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_Frete_Sim_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to dbggen.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + dbggen.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  mdGener.first;
  while not mdGener.eof do
  begin
    for i := 0 to dbggen.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + mdGener.FieldByName(dbggen.Columns[i]
        .FieldName).AsString + '</th>');
    mdGener.next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  ShowMessage('Planilha salva em ' + sarquivo);
  Panel4.Visible := false;

end;

procedure TfrmRelCusto_Generalidades.btnImprClick(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmImpressao_gener, frmImpressao_gener);
    frmImpressao_gener.QRep.Close;
    frmImpressao_gener.QRep.Parameters[0].Value := edFatura.Text;
    frmImpressao_gener.QRep.Parameters[1].Value := mdGenerCodclifor.AsInteger;
    frmImpressao_gener.QRep.Open;
    frmImpressao_gener.RLReport1.Preview;
  finally
    frmImpressao_gener.Free;
  end;
end;

procedure TfrmRelCusto_Generalidades.btnImprimirClick(Sender: TObject);
var
  tt, tc, vc: real;
  icount: Integer;
begin
  tt := 0;
  tc := 0;
  Tag := 0;
  vc := 0;
  mdGener.filtered := true;
  mdGener.First;
  while not mdGener.Eof do
  begin
    vc := mdGenerVlr_total_c.Value;

    tt := tt + mdGenerCusto_cobrado.Value;
    tc := tc + vc;
    mdGener.Next;
  end;
  edCusto.Value := tt;
  edCalculado.Value := tc;
  if tc > 0 then
  begin
    try
      Application.CreateForm(TfrmImpressao_gener, frmImpressao_gener);
      frmImpressao_gener.QRep.Close;
      frmImpressao_gener.QRep.Parameters[0].Value := mdGenerfatura.AsString;
      frmImpressao_gener.QRep.Parameters[1].Value := mdGenerCodclifor.AsInteger;
      frmImpressao_gener.QRep.Open;
      frmImpressao_gener.RLReport1.Preview;
    finally
      frmImpressao_gener.Free;
    end;
  end;
end;

procedure TfrmRelCusto_Generalidades.btnImprimirPreviaClick(Sender: TObject);
begin
    try
      Application.CreateForm(TfrmImpressao_gener, frmImpressao_gener);
      frmImpressao_gener.RLReport1.Preview;
    finally
      frmImpressao_gener.Free;
    end;
end;

procedure TfrmRelCusto_Generalidades.btnLimpaFiltroClick(Sender: TObject);
begin
  mdGener.filtered := true;
  edLocalizar.Clear;
  tag := 0;
end;

procedure TfrmRelCusto_Generalidades.btnPreviaClick(Sender: TObject);
begin
  mdGener.filtered := false;
  edPrevia.Clear;
  Panel5.Visible := true;
end;

procedure TfrmRelCusto_Generalidades.btnSairClick(Sender: TObject);
begin
  Panel2.Visible := false;
end;

procedure TfrmRelCusto_Generalidades.btnSairPreviaClick(Sender: TObject);
begin
  Panel5.Visible := false;
end;

procedure TfrmRelCusto_Generalidades.btnSalvarClick(Sender: TObject);
var
  ok: Integer;
  Origem, Destino, caminho: string;
begin
  ok := 1;

  if alltrim(edFatura.text) = '' then
  begin
    ShowMessage('N�o foi digitada a Fatura');
    edFatura.SetFocus;
    Exit;
  end;

  if ApCarac(edData.text) = '' then
  begin
    ShowMessage('N�o foi digitada a Data da Fatura');
    edData.SetFocus;
    Exit;
  end;

  if edVlrPrev.Value = 0 then
  begin
    ShowMessage('N�o Existe Valor desta Fatura ?');
    Exit;
  end;

  if (edDesconto.Value > 0) and (cbTipo.text = '') then
  begin
    ShowMessage('Todo Desconto Precisa de um Tipo !');
    cbTipo.SetFocus;
    Exit;
  end;

  if (edProcesso.Value > 0) and (cbTipo.text = '') then
  begin
    ShowMessage('Todo Desconto Precisa de um Tipo !');
    cbTipo.SetFocus;
    Exit;
  end;

  if (edDesconto.Value + edProcesso.Value = 0) and (cbTipo.text <> '') then
  begin
    ShowMessage('Qual Desconto para Este Tipo Escolhido ? !');
    edDesconto.SetFocus;
    Exit;
  end;

  if (edDesconto.Value > 0) and (Length(Memo1.text) < 10) then
  begin
    ShowMessage('O Desconto precisa de uma Justificativa !');
    Memo1.SetFocus;
    Exit;
  end;

  if (edProcesso.Value > 0) and (Length(Memo1.text) < 10) then
  begin
    ShowMessage('O Desconto precisa de uma Justificativa !');
    Memo1.SetFocus;
    Exit;
  end;

  if (edDesconto.Value + edProcesso.Value) > edVlrPrev.Value then
  begin
    showmessage('O Valor do Desconto N�o Pode ser Maior que o Valor da Fatura !');
    edDesconto.SetFocus;
    exit;
  end;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select count(*) qt  from crm_generalidades ');
  dtmDados.IQuery1.sql.Add('where fatura = :0 and transp = :1 ');
  dtmDados.IQuery1.sql.Add('and fl_status = ''P'' ');
  dtmDados.IQuery1.Parameters[0].Value := edFatura.text;
  dtmDados.IQuery1.Parameters[1].Value := mdGenerCodclifor.AsInteger;
  dtmDados.IQuery1.open;
  if dtmDados.IQuery1.fieldbyname('qt').Value > 0 then
  begin
    showmessage('J� Existe esta Fatura registrada para este Transportador!');
    edFatura.SetFocus;
    dtmDados.IQuery1.close;
    exit;
  end;
  dtmDados.IQuery1.close;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select count(*) qt from cyber.pagdoci ');
  dtmDados.IQuery1.sql.Add('where numdoc = :0 and codclifor = :1 ');
  dtmDados.IQuery1.sql.Add('and situac <> ''C'' ');
  dtmDados.IQuery1.Parameters[0].Value := edFatura.text;
  dtmDados.IQuery1.Parameters[1].Value := mdGenerCodclifor.AsInteger;
  dtmDados.IQuery1.open;
  if dtmDados.IQuery1.fieldbyname('qt').Value > 0 then
  begin
    showmessage
      ('J� Existe esta Fatura registrada no RODOPAR para este Transportador!');
    edFatura.SetFocus;
    dtmDados.IQuery1.close;
    exit;
  end;
  dtmDados.IQuery1.close;

  mdGener.first;
  while not mdGener.eof do
  begin
    if (mdGenerFATURA.AsString = edFatura.text) then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.Add
        ('update crm_generalidades set obs_fatura =:0, dt_vencimento =:1,  ');
      dtmDados.IQuery1.SQL.Add
        ('vl_desconto =:2, tipo_desconto =:3, vl_impostos =:4, vl_processo =:5, ');
      dtmDados.IQuery1.SQL.Add
        ('fl_status =:6, vl_imposto_nfs = :7, dt_fatura = :8, user_fatura = :9, dataalt = sysdate ');
      dtmDados.IQuery1.SQL.Add('where id_generalidade = :10 and fl_status = ''N''');
      dtmDados.IQuery1.Parameters[0].Value := Memo1.text;
      dtmDados.IQuery1.Parameters[1].Value := edVcto.date;
      dtmDados.IQuery1.Parameters[2].Value := edDesconto.Value;

      if cbTipo.text <> '' then
        dtmDados.IQuery1.Parameters[3].Value :=
          StrToInt(copy(cbTipo.text, 1, 1))
      else
        dtmDados.IQuery1.Parameters[3].Value := 0;

      dtmDados.IQuery1.Parameters[4].Value := edImp.Value;
      dtmDados.IQuery1.Parameters[5].Value := edProcesso.Value;
      dtmDados.IQuery1.Parameters[6].Value := 'P';
      dtmDados.IQuery1.Parameters[7].Value := edNFs.Value;
      dtmDados.IQuery1.Parameters[8].Value := edData.date;
      dtmDados.IQuery1.Parameters[9].Value := GLBUSER;
      dtmDados.IQuery1.Parameters[10].Value := mdGenerAutorizacao.AsInteger;
      dtmDados.IQuery1.ExecSQL;

      // grava o campo Vencimento Manual
      if copy(edVctoM.Text,1,2) <> '  ' then
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update crm_generalidades set dt_vcto_m = :0 ');
        dtmDados.IQuery1.sql.Add
          ('where id_generalidade = :1 ');
        dtmDados.IQuery1.Parameters[0].Value := edVctoM.Date;
        dtmDados.IQuery1.Parameters[1].Value := mdGenerAutorizacao.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end
      else
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update crm_generalidades set dt_vcto_m = null ');
        dtmDados.IQuery1.sql.Add
          ('where id_generalidade = :0 ');
        dtmDados.IQuery1.Parameters[0].Value := mdGenerAutorizacao.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end;


    end;
    mdGener.next;
  end;

  // conforme e-mail da Debora em 16/04 as 11:45 n�o integrar os seguintes servi�os no Rodopar
  // Na tabela select * from crm_tb_tiposerv where integra_rodopar = 'S'

  if (mdGenerintegra_rodopar.AsString = 'S') then
  begin
    SP_Exporta.Parameters[0].Value := edFatura.text;
    SP_Exporta.Parameters[1].Value := mdGenerCODCLIFOR.AsFloat;
      if copy(edVctoM.Text,1,2) <> '  ' then
        SP_Exporta.Parameters[2].Value := edVctoM.date
      else
        SP_Exporta.Parameters[2].Value := edVcto.date;
    SP_Exporta.Parameters[3].Value := GLBUSER;
    SP_Exporta.ExecProc;
  end;
  btnImpr.Enabled := true;
end;

procedure TfrmRelCusto_Generalidades.btnSalvarPreviaClick(Sender: TObject);
begin
  btnImprimirPrevia.Enabled := true;
  mdGener.filtered := true;
  mdGener.first;

  while not mdGener.eof do
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.Add
      ('update crm_generalidades set fatura =:0, dt_fatura = sysdate where ');
    dtmDados.IQuery1.SQL.Add('id_generalidade =:1 and fl_status = ''N'' ');
    dtmDados.IQuery1.Parameters[0].Value := edPrevia.text;
    dtmDados.IQuery1.Parameters[1].Value := mdGenerAutorizacao.AsInteger;
    dtmDados.IQuery1.ExecSQL;
    mdGener.next;
  end;

  QRep.Close;
  QRep.Parameters[0].Value := edPrevia.text;
  QRep.Parameters[1].Value := mdGenerCodclifor.AsInteger;
  QRep.Open;
  mdGener.filtered := false;
  btrelatorio.Click;
end;

procedure TfrmRelCusto_Generalidades.btProcurarClick(Sender: TObject);
var
  Operador, b: String;
  a: Integer;
begin
  Screen.Cursor := crHourGlass;
  if cbOperador.text = 'igual a' then
    Operador := '='
  else if cbOperador.text = 'contendo' then
    Operador := 'LIKE';

  if cbCampo.text = '' then
  begin
    Screen.Cursor := crDefault;
    ShowMessage('Selecione um campo para procurar');
    cbCampo.SetFocus;
    Exit;
  end
  else if cbOperador.text = '' then
  begin
    Screen.Cursor := crDefault;
    ShowMessage('Selecione um operador');
    cbOperador.SetFocus;
    Exit;
  end
  else if edLocalizar.text = '' then
  begin
    Screen.Cursor := crDefault;
    ShowMessage('Informe um valor para procurar');
    edLocalizar.SetFocus;
    Exit;
  end;
  if Operador <> '' then
  begin
    if Operador = 'LIKE' then
    begin
      // NF
      if cbCampo.text = 'NF' then
      begin
        b := FindMemo(edLocalizar.text, MMNF);
        if FindMemo(edLocalizar.text, MMNF) <> '' then
        begin
          a := StrToInt(copy(FindMemo(edLocalizar.text, MMNF), 1, 10));
          if not mdGener.Locate('NR_CONHECIMENTO', a, []) then
            ShowMessage('N�o foi localizado nenhum item');
        end;
      end
      else if not mdGener.Locate(cbFieldName.Items[cbCampo.ItemIndex],
        alltrim(edLocalizar.text), [loPartialKey]) then
        ShowMessage('N�o foi localizado nenhum item');
    end
    else
    begin
      if not mdGener.Locate(cbFieldName.Items[cbCampo.ItemIndex],
        edLocalizar.text, []) then
        ShowMessage('N�o foi localizado nenhum item');
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TfrmRelCusto_Generalidades.btrelatorioClick(Sender: TObject);
begin
  if (edId.text = '') and (edFat.text = '') then
  begin
    if copy(dtinicial.text, 1, 2) = '  ' then
    begin
      ShowMessage('Voc� n�o escolheu a Data Inicial');
      Exit;
    end;

    if copy(dtfinal.text, 1, 2) = '  ' then
    begin
      ShowMessage('Voc� n�o escolheu a Data Final');
      Exit;
    end;

    if dtfinal.date < dtinicial.date then
      begin
        ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
        Exit;
    end;
  end;

  if (Trim(ledidcliente.LookupValue) = '') and (edId.text = '') then
  begin
    ShowMessage('Voc� n�o escolheu o Transportador ou Manifesto');
    Exit;
  end;

  carregar;
end;

procedure TfrmRelCusto_Generalidades.cbCampoChange(Sender: TObject);
begin
  filtro := '';
  btProcurar.Enabled := true;
end;

procedure TfrmRelCusto_Generalidades.cbOperadorExit(Sender: TObject);
begin
  if (cbCampo.ItemIndex = 10) and (cbOperador.text = 'contendo') then
  begin
    ShowMessage('Para campo de valor este operador n�o � v�lido !');
    cbOperador.SetFocus;
  end;
end;

procedure TfrmRelCusto_Generalidades.cbTodosClick(Sender: TObject);
var docfiscal : Integer;
begin
  mdGener.first;
  while not mdGener.eof do
  begin

    if (mdGenertipo_transportdor.AsString = 'Transportador') then
    begin
      if (not mdGenerxml.IsNull) then
        docfiscal := 1
      else
      begin
        docfiscal := 0;
        showmessage('O XML do Transportador n�o est� no Fiscal, n�o sendo permitido a libera��o');
        exit;
      end;
    end
    else
    begin
      // liberado pois � agregado
      docfiscal := 1;
      cbTodos.Checked := false
    end;

    if cbTodos.Checked = false then
    begin
      if (mdGenerescol.AsString <> 'P') or (mdGenerFATURA.AsString <> '') then
      begin
        mdGener.Edit;
        mdGenerescol.Value := 'N';
        mdGener.Post;
        edcusto.Value := edcusto.Value - mdGenerCUSTO_COBRADO.Value;
        edcalculado.Value := edcalculado.Value - mdGenerVLR_TOTAL_C.Value;
      end;
    end
    else
    begin
      if (mdGenerCUSTO_COBRADO.Value <= mdGenerVLR_TOTAL_C.Value) and
        (mdGenerCUSTO_COBRADO.Value <> 0) or (mdGenerFL_STATUS.AsString <> 'P')
        and (docfiscal = 1) then
      begin
        mdGener.Edit;
        mdGenerescol.Value := 'P';
        mdGener.Post;
        edcusto.Value := edcusto.Value + mdGenerCUSTO_COBRADO.Value;
        edcalculado.Value := edcalculado.Value + mdGenerVLR_TOTAL_C.Value;
      end;
    end;
    mdGener.next;
  end;

end;

procedure TfrmRelCusto_Generalidades.dbggenCellClick(Column: TColumn);
var docfiscal : Integer;
begin
  // Evento para preencher o checkbox no crid
  if (Column.Field = mdGenerescol) then
  begin

    if (mdGenertipo_transportdor.AsString = 'Transportador') then
    begin
      if (not mdGenerxml.IsNull) then
        docfiscal := 1
      else
      begin
        docfiscal := 0;
        showmessage('O XML do Transportador n�o est� no Fiscal, n�o sendo permitido a libera��o');
        exit;
      end;
    end
    else
    begin
      // liberado pois � agregado
      docfiscal := 1;
    end;

    // Valor Cobrado maior que o valor da tabela
    if (mdGenerCusto_cobrado.Value > mdGenerVlr_total_c.Value) and
      (mdGenerescol.Value = 'N') then
    begin
      if Application.Messagebox('Voc� Deseja Liberar Este Pagamento ?',
        'Gerar Pagamento', MB_YESNO + MB_ICONQUESTION) = IDYES then
      begin
        try
          Application.CreateForm(TfrmLiberaPagto_Gen, frmLiberaPagto_Gen);
          frmLiberaPagto_Gen.ShowModal;
        finally
          frmLiberaPagto_Gen.Free;
        end;
      end;
      //else
      //  exit;
    end;

    if (mdGenerfl_Status.Value = 'N') and (mdGenerVlr_total_c.Value > 0) and (docfiscal = 1) then
    begin
      mdGener.edit;
      if (mdGenerescol.IsNull) or (mdGenerescol.Value = 'N') then
      begin
        edCusto.Value := edCusto.Value + mdGenerVlr_total_c.Value;
        edCalculado.Value := edCalculado.Value + mdGenerVlr_total_c.Value;
        mdGenerescol.Value := 'P';
      end
      else
      begin
        mdGenerescol.Value := 'N';
        edCusto.Value := edCusto.Value - mdGenerVlr_total_c.Value;
        edCalculado.Value := edCalculado.Value - mdGenerVlr_total_c.Value;
      end;
      mdGener.Post;
    end;
  end;
end;

procedure TfrmRelCusto_Generalidades.dbggenDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field.FieldName = 'VLR_TOTAL_C') then
  begin
    if mdGenerCUSTO_COBRADO.Value = mdGenerVLR_TOTAL_C.Value then
    begin
      dbggen.canvas.Brush.Color := clYellow;
      dbggen.canvas.Font.Color := clBlack;
    end;
    if (mdGenerCUSTO_COBRADO.Value < mdGenerVLR_TOTAL_C.Value) then
    begin
      dbggen.canvas.Brush.Color := clGreen;
      dbggen.canvas.Font.Color := clWhite;
    end;
    if mdGenerCUSTO_COBRADO.Value > mdGenerVLR_TOTAL_C.Value then
    begin
      dbggen.canvas.Brush.Color := clRed;
      dbggen.canvas.Font.Color := clWhite;
    end;
    if mdGenernovo_custo.Value > 0 then
    begin
      dbggen.canvas.Brush.color := $002392F5;
      dbggen.canvas.font.color := clBlack;
    end;

  end;

  if (Column.Field = mdGenerescol) then
  begin
    dbggen.canvas.FillRect(Rect);
    iml.Draw(dbggen.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if (mdGenerescol.AsString = 'P') then
      iml.Draw(dbggen.canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbggen.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if mdGenerfl_status.Value = 'P' then
  begin
    dbggen.canvas.Brush.color := clYellow;
    dbggen.canvas.font.color := clNavy;
    dbggen.DefaultDrawDataCell(Rect, dbggen.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field = mdGenerFatura) then
  begin
    if not mdGenerliberacao.IsNull then
    begin
      dbggen.canvas.Brush.color := clLime;
      dbggen.canvas.font.color := clBlack;
    end;
    dbggen.DefaultDrawDataCell(Rect, dbggen.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field.FieldName = 'VLR_TOTAL_C') then
  begin
    if mdGenernovo_custo.Value > 0 then
    begin
      dbggen.canvas.Brush.color := $002392F5;
      dbggen.canvas.font.color := clBlack;
    end;
    dbggen.DefaultDrawDataCell(Rect, dbggen.Columns[DataCol].Field, State);
  end;

  if (Column.Field = mdGenerNr_cte_pagar) then
  begin
    if (mdGenerintegra_rodopar.AsString = 'S') then
    begin
      if (mdGenerxml.IsNull) then
        dbggen.canvas.Brush.color := clLime;
        dbggen.canvas.font.color := clBlack;
    end;
    dbggen.DefaultDrawDataCell(Rect, dbggen.Columns[DataCol].Field, State);
  end;

end;

procedure TfrmRelCusto_Generalidades.dbggenEditChange(Sender: TObject);
begin
  if mdGenerFL_STATUS.AsString = 'P' then
  begin
    mdGenerCUSTO_COBRADO.ReadOnly := true;
    mdGenerFATURA.ReadOnly := true;
    mdGenerNR_CTE_PAGAR.ReadOnly := true;
  end
  else
  begin
    mdGenerCUSTO_COBRADO.ReadOnly := false;
    mdGenerFATURA.ReadOnly := false;
    mdGenerNR_CTE_PAGAR.ReadOnly := false;
  end;

end;

procedure TfrmRelCusto_Generalidades.dbggenKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var tab : Integer;
    custo : Double;
begin
  // tecla F2
  if Key = VK_F2 then
  begin
    SP_Custo.Parameters[11].Value := mdGenercte_origem.AsInteger;
    SP_Custo.Parameters[12].Value := mdGenerFilial.AsInteger;
    SP_Custo.Parameters[13].Value := mdGenerManifesto.AsInteger;
    if mdGenerveiculo.AsString <> '(sem ve�culo)' then
      SP_Custo.Parameters[14].Value := mdGenerveiculo.AsString
    else
      SP_Custo.Parameters[14].Value := '';
    if not mdGenerQuantidade.IsNull then
      SP_Custo.Parameters[15].Value := mdGenerQuantidade.AsInteger
    else
      SP_Custo.Parameters[15].Value := 0;
    SP_Custo.ExecProc;

    if mdGenerServico.Value = 'AGENDAMENTO' then
      custo :=  SP_Custo.Parameters[0].Value
    else if mdGenerServico.Value = 'ARMAZENAGEM' then
      custo :=  SP_Custo.Parameters[0].Value
    else if mdGenerServico.Value = 'AJUDANTE' then
      custo :=  SP_Custo.Parameters[1].Value
    else if mdGenerServico.Value = 'PALETIZA��O' then
      custo :=  SP_Custo.Parameters[2].Value
    else if mdGenerServico.Value = 'DI�RIA' then
      custo :=  SP_Custo.Parameters[3].Value
    else if mdGenerServico.Value = 'REENTREGA' then
      custo :=  SP_Custo.Parameters[4].Value
    else if mdGenerServico.Value = 'PERNOITE' then
      custo :=  SP_Custo.Parameters[5].Value
    else if mdGenerServico.Value = 'HORA PARADA' then
      custo :=  SP_Custo.Parameters[6].Value
    else if mdGenerServico.Value = 'VE�CULO DEDICADO' then
      custo :=  SP_Custo.Parameters[7].Value
    else if mdGenerServico.Value = 'CANHOTO' then
        custo :=  SP_Custo.Parameters[8].Value;

    if custo <> mdGenerVlr_total_c.Value then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add('update crm_generalidades set vlr_total_c = :0 ');
      dtmdados.IQuery1.SQL.add('where id_generalidade = :1');
      dtmdados.IQuery1.Parameters[0].value := custo;
      dtmdados.IQuery1.Parameters[1].value := mdGenerAutorizacao.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;

      mdGener.Edit;
      mdGenerVlr_total_c.Value := custo;
      mdGener.Post;
    end;

    end;


  // tecla F8
  if Key = VK_F8 then
  begin
    try
      Application.CreateForm(TfrmDetalhesCustoGeneralidade,
        frmDetalhesCustoGeneralidade);
      frmDetalhesCustoGeneralidade.ShowModal;
    finally
      frmDetalhesCustoGeneralidade.Free;
    end;
  end;

  // tecla F11
  if (Key = VK_F11) and (mdGenertabela_custo.AsInteger> 0) then
  begin
    if not dtmDados.PodeVisualizar('frmCadTabelaCustoFrac') then
      exit;
    try
      Application.CreateForm(TfrmCadTabelaCustoFrac, frmCadTabelaCustoFrac);
      frmCadTabelaCustoFrac.QTabela.close;
      frmCadTabelaCustoFrac.QTabela.sql.Clear;
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('select t.*, f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_fornecedor, ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('CASE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('   WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('ELSE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('end as local, ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('CASE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('   WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('ELSE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('end as local_des, op.fl_km ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('from tb_custofrac t inner join cyber.rodcli f on t.cod_fornecedor = f.codclifor ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''N'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('where f.nomeab is not null');
      frmCadTabelaCustoFrac.QTabela.sql.Add('and t.cod_custo = :0');
      frmCadTabelaCustoFrac.QTabela.Parameters[0].Value := mdGenertabela_custo.AsInteger;
      frmCadTabelaCustoFrac.QTabela.open;
      frmCadTabelaCustoFrac.Panel2.Visible := false;
      frmCadTabelaCustoFrac.Panel3.Visible := false;
      frmCadTabelaCustoFrac.TabSheet1.TabVisible := false;

      frmCadTabelaCustoFrac.ShowModal;
    finally
      frmCadTabelaCustoFrac.Free;
    end;
  end;

  // tecla F12
  if (Key = VK_F12) and (mdGenertabela_receita.AsInteger > 0) then
  begin
    if not dtmDados.PodeVisualizar('frmCadTabelaVenda') then
      exit;
    try
      Application.CreateForm(TfrmCadTabelaVenda, frmCadTabelaVenda);
      frmCadTabelaVenda.QTabela.close;
      frmCadTabelaVenda.QTabela.sql.Clear;
      frmCadTabelaVenda.QTabela.sql.Add
        ('select t.*, f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_cliente, ');
      frmCadTabelaVenda.QTabela.sql.Add('CASE ');
      frmCadTabelaVenda.QTabela.sql.Add
        ('   WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaVenda.QTabela.sql.Add('ELSE ');
      frmCadTabelaVenda.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaVenda.QTabela.sql.Add('end as local, ');
      frmCadTabelaVenda.QTabela.sql.Add('CASE ');
      frmCadTabelaVenda.QTabela.sql.Add
        ('   WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaVenda.QTabela.sql.Add('ELSE ');
      frmCadTabelaVenda.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaVenda.QTabela.sql.Add('end as local_des, op.fl_km ');
      frmCadTabelaVenda.QTabela.sql.Add
        ('from tb_tabelavenda t inner join cyber.rodcli f on t.cod_cliente = f.codclifor ');
      frmCadTabelaVenda.QTabela.sql.Add
        ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''S'' ');
      frmCadTabelaVenda.QTabela.sql.Add('where f.nomeab is not null');
      frmCadTabelaVenda.QTabela.sql.Add('and t.cod_venda = :0');
      frmCadTabelaVenda.QTabela.Parameters[0].Value := mdGenertabela_receita.AsInteger;
      frmCadTabelaVenda.QTabela.open;
      frmCadTabelaVenda.Panel2.Visible := false;
      frmCadTabelaVenda.Panel3.Visible := false;
      frmCadTabelaVenda.TabSheet1.TabVisible := false;

      frmCadTabelaVenda.ShowModal;
    finally
      frmCadTabelaVenda.Free;
    end;
  end;
end;

procedure TfrmRelCusto_Generalidades.edDataExit(Sender: TObject);
 var tt, tc, base, sest, inss : real;
     vcto : TdateTime;
     codtar, dia : integer;
     pfj, xml : String;
begin
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select * from (select codtar, adidia, ');
  dtmdados.RQuery1.SQL.add('case when adiseg = ''S'' then 2 ');
  dtmdados.RQuery1.SQL.add('when aditer = ''S'' then 3 ');
  dtmdados.RQuery1.SQL.add('when adiqua = ''S'' then 4 ');
  dtmdados.RQuery1.SQL.add('when adiqui = ''S'' then 5 ');
  dtmdados.RQuery1.SQL.add('when adisex = ''S'' then 6 ');
  dtmdados.RQuery1.SQL.add('when adisab = ''S'' then 7 ');
  dtmdados.RQuery1.SQL.add('else 1 end dia ');
  dtmdados.RQuery1.SQL.add('from cyber.rodtar_cfr where situac <> ''I'' and codclifor = :0 ');
  dtmdados.RQuery1.SQL.add('order by 2 desc) where rownum = 1 ');
  dtmdados.RQuery1.Parameters[0].value := mdGenerCodclifor.AsInteger;
  dtmdados.RQuery1.open;
  if dtmdados.RQuery1.eof then
  begin
    ShowMessage('Fornecedor sem Cadastro de Vencimentos, Pedir o cadastro no ERP !!');
    exit;
  end
  else
  begin
    vcto := dtmdados.RQuery1.FieldByName('adidia').value + edData.Date;
    dia  := dtmdados.RQuery1.FieldByName('dia').value;
  end;

  codtar := dtmdados.RQuery1.FieldByName('codtar').value;
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select * from cyber.tarrat where codtar = :0');
  dtmdados.RQuery1.Parameters[0].value := codtar;
  dtmdados.RQuery1.open;
  if dtmdados.RQuery1.eof then
  begin
    ShowMessage('Fornecedor sem Classifica��o !!');
    exit;
  end;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select fisjur from cyber.rodcli where codclifor = :0');
  dtmdados.RQuery1.Parameters[0].value := mdGenercodclifor.AsInteger;
  dtmdados.RQuery1.open;
  pfj := dtmdados.RQuery1.FieldByName('fisjur').value;
  dtmdados.RQuery1.close;

  if DayOfWeek(vcto) = 1 then
    vcto := vcto + 1;

  if DayOfWeek(vcto) = 7 then
    vcto := vcto + 2;

  // verifica se o dia do vencimento � diferente do dia da semana que deve ser
  if DayOfWeek(vcto) <> dia then
  begin
    vcto := vcto + (dia - DayOfWeek(vcto));
  end;

  edVcto.Date := vcto;
  if edData.date > date() then
  begin
    showmessage('Data de Emiss�o Superior A Data de Hoje !!');
    edData.SetFocus;
  end;
end;

procedure TfrmRelCusto_Generalidades.edIdExit(Sender: TObject);
begin
  if edId.text <> '' then
  begin
    ledIdCliente.Clear;
    ledCliente.Clear;
    dtInicial.Clear;
    dtFinal.Clear;
  end;
end;

procedure TfrmRelCusto_Generalidades.edNFsExit(Sender: TObject);
begin
  // Lan�amento do valor do Imposto quando Nota Fiscal de Servi�o
  if edNFs.Value > 0 then
  begin
    edVlrPrev.Value := edVlrPrev.Value + edNFs.Value;
  end;

  if edNFs.Value = 0 then
  begin
    edVlrPrev.Value := edcalculado.Value;
  end;

end;

procedure TfrmRelCusto_Generalidades.edVctoMExit(Sender: TObject);
begin
  if ApCarac(edVctoM.text) <> '' then
  begin
    if edVctoM.Date <= edVcto.Date then
    begin
      ShowMessage('O Vencimento Manual s� Pode Ser Maior que a Data Informada !!');
      edVctoM.Clear;
    end;
  end;
end;

procedure TfrmRelCusto_Generalidades.ledclienteExit(Sender: TObject);
begin
  ledidcliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledcliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  // btnPgto.Enabled := false;
  lbltransp.Caption := IntToStr(QFornecedorCODIGO.asInteger);
end;

procedure TfrmRelCusto_Generalidades.ledidclienteExit(Sender: TObject);
begin
  ledidcliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledcliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  // btnPgto.Enabled := false;
  lbltransp.Caption := IntToStr(QFornecedorCODIGO.asInteger);
end;

procedure TfrmRelCusto_Generalidades.mdGenerAfterPost(DataSet: TDataSet);
begin
  if not dtmDados.PodeInserir(name) then
    exit;

  if alterado = 'S' then
  begin
    SP_Atualiza.Parameters[0].Value := mdGenerNr_cte_pagar.AsInteger;
    SP_Atualiza.Parameters[1].Value := mdGenerCusto_cobrado.AsFloat;
    SP_Atualiza.Parameters[2].Value := mdGenerFatura.Value;
    SP_Atualiza.Parameters[3].Value := mdGenerAutorizacao.AsInteger;
    SP_Atualiza.ExecProc;
    alterado := '';
  end;

end;

procedure TfrmRelCusto_Generalidades.mdGenerCusto_cobradoChange(Sender: TField);
begin
  alterado := 'S';
end;

procedure TfrmRelCusto_Generalidades.mdGenerfaturaChange(Sender: TField);
begin
  alterado := 'S';
end;

procedure TfrmRelCusto_Generalidades.mdGenerFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdGener['ESCOL'] = 'P';
end;

procedure TfrmRelCusto_Generalidades.mdGenerNr_cte_pagarChange(Sender: TField);
begin
  alterado := 'S';
end;

Function TfrmRelCusto_Generalidades.FindMemo(const Enc: String;
  Var Texto: TMemo): STRING;
Var
  i, Posicao: Integer;
  Linha: string;
Begin
  For i := 0 to Texto.Lines.Count - 1 do
  begin
    Linha := Texto.Lines[i];
    Repeat
      Posicao := pos(Enc, Linha);
      If Posicao > 0 then
      Begin
        Texto.Lines[i] := Linha;
        Result := Linha;
        Exit;
      end;
    until Posicao = 0;
  end;

end;

procedure TfrmRelCusto_Generalidades.Carregar;
var i : Integer;
    nf : string;
    vc: double;
begin
  alterado := '';
  mdGener.Close;
  mdGener.Open;
  Panel1.Visible := true;
  Application.ProcessMessages;

  if cbfilial.text = 'Todas' then
    QGen.SQL[1]:= 'where filial > 0 '
  else
    QGen.SQL[1] := 'where filial = ' + copy(cbfilial.text, 1,
      pos('-', cbfilial.text) - 1);

  if (edId.Value = 0) and (edFat.Text = '') then
    QGen.SQL[2] := 'and data between to_date(''' + dtinicial.text
    + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtfinal.text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';

  if ledidcliente.text <> '' then
    QGen.SQL[3] := ' and codclifor =' + lbltransp.Caption;

  if edFat.text <> '' then
    QGen.SQL[6] := 'and fatura = ' + QuotedStr(edFat.text);

  if edId.text <> '' then
    QGen.SQL[7] := 'and autorizacao = ' + ApCarac(edId.text);

  if RadioGroup1.ItemIndex = 0 then
    QGen.SQL[4] := 'and fl_status <> ''P'' '
  else if RadioGroup1.ItemIndex = 1 then
    QGen.SQL[4] := 'and fl_status is not null ';
//showmessage(QGen.SQL.Text);
  QGen.Open;
  if QGen.RecordCount > 0 then
  begin
    while not QGen.eof do
    begin
      mdGener.Append;
      if QGenFL_STATUS.value = 'P' then
        mdGenerescol.Value := 'P'
      else
        mdGenerescol.Value := 'N';
      mdGenerData.Value := QGendata.Value;
      mdGenerAutorizacao.Value := QGenAutorizacao.Value;
      mdGenerServico.Value := QGenSERVICO.Value;
      mdGenerFilial.Value := QGenfilial.Value;
      mdGenerNr_conhecimento.Value := QGennr_conhecimento.Value;
      mdGenerQuantidade.Value :=  QGenQUANTIDADE.Value;
      if QGennovo_custo.Value > 0  then
        mdGenerVlr_total_c.Value := QGennovo_custo.Value
      else
        mdGenerVlr_total_c.Value :=  QGenVLR_TOTAL_C.Value;
      mdGenerVlr_total_r.Value :=  QGenVLR_TOTAL_R.Value;
      mdGenerfatura.Value :=  QGenFATURA.Value;
      mdGenerfl_status.Value := QGenfl_status.Value;
      mdGenerNr_cte_pagar.Value :=  QGennr_cte_pagar.Value;
      mdGenerCusto_cobrado.Value :=  QGenCUSTO_COBRADO.Value;
      mdGeneruser_fatura.Value :=  QGenUSER_FATURA.Value;
      mdGenerDt_fatura.Value :=  QGenDT_FATURA.Value;
      mdGenerCliente.Value :=  QGenCLIENTE.Value;
      mdGenerCodclifor.Value :=  QGencodclifor.Value;
      mdGenerTransportadora.Value :=  QGenTRANSPORTADORA.Value;
      mdGenerDestino.Value :=  QGenDESTINO.Value;
      mdGenerDescri.Value :=  QGenDESCRI.Value;
      mdGenerEstado.Value :=  QGenESTADO.Value;
      mdGenerMargem.value :=  QGenmargem.Value;
      mdGenerliberacao.Value := QGenliberacao.AsString;
      mdGenernovo_custo.Value := QGennovo_custo.Value;
      mdGenertabela_receita.Value := QGentabela_receita.AsInteger;
      mdGenertabela_custo.Value := QGentabela_custo.AsInteger;
      mdGenerManifesto.Value := QGenMANIFESTO.AsInteger;
      mdGenerveiculo.Value := QGenveiculo.AsString;
      mdGenercte_origem.Value := QGenCTE_ORIGEM.AsInteger;
      mdGenerintegra_rodopar.AsString := QGenintegra_rodopar.AsString;
      mdGenertipo_transportdor.AsString := QGenTIPO_TRANSPORTADOR.AsString;
      if (mdGenerintegra_rodopar.AsString = 'S') and (mdGenertipo_transportdor.AsString = 'Transportador') then
      begin
        // pegar os xmls importados
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add('select nfe_id from cyber.estent a left join tb_paramtransp_filial t on a.codclifor = t.cod_filial where tipoNF = ''CTE'' and numdoc = '+QuotedStr(QGenNR_CTE_PAGAR.AsString)+' and t.cod_transp = '+QuotedStr(QGenCODCLIFOR.AsString));
        dtmDados.IQuery1.open;
        mdGenerxml.AsString := dtmdados.IQuery1.FieldByName('nfe_id').AsString;
        dtmDados.IQuery1.close;

        if (not mdGenerxml.IsNull) then
          mdGenerescol.Value := 'P'
        else
          mdGenerescol.Value := 'N';
      end
      else
        mdGenerescol.Value := 'P';


      // inclui as NF
      QNF.close;
      QNF.Parameters[0].Value := QGenNR_CONHECIMENTO.AsInteger;
      QNF.Parameters[1].Value := QGenFILIAL.AsInteger;
      QNF.open;
      i := 0;
      while not QNF.Eof do
      begin
        if i = 0 then
          nf := QNFNR_NF.AsString
        else
          nf := nf + '/' + QNFNR_NF.AsString;
        i := i + 1;
        QNF.Next;
      end;
      QNF.close;
      mdGenernotfis.Value := nf;
      mdGener.Post;
      edCalculado.Value := edCalculado.Value + mdGenerVlr_total_c.Value;
      edCusto.Value := edCusto.Value + mdGenerCusto_cobrado.Value;
      QGen.Next;
    end;
  end;
  QGen.Close;
  Panel1.Visible := false;
end;

end.
