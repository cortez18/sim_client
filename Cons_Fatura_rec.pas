unit Cons_Fatura_rec;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, Buttons, JvGradient, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExControls, JvEnterTab, Inifiles,
  JvDBLookup, ImgList, strutils, JvExStdCtrls, JvMemo,
  ShellAPI, ComObj, ExcelXP, System.ImageList, JvComponentBase, ActiveX;

type
  TfrmCons_Fatura_rec = class(TForm)
    Panel3: TPanel;
    JvGradient1: TJvGradient;
    Label1: TLabel;
    ds: TDataSource;
    JvEnterAsTab1: TJvEnterAsTab;
    QCTRC: TADOQuery;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    edFatura: TEdit;
    QClientes: TADOQuery;
    dtsClientes: TDataSource;
    QClientesCOD_CLIENTE: TBCDField;
    QClientesNM_CLIENTE: TStringField;
    QClientesNR_CNPJ_CPF: TStringField;
    QCTRCNR_CONHECIMENTO: TBCDField;
    QCTRCDT_CONHECIMENTO: TDateTimeField;
    QCTRCNM_CLIENTE_EXP: TStringField;
    QCTRCNM_CLIENTE_DES: TStringField;
    QCTRCVL_TOTAL: TBCDField;
    QCTRCNM_CLIENTE: TStringField;
    QCTRCOPERACAO: TStringField;
    QCTRCNM_CLIENTE_CON: TStringField;
    iml: TImageList;
    QCTRCCOD_CLIENTE: TBCDField;
    QCTRCCNPJLOJA: TStringField;
    QCTRCNR_SERIE: TStringField;
    Panel4: TPanel;
    dbgFatura: TJvDBUltimGrid;
    QCTRCCODCLIFOR: TBCDField;
    QCTRCPAGOU: TBCDField;
    btnListagem: TBitBtn;
    Panel8: TPanel;
    QCTRCUFCL: TStringField;
    QCTRCUFRE: TStringField;
    QCTRCUFDE: TStringField;
    btnRelat: TBitBtn;
    QRelat: TADOQuery;
    dtsRelat: TDataSource;
    QRelatNR_CTRC: TBCDField;
    QRelatDT_CTRC: TDateTimeField;
    QRelatNR_SERIE: TStringField;
    QRelatNM_CLI: TStringField;
    QRelatCNPJ_CLI: TStringField;
    QRelatNM_CLIENTE_EXP: TStringField;
    QRelatNR_CNPJ_CPF_EXP: TStringField;
    QRelatCIDADE_EXP: TStringField;
    QRelatUF_EXP: TStringField;
    QRelatNM_CLIENTE_DES: TStringField;
    QRelatNR_CNPJ_CPF_DES: TStringField;
    QRelatCIDADE_DES: TStringField;
    QRelatUF_DES: TStringField;
    QRelatNM_CLIENTE_RED: TStringField;
    QRelatNR_CNPJ_CPF_RED: TStringField;
    QRelatCIDADE_RED: TStringField;
    QRelatUF_RED: TStringField;
    QRelatVL_NF: TBCDField;
    QRelatPESO_KG: TBCDField;
    QRelatQT_VOLUME: TBCDField;
    QRelatNR_NOTA: TStringField;
    QRelatVL_FRETE: TBCDField;
    QRelatVL_PEDAGIO: TBCDField;
    QRelatVL_SEGURO: TBCDField;
    QRelatVL_GRIS: TBCDField;
    QRelatVL_OUTROS: TBCDField;
    QRelatVL_TOTAL: TBCDField;
    QRelatCAT: TBCDField;
    QRelatVL_ADEME: TBCDField;
    QRelatVL_DESPACHO: TBCDField;
    QRelatVL_DESCARGA: TBCDField;
    QRelatVL_ENTREGA: TBCDField;
    QRelatVL_TDE: TBCDField;
    QRelatVL_ADICIONAL: TBCDField;
    QRelatDESCRICAO_ADICIONAL: TStringField;
    QRelatVL_ALIQUOTA: TBCDField;
    QRelatVL_IMPOSTO: TBCDField;
    QRelatFL_TIPO: TStringField;
    QRelatNR_FATURA: TBCDField;
    QRelatCNPJ_TRANSPORTADORA: TStringField;
    QRelatIESTADUAL: TStringField;
    QRelatOPERACAO: TStringField;
    QRelatCFOP_CLESS: TStringField;
    QRelatTIPCON: TStringField;
    QRelatFATURA: TBCDField;
    btRelatorio: TBitBtn;
    QNF: TADOQuery;
    QNFNOTFIS: TStringField;
    QSKF: TADOQuery;
    QSKFNUMDUP: TBCDField;
    QSKFCODCON: TBCDField;
    QSKFSERCON: TStringField;
    QSKFTOTFRE: TBCDField;
    QSKFICMVLR: TBCDField;
    QSKFBASCAL: TBCDField;
    QSKFCTE_ID: TStringField;
    QSKFNATOPE: TBCDField;
    QSKFCODREM: TBCDField;
    QSKFALICOT: TBCDField;
    QSKFFILDOC: TBCDField;
    btnSKF: TBitBtn;
    QSKFCTEDTA: TDateTimeField;
    QRelatCTE_ID: TStringField;
    QRelatPESO_CUB: TBCDField;
    QRelatOBSCON: TStringField;
    QSKFESTADO: TStringField;
    Label2: TLabel;
    QCTRCDATINC: TDateTimeField;
    QCTRCNUMDUP: TBCDField;
    QCTRCCAMINHO: TStringField;
    RGD: TRadioGroup;
    RGS: TRadioGroup;
    RGM: TRadioGroup;
    QSint: TADOQuery;
    dtsSint: TDataSource;
    QSintDATINC: TDateTimeField;
    QSintNUMDUP: TBCDField;
    QSintDATVEN: TDateTimeField;
    QSintTEM: TBCDField;
    QSintFALTAM: TBCDField;
    cbXML: TCheckBox;
    QCTRCCTE_ID: TStringField;
    DBSint: TJvDBUltimGrid;
    cbDacte: TCheckBox;
    cbCompr: TCheckBox;
    QCTRCFL_COMPROVANTE: TStringField;
    QConf: TADOQuery;
    QCTRCCOD_CONHECIMENTO: TBCDField;
    QRelatCODCIDADEDESTINO: TStringField;
    QRelatCODCIDADEORIGEM: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure edValorKeyPress(Sender: TObject; var Key: Char);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure QClientesBeforeOpen(DataSet: TDataSet);
    procedure QCTRCBeforeOpen(DataSet: TDataSet);
    procedure btnListagemClick(Sender: TObject);
    procedure btnRelatClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnSKFClick(Sender: TObject);
    procedure dbgFaturaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QSintBeforeOpen(DataSet: TDataSet);
    procedure DBSintDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgFaturaCellClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCons_Fatura_rec: TfrmCons_Fatura_rec;
  tt, ts : real;
  dife : string;

const
  olMailItem = 0;

implementation

uses Dados, menu , funcoes, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmCons_Fatura_rec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //md.Close;
  Action := caFree;
  frmCons_Fatura_rec := nil;
  QClientes.close;
  QCTRC.close;
end;

procedure TfrmCons_Fatura_rec.FormCreate(Sender: TObject);
begin
  QClientes.open;
end;

procedure TfrmCons_Fatura_rec.ledIdClienteCloseUp(Sender: TObject);
begin
  if (ledIdCliente.Text <> '') or (ledCliente.text <> '') then
  begin
    ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
    ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  end;
end;

procedure TfrmCons_Fatura_rec.edValorKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in ['0'..'9', #8]) then
    Key := #0;
end;

procedure TfrmCons_Fatura_rec.btnListagemClick(Sender: TObject);
var memo2: TStringList;
    i : Integer;
begin
  if dbgFatura.Visible = true then
  begin
    if QCtrc.RecordCount > 0 then
    begin
      if FileExists('fatura.html') then
        DeleteFile('fatura.html');

      Memo2 := TStringList.Create;
      memo2.Add('<HTML><HEAD><TITLE>Gera��o de Faturas</TITLE>');
      memo2.Add('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
      memo2.Add('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
      memo2.Add('       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .texto2 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
      memo2.Add('</STYLE>');
      memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
      memo2.Add('<P align=center class=titulo1>Consulta de Faturas</P>');
      Memo2.Add('<Table border=1 bordercolor="#005CB9" align=center>');

      for i:= 1 to DBGFatura.Columns.Count - 1 do
      begin
        Memo2.Add('<th><FONT class=texto1>'+ DBGFatura.Columns[i].Title.Caption +'</th>');
      end;
      Memo2.Add('</tr>');
      QCtrc.First;
      while not QCtrc.Eof do
      begin
        for i:= 1 to DBGFatura.Columns.Count - 1 do
         if QCtrc.fieldbyname(DBGFatura.Columns[0].FieldName).AsInteger = 1 then
           Memo2.Add('<th><FONT class=texto3>'+ QCtrc.fieldbyname(DBGFatura.Columns[i].FieldName).AsString +'</th>')
         else
           Memo2.Add('<th><FONT class=texto2>'+ QCtrc.fieldbyname(DBGFatura.Columns[i].FieldName).AsString +'</th>');
        QCtrc.Next;
        Memo2.Add('<tr>');
      end;
      QCtrc.First;
      memo2.Add('</TABLE><BR>');
    end;
    Memo2.SaveToFile('fatura.html');
    Memo2.Free;

    ShellExecute(Application.Handle, nil, PChar('fatura.html'), nil, nil, SW_SHOWNORMAL);
  end
  else
  begin
    if QSint.RecordCount > 0 then
    begin
      if FileExists('fatura_sint.html') then
        DeleteFile('fatura_sint.html');

      Memo2 := TStringList.Create;
      memo2.Add('<HTML><HEAD><TITLE>Gera��o de Faturas</TITLE>');
      memo2.Add('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
      memo2.Add('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
      memo2.Add('       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .texto2 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
      memo2.Add('       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
      memo2.Add('</STYLE>');
      memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
      memo2.Add('<P align=center class=titulo1>Consulta de Faturas</P>');
      Memo2.Add('<Table border=1 bordercolor="#005CB9" align=center>');

      for i:= 1 to DBSint.Columns.Count - 1 do
      begin
        Memo2.Add('<th><FONT class=texto1>'+ DBSint.Columns[i].Title.Caption +'</th>');
      end;
      Memo2.Add('</tr>');
      QSint.First;
      while not QSint.Eof do
      begin
        for i:= 1 to DBSint.Columns.Count - 1 do
          Memo2.Add('<th><FONT class=texto2>'+ QSint.fieldbyname(DBSint.Columns[i].FieldName).AsString +'</th>');
        QSint.Next;
        Memo2.Add('<tr>');
      end;
      QSint.First;
      memo2.Add('</TABLE><BR>');
    end;

    Memo2.SaveToFile('fatura_sint.html');
    Memo2.Free;

    ShellExecute(Application.Handle, nil, PChar('fatura_sint.html'), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TfrmCons_Fatura_rec.btnRelatClick(Sender: TObject);
var sarquivo :String;
    i: integer;
    memo1: TStringList;
begin
  QRelat.close;
  QRelat.Parameters[0].value := edFatura.text;
  QRelat.open;

  if not QRelat.eof then
  begin
    sArquivo := 'Rel_Fatura_' + edFatura.text+ '.xls';
    Memo1 := TStringList.Create;
    Memo1.Add('<HTML>');
    Memo1.Add('  <HEAD>');
    Memo1.Add('    <TITLE>IW - Intecom</TITLE>');
    memo1.Add('       <STYLE>.texto1  {FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo1.Add('              .texto2  {FONT: 10px Arial, Helvetica, sans-serif; COLOR: #00000}');
    memo1.Add('       </STYLE>');
    Memo1.Add('  </HEAD>');
    Memo1.Add('  <BODY <Font Color="#004080">');
    Memo1.Add('    <Center>');
    Memo1.Add('      <Table border=1 align=center>');
    Memo1.Add('        <tr>');
    for i:= 0 to QRelat.FieldCount - 1 do
    begin
      Memo1.Add('          <td><FONT class=texto1>'+ QRelat.Fields[i].FieldName +'</td>');
    end;
    Memo1.Add('        </tr>');
    Memo1.Add('        <tr>');
    QRelat.First;
    while not QRelat.Eof do
    begin
      for i:= 0 to QRelat.FieldCount - 1 do
        Memo1.Add('          <td><FONT class=texto2>'+ QRelat.fieldbyname(QRelat.Fields[i].FieldName).AsString +'</td>');
      QRelat.Next;
    Memo1.Add('        </tr>');
    end;
    Memo1.Add('      </table>');
    Memo1.Add('    </center>');
    Memo1.Add('  </body>');
    Memo1.Add('</html>');
    Memo1.SaveToFile(sArquivo);
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TfrmCons_Fatura_rec.btnSKFClick(Sender: TObject);
var Ini: TIniFile;
    PastaModelo, notas: string;
    Excel: OleVariant;
    wb: Variant;
    Linha, Coluna, cte : Integer;
begin
  if not QCtrc.eof then
  begin

    Panel8.visible := true;
    Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
    PastaModelo := Ini.ReadString('RELATORIO', 'CAMINHO', '');
    Ini.Free;
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := false;
    wb := Excel.Workbooks.Add(PastaModelo + 'Integra��o_Frete_Retroativo_Lote.xlt');
    Linha  := 3;
    Coluna := 1;
    // Preencher ABA DOF
    QSKF.Close;
    QSKF.Parameters[0].Value := StrToInt(edFatura.Text);
    QSKF.Open;
    while not QSKF.eof do
    begin
     // Excel.Workbooks[1].Sheets[1].Range['A1:FY1'].Copy;
     // Excel.Workbooks[1].Sheets[1].Range['A' + IntToStr(Linha) + ':FY' + IntToStr(Linha)].Insert(Shift := xlDown);
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2] := '61077327000156';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+3] := 'GKO_INTEGRACAO_OPEN_'+QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2] := 'E';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2] := 'CTE';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+6] := QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+7] := QSKFSERCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+8] := apcarac(apcarac(formatdatetime('dd/mm/yyyy',QSKFctedta.value)));
      if QSKFCODREM.value = 152 then
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+11]:= 'E352.04'
      else
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+11]:= 'E352.02';
      if QSKFEstado.value = 'SP' then
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+12] := '1.352'
      else
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+12] := '2.352';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+27] := 'B67183';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+33] := 'B67183';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+51] := 'B67183';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+81] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+82] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+91] := BuscaTroca(QSKFICMVLR.AsString,',','.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+92] := BuscaTroca(QSKFBASCAL.asstring,',','.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+172]:= BuscaTroca(QSKFCTE_ID.AsString,'CTE','');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+180] := '1';

      Inc(Linha);
      QSKF.Next;
    end;
    Linha  := 3;
    Coluna := 1;
    // Preencher ABA IDF
    QSKF.First;
    while not QSKF.eof do
    begin
      QNF.close;
      QNF.Parameters[0].value := QSKFCODCON.AsInteger;
      QNF.Parameters[1].value := QSKFSERCON.AsString;
      QNF.Parameters[2].value := QSKFFILDOC.AsInteger;
      QNF.open;
      notas := 'REF NOTAS FISCAIS ';
      while not QNF.eof do
      begin
        notas := notas + QNFNOTFIS.value + ' ';
        QNF.Next;
      end;
      QNF.close;
      //Excel.Workbooks[1].Sheets[2].Range['A1:CW1'].Copy;
      //Excel.Workbooks[1].Sheets[2].Range['A' + IntToStr(Linha) + ':CW' + IntToStr(Linha)].Insert(Shift := xlDown);
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2] := '61077327000156';
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+3] := 'GKO_INTEGRACAO_OPEN_'+QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+4] := '001';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+5] := '001';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+6] := 'P';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+7] := 'FRE';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+12] := '0';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+13] := '00';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+14] := '03';
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+16] := notas;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+17] := '1';
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+18] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+19] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+26] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+27] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+28] := BuscaTroca(QSKFTOTFRE.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+35] := BuscaTroca(QSKFALICOT.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+36] := BuscaTroca(QSKFBASCAL.asstring,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+37] := BuscaTroca(QSKFICMVLR.AsString,',','.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+38] := BuscaTroca(QSKFBASCAL.asstring,',','.');
      if QSKFCODREM.value = 152 then
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+74]:= 'E352.04'
      else
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+74]:= 'E352.02';
      if QSKFEstado.value = 'SP' then
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+75] := '1.352'
      else
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna+75] := '2.352';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+76] := 'UN';
      Inc(Linha);
      QSKF.Next;
    end;
    Excel.Workbooks[1].Sheets[1].Rows['1:1'].Delete(Shift := xlUp);
    Excel.Workbooks[1].Sheets[2].Rows['1:1'].Delete(Shift := xlUp);

    Excel.Visible := True;

    // relat�rio de faturamento
    QRelat.close;
    QRelat.Parameters[0].value := edFatura.text;
    QRelat.open;

    if not QRelat.eof then
    begin

      Linha := 2;
      Excel := CreateOleObject('Excel.Application');
      Excel.Caption := 'Arquivo de Faturamento';
      Excel.Visible := False;
      Excel.WorkBooks.add(1);
      // TITULO DAS COLUNAS

      Excel.Cells[1,1] := 'Cliente';
      Excel.Cells[1,2] := 'CNPJ Cliente';
      Excel.Cells[1,3] := 'Cliente Origem';
      Excel.Cells[1,4] := 'CNPJ Origem';
      Excel.Cells[1,5] := 'Cidade Origem';
      Excel.Cells[1,6] := 'UF Origem';
      Excel.Cells[1,7] := 'Cliente Destino';
      Excel.Cells[1,8] := 'CNPJ Destino';
      Excel.Cells[1,9] := 'Cidade Destino';
      Excel.Cells[1,10] := 'UF Destino';
      Excel.Cells[1,11] := 'Redespacho';
      Excel.Cells[1,12] := 'CNPJ Redespacho';
      Excel.Cells[1,13] := 'Cidade Redespacho';
      Excel.Cells[1,14] := 'UF Redespacho';
      Excel.Cells[1,15] := 'Data CT-e';
      Excel.Cells[1,16] := 'CT-e';
      Excel.Cells[1,17] := 'N� S�rie';
      Excel.Cells[1,18] := 'N� NF';
      Excel.Cells[1,19] := 'Valor NF';
      Excel.Cells[1,20] := 'Peso KG';
      Excel.Cells[1,21] := 'Peso CUB';
      Excel.Cells[1,22] := 'Volumes';
      Excel.Cells[1,23] := 'Valor Frete';
      Excel.Cells[1,24] := 'Valor Ped�gio';
      Excel.Cells[1,25] := 'Valor Seguro';
      Excel.Cells[1,26] := 'Valor GRIS';
      Excel.Cells[1,27] := 'Valor Outros';

      Excel.Cells[1,28] := 'Valor CAT';
      Excel.Cells[1,29] := 'Valor Ademe';
      Excel.Cells[1,30] := 'Valor Despacho';
      Excel.Cells[1,31] := 'Valor Descarga';
      Excel.Cells[1,32] := 'Valor Entrega';
      Excel.Cells[1,33] := 'Valor TDE';
      Excel.Cells[1,34] := 'Valor Adicional';

      Excel.Cells[1,35] := 'Valor Total';
      Excel.Cells[1,36] := 'Aliquota';
      Excel.Cells[1,37] := 'Valor Imposto';
      Excel.Cells[1,38] := 'Opera��o';
      Excel.Cells[1,39] := 'Ve�culo';
      Excel.Cells[1,40] := 'Fatura';
      Excel.Cells[1,41] := 'Chave Acesso CT-e';
      Excel.Cells[1,42] := 'Observa��o CT-e';
      Excel.Cells[1,43] := 'C�digo IBGE Origem';
      Excel.Cells[1,44] := 'C�digo IBGE Destino';

      // PRRENCHIMENTO DAS C�LULAS COM OS VALORES DOS CAMPOS DA TABELA
      Try
        While not QRelat.Eof do
        Begin
          Excel.Cells[Linha,41].NumberFormat := '@';
          Excel.Cells[Linha,1] := QRelatNM_CLI.AsString;
          Excel.Cells[Linha,2] := QRelatCNPJ_CLI.AsString;
          Excel.Cells[Linha,3] := QRelatNM_CLIENTE_EXP.AsString;
          Excel.Cells[Linha,4] := QRelatNR_CNPJ_CPF_EXP.AsString;
          Excel.Cells[Linha,5] := QRelatCIDADE_EXP.AsString;
          Excel.Cells[Linha,6] := QRelatUF_EXP.AsString;
          Excel.Cells[Linha,7] := QRelatNM_CLIENTE_DES.AsString;
          Excel.Cells[Linha,8] := QRelatNR_CNPJ_CPF_DES.AsString;
          Excel.Cells[Linha,9] := QRelatCIDADE_DES.AsString;
          Excel.Cells[Linha,10] := QRelatUF_DES.AsString;
          Excel.Cells[Linha,11] := QRelatNM_CLIENTE_RED.AsString;
          Excel.Cells[Linha,12] := QRelatNR_CNPJ_CPF_RED.AsString;
          Excel.Cells[Linha,13] := QRelatCIDADE_RED.AsString;
          Excel.Cells[Linha,14] := QRelatUF_RED.AsString;
          Excel.Cells[Linha,15] := QRelatDT_CTRC.AsString;
          Excel.Cells[Linha,16] := QRelatNR_CTRC.AsString;
          Excel.Cells[Linha,17] := QRelatNR_SERIE.AsString;
          Excel.Cells[Linha,18] := QRelatNR_NOTA.AsString;
          Excel.Cells[Linha,19] := QRelatVL_NF.Value;
          Excel.Cells[Linha,20] := QRelatPESO_KG.AsFloat;
          Excel.Cells[Linha,21] := QRelatPESO_CUB.AsFloat;
          Excel.Cells[Linha,22] := QRelatQT_VOLUME.AsFloat;
        //  if cte <> QRelatNR_CTRC.AsInteger then
        //  begin
            Excel.Cells[Linha,23] := QRelatVL_FRETE.Value;
            Excel.Cells[Linha,24] := QRelatVL_PEDAGIO.Value;
            Excel.Cells[Linha,25] := QRelatVL_SEGURO.Value;
            Excel.Cells[Linha,26] := QRelatVL_GRIS.Value;
            Excel.Cells[Linha,27] := QRelatVL_OUTROS.Value;

            Excel.Cells[Linha,28] := QRelatCAT.value;
            Excel.Cells[Linha,29] := QRelatVL_ADEME.value;
            Excel.Cells[Linha,30] := QRelatVL_DESPACHO.Value;
            Excel.Cells[Linha,31] := QRelatVL_DESCARGA.value;
            Excel.Cells[Linha,32] := QRelatVL_Entrega.Value;
            Excel.Cells[Linha,33] := QRelatVL_TDE.Value;
            Excel.Cells[Linha,34] := QRelatVL_Adicional.Value;

            Excel.Cells[Linha,35] := QRelatVL_TOTAL.Value;
            Excel.Cells[Linha,36] := QRelatVL_ALIQUOTA.AsFloat;
            Excel.Cells[Linha,37] := QRelatVL_IMPOSTO.Value;
            Excel.Cells[Linha,38] := QRelatOPERACAO.AsString;
            Excel.Cells[Linha,39] := QRelatFL_TIPO.AsString;
            Excel.Cells[Linha,40] := QRelatNR_FATURA.AsString;
            Excel.Cells[Linha,41] := BuscaTroca(QRelatCTE_ID.AsString,'CTE','');
            Excel.Cells[Linha,42] := QRelatOBSCON.AsString;
            Excel.Cells[Linha,43] := QRelatcodcidadeorigem.AsString;
            Excel.Cells[Linha,44] := QRelatcodcidadedestino.AsString;
        //  end;
          cte := QRelatNR_CTRC.AsInteger;
          Linha := Linha + 1;
          QRelat.Next;
        End;
        Excel.Columns.AutoFit;

        Excel.Visible := True;
      Finally
        Excel := Unassigned;
      end; // TRY
    end;
    Panel8.visible := false;
  end;
end;

procedure TfrmCons_Fatura_rec.btRelatorioClick(Sender: TObject);
var sArquivo, sData, sCaminho, cte, destino: String;
    mail: TStringList;
    i: Integer;
    Outlook: OleVariant;
    vMailItem: variant;
    SR: TSearchRec;
    MailItem: Variant;
begin
  self.Cursor := crHourGlass;
  Panel8.Visible := true;
  Application.ProcessMessages;
  if RGM.ItemIndex = 0 then
  begin
    DBSint.Visible := false;
    dbgFatura.Visible := true;
    QCtrc.close;
    if edfatura.text <> '' then
      QCtrc.SQL [19] := 'and fa.numdup = ' + QuotedStr(edfatura.text)
    else
      QCtrc.SQL [19] := '';
    if copy(dtInicial.text,1,2) <> '  ' then
    begin
      if RGD.ItemIndex = 0 then
        QCtrc.SQL [20] := 'and fa.datinc between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')'
      else
        QCtrc.SQL [20] := 'and f.datven between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')';
    end
    else
      QCtrc.SQL [20] := '';
    if Trim(ledIdCliente.LookupValue) <> '' then
      QCtrc.SQL [21] := 'and c.cod_pagador = ' + QuotedStr(ledIdCliente.LookupValue)
    else
      QCtrc.SQL [21] := '';

    if RGS.ItemIndex = 0 then
      QCtrc.SQL [27] := ''
    else if RGS.ItemIndex = 1 then
      QCtrc.SQL [27] := 'and p.situac is null '
    else
      QCtrc.SQL [27] := 'and p.situac = ''L'' ';

    QCtrc.open;
  end
  else
  begin
    DBSint.Visible := true;
    dbgFatura.Visible := false;
    QSint.close;
    if edfatura.text <> '' then
      QSint.SQL [7] := 'and fa.numdup = ' + QuotedStr(edfatura.text)
    else
      QSint.SQL [7] := '';
    if copy(dtInicial.text,1,2) <> '  ' then
    begin
      if RGD.ItemIndex = 0 then
        QSint.SQL [8] := 'and fa.datinc between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')'
      else
        QSint.SQL [8] := 'and f.datven between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')';
    end
    else
      QSint.SQL [8] := '';
    if Trim(ledIdCliente.LookupValue) <> '' then
      QSint.SQL [9] := 'and c.codpag = ' + QuotedStr(ledIdCliente.LookupValue)
    else
      QSint.SQL [9] := '';

    if RGS.ItemIndex = 0 then
      QSint.SQL [13] := ''
    else if RGS.ItemIndex = 1 then
      QSint.SQL [13] := 'and p.situac is null '
    else
      QSint.SQL [13] := 'and p.situac = ''L'' ';
   // showmessage(qsint.sql.text);
    QSint.open;
  end;
  self.Cursor := crDefault;
  Panel8.Visible := false;

  // Anexar XML
  if (cbXML.Checked = true) and (RGM.ItemIndex = 0) then
  begin
    QCtrc.first;
    // coloca o arquivo no repositorio do FTP
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.SQL.add('select distinct a.caminho from tb_email_xml a ');
    dtmdados.IQuery1.SQL.add('where a.codcli = (select cod_pagador from tb_conhecimento where cod_conhecimento = :0)');
    dtmdados.IQuery1.Parameters[0].value := QCTRCCOD_CONHECIMENTO.AsInteger;
    dtmdados.IQuery1.Open;
    i := 0;
    mail := TStringList.Create;
    while not QCTrc.eof do
    begin
      sData := FormatDateTime('YYYY', QCTRCDT_CONHECIMENTO.value) +
               FormatDateTime('MM', QCTRCDT_CONHECIMENTO.value);
      sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + sData + '\CTe\';
      sarquivo := scaminho+ QCTRCCTE_ID.asstring + '-cte.xml';
     // mail.values['attachment'+IntToStr(i)] := sArquivo;
        mail.add(sArquivo);
      if i = 0 then
        cte := 'CT-e(s) : ' + QCTRCNR_CONHECIMENTO.AsString
      else
        cte := cte + ' - ' + QCTRCNR_CONHECIMENTO.AsString;
      i := i + 1;
      // exite pasta de reposit�rio
      if not dtmdados.IQuery1.eof then
      begin
        sData := FormatDateTime('YYYY', QCTRCDT_CONHECIMENTO.value) +
                 FormatDateTime('MM', QCTRCDT_CONHECIMENTO.value);
        sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + sData + '\CTe\';
        sarquivo := scaminho+ QCTRCCTE_ID.asstring + '-cte.xml';
        destino := dtmdados.IQuery1.fieldbyname('caminho').AsString + QCTRCCTE_ID.asstring + '-cte.xml';
        CopyFile(PChar(sarquivo), PChar(Destino), true);
      end;
      QCtrc.next;
    end;

  //============================ ALTERA��O REALIZADA POR HUGO ANEXAR XML NO OUTLOOK =================
  CoInitialize(nil);
    try
     Outlook := GetActiveOleObject('Outlook.Application');
   except

     try
       Outlook := CreateOleObject('Outlook.Application');
     except
       on e: Exception do
         raise Exception.Create('Erro ao abrir o outlook. Detalhes: ' +
           e.Message);
     end

   end;

   try
   MailItem := Outlook.CreateItem(0);
   MailItem.Recipients.Add(' ');
   MailItem.Subject := 'Fatura :' + QCTRCNUMDUP.AsString ;
   MailItem.Body := cte;
   for i := 0 to mail.Count-1 do
    begin
      MailItem.Attachments.Add(mail[i]);
     end;
   MailItem.Display(true);

   finally
   VarClear(Outlook);
    end;


  end;

  // Anexar DACTE
  if (cbdacte.Checked = true) and (RGM.ItemIndex = 0) then
  begin
    //DeleteFile('C:\SIM\relacao.txt');
    //Lista := TStringList.Create;
    if not DirectoryExists('C:\SIM\PDF') then
      ForceDirectories('C:\SIM\PDF')
    else
    begin
      I := FindFirst('C:\SIM\PDF\*.*', faAnyFile, SR);
      while I = 0 do
      begin
        if (SR.Attr and faDirectory) <> faDirectory then
          if not DeleteFile('C:\SIM\PDF\' + SR.Name) then
            ShowMessage('N�o consegui excluir C:\SIM\PDF\' + SR.Name);
        I := FindNext(SR);
      end;
    end;
    if FileExists('C:\SIM\CTE.PDF') then
      DeleteFile('C:\SIM\CTE.PDF');
    QCtrc.first;
    i := 0;
    mail := TStringList.Create;
    while not QCTrc.eof do
    begin
      frmMenu.QrCteEletronico.close;
      frmMenu.qrCteEletronico.Parameters[0].value := QCtrcCOD_CONHECIMENTO.AsInteger;
      frmMenu.QrCteEletronico.open;
      frmMenu.QNF.close;
      frmMenu.QNF.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
      frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.Value;
      frmMenu.QNF.Parameters[2].value := frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
      frmMenu.QNF.Open;
      try
        Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
        frmDacte_Cte_2.sChaveCte := frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
        frmDacte_Cte_2.rlDacte.SaveToFile('C:\SIM\PDF\CTE-'+frmMenu.qrCteEletronicoNUMEROCTE.AsString+'.pdf' );
        frmDacte_Cte_2.rlDacte.Prepare;
      finally
        frmDacte_Cte_2.Free;
      end;
      if i = 0 then
        cte := 'CT-e(s) : ' + QCTRCNR_CONHECIMENTO.AsString
      else
        cte := cte + ' - ' + QCTRCNR_CONHECIMENTO.AsString;
      i := i + 1;
      QCtrc.next;
    end;

    Winexec(PAnsiChar('\\192.168.236.112\sim\Atualizador\pdfmerge c:\sim\cte.pdf c:\sim\pdf\'), Sw_Show);

    try
      Outlook := GetActiveOleObject('Outlook.Application');
    except
      Outlook := CreateOleObject('Outlook.Application');
    end;
    vMailItem := Outlook.CreateItem(olMailItem);
    vMailItem.Subject := 'Fatura :' + QCTRCNUMDUP.AsString ;
    vMailItem.Body    := cte;
    vMailItem.Attachments.Add('C:\SIM\cte.pdf');
    vMailItem.GetInspector.Activate;
    vMailItem.Display(False);
    VarClear(Outlook);

  end;

  // Anexar Comprovante
  if (cbCompr.Checked = true) and (RGM.ItemIndex = 0) then
  begin
    QCtrc.first;
    i := 0;
    mail := TStringList.Create;
    while not QCTrc.eof do
    begin
      if QCTRCCAMINHO.asstring <> '' then
      begin
     //   sarquivo := copy(QCTRCCAMINHO.asstring, 6, length(QCTRCCAMINHO.asstring)-6);
        sarquivo := BuscaTroca(QCTRCCAMINHO.asstring,'http:\\192.168.236.112\','\\192.168.236.112\e$\Aplicacoes\Intranet\');
        mail.values['attachment'+IntToStr(i)] := sArquivo;
        if i = 0 then
          cte := 'CT-e(s) : ' + QCTRCNR_CONHECIMENTO.AsString
        else
          cte := cte + ' - ' + QCTRCNR_CONHECIMENTO.AsString;
        i := i + 1;
      end;
      QCtrc.next;
    end;

    try
      mail.values['to'] := ''; ///AQUI VAI O EMAIL DO DESTINATARIO///
      mail.values['subject'] := 'Fatura :' + QCTRCNUMDUP.AsString ;
      mail.values['body'] := cte;
      sendEMail(Application.Handle, mail);
    finally
      mail.Free;
    end;
  end;

end;

procedure TfrmCons_Fatura_rec.dbgFaturaCellClick(Column: TColumn);
var chave : String;
begin
  chave := QCTRCCTE_ID.Value;
  if QCTRCFL_COMPROVANTE.Value = '' then
  begin
    QConf.close;
    QConf.Parameters[0].Value := 'S';
    QConf.Parameters[1].Value := QCTRCCTE_ID.Value;
    QConf.ExecSQL;
  end
  else
  begin
    QConf.close;
    QConf.Parameters[0].Value := '';
    QConf.Parameters[1].Value := QCTRCCTE_ID.Value;
    QConf.ExecSQL;
  end;
  QConf.close;
  QCtrc.close;
  QCtrc.open;
  QCtrc.locate('cte_id',chave,[]);
end;

procedure TfrmCons_Fatura_rec.dbgFaturaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = QCTRCCAMINHO) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if QCTRCCAMINHO.Value <> '' then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
  if (Column.Field = QCTRCFL_COMPROVANTE) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if QCTRCFL_COMPROVANTE.Value <> '' then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

end;

procedure TfrmCons_Fatura_rec.DBSintDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (RGM.ItemIndex =1) and ((pos('BRIL',ledCliente.Text) > 0)) then
  begin
    if QSintDATVEN.Value - now <= 40 then
    begin
      dbSint.Canvas.Font.Color  := clNavy;
      DBSint.Canvas.Brush.Color := clYellow;
      DBSint.Canvas.FillRect(Rect);
    end;
    if QSintDATVEN.Value - now <= 20 then
    begin
      DBSint.Canvas.Font.Color  := clWhite;
      DBSint.Canvas.Brush.Color := clRed;
      DBSint.Canvas.FillRect(Rect);
    end;
    DBSint.DefaultDrawDataCell(Rect, Column.Field, State);
  end;
end;

procedure TfrmCons_Fatura_rec.QClientesBeforeOpen(DataSet: TDataSet);
begin
  QClientes.Parameters[0].value := glbfilial;
end;

procedure TfrmCons_Fatura_rec.QCTRCBeforeOpen(DataSet: TDataSet);
begin
  QCtrc.Parameters[0].value := glbfilial;
end;

procedure TfrmCons_Fatura_rec.QSintBeforeOpen(DataSet: TDataSet);
begin
  QSint.Parameters[0].value := glbfilial;
end;

end.
