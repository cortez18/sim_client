unit Kardex_Rodopar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmKardex_Rodopar = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    lblDescricao: TLabel;
    edProd: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    QClienteCODCLIFOR: TBCDField;
    QClienteNOMEAB: TStringField;
    ADOQuery1: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    BCDField6: TBCDField;
    BCDField7: TBCDField;
    BCDField8: TBCDField;
    BCDField9: TBCDField;
    BCDField10: TBCDField;
    BCDField11: TBCDField;
    DateTimeField2: TDateTimeField;
    StringField5: TStringField;
    BCDField12: TBCDField;
    BCDField13: TBCDField;
    BCDField14: TBCDField;
    StringField6: TStringField;
    BCDField15: TBCDField;
    StringField7: TStringField;
    StringField8: TStringField;
    QCtrcCODOSA: TBCDField;
    QCtrcFILOSA: TBCDField;
    QCtrcCODPRO: TBCDField;
    QCtrcREFERE: TStringField;
    QCtrcCODCLIFOR: TBCDField;
    QCtrcNOTFIS: TStringField;
    QCtrcSERIEN: TStringField;
    QCtrcTIPDOC: TStringField;
    QCtrcQUANTI: TBCDField;
    QCtrcDATATU: TDateTimeField;
    QCtrcUSUATU: TStringField;
    QCtrcID_IOS: TBCDField;
    QCtrcREFCLI: TStringField;
    QCtrcDATEMI: TDateTimeField;
    Label1: TLabel;
    Edit1: TEdit;
    JvDBNavigator1: TJvDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edProdExit(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmKardex_Rodopar: TfrmKardex_Rodopar;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmKardex_Rodopar.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\' + ApCarac(DateToStr(date)) +
    glbuser + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}//');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}//');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}//');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
  // showmessage('Planilha salva em ' + sarquivo);
  { var Ini: TIniFile;
    data : Tdatetime;
    PastaModelo: string;
    Excel: OleVariant;
    wb: Variant;
    Linha, Coluna, saldo : Integer;
    begin
    Gauge1.Progress := 0;
    Qctrc.first;
    Gauge1.MaxValue := QCtrc.RecordCount;
    if not QCtrc.IsEmpty then
    begin
    // data := ADOQuery1dt_conhecimento.Value;
    Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
    PastaModelo := Ini.ReadString('RELATORIO', 'CAMINHO', '');
    Ini.Free;
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    wb := Excel.Workbooks.Add(PastaModelo + 'RelKardex.xlt');
    if copy(dtInicial.text,1,2) <> '  ' then
    Excel.Workbooks[1].Sheets[1].Cells[3, 1] := 'Periodo :' + datetostr(dtInicial.Date) + ' at� ' + datetostr(dtfinal.Date)
    else
    begin
    if ledIdCliente.Text <> '' then
    Excel.Workbooks[1].Sheets[1].Cells[3, 1] := 'Cliente : ' + ledIdCliente.Text
    else
    Excel.Workbooks[1].Sheets[1].Cells[3, 1] := 'Produto : ' + edProd.Text;
    end;
    Linha  := 5;
    Coluna := 1;

    while not QCtrc.eof do
    begin
    if linha = 5 then
    saldo := QCtrcQUANTI.AsInteger
    else
    begin
    if QCtrcTIPDOC.AsString = 'S' then
    saldo := saldo - QCtrcQUANTI.AsInteger
    else
    saldo := saldo + QCtrcQUANTI.AsInteger;
    end;
    Excel.Workbooks[1].Sheets[1].Range['A1:P1'].Copy;
    Excel.Workbooks[1].Sheets[1].Range['A' + IntToStr(Linha) + ':P' + IntToStr(Linha)].Insert(Shift := xlDown);
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna]   := QCtrcCODOSA.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+1] := QCtrcFILOSA.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2] := QCtrcCODPRO.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+3] := QCtrcRefere.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+4] := QCtrcCODCLIFOR.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+5] := QCtrcNOTFIS.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+6] := QCtrcSERIEN.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+7] := QCtrcDATEMI.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+8] := QCtrcTIPDOC.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+9] := QCtrcQUANTI.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+10]:= saldo;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+11]:= QCtrcDATATU.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+12]:= QCtrcUSUATU.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+13]:= '';
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+14]:= QCtrcID_IOS.AsInteger;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+15]:= QCtrcREFCLI.AsString;
    Inc(Linha);
    Gauge1.AddProgress(1);
    QCtrc.Next;
    end;
    Excel.Workbooks[1].Sheets[1].Rows['1:1'].Delete(Shift := xlUp);
    Excel.Visible := True;
    end; }
end;

procedure TfrmKardex_Rodopar.btRelatorioClick(Sender: TObject);
begin
  if (ledIdCliente.LookupValue = '') and (edProd.text = '') and (Edit1.text = '')
  then
  begin
    showmessage('Escolha o Cliente ou Produto!!');
    ledIdCliente.setfocus;
    exit;
  end;
  if copy(dtInicial.text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  if (ledIdCliente.LookupValue <> '') then
    QCtrc.SQL[5] := 'And z.codclifor = ' + QuotedStr(ledIdCliente.LookupValue);
  if (edProd.text <> '') then
    QCtrc.SQL[6] := 'And z.codpro = ' + QuotedStr(alltrim(edProd.text));
  if (Edit1.text <> '') then
    QCtrc.SQL[7] := 'And p.refere = ' + QuotedStr(alltrim(Edit1.text));
  if copy(dtInicial.text, 1, 2) <> '  ' then
    QCtrc.SQL[8] := 'and z.datdoc between to_date(''' + dtInicial.text +
      ''',''dd/mm/yy'') and to_date(''' + dtFinal.text + ''',''dd/mm/yy'')';
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmKardex_Rodopar.Edit1Exit(Sender: TObject);
begin
  if alltrim(Edit1.text) <> '' then
  begin
    edProd.clear;
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.SQL.clear;
    dtmdados.RQuery1.SQL.Add
      ('select distinct * from wmspro r where r.refere = :0 ');
    dtmdados.RQuery1.Parameters[0].value := alltrim(Edit1.text);
    dtmdados.RQuery1.open;
    if dtmdados.RQuery1.Eof then
    begin
      showmessage('Produto N�o Encontrado !!');
      Edit1.setfocus;
    end;
  end;
end;

procedure TfrmKardex_Rodopar.edProdExit(Sender: TObject);
begin
  if alltrim(edProd.text) <> '' then
  begin
    Edit1.clear;
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.SQL.clear;
    dtmdados.RQuery1.SQL.Add
      ('select distinct * from wmsios r where r.codpro = :0 ');
    dtmdados.RQuery1.Parameters[0].value := alltrim(edProd.text);
    dtmdados.RQuery1.open;
    if dtmdados.RQuery1.Eof then
    begin
      showmessage('Produto N�o Encontrado !!');
      edProd.setfocus;
    end;
  end;
end;

procedure TfrmKardex_Rodopar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmKardex_Rodopar.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true;
end;

end.
