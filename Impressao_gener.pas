unit Impressao_gener;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RLReport, Vcl.Imaging.jpeg, RLParser,
  Data.DB, Data.Win.ADODB;

type
  TfrmImpressao_gener = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLIMIW: TRLImage;
    RlImInt: TRLImage;
    RLLabel1: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel3: TRLLabel;
    RLBand3: TRLBand;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel17: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RLDBOBS: TRLDBText;
    RLLabel4: TRLLabel;
    RLDBResult6: TRLDBResult;
    RLTTPagarDes: TRLLabel;
    RLDBResult9: TRLDBResult;
    RLDBResult10: TRLDBResult;
    RLDBResult11: TRLDBResult;
    RLGroup1: TRLGroup;
    RLBand5: TRLBand;
    RLDBResult7: TRLDBResult;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText2: TRLDBText;
    RLBand2: TRLBand;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText10: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBResult8: TRLDBResult;
    RLBand4: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLExpressionParser1: TRLExpressionParser;
    RLDBText1: TRLDBText;
    QRep: TADOQuery;
    QRepDATA: TDateTimeField;
    QRepAUTORIZACAO: TFMTBCDField;
    QRepFL_STATUS: TStringField;
    QRepSERVICO: TStringField;
    QRepFILIAL: TFMTBCDField;
    QRepCTE_GEN: TFMTBCDField;
    QRepQUANTIDADE: TBCDField;
    QRepVLR_TOTAL_C: TBCDField;
    QRepVLR_TOTAL_R: TBCDField;
    QRepCNPJ_CLIENTE: TStringField;
    QRepFATURA: TStringField;
    QRepNR_CTE_PAGAR: TFMTBCDField;
    QRepCUSTO_COBRADO: TBCDField;
    QRepUSER_FATURA: TStringField;
    QRepDT_FATURA: TDateTimeField;
    QRepCLIENTE: TStringField;
    QRepCODCLIFOR: TFMTBCDField;
    QRepTRANSPORTADORA: TStringField;
    QRepMARGEM: TFMTBCDField;
    QRepMANIFESTO: TFMTBCDField;
    QRepLIBERACAO: TStringField;
    QRepNOVO_CUSTO: TBCDField;
    QRepVL_DESCONTO: TBCDField;
    QRepOBS_FATURA: TStringField;
    QRepVL_IMPOSTO_NFS: TBCDField;
    QRepCUSTO: TFMTBCDField;
    QRepVL_PROCESSO: TBCDField;
    dsRep: TDataSource;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImpressao_gener: TfrmImpressao_gener;

implementation

{$R *.dfm}

uses Dados, RelCusto_Generalidades;

procedure TfrmImpressao_gener.RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
begin
  RLTTPagarDes.Caption := FormatFloat('#,##0.00', RLDBResult11.Value -
  QRepVL_DESCONTO.Value - QRepVL_PROCESSO.Value);
end;

procedure TfrmImpressao_gener.RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
begin

  RLLabel1.Caption := 'Transportador : ' + QRepTRANSPORTADORA.AsString;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add
    ('select substr(razsoc,1,instr(razsoc,'' '')-1) filial from rodfil ');
  dtmDados.RQuery1.sql.Add('where codfil = ' + QRepFILIAL.AsString);
  dtmDados.RQuery1.open;
  if dtmDados.RQuery1.FieldByName('filial').Value = 'IW' then
  begin
    RLIMIW.Enabled := true;
    RlImInt.Enabled := false;
  end
  else
  begin
    RLIMIW.Enabled := false;
    RlImInt.Enabled := true;
  end;
  dtmDados.RQuery1.close;
  RlLabel3.Caption := 'Fatura : ' + QRepFATURA.AsString;
end;

end.
