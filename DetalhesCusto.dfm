object frmDetalhesCusto: TfrmDetalhesCusto
  Left = 0
  Top = 0
  Caption = 'Detalhes do Custo Cobrado'
  ClientHeight = 312
  ClientWidth = 429
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel9: TPanel
    Left = 0
    Top = 0
    Width = 429
    Height = 312
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 0
    object JvDBGrid2: TJvDBGrid
      Left = 1
      Top = 1
      Width = 427
      Height = 277
      Align = alClient
      DataSource = dtsComp
      DrawingStyle = gdsClassic
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'PARA'
          Title.Alignment = taCenter
          Title.Caption = 'Campo'
          Title.Color = clWhite
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = 6316032
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = [fsBold]
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'custo'
          Title.Alignment = taCenter
          Title.Caption = 'Custo Cobr.'
          Title.Color = clWhite
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = 6316032
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = [fsBold]
          Width = 102
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Alignment = taCenter
          Title.Caption = 'Custo Calc.'
          Title.Color = clWhite
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = 6316032
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = [fsBold]
          Width = 102
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Difer'
          Title.Alignment = taCenter
          Title.Caption = 'Diferen'#231'a'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = [fsBold]
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 1
      Top = 278
      Width = 427
      Height = 33
      Align = alBottom
      TabOrder = 1
      object Label13: TLabel
        Left = 16
        Top = 12
        Width = 58
        Height = 13
        Caption = 'Valor Total :'
      end
      object edTotalCobr: TJvCalcEdit
        Left = 119
        Top = 6
        Width = 81
        Height = 21
        DisplayFormat = ',0.00'
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
      end
      object edTotalTMS: TJvCalcEdit
        Left = 227
        Top = 6
        Width = 81
        Height = 21
        DisplayFormat = ',0.00'
        ShowButton = False
        TabOrder = 1
        DecimalPlacesAlwaysShown = False
      end
      object btnFecharCtrc: TBitBtn
        Left = 402
        Top = 6
        Width = 20
        Height = 19
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
          EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
          F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
          F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
          F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
          F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
          FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
          FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
          FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
          FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
          FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
          FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
          FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
          FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
          FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
          FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
          FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
          FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
          FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
          FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
          FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
          FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
          FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
          FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 2
        TabStop = False
        OnClick = btnFecharCtrcClick
      end
      object edDifer: TJvCalcEdit
        Left = 319
        Top = 6
        Width = 81
        Height = 21
        DisplayFormat = ',0.00'
        ShowButton = False
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
      end
    end
  end
  object QComp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    OnCalcFields = QCompCalcFields
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select upper(c.para) para, valor, de'
      
        'from tb_conversao c left join cyber.rodcli cl on substr(fnc_cnpj' +
        '(cl.codcgc),1,8) = c.cnpj'
      'where cl.codclifor = :0'
      'and tipo = '#39'V'#39
      'order by 1')
    Left = 64
    Top = 104
    object QCompPARA: TStringField
      FieldName = 'PARA'
      Size = 50
    end
    object QCompVALOR: TBCDField
      FieldName = 'VALOR'
      DisplayFormat = '##,##0.00'
      Precision = 14
      Size = 2
    end
    object QCompDE: TStringField
      FieldName = 'DE'
    end
    object QCompcusto: TFloatField
      FieldKind = fkCalculated
      FieldName = 'custo'
      DisplayFormat = '##,##0.00'
      Calculated = True
    end
    object QCompDifer: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Difer'
      DisplayFormat = '##,##0.00'
      Calculated = True
    end
  end
  object dtsComp: TDataSource
    DataSet = QComp
    Left = 116
    Top = 104
  end
end
