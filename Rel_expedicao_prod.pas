unit Rel_expedicao_prod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls, JvDBControls, JvMaskEdit, ADODB, DB,
  ExtCtrls, JvExExtCtrls, JvRadioGroup, JvBaseEdits, JvExStdCtrls, JvEdit,
  FileCtrl, JvDBLookup, Mask, JvExMask, JvToolEdit, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, Gauges, ShellApi;

type
  TfrmRel_expedicao_prod = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledCliente: TJvDBLookupEdit;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    ra: TDataSource;
    btnExcel: TBitBtn;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    Label1: TLabel;
    edVolume: TJvEdit;
    Label2: TLabel;
    edmanifesto: TJvCalcEdit;
    QClienteNM_CLIENTE: TStringField;
    RGTipo: TJvRadioGroup;
    QOcorr: TADOQuery;
    JvDBGrid2: TJvDBGrid;
    QOcorrCODBAR: TStringField;
    QOcorrDSC_MENSAGEM: TStringField;
    QOcorrDAT_INC: TDateTimeField;
    QOcorrCOD_USUINC: TBCDField;
    QOcorrNOM_USUINC: TStringField;
    QOcorrCOD_MANIFESTO: TBCDField;
    QCtrcMANIFESTO: TBCDField;
    QCtrcCODBAR: TStringField;
    QCtrcCOD_BARRAS_INT: TStringField;
    QCtrcDAT_CONFERENCIA: TDateTimeField;
    QCtrcNM_CLIENTE: TStringField;
    SP_ITC: TADOStoredProc;
    edHrF: TJvMaskEdit;
    edHrI: TJvMaskEdit;
    JvDBNavigator1: TJvDBNavigator;
    QCtrcORDCOM: TStringField;
    Label3: TLabel;
    edSerie: TJvEdit;
    QCtrcDAT_INC: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure edSerieExit(Sender: TObject);
    procedure QCtrcBeforeOpen(DataSet: TDataSet);
    procedure QOcorrBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_expedicao_prod: TfrmRel_expedicao_prod;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_expedicao_prod.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  if RGTipo.ItemIndex = 0 then
    sarquivo := DirectoryListBox1.Directory + '\Rel_coletor_expedicao.html'
  else
    sarquivo := DirectoryListBox1.Directory + '\Rel_ocorrencia_expedicao.html';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  if (RGTipo.ItemIndex = 0) or (RGTipo.ItemIndex = 2) then
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
    begin
      Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
        + '</th>');
    end;
    Memo1.Add('</tr>');
    QCtrc.First;
    while not QCtrc.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
      QCtrc.Next;
      Memo1.Add('<tr>');
    end;
  end
  else
  begin
    for i := 0 to JvDBGrid2.Columns.Count - 1 do
    begin
      Memo1.Add('<th><FONT class=texto1>' + JvDBGrid2.Columns[i].Title.Caption
        + '</th>');
    end;
    Memo1.Add('</tr>');
    QOcorr.First;
    while not QOcorr.Eof do
    begin
      for i := 0 to JvDBGrid2.Columns.Count - 1 do
        Memo1.Add('<th><FONT class=texto2>' + QOcorr.fieldbyname
          (JvDBGrid2.Columns[i].FieldName).AsString + '</th>');
      QOcorr.Next;
      Memo1.Add('<tr>');
    end;
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  Memo1.Free;
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmRel_expedicao_prod.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  if RGTipo.ItemIndex = 0 then
  begin
    navnavig.DataSet := QCtrc;
    JvDBGrid2.Visible := false;
    JvDBGrid1.Visible := true;
    QCtrc.close;
    if edmanifesto.value > 0 then
      QCtrc.sql[5] := 'and manifesto = ' + QuotedStr(apcarac(edmanifesto.Text))
    else
      QCtrc.sql[5] := '';
    if copy(dtInicial.Text, 1, 2) <> '  ' then
      QCtrc.sql[6] := 'and dat_conferencia between to_date(''' + dtInicial.Text
        + ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        dtFinal.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QCtrc.sql[6] := '';
    if alltrim(edVolume.Text) <> '' then
      QCtrc.sql[7] := 'and codbar = ' + #39 + edVolume.Text + #39
    else
      QCtrc.sql[7] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QCtrc.sql[8] := 'and nm_cliente = ''' + ledCliente.LookupValue + ''''
    else
      QCtrc.sql[8] := '';
    QCtrc.open;
  end
  else if RGTipo.ItemIndex = 1 then
  begin
    navnavig.DataSet := QOcorr;
    JvDBGrid1.Visible := false;
    JvDBGrid2.Visible := true;
    QOcorr.close;
    if edmanifesto.value > 0 then
      QOcorr.sql[3] := 'and ma.codman = ' + QuotedStr(apcarac(edmanifesto.Text))
    else
      QOcorr.sql[3] := '';
    if copy(dtInicial.Text, 1, 2) <> '  ' then
      QOcorr.sql[4] := 'and lg.dat_inc between to_date(''' + dtInicial.Text +
        ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        dtFinal.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QOcorr.sql[4] := '';
    if alltrim(edVolume.Text) <> '' then
      QOcorr.sql[5] := 'and lg.codbar = ' + #39 + edVolume.Text + #39
    else
      QOcorr.sql[5] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QOcorr.sql[6] := 'AND cl.nom_usuario = ''' + ledCliente.LookupValue + ''''
    else
      QOcorr.sql[6] := '';
    QOcorr.open;
  end
  else if RGTipo.ItemIndex = 2 then
  begin
    navnavig.DataSet := QCtrc;
    JvDBGrid2.Visible := false;
    JvDBGrid1.Visible := true;
    QCtrc.close;
    QCtrc.sql[16] := 'and em.dat_conferencia is null ';
    if copy(dtInicial.Text, 1, 2) <> '  ' then
      QCtrc.sql[17] := 'and em.dat_inc between to_date(''' + dtInicial.Text +
        ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        dtFinal.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QCtrc.sql[17] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QCtrc.sql[19] := 'and cl.nom_usuario = ''' + ledCliente.LookupValue + ''''
    else
      QCtrc.sql[19] := '';
    QCtrc.open;
  end;
  Panel1.Visible := false;
end;

procedure TfrmRel_expedicao_prod.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_expedicao_prod.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_expedicao_prod.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_expedicao_prod.edSerieExit(Sender: TObject);
begin
  if (edmanifesto.Text <> '') and (edSerie.Text <> '') then
  begin
    SP_ITC.Parameters[0].value := edmanifesto.Text;
    SP_ITC.Parameters[1].value := edSerie.Text;
    SP_ITC.Parameters[2].value := GLBFilial;
    SP_ITC.ExecProc;
  end;
end;

procedure TfrmRel_expedicao_prod.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
  QOcorr.close;
end;

procedure TfrmRel_expedicao_prod.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true;
end;

procedure TfrmRel_expedicao_prod.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_expedicao_prod.QCtrcBeforeOpen(DataSet: TDataSet);
begin
  QCtrc.Parameters[0].value := GLBFilial;
end;

procedure TfrmRel_expedicao_prod.QOcorrBeforeOpen(DataSet: TDataSet);
begin
  QOcorr.Parameters[0].value := GLBFilial;
end;

end.
