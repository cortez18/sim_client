unit LiberaPagto_Gen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TfrmLiberaPagto_Gen = class(TForm)
    Panel9: TPanel;
    Label9: TLabel;
    edMotivo: TEdit;
    btnSalva_motivo: TBitBtn;
    Label1: TLabel;
    edValor: TJvCalcEdit;
    procedure btnSalva_motivoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLiberaPagto_Gen: TfrmLiberaPagto_Gen;

implementation

uses Dados, RelCusto_Generalidades;

{$R *.dfm}

procedure TfrmLiberaPagto_Gen.btnSalva_motivoClick(Sender: TObject);
begin
  if trim(edMotivo.Text) = '' then
  begin
    ShowMessage('� necess�rio digitar mo motivo');
    edMotivo.setfocus;
  end;

  if edValor.Value = 0 then
  begin
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('update crm_generalidades set liberacao = :0 ');
    dtmdados.IQuery1.sql.add
      ('where id_generalidade = :1 ');
    dtmdados.IQuery1.Parameters[0].Value := edMotivo.Text;
    dtmdados.IQuery1.Parameters[1].Value :=
      frmRelCusto_Generalidades.mdGenerAutorizacao.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
  end
  else
  begin
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add
      ('update crm_generalidades set liberacao = :0, novo_custo = :1 ');
    dtmdados.IQuery1.sql.add
      ('where id_generalidade = :2 ');
    dtmdados.IQuery1.Parameters[0].Value := edMotivo.Text;
    dtmdados.IQuery1.Parameters[1].Value := edValor.Value;
    dtmdados.IQuery1.Parameters[2].Value :=
      frmRelCusto_Generalidades.mdGenerAutorizacao.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
  end;

  Close;
end;

procedure TfrmLiberaPagto_Gen.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if trim(edMotivo.Text) = '' then
  begin
    ShowMessage('� Necess�rio Digitar o Motivo');
    edMotivo.setfocus;
    Action := caNone;
  end
  else
    Action := caFree;
end;

procedure TfrmLiberaPagto_Gen.FormCreate(Sender: TObject);
begin
  edMotivo.Text := frmRelCusto_Generalidades.mdGenerliberacao.Value;
  edValor.value := frmRelCusto_Generalidades.mdGenernovo_custo.Value;
end;

end.
