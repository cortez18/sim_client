unit ImpFatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, Buttons, ComCtrls,
  Gauges, ADODB, FileCtrl, ComObj, JvExStdCtrls, JvListBox, JvDriveCtrls,
  JvCombobox, DateUtils;

type
  TfrmImpFatura = class(TForm)
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    btnImportar: TBitBtn;
    btnSair: TBitBtn;
    PB1: TGauge;
    Label5: TLabel;
    Label7: TLabel;
    QCte: TADOQuery;
    JvDriveCombo1: TJvDriveCombo;
    JvDirectoryListBox1: TJvDirectoryListBox;
    FLB: TJvFileListBox;
    SPFatura: TADOStoredProc;
    SPITens: TADOStoredProc;
    QCteNR_CONHECIMENTO: TBCDField;
    QCteVL_TOTAL: TFloatField;
    QCtePRAZO_PGTO: TFMTBCDField;
    QCtePRAZO_ESPECIAL: TStringField;
    QCteDIA_SEMANA: TFMTBCDField;
    procedure btnSairClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FLBClick(Sender: TObject);
    function RegraVcto(): TDateTime;
  private
    procedure importar;

  var
    fat: String;
  public
    { Public declarations }
  end;

var
  frmImpFatura: TfrmImpFatura;

implementation

uses Dados, funcoes, menu;

{$R *.dfm}

procedure TfrmImpFatura.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImpFatura.FLBClick(Sender: TObject);
begin
  Label5.caption := FLB.FileName;
end;

procedure TfrmImpFatura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCte.Close;
end;

procedure TfrmImpFatura.importar;
var
  nm, ok, pfat, nf: String;
  i, linhas, cod, a, vezes, fatura: integer;
  xlsObj: OleVariant;
  wb: variant;
  valor: double;
begin
  try
    xlsObj := CreateOleObject('Excel.Application');
    wb := xlsObj.Workbooks.open(FLB.FileName);
    i := 1; // linha que come�a na planilha
    a := 0;
    vezes := 0;
    nm := xlsObj.Workbooks[1].Sheets[1].Cells[i, 1];
    while trim(nm) <> '' do
    begin
      ok := '';
      Application.ProcessMessages;
      nm := xlsObj.Workbooks[1].Sheets[1].Cells[i, 1];
      linhas := xlsObj.Cells.SpecialCells(11).Row;
      PB1.MaxValue := linhas;
      if trim(nm) = '' then
      begin
        Showmessage('Processo terminado!!!');
        exit;
      end;
      // c�digo do cliente
      if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]) = 'C�DIGO' then
        cod := StrToInt(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 2]));
      if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]) = 'OBSERVA��O' then
        pfat := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 2]);
      if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]) = 'CT-e' then
      begin
        if a = 0 then
        begin
          nf := QuotedStr(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 2]));
          a := a + 1;
        end
        else
        begin
          nf := nf + ',' +
            QuotedStr(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 2]));
        end;
      end;
      if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]) = 'FIM' then
      begin
        a := 0;
        valor := 0;
        vezes := vezes + 1;
        QCte.Close;
        QCte.sql[2] := 'where c.nr_conhecimento in (' + nf + ')';
        QCte.Parameters[0].value := cod;
        QCte.Parameters[1].value := GLBFilial;
        QCte.open;
        while not QCte.eof do
        begin
          valor := valor + QCteVL_TOTAL.value;
          QCte.next;
        end;
        if valor = 0 then
        begin
          Showmessage(' N�o Gerada pois n�o foi encontrado os CT-es');
        end
        else
        begin
          SPFatura.Parameters[1].value := cod;
          SPFatura.Parameters[2].value := RegraVcto;
          SPFatura.Parameters[3].value := valor;
          SPFatura.Parameters[4].value := GlbUser;
          SPFatura.Parameters[5].value := glbfilial;
          SPFatura.Parameters[6].value := 0;
          SPFatura.Parameters[7].value := pfat;
          SPFatura.ExecProc;
          if vezes = 1 then
            fat := IntToStr(SPFatura.Parameters[0].value)
          else
            fat := fat + ',' + IntToStr(SPFatura.Parameters[0].value);
          SPFatura.Close;
          QCte.first;
          while not QCte.eof do
          begin
            SPITens.Parameters[0].value := SPFatura.Parameters[0].value;
            SPITens.Parameters[1].value := QCteVL_TOTAL.value;
            SPITens.Parameters[2].value := GlbUser;
            SPITens.Parameters[3].value := glbfilial;
            SPITens.Parameters[4].value := QCteNR_CONHECIMENTO.AsInteger;
            SPITens.ExecProc;
            SPITens.Close;
            QCte.next;
          end;
          QCte.Close;

          dtmdados.RQuery1.Close;
          dtmdados.RQuery1.sql.clear;
          dtmdados.RQuery1.sql.add
            ('update cyber.roddup set vlrliq = (select sum(VLRDOC) from cyber.rodidu where numdup = :0 and codfil = :1), ');
          dtmdados.RQuery1.sql.add
            ('vlrdup = (select sum(VLRDOC) from cyber.rodidu where numdup = :2 and codfil = :3) ');
          dtmdados.RQuery1.sql.add('where numdup = :4 and codfil = :5 ');
          dtmdados.RQuery1.Parameters[0].value := SPFatura.Parameters[0].value;
          dtmdados.RQuery1.Parameters[1].value := glbfilial;
          dtmdados.RQuery1.Parameters[2].value := SPFatura.Parameters[0].value;
          dtmdados.RQuery1.Parameters[3].value := glbfilial;
          dtmdados.RQuery1.Parameters[4].value := SPFatura.Parameters[0].value;
          dtmdados.RQuery1.Parameters[5].value := glbfilial;
          dtmdados.RQuery1.ExecSQL;
        end;
      end;
      PB1.AddProgress(1);
      inc(i);
    end;
    xlsObj.quit;
  except
    xlsObj.quit;
  end;
end;

procedure TfrmImpFatura.btnImportarClick(Sender: TObject);
begin
  if Application.Messagebox('Voc� Deseja Gerar esta Fatura ?', 'Gerar Fatura',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    importar;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    Showmessage('Fatura(s) Gerada(s) ' + fat);
    RenameFile(FLB.FileName, (copy(FLB.FileName, 1, (Length(FLB.FileName) - 3))
      + '_OK'));
    FLB.ItemIndex := FLB.ItemIndex + 1;
    FLB.Update;
    btnSair.Enabled := true;
  end;
end;

function TfrmImpFatura.RegraVcto;
var
  data, dtespcial, dttemp: TDateTime;
  ds: Integer;
  diaant, diaatu: string;
begin

  data := date + QCTeprazo_pgto.AsInteger;  //Pega data atual + valor setado PRAZO DE PAGAMENTO
  diaant := formatdatetime('dd', data);   //Extrai dia do m�s

  if QCTePRAZO_ESPECIAL.AsString = 'S' then
    begin
        if StrToInt(diaant) <= 15 then  //caindo na 1� quinzena data recebe dia 15
        begin
           dttemp := StrToDateTime(formatdatetime(''+inttostr(15)+'/mm/yyyy', data));
           dtespcial := dttemp;
        end
        else
        begin
           diaatu := FormatDateTime('dd',(EndOfTheMonth(data))); //sen�o data recebe ultimo dia do m�s da 2� quinzena
           dttemp := StrToDateTime(formatdatetime(''+diaatu+'/mm/yyyy', data));
           dtespcial := dttemp;
        end;
     data := dtespcial;
   end;


 //fluxo normal
  ds := DayOfWeek(data);

  if QCTedia_semana.AsInteger > 0 then
  begin
    if ds > QCTedia_semana.AsInteger then
    begin
      data := data + (7 - (ds - QCTedia_semana.AsInteger));
    end;

    if ds < QCTedia_semana.AsInteger then
    begin
      data := data + (QCTedia_semana.AsInteger - ds);
    end;
  end;

  result := data;
end;

end.
