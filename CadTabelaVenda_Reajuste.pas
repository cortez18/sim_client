unit CadTabelaVenda_Reajuste;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, ActnList, JvExStdCtrls,
  JvToolEdit, JvBaseEdits, Mask, JvExMask, JvMaskEdit, Grids, ComObj, ShellApi,
  DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, JvMemo, JvEdit,
  System.Actions, JvDBControls, Vcl.CheckLst, JvExCheckLst, JvCheckListBox,
  JvMemoryDataset, System.ImageList, Vcl.ImgList;

type
  TfrmCadTabelaVenda_Reajuste = class(TForm)
    PageControl1: TPageControl;
    dtstabela: TDataSource;
    dtsItem: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    Qcadtabela: TADOQuery;
    QItemCOD_TAB: TBCDField;
    QItemNR_PESO_DE: TBCDField;
    QItemNR_PESO_ATE: TBCDField;
    QItemVL_RODOVIARIO: TBCDField;
    QItemDATAI: TDateTimeField;
    QItemDATAF: TDateTimeField;
    QItemVR_CTRC: TBCDField;
    QItemVR_EXCED: TBCDField;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    btnFechar: TBitBtn;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    QTabelaCOD_VENDA: TBCDField;
    QTabelaCOD_CLIENTE: TBCDField;
    QTabelaFL_LOCAL: TStringField;
    QTabelaDS_UF: TStringField;
    QTabelaFL_LOCAL_DES: TStringField;
    QTabelaDS_UF_DES: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_PEDAGIO: TBCDField;
    QTabelaVL_OUTROS: TBCDField;
    QTabelaVL_OUTROS_MINIMO: TBCDField;
    QTabelaVL_EXCEDENTE: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaVL_GRIS: TFloatField;
    QTabelaVL_GRIS_MINIMO: TFloatField;
    QTabelaVL_ENTREGA: TBCDField;
    QTabelaSEGURO_MINIMO: TFloatField;
    QTabelaFL_PESO: TStringField;
    QTabelaFL_IMPOSTO: TStringField;
    QTabelaMODAL: TStringField;
    QTabelaVEICULO: TStringField;
    QTabelaSITE: TBCDField;
    QTabelaUSUARIO: TStringField;
    QTabelaDT_CADASTRO: TDateTimeField;
    QTabelaCODMUN: TBCDField;
    QTabelaCODMUNO: TBCDField;
    QTabelaVL_TDE: TBCDField;
    QTabelaVL_TR: TBCDField;
    QTabelaVL_TDEP: TBCDField;
    QTabelaVL_TDEM: TBCDField;
    QTabelaFL_STATUS: TStringField;
    QTabelaOPERACAO: TStringField;
    QTabelaVL_TDEMAX: TBCDField;
    QTabelaNM_CLIENTE: TStringField;
    QTabelaLOCAL: TStringField;
    QTabelaLOCAL_DES: TStringField;
    QcadtabelaCOD_VENDA: TBCDField;
    QcadtabelaCOD_CLIENTE: TBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TBCDField;
    QcadtabelaUSUARIO: TStringField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QcadtabelaCODMUN: TBCDField;
    QcadtabelaCODMUNO: TBCDField;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TBCDField;
    QcadtabelaVL_TDEP: TBCDField;
    QcadtabelaVL_TDEM: TBCDField;
    QcadtabelaFL_STATUS: TStringField;
    QcadtabelaOPERACAO: TStringField;
    QcadtabelaVL_TDEMAX: TBCDField;
    QItemCOD_VENDA: TBCDField;
    QItemUSUARIO: TStringField;
    QItemDATAINC: TDateTimeField;
    QItemUSUALT: TStringField;
    QItemDATAALT: TDateTimeField;
    QTabelaOBS: TStringField;
    QcadtabelaOBS: TStringField;
    QTabelaREGIAO: TStringField;
    QcadtabelaREGIAO: TStringField;
    QTabelaVL_OUTROS_P: TBCDField;
    QTabelaVL_SEG_BALSA: TBCDField;
    QTabelaVL_REDEP_FLUVIAL: TBCDField;
    QTabelaVL_AGEND: TBCDField;
    QTabelaVL_PALLET: TBCDField;
    QTabelaVL_TAS: TBCDField;
    QTabelaVL_DESPACHO: TBCDField;
    QTabelaVL_ENTREGA_PORTO: TBCDField;
    QTabelaVL_ALFAND: TBCDField;
    QTabelaVL_CANHOTO: TBCDField;
    QcadtabelaVL_OUTROS_P: TBCDField;
    QcadtabelaVL_SEG_BALSA: TBCDField;
    QcadtabelaVL_REDEP_FLUVIAL: TBCDField;
    QcadtabelaVL_AGEND: TBCDField;
    QcadtabelaVL_PALLET: TBCDField;
    QcadtabelaVL_TAS: TBCDField;
    QcadtabelaVL_DESPACHO: TBCDField;
    QcadtabelaVL_ENTREGA_PORTO: TBCDField;
    QcadtabelaVL_ALFAND: TBCDField;
    QcadtabelaVL_CANHOTO: TBCDField;
    QTabelaVL_FLUVIAL: TBCDField;
    QTabelaVL_FLUVIAL_M: TBCDField;
    QcadtabelaVL_FLUVIAL: TBCDField;
    QcadtabelaVL_FLUVIAL_M: TBCDField;
    QTabelaDT_INICIAL: TDateTimeField;
    QTabelaDT_FINAL: TDateTimeField;
    QcadtabelaDT_INICIAL: TDateTimeField;
    QcadtabelaDT_FINAL: TDateTimeField;
    QTabelaCONTROLER: TStringField;
    QTabelaDT_CONTROLER: TDateTimeField;
    QTabelaVL_DEVOLUCAO: TBCDField;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    QcadtabelaVL_DEVOLUCAO: TBCDField;
    QItemVL_MINIMO: TBCDField;
    QTabelaFL_RATEIO: TStringField;
    QcadtabelaFL_RATEIO: TStringField;
    QTabelaVL_FLUVMIN: TBCDField;
    QTabelaVL_AJUDA: TBCDField;
    QcadtabelaVL_FLUVMIN: TBCDField;
    QcadtabelaVL_AJUDA: TBCDField;
    QTabelaFL_FRETE: TStringField;
    QTabelaFL_PEDAGIO: TStringField;
    QTabelaFL_EXCEDENTE: TStringField;
    QTabelaFL_AD: TStringField;
    QTabelaFL_GRIS: TStringField;
    QTabelaFL_ENTREGA: TStringField;
    QTabelaFL_SEGURO: TStringField;
    QTabelaFL_TDE: TStringField;
    QTabelaFL_TR: TStringField;
    QTabelaFL_SEG_BALSA: TStringField;
    QTabelaFL_REDEP_FLUVIAL: TStringField;
    QTabelaFL_AGEND: TStringField;
    QTabelaFL_PALLET: TStringField;
    QTabelaFL_TAS: TStringField;
    QTabelaFL_DESPACHO: TStringField;
    QTabelaFL_ENTREGA_PORTO: TStringField;
    QTabelaFL_ALFAND: TStringField;
    QTabelaFL_CANHOTO: TStringField;
    QTabelaFL_FLUVIAL: TStringField;
    QTabelaFL_DEVOLUCAO: TStringField;
    QTabelaFL_AJUDA: TStringField;
    QcadtabelaFL_FRETE: TStringField;
    QcadtabelaFL_PEDAGIO: TStringField;
    QcadtabelaFL_EXCEDENTE: TStringField;
    QcadtabelaFL_AD: TStringField;
    QcadtabelaFL_GRIS: TStringField;
    QcadtabelaFL_ENTREGA: TStringField;
    QcadtabelaFL_SEGURO: TStringField;
    QcadtabelaFL_TDE: TStringField;
    QcadtabelaFL_TR: TStringField;
    QcadtabelaFL_SEG_BALSA: TStringField;
    QcadtabelaFL_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_AGEND: TStringField;
    QcadtabelaFL_PALLET: TStringField;
    QcadtabelaFL_TAS: TStringField;
    QcadtabelaFL_DESPACHO: TStringField;
    QcadtabelaFL_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_ALFAND: TStringField;
    QcadtabelaFL_CANHOTO: TStringField;
    QcadtabelaFL_FLUVIAL: TStringField;
    QcadtabelaFL_DEVOLUCAO: TStringField;
    QcadtabelaFL_AJUDA: TStringField;
    QcadtabelaFL_CTE: TStringField;
    QTabelaFL_CTE: TStringField;
    QcadtabelaFL_NF_FRETE: TStringField;
    QcadtabelaFL_NF_PEDAGIO: TStringField;
    QcadtabelaFL_NF_EXCEDENTE: TStringField;
    QcadtabelaFL_NF_AD: TStringField;
    QcadtabelaFL_NF_GRIS: TStringField;
    QcadtabelaFL_NF_ENTREGA: TStringField;
    QcadtabelaFL_NF_SEGURO: TStringField;
    QcadtabelaFL_NF_TDE: TStringField;
    QcadtabelaFL_NF_TR: TStringField;
    QcadtabelaFL_NF_SEG_BALSA: TStringField;
    QcadtabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_NF_AGEND: TStringField;
    QcadtabelaFL_NF_PALLET: TStringField;
    QcadtabelaFL_NF_TAS: TStringField;
    QcadtabelaFL_NF_DESPACHO: TStringField;
    QcadtabelaFL_NF_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_NF_ALFAND: TStringField;
    QcadtabelaFL_NF_CANHOTO: TStringField;
    QcadtabelaFL_NF_FLUVIAL: TStringField;
    QcadtabelaFL_NF_DEVOLUCAO: TStringField;
    QcadtabelaFL_NF_AJUDA: TStringField;
    QcadtabelaFL_NF_CTE: TStringField;
    QTabelaFL_NF_FRETE: TStringField;
    QTabelaFL_NF_PEDAGIO: TStringField;
    QTabelaFL_NF_EXCEDENTE: TStringField;
    QTabelaFL_NF_AD: TStringField;
    QTabelaFL_NF_GRIS: TStringField;
    QTabelaFL_NF_ENTREGA: TStringField;
    QTabelaFL_NF_SEGURO: TStringField;
    QTabelaFL_NF_TDE: TStringField;
    QTabelaFL_NF_TR: TStringField;
    QTabelaFL_NF_SEG_BALSA: TStringField;
    QTabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QTabelaFL_NF_AGEND: TStringField;
    QTabelaFL_NF_PALLET: TStringField;
    QTabelaFL_NF_TAS: TStringField;
    QTabelaFL_NF_DESPACHO: TStringField;
    QTabelaFL_NF_ENTREGA_PORTO: TStringField;
    QTabelaFL_NF_ALFAND: TStringField;
    QTabelaFL_NF_CANHOTO: TStringField;
    QTabelaFL_NF_FLUVIAL: TStringField;
    QTabelaFL_NF_DEVOLUCAO: TStringField;
    QTabelaFL_NF_AJUDA: TStringField;
    QTabelaFL_NF_CTE: TStringField;
    QTabelaVL_REENT_MINIMA: TBCDField;
    QcadtabelaVL_REENT_MINIMA: TBCDField;
    btnOK: TBitBtn;
    QcadtabelaDOC_CONTROLER: TStringField;
    QTabelareprovado: TStringField;
    QTabelaCONTROLER_REPR: TStringField;
    QcadtabelaREPROVADO: TStringField;
    QcadtabelaCONTROLER_REPR: TStringField;
    QcadtabelaVL_ARMAZENAGEM: TBCDField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_DEDICADO: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    QTabelaDOC_CONTROLER: TStringField;
    QTabelaVL_ARMAZENAGEM: TBCDField;
    QTabelaVL_DIARIA: TBCDField;
    QTabelaVL_REENTREGA: TBCDField;
    QTabelaVL_DEDICADO: TBCDField;
    QTabelaVL_PERNOITE: TBCDField;
    QTabelaVL_HORA_PARADA: TBCDField;
    QTabelaFL_IMPOSTO_ICMS: TStringField;
    QcadtabelaFL_IMPOSTO_ICMS: TStringField;
    QTabelaVL_TAXA_TRT: TFloatField;
    QcadtabelaVL_TAXA_TRT: TFloatField;
    QTabelaVL_FRETE_VALOR: TFloatField;
    QcadtabelaVL_FRETE_VALOR: TFloatField;
    Panel5: TPanel;
    Label15: TLabel;
    cbPesqTransp: TComboBox;
    Label16: TLabel;
    cbPesqUFO: TComboBox;
    cbPesqLO: TComboBox;
    Label53: TLabel;
    cbCidOPesq: TComboBox;
    Label24: TLabel;
    cbVeiculoPesq: TComboBox;
    Label29: TLabel;
    cbOperacaoPesq: TComboBox;
    Label17: TLabel;
    cbPesqUFD: TComboBox;
    cbPesqLD: TComboBox;
    cbCidDPesq: TComboBox;
    Label54: TLabel;
    btProcurar: TBitBtn;
    Label41: TLabel;
    edTabela: TJvCalcEdit;
    Label1: TLabel;
    EdPer: TJvCalcEdit;
    Label2: TLabel;
    Label6: TLabel;
    MaskEdit3: TMaskEdit;
    Label14: TLabel;
    MaskEdit4: TMaskEdit;
    chkcampos: TJvCheckListBox;
    dbgContrato: TJvDBUltimGrid;
    SPReajuste: TADOStoredProc;
    checkFrac: TJvCheckListBox;
    Label3: TLabel;
    Label4: TLabel;
    CheckGen: TJvCheckListBox;
    Label5: TLabel;
    Label7: TLabel;
    MaskEdit1: TMaskEdit;
    Memo1: TMemo;
    Label8: TLabel;
    MDRea: TJvMemoryData;
    MDReaCOD_VENDA: TBCDField;
    MDReaCOD_CLIENTE: TBCDField;
    MDReaFL_LOCAL: TStringField;
    MDReaDS_UF: TStringField;
    MDReaFL_LOCAL_DES: TStringField;
    MDReaDS_UF_DES: TStringField;
    MDReaVEICULO: TStringField;
    MDReaOPERACAO: TStringField;
    MDReaNM_CLIENTE: TStringField;
    iml: TImageList;
    MDReareg: TIntegerField;
    edData: TJvDateEdit;
    Label9: TLabel;
    checkReeFrac: TJvCheckListBox;
    Label10: TLabel;
    btnDcto: TBitBtn;
    OpenDialog: TOpenDialog;

    procedure btnFecharClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnOKClick(Sender: TObject);
    procedure cbPesqLOChange(Sender: TObject);
    procedure cbPesqLDChange(Sender: TObject);
    procedure dbgContratoCellClick(Column: TColumn);
    procedure MDReaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure btnDctoClick(Sender: TObject);
  private
    quant: Integer;
    tipo, sql, nomepdf: string;
  public
    { Public declarations }
  end;

var
  frmCadTabelaVenda_Reajuste: TfrmCadTabelaVenda_Reajuste;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadTabelaVenda_Reajuste.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo, altera, sData: string;
begin
  caminho := GLBControler;
  sData := FormatDateTime('YYYY', now()) +
           FormatDateTime('MM', now());
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      arquivo := caminho + 'R_' + sdata + ExtractFileName(OpenDialog.FileName);
      nomepdf := 'R_' + sdata + ExtractFileName(OpenDialog.FileName);
      // verifica se existe este arquivo j� gravado, sim renomeia
      if FileExists(arquivo) then
      begin
        altera := copy(arquivo, 1, length(arquivo) - 4) + 'A' +
          apcarac(DateToStr(now())) + '.pdf';
        RenameFile(arquivo, altera);
      end;
      CopyFile(Pchar(ori), Pchar(des), true);

      RenameFile(des, arquivo);

      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('update tb_tabelavenda set doc_controler = :c where cod_venda = :n');
      dtmDados.iQuery1.Parameters[0].value := 'R_' +
        QcadtabelaCOD_VENDA.AsString + ExtractFileName(OpenDialog.FileName);
      dtmDados.iQuery1.Parameters[1].value := QcadtabelaCOD_VENDA.AsInteger;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTabelaVenda_Reajuste.btnOKClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if nomepdf = '' then
  begin
    ShowMessage('N�o foi selecionado o arquivo PDF');
    Exit;
  end;

  if (MaskEdit4.Text = '') or (MaskEdit3.Text = '') or (MaskEdit1.Text = '')
  then
  begin
    ShowMessage('Voc� N�o Completou as Datas');
    Exit;
  end;
  MDRea.Filtered := true;
  if Application.Messagebox
    ('Voc� Deseja Atualizar os Fretes de Todas Tabela Relacionadas no Grid?',
    'Atualizar Fretes', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QTabela.First;
    While not QTabela.Eof do
    begin
      SPReajuste.Parameters[0].value := QTabelaCOD_VENDA.AsInteger;
      SPReajuste.Parameters[1].value := StrToDate(MaskEdit3.Text);
      SPReajuste.Parameters[2].value := StrToDate(MaskEdit4.Text);
      if (chkcampos.State[0] = cbChecked) then
        SPReajuste.Parameters[3].value := 1
      else
        SPReajuste.Parameters[3].value := 0;
      if (chkcampos.State[1] = cbChecked) then
        SPReajuste.Parameters[4].value := 1
      else
        SPReajuste.Parameters[4].value := 0;
      if (chkcampos.State[2] = cbChecked) then
        SPReajuste.Parameters[5].value := 1
      else
        SPReajuste.Parameters[5].value := 0;
      if (chkcampos.State[3] = cbChecked) then
        SPReajuste.Parameters[6].value := 1
      else
        SPReajuste.Parameters[6].value := 0;
      if (chkcampos.State[4] = cbChecked) then
        SPReajuste.Parameters[7].value := 1
      else
        SPReajuste.Parameters[7].value := 0;
      if (chkcampos.State[5] = cbChecked) then
        SPReajuste.Parameters[8].value := 1
      else
        SPReajuste.Parameters[8].value := 0;
      if (chkcampos.State[6] = cbChecked) then
        SPReajuste.Parameters[9].value := 1
      else
        SPReajuste.Parameters[9].value := 0;
      if (chkcampos.State[7] = cbChecked) then
        SPReajuste.Parameters[10].value := 1
      else
        SPReajuste.Parameters[10].value := 0;
      if (chkcampos.State[8] = cbChecked) then
        SPReajuste.Parameters[11].value := 1
      else
        SPReajuste.Parameters[11].value := 0;
      if (chkcampos.State[9] = cbChecked) then
        SPReajuste.Parameters[12].value := 1
      else
        SPReajuste.Parameters[12].value := 0;
      if (chkcampos.State[10] = cbChecked) then
        SPReajuste.Parameters[13].value := 1
      else
        SPReajuste.Parameters[13].value := 0;
      if (chkcampos.State[11] = cbChecked) then
        SPReajuste.Parameters[14].value := 1
      else
        SPReajuste.Parameters[14].value := 0;
      if (chkcampos.State[12] = cbChecked) then
        SPReajuste.Parameters[15].value := 1
      else
        SPReajuste.Parameters[15].value := 0;
      if (chkcampos.State[13] = cbChecked) then
        SPReajuste.Parameters[16].value := 1
      else
        SPReajuste.Parameters[16].value := 0;
      if (chkcampos.State[14] = cbChecked) then
        SPReajuste.Parameters[17].value := 1
      else
        SPReajuste.Parameters[17].value := 0;
      if (chkcampos.State[15] = cbChecked) then
        SPReajuste.Parameters[18].value := 1
      else
        SPReajuste.Parameters[18].value := 0;
      if (chkcampos.State[16] = cbChecked) then
        SPReajuste.Parameters[19].value := 1
      else
        SPReajuste.Parameters[19].value := 0;
      if (chkcampos.State[17] = cbChecked) then
        SPReajuste.Parameters[42].value := 1
      else
        SPReajuste.Parameters[42].value := 0;

      if (chkcampos.State[18] = cbChecked) then
        SPReajuste.Parameters[44].value := 1
      else
        SPReajuste.Parameters[44].value := 0;
      if (chkcampos.State[19] = cbChecked) then
        SPReajuste.Parameters[45].value := 1
      else
        SPReajuste.Parameters[45].value := 0;

      // Generalidades
      if (CheckGen.State[0] = cbChecked) then
        SPReajuste.Parameters[20].value := 1
      else
        SPReajuste.Parameters[20].value := 0;
      if (CheckGen.State[1] = cbChecked) then
        SPReajuste.Parameters[21].value := 1
      else
        SPReajuste.Parameters[21].value := 0;
      if (CheckGen.State[1] = cbChecked) then
        SPReajuste.Parameters[22].value := 1
      else
        SPReajuste.Parameters[22].value := 0;
      if (CheckGen.State[3] = cbChecked) then
        SPReajuste.Parameters[23].value := 1
      else
        SPReajuste.Parameters[23].value := 0;
      if (CheckGen.State[4] = cbChecked) then
        SPReajuste.Parameters[24].value := 1
      else
        SPReajuste.Parameters[24].value := 0;
      if (CheckGen.State[5] = cbChecked) then
        SPReajuste.Parameters[25].value := 1
      else
        SPReajuste.Parameters[25].value := 0;
      if (CheckGen.State[6] = cbChecked) then
        SPReajuste.Parameters[26].value := 1
      else
        SPReajuste.Parameters[26].value := 0;
      if (CheckGen.State[7] = cbChecked) then
        SPReajuste.Parameters[27].value := 1
      else
        SPReajuste.Parameters[27].value := 0;
      if (CheckGen.State[8] = cbChecked) then
        SPReajuste.Parameters[28].value := 1
      else
        SPReajuste.Parameters[28].value := 0;

      // Fra��o
      if (checkFrac.State[0] = cbChecked) then
        SPReajuste.Parameters[29].value := 1
      else
        SPReajuste.Parameters[29].value := 0;
      if (checkFrac.State[1] = cbChecked) then
        SPReajuste.Parameters[30].value := 1
      else
        SPReajuste.Parameters[30].value := 0;
      if (checkFrac.State[2] = cbChecked) then
        SPReajuste.Parameters[31].value := 1
      else
        SPReajuste.Parameters[31].value := 0;
      if (checkFrac.State[3] = cbChecked) then
        SPReajuste.Parameters[32].value := 1
      else
        SPReajuste.Parameters[32].value := 0;
      if (checkFrac.State[4] = cbChecked) then
        SPReajuste.Parameters[37].value := 1
      else
        SPReajuste.Parameters[37].value := 0;

      SPReajuste.Parameters[33].value := EdPer.value;
      SPReajuste.Parameters[34].value := GlbCodUser;
      SPReajuste.Parameters[35].value := StrToDate(MaskEdit1.Text);
      SPReajuste.Parameters[36].value := Memo1.Text;

      // Reentrega Fra��o
      if (checkReeFrac.State[0] = cbChecked) then
        SPReajuste.Parameters[38].value := 1
      else
        SPReajuste.Parameters[38].value := 0;
      if (checkReeFrac.State[1] = cbChecked) then
        SPReajuste.Parameters[39].value := 1
      else
        SPReajuste.Parameters[39].value := 0;
      if (checkReeFrac.State[2] = cbChecked) then
        SPReajuste.Parameters[40].value := 1
      else
        SPReajuste.Parameters[40].value := 0;
      if (checkReeFrac.State[3] = cbChecked) then
        SPReajuste.Parameters[41].value := 1
      else
        SPReajuste.Parameters[41].value := 0;

      SPReajuste.ExecProc;
      QTabela.Next;
    end;
    ShowMessage('Tabela(s) Reajustada(s)');
    btnOK.Enabled := false;
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.dbgContratoTitleClick(Column: TColumn);
var
  icount: Integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.sql.Text) > 0 then
  begin
    QTabela.sql.Text := Copy(QTabela.sql.Text, 1,
      Pos('order by', QTabela.sql.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.sql.Text := QTabela.sql.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTabelaVenda_Reajuste.FormCreate(Sender: TObject);
begin
  QCliente.Open;
  sql := QTabela.sql.Text;

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add
    ('select distinct operacao from tb_operacao where fl_receita = ''S'' order by 1');
  dtmDados.IQuery1.Open;
  cbOperacaoPesq.clear;

  cbOperacaoPesq.Items.Add('');
  while not dtmDados.IQuery1.Eof do
  begin
    cbOperacaoPesq.Items.Add(dtmDados.IQuery1.FieldByName('operacao').value);
    dtmDados.IQuery1.Next;
  end;

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add
    ('SELECT distinct descri FROM cyber.RODFRO order by 1');
  dtmDados.IQuery1.Open;
  cbVeiculoPesq.clear;
  cbVeiculoPesq.Items.Add('');
  while not dtmDados.IQuery1.Eof do
  begin
    cbVeiculoPesq.Items.Add(dtmDados.IQuery1.FieldByName('descri').value);
    dtmDados.IQuery1.Next;
  end;

  dtmDados.IQuery1.Close;
  QCliente.Open;
  cbPesqTransp.clear;
  cbPesqTransp.Items.Add('');
  While not QCliente.Eof do
  begin
    cbPesqTransp.Items.Add(alltrim(QClienteNM_CLIENTE.value));
    QCliente.Next;
  end;
  Memo1.clear;
end;

procedure TfrmCadTabelaVenda_Reajuste.MDReaFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := MDRea['Reg'] = 1;
end;

procedure TfrmCadTabelaVenda_Reajuste.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
  QCliente.Close;
end;

procedure TfrmCadTabelaVenda_Reajuste.dbgContratoCellClick(Column: TColumn);
begin
  if (Column.Field = MDReareg) then
  begin
    MDRea.edit;
    if (MDReareg.value = 1) then
      MDReareg.value := 0
    else
      MDReareg.value := 1;
    MDRea.post;
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadTabelaVenda_Reajuste.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = MDReareg) then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if MDReareg.value = 1 then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.btProcurarClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QTabela.Close;
  QTabela.sql.clear;
  QTabela.sql.Add('select distinct t.*, f.nomeab || '' - '' || f.codcgc nm_cliente, ');
  QTabela.sql.Add('CASE ');
  QTabela.sql.Add('   WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ');
  QTabela.sql.Add('ELSE ');
  QTabela.sql.Add('   ''INTERIOR'' ');
  QTabela.sql.Add('end as local, ');
  QTabela.sql.Add('CASE ');
  QTabela.sql.Add('   WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ');
  QTabela.sql.Add('ELSE ');
  QTabela.sql.Add('   ''INTERIOR'' ');
  QTabela.sql.Add('end as local_des ');
  QTabela.sql.Add
    ('from tb_tabelavenda t inner join cyber.rodcli f on t.cod_cliente = f.codclifor ');
  if copy(edData.Text,1,2) <> '  ' then
    QTabela.sql.add
      ('left join tb_tabelavenda_item i on i.cod_venda = t.cod_venda ');
  QTabela.sql.Add('where f.nomeab is not null and fl_status = ''S'' ');

  if edTabela.value > 0 then
  begin
    QTabela.sql.Add('and t.cod_venda = ' + #39 + apcarac(edTabela.Text) + #39);
  end;

  if cbPesqTransp.Text <> '' then
  begin
    QTabela.sql.Add('and (f.nomeab || '' - '' || f.codcgc) = ' + #39 +
      cbPesqTransp.Text + #39);
  end;
  if cbVeiculoPesq.Text <> '' then
  begin
    QTabela.sql.Add('and t.veiculo = ' + #39 + cbVeiculoPesq.Text + #39);
  end;
  if cbPesqUFO.Text <> '' then
  begin
    QTabela.sql.Add('and t.ds_uf = ' + #39 + cbPesqUFO.Text + #39);
  end;
  if cbPesqUFD.Text <> '' then
  begin
    QTabela.sql.Add('and t.ds_uf_des = ' + #39 + cbPesqUFD.Text + #39);
  end;
  if cbPesqLO.Text <> '' then
  begin
    QTabela.sql.Add('and t.fl_local = ' + #39 + Copy(cbPesqLO.Text, 1,
      1) + #39);
    if cbCidOPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidOPesq.Text, []);
      QTabela.sql.Add('and t.codmuno = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbPesqLD.Text <> '' then
  begin
    QTabela.sql.Add('and t.fl_local_des = ' + #39 + Copy(cbPesqLD.Text, 1,
      1) + #39);
    if cbCidDPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidDPesq.Text, []);
      QTabela.sql.Add('and t.codmun = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbOperacaoPesq.Text <> '' then
  begin
    QTabela.sql.Add('and t.operacao = ' + #39 + (cbOperacaoPesq.Text) + #39);
  end;

  if copy(edData.Text,1,2) <> '  ' then
    QTabela.sql.Add('and trunc(dataf) = to_date(' + #39 + formatdatetime('dd/mm/yyyy',edData.Date) + #39 + ',''dd/mm/yyyy'')' );

  QTabela.sql.Add('order by t.cod_venda ');
  //showmessage(Qtabela.sql.Text);
  QTabela.Open;
  Screen.Cursor := crDefault;
  if QTabela.Eof then
  begin
    ShowMessage('N�o Encontrado Nenhum Resultado Para Esta Pesquisa !!');
  end
  else
  begin
    btnOK.Enabled := true;
    MDRea.Close;
    MDRea.Open;
    while not QTabela.Eof do
    begin
      MDRea.Append;
      MDReaCOD_VENDA.value := QTabelaCOD_VENDA.value;
      MDReaCOD_CLIENTE.value := QTabelaCOD_CLIENTE.value;
      MDReaFL_LOCAL.value := QTabelaFL_LOCAL.value;
      MDReaDS_UF.value := QTabelaDS_UF.value;
      MDReaFL_LOCAL_DES.value := QTabelaFL_LOCAL_DES.value;
      MDReaDS_UF_DES.value := QTabelaDS_UF_DES.value;
      MDReaVEICULO.value := QTabelaVEICULO.value;
      MDReaOPERACAO.value := QTabelaOPERACAO.value;
      MDReaNM_CLIENTE.value := QTabelaNM_CLIENTE.value;
      MDReareg.value := 1;
      MDRea.post;
      QTabela.Next;
    end;
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.cbPesqLDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(Copy(cbPesqLD.Text, 1, 1));
  QCidade.Open;
  cbCidDPesq.clear;
  cbCidDPesq.Items.Add('');
  while not QCidade.Eof do
  begin
    cbCidDPesq.Items.Add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaVenda_Reajuste.cbPesqLOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(Copy(cbPesqLO.Text, 1, 1));
  QCidade.Open;
  cbCidOPesq.clear;
  cbCidOPesq.Items.Add('');
  while not QCidade.Eof do
  begin
    cbCidOPesq.Items.Add(QCidadeDESCRI.value);
    QCidade.Next;
  end;
end;

end.
