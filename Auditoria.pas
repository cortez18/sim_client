unit Auditoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ComCtrls;

type
  TfrmAuditoria = class(TForm)
    QAudit: TADOQuery;
    btnAlterar: TBitBtn;
    Label1: TLabel;
    edProcura: TJvCalcEdit;
    RGDoc: TRadioGroup;
    DBGrid1: TDBGrid;
    QAuditCOD_AUDITORIA: TBCDField;
    QAuditTABELA: TStringField;
    QAuditACAO: TStringField;
    QAuditCHAVE_INT: TBCDField;
    QAuditCHAVE_CHAR: TStringField;
    QAuditDESCRICAO1: TStringField;
    QAuditDESCRICAO2: TStringField;
    QAuditDESCRICAO3: TStringField;
    QAuditDESCRICAO4: TStringField;
    QAuditMAQUINA: TStringField;
    QAuditUSUARIO: TStringField;
    QAuditDAT_INC: TDateTimeField;
    dtsAudit: TDataSource;
    DBRichEdit1: TDBRichEdit;
    DBRichEdit2: TDBRichEdit;
    DBRichEdit3: TDBRichEdit;
    DBRichEdit4: TDBRichEdit;
    DBNavigator1: TDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAuditoria: TfrmAuditoria;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmAuditoria.btnAlterarClick(Sender: TObject);
begin
  QAudit.close;

  if RGDoc.ItemIndex > 0 then
    QAudit.SQL[5] := 'and tabela = ' + #39 + RGDoc.Items[RGDoc.ItemIndex] + #39
  else
    QAudit.SQL[5] := ' ';

  if edProcura.Value > 0 then
    QAudit.SQL[6] := 'and chave_int = ' + #39 + ApCarac(edProcura.Text) + #39
  else
    QAudit.SQL[6] := ' ';

  QAudit.open;
end;

procedure TfrmAuditoria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QAudit.close;
end;

procedure TfrmAuditoria.FormCreate(Sender: TObject);
begin
  dtmdados.Iquery1.close;
  dtmdados.Iquery1.SQL.clear;
  dtmdados.Iquery1.SQL.Add
    ('select distinct tabela from tb_auditoria_sim order by tabela');
  dtmdados.Iquery1.open;
  RGDoc.Items.clear;
  RGDoc.Items.Add('Todas Tabelas');
  while not dtmdados.Iquery1.Eof do
  begin
    RGDoc.Items.Add(dtmdados.Iquery1.FieldByName('tabela').AsString);
    dtmdados.Iquery1.Next;
  end;
  dtmdados.Iquery1.close;
end;

end.
