object frmORxNF: TfrmORxNF
  Left = 0
  Top = 0
  ClientHeight = 462
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 56
    Top = 11
    Width = 15
    Height = 13
    Caption = 'OR'
  end
  object Label1: TLabel
    Left = 452
    Top = 8
    Width = 13
    Height = 13
    Caption = 'NF'
  end
  object Edit1: TEdit
    Left = 77
    Top = 8
    Width = 76
    Height = 21
    TabOrder = 0
    OnEnter = Edit1Enter
    OnExit = Edit1Exit
  end
  object TreeView: TTreeView
    Left = 437
    Top = 35
    Width = 327
    Height = 419
    Indent = 19
    TabOrder = 1
  end
  object btnXML: TBitBtn
    Left = 360
    Top = 4
    Width = 75
    Height = 25
    Caption = 'XML'
    TabOrder = 2
    OnClick = btnXMLClick
  end
  object DBGrid1: TDBGrid
    Left = 5
    Top = 35
    Width = 205
    Height = 419
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_ARTIKEL'
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUANT'
        Width = 72
        Visible = True
      end>
  end
  object DBGrid2: TDBGrid
    Left = 221
    Top = 35
    Width = 205
    Height = 419
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid2DrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'Produto'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Quant'
        Visible = True
      end>
  end
  object DataSource1: TDataSource
    DataSet = QOS
    Left = 60
    Top = 164
  end
  object QOS: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select id_artikel, sum(mng_ist) quant from wepos where nr_we = :' +
        '0'
      'group by id_artikel order by 1')
    Left = 24
    Top = 164
    object QOSID_ARTIKEL: TStringField
      FieldName = 'ID_ARTIKEL'
      Size = 40
    end
    object QOSQUANT: TBCDField
      FieldName = 'QUANT'
      ReadOnly = True
      Precision = 32
    end
  end
  object SPNF: TADOStoredProc
    ProcedureName = 'PRC_NF_TAREFA'
    Parameters = <
      item
        Name = 'OS'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'NOTA'
        Attributes = [paNullable]
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'SERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = 'DATAEMI'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = 'VOLUME'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'PESO'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'VALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'PROD'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'CUBADO'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'TIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VREVP'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'COD'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VREC'
        DataType = ftFloat
        Value = 0.000000000000000000
      end>
    Left = 820
    Top = 648
  end
  object ACBrNFe1: TACBrNFe
    Configuracoes.Geral.SSLLib = libCapicomDelphiSoap
    Configuracoes.Geral.SSLCryptLib = cryCapicom
    Configuracoes.Geral.SSLHttpLib = httpIndy
    Configuracoes.Geral.SSLXmlSignLib = xsMsXmlCapicom
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Configuracoes.RespTec.IdCSRT = 0
    Left = 264
    Top = 140
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*-nfe.XML'
    Filter = 
      'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|To' +
      'dos os Arquivos (*.*)|*.*'
    Title = 'Selecione a NFe'
    Left = 316
    Top = 140
  end
  object MData: TJvMemoryData
    FieldDefs = <>
    Left = 480
    Top = 56
    object MDataProduto: TStringField
      FieldName = 'Produto'
    end
    object MDataQuant: TIntegerField
      FieldName = 'Quant'
    end
    object MDatacheck: TStringField
      FieldName = 'check'
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = MData
    Left = 552
    Top = 56
  end
end
