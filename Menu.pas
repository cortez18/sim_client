unit Menu;

interface

uses
  Windows, Messages, SysUtils, Forms, Vcl.Dialogs, ActnList, ActnMan, ToolWin,
  ActnCtrls, StdCtrls, Grids, ActnMenus, Classes, XPStyleActnCtrls, Controls,
  jpeg, ExtCtrls, ComCtrls, XPMan, ACBrMDFeDAMDFeClass, ACBrMDFe, DB, ADODB,
  math, ACBrCTeDACTEClass, ACBrCTe, ACBrCTeDACTeRLClass, ACBrMDFeDAMDFeRLClass,
  RLFilters, ACBrBase, ACBrDFe, pcnAuxiliar, AcbrDfeUtil, ACBrMail,
  DBGrids, System.Actions, ACBrDFeReport, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.types, System.UITypes;

Const
  InputBoxMessage = WM_USER + 200;

type
  TfrmMenu = class(TForm)
    ActionMainMenuBar1: TActionMainMenuBar;
    ActionManager1: TActionManager;
    Action2: TAction;
    Panel1: TPanel;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    Action16: TAction;
    Action18: TAction;
    Action19: TAction;
    Action20: TAction;
    Action21: TAction;
    Action22: TAction;
    Action23: TAction;
    Action26: TAction;
    Action27: TAction;
    Action8: TAction;
    Action33: TAction;
    Action35: TAction;
    Action36: TAction;
    Action37: TAction;
    Action39: TAction;
    Action40: TAction;
    Action41: TAction;
    Action42: TAction;
    Action43: TAction;
    Action44: TAction;
    Action45: TAction;
    Action46: TAction;
    Action47: TAction;
    Action49: TAction;
    Action48: TAction;
    Action50: TAction;
    Action52: TAction;
    Action53: TAction;
    Action54: TAction;
    Action56: TAction;
    Action57: TAction;
    Action58: TAction;
    Qpercurso: TADOQuery;
    QpercursoUF: TStringField;
    QpercursoORDEM: TBCDField;
    Action59: TAction;
    Action60: TAction;
    Action63: TAction;
    Action64: TAction;
    Action65: TAction;
    Action66: TAction;
    Action67: TAction;
    Action68: TAction;
    Action69: TAction;
    Action71: TAction;
    Action72: TAction;
    Action32: TAction;
    Action73: TAction;
    Action74: TAction;
    Action75: TAction;
    Action76: TAction;
    Action77: TAction;
    qrCteEletronico: TADOQuery;
    QNF: TADOQuery;
    ACBrCTe1: TACBrCTe;
    QCteXml: TADOQuery;
    QCteXmlCOD_CONHECIMENTO: TBCDField;
    QCteXmlXML: TBlobField;
    Query1: TADOQuery;
    Atu_Rodopar: TADOStoredProc;
    Action78: TAction;
    QNFID_NF: TBCDField;
    QNFCODCON: TBCDField;
    QNFSERCON: TStringField;
    QNFNOTFIS: TStringField;
    QNFSERIEN: TStringField;
    QNFDATNOT: TDateTimeField;
    QNFQUANTI: TBCDField;
    QNFPESOKG: TBCDField;
    QNFPESCUB: TBCDField;
    QNFVLRMER: TBCDField;
    QNFDATINC: TDateTimeField;
    QNFNOTNFE: TStringField;
    QNFFL_EMPRESA: TBCDField;
    QNFESPECIE: TStringField;
    ADOQuery1: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    BCDField3: TBCDField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    BCDField4: TBCDField;
    DateTimeField1: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    BCDField5: TBCDField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    StringField39: TStringField;
    StringField40: TStringField;
    StringField41: TStringField;
    StringField42: TStringField;
    StringField43: TStringField;
    StringField44: TStringField;
    StringField45: TStringField;
    BCDField6: TBCDField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    BCDField7: TBCDField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    StringField66: TStringField;
    StringField67: TStringField;
    StringField68: TStringField;
    StringField69: TStringField;
    StringField70: TStringField;
    StringField71: TStringField;
    StringField72: TStringField;
    StringField73: TStringField;
    BCDField8: TBCDField;
    StringField74: TStringField;
    StringField75: TStringField;
    StringField76: TStringField;
    StringField77: TStringField;
    StringField78: TStringField;
    StringField79: TStringField;
    StringField80: TStringField;
    StringField81: TStringField;
    StringField82: TStringField;
    StringField83: TStringField;
    StringField84: TStringField;
    StringField85: TStringField;
    StringField86: TStringField;
    StringField87: TStringField;
    BCDField9: TBCDField;
    StringField88: TStringField;
    StringField89: TStringField;
    StringField90: TStringField;
    StringField91: TStringField;
    StringField92: TStringField;
    StringField93: TStringField;
    StringField94: TStringField;
    StringField95: TStringField;
    StringField96: TStringField;
    StringField97: TStringField;
    StringField98: TStringField;
    StringField99: TStringField;
    StringField100: TStringField;
    BCDField10: TBCDField;
    StringField101: TStringField;
    StringField102: TStringField;
    StringField103: TStringField;
    StringField104: TStringField;
    StringField105: TStringField;
    StringField106: TStringField;
    StringField107: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    StringField108: TStringField;
    FloatField3: TFloatField;
    StringField109: TStringField;
    FloatField4: TFloatField;
    StringField110: TStringField;
    FloatField5: TFloatField;
    StringField111: TStringField;
    FloatField6: TFloatField;
    StringField112: TStringField;
    FloatField7: TFloatField;
    StringField113: TStringField;
    FloatField8: TFloatField;
    StringField114: TStringField;
    FloatField9: TFloatField;
    StringField115: TStringField;
    FloatField10: TFloatField;
    StringField116: TStringField;
    FloatField11: TFloatField;
    StringField117: TStringField;
    FloatField12: TFloatField;
    StringField118: TStringField;
    FloatField13: TFloatField;
    StringField119: TStringField;
    FloatField14: TFloatField;
    StringField120: TStringField;
    FloatField15: TFloatField;
    StringField121: TStringField;
    FloatField16: TFloatField;
    StringField122: TStringField;
    FloatField17: TFloatField;
    StringField123: TStringField;
    FloatField18: TFloatField;
    StringField124: TStringField;
    FloatField19: TFloatField;
    StringField125: TStringField;
    FloatField20: TFloatField;
    StringField126: TStringField;
    FloatField21: TFloatField;
    StringField127: TStringField;
    FloatField22: TFloatField;
    StringField128: TStringField;
    StringField129: TStringField;
    FloatField23: TFloatField;
    BCDField11: TBCDField;
    BCDField12: TBCDField;
    FloatField24: TFloatField;
    StringField130: TStringField;
    StringField131: TStringField;
    StringField132: TStringField;
    FloatField25: TFloatField;
    StringField133: TStringField;
    DateTimeField2: TDateTimeField;
    StringField134: TStringField;
    FloatField26: TFloatField;
    FloatField27: TFloatField;
    BCDField13: TBCDField;
    StringField135: TStringField;
    StringField136: TStringField;
    StringField137: TStringField;
    StringField138: TStringField;
    StringField139: TStringField;
    StringField140: TStringField;
    StringField141: TStringField;
    StringField142: TStringField;
    StringField143: TStringField;
    StringField144: TStringField;
    StringField145: TStringField;
    StringField146: TStringField;
    StringField147: TStringField;
    StringField148: TStringField;
    StringField149: TStringField;
    BCDField14: TBCDField;
    StringField150: TStringField;
    StringField151: TStringField;
    StringField152: TStringField;
    StringField153: TStringField;
    StringField154: TStringField;
    StringField155: TStringField;
    StringField156: TStringField;
    Action79: TAction;
    Action80: TAction;
    Action81: TAction;
    ACBrCTeDACTeRL1: TACBrCTeDACTeRL;
    ACBrMDFeDAMDFeRL1: TACBrMDFeDAMDFeRL;
    qrCteEletronicoCOD_CONHECIMENTO: TBCDField;
    qrCteEletronicoFL_EMPRESA: TBCDField;
    qrCteEletronicoCUFEMIT: TStringField;
    qrCteEletronicoCODUFORIGEM: TStringField;
    qrCteEletronicoUFORI: TStringField;
    qrCteEletronicoCODCTRC: TBCDField;
    qrCteEletronicoCTE_CHAVE: TStringField;
    qrCteEletronicoCFOP: TStringField;
    qrCteEletronicoNATOPERACAO: TStringField;
    qrCteEletronicoNR_SERIE: TStringField;
    qrCteEletronicoFORMAPAGTO: TStringField;
    qrCteEletronicoMODELOCTE: TStringField;
    qrCteEletronicoNUMEROCTE: TBCDField;
    qrCteEletronicoEMISSAOCTE: TDateTimeField;
    qrCteEletronicoTPIMP: TStringField;
    qrCteEletronicoTPEMIS: TStringField;
    qrCteEletronicoDIGVERFICHAVEACES: TStringField;
    qrCteEletronicoTPAMB: TStringField;
    qrCteEletronicoTPCTE: TStringField;
    qrCteEletronicoPROCEMI: TStringField;
    qrCteEletronicoVERPROC: TStringField;
    qrCteEletronicoCODMUNENVI: TStringField;
    qrCteEletronicoMUNENVI: TStringField;
    qrCteEletronicoUFENV: TStringField;
    qrCteEletronicoMODAL: TStringField;
    qrCteEletronicoTPSERV: TStringField;
    qrCteEletronicoCODMUNINI: TStringField;
    qrCteEletronicoMUNINI: TStringField;
    qrCteEletronicoUFINI: TStringField;
    qrCteEletronicoCMUNFIM: TStringField;
    qrCteEletronicoXMUNFIM: TStringField;
    qrCteEletronicoUFFIM: TStringField;
    qrCteEletronicoRETIRA: TStringField;
    qrCteEletronicoTOMA03: TStringField;
    qrCteEletronicoCNPJEMI: TStringField;
    qrCteEletronicoIEEMI: TStringField;
    qrCteEletronicoNOMEEMI: TStringField;
    qrCteEletronicoLGREMI: TStringField;
    qrCteEletronicoNROEMI: TBCDField;
    qrCteEletronicoBAIRROEMMI: TStringField;
    qrCteEletronicoCODMUNEMI: TStringField;
    qrCteEletronicoMUNEMI: TStringField;
    qrCteEletronicoCEPEMI: TStringField;
    qrCteEletronicoUFEMI: TStringField;
    qrCteEletronicoFONEEMI: TStringField;
    qrCteEletronicoCNPJTOM: TStringField;
    qrCteEletronicoIETOM: TStringField;
    qrCteEletronicoNOMETOM: TStringField;
    qrCteEletronicoFONETOM: TStringField;
    qrCteEletronicoLGRTOM: TStringField;
    qrCteEletronicoCOMPLTOM: TStringField;
    qrCteEletronicoNROTOM: TBCDField;
    qrCteEletronicoBAIRROTOM: TStringField;
    qrCteEletronicoCODMUNTOM: TStringField;
    qrCteEletronicoMUNTOM: TStringField;
    qrCteEletronicoCEPTOM: TStringField;
    qrCteEletronicoUFTOM: TStringField;
    qrCteEletronicoCODPAISTOM: TStringField;
    qrCteEletronicoNOMEPAISTOM: TStringField;
    qrCteEletronicoTIPOPESSOAREM: TStringField;
    qrCteEletronicoCNPJREM: TStringField;
    qrCteEletronicoIEREM: TStringField;
    qrCteEletronicoNOMEREM: TStringField;
    qrCteEletronicoFONEREM: TStringField;
    qrCteEletronicoLGRREM: TStringField;
    qrCteEletronicoCOMPLREM: TStringField;
    qrCteEletronicoNROREM: TBCDField;
    qrCteEletronicoBAIRROREM: TStringField;
    qrCteEletronicoCODMUNREM: TStringField;
    qrCteEletronicoMUNREM: TStringField;
    qrCteEletronicoCEPREM: TStringField;
    qrCteEletronicoUFREM: TStringField;
    qrCteEletronicoCODPAISREM: TStringField;
    qrCteEletronicoNOMEPAISREM: TStringField;
    qrCteEletronicoTIPOPESSOAEXP: TStringField;
    qrCteEletronicoCNPJEXP: TStringField;
    qrCteEletronicoIEEXP: TStringField;
    qrCteEletronicoNOMEEXP: TStringField;
    qrCteEletronicoFONEEXP: TStringField;
    qrCteEletronicoLGREXP: TStringField;
    qrCteEletronicoCOMPLEXP: TStringField;
    qrCteEletronicoNROEXP: TBCDField;
    qrCteEletronicoBAIRROEXP: TStringField;
    qrCteEletronicoCODMUNEXP: TStringField;
    qrCteEletronicoMUNEXP: TStringField;
    qrCteEletronicoCEPEXP: TStringField;
    qrCteEletronicoUFEXP: TStringField;
    qrCteEletronicoCODPAISEXP: TStringField;
    qrCteEletronicoNOMEPAISEXP: TStringField;
    qrCteEletronicoCNPJDES: TStringField;
    qrCteEletronicoTIPOPESSOA: TStringField;
    qrCteEletronicoIEDES: TStringField;
    qrCteEletronicoNOMEDES: TStringField;
    qrCteEletronicoFONEDES: TStringField;
    qrCteEletronicoLGRDES: TStringField;
    qrCteEletronicoCOMPLDES: TStringField;
    qrCteEletronicoNRODES: TBCDField;
    qrCteEletronicoBAIRRODES: TStringField;
    qrCteEletronicoCODMUNDES: TStringField;
    qrCteEletronicoMUNDES: TStringField;
    qrCteEletronicoCEPDES: TStringField;
    qrCteEletronicoUFDES: TStringField;
    qrCteEletronicoCODPAISDES: TStringField;
    qrCteEletronicoNOMEPAISDES: TStringField;
    qrCteEletronicoCNPJRED: TStringField;
    qrCteEletronicoIERED: TStringField;
    qrCteEletronicoNOMERED: TStringField;
    qrCteEletronicoFONERED: TStringField;
    qrCteEletronicoLGRRED: TStringField;
    qrCteEletronicoCOMPLRED: TStringField;
    qrCteEletronicoNRORED: TBCDField;
    qrCteEletronicoBAIRRORED: TStringField;
    qrCteEletronicoCODMUNRED: TStringField;
    qrCteEletronicoMUNRED: TStringField;
    qrCteEletronicoCEPRED: TStringField;
    qrCteEletronicoUFRED: TStringField;
    qrCteEletronicoCODPAISRED: TStringField;
    qrCteEletronicoNOMEPAISRED: TStringField;
    qrCteEletronicoVLTOTPRESTACAO: TFloatField;
    qrCteEletronicoVLRECEBPRESTACAO: TFloatField;
    qrCteEletronicoCOMPFRETE_PESO: TStringField;
    qrCteEletronicoFRETE_PESO_XML: TBCDField;
    qrCteEletronicoCOMPDESPACHO: TStringField;
    qrCteEletronicoDESPACHO_XML: TBCDField;
    qrCteEletronicoCOMPGRIS: TStringField;
    qrCteEletronicoGRIS_XML: TBCDField;
    qrCteEletronicoCOMPADVALOREN: TStringField;
    qrCteEletronicoCOMPPEDAGIO: TStringField;
    qrCteEletronicoPEDAGIO_XML: TBCDField;
    qrCteEletronicoCOMPOUTRASTAXAS: TStringField;
    qrCteEletronicoOUTRAS_TAXAS_XML: TBCDField;
    qrCteEletronicoCOMPFLUVIAL: TStringField;
    qrCteEletronicoFLUVIAL_XML: TBCDField;
    qrCteEletronicoCOMPTR: TStringField;
    qrCteEletronicoTR_XML: TBCDField;
    qrCteEletronicoCOMPTDE: TStringField;
    qrCteEletronicoTDE_XML: TBCDField;
    qrCteEletronicoCOMPEXED: TStringField;
    qrCteEletronicoEXCEDENTE_XML: TBCDField;
    qrCteEletronicoCOMPBALSA: TStringField;
    qrCteEletronicoSEG_BALSA_XML: TBCDField;
    qrCteEletronicoCOMPREDFLU: TStringField;
    qrCteEletronicoREDEP_FLUVIAL_XML: TBCDField;
    qrCteEletronicoCOMPAGEND: TStringField;
    qrCteEletronicoAGEND_XML: TBCDField;
    qrCteEletronicoCOMPPALLET: TStringField;
    qrCteEletronicoPALLET_XML: TBCDField;
    qrCteEletronicoCOMPPORTO: TStringField;
    qrCteEletronicoENTREGA_PORTO_XML: TBCDField;
    qrCteEletronicoCOMPCANHOTO: TStringField;
    qrCteEletronicoCANHOTO_XML: TBCDField;
    qrCteEletronicoCOMPDEV: TStringField;
    qrCteEletronicoCOMPDEVVALOR: TFloatField;
    qrCteEletronicoCOMPTAS: TStringField;
    qrCteEletronicoTAS_XML: TBCDField;
    qrCteEletronicoCOMPTXCTE: TStringField;
    qrCteEletronicoTAXA_CTE_XML: TBCDField;
    qrCteEletronicoCSTIMPOSTO: TStringField;
    qrCteEletronicoDESCCSTIMPOSTO: TStringField;
    qrCteEletronicoBASEIMPOSTO: TFloatField;
    qrCteEletronicoALIQIMPOSTO: TBCDField;
    qrCteEletronicoVALORIMMPOSTO: TBCDField;
    qrCteEletronicoVALORCARGA: TFloatField;
    qrCteEletronicoDESCRICAOCARGA: TStringField;
    qrCteEletronicoCODIGOUNIDADE: TStringField;
    qrCteEletronicoTIPOMEDIDA: TStringField;
    qrCteEletronicoQTDMEDIDA: TFloatField;
    qrCteEletronicoRNTRC: TStringField;
    qrCteEletronicoDPREV: TDateTimeField;
    qrCteEletronicoLOTA: TStringField;
    qrCteEletronicoPESOTOTAL: TFloatField;
    qrCteEletronicoCUBAGEM: TFloatField;
    qrCteEletronicoPESOCALCULO: TBCDField;
    qrCteEletronicoDS_TIPO_FRETE: TStringField;
    qrCteEletronicoOBSERVACAO: TStringField;
    qrCteEletronicoTIPO: TStringField;
    qrCteEletronicoFORMAPAGAMENTO: TStringField;
    qrCteEletronicoTOMADOR: TStringField;
    qrCteEletronicoPROTOCOLO: TStringField;
    qrCteEletronicoFL_CONTINGENCIA: TStringField;
    qrCteEletronicoUFORICALC: TStringField;
    qrCteEletronicoCIDORICALC: TStringField;
    qrCteEletronicoUFDESCALC: TStringField;
    qrCteEletronicoCIDDESCALC: TStringField;
    qrCteEletronicoMOTORISTA: TStringField;
    qrCteEletronicoCPF_MOT: TStringField;
    qrCteEletronicoNM_USUARIO: TStringField;
    qrCteEletronicoSEGURADORA: TStringField;
    qrCteEletronicoAPOLICE: TStringField;
    qrCteEletronicoRESPSEG: TBCDField;
    qrCteEletronicoFL_STATUS: TStringField;
    qrCteEletronicoPLACA: TStringField;
    qrCteEletronicoRENAVAM: TStringField;
    qrCteEletronicoLOTACAO: TStringField;
    qrCteEletronicoUFVEI: TStringField;
    qrCteEletronicoCTE_PROT: TStringField;
    qrCteEletronicoTP_VEI: TStringField;
    qrCteEletronicoCTE_CHAVE_CTE_R: TStringField;
    qrCteEletronicoAD_VALOREM_XML: TBCDField;
    qrCteEletronicoAJUDANTE_XML: TBCDField;
    Action83: TAction;
    qrCteEletronicoOPERACAO: TStringField;
    qrCteEletronicoOBS_TRANSPARENCIA: TStringField;
    Action5: TAction;
    Action6: TAction;
    Action85: TAction;
    ACBrMail1: TACBrMail;
    Action89: TAction;
    Action91: TAction;
    Action90: TAction;
    Action92: TAction;
    Action94: TAction;
    Action95: TAction;
    qrCteEletronicoXML_SUBSTITUICAO: TStringField;
    qrCteEletronicoCTE_SUBSTITUICAO: TBCDField;
    Action96: TAction;
    Action99: TAction;
    Action101: TAction;
    QMDFe: TADOQuery;
    QMDFeMANIFESTO: TBCDField;
    QMDFeFILIAL: TBCDField;
    QMDFeCODUF: TStringField;
    QMDFeMODELOMDFE: TStringField;
    QMDFeSERIE: TStringField;
    QMDFeCMDF: TStringField;
    QMDFeMODAL: TStringField;
    QMDFeTPEMIS: TStringField;
    QMDFeVERPROC: TStringField;
    QMDFeUFINI: TStringField;
    QMDFeUFFIM: TStringField;
    QMDFeCNPJ_EMIT: TStringField;
    QMDFeIE_EMIT: TStringField;
    QMDFeNM_RAZAO: TStringField;
    QMDFeFANTASIA: TStringField;
    QMDFeDS_ENDERECO: TStringField;
    QMDFeDS_BAIRRO: TStringField;
    QMDFeDS_CEP: TStringField;
    QMDFeDS_NUMERO: TBCDField;
    QMDFeNR_TELEFONE: TStringField;
    QMDFeE_MAIL: TStringField;
    QMDFeCODMUNEMIT: TStringField;
    QMDFeMUNEMIT: TStringField;
    QMDFeUFEMIT: TStringField;
    QMDFeEMISSAO: TDateTimeField;
    QMDFeNR_RNTC: TStringField;
    QMDFePLACA: TStringField;
    QMDFeCODVEI: TStringField;
    QMDFeCAPACMC: TBCDField;
    QMDFeCAPACT: TBCDField;
    QMDFeCARRETA: TStringField;
    QMDFeCODCARRETA: TStringField;
    QMDFeCARRMC: TBCDField;
    QMDFeCARRCAP: TBCDField;
    QMDFeUF_CARR: TStringField;
    QMDFeCNPJ_PROPCA: TStringField;
    QMDFeRAZAO_PROPCA: TStringField;
    QMDFeFISJURCA: TStringField;
    QMDFeUF_PROPCA: TStringField;
    QMDFeIE_PROPCA: TStringField;
    QMDFeRNTRC_PROP: TStringField;
    QMDFeCNPJ_PROP: TStringField;
    QMDFeMOTORISTA: TStringField;
    QMDFeCNPJ_MOT: TStringField;
    QMDFeCHAVECTE: TStringField;
    QMDFeCODCON: TBCDField;
    QMDFeCIDADE: TStringField;
    QMDFeCODIGOIBGE: TStringField;
    QMDFeVOLUME: TBCDField;
    QMDFeTIPO: TStringField;
    QMDFePESO: TBCDField;
    QMDFeVL_NF: TBCDField;
    QMDFeFL_TIPO: TStringField;
    QMDFeIE_PROP: TStringField;
    QMDFeRAZAO_PROP: TStringField;
    QMDFeFISJUR: TStringField;
    QMDFeUF_PROP: TStringField;
    QMDFeMDFE_CHAVE: TStringField;
    QMDFeMDFE_PROTOCOLO: TStringField;
    QMDFeMDFE_ENCERRAMENTO: TStringField;
    QMDFeOPERACAO: TStringField;
    QMDFeVALOR_CUSTO: TBCDField;
    QMDFeOBS: TStringField;
    QMDFeSIS: TStringField;

    QCadSeguradora: TADOQuery;
    QCadSeguradoraCODIGO: TFMTBCDField;
    QCadSeguradoraSEGURADORA: TStringField;
    QCadSeguradoraCNPJ: TStringField;
    QCadSeguradoraAPOLICE: TStringField;
    QCadSeguradoraCONTATO: TStringField;
    QCadSeguradoraTELEFONE: TStringField;
    QCadSeguradoraDATAINICIO: TDateTimeField;
    QCadSeguradoraDATAFIM: TDateTimeField;
    QCadSeguradoraVALORMIN: TBCDField;
    QCadSeguradoraVALORMAX: TBCDField;
    QCadSeguradoraUSER_WS: TStringField;
    QCadSeguradoraPASS_WS: TStringField;
    QCadSeguradoraCODE_WS: TStringField;
    QCadSeguradoraFL_EMPRESA: TFMTBCDField;
    QCadSeguradoraDOC_SEGURADORA: TStringField;
    QCadSeguradoraWSDL: TStringField;

    Action102: TAction;
    Action103: TAction;
    Action104: TAction;
    Action105: TAction;
    Action106: TAction;
    Action107: TAction;
    QMDFeCIDCAR: TStringField;
    QMDFeCODCAR: TStringField;
    prc_averba: TADOStoredProc;
    Action108: TAction;
    Action109: TAction;
    ACBrMDFe1: TACBrMDFe;
    Image1: TImage;
    StatusBar1: TStatusBar;
    Action1: TAction;
    Action3: TAction;
    QAverba: TADOQuery;
    QAverbaCTE_CHAVE: TStringField;
    QAverbaDT_CONHECIMENTO: TDateTimeField;
    QAverbaCOD_CONHECIMENTO: TBCDField;
    QAverbaFL_EMPRESA: TBCDField;
    QMDFeaverba_protocolo: TStringField;
    QMDFeCIOT: TStringField;
    QMDFefilial_cte: TBCDField;
    Action7: TAction;
    Action17: TAction;
    Action28: TAction;
    Action29: TAction;
    Action31: TAction;
    QMDFePARAFILIAL: TStringField;
    SP_Libera: TADOStoredProc;
    QNFCFOP: TStringField;
    Action86: TAction;
    Action88: TAction;
    Action110: TAction;
    Action111: TAction;
    qrCteEletronicoFL_TIPO_2: TStringField;
    Action113: TAction;
    Action114: TAction;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    Action115: TAction;
    QMDFeCODCMO: TFMTBCDField;
    Action117: TAction;
    Action118: TAction;
    //RLPDFFilter1: TRLPDFFilter;
    QPedido: TADOQuery;
    QPedidoORDCOM: TStringField;
    qrCteEletronicoCOMPSUFRAMA: TStringField;
    qrCteEletronicoSUFRAMA_XML: TFMTBCDField;
    qrCteEletronicoVEICULO: TStringField;
    QMDFeRENAVAM_CARRETA: TStringField;
    QMDFeTARA_CARRETA: TFMTBCDField;
    QMDFeRNTRC_CARRETA: TStringField;
    QMDFeCARROCERIA: TStringField;
    QMDFeCEPDESTINO: TStringField;
    QMDFeLATITUDE_ORIGEM: TFloatField;
    QMDFeLONGITUDE_ORIGEM: TFloatField;
    QMDFeLATITUDE_DESTINO: TFloatField;
    QMDFeLONGITUDE_DESTINO: TFloatField;
    Action112: TAction;
    Action4: TAction;
    Action24: TAction;
    Action25: TAction;
    Action30: TAction;
    Action34: TAction;
    qrCteEletronicotaxa_tda: TBCDField;
    qrCteEletronicocomptda: TStringField;
    QAutXml: TADOQuery;
    QAutXmlCNPJ: TStringField;
    procedure Action5Execute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MudarComEnter(var Msg: TMsg; var Handled: Boolean);
    procedure Action6Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure Action16Execute(Sender: TObject);
    procedure Action19Execute(Sender: TObject);
    procedure Action18Execute(Sender: TObject);
    procedure Action20Execute(Sender: TObject);
    procedure Action21Execute(Sender: TObject);
    procedure Action22Execute(Sender: TObject);
    procedure Action23Execute(Sender: TObject);
    procedure Action26Execute(Sender: TObject);
    procedure Action27Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action33Execute(Sender: TObject);
    procedure Action35Execute(Sender: TObject);
    procedure Action36Execute(Sender: TObject);
    procedure Action37Execute(Sender: TObject);
    procedure Action39Execute(Sender: TObject);
    procedure Action40Execute(Sender: TObject);
    procedure Action41Execute(Sender: TObject);
    procedure Action42Execute(Sender: TObject);
    procedure Action43Execute(Sender: TObject);
    procedure Action44Execute(Sender: TObject);
    procedure Action45Execute(Sender: TObject);
    procedure Action46Execute(Sender: TObject);
    procedure Action47Execute(Sender: TObject);
    procedure Action49Execute(Sender: TObject);
    procedure Action48Execute(Sender: TObject);
    procedure Action52Execute(Sender: TObject);
    procedure Action53Execute(Sender: TObject);
    procedure Action54Execute(Sender: TObject);
    procedure Action56Execute(Sender: TObject);
    procedure Action57Execute(Sender: TObject);
    procedure Action58Execute(Sender: TObject);
    procedure Action59Execute(Sender: TObject);
    procedure Action60Execute(Sender: TObject);
    procedure Action63Execute(Sender: TObject);
    procedure Action64Execute(Sender: TObject);
    procedure Action65Execute(Sender: TObject);
    procedure Action66Execute(Sender: TObject);
    procedure Action67Execute(Sender: TObject);
    procedure Action68Execute(Sender: TObject);
    procedure Action69Execute(Sender: TObject);
    procedure Action71Execute(Sender: TObject);
    // procedure Action72Execute(Sender: TObject);
    procedure Action32Execute(Sender: TObject);
    procedure Action74Execute(Sender: TObject);
    procedure Action75Execute(Sender: TObject);
    procedure Action77Execute(Sender: TObject);
    procedure Action76Execute(Sender: TObject);
    procedure Action78Execute(Sender: TObject);
    procedure Action79Execute(Sender: TObject);
    procedure Action80Execute(Sender: TObject);
    procedure Action81Execute(Sender: TObject);
    procedure Action83Execute(Sender: TObject);
    procedure Action85Execute(Sender: TObject);
    procedure Action72Execute(Sender: TObject);
    procedure Action89Execute(Sender: TObject);
    procedure Action91Execute(Sender: TObject);
    procedure Action90Execute(Sender: TObject);
    procedure Action92Execute(Sender: TObject);
    procedure Action94Execute(Sender: TObject);
    procedure Action95Execute(Sender: TObject);
    procedure Action96Execute(Sender: TObject);
    procedure Action99Execute(Sender: TObject);
    procedure Action101Execute(Sender: TObject);
    procedure Action102Execute(Sender: TObject);
    procedure Action103Execute(Sender: TObject);
    procedure Action104Execute(Sender: TObject);
    procedure Action105Execute(Sender: TObject);
    procedure Action106Execute(Sender: TObject);
    procedure Action107Execute(Sender: TObject);
    procedure Action108Execute(Sender: TObject);
    procedure Action109Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action73Execute(Sender: TObject);
    procedure Action17Execute(Sender: TObject);
    procedure Action29Execute(Sender: TObject);
    procedure Action31Execute(Sender: TObject);
    procedure Action86Execute(Sender: TObject);
    procedure Action88Execute(Sender: TObject);
    procedure Action110Execute(Sender: TObject);
    procedure Action111Execute(Sender: TObject);
    procedure Action113Execute(Sender: TObject);
    procedure Action114Execute(Sender: TObject);
    procedure Action115Execute(Sender: TObject);
    procedure Action117Execute(Sender: TObject);
    procedure Action118Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action24Execute(Sender: TObject);
    procedure Action25Execute(Sender: TObject);
    procedure Action30Execute(Sender: TObject);
    procedure Action34Execute(Sender: TObject);
  private
    procedure InputBoxSetPasswordChar(var Msg: TMessage);
      message InputBoxMessage;

  public
  var
    sJustificativa: String;
    procedure GeraMDFe(manifesto: integer);
    procedure EncerraMDFe(manifesto: integer);
    procedure CancelamentoMDFe(manifesto: integer; motivo: string);
    procedure EmailMDFe(protocolo_mdfe: string);
    procedure GeraCtrcEletronico(iNovoCtrc: integer);
    procedure CancelaCTe(nctrc: integer);
    procedure InutilizaCTe(nctrc: integer);
    procedure averba(cte: integer);
    // procedure GeraGNRe;
  end;

var
  frmMenu: TfrmMenu;

var
  GLBUSER, glbnmfilial, dts, dts2, GlbMdfeamb, GlbCteAmb, GlbSchema: string;
  GLBFilial, GLBCodUser: integer;
  glbsmtp, glbemail, glbsenha, glbimagem, GlbConf, GlbVerao, glbPDF: String;
  sLimpaRomUF, GlbControler, GLBSeguradora, glbTrip, glbimpr: String;
  bLimpaTodos: Boolean;
  iLimpaRomaneio, iLimpaTarefa: integer;
  iLimpaMotivo: String;
  GlbMdfecus: real;

implementation

{$R *.dfm}

uses funcoes, Dados, ExportaRM, RelTransporte2, Sobre,
  Rel_expedicao_prod, Auditoria, manutencao, Exporta_WMS_Rodopar, GeraRoma,
  Portariasai, PortariaEnt, ConsPortaria, RelBate, RelPedido_NF, Kardex_Rodopar,
  RelOR_Entrada, ORxNF, IncluirFatura, CadFuncionario, AlterarSite,
  diario_bordo, ConsPallet, RelProvisao, ImpCTeXML, MonitImp_Cte, CadConversao_peso,
  CadConversao, CadTabelaCustoFrac, ConsFat_pagar, RelCusto_SemTabela, AlterarSenha,
  CadControle, GeraManifesto, MonitorMDFe, ConsManifesto, RelFatxPrazo,
  RelTransporte1, Ordem_Coleta, Rel_data_vencidas, RelCusto_frete_sim, CadPercurso,
  LimparConf, CadCicloFinan, CadTabelaControler, Rel_ocorr_fatura_pagar,
  LiberacaoFat_pagar, Rel_custoxrec, Cons_Fatura_rec, AlterarTransportes, EdiDocCob,
  CadTabelaVenda, GeraCTe, MonitorCte, CT_e_Gerados, RelCT_Gerado, GerarCompl,
  GeraCTe_dev, CadTabelaControlerVenda, Exporta_JIT_Rodopar, RelSIMxRodopar, GeraRoma_wmsclient,
  ConsultaCte, CadControle_fiscal, CadEmail,
  pcnConversao, pcteConversaoCTe, ACBrUtil, DateUtils, pmdfeConversaoMDFe,
  CadOcor_fat_pagar_2, CadCidade, PainelControle_Volumes,
  RelNFMeritor, Rel_Analise_custo, CadLimiteFinan, GerarCte_Subs,
  ExpOcorren, CadJanela, CadEmailXML, Rel_OST, ImpFatura,
  Rel_provisao, Pre_fatura, CadParametros, PainelControle_Volumes_retira,
  CadSeguradora, MonAverba, Dacte_Cte_2, Rel_Averbacao, MonitImp_Doccob,
  CadMensageiro, CadTabelaVenda_Reajuste, RelCusto_Generalidades,
  CadIsca, CadTabelaCusto_upload, CadParametro_Sim, ATM, EtiquetaVolume,
  EtiquetaVolume_Reimp, CadLinhaTempo, RelProjeto35, MonRoadNet, RelCteControladoria,
  Rel_custoxrecgen, CadOcorrencia_para, CadTabelaReceita_upload, EtiquetaCross,
  EtiquetaVolume_itajai, EtiquetaVolume_Reimp_itajai, CadCiot;

procedure TfrmMenu.Action101Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadEmailXMl') then
    Exit;
  try
    Application.CreateForm(TfrmCadEmailXMl, frmCadEmailXMl);
    frmCadEmailXMl.ShowModal;
  finally
    frmCadEmailXMl.Free;
  end;
end;

procedure TfrmMenu.Action102Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_OST, frmRel_OST);
    frmRel_OST.ShowModal;
  finally
    frmRel_OST.Free;
  end;
end;

procedure TfrmMenu.Action103Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmImpFatura') then
    Exit;
  try
    Application.CreateForm(TfrmImpFatura, frmImpFatura);
    frmImpFatura.ShowModal;
  finally
    frmImpFatura.Free;
  end;
end;

procedure TfrmMenu.Action104Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_provisao, frmRel_provisao);
    frmRel_provisao.ShowModal;
  finally
    frmRel_provisao.Free;
  end;
end;

procedure TfrmMenu.Action105Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmpre_fatura') then
    Exit;
  try
    Application.CreateForm(TfrmPre_fatura, frmPre_fatura);
    frmPre_fatura.ShowModal;
  finally
    frmPre_fatura.Free;
  end;
end;

procedure TfrmMenu.Action106Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadParametros') then
    Exit;
  try
    Application.CreateForm(TfrmCadParametros, frmCadParametros);
    frmCadParametros.ShowModal;
  finally
    frmCadParametros.Free;
  end;
end;

procedure TfrmMenu.Action107Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmPainelCon_Vol_retira, frmPainelCon_Vol_retira);
    frmPainelCon_Vol_retira.ShowModal;
  finally
    frmPainelCon_Vol_retira.Free;
  end;
end;

procedure TfrmMenu.Action108Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadSeguradora') then
    Exit;
  try
    Application.CreateForm(TfrmCadSeguradora, frmCadSeguradora);
    frmCadSeguradora.ShowModal;
  finally
    frmCadSeguradora.Free;
  end;
end;

procedure TfrmMenu.Action109Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmMonAverba, frmMonAverba);
    frmMonAverba.ShowModal;
  finally
    frmMonAverba.Free;
  end;
end;

procedure TfrmMenu.Action10Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelTransporte2, frmRelTransporte2);
    frmRelTransporte2.ShowModal;
  finally
    frmRelTransporte2.Free;
  end;
end;

procedure TfrmMenu.Action110Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEtiquetaVolume_Reimp') then
    Exit;
  try
    Application.CreateForm(TfrmEtiquetaVolume_Reimp, frmEtiquetaVolume_Reimp);
    frmEtiquetaVolume_Reimp.ShowModal;
  finally
    frmEtiquetaVolume_Reimp.Free;
  end;
end;

procedure TfrmMenu.Action111Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadLinhaTempo') then
    Exit;
  try
    Application.CreateForm(TfrmCadLinhaTempo, frmCadLinhaTempo);
    frmCadLinhaTempo.ShowModal;
  finally
    frmCadLinhaTempo.Free;
  end;
end;

procedure TfrmMenu.Action113Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadOcorrencia_para') then
    Exit;
  try
    Application.CreateForm(TfrmCadOcorrencia_para, frmCadOcorrencia_para);
    frmCadOcorrencia_para.ShowModal;
  finally
    frmCadOcorrencia_para.Free;
  end;
end;

procedure TfrmMenu.Action114Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelProjeto35, frmRelProjeto35);
    frmRelProjeto35.ShowModal;
  finally
    frmRelProjeto35.Free;
  end;
end;

procedure TfrmMenu.Action115Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('RoadNet') then
    Exit;
  try
    Application.CreateForm(TfrmMonRoadNet, frmMonRoadNet);
    frmMonRoadNet.ShowModal;
  finally
    frmMonRoadNet.Free;
  end;
end;

procedure TfrmMenu.Action117Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelCteControladoria, frmRelCteControladoria);
    frmRelCteControladoria.ShowModal;
  finally
    frmRelCteControladoria.Free;
  end;
end;

procedure TfrmMenu.Action118Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_custoxrecgen, frmRel_custoxrecgen);
    frmRel_custoxrecgen.ShowModal;
  finally
    frmRel_custoxrecgen.Free;
  end;
end;

procedure TfrmMenu.Action11Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_expedicao_prod, frmRel_expedicao_prod);
    frmRel_expedicao_prod.ShowModal;
  finally
    frmRel_expedicao_prod.Free;
  end;
end;

procedure TfrmMenu.Action13Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmAuditoria') then
    Exit;
  try
    Application.CreateForm(TfrmAuditoria, frmAuditoria);
    frmAuditoria.ShowModal;
  finally
    frmAuditoria.Free;
  end;
end;

procedure TfrmMenu.Action14Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmManutencao') then
    Exit;
  try
    Application.CreateForm(TfrmManutencao, frmManutencao);
    frmManutencao.ShowModal;
  finally
    frmManutencao.Free;
  end;
end;

procedure TfrmMenu.Action15Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmExporta_WMS_Rodopar, frmExporta_WMS_Rodopar);
    frmExporta_WMS_Rodopar.ShowModal;
  finally
    frmExporta_WMS_Rodopar.Free;
  end;
end;

procedure TfrmMenu.Action16Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGeraRoma') then
    Exit;
  try
    Application.CreateForm(TfrmGeraRoma, frmGeraRoma);
    frmGeraRoma.ShowModal;
  finally
    frmGeraRoma.Free;
  end;
end;

procedure TfrmMenu.Action17Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmRelCusto_Generalidades') then
    Exit;
  try
    Application.CreateForm(TfrmRelCusto_Generalidades,
      frmRelCusto_Generalidades);
    frmRelCusto_Generalidades.ShowModal;
  finally
    frmRelCusto_Generalidades.Free;
  end;
end;

procedure TfrmMenu.Action18Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmPortariaEnt, frmPortariaEnt);
    frmPortariaEnt.ShowModal;
  finally
    frmPortariaEnt.Free;
  end;
end;

procedure TfrmMenu.Action19Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmPortariaSai, frmPortariaSai);
    frmPortariaSai.ShowModal;
  finally
    frmPortariaSai.Free;
  end;
end;

procedure TfrmMenu.Action1Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_Averbacao, frmRel_Averbacao);
    frmRel_Averbacao.ShowModal;
  finally
    frmRel_Averbacao.Free;
  end;
end;

procedure TfrmMenu.Action20Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmConsPortaria, frmConsPortaria);
    frmConsPortaria.ShowModal;
  finally
    frmConsPortaria.Free;
  end;
end;

procedure TfrmMenu.Action21Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelBate, frmRelBate);
    frmRelBate.ShowModal;
  finally
    frmRelBate.Free;
  end;
end;

procedure TfrmMenu.Action22Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelPedido_NF, frmRelPedido_NF);
    frmRelPedido_NF.ShowModal;
  finally
    frmRelPedido_NF.Free;
  end;
end;

procedure TfrmMenu.Action23Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmKardex_Rodopar, frmKardex_Rodopar);
    frmKardex_Rodopar.ShowModal;
  finally
    frmKardex_Rodopar.Free;
  end;
end;

procedure TfrmMenu.Action24Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmEtiquetaCross, frmEtiquetaCross);
    frmEtiquetaCross.ShowModal;
  finally
    frmEtiquetaCross.Free;
  end;
end;

procedure TfrmMenu.Action25Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEtiquetaVolume_itajai') then
    Exit;
  try
    Application.CreateForm(TfrmEtiquetaVolume_itajai, frmEtiquetaVolume_itajai);
    frmEtiquetaVolume_itajai.ShowModal;
  finally
    frmEtiquetaVolume_itajai.Free;
  end;
end;

procedure TfrmMenu.Action26Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelOR_Entrada, frmRelOR_Entrada);
    frmRelOR_Entrada.ShowModal;
  finally
    frmRelOR_Entrada.Free;
  end;
end;

procedure TfrmMenu.Action27Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmORxNF, frmORxNF);
    frmORxNF.ShowModal;
  finally
    frmORxNF.Free;
  end;
end;

procedure TfrmMenu.Action29Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadIsca') then
    Exit;
  try
    Application.CreateForm(TfrmCadIsca, frmCadIsca);
    frmCadIsca.ShowModal;
  finally
    frmCadIsca.Free;
  end;
end;

procedure TfrmMenu.Action2Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadFuncionario') then
    Exit;
  try
    Application.CreateForm(TfrmCadFuncionario, frmCadFuncionario);
    frmCadFuncionario.ShowModal;
  finally
    frmCadFuncionario.Free;
  end;
end;

procedure TfrmMenu.Action30Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEtiquetaVolume_Reimp_itajai') then
    Exit;
  try
    Application.CreateForm(TfrmEtiquetaVolume_Reimp_itajai, frmEtiquetaVolume_Reimp_itajai);
    frmEtiquetaVolume_Reimp_itajai.ShowModal;
  finally
    frmEtiquetaVolume_Reimp_itajai.Free;
  end;
end;

procedure TfrmMenu.Action31Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaCusto_upload') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaCusto_upload, frmCadTabelaCusto_upload);
    frmCadTabelaCusto_upload.ShowModal;
  finally
    frmCadTabelaCusto_upload.Free;
  end;
end;

procedure TfrmMenu.Action32Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaVenda') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaVenda, frmCadTabelaVenda);
    frmCadTabelaVenda.ShowModal;
  finally
    frmCadTabelaVenda.Free;
  end;
end;

procedure TfrmMenu.Action33Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmAlterarSite, frmAlterarSite);
    frmAlterarSite.ShowModal;
  finally
    frmAlterarSite.Free;
  end;
end;

procedure TfrmMenu.Action34Execute(Sender: TObject);
begin
 { try
    Application.CreateForm(TfrmCadCiot, frmCadCiot);
    frmCadCiot.ShowModal;
  finally
    frmCadCiot.Free;
  end; }
end;

procedure TfrmMenu.Action35Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmSobre, frmSobre);
    frmSobre.ShowModal;
  finally
    frmSobre.Free;
  end;
end;

procedure TfrmMenu.Action36Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmDiario_bordo, frmDiario_bordo);
    frmDiario_bordo.ShowModal;
  finally
    frmDiario_bordo.Free;
  end;
end;

procedure TfrmMenu.Action37Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmConsPallet, frmConsPallet);
    frmConsPallet.ShowModal;
  finally
    frmConsPallet.Free;
  end;
end;

procedure TfrmMenu.Action39Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelProvisao, frmRelProvisao);
    frmRelProvisao.ShowModal;
  finally
    frmRelProvisao.Free;
  end;
end;

procedure TfrmMenu.Action3Execute(Sender: TObject);
begin
  // if not dtmDados.PodeVisualizar('frmMonitImp_Doccob') then
  // Exit;
  try
    Application.CreateForm(TfrmMonitImp_Doccob, frmMonitImp_Doccob);
    frmMonitImp_Doccob.ShowModal;
  finally
    frmMonitImp_Doccob.Free;
  end;
end;

procedure TfrmMenu.Action40Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEDICTRCXML') then
    Exit;
  try
    Application.CreateForm(TfrmEDICTRCXML, frmEDICTRCXML);
    frmEDICTRCXML.ShowModal;
  finally
    frmEDICTRCXML.Free;
  end;
end;

procedure TfrmMenu.Action41Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmMonitImp_Cte') then
    Exit;
  try
    Application.CreateForm(TfrmMonitImp_Cte, frmMonitImp_Cte);
    frmMonitImp_Cte.ShowModal;
  finally
    frmMonitImp_Cte.Free;
  end;
end;

procedure TfrmMenu.Action42Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadConversao_peso') then
    Exit;
  try
    Application.CreateForm(TfrmCadConversao_peso, frmCadConversao_peso);
    frmCadConversao_peso.ShowModal;
  finally
    frmCadConversao_peso.Free;
  end;
end;

procedure TfrmMenu.Action43Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadConversao') then
    Exit;
  try
    Application.CreateForm(TfrmCadConversao, frmCadConversao);
    frmCadConversao.ShowModal;
  finally
    frmCadConversao.Free;
  end;
end;

procedure TfrmMenu.Action44Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaCustoFrac') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaCustoFrac, frmCadTabelaCustoFrac);
    frmCadTabelaCustoFrac.ShowModal;
  finally
    frmCadTabelaCustoFrac.Free;
  end;
end;

procedure TfrmMenu.Action45Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmConsFat_pagar') then
    Exit;
  try
    Application.CreateForm(TfrmConsFat_pagar, frmConsFat_pagar);
    frmConsFat_pagar.ShowModal;
  finally
    frmConsFat_pagar.Free;
  end;
end;

procedure TfrmMenu.Action46Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelCusto_SemTabela, frmRelCusto_SemTabela);
    frmRelCusto_SemTabela.ShowModal;
  finally
    frmRelCusto_SemTabela.Free;
  end;
end;

procedure TfrmMenu.Action47Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmAlterarSenha, frmAlterarSenha);
    frmAlterarSenha.ShowModal;
  finally
    frmAlterarSenha.Free;
  end;
end;

procedure TfrmMenu.Action48Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGeraManifesto') then
    Exit;
  try
    Application.CreateForm(TfrmGeraManifesto, frmGeraManifesto);
    frmGeraManifesto.ShowModal;
  finally
    frmGeraManifesto.Free;
  end;
end;

procedure TfrmMenu.Action49Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadControle') then
    Exit;
  try
    Application.CreateForm(TfrmCadControle, frmCadControle);
    frmCadControle.ShowModal;
  finally
    frmCadControle.Free;
  end;
end;

procedure TfrmMenu.Action4Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaReceita_upload') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaReceita_upload, frmCadTabelaReceita_upload);
    frmCadTabelaReceita_upload.ShowModal;
  finally
    frmCadTabelaReceita_upload.Free;
  end;
end;

procedure TfrmMenu.Action52Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmMonitorMDFe') then
    Exit;
  try
    Application.CreateForm(TfrmMonitorMDFe, frmMonitorMDFe);
    frmMonitorMDFe.ShowModal;
  finally
    frmMonitorMDFe.Free;
  end;
end;

procedure TfrmMenu.Action53Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmConsManifesto') then
    Exit;
  try
    Application.CreateForm(TfrmConsManifesto, frmConsManifesto);
    frmConsManifesto.ShowModal;
  finally
    frmConsManifesto.Free;
  end;
end;

procedure TfrmMenu.Action54Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelFatxPrazo, frmRelFatxPrazo);
    frmRelFatxPrazo.ShowModal;
  finally
    frmRelFatxPrazo.Free;
  end;
end;

procedure TfrmMenu.Action56Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelTransporte1, frmRelTransporte1);
    frmRelTransporte1.ShowModal;
  finally
    frmRelTransporte1.Free;
  end;
end;

procedure TfrmMenu.Action57Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmOrdem_Coleta') then
    Exit;
  try
    Application.CreateForm(TfrmOrdem_Coleta, frmOrdem_Coleta);
    frmOrdem_Coleta.ShowModal;
  finally
    frmOrdem_Coleta.Free;
  end;
end;

procedure TfrmMenu.Action58Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_data_vencidas, frmRel_data_vencidas);
    frmRel_data_vencidas.ShowModal;
  finally
    frmRel_data_vencidas.Free;
  end;
end;

procedure TfrmMenu.Action59Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmRelCusto_frete_sim') then
    Exit;
  try
    Application.CreateForm(TfrmRelCusto_frete_sim, frmRelCusto_frete_sim);
    frmRelCusto_frete_sim.ShowModal;
  finally
    frmRelCusto_frete_sim.Free;
  end;
end;

procedure TfrmMenu.Action5Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmConsultaCte, frmConsultaCte);
    frmConsultaCte.ShowModal;
  finally
    frmConsultaCte.Free;
  end;
end;

procedure TfrmMenu.Action60Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadPercurso') then
    Exit;
  try
    Application.CreateForm(TfrmCadPercurso, frmCadPercurso);
    frmCadPercurso.ShowModal;
  finally
    frmCadPercurso.Free;
  end;
end;

procedure TfrmMenu.Action63Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmLimparConf') then
    Exit;
  try
    Application.CreateForm(TfrmLimparConf, frmLimparConf);
    frmLimparConf.ShowModal;
  finally
    frmLimparConf.Free;
  end;
end;

procedure TfrmMenu.Action64Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadCicloFinan') then
    Exit;
  try
    Application.CreateForm(TfrmCadCicloFinan, frmCadCicloFinan);
    frmCadCicloFinan.ShowModal;
  finally
    frmCadCicloFinan.Free;
  end;
end;

procedure TfrmMenu.Action65Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaControler') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaControler, frmCadTabelaControler);
    frmCadTabelaControler.ShowModal;
  finally
    frmCadTabelaControler.Free;
  end;
end;

procedure TfrmMenu.Action66Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_ocorr_fatura_pagar,
      frmRel_ocorr_fatura_pagar);
    frmRel_ocorr_fatura_pagar.ShowModal;
  finally
    frmRel_ocorr_fatura_pagar.Free;
  end;
end;

procedure TfrmMenu.Action67Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmLiberacaoFat_pagar') then
    Exit;
  try
    Application.CreateForm(TfrmLiberacaoFat_pagar, frmLiberacaoFat_pagar);
    frmLiberacaoFat_pagar.ShowModal;
  finally
    frmLiberacaoFat_pagar.Free;
  end;
end;

procedure TfrmMenu.Action68Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_custoxrec, frmRel_custoxrec);
    frmRel_custoxrec.ShowModal;
  finally
    frmRel_custoxrec.Free;
  end;
end;

procedure TfrmMenu.Action69Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCons_Fatura_rec, frmCons_Fatura_rec);
    frmCons_Fatura_rec.ShowModal;
  finally
    frmCons_Fatura_rec.Free;
  end;
end;

procedure TfrmMenu.Action6Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadControle_fiscal') then
    Exit;
  try
    Application.CreateForm(TfrmCadControle_fiscal, frmCadControle_fiscal);
    frmCadControle_fiscal.ShowModal;
  finally
    frmCadControle_fiscal.Free;
  end;
end;

procedure TfrmMenu.Action71Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmAlteracao_transp') then
    Exit;
  try
    Application.CreateForm(TfrmAlteracao_transp, frmAlteracao_transp);
    frmAlteracao_transp.ShowModal;
  finally
    frmAlteracao_transp.Free;
  end;
end;

procedure TfrmMenu.Action72Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEdiDocCob') then
    Exit;
  try
    Application.CreateForm(TfrmEdiDocCob, frmEdiDocCob);
    frmEdiDocCob.ShowModal;
  finally
    frmEdiDocCob.Free;
  end;
end;

procedure TfrmMenu.Action73Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaVenda_Reajuste') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaVenda_Reajuste,
      frmCadTabelaVenda_Reajuste);
    frmCadTabelaVenda_Reajuste.ShowModal;
  finally
    frmCadTabelaVenda_Reajuste.Free;
  end;
end;

procedure TfrmMenu.Action74Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGeraCTe') then
    Exit;
  try
    Application.CreateForm(TfrmGeraCTe, frmGeraCTe);
    frmGeraCTe.ShowModal;
  finally
    frmGeraCTe.Free;
  end;
end;

procedure TfrmMenu.Action75Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmMonitorCte') then
    Exit;
  try
    Application.CreateForm(TfrmMonitorCte, frmMonitorCte);
    frmMonitorCte.ShowModal;
  finally
    frmMonitorCte.Free;
  end;
end;

procedure TfrmMenu.Action76Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGerarCompl') then
    Exit;
  try
    Application.CreateForm(TfrmGerarCompl, frmGerarCompl);
    frmGerarCompl.ShowModal;
  finally
    frmGerarCompl.Free;
  end;
end;

procedure TfrmMenu.Action77Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelCT_Gerado, frmRelCT_Gerado);
    frmRelCT_Gerado.ShowModal;
  finally
    frmRelCT_Gerado.Free;
  end;
end;

procedure TfrmMenu.Action78Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGeraCTe_dev') then
    Exit;
  try
    Application.CreateForm(TfrmGeraCTe_dev, frmGeraCTe_dev);
    frmGeraCTe_dev.ShowModal;
  finally
    frmGeraCTe_dev.Free;
  end;
end;

procedure TfrmMenu.Action79Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadTabelaControlerVenda') then
    Exit;
  try
    Application.CreateForm(TfrmCadTabelaControlerVenda,
      frmCadTabelaControlerVenda);
    frmCadTabelaControlerVenda.ShowModal;
  finally
    frmCadTabelaControlerVenda.Free;
  end;
end;

procedure TfrmMenu.Action7Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadmensageiro') then
    Exit;
  try
    Application.CreateForm(TfrmCadmensageiro, frmCadmensageiro);
    frmCadmensageiro.ShowModal;
  finally
    frmCadmensageiro.Free;
  end;
end;

procedure TfrmMenu.Action80Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmExporta_JIT_Rodopar, frmExporta_JIT_Rodopar);
    frmExporta_JIT_Rodopar.ShowModal;
  finally
    frmExporta_JIT_Rodopar.Free;
  end;
end;

procedure TfrmMenu.Action81Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelSIMxRodopar, frmRelSIMxRodopar);
    frmRelSIMxRodopar.ShowModal;
  finally
    frmRelSIMxRodopar.Free;
  end;
end;

procedure TfrmMenu.Action83Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGeraRoma') then
    Exit;
  try
    Application.CreateForm(TfrmGeraRoma_wmsclient, frmGeraRoma_wmsclient);
    frmGeraRoma_wmsclient.ShowModal;
  finally
    frmGeraRoma_wmsclient.Free;
  end;
end;

procedure TfrmMenu.Action85Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadEmail') then
    Exit;
  try
    Application.CreateForm(TfrmCadEmail, frmCadEmail);
    frmCadEmail.ShowModal;
  finally
    frmCadEmail.Free;
  end;
end;

procedure TfrmMenu.Action86Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadParametro_Sim') then
    Exit;
  try
    Application.CreateForm(TfrmCadParametro_Sim, frmCadParametro_Sim);
    frmCadParametro_Sim.ShowModal;
  finally
    frmCadParametro_Sim.Free;
  end;
end;

procedure TfrmMenu.Action88Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmEtiquetaVolume') then
    Exit;
  try
    Application.CreateForm(TfrmEtiquetaVolume, frmEtiquetaVolume);
    frmEtiquetaVolume.ShowModal;
  finally
    frmEtiquetaVolume.Free;
  end;
end;

procedure TfrmMenu.Action89Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadOcor_fat_pagar_2') then
    Exit;
  try
    Application.CreateForm(TfrmCadOcor_fat_pagar_2, frmCadOcor_fat_pagar_2);
    frmCadOcor_fat_pagar_2.ShowModal;
  finally
    frmCadOcor_fat_pagar_2.Free;
  end;
end;

procedure TfrmMenu.Action8Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmIncluirFatura') then
    Exit;
  try
    Application.CreateForm(TfrmIncluirFatura, frmIncluirFatura);
    frmIncluirFatura.ShowModal;
  finally
    frmIncluirFatura.Free;
  end;
end;

procedure TfrmMenu.Action90Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmPainelControleVol') then
    Exit;
  try
    Application.CreateForm(TfrmPainelControleVol, frmPainelControleVol);
    frmPainelControleVol.ShowModal;
  finally
    frmPainelControleVol.Free;
  end;
end;

procedure TfrmMenu.Action91Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadCidade') then
    Exit;
  try
    Application.CreateForm(TfrmCadCidade, frmCadCidade);
    frmCadCidade.ShowModal;
  finally
    frmCadCidade.Free;
  end;
end;

procedure TfrmMenu.Action92Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRelNFMeritor, frmRelNFMeritor);
    frmRelNFMeritor.ShowModal;
  finally
    frmRelNFMeritor.Free;
  end;
end;

procedure TfrmMenu.Action94Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmRel_Analise_custo, frmRel_Analise_custo);
    frmRel_Analise_custo.ShowModal;
  finally
    frmRel_Analise_custo.Free;
  end;
end;

procedure TfrmMenu.Action95Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmCadLimiteFinan') then
    Exit;
  try
    Application.CreateForm(TfrmCadLimiteFinan, frmCadLimiteFinan);
    frmCadLimiteFinan.ShowModal;
  finally
    frmCadLimiteFinan.Free;
  end;
end;

procedure TfrmMenu.Action96Execute(Sender: TObject);
begin
  if not dtmDados.PodeVisualizar('frmGerarCte_Subs') then
    Exit;
  try
    Application.CreateForm(TfrmGerarCte_Subs, frmGerarCte_Subs);
    frmGerarCte_Subs.ShowModal;
  finally
    frmGerarCte_Subs.Free;
  end;
end;

procedure TfrmMenu.Action99Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmExpOcorren, frmExpOcorren);
    frmExpOcorren.ShowModal;
  finally
    frmExpOcorren.Free;
  end;
end;

procedure TfrmMenu.Action9Execute(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmExportaRM, frmExportaRM);
    frmExportaRM.ShowModal;
  finally
    frmExportaRM.Free;
  end;
end;

procedure TfrmMenu.EmailMDFe(protocolo_mdfe: string);
var
  arquivo, email: String;
  cc, mensagem: TStrings;
begin
  cc := TStringList.Create;
  mensagem := TStringList.Create;

  QMDFe.Close;
  QMDFe.Parameters[0].value := copy(protocolo_mdfe, 26, 9);
  QMDFe.Open;

  if GlbMdfeamb = 'P' then
  begin
    if QMDFeE_MAIL.AsString <> '' then
      email := QMDFeE_MAIL.AsString;
  end;
  //else
  //  cc.add('paulo.cortez@iwlogistica.com.br');

  mensagem.add('A Intecom Logistica emitiu o MDF-e Anexo');

  arquivo := ACBrMDFe1.DAMDFe.PathPDF + protocolo_mdfe + '-mdfe.xml';
  ACBrMDFe1.Manifestos.Clear;
  ACBrMDFe1.Manifestos.LoadFromFile(arquivo);
  frmMenu.ACBrMDFe1.Manifestos.Items[0].EnviarEmail(email // Para
    , 'MDF-e' // edtEmailAssunto.Text
    , mensagem // mmEmailMsg.Lines
    , True // Enviar PDF junto
    , cc // nil //Lista com emails que ser�o enviado c�pias - TStrings
    , nil // Lista de anexos - TStrings
    );

  dtmDados.Query1.Close;
  cc.Free;
  mensagem.Free;
  ShowMessage('E-mail enviado com sucesso !!');
end;

procedure TfrmMenu.EncerraMDFe(manifesto: integer);
var
  prot: String;
begin
  QMDFe.Close;
  QMDFe.Parameters[0].value := manifesto;
  QMDFe.Parameters[1].value := GLBFilial;
  QMDFe.Open;
  QMDFe.last;

  // TpcnTipoAmbiente = (taProducao, taHomologacao);
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;

  ACBrMDFe1.Manifestos.Clear;
  ACBrMDFe1.EventoMDFe.Evento.Clear;
  ACBrMDFe1.Configuracoes.WebServices.UF := QMDFeUFEMIT.AsString;
  with ACBrMDFe1.EventoMDFe.Evento.New do
  begin
    infEvento.chMDFe := QMDFeMDFE_CHAVE.AsString;
    infEvento.CNPJCPF := QMDFeCNPJ_EMIT.Text;
    // hor�rio de ver�o
    if GlbVerao = 'S' then
      infEvento.dhEvento := IncHour(Now(), -1)
    else
      infEvento.dhEvento := Now;
    // showmessage(DateTimeToStr(infEvento.dhEvento));
    infEvento.tpEvento := teEncerramento;
    infEvento.nSeqEvento := 1;

    infEvento.detEvento.nProt := QMDFeMDFE_PROTOCOLO.AsString;

    infEvento.detEvento.dtEnc := Date;
    infEvento.detEvento.cUF := StrToInt(copy(QMDFeCODIGOIBGE.AsString, 1, 2));
    infEvento.detEvento.cMun := QMDFeCODIGOIBGE.AsInteger;
  end;
  try
    ACBrMDFe1.EnviarEvento(1); // 1 = Numero do Lote
    prot := ACBrMDFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.nProt;

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('update tb_manifesto set MDFE_ENCERRAMENTO = :0, ');
    dtmDados.IQuery1.SQL.add('data_chegada = sysdate, user_encerrado = :1 ');
    dtmDados.IQuery1.SQL.add
      ('where manifesto = :2 and serie = ''1'' and filial = :3');
    dtmDados.IQuery1.Parameters[0].value := prot;
    dtmDados.IQuery1.Parameters[1].value := GLBUSER;
    dtmDados.IQuery1.Parameters[2].value := QMDFeMANIFESTO.AsInteger;
    dtmDados.IQuery1.Parameters[3].value := GLBFilial;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;
    ShowMessage('Encerramento Efetuado');

    // Libera volumes se foi para filial
    if (QMDFePARAFILIAL.value = 'S') then
    begin
      SP_Libera.Parameters[0].value := QMDFeMANIFESTO.AsInteger;
      SP_Libera.Parameters[1].value := 1;
      SP_Libera.Parameters[2].value := GLBFilial;
      SP_Libera.ExecProc;
      ShowMessage('Documentos Liberados para Filial');
    end;

  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
end;

procedure TfrmMenu.CancelamentoMDFe(manifesto: integer; motivo: string);
var
  prot, SData, sCaminho, sMDFe: String;
begin
  QMDFe.Close;
  QMDFe.Parameters[0].value := manifesto;
  QMDFe.Parameters[1].value := GLBFilial;
  QMDFe.Open;

  // TpcnTipoAmbiente = (taProducao, taHomologacao);
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;

  SData := FormatDateTime('YYYY', QMDFeEMISSAO.AsDateTime) +
    FormatDateTime('MM', QMDFeEMISSAO.AsDateTime);
  sCaminho := frmMenu.ACBrMDFe1.DAMDFe.PathPDF + '\' + SData + '\Mdfe\';
  sMDFe := QMDFeMDFE_CHAVE.AsString + '-mdfe.xml';

  // arquivo := frmMenu.ACBrMDFe1.DAMDFe.PathPDF + QManifestoMDFE_CHAVE.AsString + '-mdfe.xml';
  frmMenu.ACBrMDFe1.Manifestos.Clear;
  frmMenu.ACBrMDFe1.Manifestos.LoadFromFile(sCaminho + sMDFe);

  with ACBrMDFe1.EventoMDFe.Evento.New do
  begin
    infEvento.chMDFe := QMDFeMDFE_CHAVE.AsString;
    infEvento.CNPJCPF := QMDFeCNPJ_EMIT.Text;
    // hor�rio de ver�o
    if GlbVerao = 'S' then
      infEvento.dhEvento := IncHour(Now(), -1)
    else
      infEvento.dhEvento := Now;
    infEvento.tpEvento := teCancelamento;
    infEvento.nSeqEvento := 1;
    infEvento.detEvento.nProt := QMDFeMDFE_PROTOCOLO.AsString;
    infEvento.detEvento.xJust := trim(motivo);
  end;
  try
    ACBrMDFe1.Configuracoes.WebServices.Visualizar := True;
    ACBrMDFe1.EnviarEvento(1); // 1 = Numero do Lote
    prot := ACBrMDFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.nProt;

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('update tb_manifesto set MDFE_CANC = :0, ');
    dtmDados.IQuery1.SQL.add('MDFE_MOT_CANC = :1, MDFE_CAN_DATA = sysdate, ');
    dtmDados.IQuery1.SQL.add
      ('USER_CANCELADO = :2, STATUS = ''C'', MDFE_STATUS = :3, ');
    dtmDados.IQuery1.SQL.add('MDFE_MOTIVO = :4 ');
    dtmDados.IQuery1.SQL.add('where mdfe_chave = :5 ');
    dtmDados.IQuery1.Parameters[0].value := prot;
    dtmDados.IQuery1.Parameters[1].value := motivo;
    dtmDados.IQuery1.Parameters[2].value := GLBUSER;
    dtmDados.IQuery1.Parameters[3].value :=
      ACBrMDFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.cStat;
    dtmDados.IQuery1.Parameters[4].value :=
      ACBrMDFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.xMotivo;
    dtmDados.IQuery1.Parameters[5].value := QMDFeMDFE_CHAVE.AsString;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;
    ShowMessage('Cancelamento Efetuado');
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  ACBrMDFe1.Configuracoes.WebServices.Visualizar := false;
end;

procedure TfrmMenu.FormActivate(Sender: TObject);
begin
  StatusBar1.Panels[1].Text := dts;
  StatusBar1.Panels[4].Text := 'Vers�o : ' + infosist;
  if glbimagem = 'SIM' then
    Image1.Visible := True
  else
    Image1.Visible := false;
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
begin
  Application.OnMessage := MudarComEnter;
  FormatSettings.ShortDateFormat := 'dd/mm/yy';
  glbsmtp := 'mail.ita.locamail.com.br';
  glbemail := 'iw@iwlogistica.com.br';
  glbsenha := 'iwlog@2014';
  ACBrCTe1.DACTE.MargemInferior := 5;
  ACBrCTe1.DACTE.MargemSuperior := 5;
  ACBrCTe1.DACTE.MargemEsquerda := 5;
  ACBrCTe1.DACTE.MargemDireita  := 5;
  ACBrMDFe1.DAMDFE.MargemInferior := 5;
  ACBrMDFe1.DAMDFE.MargemSuperior := 5;
  ACBrMDFe1.DAMDFE.MargemEsquerda := 5;
  ACBrMDFe1.DAMDFE.MargemDireita  := 5;
end;

procedure TfrmMenu.GeraMDFe(manifesto: integer);
var
  cc, mensagem: TStringList;
  tcte: integer;
  tpes, tnf: real;
  contingencia: String;
begin
  ACBrMDFe1.Manifestos.Clear;
  cc := TStringList.Create;
  mensagem := TStringList.Create;

  QMDFe.Close;
  QMDFe.Parameters[0].value := manifesto;
  QMDFe.Parameters[1].value := GLBFilial;
  QMDFe.Open;

  // verifica ambiente do manifesto
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('select cont_mdfe from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;
  contingencia := dtmDados.IQuery1.FieldByName('cont_mdfe').AsString;
  dtmDados.IQuery1.Close;

  // TpcnTipoAmbiente = (taProducao, taHomologacao);
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;

  ACBrMDFe1.Configuracoes.WebServices.UF := QMDFeUFEMIT.AsString;
  ACBrMDFe1.Configuracoes.WebServices.Visualizar := false;

  with ACBrMDFe1.Manifestos.add.MDFe do
  begin
    // Dados de Identifica��o do MDF-e
    Ide.cUF := QMDFeCODUF.AsInteger;

    // TpcnTipoAmbiente = (taProducao, taHomologacao);
    if GlbMdfeamb = 'P' then
      Ide.tpAmb := taProducao
    else
      Ide.tpAmb := taHomologacao;

    // TMDFeTpEmitente = ( teTransportadora, teTranspCargaPropria );
    Ide.tpEmit := teTransportadora;
    Ide.modelo := QMDFeMODELOMDFE.AsString;
    Ide.serie := QMDFeSERIE.AsInteger;
    Ide.nMDF := manifesto;
    Ide.cMDF := QMDFeCMDF.AsInteger; // C�digo Aleat�rio

    // TMDFeModal = ( moRodoviario, moAereo, moAquaviario, moFerroviario );
    Ide.modal := moRodoviario;
    // hor�rio de ver�o
    if GlbVerao = 'S' then
      Ide.dhEmi := IncHour(Now(), -1)
    else
      Ide.dhEmi := Now;
    // TpcnTipoEmissao = (teNormal, teContingencia, teSCAN, teDPEC, teFSDA);
    //Ide.tpEmis := teNormal;

    if contingencia = 'S' then
    begin
      // Ide.tpEmis := teSVCRS;
      // Ide.tpEmis := teFSDA;
      if frmMenu.ACBrCTe1.Configuracoes.WebServices.UFCodigo
        in [14, 16, 26, 35, 50, 51] then
      begin
        ACBrCTe1.Configuracoes.Geral.FormaEmissao := teSVCRS;
        Ide.tpEmis := teContingencia;
      end
      else
      begin
        ACBrCTe1.Configuracoes.Geral.FormaEmissao := teSVCSP;
        Ide.tpEmis := teContingencia;
      end;
    end
    else
    begin
      Ide.tpEmis := teNormal;
      ACBrCTe1.Configuracoes.Geral.FormaEmissao := teNormal;
    end;


    // TpcnProcessoEmissao = (peAplicativoContribuinte, peAvulsaFisco, peAvulsaContribuinte, peContribuinteAplicativoFisco);
    Ide.procEmi := peAplicativoContribuinte;
    Ide.verProc := '3.00'; // QMdfeVERPROC.AsString;
    Ide.UFIni := QMDFeUFINI.AsString;
    Ide.UFFim := QMDFeUFFIM.AsString;
    //showmessage(copy(ApCarac(QMDFeCNPJ_PROP.AsString), 1, 8));
    if copy(ApCarac(QMDFeCNPJ_PROP.AsString), 1, 8) <> '03857930' then
      Ide.tpTransp := ttETC;

    // ======== Dados do Percurso ===========================================
    Qpercurso.Close;
    Qpercurso.Parameters[0].value := QMDFeUFINI.AsString;
    Qpercurso.Parameters[1].value := QMDFeUFFIM.AsString;
    Qpercurso.Open;
    if not Qpercurso.Eof then
    begin
      while not Qpercurso.Eof do
      begin
        with Ide.infPercurso.New do
        begin
          UFPer := QpercursoUF.AsString;
        end;
        Qpercurso.Next;
      end;
      Qpercurso.Close;
    end;

    // ======== Dados do Carregamento ===========================================
    with Ide.infMunCarrega.New do
    begin
      cMunCarrega := QMDFeCODCAR.AsInteger;
      xMunCarrega := QMDFeCIDCAR.AsString;
    end;

    // ======== Dados do Emitente ===========================================
    Emit.CNPJCPF := ApCarac(QMDFeCNPJ_EMIT.AsString);
    Emit.IE := ApCarac(QMDFeIE_EMIT.AsString);
    Emit.xNome := QMDFeNM_RAZAO.AsString;
    Emit.xFant := QMDFeFANTASIA.AsString;

    Emit.EnderEmit.xLgr := QMDFeDS_ENDERECO.AsString;
    Emit.EnderEmit.nro := QMDFeDS_NUMERO.AsString;
    Emit.EnderEmit.xCpl := '';
    Emit.EnderEmit.xBairro := QMDFeDS_BAIRRO.AsString;
    Emit.EnderEmit.cMun := QMDFeCODMUNEMIT.AsInteger;
    Emit.EnderEmit.xMun := QMDFeMUNEMIT.AsString;
    Emit.EnderEmit.CEP := StrToIntDef(QMDFeDS_CEP.AsString, 0);
    Emit.EnderEmit.UF := QMDFeUFEMIT.AsString;
    Emit.EnderEmit.fone := QMDFeNR_TELEFONE.AsString;
    Emit.EnderEmit.email := QMDFeE_MAIL.AsString;

    // ======== Dados dos Ve�culos ===========================================
    if (QMDFeCIOT.AsString <> '0') then
    begin

      Rodo.CIOT := ApCarac(QMDFeCIOT.AsString);
      with Rodo.infANTT.infCIOT.New do
      begin
        CIOT := ApCarac(QMDFeCIOT.AsString);
        CNPJCPF := ApCarac(QMDFeCNPJ_EMIT.AsString);
      end;

    end;
    // rodo.CIOT := ApCarac(QMDFeCIOT.AsString);

    // rodo.infANTT.infCIOT.Add.CNPJCPF := ApCarac(QMDFeCNPJ_PROP.AsString); //'65697260000103';
    if copy(ApCarac(QMDFeCNPJ_PROP.AsString), 1, 8) = '03857930' then
    begin
      Rodo.infANTT.RNTRC := QMDFeNR_RNTC.AsString;
    end
    else
    begin
      // propriet�rio do ve�culo
      Rodo.veicTracao.prop.RNTRC := QMDFeRNTRC_PROP.AsString;
      // rodo.veicTracao.prop.CNPJCPF := ApCarac(QMDFeCNPJ_MOT.AsString);
      Rodo.veicTracao.prop.CNPJCPF := ApCarac(QMDFeCNPJ_PROP.AsString);
      Rodo.veicTracao.prop.xNome := QMDFeRAZAO_PROP.AsString;
      Rodo.veicTracao.prop.IE := ApCarac(QMDFeIE_PROP.AsString);
      Rodo.veicTracao.prop.UF := QMDFeUF_PROP.AsString;
    end;
    Rodo.infANTT.infContratante.New.CNPJCPF := ApCarac(QMDFeCNPJ_EMIT.AsString);

    // dados do ve�culo com tra��o
    Rodo.veicTracao.cInt := QMDFeCODVEI.AsString;
    Rodo.veicTracao.placa := ApCarac(QMDFePLACA.AsString);
    Rodo.veicTracao.tara := 0;
    Rodo.veicTracao.capKG := QMDFeCAPACT.AsInteger;
    Rodo.veicTracao.capM3 := QMDFeCAPACMC.AsInteger;

    // TpcteProp = (tpTACAgregado, tpTACIndependente, tpOutros);
    if QMDFeFISJUR.AsString = 'F' then
      Rodo.veicTracao.prop.tpProp := tpTACAgregado
    else
      Rodo.veicTracao.prop.tpProp := tpTACIndependente;
    // (trNaoAplicavel, trTruck, trToco, trCavaloMecanico, trVAN, trUtilitario, trOutros);
    if UpperCase(QMDFeFL_TIPO.AsString) = 'FIORINO' then
      Rodo.veicTracao.tpRod := trUtilitario
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'VAN' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'FURG�O' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'TRAFFIC' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'HR' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'IVECO' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'MASTER' then
      Rodo.veicTracao.tpRod := trVan
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'TOCO' then
      Rodo.veicTracao.tpRod := trToco
    else if UpperCase(QMDFeFL_TIPO.AsString) = 'TRUCK' then
      Rodo.veicTracao.tpRod := trTruck
    else
      Rodo.veicTracao.tpRod := trOutros;
    // (tcNaoAplicavel, tcAberta, tcFechada, tcGraneleira, tcPortaContainer, tcSider);
    Rodo.veicTracao.tpCar := tcNaoAplicavel;

    // ======== Dados dos Condutores ===========================================
    with Rodo.veicTracao.condutor.New do
    begin
      xNome := QMDFeMOTORISTA.AsString;
      CPF := ApCarac(QMDFeCNPJ_MOT.AsString);
    end;
    Rodo.veicTracao.UF := 'SP';


   {--------------------------------------------------------------------------------------------------------------}
    {veicReboque - inicio                                                                                          }
    {--------------------------------------------------------------------------------------------------------------}
    if QMDFeCARRETA.AsString <> '' then
    begin
     With RODO.veicReboque.New do
     begin
       cInt    := QMDFeCODCARRETA.AsString;
       placa   := ApCarac(QMDFeCARRETA.AsString);
       RENAVAM := QMDFeRENAVAM_CARRETA.AsString;
       While Length(RENAVAM) < 9 do RENAVAM := '0'+RENAVAM;
       tara  := QMDFeTARA_CARRETA.AsInteger;
       capKG := QMdfecarrcap.AsInteger;
       capM3 := QMdfecarrmc.AsInteger;

       {--------------------------------------------------------------------------------------------------------------}
       {prop - inicio                                                                                                 }
       {--------------------------------------------------------------------------------------------------------------}
        With prop do
        begin
          CNPJCPF := QMdfecnpj_propca.AsString;
          RNTRC   := QMDFeRNTRC_CARRETA.AsString;
          xNome   := Trim(Copy(QMdferazao_propca.AsString+space(60),1,60));
          IE      := QMDFeIE_PROPCA.AsString;
          UF      := QMdfeuf_propca.AsString;
          if QMDFefisjurca.AsString = 'F' then
            Rodo.veicTracao.prop.tpProp := tpTACAgregado
          else
            Rodo.veicTracao.prop.tpProp := tpTACIndependente;
        end;
       {--------------------------------------------------------------------------------------------------------------}
       {prop - fim                                                                                                    }
       {--------------------------------------------------------------------------------------------------------------}

       {--------------------------------------------------------------------------------------------------------------}
       {tpCar/UF - inicio                                                                                             }
       {--------------------------------------------------------------------------------------------------------------}
       if QMDFeCARROCERIA.AsString = '00' then
         tpCar := tcNaoAplicavel
       else if QMDFeCARROCERIA.AsString = '01' then
         tpCar := tcAberta
       else if QMDFeCARROCERIA.AsString = '02' then
         tpCar := tcFechada
       else if QMDFeCARROCERIA.AsString = '03' then
         tpCar := tcGraneleira
       else if QMDFeCARROCERIA.AsString = '04' then
         tpCar := tcPortaContainer
       else if QMDFeCARROCERIA.AsString = '05' then
         tpCar := tcSider;
       UF          := QMdfeuf_carr.AsString;
       //codAgPorto  := '';
       {--------------------------------------------------------------------------------------------------------------}
       {tpCar/UF - fim                                                                                                }
       {--------------------------------------------------------------------------------------------------------------}
     end;
    end;
    {--------------------------------------------------------------------------------------------------------------}
    {veicReboque - fim                                                                                             }
    {--------------------------------------------------------------------------------------------------------------}
    with prodPred do
    begin
      tpCarga := tcCargaGeral;
      xProd   := 'DIVERSOS';
      cEAN    := '';
      NCM     := '';
      // cep carrega
      infLocalCarrega.CEP :=  StrToIntDef(QMDFeDS_CEP.AsString, 0);
      infLocalCarrega.latitude  := StringToFloatDef(QMDFeLATITUDE_ORIGEM.AsString,0);
      infLocalCarrega.longitude := StringToFloatDef(QMDFeLONGITUDE_DESTINO.AsString,0);
      infLocalDescarrega.CEP :=  StrToIntDef(QMDFeCEPDESTINO.AsString, 0);
      infLocalDescarrega.latitude  := StringToFloatDef(QMDFeLATITUDE_DESTINO.AsString,0);
      infLocalDescarrega.longitude := StringToFloatDef(QMDFeLONGITUDE_DESTINO.AsString,0);
    end;

    // ======== Dados dos Descarregamentos ======================================
    tcte := 0;
    tnf := 0;
    tpes := 0;

    with infDoc.infMunDescarga.New do
    begin
      cMunDescarga := QMDFeCODIGOIBGE.AsInteger;
      xMunDescarga := QMDFeCIDADE.AsString;
      while not QMDFe.Eof do
      begin
        tcte := tcte + 1;
        // tvol := tvol + 1;
        tpes := tpes + QMDFePESO.AsFloat;
        tnf := tnf + QMDFeVL_NF.AsFloat;
        with infCTe.New do
        begin
          chCTe := QMDFeCHAVECTE.AsString;
        end;
        QMDFe.Next;
      end;
    end;

    // ========= Dados da seguradora da carga ===================================
    QCadSeguradora.Close;
    QCadSeguradora.Parameters[0].value := QMDFeFILIAL.AsInteger;
    QCadSeguradora.Open;

    With seg.New do
    begin
      CNPJ := QCadSeguradoraCNPJ.AsString;
      xSeg := QCadSeguradoraSEGURADORA.AsString;
      nApol := QCadSeguradoraAPOLICE.AsString;
      if (QMDFeaverba_protocolo.AsString = '') then
        aver.New.nAver := '99999'
      else
        aver.New.nAver := QMDFeaverba_protocolo.AsString;
    end;

    // Totais
    tot.qCTe := tcte;
    tot.vCarga := tnf;

    // UnidMed = (uM3,uKG, uTON, uUNIDADE, uLITROS);
    tot.cUnid := uKg;
    tot.qCarga := tpes;
  end;
  //ACBrMDFe1.Manifestos.GravarXML('c:\sim\teste.xml');
  ACBrMDFe1.Manifestos.Assinar;
  //ACBrMDFe1.Manifestos.GravarXML('c:\sim\teste.xml');
  ACBrMDFe1.Manifestos.Validar;
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('update tb_manifesto set ');
  dtmDados.IQuery1.SQL.add('mdfe_chave = :0, mdfe_motivo = :1 ');
  if contingencia = 'S' then
    dtmDados.IQuery1.SQL.add(', FL_CONTINGENCIA = ''S'' ');
  dtmDados.IQuery1.SQL.add('where manifesto = :2 ');
  dtmDados.IQuery1.SQL.add('and filial = :3 and serie = ''1'' ');
  dtmDados.IQuery1.Parameters[0].value :=
    copy(ACBrMDFe1.Manifestos.Items[0].MDFe.infMDFe.ID, 5, 44);
  dtmDados.IQuery1.Parameters[1].value := ACBrMDFe1.Manifestos.Items[0]
    .MDFe.procMDFe.xMotivo;
  dtmDados.IQuery1.Parameters[2].value := QMDFeMANIFESTO.AsInteger;
  dtmDados.IQuery1.Parameters[3].value := GLBFilial;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.Close;
  // ACBrMDFe1.Configuracoes.WebServices.Visualizar := true;
  // ACBrMDFe1.Manifestos.Items[0].GravarXML;
  if contingencia = 'S' then
  begin
    ACBrMDFe1.Manifestos.Items[0].GravarXML;
    ACBrMDFe1.Manifestos.ImprimirPDF;
  end
  else
  begin
    try
      ACBrMDFe1.Enviar(0);
    except
      on E: Exception do
      begin
        dtmDados.IQuery1.Close;
        dtmDados.IQuery1.SQL.Clear;
        dtmDados.IQuery1.SQL.add('update tb_manifesto set ');
        // mdfe_chave = :0, ');
        dtmDados.IQuery1.SQL.add('mdfe_mensagem = :1, mdfe_status = :2 ');
        dtmDados.IQuery1.SQL.add('where manifesto = :3 ');
        dtmDados.IQuery1.SQL.add('and filial = :4 and serie = ''1'' ');
        dtmDados.IQuery1.SQL.add('and nvl(mdfe_status,0) <> ''100'' ');
        // dtmdados.IQuery1.Parameters[0].value := Copy(ACBrMDFe1.Manifestos.Items[0].MDFe.infMDFe.ID, 5, 44);
        dtmDados.IQuery1.Parameters[0].value := E.Message;
        dtmDados.IQuery1.Parameters[1].value :=
          ACBrMDFe1.WebServices.Retorno.cStat;
        dtmDados.IQuery1.Parameters[2].value := QMDFeMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[3].value := GLBFilial;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.Close;
      end;
    end;

    if ACBrMDFe1.WebServices.Retorno.cStat = 100 then
    begin
      // ACBrMDFe1.Manifestos.Imprimir;
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add('update tb_manifesto set mdfe_status = :0, ');
      dtmDados.IQuery1.SQL.add('mdfe_motivo = :1, mdfe_mensagem = :2, ');
      dtmDados.IQuery1.SQL.add('mdfe_recibo = :3, mdfe_protocolo = :4, ');
      dtmDados.IQuery1.SQL.add
        ('mdfe_chave = :5, mdfe_data = :6, status = ''S'', fl_contingencia = null ');
      dtmDados.IQuery1.SQL.add('where manifesto = :7 ');
      dtmDados.IQuery1.SQL.add('and filial = :8 and serie = ''1'' ');
      dtmDados.IQuery1.Parameters[0].value := ACBrMDFe1.WebServices.Retorno.cStat;
      dtmDados.IQuery1.Parameters[1].value :=
        ACBrMDFe1.WebServices.Retorno.xMotivo;
      dtmDados.IQuery1.Parameters[2].value := ACBrMDFe1.WebServices.Retorno.Msg;
      dtmDados.IQuery1.Parameters[3].value :=
        ACBrMDFe1.WebServices.Retorno.Recibo;
      dtmDados.IQuery1.Parameters[4].value :=
        ACBrMDFe1.WebServices.Retorno.Protocolo;
      dtmDados.IQuery1.Parameters[5].value :=
        copy(ACBrMDFe1.Manifestos.Items[0].MDFe.infMDFe.ID, 5, 44);
      // ACBrMDFe1.WebServices.Retorno.ChaveMDFe;
      dtmDados.IQuery1.Parameters[6].value := Now;
      dtmDados.IQuery1.Parameters[7].value := QMDFeMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[8].value := GLBFilial;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
      ACBrMDFe1.Manifestos.Items[0].GravarXML;
      ACBrMDFe1.Manifestos.ImprimirPDF;

    end;
  end;
  cc.Free;
  mensagem.Free;
  ACBrMDFe1.Configuracoes.WebServices.Visualizar := false;
  QMDFe.Close;
end;

procedure TfrmMenu.MudarComEnter(var Msg: TMsg; var Handled: Boolean);
begin
  if not((Screen.ActiveControl is TCustomMemo) or
    (Screen.ActiveControl is TCustomGrid) or
    (Screen.ActiveForm.ClassName = 'TMessageForm')) then
  begin
    If Msg.Message = WM_KEYDOWN then
    begin
      Case Msg.wParam of
        VK_RETURN, VK_DOWN:
          Screen.ActiveForm.Perform(WM_NextDlgCtl, 0, 0);
        VK_UP:
          Screen.ActiveForm.Perform(WM_NextDlgCtl, 1, 0);
      end;
    end;
  end;
end;

procedure TfrmMenu.InputBoxSetPasswordChar(var Msg: TMessage);
var
  hInputForm, hEdit: HWND;
begin
  hInputForm := Screen.Forms[0].Handle;
  if (hInputForm <> 0) then
  begin
    hEdit := FindWindowEx(hInputForm, 0, 'TEdit', nil);
    SendMessage(hEdit, EM_SETPASSWORDCHAR, Ord('*'), 0);
  end;
end;

procedure TfrmMenu.InutilizaCTe(nctrc: integer);
var
  sProtocolo, sMotivo, sStat, sDatahora: String;
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('select distinct m.estado UF from cyber.rodfil f left join cyber.rodmun m on f.codmun = m.codmun ');
  dtmDados.IQuery1.SQL.add('where f.codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;
  ACBrCTe1.Configuracoes.WebServices.UF := dtmDados.IQuery1.FieldByName
    ('UF').AsString;

  // verifica ambiente do manifesto
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('select ambiente_cte, certificado, logo, pdf_cte, ');
  dtmDados.IQuery1.SQL.add
    ('schemma, proxy, porta, user_proxy, pass_proxy, cont_cte, ');
  dtmDados.IQuery1.SQL.add('presumido ');
  dtmDados.IQuery1.SQL.add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;

  frmMenu.ACBrCTe1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmDados.IQuery1.FieldByName('certificado').AsString);

  // est� carregando no Login
  // frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCte :=
  // IncludeTrailingPathDelimiter(ALLTRIM(dtmDados.IQuery1.FieldByName('pdf_cte')
  // .AsString));
  // frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar :=
  // IncludeTrailingPathDelimiter(ALLTRIM(dtmDados.IQuery1.FieldByName('pdf_cte')
  // .AsString));

  // frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSchemas :=
  // ALLTRIM(dtmDados.IQuery1.FieldByName('schemma').AsString);
  frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyHost :=
    ALLTRIM(dtmDados.IQuery1.FieldByName('proxy').AsString);
  frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyPort :=
    ALLTRIM(dtmDados.IQuery1.FieldByName('porta').AsString);
  frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyUser :=
    ALLTRIM(dtmDados.IQuery1.FieldByName('user_proxy').AsString);
  frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyPass :=
    ALLTRIM(dtmDados.IQuery1.FieldByName('pass_proxy').AsString);
  dtmDados.IQuery1.Close;

  qrCteEletronico.Close;
  qrCteEletronico.Parameters[0].value := nctrc;
  qrCteEletronico.Open;

  ACBrCTe1.WebServices.Inutiliza(qrCteEletronicoCNPJEMI.AsString,
    sJustificativa, YearOf(qrCteEletronicoEMISSAOCTE.AsDateTime),
    StrToInt(qrCteEletronicoMODELOCTE.AsString),
    StrToInt(qrCteEletronicoNR_SERIE.AsString),
    qrCteEletronicoNUMEROCTE.AsInteger, qrCteEletronicoNUMEROCTE.AsInteger);
  frmCT_e_Gerados.mCte.Visible := True;
  frmCT_e_Gerados.mCte.Lines.Text :=
    UTF8Encode(ACBrCTe1.WebServices.Inutilizacao.RetWS);
  frmCT_e_Gerados.mCte.Lines.add
    ('-------------------------------------------------------------------------------------------');
  frmCT_e_Gerados.mCte.Lines.add(' =>  Inutiliza��o de Numera��o');
  frmCT_e_Gerados.mCte.Lines.add
    ('-------------------------------------------------------------------------------------------');
  frmCT_e_Gerados.mCte.Lines.add('  Ano          : ' +
    DatetoStr(YearOf(qrCteEletronicoEMISSAOCTE.AsDateTime)));
  frmCT_e_Gerados.mCte.Lines.add('  Modelo       : ' +
    qrCteEletronicoMODELOCTE.AsString);
  frmCT_e_Gerados.mCte.Lines.add('  CT-e         : ' +
    qrCteEletronicoNUMEROCTE.AsString);
  frmCT_e_Gerados.mCte.Lines.add('  S�rie        : ' +
    qrCteEletronicoNR_SERIE.AsString);
  frmCT_e_Gerados.mCte.Lines.add('  Justificativa: ' + sJustificativa);

  sProtocolo := ACBrCTe1.WebServices.Inutilizacao.Protocolo;
  sMotivo := ACBrCTe1.WebServices.Inutilizacao.xMotivo;
  sStat := IntToStr(ACBrCTe1.WebServices.Inutilizacao.cStat);
  sDatahora := DateTimeToStr(ACBrCTe1.WebServices.Inutilizacao.dhRecbto);
  frmCT_e_Gerados.mCte.Lines.add('  Protocolo    : ' + sProtocolo);
  frmCT_e_Gerados.mCte.Lines.add('  Motivo       : ' + sMotivo);
  frmCT_e_Gerados.mCte.Lines.add('  Status       : ' + sStat);
  frmCT_e_Gerados.mCte.Lines.add('  Data/Hora    : ' + sDatahora);
  frmCT_e_Gerados.mCte.Lines.add(' ');

  MessageDlg('Numera��o Inutilizada', mtInformation, [mbOk], 0);
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('update tb_conhecimento set fl_status = ''I'', nm_usuario = :0, ');
  dtmDados.IQuery1.SQL.add
    ('ds_cancelamento = :1, CTE_can_prot = :2, CTE_DATA_CAN = sysdate, ');
  dtmDados.IQuery1.SQL.add('CTE_STATUS = :3 where cod_conhecimento = :4 ');
  dtmDados.IQuery1.Parameters[0].value := GLBUSER;
  dtmDados.IQuery1.Parameters[1].value := sJustificativa;
  dtmDados.IQuery1.Parameters[2].value := sProtocolo;
  dtmDados.IQuery1.Parameters[3].value :=
    ACBrCTe1.WebServices.Inutilizacao.cStat;
  dtmDados.IQuery1.Parameters[4].value :=
    qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
  dtmDados.IQuery1.ExecSQL;

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('update cyber.rodcon  ');
  dtmDados.IQuery1.SQL.add('   set situac = ''C'', ctepcn = :0, ');
  dtmDados.IQuery1.SQL.add(' ctedcn = to_date(' + QuotedStr(sDatahora) +
    ',''dd/mm/yy hh24:mi:ss''), ctecan = ''C'' ');
  dtmDados.IQuery1.SQL.add(' where codcon = :1 and codfil = :2 ');
  dtmDados.IQuery1.Parameters[0].value := sProtocolo;
  dtmDados.IQuery1.Parameters[1].value := qrCteEletronicoNUMEROCTE.AsInteger;
  dtmDados.IQuery1.Parameters[2].value := qrCteEletronicoFL_EMPRESA.AsInteger;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.Close;

  {dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('update tb_tsil set dt_alt_cte = sysdate ');
  dtmDados.IQuery1.SQL.add('where cte = :0 ');
  dtmDados.IQuery1.Parameters[0].value :=
    qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
  dtmDados.IQuery1.ExecSQL;}

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('update dashboard.skf_tb_trip set cte = null, data_etiqueta = null ');
  dtmDados.IQuery1.SQL.add('where cte = :0 and fl_empresa = :1');
  dtmDados.IQuery1.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := qrCteEletronicoFL_EMPRESA.AsInteger;
  dtmDados.IQuery1.ExecSQL;

  frmCT_e_Gerados.mCte.Visible := false;
end;

procedure TfrmMenu.GeraCtrcEletronico(iNovoCtrc: integer);
var
  i, j, CodigoMunicipio, Tomador: integer;
  email, smtp, senhaemail, schave, porta: string;
  obs, contingencia, cop: String;
  stStreamNF: TStringStream;
  pis, cofins: real;
  SSL: Boolean;
  sArquivo, SData, sCaminho, destino, presumido, Protocolo: String;
  mail, SL: TStringList;
begin
  // cc := TStringList.Create;
  // mensagem := TStringList.Create;
  stStreamNF := TStringStream.Create('');
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('select distinct m.estado UF from cyber.rodfil f left join cyber.rodmun m on f.codmun = m.codmun ');
  dtmDados.IQuery1.SQL.add('where f.codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;

  ACBrCTe1.Configuracoes.WebServices.UF := dtmDados.IQuery1.FieldByName
    ('UF').AsString;
  // verifica ambiente do manifesto
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add('select ambiente_cte, certificado, logo, pdf_cte, ');
  dtmDados.IQuery1.SQL.add
    ('schemma, proxy, porta, user_proxy, pass_proxy, cont_cte, ');
  dtmDados.IQuery1.SQL.add('presumido ');
  dtmDados.IQuery1.SQL.add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;
  if (dtmDados.IQuery1.Eof) or (dtmDados.IQuery1.FieldByName('ambiente_cte')
    .IsNull) then
  begin
    ShowMessage('N�o Encontrado nenhuma configura��o de CT-e, avise o TI');
  end
  else
  begin
    presumido := dtmDados.IQuery1.FieldByName('presumido').AsString;
    GlbCteAmb := dtmDados.IQuery1.FieldByName('ambiente_cte').AsString;
    contingencia := dtmDados.IQuery1.FieldByName('cont_cte').AsString;
    frmMenu.ACBrCTe1.Configuracoes.Certificados.NumeroSerie :=
      ALLTRIM(dtmDados.IQuery1.FieldByName('certificado').AsString);
    frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyHost :=
      ALLTRIM(dtmDados.IQuery1.FieldByName('proxy').AsString);
    frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyPort :=
      ALLTRIM(dtmDados.IQuery1.FieldByName('porta').AsString);
    frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyUser :=
      ALLTRIM(dtmDados.IQuery1.FieldByName('user_proxy').AsString);
    frmMenu.ACBrCTe1.Configuracoes.WebServices.ProxyPass :=
      ALLTRIM(dtmDados.IQuery1.FieldByName('pass_proxy').AsString);
    dtmDados.IQuery1.Close;
  end;

  //ACBrCTe1.Configuracoes.Geral.GerarInfCTeSupl := fgtSempre;

  qrCteEletronico.Close;
  qrCteEletronico.Parameters[0].value := iNovoCtrc;
  qrCteEletronico.Open;

  ACBrCTe1.Conhecimentos.Clear;

  with ACBrCTe1.Conhecimentos.add.cte do
  begin
    // Dados de Identifica��o do CT-e
    Ide.cUF := qrCteEletronicoCUFEMIT.AsInteger;
    Ide.cCT := qrCteEletronicoCODCTRC.AsInteger; // C�digo Aleat�rio
    Ide.CFOP := qrCteEletronicoCFOP.AsInteger;
    Ide.natOp := qrCteEletronicoNATOPERACAO.AsString;
    if qrCteEletronicoFORMAPAGTO.AsInteger = 0 then
      Ide.forPag := fpPago
    else
      Ide.forPag := fpAPagar;
    Ide.modelo := qrCteEletronicoMODELOCTE.AsInteger;
    Ide.serie := qrCteEletronicoNR_SERIE.AsInteger;
    Ide.nCT := qrCteEletronicoNUMEROCTE.AsInteger;
    if GlbVerao = 'S' then
      Ide.dhEmi := IncHour(Now(), -1)
    else
      Ide.dhEmi := Now;

    Ide.tpImp := tiRetrato;

    if contingencia = 'S' then
    begin
      // Ide.tpEmis := teSVCRS;
      Ide.dhCont := Now;
      Ide.xJust := 'Problema com comunica��o do Sistema Eletr�nico';
      // Ide.tpEmis := teFSDA;
      if frmMenu.ACBrCTe1.Configuracoes.WebServices.UFCodigo
        in [14, 16, 26, 35, 50, 51] then
      begin
        ACBrCTe1.Configuracoes.Geral.FormaEmissao := teSVCRS;
        Ide.tpEmis := teSVCRS;
      end
      else
      begin
        ACBrCTe1.Configuracoes.Geral.FormaEmissao := teSVCSP;
        Ide.tpEmis := teSVCSP;
      end;
    end
    else
    begin
      Ide.tpEmis := teNormal;
      ACBrCTe1.Configuracoes.Geral.FormaEmissao := teNormal;
    end;

    if GlbCteAmb = 'P' then
      Ide.tpAmb := taProducao
    else
      Ide.tpAmb := taHomologacao;

    // TpcteTipoCTe = (tcNormal, tcComplemento, tcAnulacao, tcSubstituto);
    case qrCteEletronicoTPCTE.AsInteger of
      0:
        Ide.tpCTe := tcNormal;
      1:
        Ide.tpCTe := tcComplemento;
      2:
        Ide.tpCTe := tcAnulacao;
      3:
        Ide.tpCTe := tcSubstituto;
    end;

    // TpcnProcessoEmissao = (peAplicativoContribuinte, peAvulsaFisco, peAvulsaContribuinte, peContribuinteAplicativoFisco);
    Ide.procEmi := peAplicativoContribuinte;
    Ide.verProc := '3.0';
    // Ide.refCTE:=''; // Chave de Acesso do CT-e Referenciado
    Ide.cMunEnv := qrCteEletronicoCODMUNENVI.AsInteger;
    Ide.xMunEnv := qrCteEletronicoMUNENVI.AsString;
    Ide.UFEnv := qrCteEletronicoUFENV.AsString;

    // TpcteModal = (mdRodoviario, mdAereo, mdAquaviario, mdFerroviario, mdDutoviario);
    Ide.modal := mdRodoviario;

    // TpcteTipoServico = (tsNormal, tsSubcontratacao, tsRedespacho, tsIntermediario);
    case qrCteEletronicoTPSERV.AsInteger of
      0:
        Ide.tpServ := tsNormal;
      1:
        Ide.tpServ := tsSubcontratacao;
      2:
        Ide.tpServ := tsRedespacho;
      3:
        Ide.tpServ := tsIntermediario;
    end;

    // Origem da Presta��o
    Ide.cMunIni := qrCteEletronicoCODMUNINI.AsInteger;
    Ide.xMunIni := qrCteEletronicoMUNINI.AsString;
    Ide.UFIni := qrCteEletronicoUFINI.AsString;

    // Destino da Presta��o
    Ide.cMunFim := qrCteEletronicoCMUNFIM.AsInteger;
    Ide.xMunFim := qrCteEletronicoXMUNFIM.AsString;
    Ide.UFFim := qrCteEletronicoUFFIM.AsString;

    // TpcteRetira = (rtSim, rtNao);
    Ide.retira := rtSim;
    Ide.xdetretira := '';

    case qrCteEletronicoTOMA03.AsInteger of
      0:
        Ide.Toma03.Toma := tmRemetente;
      1:
        Ide.Toma03.Toma := tmExpedidor;
      2:
        Ide.Toma03.Toma := tmRecebedor;
      3:
        Ide.Toma03.Toma := tmDestinatario;
    end;

    // Totamdor do Servi�o no CTe 4 = Outros
    if qrCteEletronicoTOMA03.AsInteger = 4 then
    begin

      Ide.Toma4.Toma := tmOutros;
      Ide.Toma4.CNPJCPF := qrCteEletronicoCNPJTOM.AsString;
      { if (trim(qrCteEletronicoIETOM.AsString)='') or
        (trim(qrCteEletronicoIETOM.AsString)='ISENTO') then
        Ide.Toma4.IE := '1234567890'
        else }
      Ide.Toma4.IE := trim(qrCteEletronicoIETOM.AsString);

      Ide.Toma4.xNome := qrCteEletronicoNOMETOM.AsString;
      Ide.Toma4.xFant := '';
      Ide.Toma4.EnderToma.xLgr := qrCteEletronicoLGRTOM.AsString;
      Ide.Toma4.EnderToma.nro := qrCteEletronicoNROTOM.AsString;
      Ide.Toma4.EnderToma.xCpl := '';
      Ide.Toma4.EnderToma.xBairro := qrCteEletronicoBAIRROTOM.AsString;

      Ide.Toma4.EnderToma.cMun := qrCteEletronicoCODMUNTOM.AsInteger;
      Ide.Toma4.EnderToma.xMun := qrCteEletronicoMUNTOM.AsString;
      Ide.Toma4.EnderToma.CEP := qrCteEletronicoCEPTOM.AsInteger;
      Ide.Toma4.EnderToma.UF := qrCteEletronicoUFTOM.AsString;
    end;

    //
    // Informa��es Complementares do CTe
    //
    compl.xCaracAd := trim('');
    compl.xCaracSer := trim('');
    compl.xEmi := trim(qrCteEletronicoNM_USUARIO.AsString);
    compl.fluxo.xOrig := copy(trim(qrCteEletronicoCIDORICALC.AsString), 1, 15);
    compl.fluxo.xDest := copy(trim(qrCteEletronicoCIDDESCALC.AsString), 1, 15);
    compl.fluxo.xRota := trim('');
    // compl.xObs     := qrCteEletronicoPEDIDO.AsString + '     O.S.: ' + qrCteEletronicoNR_OS.AsString;
    compl.origCalc := qrCteEletronicoCIDORICALC.AsString;
    compl.destCalc := qrCteEletronicoCIDDESCALC.AsString;

    // Verifica se tem pedido
    QPedido.Close;
    QPedido.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
    QPedido.Parameters[1].value := GLBFilial;
    QPedido.Open;
    if not QPedido.Eof then
      compl.xObs := 'PEDIDO :' + QPedidoORDCOM.AsString + '       ' +
        qrCteEletronicoOBSERVACAO.AsString
    else
      compl.xObs := qrCteEletronicoOBSERVACAO.AsString;
    QPedido.Close;

    // if qrCteEletronicoCSTIMPOSTO.AsInteger = 41 then
    // compl.xObs := compl.xObs +
    // ';Documento emitido por ME ou EPP optante pelo Simples Nacional' +
    // ';Nao gera direito a credito fiscal de ICMS';

    // Dados do Emitente
    Emit.CNPJ := qrCteEletronicoCNPJEMI.AsString;

    if (trim(qrCteEletronicoIEEMI.AsString) = '') or
      (trim(qrCteEletronicoIEEMI.AsString) = 'ISENTO') then
      Emit.IE := '1234567890'
    else
      Emit.IE := trim(qrCteEletronicoIEEMI.AsString);

    Emit.xNome := qrCteEletronicoNOMEEMI.AsString;
    Emit.xFant := '';
    Emit.EnderEmit.xLgr := qrCteEletronicoLGREMI.AsString;
    Emit.EnderEmit.nro := qrCteEletronicoNROEMI.AsString;
    Emit.EnderEmit.xCpl := '';
    Emit.EnderEmit.xBairro := qrCteEletronicoBAIRROEMMI.AsString;

    Emit.EnderEmit.cMun := qrCteEletronicoCODMUNEMI.AsInteger;
    Emit.EnderEmit.xMun := qrCteEletronicoMUNEMI.AsString;
    Emit.EnderEmit.CEP := qrCteEletronicoCEPEMI.AsInteger;
    Emit.EnderEmit.UF := qrCteEletronicoUFEMI.AsString;
    Emit.EnderEmit.fone := qrCteEletronicoFONEEMI.AsString;

    // Dados do Remetente
    if trim(qrCteEletronicoCNPJREM.AsString) <> '' then
    begin
      Rem.xNome := copy(qrCteEletronicoNOMEREM.AsString, 1, 60);
      Rem.xFant := '';
      Rem.EnderReme.xLgr := qrCteEletronicoLGRREM.AsString;
      Rem.EnderReme.nro := qrCteEletronicoNROREM.AsString;
      Rem.EnderReme.xCpl := qrCteEletronicoCOMPLREM.AsString;
      Rem.EnderReme.xBairro := qrCteEletronicoBAIRROREM.AsString;
      Rem.EnderReme.cMun := qrCteEletronicoCODMUNREM.AsInteger;
      Rem.EnderReme.xMun := qrCteEletronicoMUNREM.AsString;
      Rem.EnderReme.CEP := qrCteEletronicoCEPREM.AsInteger;
      Rem.EnderReme.UF := qrCteEletronicoUFREM.AsString;
      Rem.EnderReme.cPais := qrCteEletronicoCODPAISREM.AsInteger;
      Rem.EnderReme.xPais := qrCteEletronicoNOMEPAISREM.AsString;

      if copy(qrCteEletronicoCNPJREM.AsString, 10, 4) <> '0000' then
      begin
        Rem.CNPJCPF := qrCteEletronicoCNPJREM.AsString;
        Rem.IE := qrCteEletronicoIEREM.AsString;
      end
      else
      begin
        Rem.CNPJCPF := qrCteEletronicoCNPJREM.AsString;
        Rem.IE := 'ISENTO';
      end;

      Rem.fone := qrCteEletronicoFONEREM.AsString;

      { if GlbCteAmb = 'H' then
        begin
        // Rela��o das Notas Fiscais
        QNF.Close;
        QNF.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
        QNF.Parameters[1].value := qrCteEletronicoNR_SERIE.AsString;
        QNF.Parameters[2].value := qrCteEletronicoFL_EMPRESA.AsInteger;
        QNF.Open;
        j := QNF.RecordCount;
        if j > 0 then
        begin
        QNF.First;
        for i := 1 to j do
        begin
        with infCTeNorm.infDoc.InfOutros.add do
        begin
        tpDoc := tdDeclaracao;
        nDoc := QNFNOTFIS.AsString;
        dEmi := QNFDATNOT.AsDateTime;
        vDocFisc := RoundTo(QNFVLRMER.AsFloat, -2);
        end;
        QNF.Next;
        end;
        end;
        end
        else
        begin }
      // Rela��o das Notas Fiscais
      QNF.Close;
      QNF.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
      QNF.Parameters[1].value := qrCteEletronicoNR_SERIE.AsString;
      QNF.Parameters[2].value := qrCteEletronicoFL_EMPRESA.AsInteger;
      QNF.Open;
      j := QNF.RecordCount;
      if j > 0 then
      begin
        QNF.First;
        for i := 1 to j do
        begin
          // eletronica
          if Length(QNFNOTNFE.AsString) = 44 then
          begin
            with infCTeNorm.infDoc.infNFe.New do
            begin
              chave := QNFNOTNFE.AsString;
              // PIN   := qrNFGTIPinSuframa.AsString;
            end;
          end;
          // Nota Fiscal
          if (QNFNOTNFE.AsString = 'MANUAL') then
          begin
            with infCTeNorm.infDoc.infNF.New do
            begin
              nRoma := '';
              nPed := '';
              serie := QNFSERIEN.AsString;
              nDoc := QNFNOTFIS.AsString;
              dEmi := QNFDATNOT.AsDateTime;
              vProd := RoundTo(QNFVLRMER.AsFloat, -2);
              vNF := RoundTo(QNFVLRMER.AsFloat, -2);
              nCFOP := QNFCFOP.AsInteger;
              nPeso := RoundTo(QNFPESOKG.AsFloat, -2);
              // PIN   := qrNFGTIPinSuframa.AsString;

              // Local de Retirada
              { if trim(qrNFGTICNPJRet.AsString)<>''
                then begin
                qrCteEletronicoFJ.Close;
                qrCteEletronicoFJ.SQL.Clear;
                qrCteEletronicoFJ.SQL.Add('Select * From Sis_PessoaFJ');
                qrCteEletronicoFJ.SQL.Add('Where CGC = :xCGC');
                qrCteEletronicoFJ.Params[0].value:=qrNFGTICNPJRet.AsString;
                qrCteEletronicoFJ.Active:=True;
                qrCteEletronicoFJ.Open;

                if copy(qrCteEletronicoFJCGC.AsString,10,4)<>'0000'
                then locRet.CNPJCPF := Copy(qrCteEletronicoFJCGC.AsString, 2, 14)
                else locRet.CNPJCPF := Copy(qrCteEletronicoFJCGC.AsString, 1, 9) +
                Copy(qrCteEletronicoFJCGC.AsString, 14, 2);

                locRet.xNome:=qrCteEletronicoFJRSocial.AsString;
                locRet.xLgr:=qrCteEletronicoFJEndereco.AsString;
                locRet.nro:=qrCteEletronicoFJNumero.AsString;
                locRet.xCpl:=qrCteEletronicoFJComplemento.AsString;
                locRet.xBairro:=qrCteEletronicoFJBairro.AsString;
                CodigoMunicipio:=qrCteEletronicoFJCodigoEstado.AsInteger * 100000 +
                qrCteEletronicoFJCodigoMunicipio.AsInteger;
                locRet.cMun:=CodigoMunicipio;
                locRet.xMun:=qrCteEletronicoFJCidade.AsString;
                locRet.UF:=qrCteEletronicoFJEstado.AsString;
                end; }
            end;
          end;
          // Outros Tipos de Documentos
          if (QNFNOTNFE.AsString = 'DECLARA��O') then
          begin
            with infCTeNorm.infDoc.InfOutros.New do
            begin
              tpDoc := tdDeclaracao;
              nDoc := QNFNOTFIS.AsString;
              dEmi := QNFDATNOT.AsDateTime;
              vDocFisc := RoundTo(QNFVLRMER.AsFloat, -2);
            end;
          end;
          QNF.Next;
        end;
      end;
      // end;
    end;

    // Dados do Destinatario
    if trim(qrCteEletronicoCNPJDES.AsString) <> '' then
    begin
      Dest.xNome := copy(qrCteEletronicoNOMEDES.AsString, 1, 60);
      Dest.EnderDest.xLgr := qrCteEletronicoLGRDES.AsString;
      Dest.EnderDest.nro := qrCteEletronicoNRODES.AsString;
      Dest.EnderDest.xCpl := copy(qrCteEletronicoCOMPLDES.AsString, 1, 60);
      Dest.EnderDest.xBairro := qrCteEletronicoBAIRRODES.AsString;
      Dest.EnderDest.cMun := qrCteEletronicoCODMUNDES.AsInteger;
      Dest.EnderDest.xMun := qrCteEletronicoMUNDES.AsString;
      Dest.EnderDest.CEP := qrCteEletronicoCEPDES.AsInteger;
      Dest.EnderDest.UF := qrCteEletronicoUFDES.AsString;
      Dest.EnderDest.cPais := qrCteEletronicoCODPAISDES.AsInteger;
      Dest.EnderDest.xPais := qrCteEletronicoNOMEPAISDES.AsString;
      Dest.CNPJCPF := qrCteEletronicoCNPJDES.AsString;
      if copy(qrCteEletronicoCNPJDES.AsString, 10, 4) <> '0000' then
        Dest.IE := qrCteEletronicoIEDES.AsString
      else
        Dest.IE := 'ISENTO';

      Dest.fone := qrCteEletronicoFONEDES.AsString;

      // Local de Entrega
      { if trim(qrCteEletronicoCNPJEnt.AsString)<>'' then
        begin
        if copy(qrCteEletronicoFJCGC.AsString,10,4)<>'0000'
        then Dest.locEnt.CNPJCPF := Copy(qrCteEletronicoFJCGC.AsString, 2, 14)
        else Dest.locEnt.CNPJCPF := Copy(qrCteEletronicoFJCGC.AsString, 1, 9) +
        Copy(qrCteEletronicoFJCGC.AsString, 14, 2);

        Dest.locEnt.xNome:=qrCteEletronicoFJRSocial.AsString;
        Dest.locEnt.xLgr:=qrCteEletronicoFJEndereco.AsString;
        Dest.locEnt.nro:=qrCteEletronicoFJNumero.AsString;
        Dest.locEnt.xCpl:=qrCteEletronicoFJComplemento.AsString;
        Dest.locEnt.xBairro:=qrCteEletronicoFJBairro.AsString;
        CodigoMunicipio:=qrCteEletronicoFJCodigoEstado.AsInteger * 100000 +
        qrCteEletronicoFJCodigoMunicipio.AsInteger;
        Dest.locEnt.cMun:=CodigoMunicipio;
        Dest.locEnt.xMun:=qrCteEletronicoFJCidade.AsString;
        Dest.locEnt.UF:=qrCteEletronicoFJEstado.AsString;
        end; }
    end;

    // Dados do Expedidor
    if trim(qrCteEletronicoCNPJEXP.AsString) <> '' then
    begin
      Exped.xNome := qrCteEletronicoNOMEEXP.AsString;
      Exped.EnderExped.xLgr := qrCteEletronicoLGREXP.AsString;
      Exped.EnderExped.nro := qrCteEletronicoNROEXP.AsString;
      Exped.EnderExped.xCpl := qrCteEletronicoCOMPLEXP.AsString;
      Exped.EnderExped.xBairro := qrCteEletronicoBAIRROEXP.AsString;
      Exped.EnderExped.cMun := qrCteEletronicoCODMUNEXP.AsInteger;
      Exped.EnderExped.xMun := qrCteEletronicoMUNEXP.AsString;
      Exped.EnderExped.CEP := qrCteEletronicoCEPEXP.AsInteger;
      Exped.EnderExped.UF := qrCteEletronicoUFEXP.AsString;
      Exped.EnderExped.cPais := qrCteEletronicoCODPAISEXP.AsInteger;
      Exped.EnderExped.xPais := qrCteEletronicoNOMEPAISEXP.AsString;
      Exped.CNPJCPF := qrCteEletronicoCNPJEXP.AsString;
      if copy(qrCteEletronicoCNPJEXP.AsString, 10, 4) <> '0000' then
        Exped.IE := qrCteEletronicoIEEXP.AsString
      else
        Exped.IE := 'ISENTO';
      Exped.fone := qrCteEletronicoFONEEXP.AsString;
    end;

    // Dados do Recebedor
    if trim(qrCteEletronicoCNPJRED.AsString) <> '' then
    begin
      Receb.xNome := qrCteEletronicoNOMERED.AsString;
      Receb.EnderReceb.xLgr := qrCteEletronicoLGRRED.AsString;
      Receb.EnderReceb.nro := qrCteEletronicoNRORED.AsString;
      Receb.EnderReceb.xCpl := qrCteEletronicoCOMPLRED.AsString;
      Receb.EnderReceb.xBairro := qrCteEletronicoBAIRRORED.AsString;
      Receb.EnderReceb.cMun := qrCteEletronicoCODMUNRED.AsInteger;
      Receb.EnderReceb.xMun := qrCteEletronicoMUNRED.AsString;
      Receb.EnderReceb.CEP := qrCteEletronicoCEPRED.AsInteger;
      Receb.EnderReceb.UF := qrCteEletronicoUFRED.AsString;
      Receb.EnderReceb.cPais := qrCteEletronicoCODPAISRED.AsInteger;
      Receb.EnderReceb.xPais := qrCteEletronicoNOMEPAISRED.AsString;
      Receb.CNPJCPF := qrCteEletronicoCNPJRED.AsString;
      if copy(qrCteEletronicoCNPJRED.AsString, 10, 4) <> '0000' then
        Receb.IE := qrCteEletronicoIERED.AsString
      else
        Receb.IE := 'ISENTO';
      Receb.fone := qrCteEletronicoFONERED.AsString;
    end;

    // Quando consignat�rio - colocar dados do documento anterior
    //
    // Informa��es de Documentos de Transporte Anterior
    //
    // TpcteTipoServico = (tsNormal, tsSubcontratacao, tsRedespacho, tsIntermediario);
    { if ide.tpServ = tsSubcontratacao then
      begin
      infCTeNorm.docAnt.emiDocAnt.Add;
      infCTeNorm.docAnt.emiDocAnt[0].idDocAnt.Add;
      infCTeNorm.docAnt.emiDocAnt[0].idDocAnt[0].idDocAntEle.Add;
      infCTeNorm.docAnt.emiDocAnt[0].idDocAnt[0].idDocAntEle[0].chave := qrCteEletronicoCHAVE_CTE_R.AsString;
      infCTeNorm.docAnt.emiDocAnt[0].CNPJCPF := Exped.CNPJCPF;
      infCTeNorm.docAnt.emiDocAnt[0].IE := Exped.IE;
      infCTeNorm.docAnt.emiDocAnt[0].uf := Exped.EnderExped.UF;
      infCTeNorm.docAnt.emiDocAnt[0].xNOME := Exped.xNome;;
      end; }

    // Valores da Presta��o de Servi�o
    vPrest.vTPrest := RoundTo(qrCteEletronicoVLTOTPRESTACAO.AsFloat, -2);
    vPrest.vRec := RoundTo(qrCteEletronicoVLRECEBPRESTACAO.AsFloat, -2);

    dtmDados.RQuery1.Close;
    dtmDados.RQuery1.SQL.Clear;
    dtmDados.RQuery1.SQL.add
      ('select (perpis/100) perpis, (percof/100) percof from rodtab a where codfil = :0');
    dtmDados.RQuery1.Parameters[0].value := GLBFilial;
    dtmDados.RQuery1.Open;
    if dtmDados.RQuery1.Eof then
    begin
      ShowMessage('Pis e Cofins N�o Cadastrado para Filial, Avisar o Fiscal');
      Exit;
    end;

    // Valor Total dos Tributos
    // if presumido = 'S' then
    // begin
    pis := RoundTo(qrCteEletronicoVLRECEBPRESTACAO.AsFloat *
      (dtmDados.RQuery1.FieldByName('perpis').value), -2);
    // 0,65% sobre o faturamento
    cofins := RoundTo(qrCteEletronicoVLRECEBPRESTACAO.AsFloat *
      (dtmDados.RQuery1.FieldByName('percof').value), -2);
    // 3,00% sobre o faturamento
    // end
    // else
    // begin
    // Pis := RoundTo(qrCteEletronicoVLRECEBPRESTACAO.AsFloat* 0.0165, -2); //1,65% sobre o faturamento
    // Cofins := RoundTo(qrCteEletronicoVLRECEBPRESTACAO.AsFloat* 0.076, -2); //7,60% sobre o faturamento
    // end;
    imp.vTotTrib := RoundTo(qrCteEletronicoVALORIMMPOSTO.AsFloat + pis +
      cofins, -2);

    // Obs do Contribuinte
    if (imp.vTotTrib <> 0.0) then
    begin
      with compl.ObsCont.New do
      begin
        xCampo := 'Lei da Transparencia';
        xTexto := 'O valor aproximado de impostos � de R$ ' +
          FormatFloat('##0.00', imp.vTotTrib) + ' - PIS = ' +
          FormatFloat('##0.00', pis) + ' - Cofins = ' +
          FormatFloat('##0.00', cofins);
      end;
      obs := 'Lei da Transparencia ' + 'O valor aproximado de impostos � de R$ '
        + FormatFloat('##0.00', imp.vTotTrib) + ' - PIS = ' +
        FormatFloat('##0.00', pis) + ' - Cofins = ' +
        FormatFloat('##0.00', cofins);
    end;

    // Rela��o dos Componentes da Presta��o de Servi�o
    if qrCteEletronicoFRETE_PESO_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPFRETE_PESO.AsString;
        vComp := RoundTo(qrCteEletronicoFRETE_PESO_XML.AsFloat, -2);
      end;
    end;
    if qrCteEletronicoDESPACHO_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPDESPACHO.AsString;
        vComp := RoundTo(qrCteEletronicoDESPACHO_XML.AsFloat, -2);
      end;
    end;
    if qrCteEletronicoGRIS_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPGRIS.AsString;
        vComp := RoundTo(qrCteEletronicoGRIS_XML.AsFloat, -2);
      end;
    end;
    if qrCteEletronicoAD_VALOREM_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPADVALOREN.AsString;
        vComp := RoundTo(qrCteEletronicoAD_VALOREM_XML.AsFloat, -2);
      end;
    end;
    if qrCteEletronicoPEDAGIO_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPPEDAGIO.AsString;
        vComp := RoundTo(qrCteEletronicoPEDAGIO_XML.AsFloat, -2);
      end;
    end;
    if qrCteEletronicoOUTRAS_TAXAS_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPOUTRASTAXAS.AsString;
        vComp := RoundTo(qrCteEletronicoOUTRAS_TAXAS_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoFLUVIAL_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPFLUVIAL.AsString;
        vComp := RoundTo(qrCteEletronicoFLUVIAL_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoTR_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPTR.AsString;
        vComp := RoundTo(qrCteEletronicoTR_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoTDE_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPTDE.AsString;
        vComp := RoundTo(qrCteEletronicoTDE_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoEXCEDENTE_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPEXED.AsString;
        vComp := RoundTo(qrCteEletronicoEXCEDENTE_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoSEG_BALSA_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPBALSA.AsString;
        vComp := RoundTo(qrCteEletronicoSEG_BALSA_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoREDEP_FLUVIAL_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPREDFLU.AsString;
        vComp := RoundTo(qrCteEletronicoREDEP_FLUVIAL_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoAGEND_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPAGEND.AsString;
        vComp := RoundTo(qrCteEletronicoAGEND_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoPALLET_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPPALLET.AsString;
        vComp := RoundTo(qrCteEletronicoPALLET_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoENTREGA_PORTO_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPPORTO.AsString;
        vComp := RoundTo(qrCteEletronicoENTREGA_PORTO_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoSUFRAMA_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPSUFRAMA.AsString;
        vComp := RoundTo(qrCteEletronicoSUFRAMA_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoCANHOTO_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPCANHOTO.AsString;
        vComp := RoundTo(qrCteEletronicoCANHOTO_XML.AsFloat, -2);
      end;
    end;

    // if qrCteEletronicOcompreevalor.AsString > 0 then
    // begin
    // with vPrest.comp.Add do
    // begin
    // xNome := qrCteEletronicOcompree.AsString;
    // vComp := RoundTo(qrCteEletronicOcompreevalor.AsFloat, -2);
    // end;
    // end;

    if qrCteEletronicoCOMPDEVVALOR.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPDEV.AsString;
        vComp := RoundTo(qrCteEletronicoCOMPDEVVALOR.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoTAS_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPTAS.AsString;
        vComp := RoundTo(qrCteEletronicoTAS_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoTAXA_CTE_XML.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPTXCTE.AsString;
        vComp := RoundTo(qrCteEletronicoTAXA_CTE_XML.AsFloat, -2);
      end;
    end;

    if qrCteEletronicoTAXA_TDA.AsFloat > 0 then
    begin
      with vPrest.comp.New do
      begin
        xNome := qrCteEletronicoCOMPTDA.AsString;
        vComp := RoundTo(qrCteEletronicoTAXA_TDA.AsFloat, -2);
      end;
    end;

    { if qrCteEletronicoCOMPVL_FRETEVALOR.AsString > 0 then
      begin
      with vPrest.comp.Add do
      begin
      xNome := qrCteEletronicoCOMPVL_FRETE.AsString;
      vComp := RoundTo(qrCteEletronicOcompVL_FRETEVALOR.AsFloat, -2);
      end;
      end; }

    { if qrCteEletronicoCSTIMPOSTO.AsString = '00' then
      begin
      Imp.ICMS.SituTrib     := cst00;
      Imp.ICMS.ICMS00.CST   := cst00; // Tributa��o Normal ICMS
      Imp.ICMS.ICMS00.vBC   := RoundTo(qrCteEletronicoBASEIMPOSTO.AsFloat, -2);
      Imp.ICMS.ICMS00.pICMS := RoundTo(qrCteEletronicoALIQIMPOSTO.AsFloat, -2);
      Imp.ICMS.ICMS00.vICMS := RoundTo(qrCteEletronicoVALORIMMPOSTO.AsFloat, -2);
      end;

      if qrCteEletronicoCSTIMPOSTO.AsString = '41' then
      begin
      Imp.ICMS.SituTrib     := cst41;
      Imp.ICMS.ICMS00.CST   := cst41; // N�o Tributado
      Imp.ICMS.ICMS00.vBC   := 0;
      Imp.ICMS.ICMS00.pICMS := 0;
      Imp.ICMS.ICMS00.vICMS := 0;
      end; }

    // Valores dos Impostos
    // TpcnCSTIcms = (cst00, cst10, cst20, cst30, cst40, cst41, cst45, cst50, cst51, cst60, cst70, cst80, cst81, cst90);
    // 80 e 81 apenas para CTe

    case qrCteEletronicoCSTIMPOSTO.AsInteger of
      00:
        begin
          imp.ICMS.SituTrib := cst00;
          imp.ICMS.ICMS00.CST := cst00; // Tributa��o Normal ICMS
          imp.ICMS.ICMS00.vBC :=
            RoundTo(qrCteEletronicoBASEIMPOSTO.AsFloat, -2);
          imp.ICMS.ICMS00.pICMS :=
            RoundTo(qrCteEletronicoALIQIMPOSTO.AsFloat, -2);
          imp.ICMS.ICMS00.vICMS :=
            RoundTo(qrCteEletronicoVALORIMMPOSTO.AsFloat, -2);
        end;
      40:
        begin
          imp.ICMS.SituTrib := cst40;
          imp.ICMS.ICMS45.CST := cst40; // ICMS Isento
        end;
      41:
        begin
          imp.ICMS.SituTrib := cst41;
          imp.ICMS.ICMS45.CST := cst41; // ICMS n�o Tributada
        end;
      51:
        begin
          imp.ICMS.SituTrib := cst51;
          imp.ICMS.ICMS45.CST := cst51; // ICMS diferido
        end;
      90:
        begin
          imp.ICMS.SituTrib := cst90;
          imp.ICMS.ICMS90.CST := cst90; // ICMS Outros
          imp.ICMS.ICMS90.pRedBC := 00;
          // RoundTo(qrCteEletronicoReducaoICMS.AsFloat, -2);
          imp.ICMS.ICMS90.vBC :=
            RoundTo(qrCteEletronicoBASEIMPOSTO.AsFloat, -2);
          imp.ICMS.ICMS90.pICMS :=
            RoundTo(qrCteEletronicoALIQIMPOSTO.AsFloat, -2);
          imp.ICMS.ICMS90.vICMS :=
            RoundTo(qrCteEletronicoVALORIMMPOSTO.AsFloat, -2);
          imp.ICMS.ICMS90.vCred := 00;
          // RoundTo(qrCteEletronicoCreditoICMS.AsFloat, -2);
        end;
    end;

    // Informa��es da Carga
    infCTeNorm.infCarga.vCarga :=
      RoundTo(qrCteEletronicoVALORCARGA.AsFloat, -2);
    infCTeNorm.infCarga.proPred := QNFESPECIE.AsString;
    // qrCteEletronicoDESCRICAOCARGA.AsString;
    infCTeNorm.infCarga.xOutCat := qrCteEletronicoNATOPERACAO.AsString;

    // UnidMed = (uM3,uKG, uTON, uUNIDADE, uLITROS);
    // infCarga.InfQ.Add.cUnid := uUNIDADE;
    // infCarga.InfQ.Add.tpMed := qrCteEletronicoTIPOMEDIDA.AsString;
    // infCarga.InfQ.Add.qCarga:= RoundTo(qrCteEletronicoQTDMEDIDA.AsFloat, -2);

    with infCTeNorm.infCarga.InfQ.New do
    begin
      cUnid := uKg;
      tpMed := 'Kg';
      qCarga := qrCteEletronicoPESOTOTAL.AsCurrency;
      /// StrToFloat(buscatroca(retzero(floattostrf(Qctenr_peso_real.AsString,ffnumber,15,2),15),',','.'));
    end;
    with infCTeNorm.infCarga.InfQ.New do
    begin
      cUnid := uKg;
      tpMed := 'PESO BASE DE CALCULO';
      qCarga := qrCteEletronicoPESOCALCULO.AsCurrency;
      /// StrToFloat(buscatroca(retzero(floattostrf(Qctenr_peso_real.AsString,ffnumber,15,2),15),',','.'));
    end;
    with infCTeNorm.infCarga.InfQ.New do
    begin
      cUnid := uKg;
      tpMed := 'PESO BRUTO';
      qCarga := qrCteEletronicoPESOCALCULO.AsCurrency;
      /// StrToFloat(buscatroca(retzero(floattostrf(Qctenr_peso_real.AsString,ffnumber,15,2),15),',','.'));
    end;
    with infCTeNorm.infCarga.InfQ.New do
    begin
      cUnid := uM3;
      tpMed := 'PESO BC';
      qCarga := qrCteEletronicoCUBAGEM.AsCurrency;
    end;
    with infCTeNorm.infCarga.InfQ.New do
    begin
      cUnid := uUnidade;
      tpMed := qrCteEletronicoTIPOMEDIDA.AsString;
      qCarga := RoundTo(qrCteEletronicoQTDMEDIDA.AsFloat, -2);
    end;

    // Informa��es da Seguradora
    if trim(qrCteEletronicoSEGURADORA.AsString) <> '' then
    begin
      with infCTeNorm.seg.New do
      begin
        { case qrCteEletronicoRESPSEG.AsInteger of
          0: respSeg:=rsRemetente;
          1: respSeg:=rsExpedidor;
          2: respSeg:=rsRecebedor;
          3: respSeg:=rsDestinatario;
          4: respSeg:=rsEmitenteCTe;
          5: respSeg:=rsTomadorServico;
          end; }
        xSeg := copy(trim(qrCteEletronicoSEGURADORA.AsString), 1, 30);

        nApol := copy(trim(qrCteEletronicoAPOLICE.AsString), 1, 20);
        nAver := '';
      end;
    end;

    if qrCteEletronicoTPCTE.AsString = '3' then
    begin
      // Informa��es do Detalhamento do CTe Substituto

      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('select cod_pagador, cte_chave from tb_conhecimento c ');
      dtmDados.IQuery1.SQL.add
        ('where nr_conhecimento = :0 and fl_empresa = :1 ');
      dtmDados.IQuery1.Parameters[0].value :=
        qrCteEletronicoCTE_SUBSTITUICAO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value :=
        qrCteEletronicoFL_EMPRESA.AsInteger;
      dtmDados.IQuery1.Open;

      // infCTeNorm.infCTeSub.

      with infCTeNorm.infCTeSub do
      begin
        chCTe := dtmDados.IQuery1.FieldByName('cte_chave').AsString;

        tomaICMS.refNFe := qrCteEletronicoXML_SUBSTITUICAO.AsString;
        // tomaICMS.refNFe := qrCteEletronicoXML_SUBSTITUICAO.AsString;
        { if dtmdados.IQuery1.FieldByName('cod_pagador').AsString <> qrCteEletronicocod_tomador.AsInteger then
          tomaICMS.refCte       := DM_CNT.ConhecimentoCTeTomador.AsString
          else
          tomaNaoICMS.refCteAnu := DM_CNT.ConhecimentoAnuladoCTe.AsString; }
      end;
    end;

    if qrCteEletronicoTPCTE.AsString = '1' then
    begin
      //
      // Informa��es do Detalhamento do CTe Complementado
      //
      infCTeComp.chave := qrCteEletronicoCTE_CHAVE_CTE_R.AsString;
      { vPresComp.vTPrest := RoundTo(DM_CNT.ComplementoValorTotal.AsFloat, -2);
        //
        //  Informa��es dos Componentes Complementados
        //
        DM_CNT.Componentes.Close;
        DM_CNT.Componentes.SQL.Clear;
        DM_CNT.Componentes.SQL.Add('Select * From Cnt_Componentes');
        DM_CNT.Componentes.SQL.Add('Where Codigo = :xCodigo');
        DM_CNT.Componentes.SQL.Add('and Serie = :xSerie');
        DM_CNT.Componentes.SQL.Add('and Numero = :xNumero');
        DM_CNT.Componentes.SQL.Add('and Complemento = :xComplemento');
        DM_CNT.Componentes.SQL.Add('Order By Item');
        DM_CNT.Componentes.Params[0].AsInteger := DM_CNT.ConhecimentoCodigo.AsInteger;
        DM_CNT.Componentes.Params[1].AsInteger := DM_CNT.ConhecimentoSerie.AsInteger;
        DM_CNT.Componentes.Params[2].AsInteger := DM_CNT.ConhecimentoNumero.AsInteger;
        DM_CNT.Componentes.Params[3].value  := 'S';
        DM_CNT.Componentes.Active := True;
        DM_CNT.Componentes.Open;
        DM_CNT.Componentes.Last;
        l := DM_CNT.Componentes.RecordCount;
        if l > 0
        then begin
        DM_CNT.Componentes.First;
        for k := 1 to l do
        begin
        if DM_CNT.ComponentesValor.AsFloat > 0.0
        then begin
        with vPresComp.compComp.Add do
        begin
        xNome := DM_CNT.ComponentesDescricao.AsString;
        vComp := RoundTo(DM_CNT.ComponentesValor.AsFloat, -2);
        end;
        end;
        DM_CNT.Componentes.Next;
        end;
        end;
        //
        //  Informa��es relativas ao Impostos Complementados
        //
        // TpcnCSTIcms = (cst00, cst10, cst20, cst30, cst40, cst41, cst45, cst50, cst51, cst60, cst70, cst80, cst81, cst90);
        // 80 e 81 apenas para CTe
        case DM_CNT.ComplementoCST.AsInteger of
        00: begin
        ImpComp.ICMSComp.SituTrib     := cst00;
        ImpComp.ICMSComp.ICMS00.CST   := cst00; // Tributa��o Normal ICMS
        ImpComp.ICMSComp.ICMS00.vBC   := RoundTo(DM_CNT.ComplementoBC.AsFloat, -2);
        ImpComp.ICMSComp.ICMS00.pICMS := RoundTo(DM_CNT.ComplementoAtoma.AsFloat, -2);
        ImpComp.ICMSComp.ICMS00.vICMS := RoundTo(DM_CNT.ComplementoValorICMS.AsFloat, -2);
        end;
        20: begin
        ImpComp.ICMSComp.SituTrib      := cst20;
        ImpComp.ICMSComp.ICMS20.CST    := cst20; // Tributa��o com BC reduzida do ICMS
        ImpComp.ICMSComp.ICMS20.pRedBC := RoundTo(DM_CNT.ComplementoReducao.AsFloat, -2);
        ImpComp.ICMSComp.ICMS20.vBC    := RoundTo(DM_CNT.ComplementoBC.AsFloat, -2);
        ImpComp.ICMSComp.ICMS20.pICMS  := RoundTo(DM_CNT.ComplementoAliq.AsFloat, -2);
        ImpComp.ICMSComp.ICMS20.vICMS  := RoundTo(DM_CNT.ComplementoValorICMS.AsFloat, -2);
        end;
        40: begin
        ImpComp.ICMSComp.SituTrib   := cst40;
        ImpComp.ICMSComp.ICMS45.CST := cst40; // ICMS Isento
        end;
        41: begin
        ImpComp.ICMSComp.SituTrib   := cst41;
        ImpComp.ICMSComp.ICMS45.CST := cst41; // ICMS n�o Tributada
        end;
        51: begin
        ImpComp.ICMSComp.SituTrib   := cst51;
        ImpComp.ICMSComp.ICMS45.CST := cst51; // ICMS diferido
        end;
        60: begin
        ImpComp.ICMSComp.SituTrib          := cst60;
        ImpComp.ICMSComp.ICMS60.CST        := cst60; // Tributa��o atribuida ao tomador ou 3. por ST
        ImpComp.ICMSComp.ICMS60.vBCSTRet   := RoundTo(DM_CNT.ComplementoBC.AsFloat, -2);
        ImpComp.ICMSComp.ICMS60.pICMSSTRet := RoundTo(DM_CNT.ComplementoAliq.AsFloat, -2);
        ImpComp.ICMSComp.ICMS60.vICMSSTRet := RoundTo(DM_CNT.ComplementoValorICMS.AsFloat, -2);
        ImpComp.ICMSComp.ICMS60.vCred      := RoundTo(DM_CNT.ComplementoCredito.AsFloat, -2);
        end;
        90: begin
        ImpComp.ICMSComp.SituTrib      := cst90;
        ImpComp.ICMSComp.ICMS90.CST    := cst90; // ICMS Outros
        ImpComp.ICMSComp.ICMS90.pRedBC := RoundTo(DM_CNT.ComplementoReducao.AsFloat, -2);
        ImpComp.ICMSComp.ICMS90.vBC    := RoundTo(DM_CNT.ComplementoBC.AsFloat, -2);
        ImpComp.ICMSComp.ICMS90.pICMS  := RoundTo(DM_CNT.ComplementoAliq.AsFloat, -2);
        ImpComp.ICMSComp.ICMS90.vICMS  := RoundTo(DM_CNT.ComplementoValorICMS.AsFloat, -2);
        ImpComp.ICMSComp.ICMS90.vCred  := RoundTo(DM_CNT.ComplementoCredito.AsFloat, -2);
        end;
        end;

        // Valor Total dos Tributos
        ImpComp.vTotTrib := RoundTo(DM_CNT.ConhecimentovTotTrib.AsFloat, -2);

        // Obs do Contribuinte
        if (impComp.vTotTrib <> 0.0)
        then begin
        with compl.ObsCont.Add do
        begin
        xCampo := 'Lei da Transparencia';
        xTexto := 'O valor aproximado de tributos incidentes sobre o preco deste servico e de R$ ' +
        FormatFloat('##0.00', impComp.vTotTrib) + ' (' +
        FormatFloat('#0.00', DM_CNT.ConhecimentoAliqNBS.AsFloat) + '%) ' +
        'Fonte: IBPT';
        end;
        end;

        end;
        end;
        DM_CNT.Complemento.Next;
        end;
        end; }
    end;

    //
    // Informa��es do(s) Motorista(s)
    //
    with infCTeNorm.Rodo.moto.New do
    begin
      xNome := qrCteEletronicoMOTORISTA.AsString;
      CPF := ApCarac(qrCteEletronicoCPF_MOT.AsString);
    end;

    try
      QAutXml.Close;
      QAutXml.Parameters[0].Value := qrCteEletronicoNUMEROCTE.AsInteger;
      QAutXml.Parameters[1].Value := qrCteEletronicoFL_EMPRESA.AsInteger;
      QAutXml.Open;

      if QAutXmlCNPJ.Value <> '' then
      begin
        autXML.Add.CNPJCPF := QAutXmlCNPJ.Value;
      end;
    except
      on E: Exception do
      begin
        showmessage('Erro com a consulta do transportador para o AutXML');
      end;
    end;

    // Dados do Modal Rodovi�rio
    infCTeNorm.Rodo.RNTRC := qrCteEletronicoRNTRC.AsString;
    infCTeNorm.Rodo.dPrev := (qrCteEletronicoDPREV.AsDateTime + 1);

    // TpcteLocacao = (ltNao, ltsim);
    if qrCteEletronicoLOTACAO.AsString = 'ltSim' then
    begin
      infCTeNorm.Rodo.Lota := ltSim;
      with infCTeNorm.Rodo.Veic.New do
      begin
        placa := qrCteEletronicoPLACA.AsString;
        renavam := qrCteEletronicoRENAVAM.AsString;
        UF := qrCteEletronicoUFVEI.AsString;
      end;
    end
    else
      infCTeNorm.Rodo.Lota := ltNao;
  end;
  ACBrCTe1.Conhecimentos.Assinar;
  ACBrCTe1.Conhecimentos.Validar;
  // salvar o xml antes de enviar
  if self.Tag = 88 then
  begin
    SL := TStringList.Create;
    try
      SL.Text := ACBrCTe1.Conhecimentos.Items[0].XML;
      SL.SaveToFile('C:\SIM\'+qrCteEletronicoCOD_CONHECIMENTO.AsString+'.XML');
    finally
      SL.Free;
    end;
  end;

  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.add('update tb_conhecimento set cte_PROT = ' +
    QuotedStr(ACBrCTe1.Conhecimentos.Items[0].cte.procCTe.nProt) + ', ');
  Query1.SQL.add('cte_chave = ' + QuotedStr(copy(ACBrCTe1.Conhecimentos.Items[0]
    .cte.infCTe.ID, 4, 44)) + ', ');
  Query1.SQL.add('cte_xmotivo = ' + QuotedStr(ACBrCTe1.Conhecimentos.Items[0]
    .cte.procCTe.xMotivo) + ', ');
  Query1.SQL.add('cte_status = ' +
    QuotedStr(IntToStr(ACBrCTe1.Conhecimentos.Items[0].cte.procCTe.cStat)));
  if contingencia = 'S' then
    dtmDados.Query1.SQL.add(', fl_contingencia = ''S'' ');
  if obs <> '' then
    Query1.SQL.add(', obs_transparencia = ' + QuotedStr(obs));
  Query1.SQL.add(' where cod_conhecimento =' +
    QuotedStr(qrCteEletronicoCOD_CONHECIMENTO.AsString));
  Query1.ExecSQL;
  Query1.Close;

 { SL := TStringList.Create;
  try
    SL.Text := ACBrCTe1.Conhecimentos.Items[0].XML;
    SL.SaveToFile('C:\SIM\ARQUIVO.XML');
  finally
    SL.Free;
  end;
  }
  if ACBrCTe1.Conhecimentos.Items[0].cte.procCTe.cStat = 100 then
  begin
    stStreamNF.WriteString(trim(frmMenu.ACBrCTe1.Conhecimentos.Items[0]
      .XMLAssinado));
    // gravar o xml na base de dados
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('insert into tb_xml_cte (cod_conhecimento, xml) values (:0, :1)');
    dtmDados.IQuery1.Parameters[0].value :=
      qrCteEletronicoCOD_CONHECIMENTO.AsString;
    dtmDados.IQuery1.Parameters[1].LoadFromStream(stStreamNF, ftBlob);
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;
    FreeAndNil(stStreamNF);
  end;

  try
    ACBrCTe1.Enviar(1, false);
  except
    on E: Exception do
    begin
      Query1.Close;
      Query1.SQL.Clear;
      Query1.SQL.add('update tb_conhecimento set ');
      Query1.SQL.add('cte_xMsg = ' + QuotedStr(E.Message) + ', ');
      Query1.SQL.add('cte_status = ' +
        QuotedStr(IntToStr(ACBrCTe1.Conhecimentos.Items[0].cte.procCTe.cStat)));
      Query1.SQL.add('where cod_conhecimento =' +
        QuotedStr(qrCteEletronicoCOD_CONHECIMENTO.AsString));
      Query1.SQL.add('and (cte_status <> ''100'' or cte_status is null) ');
      Query1.ExecSQL;
      Query1.Close;
    end;
  end;

  if ACBrCTe1.WebServices.Retorno.cStat = 100 then
  begin
    ACBrCTe1.Conhecimentos.Items[0].GravarXML;
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.add('update tb_conhecimento set cte_recibo = ' +
      QuotedStr(ACBrCTe1.WebServices.Retorno.Recibo) + ', ');
    Query1.SQL.add('cte_PROT = ' + QuotedStr(ACBrCTe1.Conhecimentos.Items[0]
      .cte.procCTe.nProt) + ', ');
    Query1.SQL.add('cte_xmotivo = ' + QuotedStr(ACBrCTe1.Conhecimentos.Items[0]
      .cte.procCTe.xMotivo) + ', ');
    Query1.SQL.add('cte_status = ' +
      QuotedStr(IntToStr(ACBrCTe1.Conhecimentos.Items[0]
      .cte.procCTe.cStat)) + ', ');
    Query1.SQL.add('cte_xMsg = ' +
      QuotedStr(ACBrCTe1.WebServices.Retorno.xMsg) + ', ');
    Query1.SQL.add('cte_data = sysdate ');
    if contingencia = 'S' then
      dtmDados.Query1.SQL.add(', fl_contingencia = ''S'' ');
    Query1.SQL.add('where cod_conhecimento = ' +
      QuotedStr(qrCteEletronicoCOD_CONHECIMENTO.AsString));
    Query1.ExecSQL;
    Query1.Close;

    // coloca o arquivo no repositorio do FTP
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('select distinct a.caminho from tb_email_xml a ');
    dtmDados.IQuery1.SQL.add
      ('where a.codcli = (select cod_pagador from tb_conhecimento where cod_conhecimento = :0)');
    dtmDados.IQuery1.Parameters[0].value :=
      qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
    dtmDados.IQuery1.Open;

    try
      SData := FormatDateTime('YYYY', qrCteEletronicoEMISSAOCTE.AsDateTime) +
        FormatDateTime('MM', qrCteEletronicoEMISSAOCTE.AsDateTime);
      sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + SData
        + '\CTe\';
      sArquivo := sCaminho + copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID,
        4, 44) + '-cte.xml';
      if not dtmDados.IQuery1.Eof then
      begin
        frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sArquivo);
        frmMenu.ACBrCTe1.Consultar;

        destino := dtmDados.IQuery1.FieldByName('caminho').AsString +
          copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44) +
          '-cte.xml';
        CopyFile(PChar(sArquivo), PChar(destino), True);

      end;
      // gravar na pasta para averba��o
      destino := '\\192.168.236.112\cte\' +
        copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44) + '-cte.xml';
      CopyFile(PChar(sArquivo), PChar(destino), True);

      // envio do CT-e para o portal do transportador
      try
        RESTRequest1.Method := TRESTRequestMethod.rmPOST;
        with RESTRequest1.Params.AddItem do
        begin
          ContentType := ctAPPLICATION_JSON;
          name := 'application/xml'; // param name
          value := ACBrCTe1.Conhecimentos.Items[0].XML; // seu json
          Kind := pkREQUESTBODY;
        end;
        RESTRequest1.Execute;
      except
        mail := TStringList.Create;
        try
          mail.values['to'] := 'servicedesk@intecomlogistica.com.br';
          mail.values['subject'] :=
            'ERRO AO ENVIAR CT-E AO PORTAL DO TRANSPORTADOR';
          /// AQUI O ASSUNTO///
          mail.values['body'] := sArquivo;
          /// AQUI O TEXTO NO CORPO DO EMAIL///
          sendEMail(Application.Handle, mail);
        finally
          mail.Free;
        end;
      end;

    except
      on E: Exception do

    end;
  end;

end;

procedure TfrmMenu.averba(cte: integer);
var
  sArquivo, SData, sCaminho: String;
  // protocolo, data, hora: String;   //, sCte
  // inic, fim, a: integer;
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('select distinct user_ws, pass_ws, code_ws from tb_seguradora a ');
  dtmDados.IQuery1.SQL.add('where a.fl_empresa = :0 and trunc(datafim) >= trunc(sysdate)');
  dtmDados.IQuery1.Parameters[0].value := GLBFilial;
  dtmDados.IQuery1.Open;

  SData := FormatDateTime('YYYY', qrCteEletronicoEMISSAOCTE.AsDateTime) +
    FormatDateTime('MM', qrCteEletronicoEMISSAOCTE.AsDateTime);
  sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + SData
    + '\CTe\';
  sArquivo := sCaminho + copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4,
    44) + '-cte.xml';

  ACBrCTe1.Conhecimentos.LoadFromFile(sArquivo);
  ACBrCTe1.Consultar;
  {
    if dtmDados.IQuery1.FieldByName('user_ws').AsString <> '' then
    begin
    resultado := ATM.GetATMWebSvrPortType.averbaCTe20
    ( dtmDados.IQuery1.FieldByName('user_ws').AsString,  dtmDados.IQuery1.FieldByName('pass_ws').AsString,
    dtmDados.IQuery1.FieldByName('code_ws').AsString, frmMenu.ACBrCTe1.Conhecimentos.Items[0].XML);
    // mcte.Visible := true;
    // mcte.Clear;
    // mcte.Lines.Add(resultado);
    resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
    resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);

    if Pos('<PROTOCOLONUMERO>', resultado) > 0 then
    begin
    inic := Pos('<PROTOCOLONUMERO>', resultado) + 17;
    fim := Pos('</PROTOCOLONUMERO>', resultado);
    a := fim - inic;
    protocolo := copy(resultado, inic, a);

    inic := Pos('<DATA>', resultado) + 6;
    fim := Pos('</DATA>', resultado);
    a := fim - inic;
    data := copy(resultado, inic, a);

    inic := Pos('<HORA>', resultado) + 6;
    fim := Pos('</HORA>', resultado);
    a := fim - inic;
    hora := copy(resultado, inic, a);
    data := BuscaTroca(data, '-', '/');
    // dt := StrToDateTime(data + ' ' + hora);
    prc_averba.Parameters[0].value := protocolo;
    prc_averba.Parameters[1].value :=
    StrToDateTime(data + ' ' + hora);
    prc_averba.Parameters[2].value := cte;
    frmMenu.prc_averba.ExecProc;
    end;
    end; }

end;

procedure TfrmMenu.CancelaCTe(nctrc: integer);
var
  SData, sCaminho, sMDFe: string;
  sProtocolo, sMotivo, sDatahora: String;
  mensagem, sCC, Endereco: TStrings;
  EnviaPDF: Boolean;
  stStreamNF: TStringStream;
  sStat: integer;
begin
  qrCteEletronico.Close;
  qrCteEletronico.Parameters[0].value := nctrc;
  qrCteEletronico.Open;

  mensagem := TStringList.Create;
  sCC := TStringList.Create;
  Endereco := TStringList.Create;

  // ACBrCTe1.Configuracoes.WebServices.Visualizar := true;

  ACBrCTe1.Configuracoes.WebServices.UF := qrCteEletronicoUFEMI.AsString;

  QCteXml.Close;
  QCteXml.SQL[1] := 'Where cod_conhecimento = ' + '' +
    qrCteEletronicoCOD_CONHECIMENTO.AsString + '';
  QCteXml.Open;
  if not QCteXml.Eof then
  begin
    stStreamNF := TStringStream.Create(QCteXmlXML.AsString);
    ACBrCTe1.Conhecimentos.Clear;
    ACBrCTe1.Conhecimentos.LoadFromStream(stStreamNF);
  end
  else
  begin
    SData := FormatDateTime('YYYY', qrCteEletronicoEMISSAOCTE.AsDateTime) +
      FormatDateTime('MM', qrCteEletronicoEMISSAOCTE.AsDateTime);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + '\' + SData
      + '\cte\';
    sMDFe := qrCteEletronicoCTE_CHAVE.AsString + '-cte.xml';

    ACBrCTe1.Conhecimentos.Clear;
    ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + sMDFe);
  end;
  QCteXml.Close;

  frmCT_e_Gerados.mCte.Visible := True;
  frmCT_e_Gerados.mCte.Lines.add('XML:');
  frmCT_e_Gerados.mCte.Lines.add(' Carregado');
  frmCT_e_Gerados.mCte.Lines.add
    (' CT-e: ' + copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44));

  if (qrCteEletronicoFL_STATUS.AsString = 'A') then
  begin

    ACBrCTe1.EventoCTe.Evento.Clear;

    with ACBrCTe1.EventoCTe.Evento.New do
    begin
      //infEvento.nSeqEvento := 1;
      // Para o Evento de Cancelamento: nSeqEvento sempre = 1
      infEvento.chCTe :=
        copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44);
      infEvento.CNPJ := qrCteEletronicoCNPJEMI.AsString;
      if GlbVerao = 'S' then
        infEvento.dhEvento := IncHour(Now(), -1)
      else
        infEvento.dhEvento := Now;
      infEvento.tpEvento := teCancelamento;
      infEvento.detEvento.xJust := trim(sJustificativa);
      infEvento.detEvento.nProt := ACBrCTe1.Conhecimentos.Items[0]
        .cte.procCTe.nProt;
    end;

    ACBrCTe1.EnviarEvento(1);
    ACBrCTe1.Conhecimentos.Items[0].GravarXML;

    frmCT_e_Gerados.mCte.Lines.add
      ('-------------------------------------------------------------------------------------------');
    frmCT_e_Gerados.mCte.Lines.add
      (' =>  Cancelando CT-e com chave de acesso n� ' +
      qrCteEletronicoCTE_CHAVE.AsString);
    frmCT_e_Gerados.mCte.Lines.add
      ('-------------------------------------------------------------------------------------------');
    frmCT_e_Gerados.mCte.Lines.add('  Justificativa: ' + sJustificativa);

    sProtocolo := ACBrCTe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items
      [0].RetInfEvento.nProt;
    sMotivo := ACBrCTe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.xMotivo;
    sStat := ACBrCTe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0]
      .RetInfEvento.cStat;
    sDatahora := DateTimeToStr(ACBrCTe1.WebServices.EnvEvento.EventoRetorno.
      retEvento.Items[0].RetInfEvento.dhRegEvento);
    frmCT_e_Gerados.mCte.Lines.add('  Protocolo    : ' + sProtocolo);
    frmCT_e_Gerados.mCte.Lines.add('  Motivo       : ' + sMotivo);
    frmCT_e_Gerados.mCte.Lines.add('  Status       : ' + IntToStr(sStat));
    frmCT_e_Gerados.mCte.Lines.add('  Data/Hora    : ' + sDatahora);
    frmCT_e_Gerados.mCte.Lines.add(' ');

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('update tb_conhecimento set fl_status = ''X'', ');
    dtmDados.IQuery1.SQL.add
      ('usuario_canc = :0, ds_cancelamento = :1, cte_can_prot = :2, ');
    dtmDados.IQuery1.SQL.add('cte_status = :3, ');
    dtmDados.IQuery1.SQL.add('CTE_DATA_CAN = to_date(' + QuotedStr(sDatahora) +
      ',''dd/mm/yy hh24:mi:ss'') where cod_conhecimento = :4 ');
    dtmDados.IQuery1.Parameters[0].value := GLBUSER;
    dtmDados.IQuery1.Parameters[1].value := sJustificativa;
    dtmDados.IQuery1.Parameters[2].value := sProtocolo;
    dtmDados.IQuery1.Parameters[3].value := sStat;
    dtmDados.IQuery1.Parameters[4].value :=
      qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
    dtmDados.IQuery1.ExecSQL;

    {dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('update tb_tsil set dt_alt_cte = sysdate ');
    dtmDados.IQuery1.SQL.add('where cte = :0 ');
    dtmDados.IQuery1.Parameters[0].value :=
      qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
    dtmDados.IQuery1.ExecSQL;}

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('update dashboard.skf_tb_trip set cte = null, data_etiqueta = null ');
    dtmDados.IQuery1.SQL.add('where cte = :0 and fl_empresa = :1');
    dtmDados.IQuery1.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
    dtmDados.IQuery1.Parameters[1].value := qrCteEletronicoFL_EMPRESA.AsInteger;
    dtmDados.IQuery1.ExecSQL;

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('update cyber.rodcon  ');
    dtmDados.IQuery1.SQL.add('   set situac = ''C'', ctepcn = :0, ');
    dtmDados.IQuery1.SQL.add(' ctedcn = to_date(' + QuotedStr(sDatahora) +
      ',''dd/mm/yy hh24:mi:ss''), ctecan = ''C'' ');
    dtmDados.IQuery1.SQL.add(' where codcon = :1 and codfil = :2 ');
    dtmDados.IQuery1.Parameters[0].value := sProtocolo;
    dtmDados.IQuery1.Parameters[1].value := qrCteEletronicoNUMEROCTE.AsInteger;
    dtmDados.IQuery1.Parameters[2].value := qrCteEletronicoFL_EMPRESA.AsInteger;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;

    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('insert into cyber.fatcan (ID_CAN, TIPDOC, CODIGO, SERIE, CODFIL, USUCAN, DATCAN, MOTCAN, DATATU, USUATU, DATINC, TRANSF, INCONS, CODMTC, OBSCAN, CTRMAN, CANENF)  ');
    dtmDados.IQuery1.SQL.add
      (' values ((select max(id_can) + 1 from cyber.fatcan), ''C'', :0, :1, :2, :3, sysdate, :4, sysdate, :5, sysdate, ''N'', ''N'', 4, :6 , ''N'', ''N'') ');
    dtmDados.IQuery1.Parameters[0].value := qrCteEletronicoNUMEROCTE.AsInteger;
    dtmDados.IQuery1.Parameters[1].value := qrCteEletronicoNR_SERIE.AsString;
    dtmDados.IQuery1.Parameters[2].value := qrCteEletronicoFL_EMPRESA.AsInteger;
    dtmDados.IQuery1.Parameters[3].value := GLBUSER;
    dtmDados.IQuery1.Parameters[4].value := sJustificativa;
    dtmDados.IQuery1.Parameters[5].value := GLBUSER;
    dtmDados.IQuery1.Parameters[6].value :=
      'CTE CANCELADO PELO PROCESSO INTEGRADO';
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;

  end;

  frmCT_e_Gerados.mCte.Lines.add
    ('-------------------------------------------------------------------------------------------');
  frmCT_e_Gerados.mCte.Lines.add(' Aguarde Enviando E-mail...');
  // Somente imprime o CTe
  // ACBrCTe1.Conhecimentos.Imprimir;
  Endereco.Clear;
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('select email from cyber.rodctc e left join tb_conhecimento c on c.cod_pagador = e.codclifor ');
  dtmDados.IQuery1.SQL.add('where situac = ''A'' and c.cod_conhecimento = :0');
  dtmDados.IQuery1.Parameters[0].value :=
    qrCteEletronicoCOD_CONHECIMENTO.AsInteger;
  dtmDados.IQuery1.Open;
  while not dtmDados.IQuery1.Eof do
  begin
    Endereco.add(dtmDados.IQuery1.FieldByName('email').AsString);
    dtmDados.IQuery1.Next;
  end;
  dtmDados.IQuery1.Close;

  if Endereco.Text <> '' then
  begin
    mensagem.Clear;
    sCC.Clear;
    mensagem.add
      ('Comunicamos por este e-mail que o Conhecimento Eletr�nico (Chave=' +
      copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44) +
      ') obteve autoriza��o de cancelamento sob o protocolo: ' +
      ACBrCTe1.Conhecimentos.Items[0].cte.procCTe.nProt);
    mensagem.add(' ');
    mensagem.add
      ('Enviamos em anexo o arquivo XML do respectivo CT-e Cancelado.');
    mensagem.add(' ');
    mensagem.add('Atenciosamente,');
    mensagem.add(' ');
    // Mensagem.Add(DM_CTA.EmpresaNome.AsString);
    // Mensagem.Add(' ');
    mensagem.add('>>> Sistema SIM - TI Intecom - IW <<<');

    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('select efiscal, email, smtp, senha, porta from tb_empresa ');
    dtmDados.IQuery1.Open;

    if trim(dtmDados.IQuery1.FieldByName('email').AsString) <> '' then
      sCC.add(trim(dtmDados.IQuery1.FieldByName('email').AsString));

    sCC.add(trim(dtmDados.IQuery1.FieldByName('efiscal').AsString));
    sCC.add(Endereco.Text);

    frmCT_e_Gerados.mCte.Lines.add
      (' CT-e: ' + copy(ACBrCTe1.Conhecimentos.Items[0].cte.infCTe.ID, 4, 44));
    frmCT_e_Gerados.mCte.Lines.add(' Destinat�rio: ' + sCC.Text);

    frmCT_e_Gerados.mCte.Lines.add('  Aguarde... Enviando CT-e por e-mail. ');
    try
      ACBrCTe1.Conhecimentos.Items[0].EnviarEmail
        (trim(dtmDados.IQuery1.FieldByName('efiscal').AsString) // Para
        , 'CT-e Cancelado' // edtEmailAssunto.Text
        , mensagem // mmEmailMsg.Lines
        , EnviaPDF // Enviar PDF junto
        , Endereco// nil //Lista com emails que ser�o enviado c�pias - TStrings
        , nil // Lista de anexos - TStrings
        );
      // Ok:=True;
    except
      // Falha:=True;
    end;

  end
  else
    MessageDlg('Tomador do Servi�o n�o possui, e-mail cadastrado.',
      mtInformation, [mbOk], 0);
  mensagem.Free;
  dtmDados.IQuery1.Close;
  sCC.Free;
  MessageDlg('Conhecimento Cancelado', mtInformation, [mbOk], 0);
  frmCT_e_Gerados.mCte.Visible := false;
  ACBrCTe1.Configuracoes.WebServices.Visualizar := false;



end;

end.
