unit GerarCte_Subs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvExStdCtrls, JvCombobox, JvCheckBox, JvEdit, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, ExtCtrls, ImgList, Buttons, DB,
  ADODB, JvExExtCtrls, JvRadioGroup, IniFiles, JvDBControls, DBCtrls, JvMemo,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls,
  pcnConversao, JvMaskEdit, JvExControls, JvLabel, System.ImageList;

type
  TfrmGerarCte_Subs = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label27: TLabel;
    lblEndPA: TLabel;
    Panel1: TPanel;
    Label30: TLabel;
    Label37: TLabel;
    JvEdit1: TJvEdit;
    edCTRC: TJvCalcEdit;
    edSerie: TJvEdit;
    btnGerar: TBitBtn;
    Panel5: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label40: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    JvDBCalcEdit1: TJvDBCalcEdit;
    JvDBCalcEdit2: TJvDBCalcEdit;
    JvDBCalcEdit3: TJvDBCalcEdit;
    JvDBCalcEdit4: TJvDBCalcEdit;
    JvDBCalcEdit5: TJvDBCalcEdit;
    JvDBCalcEdit6: TJvDBCalcEdit;
    JvDBCalcEdit7: TJvDBCalcEdit;
    JvDBCalcEdit8: TJvDBCalcEdit;
    JvDBCalcEdit9: TJvDBCalcEdit;
    JvDBCalcEdit10: TJvDBCalcEdit;
    JvDBCalcEdit11: TJvDBCalcEdit;
    JvDBCalcEdit12: TJvDBCalcEdit;
    JvDBCalcEdit13: TJvDBCalcEdit;
    JvDBCalcEdit14: TJvDBCalcEdit;
    JvDBCalcEdit15: TJvDBCalcEdit;
    JvDBCalcEdit16: TJvDBCalcEdit;
    JvDBCalcEdit17: TJvDBCalcEdit;
    JvDBCalcEdit18: TJvDBCalcEdit;
    JvDBCalcEdit19: TJvDBCalcEdit;
    JvDBCalcEdit20: TJvDBCalcEdit;
    JvDBCalcEdit21: TJvDBCalcEdit;
    JvDBCalcEdit22: TJvDBCalcEdit;
    edTotal: TJvCalcEdit;
    edTas: TJvCalcEdit;
    edSegBalsa: TJvCalcEdit;
    edoutros: TJvCalcEdit;
    edImposto: TJvCalcEdit;
    edAliquota: TJvCalcEdit;
    edpedagio: TJvCalcEdit;
    edBase: TJvCalcEdit;
    edSeguro: TJvCalcEdit;
    edGris: TJvCalcEdit;
    edRedFlu: TJvCalcEdit;
    edAgend: TJvCalcEdit;
    edPallet: TJvCalcEdit;
    edPorto: TJvCalcEdit;
    edAlfa: TJvCalcEdit;
    edCanhoto: TJvCalcEdit;
    edFrete: TJvCalcEdit;
    edFluvial: TJvCalcEdit;
    edExced: TJvCalcEdit;
    edTR: TJvCalcEdit;
    edDespacho: TJvCalcEdit;
    edTDE: TJvCalcEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label34: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Panel3: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    Label14: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit13: TDBEdit;
    JvMemo1: TJvMemo;
    iml: TImageList;
    Query1: TADOQuery;
    Query1NR_CNPJ_CPF: TStringField;
    Query1COD_CLIENTE: TIntegerField;
    Query1NM_CLIENTE: TStringField;
    Query1NM_FANTASIA: TStringField;
    Query1DS_CIDADE: TStringField;
    Query1DS_UF: TStringField;
    Query1DS_IE_RG: TStringField;
    QCTRC: TADOQuery;
    Query2: TADOQuery;
    SpGerarCtrc: TADOStoredProc;
    dtsCte: TDataSource;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    btnSefaz: TBitBtn;
    btnStatus: TBitBtn;
    dgCtrc: TJvDBUltimGrid;
    MStat: TJvMemo;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    QGerados: TADOQuery;
    dtsGerados: TDataSource;
    QGeradosCOD_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO: TBCDField;
    QGeradosNR_CONHECIMENTO_COMP: TBCDField;
    QGeradosDT_CONHECIMENTO: TDateTimeField;
    QGeradosDT_CONHECIMENTO_COMP: TDateTimeField;
    QGeradosCOD_PAGADOR: TBCDField;
    QGeradosCOD_REMETENTE: TBCDField;
    QGeradosCOD_DESTINATARIO: TBCDField;
    QGeradosCOD_REDESPACHO: TBCDField;
    QGeradosVL_DECLARADO_NF: TFloatField;
    QGeradosNR_QTDE_VOL: TFloatField;
    QGeradosNR_PESO_KG: TFloatField;
    QGeradosNR_PESO_CUB: TFloatField;
    QGeradosVL_FRETE_PESO: TFloatField;
    QGeradosVL_AD: TFloatField;
    QGeradosVL_GRIS: TFloatField;
    QGeradosVL_TR: TFloatField;
    QGeradosVL_TDE: TFloatField;
    QGeradosVL_OUTROS: TFloatField;
    QGeradosVL_PEDAGIO: TFloatField;
    QGeradosVL_FLUVIAL: TFloatField;
    QGeradosVL_EXCED: TFloatField;
    QGeradosVL_DESPACHO: TFloatField;
    QGeradosVL_BALSA: TFloatField;
    QGeradosVL_REDFLU: TFloatField;
    QGeradosVL_AGEND: TFloatField;
    QGeradosVL_PALLET: TFloatField;
    QGeradosVL_PORTO: TFloatField;
    QGeradosVL_ALFAND: TFloatField;
    QGeradosVL_CANHOTO: TFloatField;
    QGeradosVL_REENT: TFloatField;
    QGeradosVL_DEVOL: TFloatField;
    QGeradosVL_BASE: TFloatField;
    QGeradosVL_ALIQUOTA: TFloatField;
    QGeradosVL_IMPOSTO: TFloatField;
    QGeradosVL_TOTAL: TFloatField;
    QGeradosNM_USUARIO: TStringField;
    QGeradosOPERACAO: TBCDField;
    QGeradosDS_TIPO_MERC: TStringField;
    QGeradosDS_TIPO_FRETE: TStringField;
    QGeradosDS_OBS: TStringField;
    QGeradosDS_PLACA: TStringField;
    QGeradosMOTORISTA: TBCDField;
    QGeradosFL_STATUS: TStringField;
    QGeradosFL_TIPO: TStringField;
    QGeradosCODMUNO: TBCDField;
    QGeradosCODMUND: TBCDField;
    QGeradosCFOP: TStringField;
    QGeradosNR_FATURA: TBCDField;
    QGeradosNR_TABELA: TBCDField;
    QGeradosPEDIDO: TStringField;
    QGeradosNR_MANIFESTO: TBCDField;
    QGeradosFL_EMPRESA: TBCDField;
    QGeradosNR_RPS: TBCDField;
    QGeradosDS_CANCELAMENTO: TStringField;
    QGeradosNR_SERIE: TStringField;
    QGeradosVL_IMPOSTO_GNRE: TFloatField;
    QGeradosIMP_DACTE: TStringField;
    QGeradosFL_CONTINGENCIA: TStringField;
    QGeradosFRETE_IMPOSTO: TFloatField;
    QGeradosSEGURO_IMPOSTO: TFloatField;
    QGeradosGRIS_IMPOSTO: TFloatField;
    QGeradosOUTRAS_IMPOSTO: TFloatField;
    QGeradosFLUVIAL_IMPOSTO: TFloatField;
    QGeradosPEDAGIO_IMPOSTO: TFloatField;
    QGeradosFL_TIPO_2: TStringField;
    QGeradosCTE_DATA: TDateTimeField;
    QGeradosCTE_CHAVE: TStringField;
    QGeradosCTE_PROT: TStringField;
    QGeradosCTE_XMOTIVO: TStringField;
    QGeradosCTE_RECIBO: TStringField;
    QGeradosCTE_STATUS: TBCDField;
    QGeradosCTE_XMSG: TStringField;
    QGeradosFL_DDR: TStringField;
    QGeradosCTE_CAN_PROT: TStringField;
    QGeradosCTE_CHAVE_CTE_R: TStringField;
    QGeradosVL_TAS: TFloatField;
    QGeradosCTE_DATA_CAN: TDateTimeField;
    QGeradosNR_LOTE: TBCDField;
    QGeradosVL_TXT_CTE: TFloatField;
    QGeradosVL_FRETE: TFloatField;
    QGeradosNR_CTE_ORIGEM: TBCDField;
    QGeradosOBS_TRANSPARENCIA: TStringField;
    QGeradosXML_VL_FRETE: TBCDField;
    QGeradosXML_VL_PEDAGIO: TBCDField;
    QGeradosXML_VL_OUTROS: TBCDField;
    QGeradosXML_VL_EXCEDENTE: TBCDField;
    QGeradosXML_VL_AD: TBCDField;
    QGeradosXML_VL_GRIS: TBCDField;
    QGeradosXML_VL_ENTREGA: TBCDField;
    QGeradosXML_VL_SEGURO: TBCDField;
    QGeradosXML_VL_TDE: TBCDField;
    QGeradosXML_VL_TR: TBCDField;
    QGeradosXML_VL_SEG_BALSA: TBCDField;
    QGeradosXML_VL_REDEP_FLUVIAL: TBCDField;
    QGeradosXML_VL_AGEND: TBCDField;
    QGeradosXML_VL_PALLET: TBCDField;
    QGeradosXML_VL_TAS: TBCDField;
    QGeradosXML_VL_DESPACHO: TBCDField;
    QGeradosXML_VL_ENTREGA_PORTO: TBCDField;
    QGeradosXML_VL_ALFAND: TBCDField;
    QGeradosXML_VL_CANHOTO: TBCDField;
    QGeradosXML_VL_FLUVIAL: TBCDField;
    QGeradosXML_VL_DEVOLUCAO: TBCDField;
    QGeradosXML_VL_AJUDA: TBCDField;
    QGeradosXML_VL_TX_CTE: TBCDField;
    QGeradosNF_VL_FRETE: TBCDField;
    QGeradosNF_VL_PEDAGIO: TBCDField;
    QGeradosNF_VL_EXCEDENTE: TBCDField;
    QGeradosNF_VL_AD: TBCDField;
    QGeradosNF_VL_GRIS: TBCDField;
    QGeradosNF_VL_ENTREGA: TBCDField;
    QGeradosNF_VL_SEGURO: TBCDField;
    QGeradosNF_VL_TDE: TBCDField;
    QGeradosNF_VL_TR: TBCDField;
    QGeradosNF_VL_SEG_BALSA: TBCDField;
    QGeradosNF_VL_REDEP_FLUVIAL: TBCDField;
    QGeradosNF_VL_AGEND: TBCDField;
    QGeradosNF_VL_PALLET: TBCDField;
    QGeradosNF_VL_TAS: TBCDField;
    QGeradosNF_VL_DESPACHO: TBCDField;
    QGeradosNF_VL_ENTREGA_PORTO: TBCDField;
    QGeradosNF_VL_ALFAND: TBCDField;
    QGeradosNF_VL_CANHOTO: TBCDField;
    QGeradosNF_VL_FLUVIAL: TBCDField;
    QGeradosNF_VL_DEVOLUCAO: TBCDField;
    QGeradosNF_VL_AJUDAR: TBCDField;
    QGeradosNF_VL_CTE: TBCDField;
    QGeradosNF_NR: TBCDField;
    QGeradosNF_VALORTT: TBCDField;
    QGeradosEMAIL_ENVIADO: TStringField;
    QGeradosLOTE_SEFAZ: TStringField;
    QGeradosNR_ROMANEIO: TBCDField;
    QGeradosCOD_EXPEDIDOR: TBCDField;
    QGeradosSERIE_MAN: TStringField;
    QGeradosFILIAL_MAN: TBCDField;
    QGeradosCOD_CST: TStringField;
    QGeradosCLIENTE: TStringField;
    QGeradosNM_ORIGEM: TStringField;
    QGeradosNM_DESTINO: TStringField;
    Label13: TLabel;
    edChave: TJvMaskEdit;
    edPagador: TJvMaskEdit;
    QTomador: TADOQuery;
    QTomadorCODCLIFOR: TBCDField;
    QTomadorNOME: TStringField;
    QTomadorCODCGC: TStringField;
    QCTRCCOD_CONHECIMENTO: TBCDField;
    QCTRCFL_EMPRESA: TBCDField;
    QCTRCCUFEMIT: TStringField;
    QCTRCCODUFORIGEM: TStringField;
    QCTRCUFORI: TStringField;
    QCTRCCODCTRC: TBCDField;
    QCTRCCTE_CHAVE: TStringField;
    QCTRCCFOP: TStringField;
    QCTRCNATOPERACAO: TStringField;
    QCTRCNR_SERIE: TStringField;
    QCTRCFORMAPAGTO: TStringField;
    QCTRCMODELOCTE: TStringField;
    QCTRCNUMEROCTE: TBCDField;
    QCTRCEMISSAOCTE: TDateTimeField;
    QCTRCTPIMP: TStringField;
    QCTRCTPEMIS: TStringField;
    QCTRCDIGVERFICHAVEACES: TStringField;
    QCTRCTPAMB: TStringField;
    QCTRCTPCTE: TStringField;
    QCTRCPROCEMI: TStringField;
    QCTRCVERPROC: TStringField;
    QCTRCCODMUNENVI: TStringField;
    QCTRCMUNENVI: TStringField;
    QCTRCUFENV: TStringField;
    QCTRCMODAL: TStringField;
    QCTRCTPSERV: TStringField;
    QCTRCCODMUNINI: TStringField;
    QCTRCMUNINI: TStringField;
    QCTRCUFINI: TStringField;
    QCTRCCMUNFIM: TStringField;
    QCTRCXMUNFIM: TStringField;
    QCTRCUFFIM: TStringField;
    QCTRCRETIRA: TStringField;
    QCTRCTOMA03: TStringField;
    QCTRCCNPJEMI: TStringField;
    QCTRCIEEMI: TStringField;
    QCTRCNOMEEMI: TStringField;
    QCTRCLGREMI: TStringField;
    QCTRCNROEMI: TBCDField;
    QCTRCBAIRROEMMI: TStringField;
    QCTRCCODMUNEMI: TStringField;
    QCTRCMUNEMI: TStringField;
    QCTRCCEPEMI: TStringField;
    QCTRCUFEMI: TStringField;
    QCTRCFONEEMI: TStringField;
    QCTRCCNPJTOM: TStringField;
    QCTRCIETOM: TStringField;
    QCTRCNOMETOM: TStringField;
    QCTRCFONETOM: TStringField;
    QCTRCLGRTOM: TStringField;
    QCTRCCOMPLTOM: TStringField;
    QCTRCNROTOM: TBCDField;
    QCTRCBAIRROTOM: TStringField;
    QCTRCCODMUNTOM: TStringField;
    QCTRCMUNTOM: TStringField;
    QCTRCCEPTOM: TStringField;
    QCTRCUFTOM: TStringField;
    QCTRCCODPAISTOM: TStringField;
    QCTRCNOMEPAISTOM: TStringField;
    QCTRCTIPOPESSOAREM: TStringField;
    QCTRCCNPJREM: TStringField;
    QCTRCIEREM: TStringField;
    QCTRCNOMEREM: TStringField;
    QCTRCFONEREM: TStringField;
    QCTRCLGRREM: TStringField;
    QCTRCCOMPLREM: TStringField;
    QCTRCNROREM: TBCDField;
    QCTRCBAIRROREM: TStringField;
    QCTRCCODMUNREM: TStringField;
    QCTRCMUNREM: TStringField;
    QCTRCCEPREM: TStringField;
    QCTRCUFREM: TStringField;
    QCTRCCODPAISREM: TStringField;
    QCTRCNOMEPAISREM: TStringField;
    QCTRCTIPOPESSOAEXP: TStringField;
    QCTRCCNPJEXP: TStringField;
    QCTRCIEEXP: TStringField;
    QCTRCNOMEEXP: TStringField;
    QCTRCFONEEXP: TStringField;
    QCTRCLGREXP: TStringField;
    QCTRCCOMPLEXP: TStringField;
    QCTRCNROEXP: TBCDField;
    QCTRCBAIRROEXP: TStringField;
    QCTRCCODMUNEXP: TStringField;
    QCTRCMUNEXP: TStringField;
    QCTRCCEPEXP: TStringField;
    QCTRCUFEXP: TStringField;
    QCTRCCODPAISEXP: TStringField;
    QCTRCNOMEPAISEXP: TStringField;
    QCTRCCNPJDES: TStringField;
    QCTRCTIPOPESSOA: TStringField;
    QCTRCIEDES: TStringField;
    QCTRCNOMEDES: TStringField;
    QCTRCFONEDES: TStringField;
    QCTRCLGRDES: TStringField;
    QCTRCCOMPLDES: TStringField;
    QCTRCNRODES: TBCDField;
    QCTRCBAIRRODES: TStringField;
    QCTRCCODMUNDES: TStringField;
    QCTRCMUNDES: TStringField;
    QCTRCCEPDES: TStringField;
    QCTRCUFDES: TStringField;
    QCTRCCODPAISDES: TStringField;
    QCTRCNOMEPAISDES: TStringField;
    QCTRCCNPJRED: TStringField;
    QCTRCIERED: TStringField;
    QCTRCNOMERED: TStringField;
    QCTRCFONERED: TStringField;
    QCTRCLGRRED: TStringField;
    QCTRCCOMPLRED: TStringField;
    QCTRCNRORED: TBCDField;
    QCTRCBAIRRORED: TStringField;
    QCTRCCODMUNRED: TStringField;
    QCTRCMUNRED: TStringField;
    QCTRCCEPRED: TStringField;
    QCTRCUFRED: TStringField;
    QCTRCCODPAISRED: TStringField;
    QCTRCNOMEPAISRED: TStringField;
    QCTRCVLTOTPRESTACAO: TFloatField;
    QCTRCVLRECEBPRESTACAO: TFloatField;
    QCTRCCOMPFRETE_PESO: TStringField;
    QCTRCCOMPFRETE_PESOVALOR: TBCDField;
    QCTRCCOMPDESPACHO: TStringField;
    QCTRCCOMPDESPACHOVALOR: TFloatField;
    QCTRCCOMPGRIS: TStringField;
    QCTRCCOMPGRISVALOR: TFloatField;
    QCTRCCOMPADVALOREN: TStringField;
    QCTRCCOMPADVALORENVALOR: TFloatField;
    QCTRCCOMPPEDAGIO: TStringField;
    QCTRCCOMPPEDAGIOVALOR: TFloatField;
    QCTRCCOMPOUTRASTAXAS: TStringField;
    QCTRCCOMPOUTROSTAXASVALOR: TFloatField;
    QCTRCCOMPFLUVIAL: TStringField;
    QCTRCCOMPFLUVIALVALOR: TFloatField;
    QCTRCCOMPTR: TStringField;
    QCTRCCOMPTRVALOR: TFloatField;
    QCTRCCOMPTDE: TStringField;
    QCTRCCOMPTDEVALOR: TFloatField;
    QCTRCCOMPEXED: TStringField;
    QCTRCCOMPEXEDVALOR: TFloatField;
    QCTRCCOMPBALSA: TStringField;
    QCTRCCOMPBALSAVALOR: TFloatField;
    QCTRCCOMPREDFLU: TStringField;
    QCTRCCOMPREDFLUVALOR: TFloatField;
    QCTRCCOMPAGEND: TStringField;
    QCTRCCOMPAGENDVALOR: TFloatField;
    QCTRCCOMPPALLET: TStringField;
    QCTRCCOMPPALLETVALOR: TFloatField;
    QCTRCCOMPPORTO: TStringField;
    QCTRCCOMPPORTOVALOR: TFloatField;
    QCTRCCOMPALFA: TStringField;
    QCTRCCOMPALFAVALOR: TFloatField;
    QCTRCCOMPCANHOTO: TStringField;
    QCTRCCOMPCANHOTOVALOR: TFloatField;
    QCTRCCOMPDEV: TStringField;
    QCTRCCOMPDEVVALOR: TFloatField;
    QCTRCCOMPTAS: TStringField;
    QCTRCCOMPTASVALOR: TFloatField;
    QCTRCCOMPTXCTE: TStringField;
    QCTRCCOMPTXCTEVALOR: TFloatField;
    QCTRCCOMPVL_FRETE: TStringField;
    QCTRCCOMPVL_FRETEVALOR: TFloatField;
    QCTRCCSTIMPOSTO: TStringField;
    QCTRCDESCCSTIMPOSTO: TStringField;
    QCTRCBASEIMPOSTO: TFloatField;
    QCTRCALIQIMPOSTO: TBCDField;
    QCTRCVALORIMMPOSTO: TBCDField;
    QCTRCVALORCARGA: TFloatField;
    QCTRCDESCRICAOCARGA: TStringField;
    QCTRCCODIGOUNIDADE: TStringField;
    QCTRCTIPOMEDIDA: TStringField;
    QCTRCQTDMEDIDA: TFloatField;
    QCTRCRNTRC: TStringField;
    QCTRCDPREV: TDateTimeField;
    QCTRCLOTA: TStringField;
    QCTRCPESOTOTAL: TFloatField;
    QCTRCCUBAGEM: TFloatField;
    QCTRCPESOCALCULO: TBCDField;
    QCTRCDS_TIPO_FRETE: TStringField;
    QCTRCOBSERVACAO: TStringField;
    QCTRCTIPO: TStringField;
    QCTRCFORMAPAGAMENTO: TStringField;
    QCTRCTOMADOR: TStringField;
    QCTRCPROTOCOLO: TStringField;
    QCTRCFL_CONTINGENCIA: TStringField;
    QCTRCUFORICALC: TStringField;
    QCTRCCIDORICALC: TStringField;
    QCTRCUFDESCALC: TStringField;
    QCTRCCIDDESCALC: TStringField;
    QCTRCMOTORISTA: TStringField;
    QCTRCCPF_MOT: TStringField;
    QCTRCNM_USUARIO: TStringField;
    QCTRCSEGURADORA: TStringField;
    QCTRCAPOLICE: TStringField;
    QCTRCRESPSEG: TBCDField;
    QCTRCFL_STATUS: TStringField;
    QCTRCPLACA: TStringField;
    QCTRCRENAVAM: TStringField;
    QCTRCLOTACAO: TStringField;
    QCTRCUFVEI: TStringField;
    QCTRCCTE_PROT: TStringField;
    QCTRCTP_VEI: TStringField;
    QCTRCCTE_CHAVE_CTE_R: TStringField;
    QCTRCNR_CONHECIMENTO_COMP: TBCDField;
    QCTRCNR_MANIFESTO: TBCDField;
    QCTRCNR_TABELA: TBCDField;
    QCTRCCOD_PAGADOR: TBCDField;
    JvLabel1: TJvLabel;
    RGTipo: TRadioGroup;
    QCTRCNR_FATURA: TBCDField;
    Label15: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure somavalores;
    procedure btnGerarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvEdit1Exit(Sender: TObject);
    procedure QCTRCAfterOpen(DataSet: TDataSet);
    procedure edSerieExit(Sender: TObject);
    procedure edFreteExit(Sender: TObject);
    procedure edpedagioExit(Sender: TObject);
    procedure edoutrosExit(Sender: TObject);
    procedure edFluvialExit(Sender: TObject);
    procedure edExcedExit(Sender: TObject);
    procedure edTRExit(Sender: TObject);
    procedure edDespachoExit(Sender: TObject);
    procedure edTDEExit(Sender: TObject);
    procedure edGrisExit(Sender: TObject);
    procedure edSegBalsaExit(Sender: TObject);
    procedure edRedFluExit(Sender: TObject);
    procedure edAgendExit(Sender: TObject);
    procedure edPalletExit(Sender: TObject);
    procedure edPortoExit(Sender: TObject);
    procedure edAlfaExit(Sender: TObject);
    procedure edCanhotoExit(Sender: TObject);
    procedure edTasExit(Sender: TObject);
    procedure edSeguroExit(Sender: TObject);
    procedure btnSefazClick(Sender: TObject);
    procedure QGeradosBeforeOpen(DataSet: TDataSet);
    procedure btnStatusClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBEdit9DblClick(Sender: TObject);

  private
  var
    new: Integer;
  public
    { Public declarations }
  end;

var
  frmGerarCte_Subs: TfrmGerarCte_Subs;
  imposto, gerado: String;

implementation

uses Dados, funcoes, Menu, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmGerarCte_Subs.BitBtn1Click(Sender: TObject);
begin
  { if cbredesp.ItemIndex > 0 then
    begin
    edPagador.text := cbredesp.Text;

    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.sql.add('select (cp.endere || '' - '' || cp.numero || '' - '' || mp.descri) cidade ');
    dtmdados.IQuery1.sql.add('from cyber.rodcli cp left join cyber.rodmun mp on cp.codmun = mp.codmun ');
    dtmdados.IQuery1.sql.add('where cp.codclifor = :0');
    dtmdados.IQuery1.Parameters[0].value := Copy(edPagador.text, Pos('-', edPagador.text) + 1, 6 );
    dtmdados.IQuery1.Open;

    JvLabel1.Caption := dtmdados.IQuery1.FieldByName('cidade').value;
    dtmdados.IQuery1.Close;
    end;
    Predesp.Visible := false; }
end;

procedure TfrmGerarCte_Subs.btnGerarClick(Sender: TObject);
var
  novo: Integer;
begin
  if QCTRCNR_FATURA.value > 0 then
  begin
    ShowMessage
      ('OEste CT-e j� foi Faturado, � necess�rio o cancelamento da Fatura');
    exit;
  end;

  if RGTipo.ItemIndex = -1 then
  begin
    ShowMessage('Qual Tipo ?');
    RGTipo.SetFocus;
    exit;
  end;

  if (edAliquota.value = 0) and (RGTipo.ItemIndex = 1) then
  begin
    ShowMessage('O Valor da Aliquota est� Zero ?');
    edAliquota.SetFocus;
    exit;
  end;

  if Length(edChave.text) < 44 then
  begin
    ShowMessage('Falta a Chave da NF Anulada do Cliente');
    edChave.SetFocus;
    exit;
  end;

  if (edTotal.value = 0) and (RGTipo.ItemIndex = 1) then
  begin
    ShowMessage('O Valor do Total est� Zero ?');
    edTotal.SetFocus;
    exit;
  end;

  if Length(JvMemo1.text) < 170 then
  begin
    ShowMessage('N�o foi colocada toda observa��o passada pelo Fiscal !!');
    JvMemo1.SetFocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Gerar Este CT-e?', 'Gerar CT-e',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    btnGerar.Enabled := false;
    SpGerarCtrc.Parameters[1].value := QCTRCNUMEROCTE.AsInteger;
    SpGerarCtrc.Parameters[2].value := QCTRCNR_SERIE.AsString;
    SpGerarCtrc.Parameters[3].value := edFrete.value;
    SpGerarCtrc.Parameters[4].value := edpedagio.value;
    SpGerarCtrc.Parameters[5].value := edoutros.value;
    SpGerarCtrc.Parameters[6].value := edFluvial.value;
    SpGerarCtrc.Parameters[7].value := edExced.value;
    SpGerarCtrc.Parameters[8].value := edTR.value;
    SpGerarCtrc.Parameters[9].value := edDespacho.value;
    SpGerarCtrc.Parameters[10].value := edTDE.value;
    SpGerarCtrc.Parameters[11].value := edTas.value;
    SpGerarCtrc.Parameters[12].value := edSeguro.value;
    SpGerarCtrc.Parameters[13].value := edGris.value;
    SpGerarCtrc.Parameters[14].value := edSegBalsa.value;
    SpGerarCtrc.Parameters[15].value := edRedFlu.value;
    SpGerarCtrc.Parameters[16].value := edAgend.value;
    SpGerarCtrc.Parameters[17].value := edPallet.value;
    SpGerarCtrc.Parameters[18].value := edPorto.value;
    SpGerarCtrc.Parameters[19].value := edAlfa.value;
    SpGerarCtrc.Parameters[20].value := edCanhoto.value;
    SpGerarCtrc.Parameters[21].value := edBase.value;
    SpGerarCtrc.Parameters[22].value := edAliquota.value;
    SpGerarCtrc.Parameters[23].value := edImposto.value;
    SpGerarCtrc.Parameters[24].value := edTotal.value;
    SpGerarCtrc.Parameters[25].value := QCTRCFL_EMPRESA.AsInteger;
    SpGerarCtrc.Parameters[26].value := alltrim(JvMemo1.text);
    SpGerarCtrc.Parameters[27].value := GLBUSER;
    // if edPagador.Text <> '' then
    // SpGerarCtrc.Parameters[28].value := Copy(edPagador.text, Pos('-', edPagador.text) + 1, 6 )
    // else
    SpGerarCtrc.Parameters[28].value := QCTRCCOD_PAGADOR.AsInteger;
    SpGerarCtrc.Parameters[29].value := edChave.text;
    if RGTipo.ItemIndex = 0 then
      SpGerarCtrc.Parameters[30].value := 'A'
    else
      SpGerarCtrc.Parameters[30].value := 'S';

    SpGerarCtrc.ExecProc;
    novo := SpGerarCtrc.Parameters[0].value;
    ShowMessage('CT-e ' + IntToStr(novo) + ' Gerado com Sucesso');
    // GravaLog(name,IntToStr(novo),'Gerar CTRC Complementar');
    frmGerarCte_Subs.JvEdit1.text := retzero(IntToStr(novo), 6);
    frmGerarCte_Subs.JvEdit1Exit(Sender);

  end;
  PageControl1.ActivePageIndex := 1;

end;

procedure TfrmGerarCte_Subs.btnSefazClick(Sender: TObject);
var
  reg: Integer;
begin
  if QGeradosFL_TIPO.value = 'C' then
  begin
    reg := QGeradosCOD_CONHECIMENTO.AsInteger;
    frmMenu.GeraCtrcEletronico(QGeradosCOD_CONHECIMENTO.AsInteger);
    try
      frmMenu.qrCteEletronico.Close;
      frmMenu.qrCteEletronico.Parameters[0].value :=
        QGeradosCOD_CONHECIMENTO.AsString;
      frmMenu.qrCteEletronico.open;

      frmMenu.QNF.Close;
      frmMenu.QNF.Parameters[0].value :=
        frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
      frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.value;
      frmMenu.QNF.Parameters[2].value :=
        frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
      frmMenu.QNF.open;

      Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
      frmDacte_Cte_2.sChaveCte := frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
      frmDacte_Cte_2.rlDacte.Preview();
    finally
      frmDacte_Cte_2.Free;
    end;
    QGerados.Close;
    QGerados.open;
    QGerados.Locate('cod_conhecimento', reg, []);
  end;
end;

procedure TfrmGerarCte_Subs.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrCTe1.WebServices.StatusServico.Executar;
  MStat.Lines.text :=
    UTF8Encode(frmMenu.ACBrCTe1.WebServices.StatusServico.RetWS);
  MStat.Visible := true;
  MStat.Clear;
  MStat.Lines.Add('Status Servi�o');
  MStat.Lines.Add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.tpAmb));
  MStat.Lines.Add('verAplic: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.verAplic);
  MStat.Lines.Add('cStat: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cStat));
  MStat.Lines.Add('xMotivo: ' + frmMenu.ACBrCTe1.WebServices.
    StatusServico.xMotivo);
  MStat.Lines.Add('cUF: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cUF));
  MStat.Lines.Add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRecbto));
  MStat.Lines.Add('tMed: ' +
    IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.TMed));
  MStat.Lines.Add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrCTe1.WebServices.StatusServico.dhRetorno));
  MStat.Lines.Add('xObs: ' + frmMenu.ACBrCTe1.WebServices.StatusServico.xObs);
end;

procedure TfrmGerarCte_Subs.DBEdit9DblClick(Sender: TObject);
begin
  { QTomador.Open;
    cbredesp.Items.Add('Nenhum');
    while not QTomador.eof do
    begin
    cbredesp.Items.Add(QTomadorNOME.Value+'-'+QTomadorCODCLIFOR.AsString);
    QTomador.next;
    end;
    QTomador.Close;
    Predesp.Visible := true;
    DBEdit9.Visible := false;
    DBText3.Visible := false;
    DBText7.Visible := false;
    JvLabel1.Visible := true; }
end;

procedure TfrmGerarCte_Subs.edAgendExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edAlfaExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edCanhotoExit(Sender: TObject);
begin
  if (edCanhoto.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edDespachoExit(Sender: TObject);
begin
  if (edDespacho.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edExcedExit(Sender: TObject);
begin
  if (edExced.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edFluvialExit(Sender: TObject);
begin
  if (edFluvial.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edFreteExit(Sender: TObject);
begin
  if (edFrete.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edGrisExit(Sender: TObject);
begin
  if (edGris.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edoutrosExit(Sender: TObject);
begin
  if (edoutros.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edPalletExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edpedagioExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edPortoExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edRedFluExit(Sender: TObject);
begin
  if (edRedFlu.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edSegBalsaExit(Sender: TObject);
begin
  if (edSegBalsa.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edSeguroExit(Sender: TObject);
begin
  if (edSeguro.text <> IntToStr(0)) then
    somavalores;
end;

procedure TfrmGerarCte_Subs.edSerieExit(Sender: TObject);
begin
  if edCTRC.value > 0 then
  begin
    QCTRC.Close;
    QCTRC.Parameters[0].value := edCTRC.value;
    QCTRC.Parameters[1].value := edSerie.text;
    QCTRC.Parameters[2].value := GLBFilial;
    QCTRC.open;
    if QCTRC.IsEmpty then
    begin
      ShowMessage('CT-e n�o encontrado !!');
      edCTRC.SetFocus;
      exit;
    end;

    btnGerar.Enabled := true;
    // JvMemo1.Clear;
    // JvMemo1.Lines.Add('Substituto do CT-e :'+ edCTRC.Text);
    edAliquota.value := QCTRCALIQIMPOSTO.value;
    edFrete.SetFocus;
  end;
end;

procedure TfrmGerarCte_Subs.edTasExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edTDEExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.edTRExit(Sender: TObject);
begin
  somavalores;
end;

procedure TfrmGerarCte_Subs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Query1.Close;
  QCTRC.Close;
  Query2.Close;
end;

procedure TfrmGerarCte_Subs.FormCreate(Sender: TObject);
begin
  self.caption := 'Gerar Conhecimento de Transportes - Substitui��o - Filial :'
    + glbnmfilial;
  imposto := 'S';
  PageControl1.TabIndex := 0;
end;

procedure TfrmGerarCte_Subs.JvEdit1Exit(Sender: TObject);
begin
  if JvEdit1.text <> '' then
  begin
    QCTRC.Close;
    QCTRC.Parameters[0].value := StrToInt(JvEdit1.text);
    QCTRC.Parameters[1].value := edSerie.text;
    QCTRC.Parameters[2].value := GLBFilial;
    QCTRC.open;
    if QCTRC.IsEmpty then
    begin
      ShowMessage('CT-e n�o encontrado !!');
      edCTRC.SetFocus;
      exit;
    end;
    gerado := 'S';
    new := QCTRCCOD_CONHECIMENTO.AsInteger;
  end;
end;

procedure TfrmGerarCte_Subs.somavalores;
begin

  edTotal.text := '';
  edBase.text := '';
  edImposto.text := '';

  if imposto = 'S' then
  begin
    edTotal.value := edFrete.value + edSeguro.value + edGris.value +
      edoutros.value + edFluvial.value + edExced.value + edImposto.value +
      edpedagio.value + edTR.value + edTDE.value + edDespacho.value +
      edSegBalsa.value + edRedFlu.value + edAgend.value + edPallet.value +
      edPorto.value + edAlfa.value + edCanhoto.value + edTas.value;
    edBase.value := edTotal.value;
  end
  else
  begin
    edTotal.value := edFrete.value + edSeguro.value + edGris.value +
      edoutros.value + edFluvial.value + edExced.value + edpedagio.value +
      edTR.value + edTDE.value + edDespacho.value + edSegBalsa.value +
      edRedFlu.value + edAgend.value + edPallet.value + edPorto.value +
      edAlfa.value + edCanhoto.value + edTas.value;
    edBase.value := edTotal.value;
  end;

  edImposto.value := (edBase.value / ((100 - edAliquota.value) / 100) *
    (edAliquota.value / 100));

  edBase.value := edBase.value + edImposto.value;
  edTotal.value := edBase.value;
end;

procedure TfrmGerarCte_Subs.TabSheet2Hide(Sender: TObject);
begin
  QGerados.Close;
end;

procedure TfrmGerarCte_Subs.TabSheet2Show(Sender: TObject);
begin
  QGerados.open;
end;

procedure TfrmGerarCte_Subs.QCTRCAfterOpen(DataSet: TDataSet);
begin
  if QCTRCCOMPFRETE_PESOVALOR.value > 0 then
    JvDBCalcEdit1.Color := $0080FFFF
  else
    JvDBCalcEdit1.Color := clWhite;
  if QCTRCCOMPPEDAGIOVALOR.value > 0 then
    JvDBCalcEdit2.Color := $0080FFFF
  else
    JvDBCalcEdit2.Color := clWhite;
  if QCTRCCOMPOUTROSTAXASVALOR.value > 0 then
    JvDBCalcEdit3.Color := $0080FFFF
  else
    JvDBCalcEdit3.Color := clWhite;
  if QCTRCCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit4.Color := $0080FFFF
  else
    JvDBCalcEdit4.Color := clWhite;
  if QCTRCCOMPEXEDVALOR.value > 0 then
    JvDBCalcEdit5.Color := $0080FFFF
  else
    JvDBCalcEdit5.Color := clWhite;
  if QCTRCCOMPTRVALOR.value > 0 then
    JvDBCalcEdit6.Color := $0080FFFF
  else
    JvDBCalcEdit6.Color := clWhite;
  if QCTRCCOMPDESPACHOVALOR.value > 0 then
    JvDBCalcEdit7.Color := $0080FFFF
  else
    JvDBCalcEdit7.Color := clWhite;
  if QCTRCCOMPTDEVALOR.value > 0 then
    JvDBCalcEdit8.Color := $0080FFFF
  else
    JvDBCalcEdit8.Color := clWhite;
  if QCTRCCOMPGRISVALOR.value > 0 then
    JvDBCalcEdit9.Color := $0080FFFF
  else
    JvDBCalcEdit9.Color := clWhite;
  if QCTRCCOMPBALSAVALOR.value > 0 then
    JvDBCalcEdit10.Color := $0080FFFF
  else
    JvDBCalcEdit10.Color := clWhite;
  if QCTRCCOMPREDFLUVALOR.value > 0 then
    JvDBCalcEdit11.Color := $0080FFFF
  else
    JvDBCalcEdit11.Color := clWhite;
  if QCTRCCOMPAGENDVALOR.value > 0 then
    JvDBCalcEdit12.Color := $0080FFFF
  else
    JvDBCalcEdit12.Color := clWhite;
  if QCTRCCOMPPALLETVALOR.value > 0 then
    JvDBCalcEdit13.Color := $0080FFFF
  else
    JvDBCalcEdit13.Color := clWhite;
  if QCTRCCOMPPORTOVALOR.value > 0 then
    JvDBCalcEdit14.Color := $0080FFFF
  else
    JvDBCalcEdit14.Color := clWhite;
  if QCTRCCOMPALFAVALOR.value > 0 then
    JvDBCalcEdit15.Color := $0080FFFF
  else
    JvDBCalcEdit15.Color := clWhite;
  if QCTRCCOMPCANHOTOVALOR.value > 0 then
    JvDBCalcEdit16.Color := $0080FFFF
  else
    JvDBCalcEdit16.Color := clWhite;
  if QCTRCCOMPTASVALOR.value > 0 then
    JvDBCalcEdit17.Color := $0080FFFF
  else
    JvDBCalcEdit17.Color := clWhite;
  if QCTRCCOMPADVALORENVALOR.value > 0 then
    JvDBCalcEdit18.Color := $0080FFFF
  else
    JvDBCalcEdit18.Color := clWhite;

  if QCTRCBASEIMPOSTO.value > 0 then
    JvDBCalcEdit19.Color := $0080FFFF
  else
    JvDBCalcEdit19.Color := clWhite;
  if QCTRCALIQIMPOSTO.value > 0 then
    JvDBCalcEdit20.Color := $0080FFFF
  else
    JvDBCalcEdit20.Color := clWhite;
  if QCTRCVALORIMMPOSTO.value > 0 then
    JvDBCalcEdit21.Color := $0080FFFF
  else
    JvDBCalcEdit21.Color := clWhite;
  if QCTRCVLTOTPRESTACAO.value > 0 then
    JvDBCalcEdit22.Color := $0080FFFF
  else
    JvDBCalcEdit22.Color := clWhite;
end;

procedure TfrmGerarCte_Subs.QGeradosBeforeOpen(DataSet: TDataSet);
begin
  QGerados.Parameters[0].value := GLBFilial;
end;

end.
