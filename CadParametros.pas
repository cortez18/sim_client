unit CadParametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, System.ImageList, System.Actions;

type
  TfrmCadParametros = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QParametro: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Label1: TLabel;
    cbTDE: TJvCheckBox;
    cbFMin: TJvCheckBox;
    cbNF: TJvCheckBox;
    cbOST: TJvCheckBox;
    cbVolume: TJvCheckBox;
    cbFatura: TJvCheckBox;
    cbKM: TJvCheckBox;
    QParametroID: TBCDField;
    QParametroCODCLI: TBCDField;
    QParametroPAR_1: TStringField;
    QParametroPAR_2: TStringField;
    Label2: TLabel;
    edit1: TJvCalcEdit;
    QParametronome: TStringField;
    QParametroPAR_3: TStringField;
    QParametroPAR_4: TStringField;
    QParametroMAPA_VOLUME: TStringField;
    QParametroPRAZO_PGTO: TFMTBCDField;
    QParametroDIA_SEMANA: TFMTBCDField;
    QParametroFATURAMENTO: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Edit1Change(Sender: TObject);
    procedure QParametroCalcFields(DataSet: TDataSet);
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadParametros: TfrmCadParametros;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadParametros.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadParametros.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  edit1.text := '';
  cbTDE.Checked := false;
  cbFMin.Checked := false;
  cbNF.Checked := false;
  cbOST.Checked := false;
  cbVolume.Checked := false;
  cbFatura.Checked := false;
  cbKM.Checked := false;
  edit1.SetFocus;
end;

procedure TfrmCadParametros.btnSalvarClick(Sender: TObject);
var
  v_id: Integer;
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select (nvl(max(id),0) + 1) codigo from tb_parametro');
      dtmDados.IQuery1.open;
      v_id := dtmDados.IQuery1.FieldByName('codigo').value;
      dtmDados.IQuery1.Close;
      QParametro.append;
      QParametroID.value := v_id;
    end
    else
    begin
      QParametro.edit;
    end;
    QParametroCODCLI.value := edit1.value;
    if cbTDE.Checked = true then
      QParametroPAR_1.value := 'S'
    else
      QParametroPAR_1.value := 'N';
    if cbFMin.Checked = true then
      QParametroPAR_2.value := 'S'
    else
      QParametroPAR_2.value := 'N';
    if cbNF.Checked = true then
      QParametroPAR_3.value := 'S'
    else
      QParametroPAR_3.value := 'N';
    if cbOST.Checked = true then
      QParametroPAR_4.value := 'S'
    else
      QParametroPAR_4.value := 'N';

    if cbVolume.Checked = true then
      QParametroMAPA_VOLUME.Value := 'S'
    else
      QParametroMAPA_VOLUME.value := 'N';

    if cbFMin.Checked = true then
      QParametroFATURAMENTO.Value := 'S'
    else
      QParametroFATURAMENTO.value := 'N';

    if cbFatura.Checked = true then
      QParametroFATURAMENTO.Value := 'S'
    else
      QParametroFATURAMENTO.value := 'N';
   {   if cbCRM.Checked = true then
      QParametroFL_CRM.Value := 'S'
      else
      QParametroFL_CRM.value := 'N';
      if cbFilial.Checked = true then
      QParametroFL_Filial.Value := 'S'
      else
      QParametroFL_Filial.value := 'N';
      if cbKM.Checked = true then
      QParametroFL_KM.Value := 'S'
      else
      QParametroFL_KM.value := 'N'; }
    QParametro.post;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QParametro.Close;
  QParametro.open;
end;

procedure TfrmCadParametros.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.Field = QParametroPAR_1 then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroPAR_1.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QParametroPAR_2 then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroPAR_2.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QParametroPAR_3 then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroPAR_3.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QParametroPAR_4 then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroPAR_4.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QParametroMAPA_VOLUME then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroMAPA_VOLUME.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
 if Column.Field = QParametroFATURAMENTO then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroFATURAMENTO.value = 'N' then
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  {  if Column.Field = QParametroFL_receita then
    begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroFL_receita.value = 'N' then
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    end;
    if Column.Field = QParametroFL_CRM then
    begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroFL_CRM.value = 'N' then
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    end;
    if Column.Field = QParametroFL_filial then
    begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroFL_Filial.value = 'N' then
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    end;
    if Column.Field = QParametroFL_km then
    begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QParametroFL_KM.value = 'N' then
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    end; }
end;

procedure TfrmCadParametros.Edit1Change(Sender: TObject);
begin
  if edit1.value > 0 then
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select razsoc from cyber.rodcli where codclifor = :0');
    dtmDados.IQuery1.Parameters[0].value := edit1.text;
    dtmDados.IQuery1.open;
    if dtmDados.IQuery1.eof then
    begin
      ShowMessage('Cliente N�o Localizado');
      edit1.value := 0;
      edit1.SetFocus;
      Exit;
    end
    else
      Label2.caption := dtmDados.IQuery1.FieldByName('razsoc').value;
  end;
end;

procedure TfrmCadParametros.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QParametro.Cancel;
end;

procedure TfrmCadParametros.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  edit1.value := QParametroCODCLI.value;

  if QParametroPAR_1.value = 'S' then
    cbTDE.Checked := true
  else
    cbTDE.Checked := false;
  if QParametroPAR_2.value = 'S' then
    cbFMin.Checked := true
  else
    cbFMin.Checked := false;
  if QParametroPAR_3.value = 'S' then
    cbNF.Checked := true
  else
    cbNF.Checked := false;
  if QParametroPAR_4.value = 'S' then
    cbOST.Checked := true
  else
    cbOST.Checked := false;

   if QParametroMAPA_VOLUME.Value = 'S' then
    cbVolume.Checked := true
   else
    cbVolume.Checked := false;
   if QParametroFATURAMENTO.Value = 'S' then
    cbFatura.Checked := true
    else
    cbFatura.Checked := false;
  {   if QParametroFL_receita.Value = 'S' then
    cbReceita.Checked := true
    else
    cbReceita.Checked := false;
    if QParametroFL_CRM.Value = 'S' then
    cbCRM.Checked := true
    else
    cbCRM.Checked := false;
    if QParametroFL_filial.Value = 'S' then
    cbFilial.Checked := true
    else
    cbFilial.Checked := false;
    if QParametroFL_KM.Value = 'S' then
    cbkm.Checked := true
    else
    cbkm.Checked := false; }
  edit1.SetFocus;
end;

procedure TfrmCadParametros.FormCreate(Sender: TObject);
begin
  QParametro.open;
  quant := QParametro.RecordCount;
  lblQuant.caption := IntToStr(QParametro.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadParametros.QParametroCalcFields(DataSet: TDataSet);
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select razsoc from cyber.rodcli where codclifor = :0');
  dtmDados.IQuery1.Parameters[0].value := QParametroCODCLI.AsInteger;
  dtmDados.IQuery1.open;
  if not dtmDados.IQuery1.eof then
    QParametronome.value := dtmDados.IQuery1.FieldByName('razsoc').value;

end;

procedure TfrmCadParametros.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QParametro.Close;
end;

procedure TfrmCadParametros.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
    pnBotao.Visible := false
  else
    pnBotao.Visible := true;
end;

procedure TfrmCadParametros.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QParametro.First;
    2:
      QParametro.Prior;
    3:
      QParametro.Next;
    4:
      QParametro.Last;
  end;
  lblQuant.caption := IntToStr(QParametro.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
