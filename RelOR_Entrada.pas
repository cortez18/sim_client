unit RelOR_Entrada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRelOR_Entrada = class(TForm)
    QEntrada: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    Label8: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    QClienteCODCLIFOR: TBCDField;
    QEntradaNR_LIEFERSCHEIN: TStringField;
    QEntradaTIME_NEU: TDateTimeField;
    QEntradaTIME_AEN: TDateTimeField;
    QEntradaCODOSA: TBCDField;
    QEntradaDATINC: TDateTimeField;
    QEntradaNOTFIS: TStringField;
    JvDBNavigator1: TJvDBNavigator;
    QClienteBASE_WMS: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelOR_Entrada: TfrmRelOR_Entrada;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelOR_Entrada.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_OR_' + ApCarac(DateToStr(date)
    ) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QEntrada.First;
  Gauge1.MaxValue := QEntrada.RecordCount;
  while not QEntrada.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QEntrada.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QEntrada.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmRelOR_Entrada.btRelatorioClick(Sender: TObject);
begin
  if ledIdCliente.LookupValue = '' then
  begin
    showmessage('Escolha o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QEntrada.close;
  QEntrada.SQL[12] := 'FROM WE' + QClienteBASE_WMS.AsString + ' B';
  QEntrada.SQL[15] := 'and B.time_aen between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QEntrada.Parameters[0].value := QClienteCODCLIFOR.AsInteger;
  QEntrada.Parameters[1].value := QClienteCODCLIFOR.AsInteger;
  QEntrada.Parameters[2].value := QClienteCODCLIFOR.AsInteger;
  QEntrada.Parameters[3].value := QClienteNM_CLIENTE.AsString;
  QEntrada.open;
  Panel1.Visible := false;
end;

procedure TfrmRelOR_Entrada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEntrada.close;
  QCliente.close;
end;

procedure TfrmRelOR_Entrada.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true;
  dtInicial.date := date - 1;
  dtFinal.date := date;
end;

procedure TfrmRelOR_Entrada.JvDBGrid1TitleClick(Column: TColumn);
begin
  if Pos('order by', QEntrada.SQL.Text) > 0 then
    QEntrada.SQL.Text := Copy(QEntrada.SQL.Text, 1, Pos('order by', QEntrada.SQL.Text) -
      1) + 'order by ' + Column.FieldName
  else
    QEntrada.SQL.Text := QEntrada.SQL.Text + ' order by ' + Column.FieldName;
  QEntrada.open;
end;

procedure TfrmRelOR_Entrada.QClienteBeforeOpen(DataSet: TDataSet);
begin
  QCliente.Parameters[0].Value := GLBFilial;
end;

end.
