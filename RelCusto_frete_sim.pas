unit RelCusto_frete_sim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj,
  JvMemoryDataset, ImgList, jpeg, FileCtrl, Vcl.Themes,
  JvMaskEdit, JvDBControls, xmldom, XMLIntf, msxmldom, XMLDoc,
  RLRichText, System.ImageList, ACBrBase, ACBrMail, JvMemo;

type
  TfrmRelCusto_frete_sim = class(TForm)
    dtsRepre: TDataSource;
    Panel1: TPanel;
    QFornecedor: TADOQuery;
    dtsFornecedor: TDataSource;
    iml: TImageList;
    QRepre: TADOQuery;
    SPRel: TADOStoredProc;
    QFornecedorCOD_FORNECEDOR: TBCDField;
    QFornecedorNM_FORNECEDOR: TStringField;
    QFornecedorNR_CNPJ_CPF: TStringField;
    QNF: TADOQuery;
    mdRepre: TJvMemoryData;
    MMNF: TMemo;
    Panel6: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    cbT: TCheckBox;
    RadioGroup1: TRadioGroup;
    btRelatorio: TBitBtn;
    Panel7: TPanel;
    JvDBGrid1: TJvDBGrid;
    Panel4: TPanel;
    Label2: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    btnGerarExcel: TBitBtn;
    Panel2: TPanel;
    Label8: TLabel;
    Label1: TLabel;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    edVlrPrev: TJvCalcEdit;
    btnImpr: TBitBtn;
    btnSair: TBitBtn;
    EdHist: TEdit;
    edFatura: TEdit;
    Panel5: TPanel;
    Label6: TLabel;
    btnSalvarPrevia: TBitBtn;
    btnCancelaPrevia: TBitBtn;
    btnImprimirPrevia: TBitBtn;
    btnSairPrevia: TBitBtn;
    edPrevia: TEdit;
    btnCarregar: TBitBtn;
    Panel3: TPanel;
    Label15: TLabel;
    Label36: TLabel;
    Label16: TLabel;
    cbCampo: TComboBox;
    edLocalizar: TEdit;
    cbOperador: TComboBox;
    btProcurar: TBitBtn;
    btRetirarFiltro: TBitBtn;
    cbFieldName: TComboBox;
    btnPrevia: TBitBtn;
    Panel8: TPanel;
    btnPgto: TBitBtn;
    btnimprimir: TBitBtn;
    btnExcel: TBitBtn;
    Label3: TLabel;
    edCusto: TJvCalcEdit;
    Label4: TLabel;
    edCalculado: TJvCalcEdit;
    Label5: TLabel;
    edManifesto: TJvMaskEdit;
    btnManifesto: TBitBtn;
    btnLimpaFiltro: TBitBtn;
    SP_FATURA: TADOStoredProc;
    Memo1: TMemo;
    Label7: TLabel;
    QRepreTIPO: TStringField;
    QRepreDATA: TDateTimeField;
    QRepreFILIAL: TBCDField;
    QRepreRAZSOC: TStringField;
    QRepreCODCON: TBCDField;
    QRepreCODFIL: TBCDField;
    QReprePESCAL: TBCDField;
    QRepreVLRMER: TBCDField;
    QRepreDESTINO: TStringField;
    QRepreTOTFRE: TBCDField;
    QRepreSTATUS: TStringField;
    QRepreFATURA: TStringField;
    QRepreTRANSP: TBCDField;
    mdRepreescol: TIntegerField;
    mdRepreDOC: TIntegerField;
    mdReprecte_custo: TIntegerField;
    mdReprevalor_cobrado: TFloatField;
    mdReprevalor_rec: TFloatField;
    mdRepreTransp: TBCDField;
    mdRepreFatura: TStringField;
    mdRepreStatus: TStringField;
    mdRepreTotfre: TBCDField;
    mdRepreDestino: TStringField;
    mdRepreVlrmer: TBCDField;
    mdReprePescal: TBCDField;
    mdRepreCodfil: TBCDField;
    mdRepreCodcon: TBCDField;
    mdRepreRazSoc: TStringField;
    mdRepreFilial: TBCDField;
    mdRepreData: TDateTimeField;
    mdRepreTipo: TStringField;
    mdRepreCusto_calc: TFloatField;
    QRepreDOC: TBCDField;
    QRepreCTE_CUSTO: TBCDField;
    QRepreVALOR_COBRADO: TBCDField;
    QRepreVALOR_REC: TBCDField;
    mdReprenf: TStringField;
    QCusto: TADOQuery;
    QCustoCUSTO: TBCDField;
    QNFNR_NF: TStringField;
    mdRepreMargem: TFloatField;
    QRepreOBS: TStringField;
    QRepreLIBERACAO: TStringField;
    mdRepreobs: TStringField;
    mdRepreliberacao: TStringField;
    JvDBNavigator1: TJvDBNavigator;
    Label9: TLabel;
    QSite: TADOQuery;
    QSiteFILIAL: TStringField;
    dtsSite: TDataSource;
    cbFilial: TJvComboBox;
    QSiteCODFIL: TBCDField;
    mdReprepeso_alterado: TFloatField;
    QReprePESO_ALTERADO: TBCDField;
    QRepreID_REGISTRO: TStringField;
    QRepreCUSTO_CALC: TBCDField;
    QRepreCUSTO: TBCDField;
    QRepreFL_TDE: TStringField;
    QRepreFL_TR: TStringField;
    mdReprefl_tde: TStringField;
    mdReprefl_tr: TStringField;
    QResumo: TADOQuery;
    QResumoRECEITA: TBCDField;
    QResumoCUSTO: TBCDField;
    QResumoMARGEM: TBCDField;
    QAdiant: TADOQuery;
    QAdiantCODFIC: TBCDField;
    QAdiantOBSERV: TStringField;
    QAdiantVLRADI: TBCDField;
    mdRepreDias: TIntegerField;
    QFornecedorDIAS: TBCDField;
    Label12: TLabel;
    edDesconto: TJvCalcEdit;
    QDesconto: TADOQuery;
    QDescontoDESCONTO: TBCDField;
    QDescontoOBS: TStringField;
    SP_CUSTO: TADOStoredProc;
    mdReprecod_tab: TIntegerField;
    QRepreCODPAG: TBCDField;
    QRepreCOD_TAB: TBCDField;
    mdReprexml: TStringField;
    DBXML: TJvDBMaskEdit;
    QRepreXML_CUSTO: TStringField;
    Label17: TLabel;
    edVcto: TJvDateEdit;
    Label18: TLabel;
    edImp: TJvCalcEdit;
    QDescontoVL_IMPOSTOS: TBCDField;
    SP_Exporta: TADOStoredProc;
    btnOco: TBitBtn;
    cbTipo: TJvComboBox;
    Label19: TLabel;
    QImpCte: TADOQuery;
    QImpCteID: TBCDField;
    QImpCteNUMERO: TBCDField;
    QImpCteVALOR: TBCDField;
    RgSerie: TRadioGroup;
    QRepreSERIE: TStringField;
    QDoccob: TADOQuery;
    QDoccobFATURA: TBCDField;
    btnBO_2: TBitBtn;
    Label20: TLabel;
    edFat: TJvMaskEdit;
    Label21: TLabel;
    edProcesso: TJvCalcEdit;
    QDescontoPROCESSO: TBCDField;
    mdRepreHE: TIntegerField;
    QRepreHE: TBCDField;
    Label22: TLabel;
    edData: TJvDateEdit;
    QRepreKM_TOTAL: TBCDField;
    mdReprekm: TFloatField;
    mdRepreserie: TStringField;
    Label23: TLabel;
    QKM: TADOQuery;
    QKMDESCRI: TStringField;
    QKMCODCLIFOR: TBCDField;
    QKMGEO: TStringField;
    XMLDoc: TXMLDocument;
    QRepreOPERACAO: TStringField;
    mdRepreoperacao: TStringField;
    QKMCODCEP: TStringField;
    QRepreplaca: TStringField;
    mdReprePlaca: TStringField;
    QReprevl_tde: TBCDField;
    mdReprevl_tde: TBCDField;
    mdRepreDEDICADO: TStringField;
    QReprecod_tab_i: TBCDField;
    QRepreromaneio: TStringField;
    mdReprenovo_custo: TBCDField;
    QReprenovo_custo: TBCDField;
    QReprecidade: TStringField;
    QRepreestado: TStringField;
    mdReprecidade: TStringField;
    mdRepreestado: TStringField;
    Label13: TLabel;
    edVctoM: TJvDateEdit;
    lblescolhido: TLabel;
    btnBaixa: TBitBtn;
    mdRepreagregado: TStringField;
    QRepreCODCMO: TFMTBCDField;
    mdReprebaixa: TIntegerField;
    Label24: TLabel;
    edCTePagar: TJvMaskEdit;
    QRepreCODDES: TFMTBCDField;
    QRepreDESCONTO: TFMTBCDField;
    QRepreVL_IMPOSTOS: TBCDField;
    QRepreDATAALT: TDateTimeField;
    QRepreTIPO_DESC: TFMTBCDField;
    QRepreDT_LIBERADO: TDateTimeField;
    QRepreNEW_CUSTO: TFMTBCDField;
    QRepreCODMUN: TFMTBCDField;
    QRepreDT_FATURA: TDateTimeField;
    QRepreTIPDOC: TStringField;
    QRepreNR_PESO_CUB: TFMTBCDField;
    mdReprePESOCUB: TFMTBCDField;
    QReprexml_custo_fis: TStringField;
    mdReprexml_custo_fis: TStringField;
    Label25: TLabel;
    cbtipo2: TJvComboBox;
    Label26: TLabel;
    edDesconto2: TJvCalcEdit;
    QDescontoDESCONTO2: TBCDField;
    QRepreTDE: TStringField;
    mdRepreTDE: TStringField;
    QReprecte_gen: TFMTBCDField;
    mdReprecte_gen: TFMTBCDField;
   { RLExpressionParser1: TRLExpressionParser;
    RLReport2: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLIMIW: TRLImage;
    RlImInt: TRLImage;
    RLLabel1: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLGroup1: TRLGroup;
    RLBand5: TRLBand;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText6: TRLDBText;
    RLBand2: TRLBand;
    RlCodfic: TRLLabel;
    RlAd: TRLLabel;
    RlCodcon: TRLLabel;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBResult5: TRLDBResult;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLBand3: TRLBand;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLTTPagarDes: TRLLabel;
    RLLabel17: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText3: TRLDBText;
    RLTTAdv: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RlObs: TRLRichText;
    RLDBResult6: TRLDBResult;
    RLBand4: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo; }
    QRepreDEDICADO: TStringField;
    mdReprefl_dedicado: TStringField;
    mensagem: TJvMemo;
    ACBrMail1: TACBrMail;
    Qemail: TADOQuery;
    QemailRAZSOC: TStringField;
    QemailTOTAL: TFMTBCDField;
    QemailDT_VCTO: TDateTimeField;
    QemailDATAALT: TStringField;
    QDedicado: TADOQuery;
    QDedicadoTEM: TFMTBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure btnPgtoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnimprimirClick(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    procedure btRetirarFiltroClick(Sender: TObject);
    procedure cbOperadorExit(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure cbTClick(Sender: TObject);
    procedure cbCampoChange(Sender: TObject);
    procedure MDRepre3AfterOpen(DataSet: TDataSet);
    procedure MDRepre3AfterClose(DataSet: TDataSet);
    procedure MDRepre3CalcFields(DataSet: TDataSet);
    procedure MDRepre3FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure mdRepreFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure AutoSizeDBGrid(const xDBGrid: TJvDBGrid);
    procedure btnExcelClick(Sender: TObject);
    procedure btnGerarExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure btnSairPreviaClick(Sender: TObject);
    procedure btnCancelaPreviaClick(Sender: TObject);
    procedure btnCarregarClick(Sender: TObject);
    procedure btnPreviaClick(Sender: TObject);
    procedure carregar;
    Function FindMemo(const Enc: String; Var Texto: TMemo): STRING;
    procedure edManifestoKeyPress(Sender: TObject; var Key: Char);
    procedure edManifestoExit(Sender: TObject);
    procedure btnSalvarPreviaClick(Sender: TObject);
    procedure btnManifestoClick(Sender: TObject);
    procedure btnLimpaFiltroClick(Sender: TObject);
    procedure mdRepreAfterPost(DataSet: TDataSet);
    procedure edFaturaClick(Sender: TObject);
    procedure mdReprepeso_alteradoChange(Sender: TField);
    procedure mdRepreBeforeEdit(DataSet: TDataSet);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mdReprecte_custoChange(Sender: TField);
    procedure ListarArquivos(diretorioInicial, mascara: string;
      listtotaldir: Boolean = false; recursive: Boolean = true);
    procedure mdRepreAfterScroll(DataSet: TDataSet);
    procedure mdRepreFaturaChange(Sender: TField);
    procedure btnOcoClick(Sender: TObject);
    procedure btnBO_2Click(Sender: TObject);
    procedure edFatExit(Sender: TObject);
    procedure calculo;
    procedure edDataExit(Sender: TObject);
    procedure edVctoMExit(Sender: TObject);
    procedure btnBaixaClick(Sender: TObject);
    procedure ACBrMail1AfterMailProcess(Sender: TObject);

  private
  var
    man, escolhido: Integer;
    listtemp2: TStrings;

  var
    km: double;

  var
    lat, lon, fdefaultStyleName: String;

    procedure calculakm;
    procedure gravageo;
    procedure enviaemail;

  public
    { Public declarations }
  end;

var
  frmRelCusto_frete_sim: TfrmRelCusto_frete_sim;
  sqlmaster, filtro, alterou, imprimir: string;
  pesant, vladi: real;

implementation

uses Dados, Menu, funcoes, LiberaPagto, DetalhesCustoFrete_sim,
  CadTabelaCustoFrac,
  CadOcor_fat_pagar, CadOcor_fat_pagar_2, DetalhesCustoFrete_km, UrlMon,
  DetalhesCusto, impressao_custo_frete;

{$R *.dfm}

procedure TfrmRelCusto_frete_sim.ACBrMail1AfterMailProcess(Sender: TObject);
begin
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('update tb_controle_custo set email_retorno = ''S'' ');
  dtmDados.IQuery1.sql.Add
    ('where fatura = :0 and codclifor = :1 ');
  dtmDados.IQuery1.Parameters[0].Value := alltrim(edFatura.text);
  dtmDados.IQuery1.Parameters[1].Value := mdRepreTransp.AsInteger;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.close;
end;

procedure TfrmRelCusto_frete_sim.AutoSizeDBGrid(const xDBGrid: TJvDBGrid);
var
  I, TotalWidht, VarWidth, QtdTotalColuna: Integer;
  xColumn: TColumn;
begin
  // Largura total de todas as colunas antes de redimensionar
  TotalWidht := 0;

  // Como dividir todo o espa�o extra na grade
  VarWidth := 0;

  // Quantas colunas devem ser auto-redimensionamento
  QtdTotalColuna := 0;
  for I := 0 to -1 + xDBGrid.Columns.Count do
  begin
    TotalWidht := TotalWidht + xDBGrid.Columns[I].Width;
    if xDBGrid.Columns[I].Field.Tag > 0 then
      Inc(QtdTotalColuna);
  end;

  // Adiciona 1px para a linha de separador de coluna
  if dgColLines in xDBGrid.Options then
    TotalWidht := TotalWidht + xDBGrid.Columns.Count;

  // Adiciona a largura da coluna indicadora
  if dgIndicator in xDBGrid.Options then
    TotalWidht := TotalWidht + IndicatorWidth;

  // width vale "Left"
  VarWidth := xDBGrid.ClientWidth - TotalWidht;

  // Da mesma forma distribuir VarWidth para todas as colunas auto-resizable
  if QtdTotalColuna > 0 then
    VarWidth := VarWidth div QtdTotalColuna;

  for I := 0 to -1 + xDBGrid.Columns.Count do
  begin
    xColumn := xDBGrid.Columns[I];
    if xColumn.Field.Tag > 0 then
    begin
      xColumn.Width := xColumn.Width + VarWidth;
      if xColumn.Width > 0 then
        xColumn.Width := xColumn.Field.Tag;
    end;
  end;
end;

procedure TfrmRelCusto_frete_sim.btnGerarExcelClick(Sender: TObject);
var
  sarquivo: String;
  Memo1: TStringList;
  I: Integer;
begin
  sarquivo := DirectoryListBox1.Directory + '\Rel_Frete_Sim_' +
    ApCarac(DateToStr(date)) + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for I := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[I].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  mdRepre.First;
  while not mdRepre.Eof do
  begin
    for I := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + mdRepre.fieldbyname
        (JvDBGrid1.Columns[I].FieldName).AsString + '</th>');
    mdRepre.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
  Panel4.Visible := false;
end;

procedure TfrmRelCusto_frete_sim.btnManifestoClick(Sender: TObject);
begin
  Tag := 1;
  mdRepre.filtered := true;
end;

procedure TfrmRelCusto_frete_sim.btnOcoClick(Sender: TObject);
begin
  if mdRepreFatura.Value = '' then
  begin
    showmessage('Sem Fatura N�o � Possivel colocar Ocorr�ncia');
    exit;
  end;
  if not dtmDados.PodeAlterar(name) then
    exit;
  try
    Application.CreateForm(TfrmCadOcor_fat_pagar, frmCadOcor_fat_pagar);
    frmCadOcor_fat_pagar.edFatura.text := mdRepreFatura.AsString;
    frmCadOcor_fat_pagar.Caption :=
      'Cadastro de Ocorr�ncia de Faturas � Pagar - Fatura : ' +
      mdRepreFatura.AsString;
    frmCadOcor_fat_pagar.ShowModal;
  finally
    frmCadOcor_fat_pagar.Free;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;
  Panel1.Caption := 'Enviando e-mail.......';
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('call SP_ocorr_fat_pag()');
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.close;
  Panel1.Visible := false;
  Panel1.Caption := 'Gerando..............';
end;

procedure TfrmRelCusto_frete_sim.btnBaixaClick(Sender: TObject);
var motivo : String;
begin
  if not dtmdados.PodeApagar(name) then
    exit;

  motivo:= Dialogs.InputBox('Justificativa', 'Prompt', 'Default string');

  if Length(motivo) < 10 then
    Showmessage('Favor colocar uma justificativa com no m�nimo 10 caracteres !');

  if (Application.Messagebox
    ('Deseja Baixar Estes Lan�amentos ?', 'Baixar Faturas de Pagamento',
    MB_YESNO + MB_ICONQUESTION) = IDYES) then
  begin
    mdRepre.First;
    While not mdRepre.Eof do
    begin
      if mdReprebaixa.Value = 1 then
      begin
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update tb_controle_custo set status = ''B'', ');
        dtmDados.IQuery1.sql.Add
          ('usuario = :0, dataalt = sysdate, obs = :1, justificativa_baixa = :2 ');
        dtmDados.IQuery1.sql.Add
          ('where nr_doc = :3 and tipo = :4 and codclifor = :5 ');
        dtmDados.IQuery1.sql.Add('and filial = :6 and cte_rec = :7');
        dtmDados.IQuery1.Parameters[0].Value := GLBUSER;
        dtmDados.IQuery1.Parameters[1].Value := 'Baixado do Sistema';
        dtmDados.IQuery1.Parameters[2].Value := motivo;
        dtmDados.IQuery1.Parameters[3].Value := mdRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[4].Value := mdRepreTipo.Value;
        dtmDados.IQuery1.Parameters[5].Value := mdRepreTransp.AsInteger;
        dtmDados.IQuery1.Parameters[6].Value := mdRepreCodfil.AsInteger;
        dtmDados.IQuery1.Parameters[7].Value := mdRepreCodcon.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end;
      mdRepre.Next
    end;
    btRelatorioClick(sender);
  end;
end;

procedure TfrmRelCusto_frete_sim.btnBO_2Click(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    exit;
  try
    Application.CreateForm(TfrmCadOcor_fat_pagar_2, frmCadOcor_fat_pagar_2);
    frmCadOcor_fat_pagar_2.PageControl1.ActivePageIndex := 1;
    frmCadOcor_fat_pagar_2.btnInserirClick(Sender);
    frmCadOcor_fat_pagar_2.edFatura.text := mdRepreFatura.AsString;
    frmCadOcor_fat_pagar_2.ednmtransp.text := QFornecedorNM_FORNECEDOR.AsString;
    frmCadOcor_fat_pagar_2.Tag := StrToInt(ledIdCliente.LookupValue);
    frmCadOcor_fat_pagar_2.edSolucao.Enabled := false;
    frmCadOcor_fat_pagar_2.ShowModal;
  finally
    frmCadOcor_fat_pagar_2.edSolucao.Enabled := true;
    frmCadOcor_fat_pagar_2.Free;
  end;
end;

procedure TfrmRelCusto_frete_sim.btnCancelaPreviaClick(Sender: TObject);
begin
  btnImprimirPrevia.Enabled := false;
  // MDTar.close;
  QRepre.filtered := false;
  Panel5.Visible := false;
  Tag := 0;
end;

procedure TfrmRelCusto_frete_sim.btnCancelarClick(Sender: TObject);
begin
  btnImpr.Enabled := false;
  // MDTar.close;
  QRepre.filtered := false;
  Tag := 0;
  Panel2.Visible := false;
end;

procedure TfrmRelCusto_frete_sim.btnCarregarClick(Sender: TObject);
begin
  if edPrevia.text <> '' then
  begin
    if mdRepre.RecordCount > 0 then
    begin
      if (Application.Messagebox
        ('Existem Dados Carregados, deseja ignora-los ?', 'Carregar Fatura',
        MB_YESNO + MB_ICONQUESTION) = IDYES) then
      begin
        carregar;
      end;
    end
    else
      carregar;
  end;
end;

procedure TfrmRelCusto_frete_sim.btnExcelClick(Sender: TObject);
begin
  Panel4.Visible := true;
end;

procedure TfrmRelCusto_frete_sim.btnimprimirClick(Sender: TObject);
var
  tt, tc, vc: real;
  icount: Integer;
begin
  mdRepre.SortOnFields('DOC;CODCON', false, true);
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    JvDBGrid1.Columns[icount].Title.font.color := clWhite;
    JvDBGrid1.Columns[icount].Title.color := clRed;
  end;
  JvDBGrid1.Columns[0].Title.font.color := clRed;
  JvDBGrid1.Columns[0].Title.color := clRed;
  JvDBGrid1.Columns[4].Title.font.color := clRed;
  JvDBGrid1.Columns[4].Title.color := clWhite;
  tt := 0;
  tc := 0;
  Tag := 0;
  vc := 0;
  mdRepre.filtered := true;
  mdRepre.First;
  while not mdRepre.Eof do
  begin
    if mdRepreCusto_calc.Value = -99 then
      vc := 0
    else
      vc := mdRepreCusto_calc.Value;

    tt := tt + mdReprevalor_cobrado.Value;
    tc := tc + vc;
    mdRepre.Next;
  end;
  edCusto.Value := tt;
  edCalculado.Value := tc;
  if tc > 0 then
  begin
    imprimir := 'S';
    man := 0;
    Application.CreateForm(TfrmImpressao_custofrete, frmImpressao_custofrete);
    frmImpressao_custofrete.QRelatorio.close;
    frmImpressao_custofrete.QRelatorio.Parameters[0].Value := trim(edFatura.Text);
    frmImpressao_custofrete.QRelatorio.Parameters[1].Value := mdRepreTransp.AsInteger;
    frmImpressao_custofrete.QRelatorio.open;
    frmImpressao_custofrete.RLReport1.Preview;
  end;
  imprimir := 'N';
end;

procedure TfrmRelCusto_frete_sim.btnLimpaFiltroClick(Sender: TObject);
begin
  edLocalizar.Clear;
  mdRepre.filtered := false;
  Tag := 0;
end;

procedure TfrmRelCusto_frete_sim.btnPgtoClick(Sender: TObject);
var
  tt, tc, base, sest, inss, vc: real;
  //vcto: TdateTime;
  filial: Integer;
  pfj, xml, fat: String;
begin
  if not dtmDados.PodeInserir(name) then
    exit;
  {
  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add('select * from (select codtar, adidia, ');
  dtmDados.RQuery1.sql.Add('case when adiseg = ''S'' then 2 ');
  dtmDados.RQuery1.sql.Add('when aditer = ''S'' then 3 ');
  dtmDados.RQuery1.sql.Add('when adiqua = ''S'' then 4 ');
  dtmDados.RQuery1.sql.Add('when adiqui = ''S'' then 5 ');
  dtmDados.RQuery1.sql.Add('when adisex = ''S'' then 6 ');
  dtmDados.RQuery1.sql.Add('when adisab = ''S'' then 7 ');
  dtmDados.RQuery1.sql.Add('else 1 end dia ');
  dtmDados.RQuery1.sql.Add
    ('from cyber.rodtar_cfr where situac <> ''I'' and codclifor = :0 ');
  dtmDados.RQuery1.sql.Add('order by 2 desc) where rownum = 1 ');
  dtmDados.RQuery1.Parameters[0].Value := mdRepreTransp.AsInteger;
  dtmDados.RQuery1.open;
  if dtmDados.RQuery1.Eof then
  begin
    showmessage
      ('Fornecedor sem Tarefa TMS Cadastrada, Pedir para Contabilidade !!');
    exit;
  end
  else
  begin
    vcto := dtmDados.RQuery1.fieldbyname('adidia').Value + date;
    dia := dtmDados.RQuery1.fieldbyname('dia').Value;
  end;

  codtar := dtmDados.RQuery1.fieldbyname('codtar').Value;
  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add('select * from cyber.tarrat where codtar = :0');
  dtmDados.RQuery1.Parameters[0].Value := codtar;
  dtmDados.RQuery1.open;
  if dtmDados.RQuery1.Eof then
  begin
    showmessage('Fornecedor sem Classifica��o !!');
    exit;
  end;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add
    ('select fisjur from cyber.rodcli where codclifor = :0');
  dtmDados.RQuery1.Parameters[0].Value := mdRepreTransp.AsInteger;
  dtmDados.RQuery1.open;
  pfj := dtmDados.RQuery1.fieldbyname('fisjur').Value;
  dtmDados.RQuery1.close;

  if DayOfWeek(vcto) = 1 then
    vcto := vcto + 1;

  if DayOfWeek(vcto) = 7 then
    vcto := vcto + 2;

  // verifica se o dia do vencimento � diferente do dia da semana que deve ser
  if DayOfWeek(vcto) <> dia then
  begin
    vcto := vcto + (dia - DayOfWeek(vcto));
  end;

  edVcto.date := vcto; }

  vc := 0;
  tt := 0;
  tc := 0;
  xml := 'S';
  // Mdtar.open;
  Tag := 0;
  mdRepre.filtered := true;
  mdRepre.First;
  filial := mdRepreCodfil.AsInteger;
  fat := mdRepreFatura.AsString;
  while not mdRepre.Eof do
  begin
    if mdReprevalor_cobrado.Value = -99 then
      vc := 0
    else
      vc := mdReprevalor_cobrado.Value;
    tt := tt + vc;
    tc := tc + mdRepreCusto_calc.Value;
    if (mdReprecte_custo.Value > 0) and (mdReprexml.Value = '') then
      xml := 'N';
    if mdRepreCodfil.AsInteger <> filial then
    begin
      showmessage
        ('Existem mais de 1 filial para este pagamento, favor limpar o campo fatura da filial e desmarcar !!');
      exit;
    end;
    if mdRepreFatura.AsString <> fat then
    begin
      showmessage
        ('Existem Lan�amentos de Faturas diferentes para este Pagamento !!');
      exit;
    end;
    mdRepre.Next;
  end;

  // verifica se existe no contas � pagar do Rodopar para transportadoras
  if mdRepreagregado.AsString = 'N' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add('select count(*) tem ');
    dtmDados.IQuery1.sql.Add('from tb_controle_custo cc left join cyber.rodcli f on f.codclifor = cc.codclifor ');
    dtmDados.IQuery1.sql.Add('left join cyber.pagdoc e on e.numdoc = to_char(cc.cte_custo) ');
    dtmDados.IQuery1.sql.Add('left join cyber.rodcli ff on ff.codclifor = e.codclifor ');
    dtmDados.IQuery1.sql.Add('where cc.cte_custo = ' + QuotedStr(mdReprecte_custo.AsString) +
    ' and cc.codclifor = ' + QuotedStr(mdRepreTransp.AsString) + ' and substr(f.codcgc,1,10) = substr(ff.codcgc,1,10) ');
    dtmDados.IQuery1.open;
    if dtmdados.IQuery1.FieldByName('tem').Value = 0 then
    begin
        showmessage
          ('Existem CT-e de Custo que n�o est�o no Contas � Pagar do Rodopar, procure a contabilidade  !!');
        exit;
    end;
    dtmDados.IQuery1.close;
  end;

  Panel2.Visible := true;
  btnSalvar.Enabled := true;
  btnCancelar.Enabled := true;
  edCusto.Value := tt;
  edVlrPrev.Value := tc; // edCalculado.value;
  edCalculado.Value := tc;
  edFatura.text := mdRepreFatura.AsString;
  edDesconto.Value := 0;
  edData.Clear;
  if Trim(ledCliente.LookupValue) <> '' then
    EdHist.text := ledCliente.text;
  Memo1.Clear;
  if pfj = 'F' then
  begin
    base := Arredondar((edVlrPrev.Value * 0.20), 2);
    sest := Arredondar((base * 0.025), 2);
    inss := Arredondar((base * 0.11), 2);
    edImp.Value := sest + inss;
    Memo1.Lines.Add('Sest/Senat = ' + FloatToStrf(sest, ffnumber, 10, 2) +
      ' INSS = ' + FloatToStrf(inss, ffnumber, 10, 2));
  end
  else
  begin
    base := 0;
    sest := 0;
    inss := 0;
    edImp.Value := sest + inss;
  end;
  if edVlrPrev.Value <> edCalculado.Value then
    showmessage
      ('Favor enviar ao TIC esta tela, o valor a pagar est� diferente do valor calculado !!');
  edData.SetFocus;
end;

procedure TfrmRelCusto_frete_sim.btnPreviaClick(Sender: TObject);
begin
  edPrevia.Clear;
  Panel5.Visible := true;
end;

procedure TfrmRelCusto_frete_sim.btnSairClick(Sender: TObject);
begin
  btnImpr.Enabled := false;
  Panel2.Visible := false;
  // MDRepre.Filtered := false;
  mdRepre.close;
  Tag := 0;
end;

procedure TfrmRelCusto_frete_sim.btnSairPreviaClick(Sender: TObject);
begin
  btnImprimirPrevia.Enabled := false;
  Panel5.Visible := false;
  mdRepre.filtered := false;
  Tag := 0;
end;

procedure TfrmRelCusto_frete_sim.btnSalvarClick(Sender: TObject);
var
  ok: Integer;
  //Origem, Destino, caminho: string;
  //a : Real;
begin
  ok := 1;

  if alltrim(edFatura.text) = '' then
  begin
    showmessage('N�o foi digitada a Fatura');
    edFatura.SetFocus;
    exit;
  end;

  if ApCarac(edData.text) = '' then
  begin
    showmessage('N�o foi digitada a Data da Fatura');
    edData.SetFocus;
    exit;
  end;

  if edVlrPrev.Value = 0 then
  begin
    showmessage('N�o Existe Valor desta Fatura ?');
    exit;
  end;

  if (edDesconto.Value > 0) and (cbTipo.text = '') then
  begin
    showmessage('Todo Desconto Precisa de um Tipo !');
    cbTipo.SetFocus;
    exit;
  end;

  if (edDesconto2.Value > 0) and (edDesconto.Value < 0) then
  begin
    showmessage('S� preencha o 2� desconto se o primeiro j� tiver preenchido !');
    cbTipo2.SetFocus;
    exit;
  end;

  if (edDesconto2.Value > 0) and (cbTipo2.text = '') then
  begin
    showmessage('Todo Desconto Precisa de um Tipo !');
    cbTipo2.SetFocus;
    exit;
  end;

  if (edProcesso.Value > 0) and (cbTipo.text = '') then
  begin
    showmessage('Todo Desconto Precisa de um Tipo !');
    cbTipo.SetFocus;
    exit;
  end;

  if (edDesconto.Value + edProcesso.Value = 0) and (cbTipo.text <> '') then
  begin
    showmessage('Qual Desconto para Este Tipo Escolhido ? !');
    edDesconto.SetFocus;
    exit;
  end;

  if (edDesconto.Value > 0) and (Length(Memo1.text) < 10) then
  begin
    showmessage('O Desconto precisa de uma Justificativa !');
    Memo1.SetFocus;
    exit;
  end;

  if (edDesconto2.Value > 0) and (Length(Memo1.text) < 10) then
  begin
    showmessage('O Desconto precisa de uma Justificativa !');
    Memo1.SetFocus;
    exit;
  end;

  if (edProcesso.Value > 0) and (Length(Memo1.text) < 10) then
  begin
    showmessage('O Desconto precisa de uma Justificativa !');
    Memo1.SetFocus;
    exit;
  end;

  if (edDesconto.Value + edDesconto2.Value) > edVlrPrev.Value then
  begin
    showmessage('O Valor do Desconto N�o Pode ser Maior que o Valor da Fatura !');
    edDesconto.SetFocus;
    exit;
  end;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select count(*) qt  from tb_controle_custo ');
  dtmDados.IQuery1.sql.Add('where fatura = :0 and codclifor = :1 ');
  dtmDados.IQuery1.sql.Add('and status = ''P'' ');
  dtmDados.IQuery1.Parameters[0].Value := edFatura.text;
  dtmDados.IQuery1.Parameters[1].Value := mdRepreTransp.AsInteger;
  dtmDados.IQuery1.open;
  if dtmDados.IQuery1.fieldbyname('qt').Value > 0 then
  begin
    showmessage('J� Existe esta Fatura registrada para este Transportador!');
    edFatura.SetFocus;
    dtmDados.IQuery1.close;
    exit;
  end;
  dtmDados.IQuery1.close;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select count(*) qt from cyber.pagdoci ');
  dtmDados.IQuery1.sql.Add('where numdoc = :0 and codclifor = :1 ');
  dtmDados.IQuery1.sql.Add('and situac <> ''C'' ');
  dtmDados.IQuery1.Parameters[0].Value := edFatura.text;
  dtmDados.IQuery1.Parameters[1].Value := mdRepreTransp.AsInteger;
  dtmDados.IQuery1.open;
  if dtmDados.IQuery1.fieldbyname('qt').Value > 0 then
  begin
    showmessage
      ('J� Existe esta Fatura registrada no RODOPAR para este Transportador!');
    edFatura.SetFocus;
    dtmDados.IQuery1.close;
    exit;
  end;
  dtmDados.IQuery1.close;

  if mdRepre.Locate('totfre', 0, []) then
  begin
    if Application.Messagebox('Existe(m) Lcto(s) sem receita, continuar ?',
      'Custo sem Receita', MB_YESNO + MB_ICONQUESTION) = IDYES then
      ok := 1
    else
      ok := 0;
  end;

  if ok = 1 then
  begin
    btnSalvar.Enabled := false;
    btnCancelar.Enabled := false;
    mdRepre.First;
    While not mdRepre.Eof do
    begin
      if mdRepreescol.Value = 1 then
      begin
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update tb_controle_custo set fatura = :0, status = ''P'', ');
        dtmDados.IQuery1.sql.Add
          ('usuario = :1, dataalt = sysdate, obs = :2, custo_calc = :3, ');
        dtmDados.IQuery1.sql.Add
          ('desconto = :4, vl_impostos = :5, tipo_desc = :6, ');
        dtmDados.IQuery1.sql.Add
          ('dt_vcto = :7, processo = :8, dt_fatura = :9, email_retorno = ''E'', ');
        dtmDados.IQuery1.sql.Add
          ('desconto2 = :10, tipo_desc2 = :11 ');
        dtmDados.IQuery1.sql.Add
          ('where nr_doc = :12 and tipo = :13 and codclifor = :14 ');
        dtmDados.IQuery1.sql.Add('and filial = :15 and cte_rec = :16');
        dtmDados.IQuery1.Parameters[0].Value := alltrim(edFatura.text);
        dtmDados.IQuery1.Parameters[1].Value := GLBUSER;
        dtmDados.IQuery1.Parameters[2].Value := Memo1.text;
        dtmDados.IQuery1.Parameters[3].Value := mdRepreCusto_calc.AsFloat;
        dtmDados.IQuery1.Parameters[4].Value := edDesconto.Value;
        dtmDados.IQuery1.Parameters[5].Value := edImp.Value;
        if cbTipo.text <> '' then
          dtmDados.IQuery1.Parameters[6].Value :=
            StrToInt(copy(cbTipo.text, 1, 1))
        else
          dtmDados.IQuery1.Parameters[6].Value := 0;
        dtmDados.IQuery1.Parameters[7].Value := edVcto.date;
        dtmDados.IQuery1.Parameters[8].Value := edProcesso.Value;
        dtmDados.IQuery1.Parameters[9].Value := edData.date;
        dtmDados.IQuery1.Parameters[10].Value := edDesconto2.Value;
        if cbTipo2.text <> '' then
          dtmDados.IQuery1.Parameters[11].Value :=
            StrToInt(copy(cbTipo2.text, 1, 1))
        else
          dtmDados.IQuery1.Parameters[11].Value := 0;
        dtmDados.IQuery1.Parameters[12].Value := mdRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[13].Value := mdRepreTipo.Value;
        dtmDados.IQuery1.Parameters[14].Value := mdRepreTransp.AsInteger;
        dtmDados.IQuery1.Parameters[15].Value := mdRepreCodfil.AsInteger;
        dtmDados.IQuery1.Parameters[16].Value := mdRepreCodcon.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
        // grava o campo Vencimento Manual
        if copy(edVctoM.Text,1,2) <> '  ' then
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add
            ('update tb_controle_custo set dt_vcto_m = :0 ');
          dtmDados.IQuery1.sql.Add
            ('where nr_doc = :1 and tipo = :2 and codclifor = :3 ');
          dtmDados.IQuery1.sql.Add('and filial = :4 and cte_rec = :5');
          dtmDados.IQuery1.Parameters[0].Value := edVctoM.Date;
          dtmDados.IQuery1.Parameters[1].Value := mdRepreDOC.AsInteger;
          dtmDados.IQuery1.Parameters[2].Value := mdRepreTipo.Value;
          dtmDados.IQuery1.Parameters[3].Value := mdRepreTransp.AsInteger;
          dtmDados.IQuery1.Parameters[4].Value := mdRepreCodfil.AsInteger;
          dtmDados.IQuery1.Parameters[5].Value := mdRepreCodcon.AsInteger;
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end
        else
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add
            ('update tb_controle_custo set dt_vcto_m = null ');
          dtmDados.IQuery1.sql.Add
            ('where nr_doc = :0 and tipo = :1 and codclifor = :2 ');
          dtmDados.IQuery1.sql.Add('and filial = :3 and cte_rec = :4');
          dtmDados.IQuery1.Parameters[0].Value := mdRepreDOC.AsInteger;
          dtmDados.IQuery1.Parameters[1].Value := mdRepreTipo.Value;
          dtmDados.IQuery1.Parameters[2].Value := mdRepreTransp.AsInteger;
          dtmDados.IQuery1.Parameters[3].Value := mdRepreCodfil.AsInteger;
          dtmDados.IQuery1.Parameters[4].Value := mdRepreCodcon.AsInteger;
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end;

      end;
      mdRepre.Next
    end;
    // RPP 000183
    SP_Exporta.Parameters[0].Value := edFatura.text;
    SP_Exporta.Parameters[1].Value := mdRepreTransp.AsInteger;
    if copy(edVctoM.Text,1,2) <> '  ' then
      SP_Exporta.Parameters[2].Value := edVctoM.date
    else
      SP_Exporta.Parameters[2].Value := edVcto.date;
    SP_Exporta.Parameters[3].Value := GLBUSER;
    SP_Exporta.ExecProc;
    // Envio do e-mail sobre o faturamento
    enviaemail();
    btnImpr.Enabled := true;
  end
  else
  begin
    btnSairClick(Sender);
  end;
end;

procedure TfrmRelCusto_frete_sim.btnSalvarPreviaClick(Sender: TObject);
var
  filial: Integer;
begin
  btnSalvarPrevia.Enabled := false;
  btnCancelaPrevia.Enabled := false;
  mdRepre.filtered := true;
  mdRepre.First;
  filial := mdRepreCodfil.AsInteger;
  While not mdRepre.Eof do
  begin
    if mdRepreescol.Value = 1 then
    begin
      if mdRepreCodfil.AsInteger <> filial then
      begin
        showmessage
          ('N�o pode ser salva a mesma fatura, para filiais diferentes !!');
        exit;
      end
      else
      begin
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update tb_controle_custo set fatura = :0, custo_calc = :1 ');
        dtmDados.IQuery1.sql.Add
          ('where nr_doc = :2 and tipo = :3 and codclifor = :4 ');
        dtmDados.IQuery1.sql.Add('and filial = :5 and cte_rec = :6');
        dtmDados.IQuery1.Parameters[0].Value := edPrevia.text;
        dtmDados.IQuery1.Parameters[1].Value := mdRepreCusto_calc.AsFloat;
        dtmDados.IQuery1.Parameters[2].Value := mdRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[3].Value := mdRepreTipo.Value;
        dtmDados.IQuery1.Parameters[4].Value := mdRepreTransp.AsInteger;
        dtmDados.IQuery1.Parameters[5].Value := mdRepreCodfil.AsInteger;
        dtmDados.IQuery1.Parameters[6].Value := mdRepreCodcon.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end;
    end;
    mdRepre.Next
  end;
  mdRepre.filtered := false;
  btnImprimirPrevia.Enabled := true;
  btnSalvarPrevia.Enabled := true;
  btnCancelaPrevia.Enabled := true;
end;

procedure TfrmRelCusto_frete_sim.btProcurarClick(Sender: TObject);
var
  Operador, b: String;
  a: Integer;
begin
  Screen.Cursor := crHourGlass;
  if cbOperador.text = 'igual a' then
    Operador := '='
  else if cbOperador.text = 'contendo' then
    Operador := 'LIKE';

  if cbCampo.text = '' then
  begin
    Screen.Cursor := crDefault;
    showmessage('Selecione um campo para procurar');
    cbCampo.SetFocus;
    exit;
  end
  else if cbOperador.text = '' then
  begin
    Screen.Cursor := crDefault;
    showmessage('Selecione um operador');
    cbOperador.SetFocus;
    exit;
  end
  else if edLocalizar.text = '' then
  begin
    Screen.Cursor := crDefault;
    showmessage('Informe um valor para procurar');
    edLocalizar.SetFocus;
    exit;
  end;
  if Operador <> '' then
  begin
    if Operador = 'LIKE' then
    begin
      // NF
      if cbCampo.text = 'NF' then
      begin
        b := FindMemo(edLocalizar.text, MMNF);
        if FindMemo(edLocalizar.text, MMNF) <> '' then
        begin
          a := StrToInt(copy(FindMemo(edLocalizar.text, MMNF), 1, 10));
          if not mdRepre.Locate('codcon', a, []) then
            showmessage('N�o foi localizado nenhum item');
        end;
      end
      else if not mdRepre.Locate(cbFieldName.Items[cbCampo.ItemIndex],
        alltrim(edLocalizar.text), [loPartialKey]) then
        showmessage('N�o foi localizado nenhum item');
    end
    else
    begin
      if not mdRepre.Locate(cbFieldName.Items[cbCampo.ItemIndex],
        edLocalizar.text, []) then
        showmessage('N�o foi localizado nenhum item');
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TfrmRelCusto_frete_sim.btRelatorioClick(Sender: TObject);
var
  icount, recal: Integer;
  Ano, Mes, dia: Word;
  vc: double;
begin
  MMNF.Clear();
  mdRepre.close;
  cbT.Checked := false;
  alterou := '';
  if (edManifesto.text = '') and (edFat.text = '') then
  begin
    if copy(dtInicial.text, 1, 2) = '  ' then
    begin
      showmessage('Voc� n�o escolheu a Data Inicial');
      exit;
    end;

    if copy(dtFinal.text, 1, 2) = '  ' then
    begin
      showmessage('Voc� n�o escolheu a Data Final');
      exit;
    end;

    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  if (Trim(ledIdCliente.LookupValue) = '') and (edManifesto.text = '') then
  begin
    showmessage('Voc� n�o escolheu o Transportador ou Manifesto');
    exit;
  end;

  edCusto.Value := 0;
  edCalculado.Value := 0;

  Panel1.left := trunc((self.width - Panel1.width) / 2); Panel1.top := trunc((self.height - Panel1.height) / 2);
  Panel1.Visible := true;
  Application.ProcessMessages;
  QRepre.close;
  QRepre.sql.Clear;
  QRepre.sql.Add
    ('select distinct v.*, case when v.novo_custo > 0 then v.novo_custo else fnc_custo_transp_sim2(v.codcon, v.transp, v.doc, v.codfil) end custo, ' +
    ' fnc_cod_tabela_sim(v.codcon, v.transp, v.doc, v.codfil) cod_tab, nvl(gen.cte_gen,0) cte_gen ' +
    ' from vw_custo_transp_sim v left join crm_generalidades gen on v.codcon = gen.cte ' +
    ' and v.filial = gen.filial and gen.servico = ''VE�CULO DEDICADO''  and gen.tipo <> ''R'' ');
  if cbFilial.text = 'Todas' then
    QRepre.sql.Add('where v.filial > 0 ')
  else
    QRepre.sql.Add('where v.filial = ' + copy(cbFilial.text, 1,
      pos('-', cbFilial.text) - 1));
  if ((edManifesto.text = '') and (edFat.text = '')) then
    QRepre.sql.Add('and v.data between to_date(''' + dtInicial.text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text +
      ' 23:59'',''dd/mm/yy hh24:mi'')');

  if edManifesto.text <> '' then
  begin
    QRepre.sql.Add(' and v.doc = ' + #39 + edManifesto.text + #39);
    if RgSerie.ItemIndex = 0 then
      QRepre.sql.Add('and v.serie = ''1'' ')
    else if RgSerie.ItemIndex = 1 then
      QRepre.sql.Add('and v.serie = ''U'' ');
  end
  else
    QRepre.sql.Add(' and v.transp = ' + ledIdCliente.LookupValue);

  if RadioGroup1.ItemIndex = 0 then
    QRepre.sql.Add(' and v.status = ''N'' ');

  if edFat.text <> '' then
    QRepre.sql.Add(' and v.fatura = ' + #39 + edFat.text + #39);

  if edCTePagar.text <> '' then
    QRepre.sql.Add(' and v.CTE_CUSTO = ' + #39 + edCTePagar.text + #39);

  QRepre.sql.Add(' order by 2,1,5 ');
  //showmessage(QRepre.SQL.Text);
  QRepre.open;
  if QRepre.RecordCount > 0 then
  begin
    self.Caption := 'Relat�rio de Confer�ncia de Frete de Custos - ' +
      IntToStr(QRepre.RecordCount) + ' CT-es';
    if edManifesto.text <> '' then
    begin
      QFornecedor.Locate('cod_fornecedor', QRepreTRANSP.AsInteger, []);
      ledIdCliente.LookupValue := QRepreTRANSP.AsString;
      ledCliente.LookupValue := QRepreTRANSP.AsString;
    end;
    QRepre.First;
    mdRepre.close;
    mdRepre.open;
    while not QRepre.Eof do
    begin
      recal := 0;
      if QRepreCODCON.AsInteger > 0 then
      begin
        QNF.close;
        QNF.Parameters[0].Value := QRepreCODCON.Value;
        QNF.Parameters[1].Value := QRepreCODFIL.Value;
        QNF.Parameters[2].Value := QRepreCODCON.Value;
        QNF.Parameters[3].Value := QRepreCODFIL.Value;
        QNF.open;
      end;
      mdRepre.Append;
      mdRepreTipo.Value := QRepreTIPO.Value;
      mdRepreDOC.Value := QRepreDOC.AsInteger;
      mdRepreserie.Value := QRepreSERIE.AsString;
      mdRepreData.Value := QRepreDATA.Value;
      mdRepreDestino.Value := QRepreDESTINO.AsString;
      mdReprefl_tde.Value := QRepreFL_TDE.AsString;
      mdReprenovo_custo.Value := QReprenovo_custo.Value;
      mdReprecidade.Value := QReprecidade.AsString;
      mdRepreestado.Value := QRepreestado.AsString;
      mdRepretde.AsString := QRepretde.AsString;
      mdReprecte_gen.AsInteger := QReprecte_gen.AsInteger;
      if QReprecodcmo.Value = 10 then
        mdRepreagregado.Value := 'S'
      else
      begin
        mdRepreagregado.Value := 'N';
        mdReprexml.AsString := QReprexml_custo_fis.AsString;
      end;

      if (QRepreFL_TDE.Value = 'N') and (QRepreSTATUS.Value = 'N') then
      begin
        QImpCte.close;
        QImpCte.Parameters[0].Value := QRepreTRANSP.AsInteger;
        QImpCte.Parameters[1].Value := QRepreCODCON.AsInteger;
        QImpCte.open;
        if not QImpCte.Eof then
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add
            ('select count(*) tem from tb_cte_xml_comp x left join tb_conversao q on q.cnpj = substr(x.emitente,1,8) and x.nome = q.de where para = ''TDE'' and x.valor > 0 and id = '
            + QuotedStr(QImpCteID.AsString));
          dtmDados.IQuery1.open;
          if dtmDados.IQuery1.fieldbyname('tem').Value > 0 then
          begin
            mdReprefl_tde.Value := 'S';
            recal := 1;
          end
          else
            mdReprefl_tde.Value := 'N';
          dtmDados.IQuery1.close;
          if QRepreCTE_CUSTO.IsNull then
          begin
            mdReprecte_custo.Value := QImpCteNUMERO.AsInteger;
            mdReprevalor_cobrado.Value := QImpCteVALOR.Value;
          end
          else
          begin
            mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
            mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
          end;
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add('update tb_cte_xml set fl_pago = ''S'' ');
          dtmDados.IQuery1.sql.Add('where id = ' +
            QuotedStr(QImpCteID.AsString));
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end
        else
        begin
          mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
          mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
        end;
        QImpCte.close;
      end
      else
      begin
        mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
        mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
      end;

      if (QRepreFL_TR.Value = 'N') and (QRepreSTATUS.Value = 'N') then
      begin
        QImpCte.close;
        QImpCte.Parameters[0].Value := QRepreTRANSP.AsInteger;
        QImpCte.Parameters[1].Value := QRepreCODCON.AsInteger;
        QImpCte.open;
        if QRepreCODCON.AsInteger = 514613 then
        showmessage('');

        if not QImpCte.Eof then
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add
            ('select count(*) tem from tb_cte_xml_comp x left join tb_conversao q on q.cnpj = substr(x.emitente,1,8) and x.nome = q.de where para = ''TRT'' and x.valor > 0 and id = '
            + QuotedStr(QImpCteID.AsString));
          dtmDados.IQuery1.open;
          if dtmDados.IQuery1.fieldbyname('tem').Value > 0 then
          begin
            mdReprefl_tr.Value := 'S';
            recal := 1;
          end
          else
            mdReprefl_tr.Value := 'N';
          dtmDados.IQuery1.close;
          if QRepreCTE_CUSTO.IsNull then
          begin
            mdReprecte_custo.Value := QImpCteNUMERO.AsInteger;
            mdReprevalor_cobrado.Value := QImpCteVALOR.Value;
          end
          else
          begin
            mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
            mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
          end;
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add('update tb_cte_xml set fl_pago = ''S'' ');
          dtmDados.IQuery1.sql.Add('where id = ' +
            QuotedStr(QImpCteID.AsString));
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end
        else
        begin
          mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
          mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
        end;
        QImpCte.close;
      end
      else
      begin
        mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
        mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
      end;

      // SINALIZAR SE HOUVER COBRAN�A DE TDE E VE�CULO DEDICADO
      // MELHORIA 52
      if mdReprefl_tde.Value = 'S' then
      begin
        {
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('select COUNT(*) tem from tb_cte_xml c left join cyber.rodcli f on c.emitente = fnc_remove_caracter(f.codcgc) ');
        dtmDados.IQuery1.sql.Add
          ('left join tb_cte_xml_comp p on c.id = p.id and c.emitente = p.emitente ');
        dtmDados.IQuery1.sql.Add
          ('left join tb_conversao q on q.cnpj = substr(c.emitente,1,8) and p.nome = q.de ');
        dtmDados.IQuery1.sql.Add('where c.numero = ' +
          QuotedStr(QRepreCTE_CUSTO.AsString) + ' and f.codclifor = ' +
          QuotedStr(QRepreTRANSP.AsString) +
          ' and para = ''VE�CULO DEDICADO'' ');
        dtmDados.IQuery1.open;
        if dtmDados.IQuery1.fieldbyname('tem').Value > 0 then
          mdRepreDEDICADO.Value := 'S'
        else
          mdRepreDEDICADO.Value := 'N';
        dtmDados.IQuery1.close; }
        QDedicado.Close;
        QDedicado.Parameters[0].Value := QRepreCTE_CUSTO.AsInteger;
        QDedicado.Parameters[1].Value := QRepreTRANSP.AsInteger;
        QDedicado.Open;
        if QDedicadoTEM.AsInteger > 0 then
          mdRepreDEDICADO.Value := 'S'
        else
          mdRepreDEDICADO.Value := 'N';
        QDedicado.close;
      end;

      // mdReprecte_custo.value := QReprecte_custo.AsInteger;
      // mdReprevalor_cobrado.value := QReprevalor_cobrado.value;
      mdReprePlaca.Value := QRepreplaca.Value;
      mdReprevalor_rec.Value := QRepreVALOR_REC.Value;
      mdRepreFilial.Value := QRepreFILIAL.Value;
      mdRepreTransp.Value := QRepreTRANSP.AsInteger;
      mdRepreCodfil.Value := QRepreCODFIL.AsInteger;
      if QRepreFATURA.Value = '' then
      begin
        QDoccob.close;
        QDoccob.Parameters[0].Value := mdReprecte_custo.Value;
        QDoccob.Parameters[1].Value := mdReprevalor_cobrado.Value;
        QDoccob.Parameters[2].Value := QRepreTRANSP.AsInteger;
        QDoccob.open;
        if not QDoccob.Eof then
        begin
          mdRepreFatura.Value := QDoccobFATURA.AsString;
        end;
        QDoccob.close;
      end
      else
      begin
        mdRepreFatura.Value := QRepreFATURA.Value;
      end;

      mdRepreStatus.Value := QRepreSTATUS.Value;
      mdRepreTotfre.Value := QRepreTOTFRE.Value;
      mdRepreVlrmer.Value := QRepreVLRMER.Value;
      mdReprePescal.Value := QReprePESCAL.Value;
      mdReprePESOCUB.Value:= QReprenr_peso_cub.Value;
      mdRepreCodcon.Value := QRepreCODCON.Value;
      mdRepreRazSoc.Value := QRepreRAZSOC.Value;

      //mdReprefl_tr.Value := QRepreFL_TR.Value;
      if QRepreSTATUS.Value = 'P' then
      begin
        if QReprenovo_custo.value > 0 then
          mdRepreCusto_calc.Value := QReprenovo_custo.value
        else
          mdRepreCusto_calc.Value := QRepreCUSTO_CALC.Value;
        mdReprecod_tab.Value := QReprecod_tab_i.AsInteger;
        mdRepreescol.Value := 1;
      end
      else
      begin
        mdRepreCusto_calc.Value := QRepreCUSTO.Value;
        mdReprecod_tab.Value := QRepreCOD_TAB.AsInteger;
      end;
      mdReprepeso_alterado.Value := QReprePESO_ALTERADO.Value;

      //mdReprexml.Value := QRepreXML_CUSTO.Value;
      mdRepreoperacao.Value := QRepreOPERACAO.AsString;
      // por periodo
      if QFornecedorDIAS.Value < 0 then
      begin
        if (QRepreDATA.Value + ((QFornecedorDIAS.Value * -1) -
          (DayOfWeek(QRepreDATA.Value)))) <= date() then
          mdRepreDias.Value := 1
        else
          mdRepreDias.Value := 0;
        // quinzenal
        // if QFornecedorDIAS.Value = (QFornecedorDIAS.Value*-1) then
        // begin
        DecodeDate(QRepreDATA.Value, Ano, Mes, dia);
        if dia <= (QFornecedorDIAS.Value * -1) then
        begin
          DecodeDate(date(), Ano, Mes, dia);
          // 1� quinzena
          if dia >= (QFornecedorDIAS.Value * -1) then
            mdRepreDias.Value := 1
          else
            mdRepreDias.Value := 0;
        end
        else
        begin
          // 2� quinzena;
          DecodeDate(date(), Ano, Mes, dia);
          // 1� quinzena
          if dia <= (QFornecedorDIAS.Value * -1) then
            mdRepreDias.Value := 1
          else
            mdRepreDias.Value := 0;
        end;
        // end;
      end;

      if QFornecedorDIAS.Value >= 0 then
      begin
        if (QRepreDATA.Value + QFornecedorDIAS.Value) <= date() then
          mdRepreDias.Value := 1
        else
          mdRepreDias.Value := 0;
      end;

      mdReprekm.Value := QRepreKM_TOTAL.Value;

      if QRepreSTATUS.Value <> 'P' then
      begin
        if (QRepreVALOR_COBRADO.Value > 0) and (QRepreTOTFRE.Value > 0) then
        begin
          mdRepreMargem.Value := ((QRepreTOTFRE.Value - QRepreVALOR_COBRADO.Value) /
            QRepreTOTFRE.Value) * 100;
        end
        else
        begin
          mdRepreMargem.Value := 0;
        end;

      end
      else
      begin
        if (QRepreVALOR_COBRADO.Value > 0) and (QRepreTOTFRE.Value > 0) then
        begin
          mdRepreMargem.Value := ((QRepreTOTFRE.Value - QRepreVALOR_COBRADO.Value) /
            QRepreTOTFRE.Value) * 100;
        end
        else
        begin
          mdRepreMargem.Value := 0;
        end;
        if QReprenovo_custo.value > 0 then
          mdRepreCusto_calc.Value := QReprenovo_custo.value
        else
          mdRepreCusto_calc.Value := QRepreCUSTO_CALC.AsFloat;
        // edCalculado.Value := edCalculado.Value + QRepreCUSTO_CALC.AsFloat;
      end;

      while not QNF.Eof do
      begin
        if mdReprenf.AsString = '' then
          mdReprenf.AsString := QNFNR_NF.AsString
        else
          mdReprenf.AsString := mdReprenf.AsString + '/' + QNFNR_NF.AsString;
        MMNF.Lines.Add(retzero(mdRepreCodcon.AsString, 10) + QNFNR_NF.AsString);
        QNF.Next;
      end;
      mdRepreobs.AsString := QRepreOBS.AsString;
      mdRepreliberacao.Value := QRepreLIBERACAO.Value;

      // bloqueio quando o servi�o for maior que 60 dias
      if (QRepreDATA.Value <= (date() - 90)) then
      begin
        if not dtmDados.PodeAlterar(name) then
          mdRepreDias.Value := 0;
      end;
      // Pegar Hora Excedente lan�ada pelo CRM
      if QRepreSTATUS.Value = 'N' then
      begin
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('select ((dt_chegada_cd - dt_saida_port) * 1440)-660 tempo from crm_tb_tempo_dest a where tempo_total is not null ');
        dtmDados.IQuery1.sql.Add
          ('and nro_cte = :0 and a.nro_manif = :1 and a.filial = :2 ');
        dtmDados.IQuery1.Parameters[0].Value := QRepreCODCON.AsInteger;
        dtmDados.IQuery1.Parameters[1].Value := QRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[2].Value := QRepreFILIAL.AsInteger;
        dtmDados.IQuery1.open;

        if not dtmDados.IQuery1.Eof then
        begin
          if dtmDados.IQuery1.fieldbyname('tempo').Value > 0 then
          begin
            mdRepreHE.AsInteger := dtmDados.IQuery1.fieldbyname('tempo').Value;
          end
          else
          begin
            mdRepreHE.AsInteger := 0;
          end;
          // grava a quant. de he no banco o valor 0, pois est� negativo e recalcula
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add('update tb_controle_custo set he = :0 ');
          dtmDados.IQuery1.sql.Add
            ('where nr_doc = :1 and codclifor = :2 and cte_rec = :3 and filial = :4');
          dtmDados.IQuery1.Parameters[0].Value := mdRepreHE.AsInteger;
          dtmDados.IQuery1.Parameters[1].Value := QRepreDOC.AsInteger;
          dtmDados.IQuery1.Parameters[2].Value := QRepreTRANSP.AsInteger;
          dtmDados.IQuery1.Parameters[3].Value := QRepreCODCON.AsInteger;
          dtmDados.IQuery1.Parameters[4].Value := QRepreFILIAL.AsInteger;
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;

          QCusto.close;
          QCusto.Parameters[0].Value := mdRepreCodcon.AsInteger;
          QCusto.Parameters[1].Value := mdRepreTransp.AsInteger;
          QCusto.Parameters[2].Value := mdRepreDOC.AsInteger;
          QCusto.Parameters[3].Value := mdRepreCodfil.AsInteger;
          QCusto.open;
          mdRepreCusto_calc.Value := QCustoCUSTO.Value;
        end
        else
          mdRepreHE.AsInteger := 0;
      end
      else
        mdRepreHE.AsInteger := QRepreHE.AsInteger;

      mdReprevl_tde.Value := QReprevl_tde.Value;
      // verifica se este destinat�rio exige Dedidado
      mdReprefl_dedicado.AsString := QRepreDEDICADO.AsString;

      mdRepre.Post;

      if recal = 1 then
        calculo;
      if edManifesto.text <> '' then
      begin
        if (copy(dtInicial.text, 1, 2) = '  ') and
          (dtInicial.date < QRepreDATA.Value) then
          dtInicial.date := QRepreDATA.Value;
        if dtFinal.date <= QRepreDATA.Value then
          dtFinal.date := QRepreDATA.Value;
      end;
      if mdRepreCusto_calc.Value = -99 then
        vc := 0
      else
        vc := mdRepreCusto_calc.Value;
      edCalculado.Value := edCalculado.Value + vc;
      edCusto.Value := edCusto.Value + mdReprevalor_cobrado.Value;

      QRepre.Next;
    end;
  end;

  Panel1.Visible := false;
  btnPgto.Enabled := true;
  sqlmaster := QRepre.sql.text;

  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    JvDBGrid1.Columns[icount].Title.font.color := clWhite;
    JvDBGrid1.Columns[icount].Title.color := clRed;
  end;
  JvDBGrid1.Columns[0].Title.font.color := clRed;
  JvDBGrid1.Columns[0].Title.color := clRed;
  JvDBGrid1.Columns[1].Title.font.color := clRed;
  JvDBGrid1.Columns[1].Title.color := clWhite;
  // marca como alterado para permitir a mudan�a do peso alterado
  alterou := 'S';
end;

procedure TfrmRelCusto_frete_sim.btRetirarFiltroClick(Sender: TObject);
begin
  cbCampo.ItemIndex := -1;
  edLocalizar.Clear;
  cbOperador.ItemIndex := -1;
  btProcurar.Enabled := true;
  filtro := '';
end;

procedure TfrmRelCusto_frete_sim.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a, b: String;
  I, x, j, k: Integer;
  //Sec: TTime;
  t: double;
  orig1, dest1: String;
begin
  x := 0;
  j := 0;
  t := 0;
  // pegar o cep da filial como ponto de partida
  if frmRelCusto_frete_sim.mdRepreTipo.Value = 'Romaneio' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add
      ('select (d.latitu ||''+''|| d.longit) cep from cyber.tb_coleta f left join cyber.rodcli d on d.codclifor = f.remetente ');
    dtmDados.IQuery1.sql.Add
      ('left join cyber.rodmun m on d.codmun = m.codmun where f.nr_oc = :0 ');
    dtmDados.IQuery1.Parameters[0].Value :=
      frmRelCusto_frete_sim.mdRepreDOC.AsInteger; // glbfilial;
    dtmDados.IQuery1.open;
    cep := dtmDados.IQuery1.fieldbyname('cep').Value;
    dtmDados.IQuery1.close;
  end
  else
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add
      ('select (latitu ||''+''|| longit) cep from cyber.rodfil where codfil = :0 ');
    dtmDados.IQuery1.Parameters[0].Value := mdRepreFilial.AsInteger;
    // glbfilial;
    dtmDados.IQuery1.open;
    cep := dtmDados.IQuery1.fieldbyname('cep').Value;
    dtmDados.IQuery1.close;
  end;

  if QKM.RecordCount > 0 then
  begin
    while not QKM.Eof do
    begin
      if x = 0 then
      begin
        orig := cep;
        orig1 := orig;
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        dest1 := dest;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest; // ApCarac(QKMCODCEP.AsString)+'+'+QKMDESCRI.AsString;
      end;

      XMLFileName := 'C:\sim\temp.xml';

      URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

      XMLDoc.FileName := 'C:\sim\temp.xml';
      XMLDoc.Active := true;
      I := 0;
      j := 0;
      k := 0;
      for I := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[I].NodeName;
        if a = 'row' then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[I]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j]
              .ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
                [k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
                [k].NodeName = 'distance' then
              begin
                // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
                // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

                km := sonumero(XMLDoc.DocumentElement.ChildNodes[I].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].text) + km;
                // edDistancia.Text := FloatToStr(t);

              end;
              b := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
                ['status'].text;
            end;
          end;
        end;
      end;
      // Memo1.Lines.Add(edit1.Text);
      // Memo1.Lines.Add('-----------------');
      XMLDoc.Active := false;
      x := x + 1;
      QKM.Next;
    end;
    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
    XMLFileName := 'C:\sim\temp.xml';
    URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

    XMLDoc.FileName := 'C:\sim\temp.xml';
    XMLDoc.Active := true;
    I := 0;
    j := 0;
    k := 0;
    for I := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
    begin
      a := XMLDoc.DocumentElement.ChildNodes[I].NodeName;
      if a = 'row' then
      begin
        for j := 0 to (XMLDoc.DocumentElement.ChildNodes[I]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].NodeName;
          for k := 0 to (XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
              [k].NodeName;
            if XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k]
              .NodeName = 'distance' then
            begin
              // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
              // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

              km := sonumero(XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j]
                .ChildNodes[k].ChildNodes['text'].text) + km;
              // edDistancia.Text := FloatToStr(t);

            end;
            b := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
              ['status'].text;
          end;
        end;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

procedure TfrmRelCusto_frete_sim.calculo;
var
  vc: double;
begin
  if mdRepreCusto_calc.Value = -99 then
    vc := 0
  else
    vc := mdRepreCusto_calc.Value;

  edCalculado.Value := edCalculado.Value - vc;

  SPRel.Parameters[0].Value := mdReprecte_custo.AsInteger;
  SPRel.Parameters[1].Value := mdReprevalor_cobrado.AsFloat;
  SPRel.Parameters[2].Value := mdRepreFatura.Value;
  SPRel.Parameters[3].Value := mdRepreDOC.AsInteger;
  SPRel.Parameters[4].Value := mdRepreTipo.Value;
  SPRel.Parameters[5].Value := mdRepreTransp.AsInteger;
  SPRel.Parameters[6].Value := mdRepreCodfil.AsInteger;
  SPRel.Parameters[7].Value := mdRepreCodcon.AsInteger;
  SPRel.Parameters[8].Value := mdRepreCusto_calc.AsFloat;
  SPRel.Parameters[9].Value := GLBUSER;
  SPRel.Parameters[10].Value := 1;
  SPRel.Parameters[11].Value := mdReprepeso_alterado.AsFloat;
  if (mdReprefl_tde.AsString = 'S') or (mdReprefl_tde.AsString = 'N') then
    SPRel.Parameters[12].Value := mdReprefl_tde.AsString;

  if (mdReprefl_tr.AsString = 'S') or (mdReprefl_tr.AsString = 'N') then
    SPRel.Parameters[13].Value := mdReprefl_tr.AsString;
  SPRel.Parameters[14].Value := mdReprecod_tab.AsInteger;
  SPRel.ExecProc;

  QCusto.close;
  QCusto.Parameters[0].Value := mdRepreCodcon.AsInteger;
  QCusto.Parameters[1].Value := mdRepreTransp.AsInteger;
  QCusto.Parameters[2].Value := mdRepreDOC.AsInteger;
  QCusto.Parameters[3].Value := mdRepreCodfil.AsInteger;
  QCusto.open;
  mdRepre.edit;
  mdRepreCusto_calc.Value := QCustoCUSTO.Value;
  if QCustoCUSTO.Value > 0 then
    mdRepreMargem.Value := ((mdRepreTotfre.Value - QCustoCUSTO.Value) /
      mdRepreTotfre.Value) * 100
  else
    mdRepreMargem.Value := 0;
  edCalculado.Value := edCalculado.Value + vc;
  mdRepre.Post
end;

procedure TfrmRelCusto_frete_sim.carregar;
var
  dt1, dt2: TdateTime;
  vc: double;
begin
  alterou := '';
  MMNF.Clear();
  edCusto.Value := 0;
  edCalculado.Value := 0;
  vc := 0;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QRepre.close;
  QRepre.sql.Clear;
  QRepre.sql.Add
    ('select distinct v.*, case when v.novo_custo > 0 then v.novo_custo else fnc_custo_transp_sim2(v.codcon, v.transp, v.doc, v.codfil) end custo, ' +
    ' fnc_cod_tabela_sim(v.codcon, v.transp, v.doc, v.codfil) cod_tab, nvl(gen.cte_gen,0) cte_gen ' +
    ' from vw_custo_transp_sim v left join crm_generalidades gen on v.codcon = gen.cte ' +
    ' and v.filial = gen.filial and gen.servico = ''VE�CULO DEDICADO''  and gen.tipo <> ''R'' ');
  QRepre.sql.Add('where v.fatura = ' + #39 + edPrevia.text + #39);
  QRepre.sql.Add(' and (v.status <> ''P'' or v.status is null) ');
  QRepre.open;
  dt1 := StrToDate('01/01/2050');
  dt2 := StrToDate('01/01/1900');
  if QRepre.RecordCount > 0 then
  begin
    QRepre.First;
    mdRepre.close;
    mdRepre.open;
    while not QRepre.Eof do
    begin
      if dt1 > QRepreDATA.Value then
        dt1 := QRepreDATA.Value;
      if dt2 < QRepreDATA.Value then
        dt2 := QRepreDATA.Value;
      mdRepre.Append;
      mdRepreTipo.Value := QRepreTIPO.Value;
      mdRepreDOC.Value := QRepreDOC.AsInteger;
      mdRepreData.Value := QRepreDATA.Value;
      mdRepreDestino.Value := QRepreDESTINO.Value;
      mdReprecte_custo.Value := QRepreCTE_CUSTO.AsInteger;
      mdReprevalor_cobrado.Value := QRepreVALOR_COBRADO.Value;
      mdReprevalor_rec.Value := QRepreVALOR_REC.Value;
      mdRepreTransp.Value := QRepreTRANSP.AsInteger;
      mdRepreFilial.Value := QRepreFILIAL.Value;
      mdRepreCodfil.Value := QRepreCODFIL.AsInteger;
      mdRepreFatura.Value := QRepreFATURA.Value;
      mdRepreStatus.Value := QRepreSTATUS.Value;
      mdRepreTotfre.Value := QRepreTOTFRE.Value;
      mdRepreVlrmer.Value := QRepreVLRMER.Value;
      mdReprePescal.Value := QReprePESCAL.Value;
      mdReprePESOCUB.Value:= QReprenr_peso_cub.Value;
      mdRepreCodcon.Value := QRepreCODCON.Value;
      mdRepreRazSoc.Value := QRepreRAZSOC.Value;
      mdReprefl_tde.Value := QRepreFL_TDE.Value;
      mdReprefl_tr.Value := QRepreFL_TR.Value;
      mdRepreCusto_calc.Value := QRepreCUSTO.AsFloat;
      mdReprepeso_alterado.Value := QReprePESO_ALTERADO.Value;
      mdReprecod_tab.Value := QRepreCOD_TAB.AsInteger;
      mdRepretde.AsString := QRepreTDE.AsString;
      mdReprecidade.Value := QReprecidade.Value;
      mdRepreestado.Value := QRepreestado.Value;
      mdRepreliberacao.AsString := QRepreLIBERACAO.AsString;

      if QReprecodcmo.Value = 10 then
        mdRepreagregado.Value := 'S'
      else
      begin
        mdRepreagregado.Value := 'N';
        mdReprexml.AsString := QReprexml_custo_fis.AsString;
      end;

      if QRepreSTATUS.Value <> 'P' then
      begin
        if (QRepreCUSTO.Value > 0) and (QRepreTOTFRE.Value > 0) then
        begin
          mdRepreMargem.Value := ((QRepreTOTFRE.Value - QRepreCUSTO.Value) /
            QRepreTOTFRE.Value) * 100;
        end
        else
        begin
          mdRepreMargem.Value := 0;
        end;
      end
      else
      begin
        if (QRepreCUSTO.Value > 0) and (QRepreTOTFRE.Value > 0) then
        begin
          mdRepreMargem.Value := ((QRepreTOTFRE.Value - QRepreCUSTO.Value) /
            QRepreTOTFRE.Value) * 100;
        end
        else
        begin
          mdRepreMargem.Value := 0;
        end;
        if QReprenovo_custo.value > 0 then
          mdRepreCusto_calc.Value := QReprenovo_custo.value
        else
          mdRepreCusto_calc.Value := QRepreCUSTO_CALC.AsFloat;
      end;

      mdRepreobs.Value := QRepreOBS.Value;
      mdRepreliberacao.Value := QRepreLIBERACAO.Value;
      QFornecedor.Locate('cod_fornecedor', QRepreTRANSP.AsInteger, []);
      ledIdCliente.LookupValue := QRepreTRANSP.AsString;
      ledCliente.LookupValue := QRepreTRANSP.AsString;
      // por periodo
      if QFornecedorDIAS.Value < 0 then
      begin
        if (QRepreDATA.Value + ((QFornecedorDIAS.Value * -1) -
          (DayOfWeek(QRepreDATA.Value)))) <= date() then
          mdRepreDias.Value := 1
        else
        begin
          mdRepreDias.Value := 0;
          mdRepreescol.Value := 0;
        end;
      end;
      if QFornecedorDIAS.Value >= 0 then
      begin
        if (QRepreDATA.Value + QFornecedorDIAS.Value) <= date() then
          mdRepreDias.Value := 1
        else
        begin
          mdRepreDias.Value := 0;
          mdRepreescol.Value := 0;
        end;
      end;

      // bloqueio quando o servi�o for maior que 60 dias
      if (QRepreDATA.Value <= (date() - 90)) then
      begin
        if not dtmDados.PodeAlterar(name) then
          mdRepreDias.Value := 0
        else
        begin
          mdRepreDias.Value := 1;
          mdRepreescol.Value := 1;
        end;
      end
      else
        mdRepreescol.Value := 1;

      // verifica se tem documento fiscal para o transportador
      if (mdRepreagregado.AsString = 'N') then
      begin
        if (not mdReprexml.IsNull) then
          mdRepreescol.Value := 1
        else
          mdRepreescol.Value := 0;
      end
      else
      begin
        // liberado pois � agregado
        mdRepreescol.Value := 1;
      end;

      // Pegar Hora Excedente lan�ada pelo CRM
      if QRepreSTATUS.Value = 'N' then
      begin
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('select ((dt_chegada_cd - dt_saida_port) * 1440)-660 tempo from crm_tb_tempo_dest a where tempo_total is not null ');
        dtmDados.IQuery1.sql.Add
          ('and nro_cte = :0 and a.nro_manif = :1 and a.filial = :2 ');
        dtmDados.IQuery1.Parameters[0].Value := QRepreCODCON.AsInteger;
        dtmDados.IQuery1.Parameters[1].Value := QRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[2].Value := QRepreFILIAL.AsInteger;
        dtmDados.IQuery1.open;
        if not dtmDados.IQuery1.Eof then
        begin
          if dtmDados.IQuery1.fieldbyname('tempo').Value > 0 then
          begin
            mdRepreHE.AsInteger := dtmDados.IQuery1.fieldbyname('tempo').Value;
            // grava a quant. de he no banco
            dtmDados.IQuery1.close;
            dtmDados.IQuery1.sql.Clear;
            dtmDados.IQuery1.sql.Add('update tb_controle_custo set he = :0 ');
            dtmDados.IQuery1.sql.Add
              ('where nr_doc = :1 and codclifor = :2 and cte_rec = :3 and filial = :4');
            dtmDados.IQuery1.Parameters[0].Value := mdRepreHE.AsInteger;
            dtmDados.IQuery1.Parameters[1].Value := QRepreDOC.AsInteger;
            dtmDados.IQuery1.Parameters[2].Value := QRepreTRANSP.AsInteger;
            dtmDados.IQuery1.Parameters[3].Value := QRepreCODCON.AsInteger;
            dtmDados.IQuery1.Parameters[4].Value := QRepreFILIAL.AsInteger;
            dtmDados.IQuery1.ExecSQL;
            dtmDados.IQuery1.close;

            QCusto.close;
            QCusto.Parameters[0].Value := mdRepreCodcon.AsInteger;
            QCusto.Parameters[1].Value := mdRepreTransp.AsInteger;
            QCusto.Parameters[2].Value := mdRepreDOC.AsInteger;
            QCusto.Parameters[3].Value := mdRepreCodfil.AsInteger;
            QCusto.open;
            mdRepreCusto_calc.Value := QCustoCUSTO.Value;
          end;
        end;
      end
      else
        mdRepreHE.AsInteger := QRepreHE.AsInteger;

      if mdReprevalor_cobrado.Value > mdRepreCusto_calc.Value then
        mdRepreescol.Value := 0;

      if mdRepreCusto_calc.Value = -99 then
        mdRepreescol.Value := 0;

      if not mdRepreliberacao.IsNull then
        mdRepreescol.Value := 1;

      if QRepreCUSTO_CALC.AsFloat = -99 then
        vc := 0
      else
        vc := mdRepreCusto_calc.Value;

      edCalculado.Value := edCalculado.Value + vc;
      mdReprevl_tde.Value := QReprevl_tde.Value;

      mdRepre.Post;
      edCusto.Value := edCusto.Value + QRepreVALOR_COBRADO.Value;
      QRepre.Next;
    end;
    Panel5.Visible := false;
    Panel1.Visible := false;
    btnPgto.Enabled := true;
    sqlmaster := QRepre.sql.text;

    dtInicial.date := dt1;
    dtFinal.date := dt2;
  end
  else
  begin
    Panel1.Visible := false;
    showmessage('N�o encontrado Pr�via desta Fatura');
  end;
  // marca como alterado para permitir a mudan�a do peso alterado
  alterou := 'S';
end;

procedure TfrmRelCusto_frete_sim.cbCampoChange(Sender: TObject);
begin
  filtro := '';
  btProcurar.Enabled := true;
end;

procedure TfrmRelCusto_frete_sim.cbOperadorExit(Sender: TObject);
begin
  if (cbCampo.ItemIndex = 10) and (cbOperador.text = 'contendo') then
  begin
    showmessage('Para campo de valor este operador n�o � v�lido !');
    cbOperador.SetFocus;
  end;
end;

procedure TfrmRelCusto_frete_sim.cbTClick(Sender: TObject);
var
  vc: double;
begin
  if not dtmDados.PodeInserir(name) then
    exit;
  edCusto.Value := 0;
  edCalculado.Value := 0;
  Application.ProcessMessages;
  vc := 0;
  escolhido := 0;
  if cbT.Checked then
  begin
    mdRepre.First;
    while not mdRepre.Eof do
    begin
      if mdRepreCusto_calc.Value = -99 then
        vc := 0
      else
        vc := mdRepreCusto_calc.Value;
      if mdRepreStatus.Value = 'N' then
      begin
        if mdRepreDias.Value = 1 then
        begin
          if (mdReprevalor_cobrado.Value <= mdRepreCusto_calc.Value) then
          begin
            mdRepre.edit;
            mdRepreescol.Value := 1;
            mdRepre.Post;
            edCusto.Value := edCusto.Value + mdReprevalor_cobrado.Value;
            edCalculado.Value := edCalculado.Value + vc;
            escolhido := escolhido + 1;
            lblescolhido.Caption := 'Escolhido : ' + IntToStr(escolhido);
            Application.ProcessMessages;
          end
          else if not mdRepreliberacao.IsNull then
          begin
            mdRepre.edit;
            mdRepreescol.Value := 1;
            mdRepre.Post;
            edCusto.Value := edCusto.Value + mdReprevalor_cobrado.Value;
            edCalculado.Value := edCalculado.Value + vc;
            escolhido := escolhido + 1;
            lblescolhido.Caption := 'Escolhido : ' + IntToStr(escolhido);
            Application.ProcessMessages;
          end;
          mdRepre.Next;
        end;
      end;
    end;
  end
  else
  begin
    mdRepre.First;
    while not mdRepre.Eof do
    begin
      if mdRepreCusto_calc.Value = -99 then
        vc := 0
      else
        vc := mdRepreCusto_calc.Value;
      mdRepre.edit;
      mdRepreescol.Value := 0;
      edCusto.Value := edCusto.Value + mdReprevalor_cobrado.Value;
      edCalculado.Value := edCalculado.Value + vc;
      Application.ProcessMessages;
      mdRepre.Post;
      mdRepre.Next;
    end;
    lblescolhido.Caption := 'Escolhido : ' + IntToStr(escolhido);
  end;
end;

procedure TfrmRelCusto_frete_sim.edCtrcEnter(Sender: TObject);
begin
  dtInicial.text := '';
  dtFinal.text := '';
end;

procedure TfrmRelCusto_frete_sim.edDataExit(Sender: TObject);
var
  vcto : TdateTime;
  codtar, dia : integer;
  pfj : String;
begin
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select * from (select codtar, adidia, ');
  dtmdados.RQuery1.SQL.add('case when adiseg = ''S'' then 2 ');
  dtmdados.RQuery1.SQL.add('when aditer = ''S'' then 3 ');
  dtmdados.RQuery1.SQL.add('when adiqua = ''S'' then 4 ');
  dtmdados.RQuery1.SQL.add('when adiqui = ''S'' then 5 ');
  dtmdados.RQuery1.SQL.add('when adisex = ''S'' then 6 ');
  dtmdados.RQuery1.SQL.add('when adisab = ''S'' then 7 ');
  dtmdados.RQuery1.SQL.add('else 1 end dia ');
  dtmdados.RQuery1.SQL.add('from cyber.rodtar_cfr where situac <> ''I'' and codclifor = :0 ');
  dtmdados.RQuery1.SQL.add('order by 2 desc) where rownum = 1 ');
  dtmdados.RQuery1.Parameters[0].value := mdRepreTransp.AsInteger;
  dtmdados.RQuery1.open;
  if dtmdados.RQuery1.eof then
  begin
    ShowMessage('Fornecedor sem Cadastro de Vencimentos, Pedir o cadastro no ERP !!');
    exit;
  end
  else
  begin
    vcto := dtmdados.RQuery1.FieldByName('adidia').value + edData.Date;
    dia  := dtmdados.RQuery1.FieldByName('dia').value;
  end;

  codtar := dtmdados.RQuery1.FieldByName('codtar').value;
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select * from cyber.tarrat where codtar = :0');
  dtmdados.RQuery1.Parameters[0].value := codtar;
  dtmdados.RQuery1.open;
  if dtmdados.RQuery1.eof then
  begin
    ShowMessage('Fornecedor sem Classifica��o !!');
    exit;
  end;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.SQL.add('select fisjur from cyber.rodcli where codclifor = :0');
  dtmdados.RQuery1.Parameters[0].value := mdRepreTransp.AsInteger;
  dtmdados.RQuery1.open;
  pfj := dtmdados.RQuery1.FieldByName('fisjur').value;
  dtmdados.RQuery1.close;

  if DayOfWeek(vcto) = 1 then
    vcto := vcto + 1;

  if DayOfWeek(vcto) = 7 then
    vcto := vcto + 2;

  // verifica se o dia do vencimento � diferente do dia da semana que deve ser
  if DayOfWeek(vcto) <> dia then
    vcto := vcto + (dia - DayOfWeek(vcto));

  edVcto.Date := vcto;
  if edData.date > date() then
  begin
    showmessage('Data de Emiss�o Superior A Data de Hoje !!');
    edData.SetFocus;
  end;
end;

procedure TfrmRelCusto_frete_sim.edFatExit(Sender: TObject);
begin
  if edFat.text <> '' then
    RadioGroup1.ItemIndex := 1;
end;

procedure TfrmRelCusto_frete_sim.edFaturaClick(Sender: TObject);
begin
  dtmDados.Query1.sql.Clear;
  dtmDados.Query1.sql.Add
    ('select * from tb_manifesto where fatura = :0 and cod_transp = :1');
  dtmDados.Query1.Parameters[0].Value := edPrevia.text;
  dtmDados.Query1.Parameters[1].Value := QFornecedorCOD_FORNECEDOR.AsInteger;
  dtmDados.Query1.open;
  if not dtmDados.Query1.Eof then
  begin
    showmessage('J� existe esta Fatura para este Fornecedor');
    edFatura.SetFocus;
    exit;
  end
  else
  begin
    dtmDados.Query1.sql.Clear;
    dtmDados.Query1.sql.Add
      ('select * from tb_tarefas where fatura = :0 and cod_terc_ta = :1');
    dtmDados.Query1.Parameters[0].Value := edPrevia.text;
    dtmDados.Query1.Parameters[1].Value := QFornecedorCOD_FORNECEDOR.AsInteger;
    dtmDados.Query1.open;
    if not dtmDados.Query1.Eof then
    begin
      showmessage('J� existe esta Fatura para este Fornecedor');
      edFatura.SetFocus;
      exit;
    end;
  end;
end;

procedure TfrmRelCusto_frete_sim.edManifestoExit(Sender: TObject);
begin
  if edManifesto.text <> '' then
  begin
    ledIdCliente.Clear;
    ledCliente.Clear;
    dtInicial.Clear;
    dtFinal.Clear;
  end;
end;

procedure TfrmRelCusto_frete_sim.edManifestoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(Key in ['0' .. '9', #8]) then
    Key := #0;
end;

procedure TfrmRelCusto_frete_sim.edVctoMExit(Sender: TObject);
begin
  if ApCarac(edVctoM.text) <> '' then
  begin
    if edVctoM.Date <= edVcto.Date then
    begin
      ShowMessage('O Vencimento Manual s� Pode Ser Maior que a Data Informada !!');
      edVctoM.Clear;
    end;
  end;
end;

procedure TfrmRelCusto_frete_sim.enviaemail;
var
  v_email, texto: string;
  i: Integer;
begin
  mensagem.Lines.Clear;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select distinct email from tb_cadmensageiro where servico = ''JOB - SP_ENVIO_FATURA'' ');
  dtmDados.IQuery1.open;
  v_email := dtmDados.IQuery1.FieldByName('email').AsString;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select email from cyber.RODCTC e ');
  dtmDados.IQuery1.sql.Add('where e.CODCLIFOR = :0 and e.situac = ''A'' ');
  dtmDados.IQuery1.Parameters[0].Value := mdRepreTransp.AsInteger;
  dtmDados.IQuery1.open;
  i := 0;
  while not dtmDados.IQuery1.eof do
  begin
    ACBrMail1.AddAddress(dtmDados.IQuery1.FieldByName('email').AsString);
    if i = 0 then
    begin
      ACBrMail1.AddCC(v_email);
      i := i + 1;
    end;
    dtmDados.IQuery1.next;
  end;
  dtmDados.IQuery1.close;
  Qemail.Close;
  Qemail.Parameters[0].Value := alltrim(edFatura.text);
  Qemail.Parameters[1].Value := mdRepreTransp.AsInteger;
  Qemail.open;
  mensagem.Lines.add('<html xmlns=http://www.w3.org/1999/xhtml><head>');
  mensagem.Lines.add('<meta http-equiv=Content-Type content=text/html; charset=iso-8859-1><title></title></head><body><table border=1><tr>');
  mensagem.Lines.add('<th><FONT class=titulo1>FATURA : ' + alltrim(edFatura.text) + ' </font></th></tr><tr>');
  mensagem.Lines.add('<th>TRANSPORTADORA : <FONT class=texto1>'+ QemailRAZSOC.AsString+'</font></th></tr><tr>');
  mensagem.Lines.add('<th>Data Emiss&atilde;o : <FONT class=texto1>'+ QemailDATAALT.AsString + '</font></th></tr><tr>');
  mensagem.Lines.add('<th>Valor : <FONT class=texto1>' + format('%10.2n',[QemailTotal.AsFloat]) +'</font></th></tr><tr>');
  mensagem.Lines.add('<th>Programada para Pagamento na Data de : <FONT class=texto1>'+ QemailDT_VCTO.AsString +'</font></th></tr>');
  mensagem.Lines.add('</table></body></html>');
  Qemail.Close;
  ACBrMail1.Body.Assign(mensagem.Lines);
  ACBrMail1.Send(false);
end;

procedure TfrmRelCusto_frete_sim.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  // MDtar.close;
  mdRepre.close;
  QFornecedor.close;
  QRepre.close;
  QSite.close;
end;

procedure TfrmRelCusto_frete_sim.FormCreate(Sender: TObject);
begin
  if Assigned(TStyleManager.ActiveStyle) then
    fdefaultStyleName := TStyleManager.ActiveStyle.Name;

  // self.Height := Ajustaaltura(self.Height);
  Panel1.Left := trunc((self.Width / 2) - (Panel1.Width / 2));
  Panel1.Top := trunc((self.Height / 2) - (Panel1.Height / 2));
  QFornecedor.open;
  QSite.open;
  cbFilial.Clear;
  cbFilial.Items.Add('Todas');
  while not QSite.Eof do
  begin
    cbFilial.Items.Add(QSiteFILIAL.AsString);
    QSite.Next;
  end;
  QSite.Locate('codfil', GLBFilial, []);
  cbFilial.ItemIndex := cbFilial.Items.IndexOf(QSiteFILIAL.AsString);
  filtro := 'S';
  alterou := '';
  escolhido := 0;

  if not dtmdados.PodeApagar(name) then
  begin
    JvDBGrid1.Columns[1].Visible := false;
  end
  else
  begin
    JvDBGrid1.Columns[1].Visible := true;
  end;

end;

procedure TfrmRelCusto_frete_sim.gravageo;
var
  XMLUrl, dest, XMLFileName, a: String;
  I, j, k: Integer;
  //t: double;
begin
  dest := ApCarac(QKMDESCRI.AsString);
  XMLUrl := 'https://maps.google.com/maps/api/geocode/xml?sensor=false&address='
    + QKMDESCRI.AsString + '+' + QKMCODCEP.AsString + '+BR' +
    '&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
  XMLFileName := 'C:\sim\geo.xml';

  URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

  XMLDoc.FileName := 'C:\sim\geo.xml';
  XMLDoc.Active := true;
  I := 0;
  j := 0;
  k := 0;
  for I := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
  begin
    a := XMLDoc.DocumentElement.ChildNodes[I].NodeName;
    if a = 'result' then
    begin
      for j := 0 to (XMLDoc.DocumentElement.ChildNodes[I]
        .ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].NodeName;
        for k := 0 to (XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
            [k].NodeName;
          if XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k]
            .NodeName = 'location' then
          begin
            lat := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
              [k].ChildNodes['lat'].text;
            lon := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes
              [k].ChildNodes['lng'].text;
          end;
        end;
      end;
    end;
  end;
  XMLDoc.Active := false;
end;

procedure TfrmRelCusto_frete_sim.JvDBGrid1CellClick(Column: TColumn);
var
  d1, d2: string;
  vc: double;
  liberado : Integer;
  docfiscal : Integer;
begin
  liberado := 0;
  docfiscal := 0;
  if mdRepreDias.Value = 1 then
  begin
    d1 := formatdatetime('dd/mm/yy', dtInicial.date); // + ' 00:00:01';
    d2 := formatdatetime('dd/mm/yy', dtFinal.date); // + ' 23:59:59';

    if not dtmDados.PodeInserir(name) then
      exit;

    if mdRepreCusto_calc.Value = -99 then
      vc := 0
    else
      vc := mdRepreCusto_calc.Value;

    if (Column.Field = mdReprebaixa) then
    begin
      if (mdRepreStatus.Value = 'N') then
      begin
        mdRepre.edit;
        if (mdReprebaixa.IsNull) or (mdReprebaixa.Value = 0) then
        begin
          mdReprebaixa.Value := 1;
          Escolhido := Escolhido + 1;
          lblEscolhido.Caption := 'Escolhido : ' + IntToStr(Escolhido);
        end
        else
        begin
          mdReprebaixa.Value := 0;
          Escolhido := Escolhido - 1;
          lblEscolhido.Caption := 'Escolhido : ' + IntToStr(Escolhido);
        end;
        mdRepre.Post;
      end;
    end;

    if (Column.Field = mdRepreescol) then
    begin

      if (mdRepreagregado.AsString = 'N') then
      begin
        if (not mdReprexml.IsNull) then
          docfiscal := 1
        else
        begin
          docfiscal := 0;
          showmessage('O XML do Transportador n�o est� no Fiscal, n�o sendo permitido a libera��o');
          exit;
        end;
      end
      else
      begin
        // liberado pois � agregado
        docfiscal := 1;
      end;

      // Se dedicado = S e TDE = S n�o pode pagar
      // retirado este bloqueio deixando para manual conforme e-mail do Fabio Sacurai.
      //if mdRepreDEDICADO.Value = 'S' then
      //  exit;
      if (mdReprevalor_cobrado.Value > mdRepreCusto_calc.Value) and
        (mdRepreescol.Value = 0) then
      begin
        if Application.Messagebox('Voc� Deseja Liberar Este Pagamento ?',
          'Gerar Pagamento', MB_YESNO + MB_ICONQUESTION) = IDYES then
        begin
          try
            Application.CreateForm(TfrmLiberaPagto, frmLiberaPagto);
            frmLiberaPagto.ShowModal;
            liberado := frmLiberaPagto.Tag;
          finally
            frmLiberaPagto.Free;
          end;
        end
        else
          exit;
      end;

      if (mdRepreStatus.Value = 'N') and ((mdRepreCusto_calc.Value > 0) or (liberado = 10))
          and (docfiscal = 1) then
      begin
        mdRepre.edit;
        if (mdRepreescol.IsNull) or (mdRepreescol.Value = 0) then
        begin
          edCusto.Value := edCusto.Value + mdReprevalor_cobrado.Value;
          edCalculado.Value := edCalculado.Value + vc;
          mdRepreescol.Value := 1;
          escolhido := escolhido + 1;
          lblescolhido.Caption := 'Escolhido : ' + IntToStr(escolhido);
        end
        else
        begin
          mdRepreescol.Value := 0;
          edCusto.Value := edCusto.Value - mdReprevalor_cobrado.Value;
          escolhido := escolhido - 1;
          lblescolhido.Caption := 'Escolhido : ' + IntToStr(escolhido);
        end;
        mdRepre.Post;
      end;

    end;
    liberado := 0;

    { if (Column.Field = mdReprefl_tde) then
      begin
      if MDReprestatus.value = 'N' then
      begin
      MDRepre.edit;
      if MDReprefl_tde.Value = 'N' then
      begin
      MDReprefl_tde.value := 'S';
      end
      else
      begin
      MDReprefl_tde.value := 'N';
      end;
      MDRepre.edit;
      MDRepre.post;
      end;
      end;

      if (Column.Field = mdReprefl_tr) then
      begin
      if MDReprestatus.value = 'N' then
      begin
      MDRepre.edit;
      if MDReprefl_tr.Value = 'N' then
      begin
      MDReprefl_tr.value := 'S';
      end
      else
      begin
      MDReprefl_tr.value := 'N';
      end;
      MDRepre.post;
      end;
      end; }

    if (Column.Field = mdReprecte_custo) then
    begin
      alterou := 'S';
    end;

    if (Column.Field = mdReprevalor_cobrado) then
    begin
      alterou := 'S';
    end;

    if (Column.Field = mdRepreFatura) then
    begin
      alterou := 'S';
    end;
  end;
end;

procedure TfrmRelCusto_frete_sim.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var numero : integer;
begin
  numero := mdRepreCodcon.AsInteger;
  if mdRepreDias.Value = 0 then
  begin
    JvDBGrid1.canvas.Brush.color := clRed;
    JvDBGrid1.canvas.font.color := clNavy;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if (mdRepreTDE.AsString = 'N') and (mdReprefl_tde.AsString = 'S') then
  begin
    JvDBGrid1.canvas.Brush.color := clFuchsia;
    JvDBGrid1.canvas.font.color := clWhite;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field.FieldName = 'Custo_calc') then
  begin
    if mdReprevalor_cobrado.Value = mdRepreCusto_calc.Value then
    begin
      JvDBGrid1.canvas.Brush.color := clYellow;
      JvDBGrid1.canvas.font.color := clBlack;
    end;
    if mdReprevalor_cobrado.Value < mdRepreCusto_calc.Value then
    begin
      JvDBGrid1.canvas.Brush.color := clGreen;
      JvDBGrid1.canvas.font.color := clWhite;
    end;
    if mdReprevalor_cobrado.Value > mdRepreCusto_calc.Value then
    begin
      JvDBGrid1.canvas.Brush.color := clRed;
      JvDBGrid1.canvas.font.color := clWhite;
    end;
    if mdRepreCusto_calc.Value = -99 then
    begin
      JvDBGrid1.canvas.Brush.color := clBlack;
      JvDBGrid1.canvas.font.color := clBlack;
    end;
    if mdReprenovo_custo.Value > 0 then
    begin
      JvDBGrid1.canvas.Brush.color := $002392F5;
      JvDBGrid1.canvas.font.color := clBlack;
    end;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field.FieldName = 'Margem') then
  begin
    if mdRepreMargem.Value < 0 then
    begin
      JvDBGrid1.canvas.Brush.color := clRed;
      JvDBGrid1.canvas.font.color := clWhite;
    end;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol].Field, State);
  end;


  if (Column.Field = mdRepreescol) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdRepreescol.Value = 1 then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprebaixa) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprebaixa.Value = 1 then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprefl_tde) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprefl_tde.Value = 'S' then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 2)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprefl_tr) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprefl_tr.Value = 'S' then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 3)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprevl_tde) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprevl_tde.Value > 0 then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 2)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprecte_gen) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprecte_gen.Value > 0 then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 2)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if (Column.Field = mdReprefl_dedicado) then
  begin
    JvDBGrid1.canvas.FillRect(Rect);
    iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdReprefl_dedicado.Value = 'S' then
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 2)
    else
      iml.Draw(JvDBGrid1.canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;

  if mdRepreStatus.Value = 'P' then
  begin
    JvDBGrid1.canvas.Brush.color := clYellow;
    JvDBGrid1.canvas.font.color := clNavy;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if mdRepreStatus.Value = 'B' then
  begin
    JvDBGrid1.canvas.Brush.color := clGray;
    JvDBGrid1.canvas.font.color := clWhite;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if mdRepreStatus.Value = 'A' then
  begin
    JvDBGrid1.canvas.Brush.color := $00400040;
    JvDBGrid1.canvas.font.color := clWhite;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field = mdRepreFatura) then
  begin
    if not mdRepreliberacao.IsNull then
    begin
      JvDBGrid1.canvas.Brush.color := clLime;
      JvDBGrid1.canvas.font.color := clBlack;
    end;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  if (Column.Field = mdReprecte_custo) then
  begin
    if (mdRepreagregado.AsString = 'N') then
    begin
      if (mdReprexml.IsNull) then
      JvDBGrid1.canvas.Brush.color := clLime;
      JvDBGrid1.canvas.font.color := clBlack;
    end;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;

  // retirado o bloqueio conforme e-mail de autoriza��o do Fabio em 19/03 as 15:35
  {
  if mdRepreDEDICADO.Value = 'S' then
  begin
    JvDBGrid1.canvas.Brush.color := clBlack;
    JvDBGrid1.canvas.font.color := clWhite;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;
  }
end;

procedure TfrmRelCusto_frete_sim.JvDBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  tab: Integer;
  l1, l2, k1: double;
begin
  if (Key = VK_F2) and (Buscapalavra(mdRepreoperacao.AsString, 'KM') > 0) then
  begin
    if mdRepreStatus.Value <> 'P' then
    begin
      // verifica km
      QKM.close;

      if mdRepreTipo.Value = 'Romaneio' then
      begin
        QKM.sql[1] :=
          'from cyber.tb_coleta i left join tb_conhecimento c on i.doc = c.nr_conhecimento and i.filial = c.fl_empresa ';
        QKM.sql[3] := 'where i.nr_oc = :0 ';
        QKM.Parameters[0].Value := QRepreDOC.AsInteger;
      end
      else
      begin
        QKM.sql[1] :=
          'from vw_custo_transp_sim v left join cyber.rodcli d on d.codclifor = v.coddes ';
        QKM.sql[3] :=
          'where v.doc = :0 and v.filial = :1 and v.serie = :2 ';
        //  'left join tb_manifesto ma on ma.manifesto = i.manifesto and ma.serie = i.serie and ma.filial = i.filial where ma.romaneio = '+QuotedStr(QRepreromaneio.AsString);
        QKM.Parameters[0].Value := QRepreDOC.AsInteger;
        QKM.Parameters[1].Value := QRepreFILIAL.AsInteger;
        QKM.Parameters[2].Value := QRepreSERIE.AsString;
        //M.Parameters[0].Value := QRepreromaneio.AsString;
      end;
      QKM.open;
      k1 := mdReprekm.Value;
      km := 0;
      if not QKM.Eof then
      begin
        While not QKM.Eof do
        begin
          if ApCarac(QKMGEO.AsString) = '' then
          begin
            lat := '';
            lon := '';
            gravageo;
            if lat <> '' then
            begin
              l1 := StrtoFloat(buscatroca(lat, '.', ','));
              l2 := StrtoFloat(buscatroca(lon, '.', ','));
              dtmDados.IQuery1.close;
              dtmDados.IQuery1.sql.Clear;
              dtmDados.IQuery1.sql.Add
                ('update cyber.rodcli set latitu = :0, longit = :1 ');
              dtmDados.IQuery1.sql.Add('where codclifor = :2 ');
              dtmDados.IQuery1.Parameters[0].Value := l1;
              dtmDados.IQuery1.Parameters[1].Value := l2;
              dtmDados.IQuery1.Parameters[2].Value := QKMCODCLIFOR.AsInteger;
              dtmDados.IQuery1.ExecSQL;
              dtmDados.IQuery1.close;
            end;
          end;
          QKM.Next;
        end;
        QKM.First;
        calculakm;
        if km > 0 then
          km := km
        else
        begin
          km := 0;
        end;
      end;

      if km > 0 then
      begin
        if mdRepreTipo.Value = 'Romaneio' then
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add('update tb_ordem set km_total = :0');
          dtmDados.IQuery1.sql.Add('where id_ordem = :1 ');
          dtmDados.IQuery1.Parameters[0].Value := km;
          dtmDados.IQuery1.Parameters[1].Value := QRepreDOC.AsInteger;
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end
        else
        begin
          dtmDados.IQuery1.close;
          dtmDados.IQuery1.sql.Clear;
          dtmDados.IQuery1.sql.Add('update tb_manifesto set km_f2 = km_total, km_total = :0');
          dtmDados.IQuery1.sql.Add
            //('where manifesto = :1 and serie = :2 and filial = :3 ');
            ('where romaneio = :1  ');
          dtmDados.IQuery1.Parameters[0].Value := km;
          dtmDados.IQuery1.Parameters[1].Value := QRepreromaneio.AsString;
          //dtmDados.IQuery1.Parameters[2].Value := QRepreSERIE.AsString;
          //dtmDados.IQuery1.Parameters[3].Value := QRepreFILIAL.AsInteger;
          dtmDados.IQuery1.ExecSQL;
          dtmDados.IQuery1.close;
        end;
        mdRepre.edit;
        mdReprekm.Value := km;
        mdRepre.Post;
        if k1 <> km then
        begin
          btRelatorioClick(Sender);
        end;

      end;
    end;

    try
      Application.CreateForm(TfrmDetalhesCustoFrete_km,
        frmDetalhesCustoFrete_km);
      frmDetalhesCustoFrete_km.ShowModal;
    finally
      frmDetalhesCustoFrete_km.Free;
    end;
  end;

  if Key = VK_F8 then
  begin
    try
      Application.CreateForm(TfrmDetalhesCustoFrete_sim,
        frmDetalhesCustoFrete_sim);
      if mdRepreTipo.Value = 'Manifesto' then
        frmDetalhesCustoFrete_sim.Tag := 1
      else
        frmDetalhesCustoFrete_sim.Tag := 2;
      frmDetalhesCustoFrete_sim.ShowModal;
      frmDetalhesCustoFrete_sim.Tag := 0;
    finally
      frmDetalhesCustoFrete_sim.Free;
    end;
  end;

  if Key = VK_F10 then
  begin
    SP_CUSTO.Parameters[0].Value := mdRepreCodcon.AsInteger;
    SP_CUSTO.Parameters[1].Value := mdRepreTransp.AsInteger;
    SP_CUSTO.Parameters[2].Value := mdRepreDOC.AsInteger;
    SP_CUSTO.Parameters[3].Value := mdRepreCodfil.AsInteger;
    SP_CUSTO.ExecProc;
    try
      Application.CreateForm(TfrmDetalhesCusto, frmDetalhesCusto);
      frmDetalhesCusto.ShowModal;
    finally
      frmDetalhesCusto.Free;
    end;

  end;
  if (Key = VK_F11) and (mdRepreCusto_calc.Value > 0) then
  begin
    if not dtmDados.PodeVisualizar('frmCadTabelaCustoFrac') then
      exit;
    try
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('select cod_custo from tb_custofrac_item where cod_tab = :0 ');
      dtmDados.IQuery1.Parameters[0].Value := mdReprecod_tab.AsInteger;
      dtmDados.IQuery1.open;
      tab := dtmDados.IQuery1.fieldbyname('cod_custo').Value;
      Application.CreateForm(TfrmCadTabelaCustoFrac, frmCadTabelaCustoFrac);
      frmCadTabelaCustoFrac.QTabela.close;
      frmCadTabelaCustoFrac.QTabela.sql.Clear;
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('select t.*, f.nomeab || '' - '' ||nvl(f.codcgc, f.codcpf) nm_fornecedor, ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('CASE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('   WHEN t.fl_local = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('ELSE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('end as local, ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('CASE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('   WHEN t.fl_local_des = ''C'' THEN ''CAPITAL'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('ELSE ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('   ''INTERIOR'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('end as local_des, op.fl_km, op.fl_aereo ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('from tb_custofrac t inner join cyber.rodcli f on t.cod_fornecedor = f.codclifor ');
      frmCadTabelaCustoFrac.QTabela.sql.Add
        ('left join tb_operacao op on op.operacao = t.operacao and op.fl_receita = ''N'' ');
      frmCadTabelaCustoFrac.QTabela.sql.Add('where f.nomeab is not null');
      frmCadTabelaCustoFrac.QTabela.sql.Add('and t.cod_custo = :0');
      frmCadTabelaCustoFrac.QTabela.Parameters[0].Value := tab;
      frmCadTabelaCustoFrac.QTabela.open;
      frmCadTabelaCustoFrac.Panel2.Visible := false;
      frmCadTabelaCustoFrac.Panel3.Visible := false;
      frmCadTabelaCustoFrac.TabSheet1.TabVisible := false;

      frmCadTabelaCustoFrac.ShowModal;
    finally
      frmCadTabelaCustoFrac.Free;
    end;
  end;
end;

procedure TfrmRelCusto_frete_sim.JvDBGrid1TitleClick(Column: TColumn);
var
  icount: Integer;
begin
  mdRepre.SortOnFields(Column.FieldName);
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    JvDBGrid1.Columns[icount].Title.font.color := clWhite;
    JvDBGrid1.Columns[icount].Title.color := clRed;
  end;
  JvDBGrid1.Columns[0].Title.font.color := clRed;
  JvDBGrid1.Columns[0].Title.color := clRed;
  Column.Title.font.color := clRed;
  Column.Title.color := clWhite;
end;

procedure TfrmRelCusto_frete_sim.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  btnPgto.Enabled := false;
  if ledIdCliente.text <> '' then
    edManifesto.Clear;
end;

procedure TfrmRelCusto_frete_sim.MDRepre3AfterClose(DataSet: TDataSet);
begin
  btProcurar.Enabled := false;
  btRetirarFiltro.Enabled := false;
end;

procedure TfrmRelCusto_frete_sim.MDRepre3AfterOpen(DataSet: TDataSet);
begin
  btProcurar.Enabled := true;
  btRetirarFiltro.Enabled := true;
end;

procedure TfrmRelCusto_frete_sim.MDRepre3CalcFields(DataSet: TDataSet);
begin
  QNF.close;
  QNF.Parameters[0].Value := mdRepreCodcon.AsInteger;
  QNF.Parameters[1].Value := mdRepreCodfil.AsInteger;
  QNF.Parameters[2].Value := mdRepreCodcon.AsInteger;
  QNF.Parameters[3].Value := mdRepreCodfil.AsInteger;
  QNF.open;
  while not QNF.Eof do
  begin
    mdRepre.edit;
    if mdReprenf.Value = '' then
      mdReprenf.AsString := QNFNR_NF.AsString
    else
      mdReprenf.AsString := mdReprenf.AsString + '/' + QNFNR_NF.AsString;
    mdRepre.Post;
    QNF.Next;
  end;
end;

procedure TfrmRelCusto_frete_sim.MDRepre3FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdRepre['ESCOL'] = 1;
end;

procedure TfrmRelCusto_frete_sim.mdRepreAfterPost(DataSet: TDataSet);
begin
  if not dtmDados.PodeInserir(name) then
    exit;

  SPRel.Parameters[0].Value := mdReprecte_custo.AsInteger;
  SPRel.Parameters[1].Value := mdReprevalor_cobrado.AsFloat;
  SPRel.Parameters[2].Value := mdRepreFatura.Value;
  SPRel.Parameters[3].Value := mdRepreDOC.AsInteger;
  SPRel.Parameters[4].Value := mdRepreTipo.Value;
  SPRel.Parameters[5].Value := mdRepreTransp.AsInteger;
  SPRel.Parameters[6].Value := mdRepreCodfil.AsInteger;
  SPRel.Parameters[7].Value := mdRepreCodcon.AsInteger;
  SPRel.Parameters[8].Value := mdRepreCusto_calc.AsFloat;
  SPRel.Parameters[9].Value := GLBUSER;
  SPRel.Parameters[10].Value := 1;
  SPRel.Parameters[11].Value := mdReprepeso_alterado.AsFloat;
  SPRel.Parameters[12].Value := mdReprefl_tde.AsString;
  SPRel.Parameters[13].Value := mdReprefl_tr.AsString;
  SPRel.Parameters[14].Value := mdReprecod_tab.AsInteger;
  SPRel.ExecProc;

end;

procedure TfrmRelCusto_frete_sim.mdRepreAfterScroll(DataSet: TDataSet);
begin
  if mdReprexml.Value <> '' then
    DBXML.color := clGreen
  else
    DBXML.color := clWindow;

  if (mdRepreDias.Value = 0) and (alterou = 'S') then
    mdRepreescol.ReadOnly := true
  else
    mdRepreescol.ReadOnly := false;
end;

procedure TfrmRelCusto_frete_sim.mdRepreBeforeEdit(DataSet: TDataSet);
begin
  if not dtmDados.PodeInserir('frmRelCusto_frete_sim') then
    exit;
  pesant := mdReprepeso_alterado.Value;
end;

procedure TfrmRelCusto_frete_sim.mdReprecte_custoChange(Sender: TField);
//var
//  diretorio, arq, cnpj: String;
begin
  {if alterou = 'S' then
  begin
    listtemp2 := TStringList.Create;
    if mdRepreFilial.Value = 1 then
      diretorio := 'P:\03857930000154\'
    else if mdRepreFilial.Value = 4 then
      diretorio := 'P:\03857930000405\'
    else if mdRepreFilial.Value = 6 then
      diretorio := 'P:\03857930000669\'
    else if mdRepreFilial.Value = 8 then
      diretorio := 'P:\10761960000128\'
    else if mdRepreFilial.Value = 9 then
      diretorio := 'P:\10761960000209\';
    if mdReprecte_custo.Value > 0 then
    begin
      dtmDados.RQuery1.close;
      dtmDados.RQuery1.sql.Clear;
      dtmDados.RQuery1.sql.Add('select nvl(codcgc,codcpf) cnpj from rodcli ');
      dtmDados.RQuery1.sql.Add('where codclifor = ' + mdRepreTransp.AsString);
      dtmDados.RQuery1.open;
      cnpj := copy(ApCarac(dtmDados.RQuery1.fieldbyname('cnpj').Value), 1, 8);
      arq := '*' + cnpj + '*' + mdReprecte_custo.AsString + '*.xml';
      ListarArquivos(diretorio, arq, true, true);
      if listtemp2.text <> '' then
      begin
        mdRepre.edit;
        mdReprexml.Value := listtemp2.text;
        mdRepre.Post;
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update tb_controle_custo set xml_custo = :0 ');
        dtmDados.IQuery1.sql.Add
          ('where nr_doc = :1 and tipo = :2 and codclifor = :3 ');
        dtmDados.IQuery1.sql.Add('and filial = :4 and cte_rec = :5');
        dtmDados.IQuery1.Parameters[0].Value := listtemp2.text;
        dtmDados.IQuery1.Parameters[1].Value := mdRepreDOC.AsInteger;
        dtmDados.IQuery1.Parameters[2].Value := mdRepreTipo.Value;
        dtmDados.IQuery1.Parameters[3].Value := mdRepreTransp.AsInteger;
        dtmDados.IQuery1.Parameters[4].Value := mdRepreFilial.AsInteger;
        dtmDados.IQuery1.Parameters[5].Value := mdRepreCodcon.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end;
      listtemp2.Free;
    end;
  end;  }
end;

procedure TfrmRelCusto_frete_sim.mdRepreFaturaChange(Sender: TField);
begin
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select distinct filial from vw_custo_transp_sim ');
  dtmDados.IQuery1.sql.Add('where fatura = :0 and transp = :1 ');
  dtmDados.IQuery1.sql.Add('and codfil <> :2');
  dtmDados.IQuery1.Parameters[0].Value := mdRepreFatura.AsString;
  dtmDados.IQuery1.Parameters[1].Value := mdRepreTransp.AsInteger;
  dtmDados.IQuery1.Parameters[2].Value := mdRepreCodfil.AsInteger;
  dtmDados.IQuery1.open;
  if ((mdRepreFilial.AsInteger = 1) and (dtmDados.IQuery1.fieldbyname('filial')
    .Value = 16)) or ((mdRepreFilial.AsInteger = 16) and
    (dtmDados.IQuery1.fieldbyname('filial').Value = 1)) then
  begin
    dtmDados.IQuery1.close;
    exit;
  end;
  if not dtmDados.IQuery1.Eof then
  begin
    showmessage('Esta Fatura J� est� Para este Fornecedor em outra Filial !!');
    mdRepreFatura.Clear;
  end;
  dtmDados.IQuery1.close;
end;

procedure TfrmRelCusto_frete_sim.mdRepreFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if Tag = 0 then
    Accept := mdRepre['ESCOL'] = 1
  else
    Accept := mdRepre['doc'] = alltrim(edLocalizar.text);
end;

procedure TfrmRelCusto_frete_sim.mdReprepeso_alteradoChange(Sender: TField);
begin
  if (alterou = 'S') and (mdRepreStatus.Value <> 'P') then
  begin
    calculo;
  end;
end;

procedure TfrmRelCusto_frete_sim.ListarArquivos(diretorioInicial,
  mascara: string; listtotaldir: Boolean = false; recursive: Boolean = true);
var
  I: Integer;
  listatemp: TStrings;

  procedure ListarDiretorios(Folder: string; lista: TStrings);
  var
    Rec: TSearchRec;
    I: Integer;
    temps: string;
  begin
    lista.Clear;
    if SysUtils.FindFirst(Folder + '*', faDirectory, Rec) = 0 then
      try
        repeat
          lista.Add(Rec.Name);
        until SysUtils.FindNext(Rec) <> 0;
      finally
        if lista.Count <> 0 then
        begin
          // deleta o diretorio ..
          lista.Delete(1);
          // deleta o diretorio .
          lista.Delete(0);
          I := 0;
          // deleta os arquivos isto e fica apenas os diretorios
          if lista.Count <> 0 then
          begin
            repeat
              temps := lista.Strings[I];
              temps := extractfileext(temps);
              if temps <> '' then
                lista.Delete(I)
              else
                Inc(I);
            until I >= lista.Count;
          end;
        end;
      end;
  end;

  procedure ListarAtahos(Folder, Mask: string; lista: TStrings);
  var
    Rec: TSearchRec;
  begin
    lista.Clear;
    if SysUtils.FindFirst(Folder + Mask, faAnyFile, Rec) = 0 then
      try
        repeat
          lista.Add(Rec.Name);
        until SysUtils.FindNext(Rec) <> 0;
      finally
        SysUtils.FindClose(Rec);
      end;
  end;

  procedure AddLIstInOther(ListSource, ListDestino: TStrings);
  var
    f: Integer;
  begin
    for f := 0 to ListSource.Count - 1 do
    begin
      ListDestino.Add(ListSource.Strings[f]);
    end;
  end;

begin
  listatemp := TStringList.Create;
  ListarAtahos(diretorioInicial, mascara, listatemp);
  if listtotaldir = true then
  begin
    for I := 0 to listatemp.Count - 1 do
    begin
      listatemp.Strings[I] := diretorioInicial + listatemp.Strings[I];
    end;
  end;
  AddLIstInOther(listatemp, listtemp2);
  if recursive = true then
  begin
    ListarDiretorios(diretorioInicial, listatemp);
    for I := 0 to listatemp.Count - 1 do
    begin
      ListarArquivos(diretorioInicial + listatemp.Strings[I] + '\', mascara,
        listtotaldir, recursive);
    end;
  end;
  listatemp.Free;
end;

Function TfrmRelCusto_frete_sim.FindMemo(const Enc: String;
  Var Texto: TMemo): STRING;
Var
  I, Posicao: Integer;
  Linha: string;
Begin
  For I := 0 to Texto.Lines.Count - 1 do
  begin
    Linha := Texto.Lines[I];
    Repeat
      Posicao := pos(Enc, Linha);
      If Posicao > 0 then
      Begin
        Texto.Lines[I] := Linha;
        Result := Linha;
        exit;
      end;
    until Posicao = 0;
  end;

end;

end.
