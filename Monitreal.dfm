object frmMonitReal: TfrmMonitReal
  Left = 0
  Top = 0
  Caption = 'Monitoramento de Transporte - Realizado'
  ClientHeight = 720
  ClientWidth = 1272
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvPageControl1: TJvPageControl
    Left = 0
    Top = 0
    Width = 1272
    Height = 489
    ActivePage = TBColetas
    Align = alTop
    TabOrder = 0
    object TBColetas: TTabSheet
      Caption = 'Coletas'
      object DBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1264
        Height = 461
        Hint = 'Clique com o bot'#227'o direito para ver a legenda'
        Align = alClient
        DataSource = dtsTarefas
        ParentShowHint = False
        PopupMenu = pmLegenda
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        OnDblClick = DBGrid1DblClick
        OnKeyDown = DBGrid1KeyDown
        OnTitleClick = DBGrid1TitleClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        UseXPThemes = False
        Columns = <
          item
            Expanded = False
            FieldName = 'NRO_OCS'
            Title.Alignment = taCenter
            Title.Caption = 'OCS'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SOLICITACAO'
            Title.Alignment = taCenter
            Title.Caption = 'Solicita'#231#227'o'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODRMC'
            Title.Alignment = taCenter
            Title.Caption = 'Roman.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_CLI'
            Title.Alignment = taCenter
            Title.Caption = 'Cliente'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 123
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCON'
            Title.Alignment = taCenter
            Title.Caption = 'CT-e/OST'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODMAN'
            Title.Alignment = taCenter
            Title.Caption = 'Manif.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_REM'
            Title.Alignment = taCenter
            Title.Caption = 'Origem'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 163
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CID_REM'
            Title.Alignment = taCenter
            Title.Caption = 'Cidade Origem'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 116
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_DES'
            Title.Alignment = taCenter
            Title.Caption = 'Destino'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 139
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CID_DES'
            Title.Alignment = taCenter
            Title.Caption = 'Cidade Destino'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 135
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAINC'
            Title.Alignment = taCenter
            Title.Caption = 'DT OCS'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_COLETA_PREV'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Prev.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_COLETA'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Real'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA_CD'
            Title.Alignment = taCenter
            Title.Caption = 'Entr. CDC'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SAIDA'
            Title.Alignment = taCenter
            Title.Caption = 'Col. Sa'#237'da'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA_PREV'
            Title.Alignment = taCenter
            Title.Caption = 'Dest. Prev.'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA'
            Title.Alignment = taCenter
            Title.Caption = 'Dest. Real'
            Title.Color = 10746620
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 447
        Top = 117
        Width = 361
        Height = 41
        Caption = 'Ordenando .......'
        Color = clLime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        Visible = False
      end
    end
  end
  object pnLegenda: TPanel
    Left = 485
    Top = 148
    Width = 226
    Height = 181
    TabOrder = 1
    Visible = False
    OnExit = pnLegendaExit
    object JvGradient8: TJvGradient
      Left = 1
      Top = 1
      Width = 20
      Height = 179
      Align = alLeft
      StartColor = 8404992
      EndColor = clBtnFace
      ExplicitHeight = 181
    end
    object lblColetaHoje: TJvFullColorLabel
      Left = 8
      Top = 48
      Width = 167
      Height = 17
      LabelColor = 67758079
      Brush.Color = 649215
      Shape = stRoundRect
      Caption = 'Coleta para ser entregue hoje'
      AutoSize = True
    end
    object lblColetaAtrasada: TJvFullColorLabel
      Left = 8
      Top = 8
      Width = 100
      Height = 17
      LabelColor = 71305697
      Brush.Color = 4196833
      Shape = stRoundRect
      Caption = 'Coleta Atrasada'
      AutoSize = True
      Color = clBtnFace
      ParentColor = False
    end
    object lblColetaPontual: TJvFullColorLabel
      Left = 8
      Top = 28
      Width = 92
      Height = 17
      LabelColor = 67143427
      Brush.Color = 34563
      Shape = stRoundRect
      Caption = 'Coleta Pontual'
      AutoSize = True
    end
    object SMS1: TJvFullColorLabel
      Left = 8
      Top = 68
      Width = 103
      Height = 17
      LabelColor = 79896628
      Brush.Color = 12787764
      Shape = stRoundRect
      Caption = 'SMS - Retornado'
      AutoSize = True
      Color = clBtnFace
      ParentColor = False
    end
    object SMS2: TJvFullColorLabel
      Left = 8
      Top = 88
      Width = 149
      Height = 17
      LabelColor = 67146737
      Brush.Color = 37873
      Shape = stRoundRect
      Caption = 'SMS - Problema no Trajeto'
      AutoSize = True
    end
    object SMS3: TJvFullColorLabel
      Left = 8
      Top = 108
      Width = 152
      Height = 17
      LabelColor = 83836811
      Brush.Color = 16727947
      Shape = stRoundRect
      Caption = 'SMS - Problema na Entrega'
      AutoSize = True
    end
    object SMS9: TJvFullColorLabel
      Left = 8
      Top = 128
      Width = 100
      Height = 17
      LabelColor = 67108864
      Brush.Color = clBlack
      Shape = stRoundRect
      Caption = 'SMS - Indefinido'
      AutoSize = True
    end
    object SMSnulo: TJvFullColorLabel
      Left = 8
      Top = 148
      Width = 112
      Height = 17
      LabelColor = 83885834
      Brush.Color = 16776970
      Shape = stRoundRect
      Caption = 'SMS - N'#227'o Enviado'
      AutoSize = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 489
    Width = 1272
    Height = 72
    Align = alTop
    TabOrder = 2
    object Label10: TLabel
      Left = 9
      Top = 1
      Width = 33
      Height = 13
      Caption = 'Cliente'
    end
    object Label7: TLabel
      Left = 282
      Top = 2
      Width = 20
      Height = 13
      Caption = 'Filial'
    end
    object lblQuant: TLabel
      Left = 1130
      Top = 8
      Width = 57
      Height = 25
      Hint = 'Quantidade de registros'
      Alignment = taCenter
      AutoSize = False
      ParentShowHint = False
      ShowHint = True
      Layout = tlCenter
    end
    object Label2: TLabel
      Left = 572
      Top = 2
      Width = 21
      Height = 13
      Caption = 'OCS'
    end
    object lblDestino: TLabel
      Left = 1035
      Top = 49
      Width = 232
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 885
      Top = 1
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 967
      Top = 1
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 693
      Top = 1
      Width = 23
      Height = 13
      Caption = 'CT-e'
    end
    object Label5: TLabel
      Left = 748
      Top = 1
      Width = 13
      Height = 13
      Caption = 'NF'
    end
    object Label6: TLabel
      Left = 803
      Top = 1
      Width = 47
      Height = 13
      Caption = 'Manifesto'
    end
    object Label9: TLabel
      Left = 632
      Top = 2
      Width = 50
      Height = 13
      Caption = 'Solicita'#231#227'o'
    end
    object Label8: TLabel
      Left = 13
      Top = 50
      Width = 110
      Height = 13
      Caption = 'Transportador Coleta :'
    end
    object lblTransc: TLabel
      Left = 136
      Top = 50
      Width = 374
      Height = 13
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 528
      Top = 50
      Width = 152
      Height = 13
      Caption = 'Transportador Distrib./Transf. :'
    end
    object lblTransf: TLabel
      Left = 699
      Top = 50
      Width = 320
      Height = 13
      AutoSize = False
      Caption = '                      '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cbCliente: TJvComboBox
      Left = 9
      Top = 14
      Width = 236
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 0
    end
    object cbFilial: TJvComboBox
      Left = 281
      Top = 15
      Width = 142
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        ''
        'C-COLETA'
        'P-PROVINDA')
    end
    object BitBtn15: TBitBtn
      Tag = 1
      Left = 1048
      Top = 8
      Width = 40
      Height = 25
      Action = acPrimeiroApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6591800C6511800C6511000FF00FF00FF00FF00FF00
        FF00FF00FF00B5301000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800E7AE7B00DE965A00C6591800FF00FF00FF00FF00FF00
        FF00B5381000C6613100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400DE966300CE611800FF00FF00FF00FF00BD49
        1000CE693900DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00DE9E6B00CE691800FF00FF00C6591800D671
        3900DE9E6B00DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7A67300CE712900CE691800D6864200E7A6
        7B00DE966300DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7B68C00D6793900DE9E6B00E7AE8400DEA6
        7300DE966300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00EFBE9C00D6864200E7A67300E7B68C00E7A6
        7B00DE9E6B00E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFB68C00DE8E5200D6864200DE966300E7B6
        9400E7AE7B00E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFBE9400DE965A00FF00FF00D6864200DE9E
        6300E7B68C00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300F7CFB500EFBE9C00E79E6300FF00FF00FF00FF00D686
        4A00DE9E6B00E7B69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400F7CFB500EFC7A500E7A67300FF00FF00FF00FF00FF00
        FF00DE8E4A00DE8E4A00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn16: TBitBtn
      Tag = 2
      Left = 1088
      Top = 8
      Width = 40
      Height = 25
      Action = acAnteriorApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B530
        1000B5301000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5381000C659
        3100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD491000CE693100DE96
        6300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00C6591800D6713900DE9E6B00DE9E
        6B00BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE712900CE692100D6864A00E7AE7B00DE9E6300DEA6
        7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200DE965A00E7B68C00E7A67300DE9E6300E7A6
        7B00C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200E79E6B00EFBE9400E7AE8400DEA67300E7AE
        8400CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DE8E5200E7A67300EFBE9C00E7AE8400E7B6
        8C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00DE965A00E7A67300EFBE9C00EFBE
        9400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE966300E7A67B00EFBE
        9C00DE864A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE9E6300E796
        6300DE965A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6B00E79E6300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn17: TBitBtn
      Tag = 3
      Left = 1189
      Top = 8
      Width = 38
      Height = 25
      Action = acProximoApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6491000B5411000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6591800CE713900BD411000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE611800DEA67300CE713900BD491000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE692100E7AE7B00E7A67B00D6713900BD511000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6713100E7AE8400DEA67300E7AE7B00D6793900C651
        1800B5411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200E7B69400E7A67300E7A67300E7AE8400D679
        4200C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200EFBE9C00E7AE8400E7AE8400E7B68C00D68E
        4A00C6611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7966300EFC7A500EFB69400EFBE9C00DE966300CE71
        2900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A66B00EFC7AD00EFC7A500E7A67300D6864200FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A67B00EFCFAD00EFB68400DE966300FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object BitBtn18: TBitBtn
      Tag = 4
      Left = 1227
      Top = 8
      Width = 40
      Height = 25
      Action = acUltimoApanha
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6511800FF00FF00FF00FF00FF00FF00FF00FF00BD41
        1000BD381000B5381000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800D6864200C6591800FF00FF00FF00FF00FF00FF00BD49
        1000D6864A00DE965A00B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400D6864200C6591800FF00FF00FF00FF00C651
        1000D6865200DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00E7AE8400D6864200C6611800FF00FF00C659
        1800D68E5200DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7AE7B00E7B68C00D6864A00CE611800CE61
        1800DE965A00DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7AE7B00E7AE8400E7B68C00DE9E6300CE69
        1800DEA67300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00E7B68400E7B68C00EFB69400DE9E6B00CE71
        2900E7AE8400E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFBE9400EFBE9C00DE9E6B00D6793900D679
        3900E7A67300E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFC7A500E7A67B00DE8E4A00FF00FF00DE86
        4200E7AE7B00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFB68C00DE966300FF00FF00FF00FF00DE8E
        5200E7AE8400EFB69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFA67B00E7A67300FF00FF00FF00FF00FF00FF00DE96
        5A00E7B68C00EFBE9C00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6300DE966300DE965A00DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object dtInicial: TJvDateEdit
      Left = 885
      Top = 14
      Width = 77
      Height = 21
      TabOrder = 6
    end
    object dtFinal: TJvDateEdit
      Left = 968
      Top = 14
      Width = 76
      Height = 21
      TabOrder = 7
    end
    object edOCS: TJvCalcEdit
      Left = 571
      Top = 15
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 8
      DecimalPlacesAlwaysShown = False
    end
    object edCte: TJvCalcEdit
      Left = 692
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 9
      DecimalPlacesAlwaysShown = False
    end
    object edNF: TJvCalcEdit
      Left = 747
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 10
      DecimalPlacesAlwaysShown = False
    end
    object edMan: TJvCalcEdit
      Left = 802
      Top = 14
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 11
      DecimalPlacesAlwaysShown = False
    end
    object edSol: TJvCalcEdit
      Left = 631
      Top = 15
      Width = 49
      Height = 21
      DecimalPlaces = 0
      DisplayFormat = '0'
      ShowButton = False
      TabOrder = 12
      DecimalPlacesAlwaysShown = False
    end
    object btnFiltrar: TBitBtn
      Left = 460
      Top = 14
      Width = 88
      Height = 25
      Caption = 'Filtrar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      TabStop = False
      OnClick = btnFiltrarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
        9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
        C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
        9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
        E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
        AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
        DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
        F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
        D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
        F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
        DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
        F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
        E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 561
    Width = 1272
    Height = 159
    Align = alClient
    TabOrder = 3
    object Label12: TLabel
      Left = 937
      Top = 9
      Width = 100
      Height = 13
      Caption = 'Ocorr'#234'ncia da Coleta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 937
      Top = 63
      Width = 107
      Height = 13
      Caption = 'Ocorr'#234'ncia da Entrega'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 937
      Top = 109
      Width = 36
      Height = 13
      Caption = 'Usu'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 626
      Top = 9
      Width = 58
      Height = 13
      Caption = 'Observa'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object JvDBGrid1: TJvDBGrid
      Left = 9
      Top = 11
      Width = 272
      Height = 138
      TabStop = False
      DataSource = dtsNF
      Options = [dgTitles, dgColumnResize, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      AutoAppend = False
      AutoSort = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      AutoSizeRows = False
      RowsHeight = 17
      TitleRowHeight = 17
      BooleanEditor = False
      UseXPThemes = False
      Columns = <
        item
          Expanded = False
          FieldName = 'REG'
          Title.Alignment = taCenter
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = 10746620
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NOTFIS'
          Title.Alignment = taCenter
          Title.Caption = 'N. F.'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTI'
          Title.Alignment = taCenter
          Title.Caption = 'Volume'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PESOKG'
          Title.Alignment = taCenter
          Title.Caption = 'Peso'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLRMER'
          Title.Alignment = taCenter
          Title.Caption = 'Valor'
          Title.Color = 10746620
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 69
          Visible = True
        end>
    end
    object btnemail: TBitBtn
      Left = 293
      Top = 13
      Width = 108
      Height = 25
      Caption = 'Listagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnemailClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object btnVer: TBitBtn
      Left = 293
      Top = 124
      Width = 108
      Height = 25
      Hint = 'Ver Comprovante desta NF'
      Caption = 'Comprovante'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnVerClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000130B0000130B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFB4CBDF5A90BE508ABAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CCE06B9ECC92B9DE5087B8C6A08BC38E68
        C08B66BE8864BB8561B9835FB47E5CB88461C89A79E6C6ABE6C5A9DFB99AB6A3
        979CB8D36395C4B5CCE0C8926CE6E5E5E5E5E5E5E5E6E5E5E5E5E5E5E6E5E5E1
        CCBBF2DCC9F8E3CEF7E0C7F8E2CBF3D1B3B2A399837677FFFFFFCA946EE7E7E7
        E8E7E7E7E7E7E7E7E7E7E7E7C3C3C3E5C4A8F5E5D6F4DAC1F3D8BDF3D8BDF8E3
        CCD9B69AAA7353FFFFFFCC976FE9E9E9D28358D28358D28358E9E9E9C3C3C3E0
        BD9EF8EADCF4DDC6F4DCC4F3D8BDF8E2CDE4C1A4AC7554FFFFFFD19C73ECECEC
        ECECEBECECEBECECECECEBECC3C3C3E6C6AAF3E4D6F6E0CAF5DEC6F5DEC5F8E6
        D3E0C2A8B07A58FFFFFFD49E75EFEEEEEFEFEFEFEEEEEFEEEEEEEFEEEEEEEEDE
        CFBFE9CDB4F5E7DBF8ECDFF2DDC9EBD0B8DFCBBAB27C5AFFFFFFD5A076F1F1F0
        F1F0F1F0F1F1F1F0F1F1F1F1C3C3C3FEFFFFE3D4C2DAC6ACE1BFA0E5C4A7E3CD
        B6EAEEF0B57E5CFFFFFFD8A279F2F2F2D28358D28358D28358F2F2F3C3C3C3FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3B7815EFFFFFFD9A379F5F5F5
        F5F5F4F4F5F4F4F4F4F5F5F4C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
        C3F4F4F5BA8560FFFFFFDBA47AF6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6
        F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6BD8763FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFDDAC85E8B992E8B992E8B992E8B992E8B992E8B992E8
        B992E8B992E8B992E8B992E8B992E8B992E8B992C19070FFFFFFDAC3B5DEB491
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC69E7FDAC3B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnExcComp: TBitBtn
      Left = 293
      Top = 93
      Width = 108
      Height = 25
      Hint = 'Excluir Comprovante'
      Caption = 'Excluir Comprov.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnExcCompClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95B0E3235CC20543
        BC1F59C186A6DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF8CABE12866CA2177E60579EA0164DD074FBE86A6DDC6A18CC38E68
        C08B66BE8864BB8561B9835FB47E5CB27C5AB17B58164BAE639DF4187FFF0076
        F80076EE0368E11E59C0C8926CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF0543BCAECDFEFFFFFFFFFFFFFFFFFF187FEF0543BCCA946EFFFFFF
        FFFFFFFFFFFEFFFFFDFEFEFDFEFEFCFEFEFCFEFEFC245DC28DB5F64D92FF1177
        FF2186FF408AEB245CC2CC976FFFFFFFFFFFFCFFFFFDFEFEFCFEFEFCFEFEFBFD
        FDFAFDFDFA95B0E13D76D28DB5F7B8D6FE72A8F52E6BCA94AFE2D19C73FFFFFF
        FEFEFCFEFEFCFEFEFCFDFDFBFDFDFBFDFDFAFDFDF8FBFBF993AEDF2A61C60543
        BC205AC15F6186FFFFFFD49E75FFFFFFFEFEFCFDFDFBFDFDFCFDFDFBFDFDF9FC
        FCF8FBF9F7FBF9F5FBF8F4FBF7F2FBF5F2FFFFFFB27C5AFFFFFFD5A076FFFFFF
        FDFDFCFDFDFBFDFDFAFCFCF9FCFBF7FBF9F5FBF8F4FBF7F3FBF5F2FAF3EFF8F2
        ECFFFFFFB57E5CFFFFFFD8A279FFFFFFFDFDFAFCFCFAFCFBF9FBFAF6FBF8F5FB
        F7F4FBF6F1F8F4EEF7F2EBF7F0EAF6ECE8FFFFFFB7815EFFFFFFD9A379FFFFFF
        FCFBF9FCFBF8FBF9F7FBF7F4FAF7F2F9F5F0F7F3EDF6EFEAF5EBE7F3EAE4F2E7
        DEFFFFFFBA8560FFFFFFDBA47AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBD8763FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFDDAD86E8B992E8B992E8B992E8B992E8B992E8B992E8
        B992E8B992E8B992E8B992E8B992E8B992E8B992C19170FFFFFFDBC3B6DEB492
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC79E80DBC3B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object edocorrCol: TJvMaskEdit
      Left = 937
      Top = 28
      Width = 281
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
    object edocorrEnt: TJvMaskEdit
      Left = 937
      Top = 82
      Width = 281
      Height = 21
      ReadOnly = True
      TabOrder = 5
    end
    object JvDBMaskEdit3: TJvDBMaskEdit
      Left = 937
      Top = 125
      Width = 281
      Height = 21
      DataField = 'USER_BAIXA'
      DataSource = dtsColeta
      ReadOnly = True
      TabOrder = 6
    end
    object JvDBRichEdit1: TJvDBRichEdit
      Left = 626
      Top = 24
      Width = 301
      Height = 122
      DataField = 'OBS_MON'
      DataSource = dtsColeta
      ReadOnly = True
      TabOrder = 7
    end
  end
  object Qtarefas: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterOpen = QtarefasAfterOpen
    AfterScroll = QtarefasAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'select distinct sequencia, nro_ocs, datainc, remetente, codcon, ' +
        'filial, solicitacao, dt_coleta_prev, dt_coleta, dt_entrega_cd, d' +
        't_entrega_prev,  to_char(dt_entrega,'#39'dd/mm/yyyy'#39') dt_entrega, dt' +
        '_saida, nm_cli, nm_rem, cid_rem,nm_des, cid_des, codman, tipocs,' +
        ' saida, destinatario, dt_transbordo1, codrmc, destino,transp_rom' +
        'a, transp_man, destma, destro, estado, nommot,status_sms, monito' +
        'ramento, cod_des, ost, codfil from VW_MONITOR_REAL'
      'where dt_entrega is not null'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by dt_entrega_prev'
      '')
    Left = 16
    Top = 64
    object QtarefasSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDATAINC: TDateTimeField
      Alignment = taCenter
      FieldName = 'DATAINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YY'
    end
    object QtarefasDT_COLETA_PREV: TStringField
      Alignment = taCenter
      FieldName = 'DT_COLETA_PREV'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_COLETA: TStringField
      Alignment = taCenter
      FieldName = 'DT_COLETA'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_ENTREGA_CD: TStringField
      Alignment = taCenter
      FieldName = 'DT_ENTREGA_CD'
      ReadOnly = True
      Size = 19
    end
    object QtarefasDT_ENTREGA_PREV: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_ENTREGA_PREV'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QtarefasDT_SAIDA: TDateTimeField
      Alignment = taCenter
      FieldName = 'DT_SAIDA'
      ReadOnly = True
    end
    object QtarefasNM_CLI: TStringField
      FieldName = 'NM_CLI'
      ReadOnly = True
    end
    object QtarefasNM_REM: TStringField
      FieldName = 'NM_REM'
      ReadOnly = True
    end
    object QtarefasCID_REM: TStringField
      FieldName = 'CID_REM'
      ReadOnly = True
      Size = 40
    end
    object QtarefasNM_DES: TStringField
      FieldName = 'NM_DES'
      ReadOnly = True
    end
    object QtarefasCID_DES: TStringField
      FieldName = 'CID_DES'
      ReadOnly = True
      Size = 40
    end
    object QtarefasCODMAN: TBCDField
      FieldName = 'CODMAN'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTIPOCS: TStringField
      FieldName = 'TIPOCS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QtarefasSAIDA: TStringField
      FieldName = 'SAIDA'
      ReadOnly = True
      Size = 8
    end
    object QtarefasDESTINATARIO: TBCDField
      FieldName = 'DESTINATARIO'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasCODCON: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDT_TRANSBORDO1: TDateTimeField
      FieldName = 'DT_TRANSBORDO1'
      ReadOnly = True
    end
    object QtarefasCODRMC: TBCDField
      FieldName = 'CODRMC'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTRANSP_ROMA: TBCDField
      FieldName = 'TRANSP_ROMA'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasTRANSP_MAN: TBCDField
      FieldName = 'TRANSP_MAN'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDESTMA: TStringField
      FieldName = 'DESTMA'
      ReadOnly = True
      Size = 40
    end
    object QtarefasDESTRO: TStringField
      FieldName = 'DESTRO'
      ReadOnly = True
      Size = 40
    end
    object QtarefasSOLICITACAO: TBCDField
      FieldName = 'SOLICITACAO'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
    end
    object QtarefasESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object QtarefasNOMMOT: TStringField
      FieldName = 'NOMMOT'
      Size = 40
    end
    object QtarefasSTATUS_SMS: TBCDField
      FieldName = 'STATUS_SMS'
      Precision = 32
      Size = 0
    end
    object QtarefasOST: TBCDField
      FieldName = 'OST'
      Precision = 32
    end
    object QtarefasFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasMONITORAMENTO: TStringField
      FieldName = 'MONITORAMENTO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QtarefasCOD_DES: TBCDField
      FieldName = 'COD_DES'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
    object QtarefasDT_ENTREGA: TStringField
      FieldName = 'DT_ENTREGA'
      ReadOnly = True
      Size = 10
    end
  end
  object act: TActionList
    Images = dtmDados.IMBotoes
    Left = 96
    Top = 614
    object acPrimeiroApanha: TAction
      Tag = 1
      Category = 'Apanha'
      Hint = 'Primeiro registro'
      ImageIndex = 3
      OnExecute = acPrimeiroApanhaExecute
    end
    object acAnteriorApanha: TAction
      Tag = 2
      Category = 'Apanha'
      Hint = 'Registro anterior'
      ImageIndex = 2
      OnExecute = acPrimeiroApanhaExecute
    end
    object acProximoApanha: TAction
      Tag = 3
      Category = 'Apanha'
      Hint = 'Pr'#243'ximo registro'
      ImageIndex = 0
      OnExecute = acPrimeiroApanhaExecute
    end
    object acUltimoApanha: TAction
      Tag = 4
      Category = 'Apanha'
      Hint = #218'ltimo registro'
      ImageIndex = 1
      OnExecute = acPrimeiroApanhaExecute
    end
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select id_ioc id, codocs cod, notfis, quanti, pesokg, vlrmer, da' +
        'tnot from rodioc'
      '')
    Left = 149
    Top = 619
    object QNFID: TBCDField
      FieldName = 'ID'
      Precision = 32
    end
    object QNFCOD: TBCDField
      FieldName = 'COD'
      Precision = 32
    end
    object QNFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QNFQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
    object QNFPESOKG: TBCDField
      FieldName = 'PESOKG'
      Precision = 14
    end
    object QNFVLRMER: TBCDField
      FieldName = 'VLRMER'
      Precision = 14
      Size = 2
    end
    object QNFDATNOT: TDateTimeField
      FieldName = 'DATNOT'
    end
  end
  object dtsNF: TDataSource
    DataSet = QNF
    Left = 185
    Top = 619
  end
  object iml: TImageList
    Left = 140
    Top = 68
    Bitmap = {
      494C010102000400440210001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object Qocorr: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select id codigo, descricao descri from tb_ocorrencia'
      'where id = :0')
    Left = 16
    Top = 236
    object QocorrCODIGO: TBCDField
      FieldName = 'CODIGO'
      Precision = 32
    end
    object QocorrDESCRI: TStringField
      FieldName = 'DESCRI'
      Size = 60
    end
  end
  object dtsocorr: TDataSource
    DataSet = Qocorr
    Left = 64
    Top = 236
  end
  object pmLegenda: TPopupMenu
    Left = 184
    Top = 68
    object Legenda1: TMenuItem
      Caption = 'Legenda'
      ImageIndex = 96
      OnClick = Legenda1Click
    end
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct c.nomeab nm_cli_os'
      'from tb_coleta t left join rodcli c on t.pagador = c.codclifor'
      'where t.dt_saida is not null '
      'order by 1')
    Left = 68
    Top = 180
    object QClienteNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      ReadOnly = True
      Size = 50
    end
  end
  object QColeta: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    AfterScroll = QColetaAfterScroll
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select t.*'
      'from tb_coleta t '
      'where sequencia = :0')
    Left = 16
    Top = 121
    object QColetaSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      Precision = 32
    end
    object QColetaDATAINC: TDateTimeField
      FieldName = 'DATAINC'
    end
    object QColetaPAGADOR: TBCDField
      FieldName = 'PAGADOR'
      Precision = 32
    end
    object QColetaREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      Precision = 32
    end
    object QColetaMUN_REM: TStringField
      FieldName = 'MUN_REM'
      Size = 100
    end
    object QColetaCOD_MUN_REM: TBCDField
      FieldName = 'COD_MUN_REM'
      Precision = 32
    end
    object QColetaDESTINATARIO: TBCDField
      FieldName = 'DESTINATARIO'
      Precision = 32
    end
    object QColetaMUN_DES: TStringField
      FieldName = 'MUN_DES'
      Size = 100
    end
    object QColetaCOD_MUN_DES: TBCDField
      FieldName = 'COD_MUN_DES'
      Precision = 32
    end
    object QColetaDT_COLETA_PREV: TDateTimeField
      FieldName = 'DT_COLETA_PREV'
    end
    object QColetaSOLICITACAO: TBCDField
      FieldName = 'SOLICITACAO'
      Precision = 32
    end
    object QColetaSOLICITANTE: TStringField
      FieldName = 'SOLICITANTE'
      Size = 100
    end
    object QColetaEMAIL_SOLIC: TStringField
      FieldName = 'EMAIL_SOLIC'
      Size = 240
    end
    object QColetaOBSERV: TStringField
      FieldName = 'OBSERV'
      Size = 3000
    end
    object QColetaFILIAL: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
    end
    object QColetaCODLINHA: TStringField
      FieldName = 'CODLINHA'
      Size = 6
    end
    object QColetaDEALER: TBCDField
      FieldName = 'DEALER'
      Precision = 32
    end
    object QColetaNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      Precision = 32
    end
    object QColetaFL_EMAIL_ENV: TBCDField
      FieldName = 'FL_EMAIL_ENV'
      Precision = 32
    end
    object QColetaDT_ENTREGA_PREV: TDateTimeField
      FieldName = 'DT_ENTREGA_PREV'
    end
    object QColetaITN_REM: TStringField
      FieldName = 'ITN_REM'
      Size = 3
    end
    object QColetaITN_DES: TStringField
      FieldName = 'ITN_DES'
      Size = 3
    end
    object QColetaDT_COLETA: TDateTimeField
      FieldName = 'DT_COLETA'
    end
    object QColetaDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
    end
    object QColetaDT_ENTREGA_CD: TDateTimeField
      FieldName = 'DT_ENTREGA_CD'
    end
    object QColetaDT_SAIDA: TDateTimeField
      FieldName = 'DT_SAIDA'
    end
    object QColetaOBS_MON: TStringField
      FieldName = 'OBS_MON'
      Size = 3000
    end
    object QColetaUSER_BAIXA: TStringField
      FieldName = 'USER_BAIXA'
      Size = 30
    end
    object QColetaOCORR_COL: TBCDField
      FieldName = 'OCORR_COL'
      Precision = 32
      Size = 0
    end
    object QColetaOCORR_ENT: TBCDField
      FieldName = 'OCORR_ENT'
      Precision = 32
      Size = 0
    end
  end
  object QFilial: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select DISTINCT (t.filial || '#39' - '#39' || e.nomeab) filial'
      'from tb_coleta t left join rodfil e on t.filial = e.codfil'
      'where t.dt_saida is null and nro_ocs is not null')
    Left = 16
    Top = 180
    object QFilialFILIAL: TStringField
      FieldName = 'FILIAL'
      ReadOnly = True
      Size = 63
    end
  end
  object dtsTarefas: TDataSource
    DataSet = Qtarefas
    Left = 68
    Top = 63
  end
  object dtsColeta: TDataSource
    DataSet = QColeta
    Left = 68
    Top = 124
  end
end
