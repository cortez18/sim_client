unit GeraManifesto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, Vcl.ComCtrls, Vcl.Mask, JvExMask, JvToolEdit, Vcl.StdCtrls,
  Vcl.ExtCtrls, JvBaseEdits, Vcl.Buttons, JvExStdCtrls, JvMemo, JvMaskEdit,
  JvCombobox, JvDBLookup, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc,
  JvMemoryDataset, System.ImageList, Vcl.ImgList, Data.Win.ADODB, ShellApi,
  pcnConversao, RLReport, RLRichText, Vcl.Imaging.pngimage, Vcl.Imaging.jpeg,
  RLParser, Vcl.DBCtrls, JvEdit, JvExControls, JvLabel, IPPeerClient,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, REST.Response.Adapter, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, System.JSON, Rest.types,
  REST.Authenticator.Basic;

type
  TfrmGeraManifesto = class(TForm)
    Panel1: TPanel;
    Panel8: TPanel;
    lblDescricao: TLabel;
    Label32: TLabel;
    Label45: TLabel;
    edValor: TEdit;
    cbPesquisa: TComboBox;
    ckRota: TCheckBox;
    cbFiltro: TCheckBox;
    edData: TJvDateEdit;
    cbCte: TCheckBox;
    edChave: TEdit;
    pgTarefas: TPageControl;
    tbRota: TTabSheet;
    dgRota: TJvDBUltimGrid;
    tbTarefa: TTabSheet;
    dbgColeta: TJvDBUltimGrid;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    tbMan: TTabSheet;
    tsManT: TTabSheet;
    TabSheet4: TTabSheet;
    Panel9: TPanel;
    btnCanTransf: TBitBtn;
    btnNovoTransf: TBitBtn;
    btnGeraTransf: TBitBtn;
    Panel4: TPanel;
    pnBotao: TPanel;
    btRomaneio: TBitBtn;
    btnCarregar_Tarefas: TBitBtn;
    btnVoltaTarefa: TBitBtn;
    Panel5: TPanel;
    Label48: TLabel;
    btnMapaRomaneio: TBitBtn;
    btnCancelaManifesto: TBitBtn;
    btnMapaRomaneioManifesto: TBitBtn;
    btnLimpaRomaneio: TBitBtn;
    btnNovoManifesto: TBitBtn;
    btnGerarMan: TBitBtn;
    edCte: TJvCalcEdit;
    edFilial: TJvCalcEdit;
    Panel7: TPanel;
    btnCanMan: TBitBtn;
    btnImprMan: TBitBtn;
    btnSefaz: TBitBtn;
    btnStatus: TBitBtn;
    btnCusto: TBitBtn;
    btnMapa2: TBitBtn;
    btnDiario: TBitBtn;
    dgTarefa: TJvDBUltimGrid;
    dbgFatura: TJvDBUltimGrid;
    dgCtrc: TJvDBUltimGrid;
    DBTransf: TJvDBUltimGrid;
    Panel3: TPanel;
    Label25: TLabel;
    Label26: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    edTotPesoTarefa: TJvCalcEdit;
    edTotValorTarefa: TJvCalcEdit;
    edTotFreteTarefa: TJvCalcEdit;
    edTotCubadoTarefa: TJvCalcEdit;
    edTotVolumeTarefa: TJvCalcEdit;
    DBGItens: TJvDBUltimGrid;
    pnManifesto: TPanel;
    Label58: TLabel;
    cbOperacao: TComboBox;
    Label11: TLabel;
    cbMotorista: TComboBox;
    Label13: TLabel;
    edPesoLimiteVeiculo: TJvCalcEdit;
    Label34: TLabel;
    JvMemo1: TJvMemo;
    Label15: TLabel;
    edQtdCubagem: TJvCalcEdit;
    edPercentCarga: TJvCalcEdit;
    Label2: TLabel;
    Label8: TLabel;
    edQtdDestino: TJvCalcEdit;
    Label9: TLabel;
    Label27: TLabel;
    edCusto: TJvCalcEdit;
    Label30: TLabel;
    edReceita: TJvCalcEdit;
    Label31: TLabel;
    edMargem: TJvCalcEdit;
    Label49: TLabel;
    edKm: TJvCalcEdit;
    Label28: TLabel;
    edVlrm: TJvCalcEdit;
    Label6: TLabel;
    edPeso: TJvCalcEdit;
    Label5: TLabel;
    edVeiculo: TJvMaskEdit;
    cbCidaded: TJvComboBox;
    cbCidadeO: TJvComboBox;
    cbUFO: TJvComboBox;
    cbUF: TJvComboBox;
    Label33: TLabel;
    Label43: TLabel;
    Label42: TLabel;
    Label44: TLabel;
    cbParaFilial: TCheckBox;
    Label1: TLabel;
    Label4: TLabel;
    cbPlaca: TJvComboBox;
    cbPlaca_carreta: TJvComboBox;
    cbTransp: TJvDBLookupEdit;
    iml: TImageList;
    qrRota: TADOQuery;
    qrRotaROTA: TStringField;
    qrRotaCODSETCL: TBCDField;
    dsRota: TDataSource;
    QSite: TADOQuery;
    QSiteSITE: TStringField;
    QDestino: TADOQuery;
    QDestinoCODCON: TBCDField;
    QDestinoINSERIDA: TBCDField;
    QDestinoNR_ROMANEIO: TBCDField;
    QDestinoRAZSOC: TStringField;
    QDestinoDATINC: TDateTimeField;
    QDestinoCODFIL: TBCDField;
    QDestinoDOC: TStringField;
    mdTemp: TJvMemoryData;
    mdTempColeta: TIntegerField;
    mdTempInserida: TIntegerField;
    mdTempreg: TIntegerField;
    mdTempnr_romaneio: TIntegerField;
    mdTempdestino: TStringField;
    mdTempdata: TDateField;
    mdTempfilial: TIntegerField;
    mdTempdoc: TStringField;
    mdTempconca: TStringField;
    mdTempromaneio: TJvMemoryData;
    mdTempromaneioSELECIONADO: TBCDField;
    mdTempromaneioNR_ROMANEIO: TBCDField;
    mdTempromaneioDESTINO: TStringField;
    mdTempromaneioPESO: TFloatField;
    mdTempromaneioNR_MANIFESTO: TIntegerField;
    mdTempromaneioDEMISSAO: TDateTimeField;
    mdTempromaneioSUSUARIO: TStringField;
    mdTempromaneioPLACA: TStringField;
    mdTempromaneioVEICULO: TStringField;
    mdTempromaneioselecionou: TIntegerField;
    mdTempromaneioconferencia: TStringField;
    mdTempromaneiovlrmer: TFloatField;
    mdTempromaneiototfre: TFloatField;
    qrRomaneio: TADOQuery;
    qrRomaneioNR_ROMANEIO: TBCDField;
    qrRomaneioDEMISSAO: TDateTimeField;
    qrRomaneioPESO: TBCDField;
    qrRomaneioUSUARIO: TStringField;
    qrRomaneioSELECIONADO: TBCDField;
    qrRomaneioUF_DESTINO: TStringField;
    qrRomaneioVLRMER: TBCDField;
    qrRomaneioDATA_LIBERADO: TDateTimeField;
    qrRomaneioTOTFRE: TBCDField;
    qrRomaneioDAT_CONFERENCIA: TDateTimeField;
    SP_ROMANEIO: TADOStoredProc;
    QTarefa: TADOQuery;
    QTarefaNR_ROMANEIO: TBCDField;
    QTarefaCODCON: TBCDField;
    QTarefaNM_EMP_ORI_TA: TStringField;
    QTarefaNR_MANIFESTO: TBCDField;
    QTarefaNR_CNPJ_CLI: TStringField;
    QTarefaNM_CLI_OS: TStringField;
    QTarefaVL_CTRC: TBCDField;
    QTarefaNM_EMP_DEST_TA_01: TStringField;
    QTarefaNR_PESO_TA_01: TBCDField;
    QTarefaFL_EMPRESA: TBCDField;
    QTarefaUF_DESTINO: TStringField;
    QTarefaSUSUARIO: TStringField;
    QTarefaDOC: TStringField;
    QTarefaCONCA: TStringField;
    qrManifestoCtrc2: TADOQuery;
    qrManifestoCtrc2NR_ROMANEIO: TBCDField;
    qrManifestoCtrc2DESTINO: TStringField;
    qrManifestoCtrc2PLACA: TStringField;
    qrManifestoCtrc2DATA: TDateTimeField;
    qrManifestoCtrc2VEICULO: TStringField;
    qrManifestoCtrc2NR_MANIFESTO: TBCDField;
    qrManifestoCtrc2NM_USUARIO: TStringField;
    qrManifestoCtrc2NM_CLI_OS: TStringField;
    qrManifestoCtrc2CODPAG: TBCDField;
    qrManifestoCtrc2PLACA_CARRETA: TStringField;
    qrManifestoCtrc2COD_TRANSP: TBCDField;
    qrManifestoCtrc2FL_EMPRESA: TBCDField;
    qrManifestoCtrc2NR_CTRC: TBCDField;
    qrManifestoCtrc2NR_CNPJ_CLI: TStringField;
    qrManifestoCtrc2CODDESTINO: TBCDField;
    qrManifestoCtrc2ORIGEM: TStringField;
    qrManifestoCtrc2NR_CNPJ_CPF_DESTINO: TStringField;
    qrManifestoCtrc2CIDADEDESTINO: TStringField;
    qrManifestoCtrc2CODIGODESTINO: TBCDField;
    qrManifestoCtrc2NM_FORNECEDOR: TStringField;
    qrManifestoCtrc2CODREM: TBCDField;
    qrManifestoCtrc2UF_DESTINO: TStringField;
    qrCalcula_Frete: TADOQuery;
    qrCalcula_FreteCUB: TBCDField;
    qrCalcula_FretePESO: TBCDField;
    qrCalcula_FreteQTD: TBCDField;
    qrCalcula_FreteVALOR: TBCDField;
    qrCalcula_FreteFRETE: TBCDField;
    qMapa: TADOQuery;
    qMapaNR_ROMANEIO: TBCDField;
    qMapaCOD_TA: TBCDField;
    qMapaDESTINO: TStringField;
    qMapaCLIREDES: TStringField;
    qMapaROTA: TStringField;
    qMapaLOCALENTREGA: TStringField;
    qMapaVEICULO: TStringField;
    qMapaCODPAG: TBCDField;
    qMapaCODFIL: TBCDField;
    QVeiculo: TADOQuery;
    QVeiculoDESCRI: TStringField;
    QVeiculoKG: TBCDField;
    QVeiculoCUB: TBCDField;
    QVeiculoPLACA: TStringField;
    QVeiculoSITRAS: TStringField;
    QVeiculoCasa: TADOQuery;
    QVeiculoCasaPLACA: TStringField;
    QVeiculoCasaDESCRI: TStringField;
    QVeiculoCasaKG: TBCDField;
    QVeiculoCasaCUB: TBCDField;
    QVeiculoCasaSITRAS: TStringField;
    QVeiculoCasaCODPRO: TBCDField;
    dtsVeiculo: TDataSource;
    QGerados: TADOQuery;
    QGeradosNR_ROMANEIO: TBCDField;
    QGeradosDEMISSAO: TDateTimeField;
    QGeradosDEMBARQUE: TDateTimeField;
    QGeradosNR_MANIFESTO: TBCDField;
    QGeradosNR_OS: TBCDField;
    QGeradosFL_EMPRESA: TBCDField;
    QGeradosUSUARIO: TStringField;
    QGeradosORIGEM: TStringField;
    QGeradosDESTINO: TStringField;
    QGeradosPESO: TFloatField;
    QGeradosSUSUARIO: TStringField;
    QGeradosDAT_CONFERENCIA: TDateTimeField;
    QGeradosCOD_USU_CONFERENCIA: TBCDField;
    QGeradosFL_CTRC: TStringField;
    QGeradosVEICULO: TStringField;
    dtsGerados: TDataSource;
    QMotor: TADOQuery;
    QMotorNOME: TStringField;
    QTransp: TADOQuery;
    QTranspNOME: TStringField;
    QTranspCODIGO: TBCDField;
    dtsTrans: TDataSource;
    dsManifestoCtrc: TDataSource;
    dstTarefa: TDataSource;
    SP_LIMPA_ROMANEIO: TADOStoredProc;
    QTransf: TADOQuery;
    QTransfMANIFESTO: TBCDField;
    QTransfUFFIM: TStringField;
    QTransfOPERACAO: TStringField;
    QTransfPESO: TBCDField;
    QTransfVLRMER: TBCDField;
    QTransfTOTFRE: TBCDField;
    QTransfCUSTO: TBCDField;
    QTransfSERIE: TStringField;
    SPTransf: TADOStoredProc;
    dtsMdTransf: TDataSource;
    MDTransf: TJvMemoryData;
    MDTransfValor: TCurrencyField;
    MDTransfSelecionado: TIntegerField;
    MDTransfNr_manifesto: TIntegerField;
    MDTransfPESO: TFloatField;
    MDTransfDestino: TStringField;
    MDTransfoperacao: TStringField;
    MDTransftotfre: TFloatField;
    MDTransfcusto: TFloatField;
    MDTransfserie: TStringField;
    dsRomaneio: TDataSource;
    dsTempRomaneio: TDataSource;
    dsTemp: TDataSource;
    ADOQuery1: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    StringField1: TStringField;
    DateTimeField1: TDateTimeField;
    BCDField4: TBCDField;
    StringField2: TStringField;
    QCusto: TADOQuery;
    QCustoCUSTO: TBCDField;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QGeraMan: TADOQuery;
    QGeraManUF_DESTINO: TStringField;
    SPManifesto: TADOStoredProc;
    QManifesto: TADOQuery;
    dtsQManifesto: TDataSource;
    Mdvolumes: TJvMemoryData;
    Mdvolumesnr_romaneio: TIntegerField;
    Mdvolumescod_ta: TIntegerField;
    Mdvolumesnf: TStringField;
    Mdvolumesdestino: TStringField;
    Mdvolumescliredes: TStringField;
    Mdvolumeslocalentrega: TStringField;
    Mdvolumesrota: TStringField;
    Mdvolumespedido: TStringField;
    Mdvolumesvolume: TStringField;
    qMapaCarga: TADOQuery;
    qMapaCargaNR_MANIFESTO: TBCDField;
    qMapaCargaDATAINC: TDateTimeField;
    qMapaCargaPLACA: TStringField;
    qMapaCargaVEICULO: TStringField;
    qMapaCargaNM_FORNECEDOR: TStringField;
    qMapaCargaNOMMOT: TStringField;
    qMapaCargaCNH: TStringField;
    qMapaCargaOBS: TStringField;
    QMapaItens: TADOQuery;
    QMapaItensDATAINC: TDateTimeField;
    QMapaItensNM_EMP_ORI_TA: TStringField;
    QMapaItensNM_EMP_DEST_TA: TStringField;
    QMapaItensNR_QUANTIDADE: TBCDField;
    QMapaItensPESO: TBCDField;
    QMapaItensNR_MANIFESTO: TBCDField;
    QMapaItensFL_EMPRESA: TBCDField;
    QMapaItensNR_CTRC: TBCDField;
    QMapaItensNR_VALOR: TBCDField;
    QMapaItensCIDADE: TStringField;
    Sp_CancelaMan: TADOStoredProc;
    Sp_Liberado: TADOStoredProc;
    QItensMapa: TADOQuery;
    QItensMapaCODCON: TBCDField;
    QItensMapaFL_EMPRESA: TBCDField;
    QItensMapaORIGEM: TStringField;
    QItensMapaDESTINO: TStringField;
    QItensMapaUF_DESTINO: TStringField;
    QItensMapaPESO: TBCDField;
    QItensMapaVLRMER: TBCDField;
    QItensMapaDOC: TStringField;
    dtsItens: TDataSource;
    qNFs: TADOQuery;
    qNFsNR_NOTA: TStringField;
    QItensRoma: TADOQuery;
    QItensRomaCODCON: TBCDField;
    QItensRomaFL_EMPRESA: TBCDField;
    QItensRomaORIGEM: TStringField;
    QItensRomaDESTINO: TStringField;
    QItensRomaUF_DESTINO: TStringField;
    QItensRomaPESO: TBCDField;
    QItensRomaVLRMER: TBCDField;
    QItensRomaDOC: TStringField;
    dtsQItensRoma: TDataSource;
    QVols: TADOQuery;
    QVolsCODCON: TBCDField;
    QVolsCODFIL: TBCDField;
    QVolsORDCOM: TStringField;
    QVolsCODBAR: TStringField;
    QVolume: TADOQuery;
    QVolumeORDCOM: TStringField;
    sp_rom_itc: TADOStoredProc;
    SP_ITC: TADOStoredProc;
    QKM: TADOQuery;
    QKMCODCLIFOR: TBCDField;
    QKMGEO: TStringField;
    QKMDESCRI: TStringField;
    QKMCODCEP: TStringField;
    XMLDoc: TXMLDocument;
    Panel10: TPanel;
    Label20: TLabel;
    edMotivo: TEdit;
    btnSalva_motivo: TBitBtn;
    Panel11: TPanel;
    Label29: TLabel;
    EditCusto: TEdit;
    btnSalvarCusto: TBitBtn;
    MStat: TJvMemo;
    pnTransf: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label19: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    edPesoTransf: TJvCalcEdit;
    edCapPesoT: TJvCalcEdit;
    edCapCubT: TJvCalcEdit;
    edCargaT: TJvCalcEdit;
    DBLTransf: TJvDBLookupEdit;
    cbMotoTransf: TComboBox;
    cbPlacaTransf: TComboBox;
    cbCarretaTransf: TComboBox;
    edVeiculoTRansf: TJvMaskEdit;
    cbFilial: TComboBox;
    cbUFD: TComboBox;
    JvMemo2: TJvMemo;
    edCustoT: TJvCalcEdit;
    edRecT: TJvCalcEdit;
    edMargT: TJvCalcEdit;
    edValorMT: TJvCalcEdit;
    edCusto_T: TJvCalcEdit;
    dtsMapa: TDataSource;
    dtsMapaCarga: TDataSource;
    dtsMapaItens: TDataSource;
    mdManifesto: TJvMemoryData;
    mdManifestoManifesto: TIntegerField;
    mdManifestodatainc: TDateField;
    mdManifestoplaca: TStringField;
    mdManifestoveiculo: TStringField;
    mdManifestonm_fornecedor: TStringField;
    mdManifestonommot: TStringField;
    mdManifestoCNH: TStringField;
    mdManifestocte: TIntegerField;
    mdManifestoremetente: TStringField;
    mdManifestodestino: TStringField;
    mdManifestocidade: TStringField;
    mdManifestovolume: TIntegerField;
    mdManifestopeso: TFloatField;
    mdManifestovalor: TFloatField;
    mdManifestoestado: TStringField;
    mdManifestooperacao: TStringField;
    mdManifestoobs: TMemoField;
    mdManifestonota: TMemoField;
    dtsmdManifesto: TDataSource;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    SPItens: TADOStoredProc;
    QOperacao: TADOQuery;
    QOperacaoOPERACAO: TStringField;
    Label46: TLabel;
    cbOperacaoTransf: TComboBox;
    Label47: TLabel;
    cbCidadeDt: TJvComboBox;
    QCustoT: TADOQuery;
    QCustoTCUSTO: TFMTBCDField;
    Label50: TLabel;
    cbIsca1: TJvComboBox;
    cbIsca2: TJvComboBox;
    Label51: TLabel;
    QIsca: TADOQuery;
    QIscaNM_ISCA: TStringField;
    QPGR_M: TADOQuery;
    QPGR_MCLIENTE: TFMTBCDField;
    QPGR_MSUMVLRMER: TFMTBCDField;
    QPGR_MID_REGRA: TFMTBCDField;
    QPGR_MRAZSOC: TStringField;
    SP_PGR: TADOStoredProc;
    mdTempromaneiopalete: TFMTBCDField;
    mdTarefa: TJvMemoryData;
    mdTarefaNR_ROMANEIO: TBCDField;
    mdTarefaCODCON: TBCDField;
    mdTarefaNM_EMP_ORI_TA: TStringField;
    mdTarefaNR_MANIFESTO: TBCDField;
    mdTarefaNR_CNPJ_CLI: TStringField;
    mdTarefaNM_CLI_OS: TStringField;
    mdTarefaVL_CTRC: TBCDField;
    mdTarefaNM_EMP_DEST_TA_01: TStringField;
    mdTarefaNR_PESO_TA_01: TBCDField;
    mdTarefaFL_EMPRESA: TBCDField;
    mdTarefaUF_DESTINO: TStringField;
    mdTarefaSUSUARIO: TStringField;
    mdTarefaDOC: TStringField;
    mdTarefaCONCA: TStringField;
    mdTarefapalete: TFMTBCDField;
    mdTarefacnpj_dest: TStringField;
    QTarefaqt_palete: TBCDField;
    mdTarefatem: TIntegerField;
    Label52: TLabel;
    edTotDoc: TJvCalcEdit;
    QTarefaCNPJ_DEST: TStringField;
    Label53: TLabel;
    ParaFilial: TJvComboBox;
    tsRoadNet: TTabSheet;
    tsRoad: TTabSheet;
    QRoad: TADOQuery;
    QRoadCODCON: TFMTBCDField;
    QRoadRAZSOC: TStringField;
    QRoadDATINC: TDateTimeField;
    QRoadCODFIL: TFMTBCDField;
    QRoadDOC: TStringField;
    QRoadNR_OC: TFMTBCDField;
    QRoadOPERACAO: TStringField;
    QRoadFL_SMDFE: TStringField;
    dbgRoad: TJvDBUltimGrid;
    Panel6: TPanel;
    Panel12: TPanel;
    btnRoadNet: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    JvDBUltimGrid1: TJvDBUltimGrid;
    Panel13: TPanel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    JvCalcEdit1: TJvCalcEdit;
    JvCalcEdit2: TJvCalcEdit;
    JvCalcEdit3: TJvCalcEdit;
    JvCalcEdit4: TJvCalcEdit;
    JvCalcEdit5: TJvCalcEdit;
    JvCalcEdit6: TJvCalcEdit;
    SP_Road: TADOStoredProc;
    QRoadNOMEAB: TStringField;
    QRoadID_ROAD: TFMTBCDField;
    QidRoad: TADOQuery;
    QidRoadNR_ROMANEIO: TFMTBCDField;
    QidRoadCODCON: TFMTBCDField;
    QidRoadNM_EMP_ORI_TA: TStringField;
    QidRoadNR_MANIFESTO: TFMTBCDField;
    QidRoadNR_CNPJ_CLI: TStringField;
    QidRoadNM_CLI_OS: TStringField;
    QidRoadVL_CTRC: TFMTBCDField;
    QidRoadNM_EMP_DEST_TA_01: TStringField;
    QidRoadNR_PESO_TA_01: TFMTBCDField;
    QidRoadFL_EMPRESA: TFMTBCDField;
    QidRoadUF_DESTINO: TStringField;
    QidRoadSUSUARIO: TStringField;
    QidRoadDOC: TStringField;
    QidRoadID_ROAD: TFMTBCDField;
    QidRoadVLRMER: TFMTBCDField;
    QidRoadCNPJ_DEST: TStringField;
    QidRoadQT_PALETE: TFMTBCDField;
    QidRoadCONCA: TStringField;
    mdTempid_road: TIntegerField;
    mdTempromaneiorota: TStringField;
    qrRomaneioROTA: TStringField;
    labeltransp: TLabel;
    qrRomaneioKM_TOTAL: TBCDField;
    mdTempromaneioKM_TOTAL: TBCDField;
    Labelplaca: TLabel;
    cbTodos: TCheckBox;
    QVeiculoCODCLIFOR: TFMTBCDField;
    btnTransp: TBitBtn;
    GroupBox2: TGroupBox;
    DBMemo2: TDBMemo;
    btnRefresh: TBitBtn;
    XMLDocument1: TXMLDocument;
    btnLimparRepom: TBitBtn;
    MDRota: TJvMemoryData;
    MDRotaid: TStringField;
    MDRotarota: TStringField;
    MDRotaid_rota: TStringField;
    MDRotaid_trace: TStringField;
    MDRotadistancia: TStringField;
    DataSource1: TDataSource;
    pnlRepom: TPanel;
    Label63: TLabel;
    edCartao2: TJvCalcEdit;
    JvLabel1: TJvLabel;
    edCiot: TJvCalcEdit;
    btnSalvar: TBitBtn;
    rgAdiantamento: TRadioGroup;
    rbSemAdiantamento: TRadioButton;
    rbComAdiantamento: TRadioButton;
    btnFechar: TBitBtn;
    cbProjeto64: TCheckBox;
    QManifestoVALOR_CUSTO: TBCDField;
    QManifestoMANIFESTO: TFMTBCDField;
    QManifestoFILIAL: TFMTBCDField;
    QManifestoSERIE: TStringField;
    QManifestoNM_TRANSP: TStringField;
    QManifestoESTADO: TStringField;
    QManifestoMARGEM: TFMTBCDField;
    QManifestoMDFE_RECIBO: TStringField;
    QManifestoOPERACAO: TStringField;
    QManifestoDATA_LIBERADO: TDateTimeField;
    QManifestoMDFE_CHAVE: TStringField;
    QManifestoMDFE_STATUS: TFMTBCDField;
    QManifestoCIOT: TStringField;
    QManifestoCARTAO_REPOM: TStringField;
    QManifestoSTATUS_VIAGEM: TMemoField;
    QManifestoID_OPERACAO: TStringField;
    QManifestoMDFE_MENSAGEM: TStringField;
    QManifestoUSUARIO: TStringField;
    QManifestoDATAINC: TDateTimeField;
    QManifestoROMANEIO: TStringField;
    QManifestoPLACA: TStringField;
    QManifestoVALOR_RECEITA: TBCDField;
    QManifestoPGR: TStringField;
    QManifestoID_MONITORAMENTO: TFMTBCDField;
    MemoRota: TMemo;
    RGPedagio: TRadioGroup;
    QTransfID_MAN: TFMTBCDField;
    MDTransfid_man: TIntegerField;
    QPGR_transf: TADOQuery;
    QPGR_transfSUMVLRMER: TFMTBCDField;
    QPGR_transfID_REGRA: TFMTBCDField;
    QPGR_transfCLIENTE: TFMTBCDField;
    QPGR_transfRAZSOC: TStringField;
    Label64: TLabel;
    cbIsca1T: TJvComboBox;
    Label65: TLabel;
    cbIsca2T: TJvComboBox;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    SP_valida_veiculo: TADOStoredProc;
    QManifestofl_contingencia: TStringField;
    rgPamcard: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgColetaCellClick(Column: TColumn);
    procedure dbgColetaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btRomaneioClick(Sender: TObject);
    procedure btnCarregar_TarefasClick(Sender: TObject);
    procedure btnLimpaRomaneioClick(Sender: TObject);
    procedure dbgFaturaCellClick(Column: TColumn);
    procedure dbgFaturaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnCarregaClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure rgTarefasClick(Sender: TObject);
    procedure btnCancelaManifestoClick(Sender: TObject);
    procedure dgRomaneioDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure mdTempAfterOpen(DataSet: TDataSet);
    procedure tbTarefaShow(Sender: TObject);
    procedure btnVoltaTarefaClick(Sender: TObject);
    procedure dstTarefaDataChange(Sender: TObject; Field: TField);
    procedure ckRotaClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure cbTranspExit(Sender: TObject);
    procedure btnNovoManifestoClick(Sender: TObject);
    procedure btnGerarManClick(Sender: TObject);
    procedure cbOperacaoChange(Sender: TObject);
    procedure tbManShow(Sender: TObject);
    procedure QManifestoBeforeOpen(DataSet: TDataSet);
    procedure btnMapaRomaneioManifestoClick(Sender: TObject);
    procedure btnStatusClick(Sender: TObject);
    procedure MStatKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnImprManClick(Sender: TObject);
    procedure btnSefazClick(Sender: TObject);
    procedure cbplacaChange(Sender: TObject);
    procedure tsManTShow(Sender: TObject);
    procedure btnNovoTransfClick(Sender: TObject);
    procedure DBTransfCellClick(Column: TColumn);
    procedure DBTransfDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBLTransfExit(Sender: TObject);
    procedure cbPlacaTransfChange(Sender: TObject);
    procedure btnCanTransfClick(Sender: TObject);
    procedure btnGeraTransfClick(Sender: TObject);
    procedure btnMapaRomaneioClick(Sender: TObject);
    procedure btnSalva_motivoClick(Sender: TObject);
    procedure btnCustoClick(Sender: TObject);
    procedure btnSalvarCustoClick(Sender: TObject);
    procedure mdTempFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure cbFiltroClick(Sender: TObject);
    procedure cbPesquisaChange(Sender: TObject);
    procedure mdTempromaneioAfterScroll(DataSet: TDataSet);
    procedure cbUFOExit(Sender: TObject);
    procedure cbCidadeOExit(Sender: TObject);
    procedure btnCanManClick(Sender: TObject);
    procedure cbFilialChange(Sender: TObject);
    procedure cbOperacaoExit(Sender: TObject);
    procedure cbUFExit(Sender: TObject);
    procedure cbPlaca_carretaChange(Sender: TObject);
    procedure btnMapa2Click(Sender: TObject);
    procedure cbCidadedExit(Sender: TObject);
    procedure qMapaAfterScroll(DataSet: TDataSet);
    procedure edChaveChange(Sender: TObject);
    procedure cbCteClick(Sender: TObject);
    procedure btnDiarioClick(Sender: TObject);
    procedure mdTempromaneioFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure edFilialExit(Sender: TObject);
    procedure edDataExit(Sender: TObject);
    procedure cbOperacaoTransfExit(Sender: TObject);
    procedure cbOperacaoTransfChange(Sender: TObject);
    procedure cbUFDExit(Sender: TObject);
    procedure cbCidadeDtChange(Sender: TObject);
    procedure dgTarefaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbParaFilialExit(Sender: TObject);
    procedure ParaFilialChange(Sender: TObject);
    procedure tsRoadShow(Sender: TObject);
    procedure dbgRoadCellClick(Column: TColumn);
    procedure dbgRoadDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnRoadNetClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure cbMotoristaExit(Sender: TObject);
    procedure cbTodosClick(Sender: TObject);
    procedure tsRoadHide(Sender: TObject);
    procedure btnTranspClick(Sender: TObject);
    procedure dgCtrcDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnLimparRepomClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure dgCtrcDblClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
    iRecDelete, v_codmun, v_codmunD: Integer;
    km: Double;
    bInicio: Boolean;
    geramdfe, contmarg, filiais, v_cidade, v_estado, gerakm: String;
    nao_atualiza, transp_roadnet: String;

    procedure CarregaRomaneio();
    procedure CarregaTarefas();
    procedure CarregaRomaneioManifesto();
    procedure LimpaCamposManifesto();
    function RetornaTarefasSelecionadas(aba: String): String;
    procedure calculakm;
    procedure gravageo;

  var
    lat, lon: String;

  public
    { Public declarations }

    procedure CarregaBtn(aba: String);
    procedure Averba(mani, serie: Integer);
  end;

var
  frmGeraManifesto: TfrmGeraManifesto;
  fTotPesoRomaneio, receita: real;
  dife, alterado: string;
  reg: Integer;

implementation

uses Dados, menu, funcoes, uLimpaRomaneio, diario_bordo, MSHTML, MSXML, UrlMon,
  ATM, diario_bordo_roldao, Manifesto_serieU;

{$R *.dfm}

procedure TfrmGeraManifesto.btnCustoClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir('frmLiberarManifesto') then
    Exit;

  {if (QManifestomargem.Value < GlbMdfecus) and
     ((QManifestoTRANSP.value <> 1145) and (QManifestoTRANSP.value <> 1018)) then
  begin
    ShowMessage('Favor Enviar esta Tela Para O TIC, erro de custo e receita ');
    Exit;
  end; }
  if QManifestoVALOR_CUSTO.Value > 0 then
  begin
    ShowMessage('Se o custo N�o est� Zero N�o precisa de Libera��o ');
    Exit;
  end;

  Panel11.Visible := true;
  EditCusto.clear;
  EditCusto.SetFocus;
end;

procedure TfrmGeraManifesto.Averba(mani, serie: Integer);
var
  protocolo, Data, hora, sData, sCaminho, resultado, rec: String;
  inic, fim, a: Integer;
begin
  frmMenu.QAverba.close;
  frmMenu.QAverba.Parameters[0].value := mani;
  frmMenu.QAverba.Parameters[1].value := GlbFilial;
  frmMenu.QAverba.Parameters[2].value := serie;
  frmMenu.QAverba.Open;
  while not frmMenu.QAverba.eof do
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select COALESCE(user_ws,'''') user_ws, pass_ws, code_ws from tb_seguradora ');
    dtmDados.IQuery1.sql.add('where fl_empresa = :0 and trunc(datafim) >= trunc(sysdate)');
    dtmDados.IQuery1.Parameters[0].value := frmMenu.QAverbaFL_EMPRESA.AsInteger;
    dtmDados.IQuery1.Open;
    if dtmDados.IQuery1.FieldByName('user_ws').value <> '' then
    begin
      rec := frmMenu.QAverbaCTE_CHAVE.asstring + '-cte.xml';
      sData := FormatDateTime('YYYY', frmMenu.QAverbaDT_CONHECIMENTO.value) +
        FormatDateTime('MM', frmMenu.QAverbaDT_CONHECIMENTO.value);
      sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sData
        + '\CTe\';

      frmMenu.ACBrCTe1.Conhecimentos.clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + rec);
      frmMenu.ACBrCTe1.Consultar;

      ATM.GetATMWebSvrPortType.averbaCTe
        (dtmDados.IQuery1.FieldByName('user_ws').value,
        dtmDados.IQuery1.FieldByName('pass_ws').value,
        dtmDados.IQuery1.FieldByName('code_ws').value,
        frmMenu.ACBrCTe1.Conhecimentos.Items[0].Xml);
      resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
      resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);
      if Pos('<PROTOCOLONUMERO>', resultado) > 0 then
      begin
        inic := Pos('<PROTOCOLONUMERO>', resultado) + 17;
        fim := Pos('</PROTOCOLONUMERO>', resultado);
        a := fim - inic;
        protocolo := copy(resultado, inic, a);

        inic := Pos('<DATA>', resultado) + 6;
        fim := Pos('</DATA>', resultado);
        a := fim - inic;
        Data := copy(resultado, inic, a);

        inic := Pos('<HORA>', resultado) + 6;
        fim := Pos('</HORA>', resultado);
        a := fim - inic;
        hora := copy(resultado, inic, a);
        Data := BuscaTroca(Data, '-', '/');

        frmMenu.prc_averba.Parameters[0].value := protocolo;
        frmMenu.prc_averba.Parameters[1].value :=
          StrToDateTime(Data + ' ' + hora);
        frmMenu.prc_averba.Parameters[2].value :=
          frmMenu.QAverbaCOD_CONHECIMENTO.value;
        frmMenu.prc_averba.ExecProc;
      end;
    end;
    frmMenu.QAverba.next;
  end;
end;

procedure TfrmGeraManifesto.btnDiarioClick(Sender: TObject);
begin
  Try
    Application.CreateForm(TfrmDiario_bordo, frmDiario_bordo);
    frmDiario_bordo.tag := 1;
    frmDiario_bordo.AdoInfcrm.close;
    frmDiario_bordo.AdoInfcrm.Parameters[0].value :=
      QManifestoMANIFESTO.AsInteger;
    frmDiario_bordo.AdoInfcrm.Parameters[1].value := QManifestoSERIE.asstring;
    frmDiario_bordo.AdoInfcrm.Open;
    if not frmDiario_bordo.AdoInfcrm.IsEmpty then
      frmDiario_bordo.RLReport2.Print;
    //else
    //  ShowMessage('Esse Di�rio de Bordo n�o existe observa��es do CRM.');
    frmDiario_bordo.tag := 0;
  finally
    frmDiario_bordo.Free;
  end;
  try
    Application.CreateForm(TfrmDiario_bordo_roldao, frmDiario_bordo_roldao);
    frmDiario_bordo_roldao.QPallet.close;
    frmDiario_bordo_roldao.QPallet.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
    frmDiario_bordo_roldao.QPallet.Parameters[1].Value := 'M';
    frmDiario_bordo_roldao.QPallet.open;
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.Add
      ('select diario_bordo from tb_parametro ');
    dtmdados.IQuery1.SQL.Add
      (' where codcli = :0 ');
    dtmdados.IQuery1.Parameters[0].value := frmDiario_bordo_roldao.QPalletCODPAG.AsInteger;
    dtmdados.IQuery1.open;
    if dtmdados.IQuery1.FieldByName('diario_bordo').AsString = 'S' then
    begin
      frmDiario_bordo_roldao.ADOQRep.close;
      frmDiario_bordo_roldao.ADOQRep.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
      frmDiario_bordo_roldao.ADOQRep.Parameters[1].Value := QManifestoSERIE.AsString;
      frmDiario_bordo_roldao.ADOQRep.Parameters[2].Value := 'M';
      frmDiario_bordo_roldao.ADOQRep.Open;
      frmDiario_bordo_roldao.QManifesto.close;
      frmDiario_bordo_roldao.QManifesto.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
      frmDiario_bordo_roldao.QManifesto.Parameters[1].Value := 'M';
      frmDiario_bordo_roldao.QManifesto.Parameters[2].Value := GLBFilial;
      frmDiario_bordo_roldao.QManifesto.Open;
      frmDiario_bordo_roldao.RLReport1.Preview;
    end;
  finally
    frmDiario_bordo_roldao.free;
  end;
end;

procedure TfrmGeraManifesto.btnFecharClick(Sender: TObject);
begin
  pnlRepom.Visible := false;
end;

procedure TfrmGeraManifesto.btnGerarManClick(Sender: TObject);
var
  man, icount, iroma, erropgr, rota, trace : Integer;
  regman, roma, pgr, ufdest : String;
  distancia : Real;
begin
  if cbMotorista.Text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    cbMotorista.SetFocus;
    Exit;
  end;

  if cbPlaca.Text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    //cbPlaca.SetFocus;
    Exit;
  end;

  if cbOperacao.Text = '' then
  begin
    ShowMessage('Escolha a Opera��o');
    cbOperacao.SetFocus;
    Exit;
  end;

  if (UpperCase(copy(cbOperacao.Text, 1, 6)) = 'TRANSF') and
    (cbParaFilial.checked = false) then
  begin
    ShowMessage
      ('Esta � uma Opera��o de Transfer�ncia e n�o vai precisar liberar para filial ?');
  end;

  if (cbParaFilial.checked = true) and (ParaFilial.Text = '') then
  begin
    ShowMessage('Favor escolher a Filial que ir� a transfer�ncia !!');
    parafilial.SetFocus;
    Exit;
  end;

  if cbCidadeO.Text = '' then
  begin
    ShowMessage('Selecione a Cidade de Origem');
    cbCidadeO.SetFocus;
  end;

  if (cbUF.Visible = true) and (cbUF.Text = '') then
  begin
    ShowMessage
      ('Por ser uma Transfer�ncia/Devolu��o � necess�rio a UF de destino');
    cbUF.SetFocus;
    Exit;
  end;

  if (edPeso.value > (edPesoLimiteVeiculo.value)) then
  begin
    if Application.Messagebox(PChar('Peso Total ' + FormatFloat('#,##0.00',
      edPeso.value) + ' ultrapassa Capacidade de Peso do Ve�culo, continua ?'),
      'Gerar Manifesto', MB_YESNO + MB_ICONQUESTION) = IDNO then
      abort;
  end;


  if RGPedagio.ItemIndex = -1 then
  begin
    ShowMessage
      ('Precisa marcar informa��es do Ped�gio!!');
    RGPedagio.SetFocus;
    Exit;
  end;

  if (rgAdiantamento.ItemIndex = -1) then
  begin
    ShowMessage
      ('Precisa Escolher a forma de Adiantamento !!');
    rgAdiantamento.SetFocus;
    Exit;
  end;

  if (cbProjeto64.Checked) and (cbCidadeO.Text <> cbCidadeD.Text) then
  begin
    ShowMessage('Esta op��o somente para Manifestos de Transporte Municipal !!');
    exit;
  end;

  rota := 0;
  trace := 0;
  distancia := 0;

  erropgr := 0;
  // verifica��o das regras do PGR
  Screen.Cursor := crHourGlass;
  mdTempromaneio.DisableControls;
  mdTempromaneio.Filtered := true;
  mdTempromaneio.First;
  icount := 0;
  iroma  := 0;
  ufdest := '';
  nao_atualiza := 'S';
  while not(mdTempromaneio.eof) do
  begin
    if (mdTempromaneioselecionou.AsInteger = 1) then
    begin
      if iroma <> mdTempromaneioNR_ROMANEIO.AsInteger then
      begin
        if icount = 0 then
          roma := mdTempromaneioNR_ROMANEIO.asstring
        else
          roma := roma + ',' + mdTempromaneioNR_ROMANEIO.asstring;
      end;
      icount := icount + 1;
      iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
    end;
    if mdTempromaneioDESTINO.AsString = 'RJ' then
      ufdest := 'RJ';
    mdTempromaneio.next;
  end;

  QPGR_M.Close;
  QPGR_M.sql [8] := 'where r.nr_romaneio in ('+ roma + ')';
  QPGR_M.sql [19] := 'where r.nr_romaneio in ('+ roma + ')';
  QPGR_M.Open;
  pgr := '';
  while not QPGR_M.eof do
  begin
    if QPGR_MID_REGRA.isnull then
    begin
      showmessage('Valor M�ximo Permitido do PGR Ultrapassado !');
      erropgr := 1; //showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end;
    SP_PGR.Parameters[2].Value := QPGR_MID_REGRA.AsInteger;
    SP_PGR.Parameters[3].Value := cbIsca1.Text;
    SP_PGR.Parameters[4].Value := cbIsca2.Text;
    SP_PGR.Parameters[5].Value := QTranspCODIGO.AsInteger;
    SP_PGR.Parameters[6].Value := QPGR_MSUMVLRMER.AsFloat;
    SP_PGR.ExecProc;
    if SP_PGR.Parameters[1].Value = 'S' then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      erropgr := 1; //showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end
    else if SP_PGR.Parameters[0].value <> null then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      // mudan�a paleativa para o proejto 71
    end;

    if SP_PGR.Parameters[0].Value = '� necess�rio o Monitoramento !' then
      pgr := 'S'
    else
      pgr := '';

      // Projeto 081
      if (ufdest = 'RJ') then
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.clear;
        dtmDados.IQuery1.sql.add
          ('select count(*) tem from tb_regra_pgr_item ip where ip.id_regra = :0 ');
        dtmDados.IQuery1.sql.add('and RJ = ''S'' and faixaf <= :1');
        dtmDados.IQuery1.Parameters[0].value := QPGR_MID_REGRA.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := edVlrm.value;
        dtmDados.IQuery1.Open;
        if dtmDados.IQuery1.FieldByName('tem').Value = 1 then
        begin
          dtmDados.IQuery1.close;
          erropgr := 1;
          ShowMessage('Valor total da mercadoria [R$ ' + FormatFloat('###,##0.00',edVlrm.value) + ' ] para o RJ, est� acima do permitido!');
        end;
        dtmDados.IQuery1.close;
      end;

    QPGR_M.Next;
  end;
  //
  mdTempromaneio.EnableControls;


  if erropgr > 0 then
  begin
    Screen.Cursor := crDefault;
    exit;
  end;

  if Application.Messagebox('Gerar Manifesto agora ?', 'Gerar Manifesto',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin

    Screen.Cursor := crHourGlass;
    mdTempromaneio.DisableControls;
    mdTempromaneio.Filtered := true;
    mdTempromaneio.First;
    icount := 0;
    iroma := 0;
    nao_atualiza := 'S';
    while not(mdTempromaneio.eof) do
    begin
      if (mdTempromaneioselecionou.AsInteger = 1) then
      begin
        if iroma <> mdTempromaneioNR_ROMANEIO.AsInteger then
        begin
          if icount = 0 then
            roma := mdTempromaneioNR_ROMANEIO.asstring
          else
            roma := roma + ',' + mdTempromaneioNR_ROMANEIO.asstring;
        end;
        icount := icount + 1;
        iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
      end;
      mdTempromaneio.next;
    end;
    nao_atualiza := 'N';
    mdTempromaneio.EnableControls;
    // N�o faz por Rota
    if (ParaFilial.Text <> '') or (geramdfe = 'N') then
    //if ((cbUF.Visible = true) or (ParaFilial.Text <> '')) or (geramdfe = 'N') then
    begin
      dtmDados.RQuery1.close;
      dtmDados.RQuery1.sql.clear;
      dtmDados.RQuery1.sql.add('select codmun from rodmun where estado = :0 ');
      dtmDados.RQuery1.sql.add('and descri = :1 ');
      dtmDados.RQuery1.Parameters[0].value := cbUFO.Text;
      dtmDados.RQuery1.Parameters[1].value := cbCidadeO.Text;
      dtmDados.RQuery1.Open;
      v_codmun := dtmDados.RQuery1.FieldByName('codmun').value;
      dtmDados.RQuery1.close;

      if cbProjeto64.Checked = true then
      begin
        Screen.Cursor := crHourGlass;
        mdTempromaneio.DisableControls;
        mdTempromaneio.Filtered := true;
        mdTempromaneio.First;
        iroma  := 0;
        nao_atualiza := 'S';
        while not(mdTempromaneio.eof) do
        begin
          if (mdTempromaneioselecionou.AsInteger = 1) then
          begin
            if iroma <> mdTempromaneioNR_ROMANEIO.AsInteger then
            begin
              QGeraMan.close;
              QGeraMan.sql[4] := 'Where t.agecar = (' + mdTempromaneioNR_ROMANEIO.AsString + ')';
              QGeraMan.sql[10] := 'Where t.nr_romaneio = (' + mdTempromaneioNR_ROMANEIO.AsString + ')';
              QGeraMan.Open;

              While not QGeraMan.eof do
              begin
                dtmDados.RQuery1.close;
                dtmDados.RQuery1.sql.clear;
                dtmDados.RQuery1.sql.add
                  ('select codmun from rodmun where estado = :0 ');
                dtmDados.RQuery1.sql.add('and descri = :1 ');
                dtmDados.RQuery1.Parameters[0].value := cbUFO.Text;
                dtmDados.RQuery1.Parameters[1].value := cbCidadeO.Text;
                dtmDados.RQuery1.Open;
                v_codmun := dtmDados.RQuery1.FieldByName('codmun').value;
                dtmDados.RQuery1.close;
                SPManifesto.Parameters[1].value := cbOperacao.Text;
                if geramdfe = 'S' then
                  SPManifesto.Parameters[2].value := '1'
                else
                  SPManifesto.Parameters[2].value := 'U';
                SPManifesto.Parameters[3].value := GlbFilial;
                SPManifesto.Parameters[4].value := QTranspCODIGO.AsInteger;
                SPManifesto.Parameters[5].value := cbPlaca.Text;
                SPManifesto.Parameters[6].value := cbPlaca_carreta.Text;
                SPManifesto.Parameters[7].value := GLBUSER;
                SPManifesto.Parameters[8].value := RemoveChar(cbMotorista.Text);
                SPManifesto.Parameters[9].value := mdTempromaneioNR_ROMANEIO.AsString;
                if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
                  SPManifesto.Parameters[10].value := cbUF.Text
                else
                  SPManifesto.Parameters[10].value := QGeraManUF_DESTINO.value;
                SPManifesto.Parameters[11].value := v_codmun;
                if edciot.Text <> '' then
                  SPManifesto.Parameters[12].value := 'N� do CIOT: ' + edciot.Text + ' / '
                    + JvMemo1.Text
                else
                  SPManifesto.Parameters[12].value := JvMemo1.Text;
                SPManifesto.Parameters[13].value := edCusto.value;

                if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
                begin
                  dtmDados.RQuery1.sql.clear;
                  dtmDados.RQuery1.sql.add
                    ('select codmun from rodmun where estado = :0 ');
                  dtmDados.RQuery1.sql.add('and descri = :1 ');
                  dtmDados.RQuery1.Parameters[0].value := cbUF.Text;
                  dtmDados.RQuery1.Parameters[1].value := cbCidaded.Text;
                  dtmDados.RQuery1.Open;
                  SPManifesto.Parameters[14].value := dtmDados.RQuery1.FieldByName
                    ('codmun').value;
                end
                else
                  SPManifesto.Parameters[14].value := 0;
                SPManifesto.Parameters[15].value := 'N';
                SPManifesto.Parameters[16].value := edReceita.value;
                SPManifesto.Parameters[17].value := RetZero(edciot.text,12);
                SPManifesto.Parameters[18].value := cbIsca1.text;
                SPManifesto.Parameters[19].value := cbIsca2.text;
                if Parafilial.Text <> '' then
                  SPManifesto.Parameters[20].value := sonumero(parafilial.Text)
                else
                  SPManifesto.Parameters[20].value := 0;
                // rota  e trace repom
                SPManifesto.Parameters[21].value := rota;
                SPManifesto.Parameters[22].value := trace;
                if rgPamcard.ItemIndex = -1 then
                  SPManifesto.Parameters[23].value := 'N/A'
                else if rgPamcard.ItemIndex = 0 then
                  SPManifesto.Parameters[23].value := 'Sim'
                else
                  SPManifesto.Parameters[23].value := 'N�o';
                SPManifesto.Parameters[24].value := distancia;
                if rgAdiantamento.ItemIndex = 0 then
                  SPManifesto.Parameters[25].value := 3
                else if rgAdiantamento.ItemIndex = 1 then
                  SPManifesto.Parameters[25].value := 4
                else
                  SPManifesto.Parameters[25].value := 0;
                SPManifesto.Parameters[26].value := pgr;
                if RGPedagio.ItemIndex = 0 then
                  SPManifesto.Parameters[27].value := 'N'
                else if RGPedagio.ItemIndex = 1 then
                  SPManifesto.Parameters[27].value := 'S'
                else
                  SPManifesto.Parameters[27].value := 'V';
                SPManifesto.ExecProc;
                man := SPManifesto.Parameters[0].value;

                SP_ITC.Parameters[0].value := man;
                if geramdfe = 'S' then
                  SP_ITC.Parameters[1].value := '1'
                else
                  SP_ITC.Parameters[1].value := 'U';
                SP_ITC.Parameters[2].value := GlbFilial;
                SP_ITC.ExecProc;

                if regman = '' then
                  regman := IntToStr(man)
                else
                  regman := regman + ' - ' + IntToStr(man);

                QGeraMan.next;
              end;
              QGeraMan.close;
            end;
            iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
          end;
          mdTempromaneio.next;
        end;
      end
      else
      begin
        SPManifesto.Parameters[1].value := cbOperacao.Text;
        if geramdfe = 'S' then
          SPManifesto.Parameters[2].value := '1'
        else
          SPManifesto.Parameters[2].value := 'U';
        SPManifesto.Parameters[3].value := GlbFilial;
        SPManifesto.Parameters[4].value := QTranspCODIGO.AsInteger;
        SPManifesto.Parameters[5].value := cbPlaca.Text;
        SPManifesto.Parameters[6].value := cbPlaca_carreta.Text;
        SPManifesto.Parameters[7].value := GLBUSER;
        SPManifesto.Parameters[8].value := RemoveChar(cbMotorista.Text);
        SPManifesto.Parameters[9].value := roma;
        if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
          SPManifesto.Parameters[10].value := cbUF.Text
        else
          SPManifesto.Parameters[10].value := mdTempromaneioDESTINO.value;
        SPManifesto.Parameters[11].value := v_codmun;
        if edciot.Text <> '' then
          SPManifesto.Parameters[12].value := 'N� do CIOT: ' + edciot.Text + ' / '
            + JvMemo1.Text
        else
          SPManifesto.Parameters[12].value := JvMemo1.Text;
        SPManifesto.Parameters[13].value := edCusto.value;

        if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
        begin
          dtmDados.RQuery1.sql.clear;
          dtmDados.RQuery1.sql.add
            ('select codmun from rodmun where estado = :0 ');
          dtmDados.RQuery1.sql.add('and descri = :1 ');
          dtmDados.RQuery1.Parameters[0].value := cbUF.Text;
          dtmDados.RQuery1.Parameters[1].value := cbCidaded.Text;
          dtmDados.RQuery1.Open;
          SPManifesto.Parameters[14].value := dtmDados.RQuery1.FieldByName
            ('codmun').value;
        end
        else
          SPManifesto.Parameters[14].value := 0;
        if cbParaFilial.checked = true then
          SPManifesto.Parameters[15].value := 'S'
        else
          SPManifesto.Parameters[15].value := 'N';
        SPManifesto.Parameters[16].value := edReceita.value;
        if edciot.Text <> '' then
          SPManifesto.Parameters[17].value := edciot.text
        else
          SPManifesto.Parameters[17].value := '0';
        SPManifesto.Parameters[18].value := cbIsca1.text;
        SPManifesto.Parameters[19].value := cbIsca2.text;
        if Parafilial.Text <> '' then
          SPManifesto.Parameters[20].value := sonumero(parafilial.Text)
        else
          SPManifesto.Parameters[20].value := 0;

        // rota  e trace repom
        SPManifesto.Parameters[21].value := rota;
        SPManifesto.Parameters[22].value := trace;
        if rgPamcard.ItemIndex = -1 then
          SPManifesto.Parameters[23].value := 'N/A'
        else if rgPamcard.ItemIndex = 0 then
          SPManifesto.Parameters[23].value := 'Sim'
        else
          SPManifesto.Parameters[23].value := 'N�o';
        SPManifesto.Parameters[24].value := distancia;
        if rgAdiantamento.ItemIndex = 0 then
          SPManifesto.Parameters[25].value := 3
        else if rgAdiantamento.ItemIndex = 1 then
          SPManifesto.Parameters[25].value := 4
        else
          SPManifesto.Parameters[25].value := 0;
        SPManifesto.Parameters[26].value := pgr;
        if RGPedagio.ItemIndex = 0 then
          SPManifesto.Parameters[27].value := 'N'
        else if RGPedagio.ItemIndex = 1 then
          SPManifesto.Parameters[27].value := 'S'
        else
          SPManifesto.Parameters[27].value := 'V';
        SPManifesto.ExecProc;
        man := SPManifesto.Parameters[0].value;
    {
           // Envia para Pamcard
        try
          try
            RESTRequest1.Params.Clear;
            RESTRequest1.Params.Add;
            RESTRequest1.Params[0].Kind := pkGETorPOST;
            RESTRequest1.Params[0].name := 'manifesto';
            RESTRequest1.Params[0].Value := IntToStr(man);
            RESTRequest1.Params.Add;
            RESTRequest1.Params[1].Kind := pkGETorPOST;
            RESTRequest1.Params[1].name := 'filial';
            RESTRequest1.Params[1].Value := IntToStr(GLBFilial);
            RESTRequest1.Execute;

            if RestResponse1.StatusCode <> 200 then
              ShowMessage('Erro ao receber o CIOT, favor verificar o Log de erro');
          except
            on e: Exception do
            begin
              showmessage('Erro: ' + e.Message + ' retorno : ' + RESTResponse1.Content);
            end
          end;
        finally

        end;
      }
        regman := IntToStr(man);

        SP_ITC.Parameters[0].value := man;
        if geramdfe = 'S' then
          SP_ITC.Parameters[1].value := '1'
        else
          SP_ITC.Parameters[1].value := 'U';
        SP_ITC.Parameters[2].value := GlbFilial;
        SP_ITC.ExecProc;
      end;
    end
    else
    begin
      QGeraMan.close;
      QGeraMan.sql[4] := 'Where t.agecar in (' + roma + ')';
      QGeraMan.sql[10] := 'Where t.nr_romaneio in (' + roma + ')';
      QGeraMan.Open;

      while not QGeraMan.eof do
      begin
        dtmDados.RQuery1.close;
        dtmDados.RQuery1.sql.clear;
        dtmDados.RQuery1.sql.add
          ('select codmun from rodmun where estado = :0 ');
        dtmDados.RQuery1.sql.add('and descri = :1 ');
        dtmDados.RQuery1.Parameters[0].value := cbUFO.Text;
        dtmDados.RQuery1.Parameters[1].value := cbCidadeO.Text;
        dtmDados.RQuery1.Open;
        v_codmun := dtmDados.RQuery1.FieldByName('codmun').value;
        dtmDados.RQuery1.close;
        SPManifesto.Parameters[1].value := cbOperacao.Text;
        if geramdfe = 'S' then
          SPManifesto.Parameters[2].value := '1'
        else
          SPManifesto.Parameters[2].value := 'U';
        SPManifesto.Parameters[3].value := GlbFilial;
        SPManifesto.Parameters[4].value := QTranspCODIGO.AsInteger;
        SPManifesto.Parameters[5].value := cbPlaca.Text;
        SPManifesto.Parameters[6].value := cbPlaca_carreta.Text;
        SPManifesto.Parameters[7].value := GLBUSER;
        SPManifesto.Parameters[8].value := RemoveChar(cbMotorista.Text);
        SPManifesto.Parameters[9].value := roma;
        //if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
        if (ParaFilial.Visible = true) then
          SPManifesto.Parameters[10].value := cbUF.Text
        else
          SPManifesto.Parameters[10].value := QGeraManUF_DESTINO.value;
        SPManifesto.Parameters[11].value := v_codmun;
        if edciot.Text <> '' then
          SPManifesto.Parameters[12].value := 'N� do CIOT: ' + edciot.Text + ' / '
          + JvMemo1.Text
        else
          SPManifesto.Parameters[12].value := JvMemo1.Text;
        SPManifesto.Parameters[13].value := edCusto.value;

        //if (cbUF.Visible = true) or (ParaFilial.Visible = true) then
        if (ParaFilial.Visible = true) then
        begin
          dtmDados.RQuery1.sql.clear;
          dtmDados.RQuery1.sql.add
            ('select codmun from rodmun where estado = :0 ');
          dtmDados.RQuery1.sql.add('and descri = :1 ');
          dtmDados.RQuery1.Parameters[0].value := cbUF.Text;
          dtmDados.RQuery1.Parameters[1].value := cbCidaded.Text;
          dtmDados.RQuery1.Open;
          SPManifesto.Parameters[14].value := dtmDados.RQuery1.FieldByName
            ('codmun').value;
        end
        else
          SPManifesto.Parameters[14].value := 0;
        SPManifesto.Parameters[15].value := 'N';
        SPManifesto.Parameters[16].value := edReceita.value;
        SPManifesto.Parameters[17].value := RetZero(edciot.text,12);
        SPManifesto.Parameters[18].value := cbIsca1.text;
        SPManifesto.Parameters[19].value := cbIsca2.text;
        if Parafilial.Text <> '' then
          SPManifesto.Parameters[20].value := sonumero(parafilial.Text)
        else
          SPManifesto.Parameters[20].value := 0;
        // rota  e trace repom
        SPManifesto.Parameters[21].value := rota;
        SPManifesto.Parameters[22].value := trace;
        if rgPamcard.ItemIndex = -1 then
          SPManifesto.Parameters[23].value := 'N/A'
        else if rgPamcard.ItemIndex = 0 then
          SPManifesto.Parameters[23].value := 'Sim'
        else
          SPManifesto.Parameters[23].value := 'N�o';
        SPManifesto.Parameters[24].value := distancia;
        if rgAdiantamento.ItemIndex = 0 then
          SPManifesto.Parameters[25].value := 'S'
        else if rgAdiantamento.ItemIndex = 1 then
          SPManifesto.Parameters[25].value := 'N';
        SPManifesto.Parameters[26].value := pgr;
        if RGPedagio.ItemIndex = 0 then
          SPManifesto.Parameters[27].value := 'N'
        else if RGPedagio.ItemIndex = 1 then
          SPManifesto.Parameters[27].value := 'S'
        else
          SPManifesto.Parameters[27].value := 'V';
        SPManifesto.ExecProc;
        man := SPManifesto.Parameters[0].value;
     {
        // Envia para Pamcard
        try
          try
            RESTRequest1.Params.Clear;
            RESTRequest1.Params.Add;
            RESTRequest1.Params[0].Kind := pkGETorPOST;
            RESTRequest1.Params[0].name := 'manifesto';
            RESTRequest1.Params[0].Value := IntToStr(man);
            RESTRequest1.Params.Add;
            RESTRequest1.Params[1].Kind := pkGETorPOST;
            RESTRequest1.Params[1].name := 'filial';
            RESTRequest1.Params[1].Value := IntToStr(GLBFilial);
            RESTRequest1.Execute;

            if RestResponse1.StatusCode <> 200 then
              ShowMessage('Erro ao receber o CIOT, favor verificar log no painel de Log');
          except
            on e: Exception do
            begin
              showmessage('Erro: ' + e.Message + ' retorno : ' + RESTResponse1.Content);
            end
          end;
        finally

        end;
     }
        SP_ITC.Parameters[0].value := man;
        if geramdfe = 'S' then
          SP_ITC.Parameters[1].value := '1'
        else
          SP_ITC.Parameters[1].value := 'U';
        SP_ITC.Parameters[2].value := GlbFilial;
        SP_ITC.ExecProc;

        if regman = '' then
          regman := IntToStr(man)
        else
          regman := regman + ' - ' + IntToStr(man);

        QGeraMan.next;
      end;
      QGeraMan.close;
    end;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Manifesto N�(s) ' + regman + ' Gerado(s)');
    mdTempromaneio.Filtered := false;
    btnGerarMan.Enabled := false;


    if gerakm = 'S' then
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('update tb_manifesto set km_total = :0');
      dtmDados.IQuery1.sql.add
        ('where manifesto = :1 and serie = :2 and filial = :3 ');
      dtmDados.IQuery1.Parameters[0].value := edKm.value;
      dtmDados.IQuery1.Parameters[1].value := man;
      if geramdfe = 'S' then
        dtmDados.IQuery1.Parameters[2].value := '1'
      else
        dtmDados.IQuery1.Parameters[2].value := 'U';
      dtmDados.IQuery1.Parameters[3].value := GlbFilial;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
    end;

    if cbParaFilial.checked = false then
    begin
      // Cadastrar veiculo dedicado
      dtmdados.iQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add('call prc_inclui_gener_vd(:0, :1, :2)');
      dtmdados.IQuery1.Parameters[0].value := man;
      if geramdfe = 'S' then
        dtmdados.IQuery1.Parameters[1].value := '1'
      else
        dtmdados.IQuery1.Parameters[1].value := 'U';
      dtmdados.IQuery1.Parameters[2].value := GlbFilial;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.iQuery1.close;
    end;

    qrManifestoCtrc2.close;
    qrManifestoCtrc2.Open;

    alterado := '';
    mdTemp.close;
    mdTemp.Open;

    CarregaTarefas();

    Try
      Application.CreateForm(TfrmDiario_bordo, frmDiario_bordo);
      frmDiario_bordo.AdoInfcrm.close;
      frmDiario_bordo.AdoInfcrm.Parameters[0].value := man;
      frmDiario_bordo.AdoInfcrm.Parameters[1].value := 'U';

      frmDiario_bordo.AdoInfcrm.Open;
      if not frmDiario_bordo.AdoInfcrm.IsEmpty then
        frmDiario_bordo.RLReport2.Print;
    finally
      frmDiario_bordo.Free;
    end;

    // Envia email com os XML dos CTs-e para os parceiros
    // emailXmls (IntToStr(man),'U',IntToStr(GLBFilial),IntToStr(QTranspCODIGO.AsInteger));

    CarregaRomaneioManifesto();
    LimpaCamposManifesto();
    pnManifesto.Enabled := false;
    dbgFatura.Enabled := true;
    DBGItens.Enabled := true;
    PageControl1.ActivePageIndex := 2;
  end
  else
  begin
    Screen.Cursor := crDefault;
    mdTempromaneio.DisableControls;
    mdTempromaneio.Filtered := false;
    mdTempromaneio.First;
  end;
  Screen.Cursor := crDefault;
end;

procedure TfrmGeraManifesto.btnGeraTransfClick(Sender: TObject);
var
  man, icount, erropgr: Integer;
  regman, roma, ufdest, pgr: String;
begin
  // edcusto.Value := 0;

  if cbMotoTransf.Text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    cbMotoTransf.SetFocus;
    Exit;
  end;

  if cbFilial.Text = '' then
  begin
    ShowMessage('Informe a Filial de Destino');
    cbFilial.SetFocus;
    Exit;
  end;

  if (RemoveChar(cbFilial.Text) = '0') and (cbUFD.Text = '') then
  begin
    ShowMessage('Informe a UF de Destino');
    cbUFD.SetFocus;
    Exit;
  end;

  if (v_codmunD = 0) and (cbUFD.Text <> '') then
  begin
    ShowMessage('Informe a Cidade de Destino');
    cbCidadeDt.SetFocus;
    Exit;
  end;

  if cbPlacaTransf.Text = '' then
  begin
    ShowMessage('Informe a placa do ve�culo');
    cbPlacaTransf.SetFocus;
    Exit;
  end;

  if (edPesoTransf.value > (edCapPesoT.value * 1000)) then
  begin
    if Application.Messagebox(PChar('Peso Total ' + FormatFloat('#,##0.00',
      edPesoTransf.value) +
      ' ultrapassa Capacidade de Peso do Ve�culo, continua ?'),
      'Gerar Manifesto', MB_YESNO + MB_ICONQUESTION) = IDNO then
      abort;
  end;

  erropgr := 0;
  // verifica��o das regras do PGR
  Screen.Cursor := crHourGlass;
  MDTransf.DisableControls;
  MDTransf.Filtered := true;
  MDTransf.First;
  icount := 0;
  roma  := '';
  ufdest := '';
  nao_atualiza := 'S';
  while not(MDTransf.eof) do
  begin
    if (MDTransfSelecionado.AsInteger = 1) then
    begin
      if roma <> MDTransfid_man.AsString then
      begin
        if icount = 0 then
          roma := MDTransfid_man.AsString
        else
          roma := roma + ',' + MDTransfid_man.AsString;
      end;
      icount := icount + 1;
      roma := MDTransfid_man.AsString;
    end;
    if MDTransfDestino.AsString = 'RJ' then
      ufdest := 'RJ';
    MDTransf.next;
  end;

  QPGR_transf.Close;
  QPGR_transf.sql [4] := 'where vw.id_man in ('+ roma + ')';
  QPGR_transf.Open;
  pgr := '';
  while not QPGR_transf.eof do
  begin
    if QPGR_transfID_REGRA.isnull then
    begin
      showmessage('Valor M�ximo Permitido do PGR Ultrapassado !');
      erropgr := 1; //showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end;
    SP_PGR.Parameters[2].Value := QPGR_transfID_REGRA.AsInteger;
    SP_PGR.Parameters[3].Value := cbIsca1T.Text;
    SP_PGR.Parameters[4].Value := cbIsca2T.Text;
    SP_PGR.Parameters[5].Value := QTranspCODIGO.AsInteger;
    SP_PGR.Parameters[6].Value := QPGR_transfSUMVLRMER.AsFloat;
    SP_PGR.ExecProc;
    if SP_PGR.Parameters[1].Value = 'S' then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      erropgr := 1; //showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end
    else if SP_PGR.Parameters[0].value <> null then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      // mudan�a paleativa para o proejto 71
    end;

    if SP_PGR.Parameters[0].Value = '� necess�rio o Monitoramento !' then
      pgr := 'S'
    else
      pgr := '';

      // Projeto 081
      if (ufdest = 'RJ') then
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.clear;
        dtmDados.IQuery1.sql.add
          ('select count(*) tem from tb_regra_pgr_item ip where ip.id_regra = :0 ');
        dtmDados.IQuery1.sql.add('and RJ = ''S'' and faixaf <= :1');
        dtmDados.IQuery1.Parameters[0].value := QPGR_transfID_REGRA.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := edValorMT.value;
        dtmDados.IQuery1.Open;
        if dtmDados.IQuery1.FieldByName('tem').Value = 1 then
        begin
          dtmDados.IQuery1.close;
          erropgr := 1;
          ShowMessage('Valor total da mercadoria [R$ ' + FormatFloat('###,##0.00',edValorMT.value) + ' ] para o RJ, est� acima do permitido!');
        end;
        dtmDados.IQuery1.close;
      end;

    QPGR_transf.Next;
  end;
  //
  MDTransf.EnableControls;


  if erropgr > 0 then
  begin
    Screen.Cursor := crDefault;
    exit;
  end;


  if Application.Messagebox('Gerar Manifesto Agora ?', 'Gerar Manifesto',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    MDTransf.DisableControls;
    MDTransf.First;
    icount := 0;
    while not(MDTransf.eof) do
    begin
      if (MDTransfSelecionado.AsInteger = 1) then
      begin
        if icount = 0 then
          roma := MDTransfid_man.asstring
        else
          roma := roma + ',' + MDTransfid_man.asstring;
        inc(icount);
      end;
      MDTransf.next;
    end;
    MDTransf.locate('selecionado', 1, []);
    MDTransf.EnableControls;

    SPTransf.Parameters[1].value := cbOperacaoTransf.Text;
    SPTransf.Parameters[2].value := '1';
    SPTransf.Parameters[3].value := GlbFilial;
    SPTransf.Parameters[4].value := QTranspCODIGO.AsInteger;
    SPTransf.Parameters[5].value := cbPlacaTransf.Text;
    SPTransf.Parameters[6].value := cbCarretaTransf.Text;
    SPTransf.Parameters[7].value := GLBUSER;
    SPTransf.Parameters[8].value := RemoveChar(cbMotoTransf.Text);
    SPTransf.Parameters[9].value := roma;
    SPTransf.Parameters[10].value := cbUFD.Text;
    SPTransf.Parameters[11].value := edCustoT.value;
    SPTransf.Parameters[12].value := edReceita.value;
    SPTransf.Parameters[13].value := v_codmunD;
    SPTransf.Parameters[14].value := cbIsca1T.text;
    SPTransf.Parameters[15].value := cbIsca2T.text;
    SPTransf.Parameters[16].value := JvMemo2.Text;
    SPTransf.ExecProc;
    man := SPTransf.Parameters[0].value;
    if regman = '' then
      regman := IntToStr(man)
    else
      regman := regman + ' - ' + IntToStr(man);

    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Manifesto Transfer�ncia N� ' + regman + ' Gerado');

    btnGerarMan.Enabled := false;
    edPesoTransf.value := 0;
    edCapCubT.value := 0;
    edCargaT.value := 0;
    cbFilial.ItemIndex := -1;
    QTransp.close;
    cbMotoTransf.ItemIndex := -1;
    cbPlacaTransf.ItemIndex := -1;
    cbCarretaTransf.ItemIndex := -1;
    edVeiculoTRansf.clear;
    edCargaT.Color := clSilver;
  end;
  PageControl1.ActivePageIndex := 3;
end;

procedure TfrmGeraManifesto.BitBtn2Click(Sender: TObject);
begin
  CarregaBtn('Road');
end;

procedure TfrmGeraManifesto.BitBtn3Click(Sender: TObject);
begin
  iRecDelete := mdTarefaCODCON.AsInteger;

  mdTemp.locate('Conca', mdTarefaCONCA.asstring, []);

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
  qrCalcula_Frete.Open;

  JvCalcEdit4.value := JvCalcEdit4.value -
    qrCalcula_Frete.FieldByName('cub').AsFloat;
  JvCalcEdit1.value := JvCalcEdit1.value - qrCalcula_Frete.FieldByName
    ('peso').AsFloat;
  JvCalcEdit2.value := JvCalcEdit2.value -
    qrCalcula_Frete.FieldByName('valor').AsFloat;
  JvCalcEdit5.value := JvCalcEdit5.value -
    qrCalcula_Frete.FieldByName('qtd').AsFloat;
  JvCalcEdit3.value := JvCalcEdit3.value -
    qrCalcula_Frete.FieldByName('frete').AsFloat;
  JvCalcEdit6.value := edTotDoc.Value - 1;
  //
  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.Post;

  CarregaBtn('Road');
end;

procedure TfrmGeraManifesto.btnCancelaManifestoClick(Sender: TObject);
begin
  mdTempromaneio.Filtered := false;
  LimpaCamposManifesto();
  btnCancelaManifesto.Enabled := false;
  pnManifesto.Enabled := false;
  dbgFatura.Enabled := true;
  DBGItens.Enabled := true;
  btnNovoManifesto.Enabled := true;
  btnLimpaRomaneio.Enabled := true;

  edPeso.clear;
  edPesoLimiteVeiculo.clear;
  edPercentCarga.clear;
  edQtdCubagem.clear;
  edQtdDestino.clear;
  edVlrm.clear;

  edQtdDestino.Color := clSilver;
  CarregaRomaneioManifesto();
end;

procedure TfrmGeraManifesto.btnCanManClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;

  if (QManifestoSERIE.value = '1') and (NOT QManifestoMDFE_RECIBO.IsNull) then
  begin
    ShowMessage('Para Cancelar MDF-e somente na tela de consulta de manifesto');
    Exit;
  end;

  if Application.Messagebox('Voc� Deseja Excluir Este Manifesto?',
    'Excluir Manifesto', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Sp_CancelaMan.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    Sp_CancelaMan.Parameters[1].value := QManifestoSERIE.asstring;
    Sp_CancelaMan.Parameters[2].value := QManifestoFILIAL.AsInteger;
    Sp_CancelaMan.ExecProc;

    CarregaRomaneioManifesto();
    LimpaCamposManifesto();

    PageControl1.ActivePageIndex := 1;
  end;
end;

procedure TfrmGeraManifesto.btnCanTransfClick(Sender: TObject);
begin
  btnCanTransf.Enabled := false;
  pnTransf.Enabled := false;
  DBTransf.Enabled := true;
  edPesoTransf.clear;
  edCapPesoT.clear;
  edCargaT.clear;
  edCapCubT.clear;
  tsManTShow(Sender);
end;

procedure TfrmGeraManifesto.btnCarregaClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraManifesto.btnCarregar_TarefasClick(Sender: TObject);
begin
  CarregaBtn('');
end;

procedure TfrmGeraManifesto.btnImprManClick(Sender: TObject);
var
  nf: String;
begin

  if (QManifestoPGR.AsString = 'S') and (QManifestoid_monitoramento.AsString = '') and (QManifestoSERIE.AsString = '1') then
  begin
    ShowMessage('Este Manifesto, como informado na gera��o, precisa de SM');
    Exit;
  end;

  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  qMapaCarga.Parameters[1].value := QManifestoSERIE.asstring;
  qMapaCarga.Parameters[2].value := QManifestoFILIAL.AsInteger;
  qMapaCarga.Open;
  QMapaItens.close;
  QMapaItens.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  QMapaItens.Parameters[1].value := QManifestoSERIE.AsString;
  QMapaItens.Parameters[2].value := QManifestoFILIAL.AsInteger;
  QMapaItens.Open;

  try
    Application.CreateForm(TfrmManifesto_serieU, frmManifesto_serieU);
    frmManifesto_serieU.mdManifesto.Active := false;
    frmManifesto_serieU.mdManifesto.Active := true;
    frmManifesto_serieU.tag := QMapaItens.RecordCount;
    While not QMapaItens.eof do
    begin
      frmManifesto_serieU.mdManifesto.Insert;
      frmManifesto_serieU.mdManifestoManifesto.value := qMapaCargaNR_MANIFESTO.AsInteger;
      frmManifesto_serieU.mdManifestodatainc.value := qMapaCargaDATAINC.value;
      frmManifesto_serieU.mdManifestoplaca.value := qMapaCargaPLACA.AsString;
      frmManifesto_serieU.mdManifestonm_fornecedor.value := qMapaCargaNM_FORNECEDOR.AsString;
      frmManifesto_serieU.mdManifestonommot.value := qMapaCargaNOMMOT.AsString;
      frmManifesto_serieU.mdManifestoCNH.value := qMapaCargaCNH.AsString;
      frmManifesto_serieU.mdManifestoobs.value := qMapaCargaOBS.AsString;
      frmManifesto_serieU.mdManifestooperacao.value := QManifestoOPERACAO.AsString;
      frmManifesto_serieU.mdManifestoestado.value := QManifestoESTADO.AsString;

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select distinct notfis from vw_manifesto_detalhe ');
      dtmDados.IQuery1.sql.add
        ('where nr_ctrc = :0 and nr_manifesto = :1 order by 1 ');
      dtmDados.IQuery1.Parameters[0].value := QMapaItensNR_CTRC.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QMapaItensNR_MANIFESTO.AsInteger;
      dtmDados.IQuery1.Open;
      nf := '';
      while not dtmDados.IQuery1.eof do
      begin
        nf := dtmDados.IQuery1.FieldByName('notfis').value + ' / ' + nf;
        dtmDados.IQuery1.next;
      end;
      dtmDados.IQuery1.close;

      frmManifesto_serieU.mdManifestonota.value := nf;
      frmManifesto_serieU.mdManifestoremetente.value := QMapaItensNM_EMP_ORI_TA.value;
      frmManifesto_serieU.mdManifestodestino.value := QMapaItensNM_EMP_DEST_TA.value;
      frmManifesto_serieU.mdManifestocte.AsInteger := QMapaItensNR_CTRC.AsInteger;
      frmManifesto_serieU.mdManifestocidade.value := QMapaItensCIDADE.AsString;
      frmManifesto_serieU.mdManifestovolume.AsInteger := QMapaItensNR_QUANTIDADE.AsInteger;
      frmManifesto_serieU.mdManifestopeso.value := QMapaItensPESO.value;
      frmManifesto_serieU.mdManifestovalor.value := QMapaItensNR_VALOR.value;
      frmManifesto_serieU.mdManifesto.Post;

      QMapaItens.Next;
    end;
    frmManifesto_serieU.RlManifesto.Preview;
  finally
    frmManifesto_serieU.tag := 0;
    frmManifesto_serieU.Free;
  end;


  if QManifestoSERIE.value = 'U' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('update tb_manifesto set status = ''S'' ');
    dtmDados.IQuery1.sql.add
      ('where serie = ''U'' and manifesto = :0 and filial = :1  ');
    dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.close;
  end;
  QManifesto.close;
  QManifesto.Open;
end;

procedure TfrmGeraManifesto.btnLimpaRomaneioClick(Sender: TObject);
var
  ufd: string;
  frmLimpa: TfrmLimpaRomaneio;
  qrLimpaTarefa: TADOQuery;
  iResulta: Integer;
begin
  iLimpaRomaneio := 0;
  iLimpaTarefa := 0;
  iLimpaMotivo := '';
  frmLimpa := TfrmLimpaRomaneio.Create(nil);
  frmLimpa.sTela := '1';
  frmLimpa.sRoma := mdTempromaneioNR_ROMANEIO.AsInteger;
  frmLimpa.sRota := mdTempromaneiorota.AsString;
  iResulta := frmLimpa.ShowModal;
  if iResulta = 2 then
    Exit;
  if ((sLimpaRomUF = '') and (iLimpaRomaneio = 0) and (iLimpaTarefa = 0) and
    (not bLimpaTodos)) then
    Exit;

  if mdTempromaneio.RecordCount = 0 then
    Exit;
  if Application.Messagebox('Exclui Mapa de Separa��o agora ?', 'Excluir Mapa',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if (iLimpaTarefa > 0) then
    begin
      qrLimpaTarefa := TADOQuery.Create(Self);
      with qrLimpaTarefa, sql do
      begin
        Connection := dtmDados.ConIntecom;
        add('update cyber.rodcon set agecar = null');
        add('where codcon = :tarefa and agecar = :mapa');
        Parameters[0].value := iLimpaTarefa;
        Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        ExecSQL;
        clear;
        add('update cyber.rodord set agecar = null');
        add('where codigo = :tarefa and agecar = :mapa');
        Parameters[0].value := iLimpaTarefa;
        Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        ExecSQL;
        clear;
        add('update tb_conhecimento set nr_romaneio = null');
        add('where nr_conhecimento = :tarefa and nr_romaneio = :mapa');
        Parameters[0].value := iLimpaTarefa;
        Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        ExecSQL;
        clear;
        add('update cyber.itc_man_emb set romaneio = null ');
        add('where romaneio = :tarefa ');
        Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        ExecSQL;

        dtmdados.iQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('delete from tb_palet_romaneio ');
        dtmdados.IQuery1.sql.add('where numero = :0 and tipo = ''M'' ');
        dtmdados.IQuery1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.iQuery1.close;

        dtmdados.iQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('update dashboard.skf_tb_trip set data_exp = null, ');
        dtmdados.IQuery1.sql.add('data_fim_exp = null ');
        dtmdados.IQuery1.sql.add('where cte = :0 and fl_empresa = :1 ');
        dtmdados.IQuery1.Parameters[0].value := iLimpaTarefa;
        dtmdados.IQuery1.Parameters[1].value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.iQuery1.close;

        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('select nr_romaneio from ( select agecar nr_romaneio from cyber.rodord t left join tb_romaneio r on (r.nr_romaneio = t.agecar) ');
        dtmdados.IQuery1.sql.add('where r.nr_manifesto is null union all select ct.nr_romaneio from tb_conhecimento ct  ');
        dtmdados.IQuery1.sql.add('left join tb_romaneio r on (r.nr_romaneio = ct.nr_romaneio) where r.nr_manifesto is null) ');
        dtmdados.IQuery1.sql.add('where nr_romaneio = :0');
        dtmdados.IQuery1.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.iQuery1.close;
      end;
      FreeAndNil(qrLimpaTarefa);
    end
    else
    begin
      // QTarefa.First;
      ufd := '';
      mdTempromaneio.First;
      // iQtdRom := 0;
      if (iLimpaRomaneio > 0) then
      begin
        {dtmDados.IQuery1.Close;
        dtmDados.IQuery1.sql.clear;
        dtmDados.IQuery1.sql.Add('call SP_LIMPA_ROMANEIO(:0,:1,:2)');
        dtmDados.IQuery1.Parameters[0].value := iLimpaRomaneio;
        dtmDados.IQuery1.Parameters[1].value := iLimpaMotivo;
        dtmDados.IQuery1.Parameters[2].value := GLBUSER;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.Close;  }
        SP_LIMPA_ROMANEIO.Parameters[0].value := iLimpaRomaneio;
        SP_LIMPA_ROMANEIO.Parameters[1].value := iLimpaMotivo;
        SP_LIMPA_ROMANEIO.Parameters[2].value := GLBUSER;
        SP_LIMPA_ROMANEIO.ExecProc;
        // iQtdRom := iQtdRom + 1;
      end
      else
      begin
        while not mdTempromaneio.eof do
        begin
          if not(mdTempromaneioNR_MANIFESTO.value > 0) then
          begin
            if (bLimpaTodos) then
            begin
              SP_LIMPA_ROMANEIO.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
              SP_LIMPA_ROMANEIO.Parameters[1].value := iLimpaMotivo;
              SP_LIMPA_ROMANEIO.Parameters[2].value := GLBUSER;
              SP_LIMPA_ROMANEIO.ExecProc;
              // iQtdRom := iQtdRom + 1;
            end
            else if (mdTempromaneioDESTINO.asstring = sLimpaRomUF) then
            begin
              SP_LIMPA_ROMANEIO.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
              SP_LIMPA_ROMANEIO.Parameters[1].value := iLimpaMotivo;
              SP_LIMPA_ROMANEIO.Parameters[2].value := GLBUSER;
              SP_LIMPA_ROMANEIO.ExecProc;
              // iQtdRom := iQtdRom + 1;
            end;
          end;
          mdTempromaneio.next;
        end;
      end;
    end;
    Application.ProcessMessages;
    if (iLimpaTarefa > 0) then
      ShowMessage('CT-e/OST ' + IntToStr(iLimpaTarefa) + ' Exclu�do do Mapa')
    else
      ShowMessage('Mapa ' + IntToStr(iLimpaRomaneio) + ' Exclu�do');
    mdTempromaneio.close;
  end;
  CarregaTarefas();
  CarregaRomaneioManifesto();
end;

procedure TfrmGeraManifesto.btnLimparRepomClick(Sender: TObject);
begin
  if Application.Messagebox('Voc� Deseja Limpar a Apagar a Viagem do Repom?',
    'Apagar Viagem Repom', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('update tb_manifesto set chave_viagem = null');
    dtmdados.IQuery1.sql.add('where manifesto = :0 and filial = :1 ');
    dtmdados.IQuery1.sql.add('and serie = :2');
    dtmdados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := glbfilial;
    dtmdados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
    showMessage('Viagem Repom Deletada');
  end;
end;

procedure TfrmGeraManifesto.btnMapa2Click(Sender: TObject);
var caminho : String;
begin
  if dts = 'INTECDSV' then
    caminho := 'http://192.168.236.112/Relatorios.dsv/sim/sim0001.aspx?rom=' +
      ApCarac(mdTempromaneioNR_ROMANEIO.asstring) + '&fil=' + IntToStr(GlbFilial)
  else
    caminho := 'http://192.168.236.112/Relatorios/sim/sim0001.aspx?rom=' +
      ApCarac(mdTempromaneioNR_ROMANEIO.asstring) + '&fil=' + IntToStr(GlbFilial);

  Winexec(PAnsiChar(caminho), Sw_Show);
  ShellExecute(handle, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);
  {
    // transfer�ncia para distribui��o
    if QManifestoTRANSFERIDO.value = 1 then
    begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('select romaneio from tb_manifesto ');
    dtmdados.IQuery1.sql.add('where manifesto = :0 and filial = :1 ');
    dtmdados.IQuery1.sql.add('and serie = :2');
    dtmdados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := glbfilial;
    dtmdados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
    dtmdados.IQuery1.Open;
    QMapa.close;
    QMapa.sql[7] := 'where r.nr_romaneio in (' + dtmdados.IQuery1.FieldByName('romaneio').value + ')';
    QMapa.sql[16] := 'where r.nr_romaneio in (' + dtmdados.IQuery1.FieldByName('romaneio').value + ')';
    QMapa.sql[26] := 'where r.nr_romaneio in (' + dtmdados.IQuery1.FieldByName('romaneio').value + ')';
    QMapa.open;
    end
    else
    begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('select nr_romaneio from tb_romaneio ');
    dtmdados.IQuery1.sql.add('where nr_manifesto = :0 and fl_empresa = :1 ');
    dtmdados.IQuery1.sql.add('and serie_manifesto = :2');
    dtmdados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := glbfilial;
    dtmdados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
    dtmdados.IQuery1.Open;
    QMapa.close;
    //  QMapa.Parameters[0].value := dtmdados.IQuery1.FieldByName('nr_romaneio').value;
    QMapa.Parameters[0].value := dtmdados.IQuery1.FieldByName('nr_romaneio').value;
    QMapa.Parameters[1].value := dtmdados.IQuery1.FieldByName('nr_romaneio').value;

    //showmessage(Qmapa.sql.text);

    QMapa.open;
    end;
    Mdvolumes.Active := false;
    Mdvolumes.Active := true;
    while not qMapa.Eof do
    begin
    qNFs.close;
    qNFs.Parameters[0].value := qMapaCODFIL.AsInteger;
    qNFs.Parameters[1].value := qMapaCOD_TA.AsString;
    qNFs.Parameters[2].value := qMapaCODFIL.AsInteger;
    qNFs.Parameters[3].value := qMapaCOD_TA.AsString;
    qNFs.Open;
    nf := '';
    while not qNFs.Eof do
    begin
    nf := nf +' '+ qNFsNR_NOTA.AsString;
    qNFs.Next;
    end;
    QVols.close;
    QVols.Parameters[0].value := qMapaCOD_TA.AsInteger;
    QVols.Parameters[1].value := qMapaCODFIL.AsInteger;
    QVols.open;
    ped := '';

    while not QVols.eof do
    begin
    if ped = '' then
    begin
    ped := QVolsORDCOM.AsString;
    vol := QVolsCODBAR.AsString;
    end
    else
    begin
    if QVolsORDCOM.AsString = ped then
    vol := vol +' '+QVolsCODBAR.AsString
    else
    begin
    Mdvolumes.Insert;
    Mdvolumesnr_romaneio.Value := qMapaNR_ROMANEIO.AsInteger;
    Mdvolumescod_ta.Value := qMapaCOD_TA.AsInteger;
    Mdvolumesdestino.Value := qMapaDESTINO.AsString;
    Mdvolumescliredes.Value := qMapaCLIREDES.AsString;
    Mdvolumeslocalentrega.Value := qMapaLOCALENTREGA.AsString;
    Mdvolumesrota.Value := qMapaROTA.AsString;
    Mdvolumesnf.Value := nf;
    Mdvolumespedido.Value := ped;
    Mdvolumesvolume.Value := vol;
    Mdvolumes.Post;
    ped := '';
    ped := QVolsORDCOM.AsString;
    vol := QVolsCODBAR.AsString;
    end;
    end;
    QVols.Next;
    end;
    Mdvolumes.Insert;
    Mdvolumesnr_romaneio.Value := qMapaNR_ROMANEIO.AsInteger;
    Mdvolumescod_ta.Value := qMapaCOD_TA.AsInteger;
    Mdvolumesdestino.Value := qMapaDESTINO.AsString;
    Mdvolumescliredes.Value := qMapaCLIREDES.AsString;
    Mdvolumeslocalentrega.Value := qMapaLOCALENTREGA.AsString;
    Mdvolumesrota.Value := qMapaROTA.AsString;
    Mdvolumesnf.Value := nf;
    Mdvolumespedido.Value := ped;
    Mdvolumesvolume.Value := vol;
    Mdvolumes.Post;
    qMapa.Next
    end;
    RlReport2.preview;
    QMapa.Close; }
end;

procedure TfrmGeraManifesto.btnMapaRomaneioClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir('frmLiberarConferencia') then
    Exit;

  Panel10.Visible := true;
  edMotivo.clear;
  edMotivo.SetFocus;
end;

procedure TfrmGeraManifesto.btnMapaRomaneioManifestoClick(Sender: TObject);
var
 // ped, vol, nf: String;
  caminho: String;
begin
  if dts = 'INTECDSV' then
    caminho := 'http://192.168.236.112/Relatorios.dsv/sim/sim0001.aspx?rom=' +
      ApCarac(mdTempromaneioNR_ROMANEIO.asstring) + '&fil=' + IntToStr(GlbFilial)
  else
    caminho := 'http://192.168.236.112/Relatorios/sim/sim0001.aspx?rom=' +
      ApCarac(mdTempromaneioNR_ROMANEIO.asstring) + '&fil=' + IntToStr(GlbFilial);

  Winexec(PAnsiChar(caminho), Sw_Show);
  ShellExecute(handle, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);
end;

procedure TfrmGeraManifesto.btnNovoManifestoClick(Sender: TObject);
begin
  if (mdTempromaneioconferencia.value <> 'Conferido') and (GlbConf = 'S') then
  begin
    ShowMessage('Mapa n�o Conferido !!');
    Exit;
  end;

  pnManifesto.Enabled := true;
  btnNovoManifesto.Enabled := false;
  // btnGerarMan.Enabled   := True;
  btnCancelaManifesto.Enabled := true;
  btnLimpaRomaneio.Enabled := false;
  dbgFatura.Enabled := false;
  DBGItens.Enabled := false;
  edVlrm.value := 0;
  JvMemo1.clear;
  mdTempromaneio.DisableControls;
  mdTempromaneio.Filtered := true;
  mdTempromaneio.First;
  while not(mdTempromaneio.eof) do
  begin
    if (mdTempromaneioselecionou.AsInteger = 1) then
    begin
      edVlrm.value := edVlrm.value + mdTempromaneiovlrmer.value;
    end;
    mdTempromaneio.next;
  end;


  mdTempromaneio.locate('selecionou', 1, []);
  mdTempromaneio.EnableControls;
  //mdTempromaneio.Filtered := false;
  cbUFO.ItemIndex := cbUFO.Items.IndexOf(v_estado);

  QCidade.close;
  QCidade.Parameters[0].value := v_estado;
  QCidade.Open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.AsString);
    QCidade.next;
  end;
  cbCidadeO.ItemIndex := cbCidadeO.Items.IndexOf(v_cidade);
  QCidade.locate('descri', v_cidade, []);
  v_codmun := QCidadeCODMUN.AsInteger;
  v_codmunD := 0;

  QTransp.close;
  QTransp.Open;

  // carregar as iscas
  QIsca.close;
  QIsca.Open;
  cbIsca1.clear;
  cbIsca2.clear;
  cbIsca1.Items.add('');
  cbIsca2.Items.add('');
  while not QIsca.eof do
  begin
    cbIsca1.Items.add(QIscaNM_ISCA.AsString);
    cbIsca2.Items.add(QIscaNM_ISCA.AsString);
    QIsca.next;
  end;

  // carregar as filiais
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add('select (f.codfil || '' - '' || f.nomeab) filial ');
  dtmDados.IQuery1.sql.add('from cyber.rodfil f left join tb_empresa e on e.codfil = f.codfil ');
  dtmDados.IQuery1.sql.add('where e.usar = ''S'' and f.codfil <> :0 order by f.codfil ');
  dtmDados.IQuery1.Parameters[0].Value := GLBFilial;
  dtmDados.IQuery1.Open;
  ParaFilial.clear;
  ParaFilial.Items.add('');
  while not dtmDados.IQuery1.eof do
  begin
    ParaFilial.Items.add(dtmDados.IQuery1.FieldByName('filial').value);
    dtmDados.IQuery1.next;
  end;
  dtmDados.IQuery1.Close;

  // se tiver rota n�o pode mudar o transportador nem placa
  if (mdTempromaneiorota.AsString <> '') then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('select distinct transportador from tb_roadnet where rota = '+QuotedStr(mdTempromaneiorota.AsString));
    dtmDados.IQuery1.open;
    if QTransp.Locate('CODIGO', dtmdados.IQuery1.FieldByName('transportador').AsInteger,[]) then
    begin
      cbtransp.Visible := false;
      labeltransp.caption := QTranspNOME.AsString;
    end;
    dtmDados.IQuery1.close;
    cbTranspExit(Sender);
    cbTransp.Enabled := false;
    edKm.Value := mdTempromaneioKM_TOTAL.Value;
  end
  else
  begin
    cbTransp.Enabled := true;
    if cbTransp.CanFocus then
      cbTransp.SetFocus;
  end;
end;

procedure TfrmGeraManifesto.btnNovoTransfClick(Sender: TObject);
begin
  pnTransf.Enabled := true;
  btnNovoTransf.Enabled := false;
  btnCanTransf.Enabled := true;
  DBTransf.Enabled := false;

  MDTransf.DisableControls;
  MDTransf.First;
  while not(MDTransf.eof) do
  begin
    if (MDTransfSelecionado.AsInteger = 1) then
    begin
      edCusto_T.value := edCusto_T.value + MDTransfcusto.value;
      edRecT.value := edRecT.value + MDTransftotfre.value;
      edPesoTransf.value := edPesoTransf.value + MDTransfPESO.value;
      edValorMT.value := edValorMT.value + MDTransfValor.value;
    end;
    MDTransf.next;
  end;
  MDTransf.locate('selecionado', 1, []);
  MDTransf.EnableControls;

  QTransp.Close;
  QTransp.Open;

  // carregar as iscas
  QIsca.close;
  QIsca.Open;
  cbIsca1T.clear;
  cbIsca2T.clear;
  cbIsca1T.Items.add('');
  cbIsca2T.Items.add('');
  while not QIsca.eof do
  begin
    cbIsca1T.Items.add(QIscaNM_ISCA.AsString);
    cbIsca2T.Items.add(QIscaNM_ISCA.AsString);
    QIsca.next;
  end;

  if DBLTransf.CanFocus then
    DBLTransf.SetFocus;
end;

procedure TfrmGeraManifesto.btnRefreshClick(Sender: TObject);
begin
  QManifesto.Close;
  QManifesto.Open;
end;

procedure TfrmGeraManifesto.btnRoadNetClick(Sender: TObject);
begin
  // enviar os CT-es para o RoadNet
  if not dtmDados.PodeInserir('RoadNet') then
    Exit;

  if mdTarefa.RecordCount = 0 then
    Exit;

  if Application.Messagebox('Enviar para o RoadNet agora ?', 'Gerar Roteiriza��o',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    mdTarefa.First;

    while not mdTarefa.eof do
    begin
      SP_Road.Parameters[0].Value := mdTarefaNR_ROMANEIO.AsInteger;
      SP_Road.Parameters[1].Value := 'I';
      SP_Road.Parameters[2].Value := GLBCodUser;
      SP_Road.Parameters[3].Value := mdTarefaNR_CNPJ_CLI.AsString;
      SP_Road.Parameters[4].Value := '';
      SP_Road.Parameters[5].Value := GLBFilial.ToString;
      SP_Road.ExecProc;
      mdTarefa.next;
    end;

    ShowMessage('Documentos enviados ao RoadNet');
    mdTarefa.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmGeraManifesto.btnSalvarClick(Sender: TObject);
begin
  if Application.Messagebox('Voc� Deseja Alterar os dados ?',
    'Salvar Dados Repom', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('update tb_manifesto set cartao_repom = :0, ciot = :1, id_operacao = :2');
    dtmdados.IQuery1.sql.add('where manifesto = :3 and filial = :4 ');
    dtmdados.IQuery1.sql.add('and serie = :5');
    dtmdados.IQuery1.Parameters[0].value := edCartao2.Text;
    dtmdados.IQuery1.Parameters[1].value := edCiot.Text;
    if rbSemAdiantamento.Checked = true then
      dtmdados.IQuery1.Parameters[2].value := '3'
    else
      dtmdados.IQuery1.Parameters[2].value := '4';
    dtmdados.IQuery1.Parameters[3].value := QManifestoMANIFESTO.AsInteger;
    dtmdados.IQuery1.Parameters[4].value := glbfilial;
    dtmdados.IQuery1.Parameters[5].value := QManifestoSERIE.AsString;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
    showMessage('Altera��o Conclu�da');
  end;
  pnlRepom.Visible := false;
end;

procedure TfrmGeraManifesto.btnSalvarCustoClick(Sender: TObject);
begin
  if EditCusto.Text = '' then
  begin
    Panel10.Visible := false;
    Exit;
  end;

  if Length(EditCusto.Text) < 10 then
  begin
    ShowMessage('O Motivo deve ter mais que 10 caracteres');
    EditCusto.SetFocus;
    Exit;
  end;

  if Application.Messagebox('Voc� Deseja Liberar o Manifesto com Custo Maior ?',
    'Liberar Manifesto', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('update tb_manifesto set motivo_liberado = :0, ');
    dtmDados.IQuery1.sql.add
      ('user_liberado = :1, data_liberado = sysdate, fl_pago = ''C'' ');
    dtmDados.IQuery1.sql.add('where manifesto = :2 and filial = :3 ');
    dtmDados.IQuery1.sql.add('and serie = :4 ');
    dtmDados.IQuery1.Parameters[0].value := EditCusto.Text;
    dtmDados.IQuery1.Parameters[1].value := GLBUSER;
    dtmDados.IQuery1.Parameters[2].value := QManifestoMANIFESTO.AsInteger;
    dtmDados.IQuery1.Parameters[3].value := QManifestoFILIAL.AsInteger;
    dtmDados.IQuery1.Parameters[4].value := QManifestoSERIE.asstring;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.close;

    { Sp_Liberado.Parameters[0].value :=  QManifestoMANIFESTO.AsInteger;
      Sp_Liberado.Parameters[1].value :=  QManifestoSERIE.AsString;
      Sp_Liberado.Parameters[2].value :=  QManifestoFILIAL.AsInteger;
      Sp_Liberado.ExecProc; }

    QManifesto.close;
    QManifesto.Open;
  end;
  Panel11.Visible := false;
end;

procedure TfrmGeraManifesto.btnSalva_motivoClick(Sender: TObject);
begin
  if edMotivo.Text = '' then
  begin
    Panel10.Visible := false;
    Exit;
  end;

  if Length(edMotivo.Text) < 10 then
  begin
    ShowMessage('O Motivo deve ter mais que 10 caracteres');
    edMotivo.SetFocus;
    Exit;
  end;

  if Panel10.Color = clRed then
  begin
    if Application.Messagebox('Voc� Deseja Liberar A Confer�ncia ?',
      'Liberar Confer�ncia', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      mdTempromaneio.edit;
      mdTempromaneioconferencia.value := 'Conferido';
      mdTempromaneio.Post;
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('update tb_romaneio set conf_liberado = :0, ');
      dtmDados.IQuery1.sql.add('user_liberado = :1, data_liberado = sysdate ');
      dtmDados.IQuery1.sql.add('where nr_romaneio = :2');
      dtmDados.IQuery1.Parameters[0].value := edMotivo.Text;
      dtmDados.IQuery1.Parameters[1].value := GLBUSER;
      dtmDados.IQuery1.Parameters[2].value := mdTempromaneioNR_ROMANEIO.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;

      // registrar a libera��o nos volumes da SKF
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('update dashboard.skf_tb_trip set user_exp = :0, data_exp = sysdate, ');
      dtmDados.IQuery1.sql.add('data_fim_exp = sysdate, fl_liberado = ''S'' ');
      dtmDados.IQuery1.sql.add('where box in (  ');
      dtmDados.IQuery1.sql.add('select distinct tp.box  ');
      dtmDados.IQuery1.sql.add('from tb_conhecimento ct left join dashboard.skf_tb_trip tp on ct.nr_conhecimento = tp.cte ');
      dtmDados.IQuery1.sql.add('where ct.nr_romaneio = :1 and ct.fl_empresa = 20 ');
      dtmDados.IQuery1.sql.add('and data_fim_exp is null and box is not null)');
      dtmDados.IQuery1.Parameters[0].value := GLBUSER;
      dtmDados.IQuery1.Parameters[1].value := mdTempromaneioNR_ROMANEIO.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;

    end;
  end
  else
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('update tb_roadnet set motivo = :0, data_liberado = sysdate, ');
    dtmDados.IQuery1.sql.add('user_liberado = :1 ');
    dtmDados.IQuery1.sql.add('where rota = :2 ');
    dtmDados.IQuery1.Parameters[0].value := edMotivo.Text;
    dtmDados.IQuery1.Parameters[1].value := GLBUSER;
    dtmDados.IQuery1.Parameters[2].value := mdTempromaneiorota.AsString;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.close;

    cbtransp.Visible := true;
    cbTransp.Enabled := true;
    if cbTransp.CanFocus then
      cbTransp.SetFocus;
  end;
  Panel10.Visible := false;
end;

procedure TfrmGeraManifesto.btnSefazClick(Sender: TObject);
var
  man: Integer;
  //custo, margem: real;
begin
  if QManifestoSERIE.value = 'U' then
  begin
    ShowMessage('Este Manifesto n�o � Eletr�nico');
    Exit;
  end;
  {
  if (QManifestoCIOT.AsString = '0') and (QManifestoSERIE.AsString = '1') then
  begin
    ShowMessage('Este Manifesto est� sem CIOT');
    Exit;
  end;
      }

  if (QManifestoPGR.AsString = 'S') and (QManifestoid_monitoramento.AsString = '') and (QManifestoSERIE.AsString = '1') then
  begin
    ShowMessage('Este Manifesto, como informado na gera��o, precisa de SM');
    Exit;
  end;

  man := QManifestoMANIFESTO.AsInteger;
  QManifesto.close;
  QManifesto.Open;
  // =================== Averba��o implementada por Hugo ========================
  try
    Averba(man, 1);
  except
    on e: Exception do
    begin

    end;
  end;

  // =================== Averba��o implementada por Hugo ========================

  if QManifesto.locate('manifesto', man, []) then
  begin
    // Controle de Margem
    // Desabilitado em 25/05/2018
    {dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select fl_margem from tb_operacao where operacao = :0 ');
    dtmDados.IQuery1.Parameters[0].value := QManifestoOPERACAO.value;
    dtmDados.IQuery1.Open;
    contmarg := dtmDados.IQuery1.FieldByName('fl_margem').asstring;
    dtmDados.IQuery1.close;
    custo := QManifestoVALOR_CUSTO.value;
    if (contmarg = 'S') and (QManifestoDATA_LIBERADO.IsNull) and
      ((QManifestoTRANSP.value <> 1145) or (QManifestoTRANSP.value <> 1018))
    then
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select sum(totfre) receita from vw_itens_manifesto_sim s  ');
      dtmDados.IQuery1.sql.add
        ('where manifesto= :0 and filial = :1 and serie = :2 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.asstring;
      dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.Open;
      receita := dtmDados.IQuery1.FieldByName('receita').AsFloat;
      dtmDados.IQuery1.close;
      if custo > 0 then
        margem := (custo / receita) * 100
      else
        margem := 0;

      if custo = 0 then
        margem := 100;

      if margem > GlbMdfecus then
      begin
        ShowMessage('O Custo/Receita est� Maior que a Permitido !! ');
        Exit;
      end;
    end;  }

    if (QManifestoVALOR_CUSTO.Value = 0) and (QManifestoDATA_LIBERADO.IsNull) then
    begin
      ShowMessage('O Custo N�o Pode Ser Zero, precisa de Libera��o !! ');
      Exit;
    end;


    if (QManifestoMDFE_STATUS.value = 204)
    // or (QManifestoMDFE_STATUS.Value = 100)
      or (QManifestoMDFE_STATUS.value = 609) then
    begin
      frmMenu.ACBrMDFe1.WebServices.Consulta.MDFeChave :=
        QManifestoMDFE_CHAVE.asstring;
      frmMenu.ACBrMDFe1.WebServices.Consulta.Executar;
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('update tb_manifesto set mdfe_protocolo = :0, ');
      dtmDados.IQuery1.sql.add('mdfe_data = :1, mdfe_status = 100, ');
      dtmDados.IQuery1.sql.add('mdfe_motivo = ''Autorizado o uso do MDF-e'', ');
      dtmDados.IQuery1.sql.add
        ('mdfe_mensagem = ''Autorizado o uso do MDF-e'', ');
      dtmDados.IQuery1.sql.add('status = ''S'' where mdfe_chave = :2 ');
      dtmDados.IQuery1.Parameters[0].value :=
        frmMenu.ACBrMDFe1.WebServices.Consulta.protocolo;
      dtmDados.IQuery1.Parameters[1].value :=
        frmMenu.ACBrMDFe1.WebServices.Consulta.DhRecbto;
      dtmDados.IQuery1.Parameters[2].value := QManifestoMDFE_CHAVE.asstring;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
    end
    else
    begin
      // verifica se a tb_manitem est� ok
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select * from tb_manitem where manifesto = :0 and serie = ''1'' and filial = :1 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.Open;
      if dtmDados.IQuery1.eof then
      begin
        SPItens.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        SPItens.Parameters[1].value := QManifestoFILIAL.AsInteger;
        Sp_Liberado.ExecProc;
      end;
      dtmDados.IQuery1.close;

      // verifica se tem algum manifesto sem averba��o
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select count(*) tem from tb_conhecimento cte where fl_tipo = ''C'' and fl_status =''A'' and cte.fl_tipo_2 in(''R'',''N'') and cte.cte_chave is not null ');
      dtmDados.IQuery1.sql.add
        ('and averba_protocolo is null and nr_manifesto = :0 and cte.filial_man = :1 and cte.serie_man = ''1'' order by 1 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.Open;
      if dtmDados.IQuery1.FieldByName('tem').AsInteger > 0 then
      begin
        ShowMessage('Existem CT-es neste Manifesto que n�o foram Averbados !!');
        exit;
      end;


      frmMenu.geramdfe(QManifestoMANIFESTO.AsInteger);
    end;


    Try
      Application.CreateForm(TfrmDiario_bordo, frmDiario_bordo);
      frmDiario_bordo.AdoInfcrm.close;
      frmDiario_bordo.AdoInfcrm.Parameters[0].value :=
        QManifestoMANIFESTO.AsInteger;
      frmDiario_bordo.AdoInfcrm.Parameters[1].value := '1';
      frmDiario_bordo.AdoInfcrm.Open;
      if not frmDiario_bordo.AdoInfcrm.IsEmpty then
        frmDiario_bordo.RLReport2.Print;
    finally
      frmDiario_bordo.Free;
    end;

    QManifesto.close;
    QManifesto.Open;
    QManifesto.locate('manifesto', man, []);
  end;
end;

procedure TfrmGeraManifesto.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrMDFe1.WebServices.StatusServico.Executar;
  MStat.Lines.Text :=
    UTF8Encode(frmMenu.ACBrMDFe1.WebServices.StatusServico.RetWS);
  MStat.Visible := true;
  MStat.clear;
  MStat.Lines.add('Status Servi�o');
  MStat.Lines.add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.tpAmb));
  MStat.Lines.add('verAplic: ' + frmMenu.ACBrMDFe1.WebServices.
    StatusServico.verAplic);
  MStat.Lines.add('cStat: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.cStat));
  MStat.Lines.add('xMotivo: ' + frmMenu.ACBrMDFe1.WebServices.
    StatusServico.xMotivo);
  MStat.Lines.add('cUF: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.cUF));
  MStat.Lines.add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrMDFe1.WebServices.StatusServico.DhRecbto));
  MStat.Lines.add('tMed: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.TMed));
  MStat.Lines.add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrMDFe1.WebServices.StatusServico.dhRetorno));
  MStat.Lines.add('xObs: ' + frmMenu.ACBrMDFe1.WebServices.StatusServico.xObs);
end;

procedure TfrmGeraManifesto.btnTranspClick(Sender: TObject);
begin
  Panel10.Color := clYellow;
  Panel10.Visible := true;
  edMotivo.clear;
  edMotivo.SetFocus;
end;

procedure TfrmGeraManifesto.btnVoltaTarefaClick(Sender: TObject);
begin
  iRecDelete := mdTarefaCODCON.AsInteger;

  mdTemp.locate('Conca', mdTarefaCONCA.asstring, []);

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
  qrCalcula_Frete.Open;

  edTotCubadoTarefa.value := edTotCubadoTarefa.value -
    qrCalcula_Frete.FieldByName('cub').AsFloat;
  edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
    ('peso').AsFloat;
  edTotValorTarefa.value := edTotValorTarefa.value - qrCalcula_Frete.FieldByName
    ('valor').AsFloat;
  edTotVolumeTarefa.value := edTotVolumeTarefa.value -
    qrCalcula_Frete.FieldByName('qtd').AsFloat;
  edTotFreteTarefa.value := edTotFreteTarefa.value - qrCalcula_Frete.FieldByName
    ('frete').AsFloat;
  //
  mdTemp.edit;
  mdTempInserida.value := 0;
  mdTemp.Post;

  CarregaBtn('');
end;

procedure TfrmGeraManifesto.btRomaneioClick(Sender: TObject);
var
  ufd, ori, des: string;
  peso: real;
  irom: Integer;
  // bExisteTarefa: Boolean;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if mdTarefa.RecordCount = 0 then
    Exit;

  mdTarefa.First;
  while not mdTarefa.eof do
  begin
    if (mdTarefatem.value = -1) and (mdTarefapalete.Value <=0) then
    begin
      showMessage('Existem Destinat�rios que precisam da quantidade de Palete, favor informar !!');
      exit;
    end;
    mdTarefa.Next;
  end;
  ori := '';
  des := '';

  if Application.Messagebox('Gerar Mapa agora ?', 'Gerar Mapa',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    mdTarefa.First;
    ufd := '';
    irom := 0;
    while not mdTarefa.eof do
    begin
      if irom = 0 then
      begin
        SP_ROMANEIO.Parameters[1].value := irom;
        SP_ROMANEIO.Parameters[2].value := irom;
        SP_ROMANEIO.Parameters[3].value := '';
        SP_ROMANEIO.Parameters[4].value := 0;
        SP_ROMANEIO.Parameters[5].value := GlbFilial;
        SP_ROMANEIO.Parameters[6].value := mdTarefaUF_DESTINO.value;
        SP_ROMANEIO.Parameters[7].value := 0.00;
        SP_ROMANEIO.Parameters[8].value := 'I';
        SP_ROMANEIO.Parameters[9].value := GLBUSER;
        SP_ROMANEIO.ExecProc;
        irom := SP_ROMANEIO.Parameters[0].value;
      end;
      ufd := mdTarefaUF_DESTINO.AsString;
      // iQtdRom := iQtdRom + 1;
      peso := 0;
      if mdTarefaDOC.value = 'CTE' then
      begin
        { dtmdados.IQuery1.close;
          dtmdados.IQuery1.sql.clear;
          dtmdados.IQuery1.SQL.add('update cyber.rodcon set agecar = :0 ');
          dtmdados.IQuery1.SQL.add('where codcon = :1 and codfil = :2 ');
          dtmdados.IQuery1.Parameters[0].value := irom;
          dtmdados.IQuery1.Parameters[1].value := mdTarefaCODCON.AsInteger;
          dtmdados.IQuery1.Parameters[2].value := mdTarefaFL_EMPRESA.AsInteger;
          dtmdados.IQuery1.ExecSQL; }

        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.clear;
        dtmDados.IQuery1.sql.add
          ('update tb_conhecimento set nr_romaneio = :0 ');
        dtmDados.IQuery1.sql.add
          ('where nr_conhecimento = :1 and fl_empresa = :2 ');
        dtmDados.IQuery1.Parameters[0].value := irom;
        dtmDados.IQuery1.Parameters[1].value := mdTarefaCODCON.AsInteger;
        dtmDados.IQuery1.Parameters[2].value := mdTarefaFL_EMPRESA.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;

      end
      else
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.clear;
        dtmDados.IQuery1.sql.add('update cyber.rodord set agecar = :0 ');
        dtmDados.IQuery1.sql.add('where codigo = :1 and codfil = :2 ');
        dtmDados.IQuery1.Parameters[0].value := irom;
        dtmDados.IQuery1.Parameters[1].value := mdTarefaCODCON.AsInteger;
        dtmDados.IQuery1.Parameters[2].value := mdTarefaFL_EMPRESA.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
      end;

      // Cadastrar palete do Mapa
      if mdTarefapalete.Value > 0 then
      begin
        if (ori = ApCarac(mdTarefaNR_CNPJ_CLI.AsString))
         and (des = ApCarac(mdTarefacnpj_dest.AsString)) then
        begin
          dtmdados.iQuery1.close;
          dtmdados.IQuery1.sql.clear;
          dtmdados.IQuery1.sql.add('update tb_palet_romaneio set qt_palete = (qt_palete + :0) ');
          dtmdados.IQuery1.sql.add('where numero = :1 and tipo = ''M'' ');
          dtmdados.IQuery1.Parameters[0].value := mdTarefapalete.AsInteger;
          dtmdados.IQuery1.Parameters[1].value := irom;
          dtmdados.IQuery1.ExecSQL;
          dtmdados.iQuery1.close;
        end
        else
        begin
          dtmdados.iQuery1.close;
          dtmdados.IQuery1.sql.clear;
          dtmdados.IQuery1.sql.add('insert into tb_palet_romaneio (numero, ');
          dtmdados.IQuery1.sql.add('cnpj_destino, cnpj_cliente, qt_palete, tipo, ');
          dtmdados.IQuery1.sql.add('fl_empresa, usuario, cte, filial_cte) ');
          dtmdados.IQuery1.sql.add('values (:0, :1, :2, :3, ''M'', :4, :5, :6, :7)');
          dtmdados.IQuery1.Parameters[0].value := irom;
          dtmdados.IQuery1.Parameters[1].value := ApCarac(mdTarefacnpj_dest.AsString);
          dtmdados.IQuery1.Parameters[2].value := ApCarac(mdTarefaNR_CNPJ_CLI.AsString);
          dtmdados.IQuery1.Parameters[3].value := mdTarefapalete.AsInteger;
          dtmdados.IQuery1.Parameters[4].value := GLBFilial;
          dtmdados.IQuery1.Parameters[5].value := GLBUSER;
          dtmdados.IQuery1.Parameters[6].value := mdTarefaCODCON.AsInteger;
          dtmdados.IQuery1.Parameters[7].value := mdTarefaFL_EMPRESA.AsInteger;
          dtmdados.IQuery1.ExecSQL;
          dtmdados.iQuery1.close;
        end;
      end;
      ori := ApCarac(mdTarefaNR_CNPJ_CLI.AsString);
      des := ApCarac(mdTarefacnpj_dest.AsString);

      peso := peso + mdTarefaNR_PESO_TA_01.value;
      // end;
      mdTarefa.next;
    end;

    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('update tb_romaneio set peso = :0 ');
    dtmDados.IQuery1.sql.add('where nr_romaneio = :1 ');
    dtmDados.IQuery1.Parameters[0].value := peso;
    dtmDados.IQuery1.Parameters[1].value := irom;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.close;

    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Mapa ' + IntToStr(irom) + ' Gerado');
    { dtmdados.RQuery1.close;
      dtmdados.RQuery1.sql.clear;
      dtmdados.RQuery1.SQL.add('call sp_upd_vols_coletor(:0)');
      dtmdados.RQuery1.Parameters[0].value := irom;
      dtmdados.RQuery1.ExecSQL;
      dtmdados.RQuery1.close;   { }

    // Atualizar a coluna romaneio na tabela itc_man_emb

    sp_rom_itc.Parameters[0].value := irom;
    sp_rom_itc.Parameters[1].value := GlbFilial;
    sp_rom_itc.ExecProc;

    edTotPesoTarefa.value := 0;
    edTotValorTarefa.value := 0;
    edTotFreteTarefa.value := 0;
    edTotCubadoTarefa.value := 0;
    edTotVolumeTarefa.value := 0;
    PageControl1.TabIndex := 1;
    mdTarefa.close;
    btnMapaRomaneio.Enabled := true;
    alterado := '';
    // CarregaRomaneio();
    CarregaRomaneioManifesto();
  end;
end;

procedure TfrmGeraManifesto.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a, b: String;
  orig1, dest1: String;
  i, x, j, k: Integer;
  arquivokm: String;
  EntryNode, EntryType :IXmlNode;
begin
  x := 0;

  // pegar o cep da filial como ponto de partida
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select (latitu ||''+''|| longit) cep from cyber.rodfil where codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GlbFilial;
  dtmDados.IQuery1.Open;
  cep := dtmDados.IQuery1.FieldByName('cep').value;
  dtmDados.IQuery1.close;

  if QKM.RecordCount > 0 then
  begin
    while not QKM.eof do
    begin
      if x = 0 then
      begin
        orig := cep;
        orig1 := orig;
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if ApCarac(QKMGEO.AsString) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        dest1 := dest;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end;
      RestClient1.BaseURL := XMLUrl;
      RESTRequest1.Accept := 'text/xml';
      RESTRequest1.Execute;
      if RestResponse1.StatusCode = 200 then
      begin
        arquivokm := RESTResponse1.Content;
        XMLDoc.LoadFromXML(RESTResponse1.Content);
        XMLDoc.Active := true;
        EntryNode := XMLDoc.DocumentElement;
        EntryType := EntryNode.ChildNodes.First;
        for i := 0 to EntryNode.ChildNodes.Count -1 do
        begin
          if (EntryType.NodeName = 'row') then
          begin
            for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
              for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1) do
              begin
                a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName;
                if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName = 'distance' then
                begin
                  km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                    [j].ChildNodes[k].ChildNodes['text'].Text) + km;
                end;
              end;
            end;
          end;
          EntryType := EntryType.NextSibling;
        end;
      end;
      XMLDoc.Active := false;
      x := x + 1;
      QKM.next;
    end;

    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';

    RestClient1.BaseURL := XMLUrl;
    RESTRequest1.Execute;
    if RestResponse1.StatusCode = 200 then
    begin
      arquivokm := RESTResponse1.Content;
      XMLDoc.LoadFromXML(RESTResponse1.Content);
      XMLDoc.Active := true;
      EntryNode := XMLDoc.DocumentElement;
      EntryType := EntryNode.ChildNodes.First;
      for i := 0 to EntryNode.ChildNodes.Count -1 do
      begin
        if (EntryType.NodeName = 'row') then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName = 'distance' then
              begin
                km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].Text) + km;
              end;
            end;
          end;
        end;
        EntryType := EntryType.NextSibling;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

procedure TfrmGeraManifesto.CarregaBtn(aba : String);
var
  starefa: string;
begin
  if aba = 'Road' then
  begin
    starefa := '';
    starefa := RetornaTarefasSelecionadas('Road');

    if (starefa = '') then
      starefa := QuotedStr('0');

    QidRoad.close;
    QidRoad.sql[3] := 'where CONCA in (' + starefa + ')';
    QidRoad.Open;

    QidRoad.DisableControls;
    mdTarefa.Close;
    mdTarefa.Open;
    while not QidRoad.eof do
    begin
      mdTarefa.Append;
      mdTarefaNR_ROMANEIO.AsInteger := QidRoadID_ROAD.AsInteger;
      mdTarefaCODCON.Value := QidRoadCodcon.AsInteger;
      mdTarefaNM_EMP_ORI_TA.Value := QidRoadNM_EMP_ORI_TA.Value;
      mdTarefaNR_MANIFESTO.Value := QidRoadNR_MANIFESTO.AsInteger;
      mdTarefaNR_CNPJ_CLI.Value := QidRoadNR_CNPJ_CLI.Value;
      mdTarefaNM_CLI_OS.Value := QidRoadNM_CLI_OS.Value;
      mdTarefaVL_CTRC.Value := QidRoadVL_CTRC.AsFloat;
      mdTarefaNM_EMP_DEST_TA_01.Value := QidRoadNM_EMP_DEST_TA_01.Value;
      mdTarefaNR_PESO_TA_01.Value := QidRoadNR_PESO_TA_01.AsFloat;
      mdTarefaFL_EMPRESA.Value := QidRoadfl_empresa.AsInteger;
      mdTarefaUF_DESTINO.Value :=  QidRoadUF_DESTINO.Value;
      mdTarefaSUSUARIO.Value := QidRoadSUSUARIO.Value;
      mdTarefaDOC.Value := QidRoaddoc.Value;
      mdTarefaCONCA.Value := QidRoadid_road.AsString;
      mdTarefacnpj_dest.Value := QidRoadcnpj_dest.Value;
      mdTarefapalete.value := QidRoadqt_palete.AsInteger;
      mdTarefa.Post;

      if mdTarefapalete.value = 0 then
      begin
        // verifca se o destino precisa de palete
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('select fnc_utiliza_pallet(:0, :1) tem from dual');
        dtmdados.IQuery1.Parameters[0].value := mdTarefaNR_CNPJ_CLI.AsString;
        dtmdados.IQuery1.Parameters[1].value := mdTarefacnpj_dest.AsString;
        dtmdados.IQuery1.open;
        if dtmdados.IQuery1.FieldByName('tem').value > 0 then
        begin
          showmessage('Este Destino para este Cliente solicita entrega em Palete, favor informar');
          mdTarefa.Edit;
          mdTarefatem.Value := -1;
          mdTarefa.Post;
        end;
      end;
      QidRoad.Next;
    end;
    if mdTemp.RecordCount > 0 then
      btnRoadNet.Enabled := true;
    QidRoad.First;
    QidRoad.EnableControls;
    QidRoad.close;

    mdTemp.First;
  end
  else
  begin
    starefa := '';
    starefa := RetornaTarefasSelecionadas('');

    if (starefa = '') then
      starefa := QuotedStr('0');

    QTarefa.close;
    QTarefa.sql[3] := 'where v.conca in (' + starefa + ')';
    QTarefa.Open;

    QTarefa.DisableControls;
    mdTarefa.Close;
    mdTarefa.Open;
    while not QTarefa.eof do
    begin
      mdTarefa.Append;
      mdTarefaNR_ROMANEIO.Value := QTarefaNR_ROMANEIO.Value;
      mdTarefaCODCON.Value := QTarefaCodcon.Value;
      mdTarefaNM_EMP_ORI_TA.Value := QTarefaNM_EMP_ORI_TA.Value;
      mdTarefaNR_MANIFESTO.Value := QTarefaNR_MANIFESTO.Value;
      mdTarefaNR_CNPJ_CLI.Value := QTarefaNR_CNPJ_CLI.Value;
      mdTarefaNM_CLI_OS.Value := QTarefaNM_CLI_OS.Value;
      mdTarefaVL_CTRC.Value := QTarefaVL_CTRC.Value;
      mdTarefaNM_EMP_DEST_TA_01.Value := QTarefaNM_EMP_DEST_TA_01.Value;
      mdTarefaNR_PESO_TA_01.Value := QTarefaNR_PESO_TA_01.Value;
      mdTarefaFL_EMPRESA.Value := Qtarefafl_empresa.Value;
      mdTarefaUF_DESTINO.Value :=  QTarefaUF_DESTINO.Value;
      mdTarefaSUSUARIO.Value := QTarefaSUSUARIO.Value;
      mdTarefaDOC.Value := Qtarefadoc.Value;
      mdTarefaCONCA.Value := Qtarefaconca.value;
      mdTarefacnpj_dest.Value := QTarefacnpj_dest.Value;
      mdTarefapalete.value := QTarefaqt_palete.AsInteger;
      mdTarefa.Post;

      if mdTarefapalete.value = 0 then
      begin
        // verifca se o destino precisa de palete
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add('select fnc_utiliza_pallet(:0, :1) tem from dual');
        dtmdados.IQuery1.Parameters[0].value := mdTarefaNR_CNPJ_CLI.AsString;
        dtmdados.IQuery1.Parameters[1].value := mdTarefacnpj_dest.AsString;
        dtmdados.IQuery1.open;
        if dtmdados.IQuery1.FieldByName('tem').value > 0 then
        begin
          showmessage('Este Destino para este Cliente solicita entrega em Palete, favor informar');
          mdTarefa.Edit;
          mdTarefatem.Value := -1;
          mdTarefa.Post;
        end;
      end;
      QTarefa.Next;
    end;
    QTarefa.First;
    QTarefa.EnableControls;
    QTarefa.close;

    mdTemp.First;
  end;
end;

procedure TfrmGeraManifesto.CarregaRomaneio();
begin
  CarregaTarefas();
  mdTemp.close;
  mdTemp.Open;
  // carrega em temp tarefas n�o romaneadas
  QDestino.First;
  while not QDestino.eof do
  begin
    mdTemp.Append;
    mdTempInserida.value := 0;
    mdTempColeta.value := QDestinoCODCON.AsInteger;
    mdTempdestino.value := QDestinoRAZSOC.value;
    mdTempdata.value := QDestinoDATINC.value;
    mdTempfilial.value := QDestinoCODFIL.AsInteger;
    mdTempdoc.value := QDestinoDOC.value;
    mdTempconca.value := QDestinoCODCON.asstring + QDestinoCODFIL.asstring +
      QDestinoDOC.AsString;
    mdTemp.Post;
    QDestino.next;
  end;
  QDestino.close;
  CarregaBtn('');
  mdTemp.First;
end;

procedure TfrmGeraManifesto.CarregaRomaneioManifesto;
begin
  nao_atualiza := 'S';
  mdTempromaneio.close;
  mdTempromaneio.Open;
  qrRomaneio.close;
  qrRomaneio.Parameters[0].value := GlbFilial;
  qrRomaneio.Open;
  qrRomaneio.First;
  while not qrRomaneio.eof do
  begin
    mdTempromaneio.Append;
    mdTempromaneioNR_ROMANEIO.value := qrRomaneioNR_ROMANEIO.value;
    mdTempromaneioDESTINO.value := qrRomaneioUF_DESTINO.value;
    mdTempromaneioDEMISSAO.value := qrRomaneioDEMISSAO.value;
    mdTempromaneioPESO.value := qrRomaneioPESO.value;
    mdTempromaneioSUSUARIO.value := qrRomaneioUSUARIO.value;
    mdTempromaneiovlrmer.value := qrRomaneioVLRMER.value;
    mdTempromaneiototfre.value := qrRomaneioTOTFRE.value;
    mdTempromaneiorota.AsString := qrRomaneioROTA.AsString;
    mdTempromaneioSELECIONADO.AsInteger := 0;
    if qrRomaneioDATA_LIBERADO.IsNull then
    begin
      if (qrRomaneioDAT_CONFERENCIA.IsNull) and (GlbConf = 'S') then
        mdTempromaneioconferencia.value := 'Aguardando Confer�ncia'
      else
        mdTempromaneioconferencia.value := 'Conferido';
    end
    else
      mdTempromaneioconferencia.value := 'Conferido';
    mdTempromaneioKM_TOTAL.value := qrRomaneioKM_TOTAL.value;
    mdTempromaneio.Post;
    qrRomaneio.next;
  end;
  nao_atualiza := 'N';
end;

procedure TfrmGeraManifesto.CarregaTarefas();
begin
  if (ckRota.checked = false) then
  begin
    QDestino.close;
    QDestino.sql[4] := 'where v.codfil in(' + filiais + ')';
    QDestino.sql[5] := ' ';
    QDestino.sql[6] := ' and v.datinc > to_date(' + QuotedStr(edData.Text) +
      ',''dd/mm/yyyy'')';
    QDestino.Open;
    QDestino.First;
    mdTemp.close;
    mdTemp.Open;
    while not QDestino.eof do
    begin
      mdTemp.Append;
      mdTempColeta.value := QDestinoCODCON.AsInteger;
      mdTempdestino.value := QDestinoRAZSOC.value;
      mdTempdata.value := QDestinoDATINC.value;
      mdTempfilial.value := QDestinoCODFIL.AsInteger;
      mdTempdoc.value := QDestinoDOC.value;
      mdTempconca.value := QDestinoCODCON.asstring + QDestinoCODFIL.asstring +
        QDestinoDOC.AsString;
      QDestino.next;
    end;
    mdTemp.First;
  end
  else
  begin
    QDestino.close;
    QDestino.sql[4] := 'Where v.codfil in(' + filiais + ')';
    if (qrRotaROTA.asstring = 'SEM ROTA') then
    begin
      QDestino.sql[5] := ' and nvl(v.codsetcl,0) = 0 ';
    end
    else
    begin
      QDestino.sql[5] := ' and v.codsetcl = :0 ';
      QDestino.Parameters[0].value := qrRotaCODSETCL.asstring;
    end;
    QDestino.sql[6] := ' and v.datinc > to_date(' + QuotedStr(edData.Text) +
      ',''dd/mm/yyyy'')';
    // QDestino.SQL[16] := ' and t.situac <> ''C'' and t.ctepro is not null and t.datinc > to_date('+QuotedStr(eddata.Text)+',''dd/mm/yyyy'')';
    // QDestino.SQL[26] := ' and t.situac <> ''C'' and t.datinc > to_date('+QuotedStr(eddata.Text)+',''dd/mm/yyyy'')';
    // QDestino.SQL[38] := ' and ct.fl_status = ''A'' and ct.cte_prot is not null and ct.dt_conhecimento > to_date('+QuotedStr(eddata.Text)+',''dd/mm/yyyy'')';
    // QDestino.SQL[50] := ' and ct.fl_status = ''A''  and ct.cte_prot is not null and ct.dt_conhecimento > to_date('+QuotedStr(eddata.Text)+',''dd/mm/yyyy'')';
    QDestino.Open;
    QDestino.First;
    mdTemp.close;
    mdTemp.Open;
    while not QDestino.eof do
    begin
      mdTemp.Append;
      mdTempColeta.value := QDestinoCODCON.AsInteger;
      mdTempdestino.value := QDestinoRAZSOC.value;
      mdTempdata.value := QDestinoDATINC.value;
      mdTempfilial.value := QDestinoCODFIL.AsInteger;
      mdTempdoc.value := QDestinoDOC.value;
      mdTempconca.value := QDestinoCODCON.asstring + QDestinoCODFIL.asstring +
        QDestinoDOC.AsString;
      QDestino.next;
    end;
    mdTemp.First;
  end;
  Label32.caption := IntToStr(mdTemp.RecordCount) + ' Registros';
end;

procedure TfrmGeraManifesto.cbOperacaoChange(Sender: TObject);
var
  icount, iroma: Integer;
  roma: String;
  l1, l2: Double;
begin
  if cbOperacao.Text <> '' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select fl_peso, fl_viagem, fl_mdfe, fl_margem, fl_km from tb_operacao where operacao = :0 ');
    dtmDados.IQuery1.Parameters[0].value := cbOperacao.Text;
    dtmDados.IQuery1.Open;
    geramdfe := dtmDados.IQuery1.FieldByName('fl_mdfe').asstring;
    contmarg := dtmDados.IQuery1.FieldByName('fl_margem').asstring;
    gerakm := dtmDados.IQuery1.FieldByName('fl_km').asstring;


    // calcular km
    if (gerakm = 'S') and (edkm.Value = 0) then
    begin
      Screen.Cursor := crHourGlass;
      mdTempromaneio.DisableControls;
      mdTempromaneio.Filtered := true;
      mdTempromaneio.First;
      icount := 0;
      iroma := 0;
      nao_atualiza := 'S';
      while not(mdTempromaneio.eof) do
      begin
        if (mdTempromaneioselecionou.AsInteger = 1) then
        begin
          if iroma <> mdTempromaneioNR_ROMANEIO.AsInteger then
          begin
            if icount = 0 then
              roma := mdTempromaneioNR_ROMANEIO.asstring
            else
              roma := roma + ',' + mdTempromaneioNR_ROMANEIO.asstring;
          end;
          icount := icount + 1;
          iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
        end;
        mdTempromaneio.next;
      end;
      mdTempromaneio.EnableControls;
      QKM.close;
      QKM.sql[4] := 'where nr_romaneio in (' + roma + ')';
      // showmessage(qkm.sql.Text);
      // QKM.Parameters[0].value := roma;
      QKM.Open;
      km := 0;
      if not QKM.eof then
      begin
        While not QKM.eof do
        begin
          if ApCarac(QKMGEO.AsString) = '' then
          begin
            lat := '';
            lon := '';
            gravageo;
            if lat <> '' then
            begin
              l1 := StrtoFloat(BuscaTroca(lat, '.', ','));
              l2 := StrtoFloat(BuscaTroca(lon, '.', ','));
              dtmDados.IQuery1.close;
              dtmDados.IQuery1.sql.clear;
              dtmDados.IQuery1.sql.add
                ('update cyber.rodcli set latitu = :0, longit = :1 ');
              dtmDados.IQuery1.sql.add('where codclifor = :2 ');
              dtmDados.IQuery1.Parameters[0].value := l1;
              dtmDados.IQuery1.Parameters[1].value := l2;
              dtmDados.IQuery1.Parameters[2].value := QKMCODCLIFOR.AsInteger;
              dtmDados.IQuery1.ExecSQL;
              dtmDados.IQuery1.close;
            end;
          end;
          QKM.next;
        end;
        QKM.First;
        calculakm;
        if km > 0 then
          edKm.value := km
        else
          edKm.value := 0;
      end;
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfrmGeraManifesto.cbOperacaoExit(Sender: TObject);
var
  filial, ecomm: String;
begin
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select fl_filial from tb_operacao where fl_receita = ''N'' and operacao = :0 ');
  dtmDados.IQuery1.Parameters[0].value := cbOperacao.Text;
  dtmDados.IQuery1.Open;
  if dtmDados.IQuery1.FieldByName('fl_filial').value = 'S' then
    filial := 'S'
  else
    filial := 'N';
  dtmDados.IQuery1.close;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select nvl(fl_viagem,''N'') fl_viagem, fl_ecommerce from tb_operacao where operacao = :0');
  dtmDados.IQuery1.Parameters[0].value := cbOperacao.Text;
  dtmDados.IQuery1.Open;
  ecomm := dtmDados.IQuery1.FieldByName('fl_ecommerce').AsString;

  if (filial = 'S') or (cbOperacao.Text = 'DEVOLU��O') then
  begin
    cbParaFilial.checked := true;
    cbUF.Visible := true;
    cbCidaded.Visible := true;
    Label42.Visible := true;
    Label44.Visible := true;
  end
  else
  begin
    cbParaFilial.checked := false;
    cbUF.Visible := false;
    cbCidaded.Visible := false;
    Label42.Visible := false;
    Label44.Visible := false;
  end;

  if filial = 'S' then
  begin
    cbParaFilial.Visible := true;
    cbParaFilial.checked := true;
    ParaFilial.Visible := true;
    Label53.Visible := true;
    cbUF.Visible := false;
    cbCidaded.Visible := false;
    Label42.Visible := false;
    Label44.Visible := false;
  end
  else
  begin
    ParaFilial.Visible := false;
    Label53.Visible := false;
    cbParaFilial.Visible := false;
    cbParaFilial.checked := false;
    if (cbOperacao.Text <> 'DISTRIBUI��O KM') and (ecomm = 'N') then
    begin
      cbUF.Visible := true;
      cbCidaded.Visible := true;
      Label42.Visible := true;
      Label44.Visible := true;
    end;
  end;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select nvl(fl_viagem,''N'') fl_viagem from tb_operacao where operacao = :0');
  dtmDados.IQuery1.Parameters[0].value := cbOperacao.Text;
  dtmDados.IQuery1.Open;

  // tem rota o ve�culo � fixo
  if mdTempromaneiorota.AsString <> '' then
  begin
    if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.close;
      QVeiculoCasa.Open;
      cbPlaca.clear;
      cbPlaca.Items.add('');
      while not QVeiculoCasa.eof do
      begin
        cbPlaca.Items.add(QVeiculoCasaPLACA.AsString);
        QVeiculoCasa.next;
      end;
      // marca que n�o controla custo
      contmarg := 'N';

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('select distinct veiculo from tb_roadnet where rota = '+QuotedStr(mdTempromaneiorota.AsString));
      dtmDados.IQuery1.open;

      if QVeiculoCasa.Locate('PLACA', dtmdados.IQuery1.FieldByName('veiculo').AsString,[]) then
      begin
        cbplaca.ItemIndex := cbplaca.Items.IndexOf(dtmdados.IQuery1.FieldByName('veiculo').AsString);
      end;
      dtmDados.IQuery1.close;

    end
    else
    begin
      QVeiculo.close;
      dtmDados.IQuery1.close;
      QVeiculo.Parameters[0].value := QTranspCODIGO.AsInteger;
      QVeiculo.Open;
      cbPlaca.clear;
      cbPlaca.Items.add('');
      while not QVeiculo.eof do
      begin
        cbPlaca.Items.add(QVeiculoPLACA.AsString);
        QVeiculo.next;
      end;

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('select distinct veiculo from tb_roadnet where rota = '+QuotedStr(mdTempromaneiorota.AsString));
      dtmDados.IQuery1.open;

      if QVeiculo.Locate('PLACA', dtmdados.IQuery1.FieldByName('veiculo').AsString,[]) then
      begin
        cbplaca.ItemIndex := cbplaca.Items.IndexOf(dtmdados.IQuery1.FieldByName('veiculo').AsString);
      end;
      dtmDados.IQuery1.close;
    end;

    cbMotorista.SetFocus;

  end
  else
  begin
    cbplaca.Enabled := true;
    if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
    begin
      QVeiculoCasa.close;
      QVeiculoCasa.Open;
      cbPlaca.clear;
      cbPlaca.Items.add('');
      while not QVeiculoCasa.eof do
      begin
        cbPlaca.Items.add(QVeiculoCasaPLACA.AsString);
        QVeiculoCasa.next;
      end;
      // marca que n�o controla custo
      contmarg := 'N';
    end
    else
    begin
      QVeiculo.close;
      dtmDados.IQuery1.close;
      QVeiculo.Parameters[0].value := QTranspCODIGO.AsInteger;
      QVeiculo.Open;
      cbPlaca.clear;
      cbPlaca.Items.add('');
      while not QVeiculo.eof do
      begin
        cbPlaca.Items.add(QVeiculoPLACA.AsString);
        QVeiculo.next;
      end;
    end;
  end;

  cbPlaca_carreta.clear;
  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.clear;
  dtmDados.RQuery1.sql.add
    ('select codvei, numvei placa_carreta, codpro ');
  dtmDados.RQuery1.sql.add
    ('from cyber.rodvei v where v.tipvei = 7 and v.codclifor = :0 ');
  dtmDados.RQuery1.sql.add
    ('and vendua >= sysdate and datant >= sysdate and datgar >= sysdate ');
  dtmDados.RQuery1.sql.add
    ('order by 2');
  dtmDados.RQuery1.Parameters[0].value := QTranspCODIGO.AsInteger;
  dtmDados.RQuery1.Open;
  cbPlaca_carreta.Items.add('');
  while not dtmDados.RQuery1.eof do
  begin
    cbPlaca_carreta.Items.add(dtmDados.RQuery1.FieldByName
      ('placa_carreta').value);
    dtmDados.RQuery1.next;
  end;
  dtmDados.RQuery1.close;

  if filial = 'S' then
    ParaFilial.SetFocus
  else
    cbMotorista.SetFocus;

end;

procedure TfrmGeraManifesto.cbOperacaoTransfChange(Sender: TObject);
begin
  if cbOperacaoTransf.Text <> '' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select fl_peso, fl_viagem, fl_mdfe, fl_margem, fl_km from tb_operacao where operacao = :0 ');
    dtmDados.IQuery1.Parameters[0].value := cbOperacaoTransf.Text;
    dtmDados.IQuery1.Open;
    geramdfe := dtmDados.IQuery1.FieldByName('fl_mdfe').asstring;
    contmarg := dtmDados.IQuery1.FieldByName('fl_margem').asstring;
    gerakm := dtmDados.IQuery1.FieldByName('fl_km').asstring;
   {
    // calcular km
    if gerakm = 'S' then
    begin
      Screen.Cursor := crHourGlass;
      mdTempromaneio.DisableControls;
      mdTempromaneio.Filtered := true;
      mdTempromaneio.First;
      icount := 0;
      iroma := 0;
      nao_atualiza := 'S';
      while not(mdTempromaneio.eof) do
      begin
        if (mdTempromaneioselecionou.AsInteger = 1) then
        begin
          if iroma <> mdTempromaneioNR_ROMANEIO.AsInteger then
          begin
            if icount = 0 then
              roma := mdTempromaneioNR_ROMANEIO.asstring
            else
              roma := roma + ',' + mdTempromaneioNR_ROMANEIO.asstring;
          end;
          icount := icount + 1;
          iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
        end;
        mdTempromaneio.next;
      end;
      mdTempromaneio.EnableControls;
      QKM.close;
      QKM.sql[4] := 'where nr_romaneio in (' + roma + ')';
      // showmessage(qkm.sql.Text);
      // QKM.Parameters[0].value := roma;
      QKM.Open;
      km := 0;
      if not QKM.eof then
      begin
        While not QKM.eof do
        begin
          if ApCarac(QKMGEO.value) = '' then
          begin
            lat := '';
            lon := '';
            gravageo;
            if lat <> '' then
            begin
              l1 := StrtoFloat(BuscaTroca(lat, '.', ','));
              l2 := StrtoFloat(BuscaTroca(lon, '.', ','));
              dtmDados.IQuery1.close;
              dtmDados.IQuery1.sql.clear;
              dtmDados.IQuery1.sql.add
                ('update cyber.rodcli set latitu = :0, longit = :1 ');
              dtmDados.IQuery1.sql.add('where codclifor = :2 ');
              dtmDados.IQuery1.Parameters[0].value := l1;
              dtmDados.IQuery1.Parameters[1].value := l2;
              dtmDados.IQuery1.Parameters[2].value := QKMCODCLIFOR.AsInteger;
              dtmDados.IQuery1.ExecSQL;
              dtmDados.IQuery1.close;
            end;
          end;
          QKM.next;
        end;
        QKM.First;
        calculakm;
        if km > 0 then
          edKm.value := km
        else
          edKm.value := 0;
      end;
      Screen.Cursor := crDefault;
    end; }
  end;
end;

procedure TfrmGeraManifesto.cbOperacaoTransfExit(Sender: TObject);
begin
  if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
  begin
    QVeiculoCasa.close;
    QVeiculoCasa.Open;
    cbPlacaTransf.clear;
    cbPlacaTransf.Items.add('');
    while not QVeiculoCasa.eof do
    begin
      cbPlacaTransf.Items.add(QVeiculoCasaPLACA.AsString);
      QVeiculoCasa.next;
    end;
    // marca que n�o controla custo
    contmarg := 'N';
  end
  else
  begin
    QVeiculo.close;
    QVeiculo.Parameters[0].value := QTranspCODIGO.AsInteger;
    QVeiculo.Open;
    cbPlacaTransf.clear;
    cbPlacaTransf.Items.add('');
    while not QVeiculo.eof do
    begin
      cbPlacaTransf.Items.add(QVeiculoPLACA.AsString);
      QVeiculo.next;
    end;
  end;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.clear;
  dtmDados.RQuery1.sql.add
    ('select codvei, numvei placa_carreta from cyber.rodvei where tipvei = 7 order by 2');
  dtmDados.RQuery1.Open;
  cbCarretaTransf.Items.add('');
  while not dtmDados.RQuery1.eof do
  begin
    cbCarretaTransf.Items.add(dtmDados.RQuery1.FieldByName
      ('placa_carreta').value);
    dtmDados.RQuery1.next;
  end;
  dtmDados.RQuery1.close;
end;

procedure TfrmGeraManifesto.cbParaFilialExit(Sender: TObject);
begin
  if cbParaFilial.Checked = true then
  begin
    ParaFilial.Visible := true;
    Label53.Visible := true;
    Label42.Visible := false;
    cbUF.Visible := false;
    Label44.Visible := false;
    cbCidaded.Visible := false;
  end
  else
  begin
    ParaFilial.Visible := false;
    Label53.Visible := false;
    Label42.Visible := true;
    cbUF.Visible := true;
    Label44.Visible := true;
    cbCidaded.Visible := true;
  end;
end;

procedure TfrmGeraManifesto.cbPesquisaChange(Sender: TObject);
begin
  cbFiltro.checked := false;
  mdTemp.Filtered := false;
end;

procedure TfrmGeraManifesto.cbplacaChange(Sender: TObject);
var
  custofrac: Double;
  manifesto: Integer;
begin
  custofrac := 0;
  btnGerarMan.Enabled := false;

  if ((cbOperacao.Text = 'TRANSFER�NCIA') or (cbOperacao.Text = 'DEVOLU��O'))
    and (cbCidaded.Text = '') then
  begin
    ShowMessage('Para esta opera��o � necess�rio informar a cidade destino');
    cbUF.SetFocus;
    cbPlaca.ItemIndex := -1;
    Exit;
  end;

  if cbPlaca.Text <> '' then
  begin
    // Faz valida��o dos vencimentos //
    // Projeto 0078 - Bloqueio de ve�culos
    SP_valida_veiculo.Parameters[1].Value := cbPlaca.text;
    SP_valida_veiculo.ExecProc;
    if SP_valida_veiculo.Parameters[0].value <> null then
    begin
      showmessage(SP_valida_veiculo.Parameters[0].Value);
      exit;
    end;

    if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
    begin
      edVeiculo.Text := QVeiculoCasaDESCRI.AsString;
      edPeso.value := 0;
      if edVeiculo.Text = 'CAVALO MECANICO' then
      begin
        cbPlaca_carreta.Enabled := true;
      end
      else
      begin
        edPesoLimiteVeiculo.value := QVeiculoCasaKG.value;
        edQtdCubagem.value := QVeiculoCasaCUB.value;
      end;
      contmarg := 'N';
    end
    else
    begin
      QVeiculo.locate('placa', cbPlaca.Text, []);
      edVeiculo.Text := QVeiculoDESCRI.AsString;
      edPeso.value := 0;
      if edVeiculo.Text = 'CAVALO MECANICO' then
      begin
        cbPlaca_carreta.Enabled := true;
      end
      else
      begin
        edPesoLimiteVeiculo.value := QVeiculoKG.value;
        edQtdCubagem.value := QVeiculoCUB.value;
      end;
    end;

    edReceita.value := 0;
    mdTempromaneio.DisableControls;
    mdTempromaneio.Filtered := true;
    mdTempromaneio.First;
    manifesto := 0;
    while not (mdTempromaneio.eof) do
    begin
      if (mdTempromaneioselecionou.AsInteger = 1) then
      begin
        edPeso.value := edPeso.value + mdTempromaneioPESO.value;
        edReceita.value := edReceita.value + mdTempromaneiototfre.value;
        if (manifesto <> mdTempromaneioNR_ROMANEIO.AsInteger) then
        begin
          QCusto.close;
          QCusto.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
          QCusto.Parameters[1].value := cbOperacao.Text;
          QCusto.Parameters[2].value := cbPlaca.Text;
          QCusto.Parameters[3].value := QTranspCODIGO.value;
          QCusto.Parameters[4].value := v_codmun;
          QCusto.Parameters[5].value := v_codmunD;
          QCusto.Parameters[6].value := edKm.value;
          QCusto.Open;
          custofrac := custofrac + QCustoCUSTO.value;
          QCusto.close;
        end;
        manifesto := mdTempromaneioNR_ROMANEIO.AsInteger;
      end;
      mdTempromaneio.next;
    end;

    mdTempromaneio.locate('selecionou', 1, []);
    mdTempromaneio.EnableControls;
    if edPesoLimiteVeiculo.value > 0 then
      edPercentCarga.value := (edPeso.value / edPesoLimiteVeiculo.value) * 100
    else
      edPercentCarga.value := 0;
    if edPercentCarga.value < 50 then
      edPercentCarga.Color := clred;

    if edPercentCarga.value > 70 then
      edPercentCarga.Color := $000080FF;

    if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
      contmarg := 'N';

    QCusto.close;
    QCusto.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QCusto.Parameters[1].value := cbOperacao.Text;
    QCusto.Parameters[2].value := cbPlaca.Text;
    QCusto.Parameters[3].value := QTranspCODIGO.value;
    QCusto.Parameters[4].value := v_codmun;
    QCusto.Parameters[5].value := v_codmunD;
    QCusto.Open;
    edCusto.value := QCustoCUSTO.value;
    QCusto.close;

    if (edCusto.value > 0) and (edReceita.value > 0) then
      edMargem.value := (edCusto.value / edReceita.value) * 100
    else
      edMargem.value := 0;

    if edCusto.value = 0 then
      edMargem.value := 100;

    if edCusto.value = 0 then
    begin
      if Application.Messagebox
        ('O Custo est� Zero, Continuar ?',
        'Gerar Manifesto', MB_YESNO + MB_ICONQUESTION) = IDNO then
        Exit
    end;
    btnGerarMan.Enabled := true;

  end;

end;

procedure TfrmGeraManifesto.cbPlacaTransfChange(Sender: TObject);
var
  receita: real;
begin
  btnGeraTransf.Enabled := false;
  if cbPlacaTransf.Text <> '' then
  begin
    // Faz valida��o dos vencimentos //
    // Projeto 0078 - Bloqueio de ve�culos
    SP_valida_veiculo.Parameters[1].Value := cbPlacaTransf.text;
    SP_valida_veiculo.ExecProc;
    if SP_valida_veiculo.Parameters[0].value <> null then
    begin
      showmessage(SP_valida_veiculo.Parameters[0].Value);
      exit;
    end;

    QVeiculo.locate('placa', cbPlacaTransf.Text, []);
    MDTransf.DisableControls;
    edVeiculoTRansf.Text := QVeiculoDESCRI.AsString;
    edPesoTransf.value := 0;
    edCapPesoT.value := QVeiculoKG.value;
    edCapCubT.value := QVeiculoCUB.value;
    receita := 0;
    MDTransf.First;
    while not(MDTransf.eof) do
    begin
      if (MDTransfSelecionado.AsInteger = 1) then
      begin
        edPesoTransf.value := edPesoTransf.value + MDTransfPESO.value;
        receita := receita + MDTransftotfre.value;
        dtmDados.IQuery1.close;
      end;
      MDTransf.next;
    end;
    MDTransf.EnableControls;
    if edCapPesoT.value > 0 then
      edCargaT.value := (edPesoTransf.value / edCapPesoT.value) * 100
    else
      edCargaT.value := 0;
    if edCapPesoT.value < 50 then
      edCapPesoT.Color := clred;

    if edCapPesoT.value > 70 then
      edCapPesoT.Color := $000080FF;

    if copy(QTranspNOME.value, 1, 7) = 'INTECOM' then
      contmarg := 'N';
  end;
end;

procedure TfrmGeraManifesto.cbPlaca_carretaChange(Sender: TObject);
begin
  if cbPlaca_carreta.Text <> '' then
  begin
    dtmDados.RQuery1.close;
    dtmDados.RQuery1.sql.clear;
    dtmDados.RQuery1.sql.add
      ('select nvl(capaci,0) kg, nvl(capam3,0) cub from cyber.rodvei ');
    dtmDados.RQuery1.sql.add('where situac = 1 and numvei = :0');
    dtmDados.RQuery1.Parameters[0].value := cbPlaca_carreta.Text;
    dtmDados.RQuery1.Open;

    edPeso.value := 0;
    edPesoLimiteVeiculo.value := dtmDados.RQuery1.FieldByName('kg').value;
    edQtdCubagem.value := dtmDados.RQuery1.FieldByName('cub').value;

    if edPesoLimiteVeiculo.value > 0 then
      edPercentCarga.value := (edPeso.value / edPesoLimiteVeiculo.value) * 100
    else
      edPercentCarga.value := 0;
    if edPercentCarga.value < 50 then
      edPercentCarga.Color := clred;

    if edPercentCarga.value > 70 then
      edPercentCarga.Color := $000080FF;
  end;
end;

procedure TfrmGeraManifesto.cbTodosClick(Sender: TObject);
begin
  if cbTodos.Checked then
  begin
    JvCalcEdit4.value := 0;
    JvCalcEdit1.value := 0;
    JvCalcEdit2.value := 0;
    JvCalcEdit5.value := 0;
    JvCalcEdit3.value := 0;
    JvCalcEdit6.value := 0;
    mdTemp.First;
    while not mdTemp.Eof do
    begin
      qrCalcula_Frete.close;
      qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
      qrCalcula_Frete.Open;
      mdTemp.edit;
      reg := reg + 1;
      mdTempreg.value := reg;
      mdTempInserida.value := 1;
      JvCalcEdit4.value := JvCalcEdit4.value +
        qrCalcula_Frete.FieldByName('cub').AsFloat;
      JvCalcEdit1.value := JvCalcEdit1.value + qrCalcula_Frete.FieldByName
        ('peso').AsFloat;
      JvCalcEdit2.value := JvCalcEdit2.value +
        qrCalcula_Frete.FieldByName('valor').AsFloat;
      JvCalcEdit5.value := JvCalcEdit5.value +
        qrCalcula_Frete.FieldByName('qtd').AsFloat;
      JvCalcEdit3.value := JvCalcEdit3.value +
        qrCalcula_Frete.FieldByName('frete').AsFloat;
      JvCalcEdit6.value := JvCalcEdit6.value + 1;
      mdTemp.Post;
      mdTemp.Next;
    end;
  end
  else
  begin
    mdTemp.First;
    while not mdTemp.Eof do
    begin
      mdTemp.edit;
      mdTempreg.Value := 0;
      mdTemp.Post;
      mdTemp.Next;
    end;
    JvCalcEdit4.value := 0;
    JvCalcEdit1.value := 0;
    JvCalcEdit2.value := 0;
    JvCalcEdit5.value := 0;
    JvCalcEdit3.value := 0;
    JvCalcEdit6.value := 0;
  end;
end;

procedure TfrmGeraManifesto.cbTranspExit(Sender: TObject);
begin
  // Carrega as opera��es
  QOperacao.Close;
  QOperacao.Parameters[0].Value := QTranspCODIGO.AsInteger;
  QOperacao.Parameters[1].Value := QTranspCODIGO.AsInteger;
  QOperacao.Open;
  cbOperacao.clear;
  cbOperacao.Items.add('');
  while not QOperacao.eof do
  begin
    cbOperacao.Items.add
      (UpperCase(QOperacaoOPERACAO.AsString));
    QOperacao.next;
  end;
  QOperacao.close;

  QMotor.close;
  QMotor.Parameters[0].value := QTranspCODIGO.AsInteger;
  QMotor.Open;
  cbMotorista.clear;
  while not QMotor.eof do
  begin
    cbMotorista.Items.add(QMotorNOME.AsString);
    QMotor.next;
  end;
  QMotor.close;

  // se tiver rota n�o pode mudar o transportador nem placa
  if mdTempromaneiorota.AsString <> '' then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add('select distinct (codmot ||'' - ''||nommot) nome from tb_roadnet r left join cyber.rodmot m on r.motorista = m.codmot where rota = ' + QuotedStr(mdTempromaneiorota.AsString));
    dtmDados.IQuery1.open;
    cbMotorista.ItemIndex := cbMotorista.Items.IndexOf(dtmDados.IQuery1.FieldByName('nome').AsString);
    dtmDados.IQuery1.close;
  end;

end;

procedure TfrmGeraManifesto.cbUFDExit(Sender: TObject);
begin
  QCidade.close;
  QCidade.Parameters[0].value := cbUFD.Text;
  QCidade.Open;
  cbCidadedt.clear;
  cbCidadedt.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadedt.Items.add(QCidadeDESCRI.AsString);
    QCidade.next;
  end;
end;

procedure TfrmGeraManifesto.cbUFExit(Sender: TObject);
begin
  QCidade.close;
  QCidade.Parameters[0].value := cbUF.Text;
  QCidade.Open;
  cbCidaded.clear;
  cbCidaded.Items.add('');
  while not QCidade.eof do
  begin
    cbCidaded.Items.add(QCidadeDESCRI.AsString);
    QCidade.next;
  end;
end;

procedure TfrmGeraManifesto.cbUFOExit(Sender: TObject);
begin
  QCidade.close;
  QCidade.Parameters[0].value := cbUFO.Text;
  QCidade.Open;
  cbCidadeO.clear;
  cbCidadeO.Items.add('');
  while not QCidade.eof do
  begin
    cbCidadeO.Items.add(QCidadeDESCRI.AsString);
    QCidade.next;
  end;
end;

procedure TfrmGeraManifesto.cbCidadedExit(Sender: TObject);
begin
  QCidade.locate('descri', cbCidaded.Text, []);
  v_codmunD := QCidadeCODMUN.AsInteger;
  cbplacaChange(sender);
end;

procedure TfrmGeraManifesto.cbCidadeDtChange(Sender: TObject);
begin

  QCidade.locate('descri', cbCidadedT.Text, []);
  v_codmunD := QCidadeCODMUN.AsInteger;

  // pega o c�digo da cidade da filial
  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.clear;
  dtmDados.RQuery1.sql.add('select codmun from rodfil where codfil = :0 ');
  dtmDados.RQuery1.Parameters[0].value := GLBFilial;
  dtmDados.RQuery1.Open;
  v_codmun := dtmDados.RQuery1.FieldByName('codmun').value;
// calculo do custo
  QCustoT.close;
  QCustoT.Parameters[0].value := cbOperacaoTransf.Text;
  QCustoT.Parameters[1].value := cbPlacaTransf.Text;
  QCustoT.Parameters[2].value := QTranspCODIGO.value;
  QCustoT.Parameters[3].value := v_codmun;
  QCustoT.Parameters[4].value := v_codmunD;
  QCustoT.Parameters[5].value := 0;
  QCustoT.Parameters[6].value := edPesoTransf.Value;
  QCustoT.Parameters[7].value := edValorMT.Value;
  QCustoT.Parameters[8].value := edRecT.value;
  QCustoT.Open;
  edCustoT.value := QCustoTCUSTO.AsFloat;
  QCusto.close;

  if (edCusto.value > 0) and (edReceita.value > 0) then
    edMargem.value := (edCusto.value / edReceita.value) * 100
  else
    edMargem.value := 0;

  if edCusto.value = 0 then
    edMargem.value := 100;

  if edCustoT.Value = 0 then
  begin
    edCustoT.Color := clred;
    ShowMessage('N�o pode ter custo Zero');
    Exit;
  end;

  btnGeraTransf.Enabled := true;

end;

procedure TfrmGeraManifesto.cbCidadeOExit(Sender: TObject);
begin
  QCidade.locate('descri', cbCidadeO.Text, []);
  v_codmun := QCidadeCODMUN.AsInteger;
end;

procedure TfrmGeraManifesto.cbCteClick(Sender: TObject);
begin
  if (cbCte.checked = false) then
  begin
    edChave.Visible := false;
    edChave.Color := clWhite;
  end
  else
  begin
    edChave.Visible := true;
    edChave.Color := clYellow;
    edChave.SetFocus;
  end;
end;

procedure TfrmGeraManifesto.cbFilialChange(Sender: TObject);
begin
  if StrToInt(RemoveChar(cbFilial.Text)) > 0 then
  begin
    dtmDados.RQuery1.close;
    dtmDados.RQuery1.sql.clear;
    dtmDados.RQuery1.sql.add
      ('select m.estado from cyber.rodfil s left join cyber.rodmun m on s.codmun = m.codmun where s.codfil = :0');
    dtmDados.RQuery1.Parameters[0].value := RemoveChar(cbFilial.Text);
    dtmDados.RQuery1.Open;
    cbUFD.ItemIndex := cbUFD.Items.IndexOf
      (dtmDados.RQuery1.FieldByName('estado').value);
  end;
end;

procedure TfrmGeraManifesto.cbFiltroClick(Sender: TObject);
var
  ini, fim: Integer;
begin
  if cbFiltro.checked = true then
    mdTemp.Filtered := true
  else
    mdTemp.Filtered := false;
  mdTemp.First;
  ini := mdTemp.RecNo - 1;
  mdTemp.last;
  fim := mdTemp.RecNo;
  Label32.caption := IntToStr(fim - ini) + ' Registros';
  mdTemp.First;
end;

procedure TfrmGeraManifesto.cbMotoristaExit(Sender: TObject);
begin
  if cbTransp.Visible = false then
    cbplacaChange(Sender);
end;

procedure TfrmGeraManifesto.ckRotaClick(Sender: TObject);
begin
  if (ckRota.checked = false) then
  begin
    qrRota.close;
    tbRota.TabVisible := false;
    pgTarefas.ActivePageIndex := 1;
    CarregaTarefas();
  end
  else
  begin
    qrRota.close;
    qrRota.sql[3] := 'Where t.codfil in(' + filiais + ') and r.codfil in(' +
      filiais + ') ';
    qrRota.sql[11] := 'Where ct.fl_empresa in(' + filiais + ') and r.codfil in('
      + filiais + ') ';
    qrRota.sql[17] := 'Where t.codfil in(' + filiais + ')';
    qrRota.sql[24] := 'Where t.codfil in(' + filiais + ') and r.codfil in(' +
      filiais + ') ';
    qrRota.sql[30] := 'Where t.codfil in(' + filiais + ')';
    qrRota.Open;
    qrRota.First;
    pgTarefas.ActivePageIndex := 0;
    tbRota.TabVisible := true;
  end;
end;

procedure TfrmGeraManifesto.dbgColetaCellClick(Column: TColumn);
begin
  if not dtmDados.PodeInserir('frmGeraManifesto') then
    Exit;
  //
  if (mdTempnr_romaneio.AsInteger > 0) then
  begin
    ShowMessage('Esta Tarefa j� esta Romaneada');
    Exit;
  end;

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
  qrCalcula_Frete.Open;
  //
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    mdTempreg.value := reg;
    mdTempInserida.value := 1;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value +
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value + qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value +
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value +
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    edTotFreteTarefa.value := edTotFreteTarefa.value +
      qrCalcula_Frete.FieldByName('frete').AsFloat;
    edTotDoc.Value := edTotDoc.Value + 1;
  end
  else
  begin
    mdTempInserida.value := 0;
    //
    edTotCubadoTarefa.value := edTotCubadoTarefa.value -
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    edTotPesoTarefa.value := edTotPesoTarefa.value - qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    edTotValorTarefa.value := edTotValorTarefa.value -
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    edTotVolumeTarefa.value := edTotVolumeTarefa.value -
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    edTotFreteTarefa.value := edTotFreteTarefa.value -
      qrCalcula_Frete.FieldByName('frete').AsFloat;
    edTotDoc.Value := edTotDoc.Value - 1;
  end;
  mdTemp.Post;
end;

procedure TfrmGeraManifesto.dbgColetaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgColeta.Canvas.FillRect(Rect);
      iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgColeta.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraManifesto.dbgFaturaCellClick(Column: TColumn);
var
  iroma: Integer;
  rota : Integer;
begin
  if (Column <> dbgFatura.Columns[0]) then
    abort;

  if (pnManifesto.Enabled) then
  begin
    ShowMessage(' Grave ou Cancele o Manifesto ');
    if cbPlaca.CanFocus then
      cbPlaca.SetFocus;
    Exit;
  end;

  if (Column = dbgFatura.Columns[0]) then
  begin
    if (mdTempromaneioNR_MANIFESTO.AsInteger > 0) then
    begin
      ShowMessage('Este Romaneio j� esta com Manifesto ');
      Exit;
    end;

    if (mdTempromaneioSELECIONADO.IsNull) or
      (mdTempromaneioSELECIONADO.value = 0) then
    begin
      iroma := mdTempromaneioNR_ROMANEIO.AsInteger;
      nao_atualiza := 'S';
      while not(mdTempromaneio.eof) do
      begin
        if mdTempromaneioNR_ROMANEIO.AsInteger = iroma then
        begin
          mdTempromaneio.edit;
          mdTempromaneioSELECIONADO.value := 1;
          mdTempromaneioselecionou.AsInteger := 1;
          mdTempromaneio.Post;
          // Mapa com Rota
          if not mdTempromaneiorota.IsNull then
            rota := mdTempromaneioNR_ROMANEIO.AsInteger;
        end;
        mdTempromaneio.next;
      end;
      mdTempromaneio.locate('nr_romaneio', iroma, []);
      mdTempromaneio.EnableControls;
      nao_atualiza := 'N';
    end
    else
    begin
      mdTempromaneio.edit;
      mdTempromaneioSELECIONADO.value := 0;
      mdTempromaneioselecionou.AsInteger := 0;
      mdTempromaneio.Post;
    end;

    btnNovoManifesto.Enabled := (mdTempromaneioSELECIONADO.value = 1);
    btnLimpaRomaneio.Enabled := btnNovoManifesto.Enabled;

    if rota > 0 then
    begin
      btnNovoManifestoClick(nil);
    end;

  end;
end;

procedure TfrmGeraManifesto.dbgFaturaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempromaneioSELECIONADO) then
    begin
      dbgFatura.Canvas.FillRect(Rect);
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      //
      if mdTempromaneioSELECIONADO.value = 1 then
        iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
    if (Column.Field.FieldName = 'conferencia') then
    begin
      if mdTempromaneioconferencia.value = 'Aguardando Confer�ncia' then
      begin
        // dbgFatura.canvas.Brush.Color := clYellow;
        dbgFatura.Canvas.Font.Color := clred;
      end;
      if mdTempromaneioconferencia.value = 'Em Confer�ncia' then
      begin
        // dbgFatura.canvas.Brush.Color := clYellow;
        dbgFatura.Canvas.Font.Color := clYellow;
      end;
      if mdTempromaneioconferencia.value = 'Conferido' then
      begin
        // dbgFatura.canvas.Brush.Color := clYellow;
        dbgFatura.Canvas.Font.Color := clGreen;
      end;
      dbgFatura.DefaultDrawDataCell(Rect,
        dbgFatura.Columns[DataCol].Field, State);
    end;
  except
  end;
end;

procedure TfrmGeraManifesto.dbgRoadCellClick(Column: TColumn);
begin
  if not dtmDados.PodeInserir('frmGeraManifesto') then
    Exit;
  //
  if (mdTempnr_romaneio.AsInteger > 0) then
  begin
    ShowMessage('Esta Tarefa j� esta Romaneada');
    Exit;
  end;

  qrCalcula_Frete.close;
  qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
  qrCalcula_Frete.Open;
  //
  mdTemp.edit;
  //
  if (mdTempInserida.IsNull) or (mdTempInserida.value = 0) then
  begin
    reg := reg + 1;
    mdTempreg.value := reg;
    mdTempInserida.value := 1;
    //
    JvCalcEdit4.value := JvCalcEdit4.value +
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    JvCalcEdit1.value := JvCalcEdit1.value + qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    JvCalcEdit2.value := JvCalcEdit2.value +
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    JvCalcEdit5.value := JvCalcEdit5.value +
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    JvCalcEdit3.value := JvCalcEdit3.value +
      qrCalcula_Frete.FieldByName('frete').AsFloat;
    JvCalcEdit6.value := JvCalcEdit6.value + 1;
  end
  else
  begin
    mdTempInserida.value := 0;
    //
    JvCalcEdit4.value := JvCalcEdit4.value -
      qrCalcula_Frete.FieldByName('cub').AsFloat;
    JvCalcEdit1.value := JvCalcEdit1.value - qrCalcula_Frete.FieldByName
      ('peso').AsFloat;
    JvCalcEdit2.value := JvCalcEdit2.value -
      qrCalcula_Frete.FieldByName('valor').AsFloat;
    JvCalcEdit5.value := JvCalcEdit5.value -
      qrCalcula_Frete.FieldByName('qtd').AsFloat;
    JvCalcEdit3.value := JvCalcEdit3.value -
      qrCalcula_Frete.FieldByName('frete').AsFloat;
    JvCalcEdit6.value := JvCalcEdit6.value - 1;
  end;
  mdTemp.Post;
end;

procedure TfrmGeraManifesto.dbgRoadDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempInserida) then
    begin
      dbgRoad.Canvas.FillRect(Rect);
      iml.Draw(dbgRoad.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempInserida.value = 1 then
        iml.Draw(dbgRoad.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(dbgRoad.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraManifesto.DBLTransfExit(Sender: TObject);
begin
  // Carrega as opera��es
  QOperacao.Close;
  QOperacao.Parameters[0].Value := QTranspCODIGO.AsInteger;
  QOperacao.Open;
  cbOperacaoTransf.clear;
  cbOperacaoTransf.Items.add('');
  while not QOperacao.eof do
  begin
    cbOperacaoTransf.Items.add
      (UpperCase(QOperacaoOPERACAO.AsString));
    QOperacao.next;
  end;
  QOperacao.close;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select (codmot ||'' - ''||nommot) nome, nommot from cyber.rodmot where codclifor = :0 order by 2');
  dtmDados.IQuery1.Parameters[0].value := QTranspCODIGO.AsInteger;
  dtmDados.IQuery1.Open;
  while not dtmDados.IQuery1.eof do
  begin
    cbMotoTransf.Items.add(dtmDados.IQuery1.FieldByName('nome').value);
    dtmDados.IQuery1.next;
  end;
end;

procedure TfrmGeraManifesto.DBTransfCellClick(Column: TColumn);
begin
  if (Column <> DBTransf.Columns[0]) then
    abort;

  if (pnTransf.Enabled) then
  begin
    ShowMessage('Grave ou Cancele o Manifesto ');
    if DBLTransf.CanFocus then
      DBLTransf.SetFocus;
    Exit;
  end;

  if (Column = DBTransf.Columns[0]) then
  begin
    MDTransf.edit;
    if (MDTransfSelecionado.IsNull) or (MDTransfSelecionado.value = 0) then
    begin
      MDTransfSelecionado.value := 1;
      // mdTempromaneioselecionou.AsInteger := 1;
    end
    else
    begin
      MDTransfSelecionado.value := 0;
      // mdTempromaneioselecionou.AsInteger := 0;
    end;
    MDTransf.Post;
    btnNovoTransf.Enabled := (MDTransfSelecionado.value = 1);
  end;
end;

procedure TfrmGeraManifesto.DBTransfDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = MDTransfSelecionado) then
    begin
      DBTransf.Canvas.FillRect(Rect);
      iml.Draw(DBTransf.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if MDTransfSelecionado.value = 1 then
        iml.Draw(DBTransf.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(DBTransf.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
end;

procedure TfrmGeraManifesto.dgCtrcDblClick(Sender: TObject);
begin



  if QManifestoSERIE.AsString = '1' then
  begin
    pnlRepom.Visible := true;
    edCiot.text:= QManifestoCIOT.AsString;
    edCartao2.Text := QManifestoCARTAO_REPOM.AsString;
    rbComAdiantamento.Checked := false;
    rbSemAdiantamento.Checked := false;
    if QManifestoID_OPERACAO.AsString = '3' then
    begin
      rbSemAdiantamento.Checked := true;
      rbComAdiantamento.Checked := false;
    end;
    if QManifestoID_OPERACAO.AsString = '4' then
    begin
      rbSemAdiantamento.Checked := false;
      rbComAdiantamento.Checked := true;
    end;
  end;
end;

procedure TfrmGeraManifesto.dgCtrcDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QManifestoCIOT.AsString = '0') or (QManifestoCIOT.AsString = '') and (QManifestoSERIE.AsString = '1') then
  begin
    dgCtrc.canvas.Brush.color := clRed;
    dgCtrc.canvas.font.color := clWhite;
  end;

  if (QManifestoPGR.AsString = 'S') and (QManifestoid_monitoramento.AsString = '') and (QManifestoSerie.AsString = '1')then
  begin
    dgCtrc.canvas.Brush.color := clBlack;
    dgCtrc.canvas.font.color := clWhite;
  end;

  if QManifestoFL_CONTINGENCIA.value = 'S' then
  begin
    dgCtrc.canvas.Brush.Color := clYellow;
    dgCtrc.canvas.Font.Color := clBlack;
  end;

  dgCtrc.DefaultDrawDataCell(Rect, dgCtrc.Columns[DataCol]
    .field, State);
end;

procedure TfrmGeraManifesto.dgRomaneioDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.Field = mdTempromaneioSELECIONADO) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    //
    if mdTempromaneioSELECIONADO.value = 1 then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;

procedure TfrmGeraManifesto.dgTarefaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (mdTarefatem.value = -1) and (mdTarefapalete.Value <=0) then
  begin
    dgTarefa.canvas.Brush.color := clRed;
    dgTarefa.canvas.font.color := clWhite;
  end;

  dgTarefa.DefaultDrawDataCell(Rect, dgTarefa.Columns[DataCol].field, State);
end;

procedure TfrmGeraManifesto.dstTarefaDataChange(Sender: TObject; Field: TField);
begin
  btnVoltaTarefa.Enabled := QTarefa.RecordCount > 0;
end;

procedure TfrmGeraManifesto.edChaveChange(Sender: TObject);
var
  qrPesquisa: TADOQuery;
  scte: Integer;
  sfilial: string;
begin
  pgTarefas.ActivePageIndex := 1;
  if (Alltrim(edChave.Text) = '') then
    Exit;

  if Length(Alltrim(edChave.Text)) = 44 then
  begin
    sfilial := copy(edChave.Text, 7, 14);
    scte := StrToInt(copy(edChave.Text, 26, 9));
    qrPesquisa := TADOQuery.Create(Nil);

    with qrPesquisa, sql do
    begin
      Connection := dtmDados.ConIntecom;
      add('select t.codcon  ');
      add('from cyber.rodcon t left join tb_romaneio r on (r.nr_romaneio = t.agecar)');
      add('where (nvl(t.agecar,0) = 0) and t.situac <> ''C'' and t.codman is null ');
      add('and t.codcon = :0 ');
      add('and t.codfil in(' + filiais + ')');
      Parameters[0].value := scte;
      Open;

      if (qrPesquisa.RecordCount = 0) then
      begin
        ShowMessage('CT-e n�o Encontrado !!');
        FreeAndNil(qrPesquisa);
        edChave.clear;
        edChave.SetFocus;
        Exit;
      end
      else
      begin
        if not mdTemp.locate('coleta', qrPesquisa.FieldByName('codcon')
          .asstring, []) then
        begin
          ShowMessage('CT-e n�o Encontrado !!');
        end
        else
        begin
          if not dtmDados.PodeInserir('frmGeraManifesto') then
            Exit;
          //
          if (mdTempnr_romaneio.AsInteger > 0) then
          begin
            ShowMessage('Esta Tarefa j� esta Romaneada');
            Exit;
          end;

          qrCalcula_Frete.close;
          qrCalcula_Frete.Parameters[0].value := mdTempconca.asstring;
          qrCalcula_Frete.Open;
          //
          mdTemp.edit;
          reg := reg + 1;
          mdTempreg.value := reg;
          mdTempInserida.value := 1;
          edTotCubadoTarefa.value := edTotCubadoTarefa.value +
            qrCalcula_Frete.FieldByName('cub').AsFloat;
          edTotPesoTarefa.value := edTotPesoTarefa.value +
            qrCalcula_Frete.FieldByName('peso').AsFloat;
          edTotValorTarefa.value := edTotValorTarefa.value +
            qrCalcula_Frete.FieldByName('valor').AsFloat;
          edTotVolumeTarefa.value := edTotVolumeTarefa.value +
            qrCalcula_Frete.FieldByName('qtd').AsFloat;
          edTotFreteTarefa.value := edTotFreteTarefa.value +
            qrCalcula_Frete.FieldByName('frete').AsFloat;
          mdTemp.Post;
        end;
      end;
    end;
    edChave.clear;
    // edChave.setfocus;
    pgTarefas.SetFocus;
    keybd_event(VK_SHIFT, 0, 0, 0);
    keybd_event(VK_TAB, 0, 0, 0);
    keybd_event(VK_TAB, 0, KEYEVENTF_KEYUP, 0);
    keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0);
  end;

end;

procedure TfrmGeraManifesto.edDataExit(Sender: TObject);
begin
  if pgTarefas.ActivePageIndex = 2 then
    tsRoadShow(Sender)
  else
    CarregaTarefas;
end;

procedure TfrmGeraManifesto.edFilialExit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
begin
  if (edCte.value > 0) and (edFilial.value > 0) then
  begin
    qrPesquisa := TADOQuery.Create(Nil);
    with qrPesquisa, sql do
    begin
      Connection := dtmDados.ConIntecom;
      add('select nr_romaneio ');
      add('from vw_itens_mapa_sim ');
      add('where codcon = :0 ');
      add('and fl_empresa = :1 ');
      Parameters[0].value := edCte.value;
      Parameters[1].value := edFilial.value;
      Open;
    end;
    if (qrPesquisa.RecordCount = 0) then
    begin
      ShowMessage('CT-e n�o Encontrado !!');
      FreeAndNil(qrPesquisa);
      Exit;
    end
    else
    begin
      if not mdTempromaneio.locate('nr_romaneio',
        qrPesquisa.FieldByName('nr_romaneio').asstring, []) then
      begin
        ShowMessage('CT-e n�o Encontrado !!');
      end
      else
      begin
        edCte.value := 0;
        edFilial.value := 0;
      end;
    end;
  end;
end;

procedure TfrmGeraManifesto.edValorExit(Sender: TObject);
var
  qrPesquisa: TADOQuery;
  i: Integer;
  svalor: string;
begin
  if (Alltrim(edValor.Text) = '') then
    Exit;
  if pgTarefas.ActivePageIndex = 2 then
  begin

    i := cbPesquisa.ItemIndex;
    svalor := edValor.Text;
    if cbPesquisa.ItemIndex <> -1 then
    begin
      qrPesquisa := TADOQuery.Create(Nil);
      with qrPesquisa, sql do
      begin
        Connection := dtmDados.ConIntecom;
        if (cbPesquisa.ItemIndex = 0) then // Nota
        begin
          add('select codcon from (select t.codcon ');
          add('from vw_gerar_manifesto_roadnet t left join tb_cte_nf n on n.codcon = t.codcon and n.fl_empresa = t.codfil ');
          add('Where n.notfis = :0 ');
          add('and n.fl_empresa in(' + filiais + '))');
           //showmessage(qrpesquisa.SQL.text);
        end
        else if (cbPesquisa.ItemIndex = 1) then // Cte
        begin
          add('select codcon, codfil from (select t.codcon, t.codfil  ');
          add('from vw_gerar_manifesto_roadnet t left join tb_cte_nf n on n.codcon = t.codcon and n.fl_empresa = t.codfil ');
          add('where t.codcon = :0 and t.datinc > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and codfil in(' + filiais + '))');
        end
        else if (cbPesquisa.ItemIndex = 2) then // Filial
        begin
          add('select codcon, codfil from (select t.codcon, t.codfil  ');
          add('rom vw_gerar_manifesto_roadnet t left join tb_cte_nf n on n.codcon = t.codcon and n.fl_empresa = t.codfil ');
          add('where t.datinc > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and codfil = :0 )');
        end;
        Parameters[0].value := edValor.Text;
         //showmessage(qrpesquisa.SQL.text);
        Open;
      end;
      if (qrPesquisa.RecordCount = 0) then
      begin
        if (cbPesquisa.ItemIndex = 0) then
          ShowMessage('Nota n�o Encontrada !!')
        else if (cbPesquisa.ItemIndex = 1) then
          ShowMessage('CT-e n�o Encontrado !!')
        else if (cbPesquisa.ItemIndex = 2) then
          ShowMessage('Filial n�o Encontrada !!')
        else if (cbPesquisa.ItemIndex = 3) then
          ShowMessage('OST n�o Encontrada !!');
        FreeAndNil(qrPesquisa);
        Exit;
      end
      else
      begin
        if not mdTemp.locate('coleta', qrPesquisa.FieldByName('codcon')
          .asstring, []) then
        begin
          if (cbPesquisa.ItemIndex = 0) then
            ShowMessage('Nota n�o Encontrada !!')
          else if (cbPesquisa.ItemIndex = 1) then
            ShowMessage('CT-e n�o Encontrado !!')
          else if (cbPesquisa.ItemIndex = 2) then
            ShowMessage('Filial n�o Encontrada !!')
          else if (cbPesquisa.ItemIndex = 3) then
            ShowMessage('OST n�o Encontrada !!');
        end
        else if (cbPesquisa.ItemIndex = 2) then
          mdTemp.SortOnFields('filial')
        else if (cbPesquisa.ItemIndex = 3) then
          mdTemp.SortOnFields('doc');
      end;
    end;
  end
  else
  begin
    pgTarefas.ActivePageIndex := 1;

    i := cbPesquisa.ItemIndex;
    svalor := edValor.Text;
    if cbPesquisa.ItemIndex <> -1 then
    begin
      qrPesquisa := TADOQuery.Create(Nil);
      with qrPesquisa, sql do
      begin
        Connection := dtmDados.ConIntecom;
        if (cbPesquisa.ItemIndex = 0) then // Nota
        begin
          add('select codcon from (select t.codcon ');
          add('from cyber.rodcon t left join tb_romaneio r on (r.nr_romaneio = t.agecar)');
          add('                    left join cyber.rodnfc n on n.codcon = t.codcon and n.codfil = t.codfil ');
          add('where (nvl(t.agecar,0) = 0) and t.situac <> ''C'' and t.codman is null ');
          add('and t.ctepro is not null and t.datinc > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.tipcon <> ''C'' ');
          add('and n.notfis = ' + QuotedStr(edValor.Text));
          add('and t.codfil in(' + filiais + ')');
          add('union ');
          add('select nr_conhecimento codcon ');
          add('from tb_conhecimento t left join tb_romaneio r on (r.nr_romaneio = t.nr_romaneio) ');
          add('          left join tb_cte_nf n on n.codcon = t.nr_conhecimento and n.fl_empresa = t.fl_empresa ');
          add('where (nvl(t.nr_romaneio,0) = 0) and t.fl_status = ''A'' and t.nr_manifesto is null  ');
          add('and t.cte_prot is not null and t.dt_conhecimento > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.fl_tipo = ''C'' and t.fl_tipo_2 <> ''C'' ');
          add('and n.notfis = :0 ');
          add('and t.fl_empresa in(' + filiais + '))');
          // showmessage(qrpesquisa.SQL.text);
        end
        else if (cbPesquisa.ItemIndex = 1) then // Cte
        begin
          add('select codcon, codfil from (select t.codcon, t.codfil  ');
          add('from cyber.rodcon t left join tb_romaneio r on (r.nr_romaneio = t.agecar)');
          add('where (nvl(t.agecar,0) = 0) and t.situac <> ''C'' and t.codman is null ');
          add('and t.ctepro is not null and t.datinc > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.tipcon <> ''C'' ');
          add('union ');
          add('select nr_conhecimento codcon, t.fl_empresa codfil ');
          add('from tb_conhecimento t left join tb_romaneio r on (r.nr_romaneio = t.nr_romaneio) ');
          add('where (nvl(t.nr_romaneio,0) = 0) and t.fl_status = ''A'' and t.nr_manifesto is null  ');
          add('and t.cte_prot is not null and t.dt_conhecimento > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.fl_tipo = ''C'' and t.fl_tipo_2 <> ''C'')');
          add('where codcon = :0 ');
          add('and codfil in(' + filiais + ')');
        end
        else if (cbPesquisa.ItemIndex = 2) then // Filial
        begin
          add('select codcon, codfil from (select t.codcon, t.codfil  ');
          add('from cyber.rodcon t left join tb_romaneio r on (r.nr_romaneio = t.agecar)');
          add('where (nvl(t.agecar,0) = 0) and t.situac <> ''C'' and t.codman is null ');
          add('and t.ctepro is not null and t.datinc > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.tipcon <> ''C'' ');
          add('union ');
          add('select nr_conhecimento codcon, t.fl_empresa codfil ');
          add('from tb_conhecimento t left join tb_romaneio r on (r.nr_romaneio = t.nr_romaneio) ');
          add('where (nvl(t.nr_romaneio,0) = 0) and t.fl_status = ''A'' and t.nr_manifesto is null  ');
          add('and t.cte_prot is not null and t.dt_conhecimento > to_date(' +
            QuotedStr(edData.Text) + ',''dd/mm/yyyy'')');
          add('and t.fl_tipo = ''C'' and t.fl_tipo_2 <> ''C'')');

          add('where codfil = :0 ');
          add('and codfil in(' + filiais + ')');
        end
        else if (cbPesquisa.ItemIndex = 3) then // OST
        begin
          add('select t.codigo codcon  ');
          add('from cyber.rodord t left join tb_romaneio r on (r.nr_romaneio = t.agecar)');
          add('where (nvl(t.agecar,0) = 0) and t.situac <> ''C'' and t.codman is null ');
          add('and t.situac = ''D'' ');
          add('and t.codigo = :0');
          add('and t.codfil in(' + filiais + ')');
        end;
        Parameters[0].value := edValor.Text;
         //showmessage(qrpesquisa.SQL.text);
        Open;
      end;
      if (qrPesquisa.RecordCount = 0) then
      begin
        if (cbPesquisa.ItemIndex = 0) then
          ShowMessage('Nota n�o Encontrada !!')
        else if (cbPesquisa.ItemIndex = 1) then
          ShowMessage('CT-e n�o Encontrado !!')
        else if (cbPesquisa.ItemIndex = 2) then
          ShowMessage('Filial n�o Encontrada !!')
        else if (cbPesquisa.ItemIndex = 3) then
          ShowMessage('OST n�o Encontrada !!');
        FreeAndNil(qrPesquisa);
        Exit;
      end
      else
      begin
        if not mdTemp.locate('coleta', qrPesquisa.FieldByName('codcon')
          .asstring, []) then
        begin
          if (cbPesquisa.ItemIndex = 0) then
            ShowMessage('Nota n�o Encontrada !!')
          else if (cbPesquisa.ItemIndex = 1) then
            ShowMessage('CT-e n�o Encontrado !!')
          else if (cbPesquisa.ItemIndex = 2) then
            ShowMessage('Filial n�o Encontrada !!')
          else if (cbPesquisa.ItemIndex = 3) then
            ShowMessage('OST n�o Encontrada !!');
        end
        else if (cbPesquisa.ItemIndex = 2) then
          mdTemp.SortOnFields('filial')
        else if (cbPesquisa.ItemIndex = 3) then
          mdTemp.SortOnFields('doc');
      end;
    end;
  end;
  cbPesquisa.ItemIndex := i;
  edValor.Text := svalor;
end;

procedure TfrmGeraManifesto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qrRomaneio.close;
  qrManifestoCtrc2.close;
  qMapa.close;
  QVeiculo.close;
  QDestino.close;
  QTarefa.close;
  mdTemp.close;
  mdTempromaneio.close;
  QGerados.close;
  Action := caFree;
end;

procedure TfrmGeraManifesto.FormCreate(Sender: TObject);
begin
  bInicio := true;
  PageControl1.ActivePageIndex := 0;
  pgTarefas.ActivePageIndex := 1;

  // Carrega as cidades
  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.clear;
  dtmDados.RQuery1.sql.add('select c.descri cidade, c.estado ');
  dtmDados.RQuery1.sql.add
    ('from cyber.rodfil f left join rodmun c on f.codmun = c.codmun ');
  dtmDados.RQuery1.sql.add('where f.codfil = :0');
  dtmDados.RQuery1.Parameters[0].value := GlbFilial;
  dtmDados.RQuery1.Open;

  v_cidade := dtmDados.RQuery1.FieldByName('cidade').value;
  v_estado := dtmDados.RQuery1.FieldByName('estado').value;

  QSite.close;
  QSite.Parameters[0].value := GLBCodUser;
  QSite.Open;

  filiais := QSiteSITE.asstring;


  reg := 0;

  QVeiculo.Open;
  edData.date := date - 30;

  cbPesquisa.clear;
  cbPesquisa.Items.add('Nota');
  cbPesquisa.Items.add('CT-e');
  cbPesquisa.Items.add('Filial');
  cbPesquisa.Items.add('OST');
  cbPesquisa.Items.add('Chave');
  cbPesquisa.Items.add('');

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add('select transp_roadnet from tb_empresa ');
  dtmDados.IQuery1.sql.add('where codfil = :0');
  dtmDados.IQuery1.Parameters[0].value := GlbFilial;
  dtmDados.IQuery1.Open;
  if dtmDados.IQuery1.FieldByName('transp_roadnet').AsString = 'S' then
  begin
    dtmdados.QryAcesso.Close;
    dtmdados.QryAcesso.SQL.clear;
    dtmdados.QryAcesso.SQL.Add
      ('SELECT A.*, T.DESCRICAO FROM tb_PERMISSAO_sis A, tb_TELA_sis T');
    dtmdados.QryAcesso.SQL.Add('WHERE A.COD_usuario= ' + #39 + IntToStr(GLBCodUser) + #39);
    dtmdados.QryAcesso.SQL.Add('AND T.IDTELA = A.IDTELA ');
    dtmdados.QryAcesso.SQL.Add('AND upper(T.NAME) = upper(''libera_transp_roadnet'')');
    dtmdados.QryAcesso.Open;
    if dtmdados.QryAcesso.FieldByName('ALTERAR').AsInteger = 1 then
    begin
      transp_roadnet := 'S';
      btnTransp.Enabled := true;
    end
    else
    begin
      transp_roadnet := 'N';
      btnTransp.Enabled := false;
    end;
  end
  else
  begin
    transp_roadnet := 'N';
    btnTransp.Enabled := false;
  end;
end;

procedure TfrmGeraManifesto.gravageo;
var
  XMLUrl, dest, arquivokm, a: String;
  i, j, k: Integer;
  EntryNode, EntryType :IXmlNode;
begin
  dest := ApCarac(QKMDESCRI.asstring);
  XMLUrl := 'https://maps.google.com/maps/api/geocode/xml?sensor=false&address='
    + QKMDESCRI.AsString + '+' + QKMCODCEP.AsString + '+BR' +
    '&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';

  RestClient1.BaseURL := 'https://maps.google.com/maps/api/geocode/xml?sensor=false&address='
                          + QKMDESCRI.AsString + '+' + QKMCODCEP.AsString + '+BR' +
                          '&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
  RESTRequest1.Accept := 'text/xml';
  RESTRequest1.Execute;
  if RestResponse1.StatusCode = 200 then
  begin
    arquivokm := RESTResponse1.Content;
    XMLDoc.LoadFromXML(RESTResponse1.Content);
    XMLDoc.Active := true;
    EntryNode := XMLDoc.DocumentElement;
    EntryType := EntryNode.ChildNodes.First;
    for i := 0 to EntryNode.ChildNodes.Count -1 do
    begin
      if (EntryType.NodeName = 'result') then
      begin
        for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
          for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName;
            if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].NodeName = 'location' then
            begin
              lat := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes['lat'].Text;
              lon := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes['lng'].Text;
            end;
          end;
        end;
      end;
    end;
  end;
  XMLDoc.Active := false;
end;

procedure TfrmGeraManifesto.LimpaCamposManifesto;
begin
  edPeso.value := 0;
  edQtdDestino.value := 0;
  edQtdDestino.Color := clSilver;
  edQtdCubagem.value := 0;
  edPercentCarga.value := 0;
  edCusto.value := 0;
  cbOperacao.ItemIndex := -1;
  QTransp.close;
  cbMotorista.ItemIndex := -1;
  cbPlaca.ItemIndex := -1;
  cbPlaca_carreta.ItemIndex := -1;
  edVeiculo.clear;
  cbUF.Visible := false;
  cbCidaded.Visible := false;
  edPercentCarga.Color := clSilver;
end;

procedure TfrmGeraManifesto.mdTempAfterOpen(DataSet: TDataSet);
begin
  mdTemp.First;
end;

procedure TfrmGeraManifesto.mdTempFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  // CT-e
  // Filial
  // OST
  if cbPesquisa.ItemIndex = 1 then
    Accept := mdTemp['doc'] = 'CTE'
  else if cbPesquisa.ItemIndex = 2 then
    Accept := mdTemp['filial'] = Alltrim(edValor.Text)
  else if cbPesquisa.ItemIndex = 3 then
    Accept := mdTemp['doc'] = 'OST';
end;

procedure TfrmGeraManifesto.mdTempromaneioAfterScroll(DataSet: TDataSet);
begin
  if nao_atualiza = 'N' then
  begin
    QItensRoma.close;
    QItensRoma.Parameters[0].value := mdTempromaneioNR_ROMANEIO.AsInteger;
    QItensRoma.Parameters[1].value := mdTempromaneioDESTINO.value;
    QItensRoma.Open;
  end;
end;

procedure TfrmGeraManifesto.mdTempromaneioFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdTempromaneio['SELECIONADO'] = 1;
end;

procedure TfrmGeraManifesto.MStatKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    MStat.clear;
    MStat.Visible := false;
  end;
end;

procedure TfrmGeraManifesto.ParaFilialChange(Sender: TObject);
begin
  // carregar as filiais
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add('select c.descri cidade, c.estado from cyber.rodfil f left join cyber.rodmun c on f.codmun = c.codmun ');
  dtmDados.IQuery1.sql.add('where f.codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].Value := SoNumero(ParaFilial.Text);
  dtmDados.IQuery1.Open;
  cbUF.Text := dtmdados.IQuery1.FieldByName('estado').Value;
  cbUFExit(Sender);
  cbcidaded.Text := dtmdados.IQuery1.FieldByName('cidade').Value;
  dtmDados.IQuery1.close;
  cbCidadedExit(sender);
end;

procedure TfrmGeraManifesto.QManifestoBeforeOpen(DataSet: TDataSet);
begin
  QManifesto.Parameters[0].value := GlbFilial;
end;

procedure TfrmGeraManifesto.qMapaAfterScroll(DataSet: TDataSet);
begin
  QVolume.close;
  QVolume.Parameters[0].value := qMapaCOD_TA.AsInteger;
  QVolume.Parameters[1].value := qMapaCODFIL.AsInteger;
  QVolume.Open;
end;

function TfrmGeraManifesto.RetornaTarefasSelecionadas(aba: String): String;
var
  sTarSelec: String;
  icount: Integer;
begin
  mdTemp.First;
  icount := 0;
  mdTemp.DisableControls;

  if aba = 'Road' then
  begin
    if not(QidRoad.Active) then
      QidRoad.Open;
    // incluir tarefas j� existente
    QidRoad.First;
    while not QidRoad.eof do
    begin
      if (QidRoadid_road.AsInteger <> iRecDelete) then
      begin
        if icount = 0 then
          sTarSelec := QuotedStr(QidRoadCONCA.AsString)
        else
          sTarSelec := sTarSelec + ',' + QuotedStr(QidRoadConca.AsString);
        inc(icount);
      end;
      QidRoad.next;
    end;
  end
  else
  begin
    if not(QTarefa.Active) then
      QTarefa.Open;
    // incluir tarefas j� existente
    QTarefa.First;
    while not QTarefa.eof do
    begin
      if (QTarefaCODCON.AsInteger <> iRecDelete) then
      begin
        if icount = 0 then
          sTarSelec := QuotedStr(QTarefaCONCA.AsString)
        else
          sTarSelec := sTarSelec + ',' + QuotedStr(QTarefaCONCA.AsString);
        inc(icount);
      end;
      QTarefa.next;
    end;
  end;
  iRecDelete := 0;

  while not mdTemp.eof do
  begin
    if mdTempInserida.value = 1 then
    begin
      if icount = 0 then
        sTarSelec := QuotedStr(mdTempColeta.asstring + mdTempfilial.asstring +
          mdTempdoc.AsString)
      else
        sTarSelec := sTarSelec + ',' +
          QuotedStr(mdTempColeta.asstring + mdTempfilial.asstring +
          mdTempdoc.AsString);
      inc(icount);
    end;
    mdTemp.next;
  end;
  mdTemp.EnableControls;
  result := sTarSelec;
end;

procedure TfrmGeraManifesto.rgTarefasClick(Sender: TObject);
begin
  CarregaRomaneio();
end;

procedure TfrmGeraManifesto.TabSheet1Show(Sender: TObject);
begin
  CarregaRomaneioManifesto();
  pnManifesto.Enabled := false;
  mdTempromaneio.First;

  edPeso.clear;
  edPesoLimiteVeiculo.clear;
  edPercentCarga.clear;
  edQtdCubagem.clear;
  edQtdDestino.clear;
  edQtdDestino.Color := clSilver;
  btnCancelaManifesto.Enabled := false;
end;

procedure TfrmGeraManifesto.TabSheet2Show(Sender: TObject);
begin
  QGerados.close;
  QGerados.Open;
end;

procedure TfrmGeraManifesto.tbManShow(Sender: TObject);
begin
  QManifesto.close;
  QManifesto.Open;
end;

procedure TfrmGeraManifesto.tbTarefaShow(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  CarregaTarefas();
end;

procedure TfrmGeraManifesto.tsManTShow(Sender: TObject);
begin
  MDTransf.close;
  MDTransf.Open;
  QTransf.close;
  QTransf.Parameters[0].value := GlbFilial;
  QTransf.Open;
  While not QTransf.eof do
  begin
    MDTransf.Append;
    MDTransfNr_manifesto.value := QTransfMANIFESTO.AsInteger;
    MDTransfPESO.value := QTransfPESO.AsFloat;
    MDTransfValor.value := QTransfVLRMER.AsFloat;
    MDTransfDestino.value := QTransfUFFIM.AsAnsiString;
    MDTransfoperacao.value := QTransfOPERACAO.value;
    MDTransftotfre.value := QTransfTOTFRE.value;
    MDTransfcusto.value := QTransfCUSTO.value;
    MDTransfserie.value := QTransfSERIE.value;
    MDTransfid_man.AsInteger := QTransfID_MAN.AsInteger;
    QTransf.next;
  end;
  QTransf.close;
  MDTransf.First;
  pnTransf.Enabled := false;
  cbMotoTransf.clear;
  edVeiculoTRansf.clear;
  cbPlacaTransf.ItemIndex := -1;
  cbCarretaTransf.ItemIndex := -1;
  edPesoTransf.clear;
  edCapPesoT.clear;
  edCargaT.clear;
  edCapCubT.clear;
  edReceita.clear;
  edCusto_T.value := 0;
  edCustoT.value := 0;
  edMargT.value := 0;
  edValorMT.value := 0;
  cbUFD.ItemIndex := -1;
  JvMemo2.clear;

  btnCanTransf.Enabled := false;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select distinct (f.codfil || '' - '' ||f.nomeab) sitedes ');
  dtmDados.IQuery1.sql.add
    ('from cyber.rodfil f right join cyber.rodcon c on f.codfil = c.codfil ');
  dtmDados.IQuery1.sql.add('where f.ativa = ''S'' ');
  dtmDados.IQuery1.sql.add('and f.codfil not in (:0) ');
  dtmDados.IQuery1.sql.add('order by 1');
  dtmDados.IQuery1.Parameters[0].value := GlbFilial;
  dtmDados.IQuery1.Open;

  cbFilial.clear;
  cbFilial.Items.add('');
  cbFilial.Items.add('0 - Parceiro');
  while not dtmDados.IQuery1.eof do
  begin
    cbFilial.Items.add
      (UpperCase(dtmDados.IQuery1.FieldByName('sitedes').value));
    dtmDados.IQuery1.next;
  end;
  dtmDados.IQuery1.close;
  QTransp.close;

  cbUFD.clear;
  cbUFD.Items.add('');
  cbUFD.Items.add('AC');
  cbUFD.Items.add('AL');
  cbUFD.Items.add('AM');
  cbUFD.Items.add('AP');
  cbUFD.Items.add('BA');
  cbUFD.Items.add('CE');
  cbUFD.Items.add('DF');
  cbUFD.Items.add('ES');
  cbUFD.Items.add('GO');
  cbUFD.Items.add('MA');
  cbUFD.Items.add('MG');
  cbUFD.Items.add('MS');
  cbUFD.Items.add('MT');
  cbUFD.Items.add('PA');
  cbUFD.Items.add('PB');
  cbUFD.Items.add('PE');
  cbUFD.Items.add('PI');
  cbUFD.Items.add('PR');
  cbUFD.Items.add('RJ');
  cbUFD.Items.add('RN');
  cbUFD.Items.add('RO');
  cbUFD.Items.add('RR');
  cbUFD.Items.add('RS');
  cbUFD.Items.add('SC');
  cbUFD.Items.add('SE');
  cbUFD.Items.add('SP');
  cbUFD.Items.add('TO');

end;

procedure TfrmGeraManifesto.tsRoadHide(Sender: TObject);
begin
  //cbTodos.Visible := false;
end;

procedure TfrmGeraManifesto.tsRoadShow(Sender: TObject);
begin
  // carregar os CT-e paratmetrizados com os clientes que tenham roteiriza��o
  // tb_parametro - coluna roadnet = S

  PageControl1.ActivePageIndex := 4;

  QRoad.close;
  QRoad.sql[5] := 'where v.codfil in(' + filiais + ')';
  QRoad.sql[7] := ' and v.datinc > to_date(' + QuotedStr(edData.Text) +
      ',''dd/mm/yyyy'')';
  QRoad.Open;
  QRoad.First;
  mdTemp.close;
  mdTemp.Open;
  while not QRoad.eof do
  begin
    mdTemp.Append;
    mdTempid_road.Value     := QRoadID_ROAD.AsInteger;
    mdTempColeta.value      := QRoadCODCON.AsInteger;
    mdTempdestino.value     := QRoadRAZSOC.AsAnsiString;
    mdTempdata.value        := QRoadDATINC.AsDateTime;
    mdTempfilial.value      := QRoadCODFIL.AsInteger;
    mdTempdoc.value         := QRoadDOC.value;
    mdTempconca.value       := QRoadCODCON.AsString + QRoadCODFIL.AsString +
      QRoadDOC.AsString;
    QRoad.next;
  end;
  mdTemp.First;
  Label32.caption := IntToStr(mdTemp.RecordCount) + ' Registros';
end;

end.
