unit MonitorCte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, JvExMask,
  JvToolEdit, Buttons, DBCtrls, SHDocVw, xmldom, XMLIntf, msxmldom, XMLDoc,
  InvokeRegistry, Rio, SOAPHTTPClient, Vcl.OleCtrls;

type
  TfrmMonitorCte = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    grMonitor: TDBGrid;
    Panel3: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    tmImprimeDacte: TTimer;
    qrMonitor: TADOQuery;
    dsMonitor: TDataSource;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    gbData: TGroupBox;
    Label4: TLabel;
    btnPesquisa: TBitBtn;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btnImprimir: TBitBtn;
    edMotivo: TDBMemo;
    Label37: TLabel;
    mCte: TMemo;
    btnGerarCtrc: TBitBtn;
    cbTipo: TComboBox;
    lbTotReg: TLabel;
    QSite: TADOQuery;
    QSiteNR_CNPJ_CPF: TStringField;
    QSiteNM_LOJA: TStringField;
    dtsSite: TDataSource;
    btnEnviar: TBitBtn;
    cbFilial: TComboBox;
    btnStatus: TBitBtn;
    btnXML: TBitBtn;
    btnConsXML: TBitBtn;
    QSiteds_uf: TStringField;
    OpenDialog1: TOpenDialog;
    XMLDoc: TXMLDocument;
    QSiteCOD_LOJA: TBCDField;
    qrMonitorCOD_CONHECIMENTO: TBCDField;
    qrMonitorCTRC: TBCDField;
    qrMonitorDREGISTRO: TDateTimeField;
    qrMonitorCIA_NAME: TStringField;
    qrMonitorXMOTIVO_CTE: TStringField;
    qrMonitorCONFIGURACAO: TStringField;
    qrMonitorCTE_STATUS: TBCDField;
    qrMonitorCTE_PROT: TStringField;
    qrMonitorCOD_LOJA: TBCDField;
    qrMonitorCHAVE_CTE: TStringField;
    BitBtn2: TBitBtn;
    qrMonitorCTE_XMSG: TStringField;
    qrMonitorCTE_XMOTIVO: TStringField;
    qrMonitorAVERBA_PROTOCOLO: TStringField;
    Shape4: TShape;
    Label5: TLabel;
    qrMonitorfl_contingencia: TStringField;
    qrMonitorcte_can_prot: TStringField;
    cbXml: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure grMonitorDblClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnGerarCtrcClick(Sender: TObject);
    procedure grMonitorDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure mCteClick(Sender: TObject);
    procedure mCteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEnviarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnStatusClick(Sender: TObject);
    procedure LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser);
    procedure btnConsXMLClick(Sender: TObject);
    procedure btnXMLClick(Sender: TObject);
    function TraduzMensagem(MensagemOriginal: String): String;
    function TagToNameTag(ChildNode:IXMLNode; NomeTag:String):String;
    procedure BitBtn2Click(Sender: TObject);
  private
     var ok : string;
    { Private declarations }
    procedure Pesquisa();

  public
    { Public declarations }
    iEmpresa:integer;
    bContingencia: Boolean;
  end;

var
  frmMonitorCte: TfrmMonitorCte;

implementation

uses Dados, menu , funcoes, pcnConversao, Dacte_Cte_2, ATM;

{$R *.dfm}

procedure TfrmMonitorCte.btnPesquisaClick(Sender: TObject);
begin
 // qrMonitor.open;
  Pesquisa();
end;

procedure TfrmMonitorCte.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrCTe1.WebServices.StatusServico.Executar;
  mCte.Lines.Text := UTF8Encode(frmMenu.ACBrCTe1.WebServices.StatusServico.RetWS);
  mcte.Visible := true;
  mcte.Clear;
  mcte.Lines.Add('Status Servi�o');
  mcte.Lines.Add('tpAmb: '    +TpAmbToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.tpAmb));
  mcte.Lines.Add('verAplic: ' +frmMenu.ACBrCTe1.WebServices.StatusServico.verAplic);
  mcte.Lines.Add('cStat: '    +IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cStat));
  mcte.Lines.Add('xMotivo: '  +frmMenu.ACBrCTe1.WebServices.StatusServico.xMotivo);
  mcte.Lines.Add('cUF: '      +IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.cUF));
  mcte.Lines.Add('dhRecbto: ' +DateTimeToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.dhRecbto));
  mcte.Lines.Add('tMed: '     +IntToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.TMed));
  mcte.Lines.Add('dhRetorno: '+DateTimeToStr(frmMenu.ACBrCTe1.WebServices.StatusServico.dhRetorno));
  mcte.Lines.Add('xObs: '     +frmMenu.ACBrCTe1.WebServices.StatusServico.xObs);
end;

procedure TfrmMonitorCte.btnXMLClick(Sender: TObject);
var stStreamNF: TStringStream;
    sData, sCaminho, sMDFe : string;
    reg : integer;
begin
  stStreamNF := TStringStream.Create('');
  if qrMonitorCHAVE_CTE.AsString = '' then
  begin
    ShowMessage('Consulta Pela Chave, este est� sem !!');
    exit;
  end;

  frmMenu.ACBrCTe1.Configuracoes.WebServices.UF := QSiteds_UF.AsString;

  if GlbCteAmb = 'H' then
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taHomologacao;
  end
  else
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taProducao;
  end;

  frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := true;

  //frmMenu.QCtexml.close;
  //frmMenu.QCtexml.SQL[1] := 'Where cod_conhecimento = '+''+qrMonitorCOD_CONHECIMENTO.asString+'';
  //frmMenu.QCtexml.open;
  //if not frmMenu.Qctexml.eof then
  //begin
  //  stStreamNF := TStringStream.Create(frmMenu.QCteXmlxml.value);
  //  frmMenu.ACBrCTe1.Conhecimentos.Clear;
  //  frmMenu.ACBrCTe1.Conhecimentos.LoadFromString(frmMenu.QCteXmlxml.AsString);
  //  frmMenu.ACBrCTe1.Consultar;
  //end
  //else
  //begin
  if OpenDialog1.Execute then
    begin
      OpenDialog1.InitialDir := frmMenu.ACBrCTe1.DACTe.PathPDF;
      frmMenu.ACBrCTe1.Conhecimentos.Clear;
      frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(OpenDialog1.FileName);
      stStreamNF.WriteString(trim(frmMenu.ACBrCTe1.Conhecimentos.Items[0].XMLAssinado));
      frmMenu.ACBrCTe1.Consultar;
    end;
  //end;

  if not frmMenu.Qctexml.eof then
    begin
      dtmdados.iQuery1.close;
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.SQL.add('update tb_xml_cte set xml = :0 where cod_conhecimento = :1');
      dtmdados.iQuery1.Parameters[0].LoadFromStream(stStreamNF,ftBlob);
      dtmdados.iQuery1.Parameters[1].Value := qrMonitorCOD_CONHECIMENTO.AsString;
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.Close;
    end
    else
    begin
      dtmdados.iQuery1.SQL.Clear;
      dtmdados.iQuery1.SQL.add('insert into tb_xml_cte (cod_conhecimento, xml) values (:0, :1)');
      dtmdados.iQuery1.Parameters[0].Value := qrMonitorCOD_CONHECIMENTO.AsString;
      dtmdados.iQuery1.Parameters[1].LoadFromStream(stStreamNF,ftBlob);
      dtmdados.iQuery1.ExecSQL;
      dtmdados.iQuery1.Close;
    end;

  frmMenu.QCtexml.close;
  FreeAndNil(stStreamNF);

  if (qrMonitorCHAVE_CTE.AsString = frmMenu.ACBrCTe1.WebServices.Consulta.CTeChave)
     and (qrMonitorcte_prot.Value = '') then
  begin
    reg := qrMonitorCOD_CONHECIMENTO.AsInteger;
    dtmdados.iQuery1.close;
    dtmdados.iQuery1.SQL.clear;
    dtmdados.iQuery1.sql.add('update tb_conhecimento set cte_prot = :0, ');
    dtmdados.iQuery1.sql.add('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
    dtmdados.iQuery1.sql.add('cte_xmsg = ''Autorizado o uso do MDF-e'' ');
    dtmdados.iQuery1.sql.add('where cte_chave = :1 ');
    dtmdados.iQuery1.Parameters[0].value := frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
    dtmdados.iQuery1.Parameters[1].value := qrMonitorCHAVE_CTE.AsString;
    dtmdados.iQuery1.ExecSQL;
    dtmdados.iQuery1.close;
    qrMonitor.close;
    qrMonitor.open;
    QrMonitor.locate('cod_conhecimento',reg,[]);
  end;
  frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := false;
end;

procedure TfrmMonitorCte.BitBtn2Click(Sender: TObject);
var protocolo, data, hora, sData, sCaminho, sCte: String;
    inic, fim, a : Integer;
    erro: Retorno;
    datahora : TDatetime;
    resultado : SuccessProcesso;
begin
  frmMenu.ACBrCTe1.Configuracoes.WebServices.UF := QSiteds_UF.AsString;

  if GlbCteAmb = 'H' then
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taHomologacao;
  end
  else
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taProducao;
  end;



  sData := FormatDateTime('YYYY', qrMonitorDREGISTRO.value) +
           FormatDateTime('MM', QrMonitorDREGISTRO.value);
  sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + '\' + sData + '\CTe\';
  sCTe := qrMonitorCHAVE_CTE.AsString + '-cte.xml';

  frmMenu.ACBrCTe1.Conhecimentos.Clear;
  frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + sCTe);
  frmMenu.ACBrCTe1.Consultar;

  dtmdados.iQuery1.close;
  dtmdados.iQuery1.sql.clear;
  dtmdados.iQuery1.SQL.add('select COALESCE(user_ws,'''') user_ws, pass_ws, code_ws from tb_seguradora ');
  dtmdados.IQuery1.SQL.add('where fl_empresa = :0 and trunc(datafim) >= trunc(sysdate) ');
  dtmdados.IQuery1.Parameters[0].value := QSiteCOD_LOJA.AsInteger;
  dtmdados.iQuery1.open;
 //   mcte.Visible := true;
 //   mcte.Clear;
  if dtmdados.iQuery1.FieldByName('user_ws').value <> '' then
  begin
    erro := ATM.GetATMWebSvrPortType.averbaCTe
      (dtmDados.IQuery1.fieldbyname('user_ws').value,
      dtmDados.IQuery1.fieldbyname('pass_ws').value,
      dtmDados.IQuery1.fieldbyname('code_ws').value,
      frmMenu.ACBrCTe1.Conhecimentos.Items[0].XML);
    protocolo := resultado.protocolo;
    datahora := (resultado.dhAverbacao.AsDateTime);
    dtmdados.iQuery1.close;
    mcte.Visible := true;
    mcte.Clear;
    mcte.Lines.Add(erro.ToString);
    //resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
    //resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);

    if protocolo <> '' then
    begin
      frmMenu.prc_averba.Parameters[0].value := protocolo;
      frmMenu.prc_averba.Parameters[1].value := datahora; //StrToDateTime(data + ' ' + hora);
      frmMenu.prc_averba.Parameters[2].value := qrMonitorCOD_CONHECIMENTO.Value;
      frmMenu.prc_averba.ExecProc;
    end;
    pesquisa;
  end;
end;

procedure TfrmMonitorCte.btnConsXMLClick(Sender: TObject);
var arquivo : String;
    cc,mensagem : TStrings;
    reg : integer;
begin
  if qrMonitorCHAVE_CTE.AsString = '' then
  begin
    ShowMessage('Consulta Pela Chave, este est� sem !!');
    exit;
  end;

  frmMenu.ACBrCTe1.Configuracoes.WebServices.UF := QSiteds_UF.AsString;

  if GlbCteAmb = 'H' then
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taHomologacao;
  end
  else
  begin
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Ambiente := taProducao;
  end;


  frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := true;
  frmMenu.ACBrCTe1.WebServices.Consulta.CTeChave := qrMonitorCHAVE_CTE.AsString;
  frmMenu.ACBrCTe1.WebServices.Consulta.Executar;
 // if (qrMonitorcte_status.Value = 204) or (qrMonitorcte_status.Value = 609)
 //  or (qrMonitorcte_status.Value = 100 )then
  if frmMenu.ACBrCTe1.WebServices.Consulta.cStat = 100 then
  begin
    reg := qrMonitorCOD_CONHECIMENTO.AsInteger;
    dtmdados.iQuery1.close;
    dtmdados.iQuery1.SQL.clear;
    dtmdados.iQuery1.sql.add('update tb_conhecimento set cte_prot = :0, ');
    dtmdados.iQuery1.sql.add('cte_status = 100, cte_xmotivo = ''Autorizado o uso do CT-e'',  ');
    dtmdados.iQuery1.sql.add('cte_xmsg = ''Autorizado o uso do CT-e'', cte_data = :1 ');
    dtmdados.iQuery1.sql.add('where cte_chave = :2 ');
    dtmdados.iQuery1.Parameters[0].value := frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
    dtmdados.iQuery1.Parameters[1].value := frmMenu.ACBrCTe1.WebServices.Consulta.DhRecbto;
    dtmdados.iQuery1.Parameters[2].value := qrMonitorCHAVE_CTE.AsString;
    dtmdados.iQuery1.ExecSQL;
    dtmdados.iQuery1.close;

    dtmdados.iQuery1.close;
    dtmdados.iQuery1.SQL.clear;
    dtmdados.iQuery1.sql.add('update cyber.rodcon set ctepro = :0, ctedta = :1, datcad = :2 ');
    dtmdados.iQuery1.sql.add('where cte_id = :3 ');
    dtmdados.iQuery1.Parameters[0].value := frmMenu.ACBrCTe1.WebServices.Consulta.Protocolo;
    dtmdados.iQuery1.Parameters[1].value := frmMenu.ACBrCTe1.WebServices.Consulta.DhRecbto;
    dtmdados.iQuery1.Parameters[2].value := qrMonitorDREGISTRO.Value; 
    dtmdados.iQuery1.Parameters[3].value := qrMonitorCHAVE_CTE.AsString;
    dtmdados.iQuery1.ExecSQL;
    dtmdados.iQuery1.close;
    qrMonitor.close;
    qrMonitor.open;
    QrMonitor.locate('cod_conhecimento',reg,[]);
  end;
  frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := false;
end;

procedure TfrmMonitorCte.btnEnviarClick(Sender: TObject);
var arquivo : String;
    cc, mensagem, xx : TStrings;
    email, smtp, senhaemail, porta, efiscal, cop, envio: string;
    sData, sCaminho, sMDFe : string;
    stStreamNF: TStringStream;
    SSL : Boolean;
begin
  if qrMonitorXMOTIVO_CTE.AsString = 'Cancelado' then
  begin
    xx := TStringList.Create;
    mensagem := TStringList.Create;
    sData := FormatDateTime('YYYY', qrMonitorDREGISTRO.value) +
             FormatDateTime('MM', QrMonitorDREGISTRO.value);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + sData + '\CTe\';

    arquivo := scaminho+ qrMonitorCHAVE_CTE.asstring + '-cte.xml';
    frmMenu.ACBrCTe1.Conhecimentos.Clear;
    frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(arquivo);

    xx.Clear;
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add
      ('select email from cyber.rodctc e left join tb_conhecimento c on c.cod_pagador = e.codclifor ');
    dtmDados.IQuery1.SQL.add('where situac = ''A'' and c.cod_conhecimento = :0');
    dtmDados.IQuery1.Parameters[0].value :=
      qrMonitorCOD_CONHECIMENTO.AsInteger;
    dtmDados.IQuery1.Open;
    while not dtmDados.IQuery1.Eof do
    begin
      xx.add(dtmDados.IQuery1.FieldByName('email').AsString);
      dtmDados.IQuery1.Next;
    end;
    dtmDados.IQuery1.Close;

    if xx.Text <> '' then
    begin
      mensagem.Clear;
      mensagem.add
        ('Comunicamos por este e-mail que o Conhecimento Eletr�nico (Chave=' +
        qrMonitorCHAVE_CTE.AsString +
        ') obteve autoriza��o de cancelamento sob o protocolo: ' +
        qrMonitorcte_can_prot.AsString);
      mensagem.add(' ');
      mensagem.add
        ('Enviamos em anexo o arquivo XML do respectivo CT-e Cancelado.');
      mensagem.add(' ');
      mensagem.add('Atenciosamente,');
      mensagem.add(' ');
      mensagem.add('>>> Sistema SIM - TI Intecom - IW <<<');

      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('select efiscal, email, smtp, senha, porta from tb_empresa ');
      dtmDados.IQuery1.Open;

      try
        frmMenu.ACBrCTe1.Conhecimentos.Items[0].EnviarEmail
          (trim(dtmDados.IQuery1.FieldByName('efiscal').AsString) // Para
          , 'CT-e Cancelado' // edtEmailAssunto.Text
          , mensagem // mmEmailMsg.Lines
          , true // Enviar PDF junto
          , xx// nil //Lista com emails que ser�o enviado c�pias - TStrings
          , nil // Lista de anexos - TStrings
          );
        // Ok:=True;
      except
        // Falha:=True;
      end;

    end
    else
      MessageDlg('Tomador do Servi�o n�o possui, e-mail cadastrado.',
        mtInformation, [mbOk], 0);
    mensagem.Free;
    dtmDados.IQuery1.Close;
    MessageDlg('Conhecimento Cancelado', mtInformation, [mbOk], 0);
    frmMenu.ACBrCTe1.Configuracoes.WebServices.Visualizar := false;
  end
  else
  begin
    cc := TStringList.Create;
    xx := TStringList.Create;
    mensagem := TStringList.Create;
    // j� enviou e autorizou, n�o envia de novo
    if (Sender as TBitBtn).Name = 'btnGerarCtrc' then
    begin
      if qrMonitorcte_status.Value <> 100 then
      begin
        ShowMessage(' CT-e ainda n�o autorizado pelo SEFAZ !');
        exit;
      end;
    end;

    sData := FormatDateTime('YYYY', qrMonitorDREGISTRO.value) +
             FormatDateTime('MM', QrMonitorDREGISTRO.value);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + sData + '\CTe\';

    cc.add('suporte@intecomlogistica.com.br');

    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.clear;
    dtmdados.RQuery1.SQL.add('select distinct razsoc from cyber.rodfil f ');
    dtmdados.RQuery1.SQL.add('where codfil = :0');
    dtmdados.RQuery1.Parameters[0].value := qrMonitorCOD_LOJA.AsInteger;
    dtmdados.RQuery1.open;

    mensagem.add('A ' + dtmdados.RQuery1.FieldByName('razsoc').value  + ' emitiu o CT-e Anexo');
    Mensagem.Add(' ');
    Mensagem.Add('>>> Sistema SIM - TI Intecom/IW <<<');

    arquivo := scaminho+ qrMonitorCHAVE_CTE.asstring + '-cte.xml';
    frmMenu.ACBrCTe1.Conhecimentos.Clear;
    frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(arquivo);

    // Envio de e-mail
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.Add('select * from tb_empresa where codfil = :0 ');
    dtmdados.IQuery1.Parameters[0].value := GLBFilial;
    dtmdados.IQuery1.open;

    smtp := dtmdados.IQuery1.FieldByName('smtp').AsString;
    porta := dtmdados.IQuery1.FieldByName('porta').AsString;
    senhaemail := dtmdados.IQuery1.FieldByName('senha').AsString;
    email := dtmdados.IQuery1.FieldByName('email').AsString;
    envio := dtmdados.IQuery1.FieldByName('efiscal').AsString;
    if dtmdados.IQuery1.FieldByName('reqaut').AsString = 'S' then
      SSL := true
    else
      SSL := false;
    dtmdados.IQuery1.close;


   // xx.Add(frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar +'\'+qrMonitorCHAVE_CTE.asstring+'-cte.pdf');
    frmMenu.ACBrCTe1.Conhecimentos.Items[0].EnviarEmail(envio
                                               , 'CT-e' //edtEmailAssunto.Text
                                               , mensagem //mmEmailMsg.Lines
                                               , true //Enviar PDF junto
                                               , cc //nil //Lista com emails que ser�o enviado c�pias - TStrings
                                               , nil // Lista de anexos - TStrings
                                                );

    CC.Free;
    mensagem.Free;
    dtmdados.Query1.Close;
    ShowMessage('E-mail enviado com sucesso !!');
  end;
end;

procedure TfrmMonitorCte.btnGerarCtrcClick(Sender: TObject);
var x : Integer;
begin
  // j� enviou e autorizou, n�o envia de novo
  if (Sender as TBitBtn).Name = 'btnGerarCtrc' then
  begin
    if (qrMonitorcte_status.AsInteger = 6) and
       (qrMonitorcte_status.Value = 104) and
       (qrMonitorcte_status.Value = 100) then
    begin
      ShowMessage(' CT-e j� enviado e autorizado pelo SEFAZ !');
      exit;
    end;
  end;
  x := frmMenu.Tag;
  iEmpresa := qrMonitorCOD_LOJA.AsInteger;
  if cbXml.Checked = true then
    frmMenu.Tag := 88;
  FrmMenu.GeraCtrcEletronico(qrMonitorCOD_CONHECIMENTO.AsInteger);
  frmMenu.Tag := x;
  pesquisa;
end;

procedure TfrmMonitorCte.btnImprimirClick(Sender: TObject);
var sData, sCaminho, sMDFe : string;
    stStreamNF: TStringStream;
begin
  if (qrMonitorcte_prot.value = '') then
  begin
    ShowMessage('Erro na Transmiss�o � necess�rio o Envio Novamente !');
    exit;
  end;

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select ambiente_cte, pdf_cte from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := GLBFilial;
  dtmdados.IQuery1.open;

  sData := FormatDateTime('YYYY', qrMonitorDREGISTRO.value) +
             FormatDateTime('MM', QrMonitorDREGISTRO.value);
  sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + '\' + sData + '\CTe\';
  sMDFe := qrMonitorCHAVE_CTE.AsString + '-cte.xml';
 // frmMenu.ACBrCte1.DACte.PathPDF := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));
  frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));
  frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar  := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));

  frmMenu.QCtexml.close;
  frmMenu.QCtexml.SQL[1] := 'Where cod_conhecimento = '+''+qrMonitorCOD_CONHECIMENTO.asString+'';
  frmMenu.QCtexml.open;
  if not frmMenu.Qctexml.eof then
  begin
    stStreamNF := TStringStream.Create(frmMenu.QCteXmlxml.value);
    frmMenu.ACBrCTe1.Conhecimentos.Clear;
    frmMenu.ACBrCTe1.Conhecimentos.LoadFromString(frmMenu.QCteXmlxml.AsString);
    frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
  end
  else
  begin
    //arquivo := glbXML_cte + qrMonitorCHAVE_CTE.AsString + '-cte.xml';
    frmMenu.ACBrCTe1.Conhecimentos.Clear;
    frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + sMDFe);
    frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
  end;
  try
    frmMenu.qrCteEletronico.Close;
    frmMenu.qrCteEletronico.Parameters[0].Value := qrMonitorCOD_CONHECIMENTO.AsInteger;
    frmMenu.qrCteEletronico.open;

    frmMenu.QNF.close;
    frmMenu.QNF.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
    frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.Value;
    frmMenu.QNF.Parameters[2].value := frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
    frmMenu.QNF.Open;

    frmMenu.ACBrCTe1.Conhecimentos.Imprimir;

    //Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
    //frmDacte_Cte_2.sChaveCte := frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
    //frmDacte_Cte_2.rlDacte.Preview();
  finally
    //frmDacte_Cte_2.Free;
  end;
  frmMenu.QCtexml.close;
end;

procedure TfrmMonitorCte.grMonitorDblClick(Sender: TObject);
var Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + grMonitor.Columns.Items[grMonitor.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QrMonitor.Locate(grMonitor.Columns.Items[grMonitor.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(grMonitor.Columns.Items[grMonitor.SelectedIndex].Title.Caption + ' n�o localizado');
end;

procedure TfrmMonitorCte.grMonitorDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
   if (DataCol <> 3) then exit;
   if (QrMonitorcte_status.Value = 100) then
   begin
      grMonitor.Canvas.Font.Color  := clWhite;
      grMonitor.Canvas.Brush.Color := clGreen;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      if (QrMonitoraverba_protocolo.IsNull) then
      begin
        grMonitor.Canvas.Font.Color  := clBlack;
        grMonitor.Canvas.Brush.Color := clYellow;
        grMonitor.Canvas.FillRect(Rect);
        grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      end;
      if (qrMonitorfl_contingencia.AsString = 'S') then
      begin
        grMonitor.Canvas.Font.Color  := clWhite;
        grMonitor.Canvas.Brush.Color := clNavy;
        grMonitor.Canvas.FillRect(Rect);
        grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      end;
      ok := '';
   end
   else if (QrMonitorcte_status.AsInteger = 101) or (QrMonitorcte_status.AsInteger = 206)
        or (QrMonitorcte_status.AsInteger = 135) then
   begin
      grMonitor.Canvas.Font.Color  := clWhite;
      grMonitor.Canvas.Brush.Color := clRed;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := 'vermelho';
   end
   else if (QrMonitorcte_status.AsInteger = 94) then
   begin
      grMonitor.Canvas.Font.Color  := clWhite;
      grMonitor.Canvas.Brush.Color := clRed;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := 'vermelho';
   end
   else if (QrMonitorcte_status.AsInteger = 94) and (QrMonitorcte_status.IsNull) then
   begin
      grMonitor.Canvas.Font.Color  := clWhite;
      grMonitor.Canvas.Brush.Color := clYellow;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := '';
   end
   else if ((QrMonitorcte_prot.value = '') or (QrMonitorcte_status.Value = 103) or (QrMonitorcte_status.Value = 105)
        and (qrMonitorcte_status.AsInteger = 94)) then
   begin
      grMonitor.Canvas.Font.Color  := clBlack;
      grMonitor.Canvas.Brush.Color := clYellow;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := '';
   end
   else if (QrMonitorcte_status.AsInteger = 6) and (QrMonitorcte_prot.value = '') then
   begin
      grMonitor.Canvas.Font.Color  := clBlack;
      grMonitor.Canvas.Brush.Color := clYellow;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := '';
   end
   else if (QrMonitorcte_status.AsString = '-11') then
   begin
      grMonitor.Canvas.Font.Color  := clWhite;
      grMonitor.Canvas.Brush.Color := clRed;
      grMonitor.Canvas.FillRect(Rect);
      grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
      ok := 'vermelho';
   end;
end;

procedure TfrmMonitorCte.LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser);
begin
  MyMemo.Lines.SaveToFile(ExtractFileDir(application.ExeName)+'temp.xml');
  MyWebBrowser.Navigate(ExtractFileDir(application.ExeName)+'temp.xml');
end;

procedure TfrmMonitorCte.mCteClick(Sender: TObject);
begin
   mCte.Color := clWhite;
   mCte.Font.Style := [fsBold];
end;

procedure TfrmMonitorCte.mCteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    mCte.Clear;
    mCte.Visible := False;
  end;
end;

procedure TfrmMonitorCte.Pesquisa();
var sCnpj,a :String;
    qrSite :TADOQuery;
    d1, d2 : string;
begin
  d1 := formatdatetime('dd/mm/yy',dtinicial.Date) + ' 00:00:01';
  d2 := formatdatetime('dd/mm/yy',dtfinal.Date) + ' 23:59:59';
  QrMonitor.Close;
  qrMonitor.SQL[44] := 'and nvl(cte.cte_data, dt_conhecimento) between to_date(' + #39 + d1 + #39 + ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + d2 + #39+',''dd/mm/yy hh24:mi:ss'')';

  //
  sCNPJ := '';
  if cbFilial.ItemIndex > -1 then
  begin
    a := Copy(cbFilial.text, 1, 18);
    if QSite.Locate('NR_CNPJ_CPF',a,[]) then
      sCNPJ := QSiteNR_CNPJ_CPF.value;
  end;

  if (cbFilial.ItemIndex > -1) then
  begin
    qrMonitor.SQL[45] := ' and CIA.codcgc = ' + QuotedStr(sCnpj);
  end;
  // limpa consulta por Status
  qrMonitor.SQL[46] := '';
  // monitorando
  if (cbTipo.ItemIndex = 1) then
      qrMonitor.SQL[46] := 'and (((cte_prot = '''') or (cte_status = ' + QuotedStr('103') + ') ' +
                            ' or (cte_status = ' + QuotedStr('105') + ')) and (cte_status = 6)) '
  // Definitivo com Sucesso
  else if (cbTipo.ItemIndex = 2) then
      qrMonitor.SQL[46] := 'and ((cte_status = 6) or (cte_status = ' + QuotedStr('104') + ') or ' +
         '(cte_status = ' + QuotedStr('100') + '))'
  //
  // Definitivo com Erro
  else if (cbTipo.ItemIndex = 3) then
      qrMonitor.SQL[46] := ' and ((cte_status = 6) or (cte_status = ' + QuotedStr('104') + ') ' +
                           ' or (cte_status <> ' + QuotedStr('100') + '))'
  // Erro Schema
  else if (cbTipo.ItemIndex = 4) then
      qrMonitor.SQL[46] := ' and (cte_status = 94)';
  //showmessage(QrMonitor.SQL.text);
  QrMonitor.Open;
  QrMonitor.First;
  lbTotReg.Caption := 'Total de Registros: ' + InttoStr(QrMonitor.RecordCount);
end;

procedure TfrmMonitorCte.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Qsite.close;
end;

procedure TfrmMonitorCte.FormCreate(Sender: TObject);
var nm : string;
begin
  Qsite.open;
  cbFilial.clear;
  cbFilial.Items.add('');
  while not QSite.eof do
  begin
    cbFilial.Items.add(QSiteNR_CNPJ_CPF.value + '-' + QSiteNM_LOJA.value);
    if GLBFilial = QSiteCOD_LOJA.AsInteger then
      nm := QSiteNR_CNPJ_CPF.value + '-' + QSiteNM_LOJA.value;
    QSite.Next;
  end;
  if Qsite.RecordCount = 1 then
  begin
    cbFilial.ItemIndex := 1;
  end
  else
    cbFilial.itemindex := cbfilial.Items.IndexOf(nm);
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select ambiente_cte, certificado, logo, pdf_cte, ');
  dtmdados.IQuery1.SQL.add('schemma, proxy, porta, user_proxy, pass_proxy ');
  dtmdados.IQuery1.SQL.add('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := GLBFilial;
  dtmdados.IQuery1.open;

  if (dtmdados.IQuery1.Eof) or (dtmdados.IQuery1.FieldByName('ambiente_cte').IsNull) then
  begin
    ShowMessage('N�o Encontrado nenhuma configura��o de CT-e, avise o TI');
  end
  else
  begin
    GlbSchema := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
    GlbCteAmb := dtmdados.IQuery1.FieldByName('ambiente_cte').AsString;

    frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie := ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar  := IncludeTrailingPathDelimiter(ALLTRIM(dtmdados.IQuery1.FieldByName('pdf_cte').AsString));

    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyHost := ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPort := ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyUser := ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPass := ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
    dtmdados.IQuery1.close;
  end;

end;

procedure TfrmMonitorCte.FormShow(Sender: TObject);
begin
  dtInicial.Date := Date - 1;
  dtFinal.Date := Date;
  bContingencia := False;
  grMonitor.SetFocus;
  cbTipo.ItemIndex := 0;
end;

function TfrmMonitorCte.TraduzMensagem(MensagemOriginal: String): String;
var vMensagemTraduzida, vMensagemOriginal:TStrings;
    vString:String;
    I, vPosicao1, vPosicao2: Integer;
begin
  vMensagemTraduzida := TStringList.Create;
  vMensagemOriginal := TStringList.Create;

  while Trim(MensagemOriginal) <> '' do
  begin
    if Pos(#$D#$A, MensagemOriginal) <> 0 then
      vMensagemOriginal.Add(Copy(MensagemOriginal, 1,Pos(#$D#$A, MensagemOriginal) +1))
    else
      vMensagemOriginal.Add(Copy(MensagemOriginal, 1, Length(MensagemOriginal)));
    if Pos(#$D#$A, MensagemOriginal) <> 0 then
      MensagemOriginal := Copy(MensagemOriginal, Pos(#$D#$A, MensagemOriginal) +2, Length(MensagemOriginal))
      else
      MensagemOriginal := '';
  end;

  try
    if pos('Falha na valida��o dos dados do Conhecimento',vMensagemOriginal.Strings[0]) <> 0 then
    begin
      vMensagemTraduzida.Add(vMensagemOriginal[0]);
    end;

    for I := 0 to vMensagemOriginal.Count - 1 do
    begin
      if pos('TAG:',vMensagemOriginal[i]) <> 0 then
      begin
        vPosicao1 := (Pos(#$D, vMensagemOriginal[i])- Pos('/', vMensagemOriginal[i])+1);
        vMensagemTraduzida.Add('TAG: '+Copy(vMensagemOriginal[i],Pos('/', vMensagemOriginal[i])+1, vPosicao1-1));
      end
      else
      if pos('element', vMensagemOriginal[i]) <> 0 then
      begin
        vString := Copy(vMensagemOriginal[i], Pos('}', vMensagemOriginal[i])+1,
        Pos('''', Copy(vMensagemOriginal[i], Pos('}', vMensagemOriginal[i])+1, Length(vMensagemOriginal[i])))-1);
        vString := 'TAG: '+ vString +'('+TagToNameTag(XMLDoc.DocumentElement, vString) + ') ';
        if pos('with value', vMensagemOriginal[i]) <> 0 then
        begin
          vPosicao1 := pos(' with value ', vMensagemOriginal[i]) +12;
          vPosicao2:= Pos('''',Copy(vMensagemOriginal[i], vPosicao1+1, Length(vMensagemOriginal[i])))+1;

          vString := vString + ' com preenchimento inv�lido de acordo com o Schema. Contendo o valor:' +Copy(vMensagemOriginal[i], vPosicao1,vPosicao2);
        end
        else
        if pos('is incomplete according to the DTD/Schema', vMensagemOriginal[i]) <> 0 then
        begin
          vString := vString + ' est� incompleto de acordo com o Schema.'
        end;
        vMensagemTraduzida.Add(vString);
      end
      else
      if pos('Expecting:', vMensagemOriginal[i]) <> 0 then
      begin
        vPosicao1 := Pos('}', vMensagemOriginal[i])+1;
        vString := Copy(vMensagemOriginal[i], vPosicao1,
        Pos(',', Copy(vMensagemOriginal[i], vPosicao1, Length(vMensagemOriginal[i])))-1);
        vString := 'Esperando-se o preenchimento de: '+ vString +'('+TagToNameTag(XMLDoc.DocumentElement, vString) + ') ';
        while Length(vMensagemOriginal[i]) > 0 do
        begin
          vPosicao2 := Pos(',', vMensagemOriginal[i]);
          if (vPosicao2 =0) and (vPosicao1 > 0) then
          begin
            vMensagemOriginal[i] := '';
            Break;
          end;
          vMensagemOriginal[i] := Copy(vMensagemOriginal[i], vPosicao2+1, Length(vMensagemOriginal[i]));
          vPosicao1 := Pos('}', vMensagemOriginal[i]);
          if (vPosicao1 = 0) and (Pos('{', vMensagemOriginal[i]) >0) then
          begin
            vString := vString + '...';
            Break;
          end
          else
          begin
            vPosicao2 := Pos(',', vMensagemOriginal[i]);
            if (vPosicao2 =0) and (vPosicao1 > 0) then
            begin
              vString := vString+' e/ou '+ Copy(vMensagemOriginal[i], vPosicao1+1, Length(vMensagemOriginal[i]));
            end
            else
              vString := vString+' e/ou '+ Copy(vMensagemOriginal[i], vPosicao1+1, (vPosicao2 -1)-vPosicao1)+
              '('+TagToNameTag(XMLDoc.DocumentElement, Copy(vMensagemOriginal[i], vPosicao1+1, (vPosicao2-1)-vPosicao1)) + ') ';
          end;
        end;
        vMensagemTraduzida.Add(vString);
      end;
    end;
    Result := vMensagemTraduzida.Text;
  finally
  FreeAndNil(vMensagemTraduzida);
  FreeAndNil(vMensagemOriginal);
  end;
end;

function TfrmMonitorCte.TagToNameTag(ChildNode:IXMLNode; NomeTag:String):String;
var I: Integer;
begin
  Result := '';
  ChildNode.ChildNodes.First;
  if (ChildNode.NodeName = 'xs:element') then
  begin
    if ChildNode.GetAttributeNS('name','') = NomeTag then
    begin
      if (ChildNode.ChildNodes.FindNode('xs:annotation') <> nil) then
      begin
        Result := ChildNode.ChildNodes.FindNode('xs:annotation').ChildNodes.FindNode('xs:documentation').Text;
        Exit;
      end;
    end;
  end;
  for I := 0 to ChildNode.ChildNodes.Count - 1 do
  begin
    Result := TagToNameTag(ChildNode.ChildNodes.Nodes[i], NomeTag);
    if Result <> '' then
      Exit;
  end;
end;


end.

