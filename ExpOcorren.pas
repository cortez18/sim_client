unit ExpOcorren;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents;

type
  TfrmExpOcorren = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    Memo1: TMemo;
    btnExportar: TBitBtn;
    QCtrcDT_DESTINO: TDateTimeField;
    QCtrcNM_CLI_OS: TStringField;
    Label1: TLabel;
    cbOper: TJvComboBox;
    QOperacao: TADOQuery;
    QCtrcNR_SERIE: TStringField;
    DBNavigator1: TDBNavigator;
    Panel1: TPanel;
    Panel2: TPanel;
    Label8: TLabel;
    DriveComboBox1: TDriveComboBox;
    DirectoryListBox1: TDirectoryListBox;
    QCtrccod_obs: TStringField;
    QCtrcnome: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    QOperacaoID_OPERACAO: TBCDField;
    QOperacaoOPERACAO: TStringField;
    QCtrcDS_OCOR_ENT: TBCDField;
    QCtrcNR_NF: TStringField;
    SP_OCO_COLGATE: TADOStoredProc;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
    procedure QCtrcBeforeOpen(DataSet: TDataSet);
    procedure dtFinalExit(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  frmExpOcorren: TfrmExpOcorren;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmExpOcorren.btnExportarClick(Sender: TObject);
var
  Info: TextFile;
  sArquivo, caminho: String;
  i: integer;
begin

  caminho := '';

  dtmdados.iQuery1.close;
  dtmdados.iQuery1.sql.clear;
  dtmdados.iQuery1.sql.add('select * from cyber.rodfil where codfil = :0');
  dtmdados.iQuery1.Parameters[0].Value := GLBFilial;
  dtmdados.iQuery1.open;

  if QCtrc.IsEmpty then
    ShowMessage('N�o existem dados para exportar !!')
  else
  begin
    i := 0;

    if caminho = '' then
      sArquivo := DirectoryListBox1.Directory + '\' + 'OCORREN' +
        ledIdCliente.LookupValue + '.txt'
    else
      sArquivo := caminho + '\' + 'OCORREN' + ledIdCliente.LookupValue + '.txt';

    AssignFile(Info, sArquivo);
    if not FileExists(sArquivo) then
      Rewrite(Info)
    else
      Rewrite(Info);

    // bloco 000
    WriteLn(Info, '000',
      Retbran(copy(alltrim(dtmdados.iQuery1.FieldByName('razsoc').Value), 1,
      35), 35), Retbran(copy(alltrim(QCtrcNM_CLI_OS.Value), 1, 35), 35),
      apcarac(apcarac(formatdatetime('dd/mm/yy', date))),
      apcarac(apcarac(formatdatetime('hh:mm', time))),
      'OCO' + copy(apcarac(formatdatetime('dd/mm/yy', date)), 1, 4) +
      apcarac(apcarac(formatdatetime('hh:mm', time))) + '0', retbranE('', 25));
    // bloco 340
    WriteLn(Info, '340', 'OCORR' + copy(apcarac(formatdatetime('dd/mm/yy', date)
      ), 1, 4) + apcarac(apcarac(formatdatetime('hh:mm', time))) + '0',
      retbranE('', 103));
    // bloco 341
    WriteLn(Info, '341',
      Retbran(copy(alltrim(apcarac(dtmdados.iQuery1.FieldByName('codcgc').Value)
      ), 1, 14), 14),
      Retbran(copy(alltrim(dtmdados.iQuery1.FieldByName('razsoc').Value), 1,
      40), 40), Retbran('', 63));
    QCtrc.First;
    while not QCtrc.Eof do
    begin
      // bloco 342
      WriteLn(Info, '342',
        retbranE(copy(alltrim(apcarac(QClienteNR_CNPJ_CPF.Value)), 1, 14), 14),
        Retbran(QCtrcNR_SERIE.AsString, 3), RetZero(QCtrcNR_NF.AsString, 8),
        RetZero(QCtrcDS_OCOR_ENT.AsString, 2),
        apcarac(apcarac(formatdatetime('dd/mm/yyyy', QCtrcDT_DESTINO.Value))),
        apcarac(apcarac(formatdatetime('hh:mm', QCtrcDT_DESTINO.Value))),
        QCtrccod_obs.Value, Retbran(QCtrcnome.AsString, 70), retbranE('', 6));
      QCtrc.next;
    end;
    ShowMessage('Processo Terminado !!');
  end;
  closefile(Info);
end;

procedure TfrmExpOcorren.btRelatorioClick(Sender: TObject);
var
  caminho: string;
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    ShowMessage('N�o foi escolhido o per�odo !!');
    dtInicial.setfocus;
    exit;
  end;
  if dtFinal.date < dtInicial.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    dtFinal.setfocus;
    exit;
  end;
  if Trim(ledIdCliente.LookupValue) = '' then
  begin
    ShowMessage('N�o foi escolhido o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;

  caminho := '';

  Panel1.Visible := true;
  Application.ProcessMessages;
  //Colgate
  if ledIdCliente.LookupValue = '55783' then
  begin
    SP_OCO_COLGATE.Parameters[1].Value := dtInicial.Text;
    SP_OCO_COLGATE.Parameters[2].Value := dtFinal.Text;
    SP_OCO_COLGATE.ExecProc;
    caminho := SP_OCO_COLGATE.Parameters[0].Value;
    showmessage('Arquivo Gerado em \\192.168.236.112\Pre_Faturas\ocorrencias\' + caminho);
  end
  else
  begin
    QCtrc.close;
    QCtrc.sql[26] := 'where cod_pagador = ' + ledIdCliente.LookupValue;
    QCtrc.sql[27] := 'and DT_DESTINO between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')';
    QCtrc.open;
  end;
  Panel1.Visible := false;
end;

procedure TfrmExpOcorren.dtFinalExit(Sender: TObject);
begin
  QCliente.close;
  QCliente.sql[4] := 'and p.dt_conhecimento between to_date(''' + dtInicial.Text
    + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCliente.open;
end;

procedure TfrmExpOcorren.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmExpOcorren.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmExpOcorren.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmExpOcorren.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmExpOcorren.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmExpOcorren.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmExpOcorren.QClienteBeforeOpen(DataSet: TDataSet);
begin
  QCliente.Parameters[0].Value := GLBFilial;
end;

procedure TfrmExpOcorren.QCtrcBeforeOpen(DataSet: TDataSet);
begin
  QCtrc.Parameters[0].Value := GLBFilial;
end;

end.
