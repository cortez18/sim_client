unit CadOcor_fat_pagar_2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, JvCombobox,
  JvToolEdit, JvBaseEdits, JvExMask, JvMaskEdit, DBCtrls, Mask, ComCtrls,
  ShellApi, ACBrBase, ACBrMail, ComObj, Gauges, System.ImageList, System.Actions;

type
  TfrmCadOcor_fat_pagar_2 = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QOperacao: TADOQuery;
    iml: TImageList;
    QOperacaoDT_INC: TDateTimeField;
    QOperacaoDT_REC_FAT: TDateTimeField;
    QOperacaoDT_EMI_BOL: TDateTimeField;
    QOperacaoDT_VCTO: TDateTimeField;
    QOperacaoFATURA: TStringField;
    QOperacaoVL_FATURA: TBCDField;
    QOperacaoE_MAIL: TStringField;
    QOperacaoOBSE: TStringField;
    QOperacaoUSER_INC: TStringField;
    QOperacaoANEXO: TBlobField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit12: TDBEdit;
    edemail: TJvMaskEdit;
    edValor: TJvCalcEdit;
    edRecFat: TJvDateEdit;
    Label14: TLabel;
    edDt_Bol: TJvDateEdit;
    edVcto_Bol: TJvDateEdit;
    Label4: TLabel;
    edObs: TJvMaskEdit;
    Label5: TLabel;
    edSolucao: TJvDateEdit;
    QOco: TADOQuery;
    QOperacaoDT_SOLUCAO: TDateTimeField;
    QOperacaoNM_TRANSP: TStringField;
    QOcoID: TBCDField;
    QOcoDT_INC: TDateTimeField;
    QOcoDT_REC_FAT: TDateTimeField;
    QOcoDT_EMI_BOL: TDateTimeField;
    QOcoDT_VCTO: TDateTimeField;
    QOcoFATURA: TStringField;
    QOcoTRANSP: TBCDField;
    QOcoVL_FATURA: TBCDField;
    QOcoE_MAIL: TStringField;
    QOcoOBSE: TStringField;
    QOcoLIDO: TStringField;
    QOcoUSER_INC: TStringField;
    QOcoANEXO: TBlobField;
    QOcoDT_SOLUCAO: TDateTimeField;
    OpenDialog1: TOpenDialog;
    edFatura: TJvMaskEdit;
    ednmTransp: TJvMaskEdit;
    QOperacaoID: TBCDField;
    btnPDF: TBitBtn;
    QOperacaoTRANSP: TBCDField;
    btnEmail: TBitBtn;
    Memo1: TMemo;
    ACBrMail1: TACBrMail;
    Panel2: TPanel;
    btnExcel: TBitBtn;
    RadioGroup1: TRadioGroup;
    Gauge1: TGauge;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure QOperacaoAfterScroll(DataSet: TDataSet);
    procedure btnPDFClick(Sender: TObject);
    procedure btnEmailClick(Sender: TObject);
    procedure ACBrMail1MailException(const AMail: TACBrMail; const E: Exception;
      var ThrowIt: Boolean);
    procedure ACBrMail1AfterMailProcess(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    tipo, codtrans: String;
  public
    { Public declarations }
  end;

var
  frmCadOcor_fat_pagar_2: TfrmCadOcor_fat_pagar_2;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadOcor_fat_pagar_2.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadOcor_fat_pagar_2.btnInserirClick(Sender: TObject);
begin
  Restaura;
  tipo := 'I';
  edValor.value := 0;
  edRecFat.clear;
  edDt_Bol.clear;
  edVcto_Bol.clear;
  edObs.clear;
  edSolucao.clear;
  edemail.clear;
  edFatura.clear;
  ednmTransp.clear;
  DBEdit1.Visible := false;
  DBEdit2.Visible := false;
  DBEdit12.Visible := false;
end;

procedure TfrmCadOcor_fat_pagar_2.btnPDFClick(Sender: TObject);
begin
  // Salvando o arquivo
  TBlobField(QOperacaoANEXO).SaveToFile('C:\SIM\Arquivo.pdf');

  // Abrindo
  ShellExecute(Application.Handle, nil, PChar('C:\SIM\Arquivo.pdf'), nil, nil,
    SW_SHOWNORMAL);
end;

procedure TfrmCadOcor_fat_pagar_2.btnSalvarClick(Sender: TObject);
var
  Stream: TMemoryStream;
  mail: TStringList;
  rec: Integer;
begin
  if edemail.text = '' then
  begin
    ShowMessage('� Obrigat�rio o e-mail do destinat�rio');
    edemail.SetFocus;
    exit;
  end;

  if edValor.value = 0 then
  begin
    ShowMessage('� Obrigat�rio o Valor da Fatura');
    edValor.SetFocus;
    exit;
  end;

  if copy(edRecFat.text, 1, 2) = '  ' then
  begin
    ShowMessage('� Obrigat�rio a Data do Recebimento da Fatura');
    edRecFat.SetFocus;
    exit;
  end;

  if copy(edDt_Bol.text, 1, 2) = '  ' then
  begin
    ShowMessage('� Obrigat�rio a Data de Emiss�o do Boleto');
    edDt_Bol.SetFocus;
    exit;
  end;

  if copy(edVcto_Bol.text, 1, 2) = '  ' then
  begin
    ShowMessage('� Obrigat�rio a Data do Vencimento do Boleto');
    edVcto_Bol.SetFocus;
    exit;
  end;

  if edObs.text = '' then
  begin
    ShowMessage('� Obrigat�rio a Observa��o');
    edObs.SetFocus;
    exit;
  end;

  if (tipo = 'A') and (copy(edSolucao.text, 1, 2) = '  ') then
  begin
    ShowMessage('� Obrigat�rio a Data da Solu��o');
    edSolucao.SetFocus;
    exit;
  end;

  QOco.Close;
  QOco.Parameters[0].value := QOperacaoID.value;
  QOco.open;

  try
    if tipo = 'I' then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('select nvl(max(id),0) + 1 codigo from tb_ocor_fat_pagar_2');
      dtmdados.IQuery1.open;

      QOco.append;
      QOcoID.value := dtmdados.IQuery1.FieldByName('codigo').value;
      QOcoDT_REC_FAT.value := edRecFat.Date;
      QOcoDT_EMI_BOL.value := edDt_Bol.Date;
      QOcoDT_VCTO.value := edVcto_Bol.Date;
      QOcoFATURA.value := edFatura.text;
      QOcoTRANSP.value := self.tag;
      QOcoVL_FATURA.value := edValor.value;
      QOcoE_MAIL.value := edemail.text;
      QOcoOBSE.value := edObs.text;
      QOcoUSER_INC.value := GLBUSER;

      if OpenDialog1.Execute then
      begin
        Stream := TMemoryStream.Create;
        Stream.LoadFromFile(OpenDialog1.FileName);
        try
          Stream.Seek(0, soFromBeginning);
          QOcoANEXO.LoadFromStream(Stream);
        finally
          Stream.Free;
        end;
      end;
    end
    else
    begin
      QOco.edit;
      QOcoDT_SOLUCAO.value := edSolucao.Date;
    end;
    QOco.post;
    rec := QOcoID.AsInteger;
  except
    on E: Exception do
    begin
      ShowMessage(E.message);
    end;
  end;
  Restaura;
  QOperacao.Close;
  QOperacao.open;
  QOperacao.Locate('id', rec, []);

  if tipo = 'I' then
  begin
    try
      btnEmailClick(Sender);
    finally

    end;
  end;

  tipo := '';
  DBEdit1.Visible := true;
  DBEdit2.Visible := true;
  DBEdit12.Visible := true;
end;

procedure TfrmCadOcor_fat_pagar_2.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QOperacao.Cancel;
end;

procedure TfrmCadOcor_fat_pagar_2.btnEmailClick(Sender: TObject);
var
  mail, cc, cc1, cc2: String;
  posIni: Integer;
begin
  // Salvando o arquivo
  TBlobField(QOperacaoANEXO).SaveToFile('C:\SIM\Arquivo.pdf');

  // criando o corpo do email
  Memo1.clear;
  Memo1.Lines.add('<html xmlns=' + #39 + 'http://www.w3.org/1999/xhtml' +
    #39 + '>');
  Memo1.Lines.add('<head>');
  Memo1.Lines.add
    ('<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Lines.add
    ('.titulo2 {  FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  Memo1.Lines.add
    ('.titulo3 {  FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  Memo1.Lines.add
    ('.texto1 {  FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Lines.add
    ('.texto2 {  FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Lines.add
    ('.texto3 {  FONT: 10px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  Memo1.Lines.add('</STYLE>');
  Memo1.Lines.add('<meta http-equiv=' + #39 + 'Content-Type' + #39 + ' content='
    + #39 + 'text/html; charset=iso-8859-1' + #39 + '>');
  Memo1.Lines.add('<table border=0><title></title></head><body>');
  Memo1.Lines.add('<table border=1>');
  Memo1.Lines.add('<tr>');
  Memo1.Lines.add
    ('<th colspan=9><FONT class=titulo1>Ocorr�ncia - Fatura Faltando Receita</th></font>');
  Memo1.Lines.add('</tr>');

  Memo1.Lines.add('<tr>');
  Memo1.Lines.add
    (' <th bgcolor="#d0d0d0"><font class="texto2">Transportador</th>');
  Memo1.Lines.add(' <th bgcolor="#d0d0d0"><font class="texto2">Fatura</th>');
  Memo1.Lines.add(' <th bgcolor="#d0d0d0"><font class="texto2">Valor</th>');
  Memo1.Lines.add
    (' <th bgcolor="#d0d0d0"><font class="texto2">Recebimento Fatura</th>');
  Memo1.Lines.add
    (' <th bgcolor="#d0d0d0"><font class="texto2">Emiss�o Boleto</th>');
  Memo1.Lines.add
    (' <th bgcolor="#d0d0d0"><font class="texto2">vencimento</th>');
  Memo1.Lines.add(' <th bgcolor="#d0d0d0"><font class="texto2">Usuario</th>');
  Memo1.Lines.add(' <th bgcolor="#d0d0d0"><font class="texto2">Data</th>');
  Memo1.Lines.add
    (' <th bgcolor="#d0d0d0"><font class="texto2">Observa��o</th>');
  Memo1.Lines.add('</tr>');

  Memo1.Lines.add('   <tr>');
  Memo1.Lines.add('      <th><font class="texto2">' + QOperacaoNM_TRANSP.value
    + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' + QOperacaoFATURA.value
    + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' + FormatFloat('##,##0.00',
    QOperacaoVL_FATURA.value) + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' +
    FormatDateTime('dd/mm/yyyy', QOperacaoDT_REC_FAT.value) + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' +
    FormatDateTime('dd/mm/yyyy', QOperacaoDT_EMI_BOL.value) + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' +
    FormatDateTime('dd/mm/yyyy', QOperacaoDT_VCTO.value) + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' + QOperacaoUSER_INC.value
    + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' +
    FormatDateTime('dd/mm/yyyy', QOperacaoDT_INC.value) + '</th>');
  Memo1.Lines.add('      <th><font class="texto2">' + QOperacaoOBSE.value
    + '</th>');
  Memo1.Lines.add('   </tr>');

  Memo1.Lines.add('</table></body>');
  Memo1.Lines.add('</html>');

  mail := QOperacaoE_MAIL.value;
  posIni := pos(';', QOperacaoE_MAIL.value);
  cc := copy(QOperacaoE_MAIL.value, 1, posIni - 1);

  mail := copy(mail, posIni + 1, length(mail) - posIni);
  posIni := pos(';', mail);
  cc1 := copy(mail, posIni + 1, length(mail) - posIni);

  if posIni > 0 then
  begin
    mail := copy(mail, posIni + 1, length(mail) - posIni);
    posIni := pos(';', mail);
    cc2 := copy(mail, posIni + 1, length(mail) - posIni);
  end;

  try
    ACBrMail1.clear;
    ACBrMail1.Body.Assign(Memo1.Lines);
    ACBrMail1.AddAttachment('C:\SIM\Arquivo.pdf', 'Arquivo');
    ACBrMail1.AddAddress(cc);
    if cc1 <> '' then
      ACBrMail1.AddCC(cc1);
    if cc2 <> '' then
      ACBrMail1.AddCC(cc2);
    ACBrMail1.Subject := 'Fatura Faltando Receita';
    ACBrMail1.Send(false);

  finally

  end;
end;

procedure TfrmCadOcor_fat_pagar_2.btnExcelClick(Sender: TObject);
var
  coluna, linha: Integer;
  excel: variant;
  valor: string;
begin
  try
    excel := CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
  except
    Application.MessageBox('Vers�o do Ms-Excel Incompat�vel', 'Erro',
      MB_OK + MB_ICONEXCLAMATION);
  end;
  QOperacao.First;
  Gauge1.MaxValue := QOperacao.RecordCount;
  try
    for linha := 0 to QOperacao.RecordCount - 1 do
    begin
      for coluna := 1 to dbgContrato.Columns.Count do
      begin
        valor := QOperacao.Fields[coluna - 1].AsString;
        if valor <> '' then
        begin
          if QOperacao.Fields[coluna - 1].DataType = ftDateTime then
          begin
            excel.cells[linha + 2, coluna].NumberFormat := 'dd/mm/aaaa';
            excel.cells[linha + 2, coluna] := QOperacao.Fields[coluna - 1]
              .AsDateTime;
          end
          else if QOperacao.Fields[coluna - 1].DataType = ftString then
          begin
            excel.cells[linha + 2, coluna].NumberFormat := '@';
            excel.cells[linha + 2, coluna] := QOperacao.Fields
              [coluna - 1].AsString;
          end
          else if QOperacao.Fields[coluna - 1].DataType = ftBCD then
          begin
            excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
            excel.cells[linha + 2, coluna] := QOperacao.Fields
              [coluna - 1].AsFloat;
          end
          else if QOperacao.Fields[coluna - 1].DataType = ftFloat then
          begin
            excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
            excel.cells[linha + 2, coluna] := QOperacao.Fields
              [coluna - 1].AsFloat;
          end
          else if QOperacao.Fields[coluna - 1].DataType = ftInteger then
          begin
            excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0';
            excel.cells[linha + 2, coluna] := QOperacao.Fields[coluna - 1]
              .AsInteger;
          end
          else
          begin
            excel.cells[linha + 2, coluna].NumberFormat := '@';
            excel.cells[linha + 2, coluna] := QOperacao.Fields
              [coluna - 1].AsString;
          end;
        end;
      end;
      Gauge1.AddProgress(1);
      QOperacao.Next;
    end;
    for coluna := 1 to dbgContrato.Columns.Count do
    begin
      valor := dbgContrato.Columns[coluna - 1].Title.Caption;
      excel.cells[1, coluna] := valor;
    end;
    excel.Columns.AutoFit;
    excel.Visible := true;
  except
    Application.MessageBox('Aconteceu um erro desconhecido durante a convers�o'
      + 'da tabela para o Ms-Excel', 'Erro', MB_OK + MB_ICONEXCLAMATION);
  end;

end;

procedure TfrmCadOcor_fat_pagar_2.btnAlterarClick(Sender: TObject);
begin
  Restaura;
  tipo := 'A';
  edValor.ReadOnly := true;
  edRecFat.ReadOnly := true;
  edDt_Bol.ReadOnly := true;
  edVcto_Bol.ReadOnly := true;
  edObs.ReadOnly := true;
  edSolucao.ReadOnly := false;
  edemail.ReadOnly := true;
  edFatura.ReadOnly := true;
  ednmTransp.ReadOnly := true;
  edSolucao.SetFocus;
  self.tag := QOperacaoTRANSP.AsInteger;
end;

procedure TfrmCadOcor_fat_pagar_2.QOperacaoAfterScroll(DataSet: TDataSet);
begin
  edValor.value := QOperacaoVL_FATURA.value;
  edRecFat.Date := QOperacaoDT_REC_FAT.value;
  edDt_Bol.Date := QOperacaoDT_EMI_BOL.value;
  edVcto_Bol.Date := QOperacaoDT_VCTO.value;

  edObs.text := QOperacaoOBSE.value;
  edSolucao.Date := QOperacaoDT_SOLUCAO.value;
  edemail.text := QOperacaoE_MAIL.value;
  edFatura.text := QOperacaoFATURA.value;
  ednmTransp.text := QOperacaoNM_TRANSP.value;

  if QOperacaoANEXO.IsNull then
    btnPDF.Enabled := false
  else
    btnPDF.Enabled := true;
end;

procedure TfrmCadOcor_fat_pagar_2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QOperacao.Close;
end;

procedure TfrmCadOcor_fat_pagar_2.FormCreate(Sender: TObject);
begin
  QOperacao.open;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 +
    IntToStr(QOperacao.RecordCount);
end;

procedure TfrmCadOcor_fat_pagar_2.RadioGroup1Click(Sender: TObject);
begin
  QOperacao.Close;
  if RadioGroup1.ItemIndex = 0 then
    QOperacao.sql[2] := ''
  else if RadioGroup1.ItemIndex = 1 then
    QOperacao.sql[2] := 'Where dt_solucao is null'
  else if RadioGroup1.ItemIndex = 2 then
    QOperacao.sql[2] := 'Where dt_solucao is not null';
  QOperacao.open;
  Gauge1.MaxValue := 0;
end;

procedure TfrmCadOcor_fat_pagar_2.Restaura;
begin
  // btnInserir.Enabled  := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;
end;

procedure TfrmCadOcor_fat_pagar_2.ACBrMail1AfterMailProcess(Sender: TObject);
begin
  ShowMessage('E-mail Enviado: ' + TACBrMail(Sender).Subject);
end;

procedure TfrmCadOcor_fat_pagar_2.ACBrMail1MailException(const AMail: TACBrMail;
  const E: Exception; var ThrowIt: Boolean);
begin
  ShowMessage(E.message);
  ThrowIt := false;
  ShowMessage('*** Erro ao Enviar o email: ' + AMail.Subject);
end;

procedure TfrmCadOcor_fat_pagar_2.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).tag of
    1:
      QOperacao.First;
    2:
      QOperacao.Prior;
    3:
      QOperacao.Next;
    4:
      QOperacao.Last;
  end;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 +
    IntToStr(QOperacao.RecordCount);
  Screen.Cursor := crDefault;
end;

end.
