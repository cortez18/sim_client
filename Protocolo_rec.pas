unit Protocolo_rec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ADODB, DBCtrls, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, DB, ExtCtrls, ComCtrls;

type
  TfrmProtocolo_rec = class(TForm)
    QPendente: TADOQuery;
    dtsPendente: TDataSource;
    QPendenteCOD: TBCDField;
    QPendenteNR_NOTA: TStringField;
    QPendenteDT_NOTA: TDateTimeField;
    QPendenteFORNECEDOR: TStringField;
    QPendenteDT_VENCIMENTO: TDateTimeField;
    QPendenteDT_RECEBIMENTO: TDateTimeField;
    QPendenteDT_LANCAMENTO: TDateTimeField;
    QPendenteDT_ENTREGA: TDateTimeField;
    QPendenteNR_SOLCITACAO: TStringField;
    QPendenteUSUARIO: TStringField;
    QPendenteFL_STATUS: TStringField;
    QPendenteOBSERVACAO: TStringField;
    QPendenteCODCLIFOR: TBCDField;
    QPendenteRAZSOC: TStringField;
    QPendenteCODCGC: TStringField;
    SPEntrada: TADOStoredProc;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    JvDBGrid2: TJvDBGrid;
    QRecebido: TADOQuery;
    BCDField1: TBCDField;
    StringField1: TStringField;
    DateTimeField1: TDateTimeField;
    StringField2: TStringField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    DateTimeField4: TDateTimeField;
    DateTimeField5: TDateTimeField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    BCDField2: TBCDField;
    StringField7: TStringField;
    StringField8: TStringField;
    dtsRecebido: TDataSource;
    Panel2: TPanel;
    Label10: TLabel;
    DBNavigator1: TDBNavigator;
    btnGravar: TBitBtn;
    Memo1: TMemo;
    procedure btnGravarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid2TitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProtocolo_rec: TfrmProtocolo_rec;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmProtocolo_rec.btnGravarClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if Application.Messagebox('Voc� Deseja Gerar Este Recebimento ?',
    'Protocolo - NF', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    SPEntrada.Parameters[0].value := 0;
    SPEntrada.Parameters[1].value := QPendenteCOD.AsInteger;
    SPEntrada.Parameters[2].value := QPendenteCODCLIFOR.AsInteger;
    SPEntrada.Parameters[3].value := QPendenteDT_RECEBIMENTO.value;
    SPEntrada.Parameters[4].value := QPendenteDT_NOTA.value;
    SPEntrada.Parameters[5].value := GlbUser;
    SPEntrada.Parameters[6].value := Date;
    SPEntrada.Parameters[7].value := QPendenteNR_SOLCITACAO.value;
    SPEntrada.Parameters[8].value := Memo1.Text;
    SPEntrada.Parameters[9].value := QPendenteNR_NOTA.value;
    SPEntrada.Parameters[10].value := 'R';
    SPEntrada.ExecProc;
    ShowMessage('Protocolo Recebido');
    QPendente.Close;
    QPendente.Open;
    QRecebido.Close;
    QRecebido.Open;
  end;

end;

procedure TfrmProtocolo_rec.JvDBGrid1TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', QPendente.SQL.Text) > 0 then
    QPendente.SQL.Text := Copy(QPendente.SQL.Text, 1,
      Pos('order by', QPendente.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QPendente.SQL.Text := QPendente.SQL.Text + ' order by ' + Column.FieldName;
  QPendente.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
    JvDBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmProtocolo_rec.JvDBGrid2TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', QRecebido.SQL.Text) > 0 then
    QRecebido.SQL.Text := Copy(QRecebido.SQL.Text, 1,
      Pos('order by', QRecebido.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QRecebido.SQL.Text := QRecebido.SQL.Text + ' order by ' + Column.FieldName;
  QRecebido.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid1.Columns.Count - 1 do
    JvDBGrid1.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmProtocolo_rec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QPendente.Close;
  QRecebido.Close;
end;

procedure TfrmProtocolo_rec.FormCreate(Sender: TObject);
begin
  QPendente.Open;
  QRecebido.Open;
end;

end.
