object frmMonRoadNet: TfrmMonRoadNet
  Left = 0
  Top = 0
  Caption = 'Monitoramento RoadNet'
  ClientHeight = 531
  ClientWidth = 1014
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 37
    Width = 1014
    Height = 494
    Align = alClient
    TabOrder = 1
    object grMonitor: TDBGrid
      Left = 1
      Top = 1
      Width = 1012
      Height = 492
      Align = alClient
      DataSource = dsMonitor
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = grMonitorDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'ID_ROADNET'
          Title.Caption = 'ID'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATAINC'
          Title.Caption = 'Data'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODCON'
          Title.Caption = 'CT-e/NFs'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_CLI'
          Title.Caption = 'Cliente'
          Width = 204
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DOC'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NM_STATUS'
          Title.Caption = 'Status'
          Width = 267
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS'
          Title.Caption = 'Status'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'COD_USUARIO'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'ROTA'
          Title.Caption = 'Rota'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VEICULO'
          Title.Caption = 'Ve'#237'culo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATAROTA'
          Title.Caption = 'Data Rota'
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 37
    Align = alTop
    TabOrder = 0
    object lbTotReg: TLabel
      Left = 11
      Top = 10
      Width = 91
      Height = 13
      Caption = 'Total de Registros:'
    end
    object Label4: TLabel
      Left = 502
      Top = 10
      Width = 6
      Height = 13
      Caption = #224
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dtInicial: TJvDateEdit
      Left = 409
      Top = 4
      Width = 85
      Height = 21
      Date = 41049.000000000000000000
      DateFormat = 'dd/mm/yyyy'
      DateFormatPreferred = pdCustom
      ShowNullDate = False
      TabOrder = 0
    end
    object dtFinal: TJvDateEdit
      Left = 519
      Top = 4
      Width = 88
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object cbTipo: TComboBox
      Left = 657
      Top = 4
      Width = 176
      Height = 21
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 2
      Text = 'Em Processamento de Retorno'
      Items.Strings = (
        ''
        'Em Processamento de Retorno'
        'Enviado RN'
        'Retornado RN'
        'Aguardando Aceite'
        'Aguardando Agendamento'
        '')
    end
    object btnPesquisa: TBitBtn
      Left = 289
      Top = 4
      Width = 96
      Height = 27
      Hint = 'Filtro por Data'
      Caption = 'Pesquisar'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
        FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
        96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
        92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
        BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
        CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
        D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
        EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
        E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
        E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
        8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
        E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
        FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
        C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
        CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
        CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
        D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
        9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnPesquisaClick
    end
    object btnReprocessar: TBitBtn
      Left = 857
      Top = 4
      Width = 96
      Height = 27
      Hint = 'Reprocessar arquivo'
      Caption = 'Reprocessar'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFD6E9B70FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF6197654F8853FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFB493D59D74
        D19668CE9263CB8E5EC98A5BC7875666945B569D5E53995A2A712F266B2B2366
        274C723AC6D7C7FFFFFFD7A175F8F2EDF7F0EAF6EDE6F4EAE2F3E7DE529A5860
        A7688DCD978ACB9487CA9184C98E81C88C60A7684D8250C6D7C7D9A47AF9F3EE
        EBD2BEFFFFFFEBD3BFFFFFFFFFFFFF75B17B62A96A5DA46535803B317A365197
        5882C88D5BA1637AA27CDDA87EF9F3EFEBD0BAEBD0BBEBD0BBF4E6DAF4EFE7F9
        F1EC70AB72609F62F4E6D9F4E6D96197632D7533296F2E407C44DFAA82F9F3EF
        EACEB7FFFFFFEBD0BBFFFFFFFFFFFFFFFFFFF9F2EC82B886FFFFFFFFFFFFF7F0
        EBC88D5FFFFFFFFFFFFFE1AE87FAF4F0EACBB2EACCB3EACCB3F6E9DEF9F1EAF9
        F2EBF3E5D9F5E6DBF3E3D77CAC78F5EFE9C48654FFFFFFFFFFFFE3B18CFAF6F1
        EAC9AEFFFFFFEAC9B074C57E5DB8685AB3647CBB7DFFFFFFFFFFFF68AC6F6EAA
        72C58655F8FBF8FFFFFFE5B48FFAF6F2E9C6AAE9C6ACEAC7AC9ECF988ECF97AA
        D9B17AC38357AF6152A95C6FB7786BB37468924FFFFFFFFFFFFFE7B794FBF7F4
        E9C3A6FFFFFFE8C4A9D9F1DC84CF8D94D29CABDAB2A8D9AFA5D8ADA2D6AA9FD5
        A76CB4745FA566FFFFFFE9BA98FBF7F4E9C3A6E9C3A6E9C3A6EFD3BDD3E0C38B
        CF9063C06F60BC6B5DB76779C28275BE7E73A15CFFFFFFFFFFFFEBBD9BFBF7F4
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7AC58382C5
        89D1976AFFFFFFFFFFFFECBF9EFBF7F49CD5A598D3A194D09D90CE988BCB9387
        C98EA3D5A8B9DFBCCDE8CF7FC987F9F6F2D49B6FFFFFFFFFFFFFEFC6A8FBF7F4
        FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7
        F4D8A378FFFFFFFFFFFFF7E1D2F1C8ACEDC09FEBBE9DEBBC9AE9BA96E7B793E6
        B590E4B28CE2AF88E0AC84DDA980DCA57DE2B696FFFFFFFFFFFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      TabStop = False
      OnClick = btnReprocessarClick
    end
  end
  object qrMonitor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select r.* , c.nomeab nome_cli,'
      'case when r.status = '#39'I'#39' then '#39'Enviado RN'#39
      '     when r.status = '#39'R'#39' then '#39'Retornado RN'#39
      '     when r.status = '#39'A'#39' then '#39'Aguardando Aceite'#39
      '     when r.status = '#39'G'#39' then '#39'Aguardando Agendamento'#39
      '     else '#39'Em Processamento de Retorno'#39
      'end nm_status, v.codcon, c.codcgc'
      
        'from tb_roadnet r left join cyber.rodcli c on c.codclifor = r.cl' +
        'iente'
      
        '                  left join vw_rodnet_romaneio v on v.id_road = ' +
        'r.id_roadnet'
      ''
      ''
      ''
      ''
      ''
      'order by 1')
    Left = 248
    Top = 176
    object qrMonitorID_ROADNET: TFMTBCDField
      FieldName = 'ID_ROADNET'
      Precision = 38
      Size = 0
    end
    object qrMonitorCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 50
    end
    object qrMonitorNOME_CLI: TStringField
      FieldName = 'NOME_CLI'
      Size = 50
    end
    object qrMonitorDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object qrMonitorDOC: TStringField
      FieldName = 'DOC'
      Size = 3
    end
    object qrMonitorNM_STATUS: TStringField
      FieldName = 'NM_STATUS'
      ReadOnly = True
      Size = 40
    end
    object qrMonitorSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object qrMonitorCOD_USUARIO: TFMTBCDField
      FieldName = 'COD_USUARIO'
      Precision = 38
      Size = 0
    end
    object qrMonitorROTA: TStringField
      FieldName = 'ROTA'
      Size = 10
    end
    object qrMonitorVEICULO: TStringField
      FieldName = 'VEICULO'
    end
    object qrMonitorDATAROTA: TDateTimeField
      FieldName = 'DATAROTA'
    end
    object qrMonitorCODCON: TFMTBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object qrMonitorCODCGC: TStringField
      FieldName = 'CODCGC'
    end
  end
  object dsMonitor: TDataSource
    AutoEdit = False
    DataSet = qrMonitor
    Left = 312
    Top = 176
  end
  object SP_Road: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_ROADNET'
    Parameters = <
      item
        Name = 'VID_ROADNET'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSER'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'vcnpj'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VVEIC'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VROTA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 404
    Top = 180
  end
end
