unit ConsManifesto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  JvDBControls, jpeg, JvRichEdit, JvDBRichEdit, JvMemoryDataset, pcnConversao,
  ShellApi, xmldom, XMLIntf, msxmldom, XMLDoc, Vcl.Imaging.pngimage;

type
  TfrmConsManifesto = class(TForm)
    QManifesto: TADOQuery;
    navnavig: TDataSource;
    Panel2: TPanel;
    JvDBGrid2: TJvDBGrid;
    QCtrc: TADOQuery;
    dtsCtrc: TDataSource;
    QManifestoMANIFESTO: TBCDField;
    QManifestoSERIE: TStringField;
    QManifestoDATAINC: TDateTimeField;
    QManifestoOPERACAO: TStringField;
    QManifestoPLACA: TStringField;
    QManifestoUSUARIO: TStringField;
    QManifestoMDFE_CHAVE: TStringField;
    QManifestoMDFE_PROTOCOLO: TStringField;
    QManifestoMDFE_ENCERRAMENTO: TStringField;
    QManifestoNM_FORNECEDOR: TStringField;
    QManifestoDATA_S: TDateTimeField;
    QManifestoFROTA: TStringField;
    QManifestoNM_TRANSP: TStringField;
    QManifestoDATA_CHEGADA: TDateTimeField;
    QManifestoSTATUS: TStringField;
    QMapaItens: TADOQuery;
    QMapaItensDATAINC: TDateTimeField;
    QMapaItensNM_EMP_ORI_TA: TStringField;
    QMapaItensNM_EMP_DEST_TA: TStringField;
    QMapaItensNR_QUANTIDADE: TBCDField;
    QMapaItensPESO: TBCDField;
    QMapaItensNR_MANIFESTO: TBCDField;
    QMapaItensFL_EMPRESA: TBCDField;
    QMapaItensNR_CTRC: TBCDField;
    QMapaItensNR_VALOR: TBCDField;
    QMapaItensCIDADE: TStringField;
    qMapaCarga: TADOQuery;
    qMapaCargaNR_MANIFESTO: TBCDField;
    qMapaCargaDATAINC: TDateTimeField;
    qMapaCargaPLACA: TStringField;
    qMapaCargaVEICULO: TStringField;
    qMapaCargaNM_FORNECEDOR: TStringField;
    qMapaCargaNOMMOT: TStringField;
    qMapaCargaCNH: TStringField;
    qMapaCargaOBS: TStringField;
    QManifestoESTADO: TStringField;
    QCtrcPESO: TBCDField;
    QCtrcVLRMER: TBCDField;
    QCtrcNOTFIS: TStringField;
    dtsItens: TDataSource;
    Panel3: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label1: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    edMan: TJvCalcEdit;
    btnConsulta: TBitBtn;
    btnEncerrar: TBitBtn;
    Panel4: TPanel;
    JvDBGrid1: TJvDBGrid;
    Panel10: TPanel;
    Label29: TLabel;
    EdMotivo: TEdit;
    btnSalvarCancelar: TBitBtn;
    DBNavigator1: TDBNavigator;
    Gauge1: TGauge;
    BitBtn1: TBitBtn;
    QItens: TADOQuery;
    QItensDATAINC: TDateTimeField;
    QItensNM_EMP_ORI_TA: TStringField;
    QItensNM_EMP_DEST_TA: TStringField;
    QItensNR_QUANTIDADE: TBCDField;
    QItensPESO: TBCDField;
    QItensNR_MANIFESTO: TBCDField;
    QItensFL_EMPRESA: TBCDField;
    QItensNR_CTRC: TBCDField;
    QItensNR_VALOR: TBCDField;
    QItensCIDADE: TStringField;
    Label2: TLabel;
    edPlaca: TJvMaskEdit;
    RGStatus: TRadioGroup;
    QManifestoROMANEIO: TStringField;
    RgSerie: TRadioGroup;
    btnMapa: TBitBtn;
    qMapa: TADOQuery;
    qMapaNR_ROMANEIO: TBCDField;
    qMapaCOD_TA: TBCDField;
    qMapaDESTINO: TStringField;
    qMapaROTA: TStringField;
    qMapaCLIREDES: TStringField;
    qMapaLOCALENTREGA: TStringField;
    qMapaVEICULO: TStringField;
    qMapaCODPAG: TBCDField;
    Label3: TLabel;
    edMapa: TJvCalcEdit;
    qMapaCODFIL: TBCDField;
    QManifestoPARAFILIAL: TStringField;
    btnRecibo: TBitBtn;
    QItensDOC: TStringField;
    QManifestoOBS: TStringField;
    Label4: TLabel;
    edCte: TJvCalcEdit;
    QManifestoFILIAL: TBCDField;
    QManifestoUSER_ENCERRADO: TStringField;
    QManifestoMDFE_CANC: TStringField;
    DBMemo1: TDBMemo;
    QManifestoMOTIVO: TStringField;
    JvDBRichEdit1: TJvDBRichEdit;
    btnCheck: TBitBtn;
    QManifestoCARRETA: TStringField;
    QVolume: TADOQuery;
    QVolumeORDCOM: TStringField;
    QVols: TADOQuery;
    QVolsCODCON: TBCDField;
    QVolsCODFIL: TBCDField;
    QVolsORDCOM: TStringField;
    QVolsCODBAR: TStringField;
    QManifestoDESCRI: TStringField;
    QManifestoTRANSFERIDO: TBCDField;
    btnSefaz: TBitBtn;
    btnCancelar: TBitBtn;
    Label5: TLabel;
    edPedido: TJvCalcEdit;
    dsvols: TDataSource;
    MdVolumes: TJvMemoryData;
    MdVolumespedido: TStringField;
    MdVolumesvolume: TStringField;
    MdVolumesnr_romaneio: TIntegerField;
    MdVolumesdestino: TStringField;
    MdVolumescod_ta: TIntegerField;
    MdVolumescliredes: TStringField;
    MdVolumeslocalentrega: TStringField;
    MdVolumesrota: TStringField;
    MdVolumesnf: TStringField;
    qNFs: TADOQuery;
    qNFsNR_NOTA: TStringField;
    btnNEnc: TBitBtn;
    Memo1: TMemo;
    btnfintran: TBitBtn;
    btnImprimir: TBitBtn;
    Label6: TLabel;
    edNF: TJvCalcEdit;
    QKM: TADOQuery;
    XMLDoc: TXMLDocument;
    QKMDESCRI: TStringField;
    QKMCODCLIFOR: TBCDField;
    QKMGEO: TStringField;
    QKMCODCEP: TStringField;
    QItensAVERBA_PROTOCOLO: TStringField;
    QItensAVERBA_DATA: TDateTimeField;
    QAverba: TADOQuery;
    QAverbaCTE_CHAVE: TStringField;
    QAverbaDT_CONHECIMENTO: TDateTimeField;
    QItensCTE_CHAVE: TStringField;
    QAverbaCOD_CONHECIMENTO: TBCDField;
    QAverbaFL_EMPRESA: TBCDField;
   { RlManifesto: TRLReport;
    RLBand5: TRLBand;
    RLLogoIw: TRLImage;
    RLLogoInt: TRLImage;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    RLLabel36: TRLLabel;
    RLBand6: TRLBand;
    RLDBText9: TRLDBText;  }
    dtsMapaItens: TDataSource;
  {  RLDBText10: TRLDBText;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBText15: TRLDBText;
    RLBand7: TRLBand;
    RLBand8: TRLBand;
    RLLabel37: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel39: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;   }
    dtsMapa: TDataSource;
  {  RLDraw1: TRLDraw;
    RLMemo1: TRLMemo;
    RLLabel40: TRLLabel;
    RLDBText16: TRLDBText;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel43: TRLLabel;
    RLLabel44: TRLLabel;
    RLLabel45: TRLLabel;
    RLDBText17: TRLDBText;
    RLDBText18: TRLDBText;
    RLDBText19: TRLDBText;
    RLDBText20: TRLDBText;
    RLDBText21: TRLDBText;
    RLDBText22: TRLDBText;
    RLLabel46: TRLLabel;
    RLExpressionParser1: TRLExpressionParser;    }
    mdManifesto: TJvMemoryData;
    mdManifestoManifesto: TIntegerField;
    mdManifestodatainc: TDateField;
    mdManifestoplaca: TStringField;
    mdManifestoveiculo: TStringField;
    mdManifestonm_fornecedor: TStringField;
    mdManifestonommot: TStringField;
    mdManifestoCNH: TStringField;
    mdManifestocte: TIntegerField;
    mdManifestoremetente: TStringField;
    mdManifestodestino: TStringField;
    mdManifestocidade: TStringField;
    mdManifestovolume: TIntegerField;
    mdManifestopeso: TFloatField;
    mdManifestovalor: TFloatField;
    mdManifestoestado: TStringField;
    mdManifestooperacao: TStringField;
    mdManifestoobs: TMemoField;
    mdManifestonota: TMemoField;
 {   RLDBMemo1: TRLDBMemo;
    RLReport2: TRLReport;
    RLBand9: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLDBText6: TRLDBText;
    RLSystemInfo1: TRLSystemInfo;
    RLGroup2: TRLGroup;
    RLBand10: TRLBand;
    RLLabel3: TRLLabel;
    RLDBText2: TRLDBText;
    RLLabel4: TRLLabel;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLLabel6: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel8: TRLLabel;
    RLDBRichText1: TRLDBRichText;
    RLLabel5: TRLLabel;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLLabel20: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel7: TRLLabel;
    RLBand13: TRLBand;
    RLDBText8: TRLDBText;
    RLDBRichText2: TRLDBRichText;
    RLBand14: TRLBand;
    RLLabel15: TRLLabel;
    RLPeso: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLBand15: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLRichText1: TRLRichText;    }
    SP_Libera: TADOStoredProc;
    QPGR_M: TADOQuery;
    QPGR_MCLIENTE: TFMTBCDField;
    SP_PGR: TADOStoredProc;
    QManifestoTRANSP: TFMTBCDField;
    QPGR_MSUMVLRMER: TFMTBCDField;
    QPGR_MID_REGRA: TFMTBCDField;
    QPGR_MRAZSOC: TStringField;
    qMapaCargaUF_DESTINO: TStringField;
    QManifestoID_MAN: TFMTBCDField;
    QLiberaMapa: TADOQuery;
    QLiberaMapaLIBERADO: TStringField;
    dtsLibera: TDataSource;
    RELiberado: TJvRichEdit;
    Panel1: TPanel;
    JvDBGrid3: TJvDBGrid;
    BitBtn2: TBitBtn;
    Label7: TLabel;
    JvDBMaskEdit1: TJvDBMaskEdit;
    Label8: TLabel;
    JvDBDateEdit1: TJvDBDateEdit;
    BitBtn3: TBitBtn;
    btnEstornar: TBitBtn;
    lbl1: TLabel;
    lblT: TLabel;
    lblv: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    QManifestoROTA: TStringField;
    QXML: TADOQuery;
    QXMLCTE_CHAVE: TStringField;
    QXMLDT_EMISSAO: TDateTimeField;
    Qemail: TADOQuery;
    QemailEMAIL: TStringField;
    QItensCOD_TAB: TFMTBCDField;
    Button1: TButton;
    QCusto: TADOQuery;
    QCustoCUSTO: TBCDField;
    QManifestoFL_CONTINGENCIA: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edManEnter(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure dtInicialEnter(Sender: TObject);
    procedure btnConsultaClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnFecharCtrcClick(Sender: TObject);
    procedure btnEncerrarClick(Sender: TObject);
    procedure QManifestoBeforeOpen(DataSet: TDataSet);
    procedure btnSefazClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnSalvarCancelarClick(Sender: TObject);
    procedure QManifestoAfterScroll(DataSet: TDataSet);
    procedure QItensAfterScroll(DataSet: TDataSet);
    procedure btnMapaClick(Sender: TObject);
    procedure btnReciboClick(Sender: TObject);
    procedure edMapaEnter(Sender: TObject);
    procedure edCteEnter(Sender: TObject);
    procedure btnCheckClick(Sender: TObject);
    procedure qMapaAfterScroll(DataSet: TDataSet);
    procedure btnNEncClick(Sender: TObject);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEstornarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnfintranClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBMaskEdit1Click(Sender: TObject);
   // procedure RLBand5BeforePrint(Sender: TObject; var PrintIt: Boolean);
   // procedure RLBand7BeforePrint(Sender: TObject; var PrintIt: Boolean);
   // procedure RLBand6BeforePrint(Sender: TObject; var PrintIt: Boolean);
   // procedure RLBand9BeforePrint(Sender: TObject; var PrintIt: Boolean);
   // procedure RLBand14BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    // iTotGeralVol : Integer;
    procedure calculakm;
    procedure gravageo;

  var
    km: double;
    lat, lon: String;
  public
    { Public declarations }
  end;

var
  frmConsManifesto: TfrmConsManifesto;

implementation

uses Dados, Menu, funcoes, UrlMon, ATM, ACBrDFeSSl, Manifesto_serieU;

{$R *.dfm}

procedure TfrmConsManifesto.BitBtn1Click(Sender: TObject);
var
  Memo1: TStringList;
  texto: string;
  wb: Variant;
  i: Integer;
begin
  Memo1 := TStringList.Create;
  texto := 'Relat�rio de Manifestos';

  Memo1.Clear();
  Memo1.Add('<HTML><HEAD><TITLE>Relat�rio de Manifestos</TITLE>');
  Memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  Memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  Memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add('</STYLE>');
  Memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  Memo1.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  Memo1.Add('<table border=0><title></title></head><body>');
  Memo1.Add('<table border=1 align=center>');
  Memo1.Add('<tr>');
  Memo1.Add('<th colspan=2><FONT class=titulo1>' + texto + '</th></font>');
  Memo1.Add('<tr>');
  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QManifesto.First;
  Gauge1.MaxValue := QManifesto.RecordCount;
  while not QManifesto.Eof do
  begin
    Memo1.Add('<tr>');
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      if QManifestoSTATUS.value = 'C' then
        Memo1.Add('<th><FONT class=texto3>' + QManifesto.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>')
      else
        Memo1.Add('<th><FONT class=texto2>' + QManifesto.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
    QManifesto.Next;
    Gauge1.AddProgress(1);
    Memo1.Add('</tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile('C:/SIM/ConsultaMan.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := true;
  wb.Navigate('C:/SIM/ConsultaMan.html');
end;

procedure TfrmConsManifesto.btnEncerrarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if QManifestoMDFE_PROTOCOLO.value = '' then
  begin
    ShowMessage('MDF-e Ainda sem Autoriza��o do Sefaz !!');
    Exit;
  end;

  if QManifestoMDFE_ENCERRAMENTO.value <> '' then
  begin
    ShowMessage('MDF-e J� Encerrado');
    Exit;
  end;

  if Application.Messagebox('Encerra Manifesto ?', 'Gerar Manifesto',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin

    frmMenu.EncerraMDFe(QManifestoMANIFESTO.AsInteger);

    QManifesto.close;
    QManifesto.open;
    QManifesto.locate('manifesto', QManifestoMANIFESTO.AsInteger, []);



  end;
end;

procedure TfrmConsManifesto.btnEstornarClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;

  if QManifestoDATA_CHEGADA.IsNull then
  begin
    ShowMessage('N�o houve Baixa Deste Manifesto !!!');
    Exit;
  end;

  if QManifestoSERIE.value = 'U' then
  begin

    if Application.Messagebox('Estornar a Baixa de Entrega ?',
      'Estornar a Baixa', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add('update tb_manifesto set ');
      dtmDados.IQuery1.sql.Add('data_chegada = null, user_encerrado = :0 ');
      dtmDados.IQuery1.sql.Add
        ('where manifesto = :1 and serie = ''U'' and filial = :2');
      dtmDados.IQuery1.Parameters[0].value := GLBUser;
      dtmDados.IQuery1.Parameters[1].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := glbfilial;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
      edMan.value := QManifestoMANIFESTO.AsInteger;
      btnConsultaClick(Sender);
    end;
  end
  else
    ShowMessage('Manifesto Eletr�nico n�o pode ser estornado !');
end;

procedure TfrmConsManifesto.BitBtn2Click(Sender: TObject);
var
  sarquivo: String;
begin
  // pegar arquivo que tenha IT de paletiza��o
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select DISTINCT (t.codpag ||''-''|| substr(ds.codcgc,1,10)) cnpj from cyber.rodord t left join cyber.rodcli ds on t.coddes = ds.codclifor ');
  dtmDados.IQuery1.sql.Add
    ('where (t.codman = :0 and t.filman = :1 and t.serman = :2) ');
  dtmDados.IQuery1.sql.Add('union all ');
  dtmDados.IQuery1.sql.Add
    ('select distinct (ct.cod_pagador ||''-''||substr(ds.codcgc,1,10)) cnpj from tb_conhecimento ct left join cyber.rodcli ds on ct.cod_destinatario = ds.codclifor ');
  dtmDados.IQuery1.sql.Add
    ('where nr_manifesto = :3 and fl_empresa = :4 and serie_man = :5 ');
  dtmDados.IQuery1.sql.Add('order by 1 ');
  dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
  dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
  dtmDados.IQuery1.Parameters[3].value := QManifestoMANIFESTO.AsInteger;
  dtmDados.IQuery1.Parameters[4].value := QManifestoFILIAL.AsInteger;
  dtmDados.IQuery1.Parameters[5].value := QManifestoSERIE.AsString;
  dtmDados.IQuery1.open;
  while not dtmDados.IQuery1.Eof do
  begin
    sarquivo := '\\192.168.236.112\sim\Atualizador\Relatorio\' +
      dtmDados.IQuery1.fieldbyname('cnpj').AsString + '.PDF';
    if fileExists(sarquivo) = true THEN
    begin
      ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
        SW_SHOWNORMAL);
    end;
    dtmDados.IQuery1.Next;
  end;
  dtmDados.IQuery1.close;
end;

procedure TfrmConsManifesto.BitBtn3Click(Sender: TObject);
var roma : String;
begin
  roma := QManifestoROMANEIO.AsString;
  QPGR_M.Close;
  QPGR_M.sql [8] := 'where r.nr_romaneio in ('+ roma + ')';
  QPGR_M.sql [19] := 'where r.nr_romaneio in ('+ roma + ')';
  QPGR_M.Open;

  while not QPGR_M.eof do
  begin
    if QPGR_MID_REGRA.isnull then
    begin
      showmessage('Valor M�ximo Permitido do PGR Ultrapassado !');
      showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end;
    SP_PGR.Parameters[2].Value := QPGR_MID_REGRA.AsInteger;
    SP_PGR.Parameters[3].Value := '';
    SP_PGR.Parameters[4].Value := '';
    SP_PGR.Parameters[5].Value := QManifestotransp.AsInteger;
    SP_PGR.Parameters[6].Value := QPGR_MSUMVLRMER.AsFloat;
    SP_PGR.ExecProc;
    if SP_PGR.Parameters[1].Value = 'S' then
    begin
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
      showmessage('Este erro ir� bloquear a viagem quando ap�s o processo homologado');
    end
    else if SP_PGR.Parameters[0].value <> null then
      showmessage(SP_PGR.Parameters[0].Value + 'Para o Cliente ' + QPGR_MRAZSOC.AsString);
    QPGR_M.Next;
  end;
end;

procedure TfrmConsManifesto.btnCancelarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  // if QManifestoSTATUS.value <> 'C' then
  // begin
  if QManifestoSERIE.value <> 'U' then
  begin
    if (QManifestoMDFE_PROTOCOLO.IsNull) then
    begin
      ShowMessage('Erro na Transmiss�o � necess�rio o Envio Novamente !');
      Exit;
    end;

    if QManifestoMDFE_CHAVE.value = '' then
    begin
      ShowMessage('MDF-e Ainda sem Autroriza��o do Sefaz');
      Exit;
    end;

    if QManifestoMDFE_ENCERRAMENTO.value <> '' then
    begin
      ShowMessage('MDF-e J� Encerrado');
      Exit;
    end;

    if QManifestoDATA_S.AsString <> '' then
    begin
      ShowMessage('MDF-e J� Est� Viajando !');
      Exit;
    end;
  end
  else
  begin
    if not QManifestoDATA_CHEGADA.IsNull then
    begin
      ShowMessage('Manifesto J� Teve Sa�da na Portaria !');
      Exit;
    end;
  end;

  Panel10.Visible := true;
  // end;
end;

procedure TfrmConsManifesto.btnCheckClick(Sender: TObject);
Var
  WinWord, Docs, Doc: Variant;
begin
  inherited;
  // Cria objeto principal de controle
  WinWord := CreateOleObject('Word.Application');
  // Mostra o Word
  WinWord.Visible := true;
  // Pega uma interface para o objeto que manipula documentos
  Docs := WinWord.Documents;
  if fileExists('\\192.168.236.112\SIM\ATUALIZADOR\CHECK_LIST.DOC') = false THEN
  begin
    ShowMessage('Avisar o TI que falta o arquivo do check-list !!');
    Exit;
  end;
  // Abre um Documento
  Doc := Docs.open('\\192.168.236.112\SIM\ATUALIZADOR\CHECK_LIST.DOC');
  // Substitui texto via "name parameters"
  Doc.Content.Find.Execute(FindText := '%nome%', ReplaceWith :=
    QManifestoNM_FORNECEDOR.value, Replace := 2);
  Doc.Content.Find.Execute(FindText := '%placa%', ReplaceWith :=
    QManifestoPLACA.value, Replace := 2);
  Doc.Content.Find.Execute(FindText := '%placa_carreta%', ReplaceWith :=
    QManifestoCARRETA.value, Replace := 2);
  Doc.Content.Find.Execute(FindText := '%veiculo%', ReplaceWith :=
    QManifestoFROTA.value, Replace := 2);
  Doc.Content.Find.Execute(FindText := '%viagem%', ReplaceWith :=
    QManifestoMANIFESTO.AsString, Replace := 2);
  Doc.Content.Find.Execute(FindText := '%data%', ReplaceWith :=
    formatdatetime('dd/mm/yyyy', QManifestoDATAINC.value), Replace := 2);
  { if QmanifestoFROTA.value > 0 then
    Doc.Content.Find.Execute(FindText := '%frota%', ReplaceWith := QmanifestoFROTA.AsString, Replace:=2)
    else }
  Doc.Content.Find.Execute(FindText := '%frota%', ReplaceWith := '',
    Replace := 2);
  // Replace:=2, para substituir todas as ocorr�ncias
  // Grava documento
  Doc.SaveAs('\\192.168.236.112\SIM\ATUALIZADOR\CHECK_LIST_1.DOC');
  // Imprime //
  Doc.PrintOut(false);
  // Fecha o Word
  WinWord.Quit;
end;

procedure TfrmConsManifesto.btnConsultaClick(Sender: TObject);
var
  ser, man: String;
  i: Integer;
begin
  lblT.Caption := '';
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      Exit;
    end;
  end;
  Application.ProcessMessages;
  if edCte.value > 0 then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add
      ('select distinct t.nr_manifesto, t.serie from vw_manifesto_detalhe t ');
    dtmDados.IQuery1.sql.Add('where t.nr_ctrc = :0 and t.fl_empresa = :1 ');
    dtmDados.IQuery1.Parameters[0].value := edCte.value;
    dtmDados.IQuery1.Parameters[1].value := glbfilial;
    dtmDados.IQuery1.open;
    i := 0;
    if not dtmDados.IQuery1.Eof then
    begin
      if dtmDados.IQuery1.RecordCount = 1 then
      begin
        edMan.Text := dtmDados.IQuery1.fieldbyname('nr_manifesto').value;
        ser := dtmDados.IQuery1.fieldbyname('serie').value;
      end
      else
      begin
        while not dtmDados.IQuery1.Eof do
        begin
          if i = 0 then
            man := dtmDados.IQuery1.fieldbyname('nr_manifesto').AsString
          else
            man := man + #39 + ',' + #39 + dtmDados.IQuery1.fieldbyname
              ('nr_manifesto').AsString;
          dtmDados.IQuery1.Next;
          i := i + 1;
        end;
      end;
    end
    else
    begin
      ShowMessage('Documento N�o Encontrado');
      edCte.SetFocus;
      Exit;
    end;
    dtmDados.IQuery1.close;
  end;
  // Procura por Pedido
  if edPedido.value > 0 then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add
      ('select distinct i.manifesto, i.serie from tb_cte_nf n left join tb_manitem i on n.codcon = i.coddoc and n.sercon = i.serdoc and n.fl_empresa = i.fildoc ');
    dtmDados.IQuery1.sql.Add('where n.ordcom = :0 and n.fl_empresa = :1');
    dtmDados.IQuery1.Parameters[0].value := edPedido.Text;
    dtmDados.IQuery1.Parameters[1].value := glbfilial;
    dtmDados.IQuery1.open;
    // i := 0;
    if not dtmDados.IQuery1.Eof then
    begin
      edMan.Text := dtmDados.IQuery1.fieldbyname('manifesto').value;
      ser := dtmDados.IQuery1.fieldbyname('serie').value;
    end
    else
    begin
      ShowMessage('Pedido N�o Encontrado');
      edPedido.SetFocus;
      Exit;
    end;
    dtmDados.IQuery1.close;
  end;
  // Procura por NF
  if edNF.value > 0 then
  begin
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add
      ('select distinct d.nr_manifesto, d.serie from vw_manifesto_detalhe d ');
    dtmDados.IQuery1.sql.Add('where d.notfis = :0 and d.fl_empresa = :1');
    dtmDados.IQuery1.Parameters[0].value := ApCarac(edNF.Text);
    dtmDados.IQuery1.Parameters[1].value := glbfilial;
    dtmDados.IQuery1.open;
    i := 0;
    if not dtmDados.IQuery1.Eof then
    begin
      if dtmDados.IQuery1.RecordCount = 1 then
      begin
        edMan.Text := dtmDados.IQuery1.fieldbyname('nr_manifesto').value;
        ser := dtmDados.IQuery1.fieldbyname('serie').value;
      end
      else
      begin
        while not dtmDados.IQuery1.Eof do
        begin
          if i = 0 then
            man := dtmDados.IQuery1.fieldbyname('nr_manifesto').AsString
          else
            man := man + #39 + ',' + #39 + dtmDados.IQuery1.fieldbyname
              ('nr_manifesto').AsString;
          dtmDados.IQuery1.Next;
          i := i + 1;
        end;
      end;
    end
    else
    begin
      ShowMessage('NF N�o Encontrada');
      edNF.SetFocus;
      Exit;
    end;
  end;

  QManifesto.close;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QManifesto.sql[15] := 'and m.datainc between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QManifesto.sql[15] := '';
  if (edMan.value > 0) or (man <> '') then
  begin
    if edMan.value > 0 then
      QManifesto.sql[16] := 'and m.manifesto = ' + #39 +
        ApCarac(edMan.Text) + #39
    else
      QManifesto.sql[16] := 'and m.manifesto in ( ' + #39 + man + #39 + ')';
  end
  else
    QManifesto.sql[16] := '';
  if edPlaca.Text <> '' then
    QManifesto.sql[17] := 'and m.placa = ' + #39 + edPlaca.Text + #39
  else
    QManifesto.sql[17] := '';

  if RGStatus.ItemIndex = 0 then
    QManifesto.sql[18] := ''
  else if RGStatus.ItemIndex = 1 then
    QManifesto.sql[18] :=
      'and m.mdfe_encerramento is not null and STATUS <> ''C'' '
  else if RGStatus.ItemIndex = 2 then
    QManifesto.sql[18] :=
      'and m.mdfe_encerramento is null and STATUS <> ''C'' ';

  if RgSerie.ItemIndex = 2 then
    QManifesto.sql[19] := ''
  else if RgSerie.ItemIndex = 0 then
    QManifesto.sql[19] := 'and m.serie = ''1'' '
  else if RgSerie.ItemIndex = 1 then
    QManifesto.sql[19] := 'and m.serie = ''U'' ';

  if edMapa.value > 0 then
  begin
    QManifesto.sql[20] := 'and manifesto in ( ';
    QManifesto.sql[21] :=
      'select nr_manifesto from tb_romaneio where nr_romaneio =' +
      QuotedStr(ApCarac(edMapa.Text)) + ')';
  end;
  // Qmanifesto.sql.savetofile('c:\sim\sql.txt');
  QManifesto.open;
  if not QManifesto.Eof then
  begin
    if edCte.value > 0 then
    begin
      QItens.locate('nr_ctrc', edCte.value, []);
    end;
  end;
  lblT.Caption := 'Total : ' + IntToStr(QManifesto.RecordCount);

end;

procedure TfrmConsManifesto.btnFecharCtrcClick(Sender: TObject);
begin
  QCtrc.close;
  Panel2.Visible := false;
end;

procedure TfrmConsManifesto.btnfintranClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if QManifestoMDFE_CHAVE.value <> '' then
  begin
    ShowMessage('Procure o TI Este Manifesto � Eletr�nico !!');
    Exit;
  end;

  if not QManifestoDATA_CHEGADA.IsNull then
  begin
    ShowMessage('Manifesto J� Encerrado!!');
    Exit;
  end;

  if Application.Messagebox
    ('Encerra Manifesto de Transfer�ncia, os Volumes ser�o liberados para nova Confer�ncia ?',
    'Encerra Manifesto', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin

    if (QManifestoPARAFILIAL.value = 'S') and (QManifestoDATA_CHEGADA.IsNull)
    then
    begin

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('update tb_manifesto set data_chegada = sysdate, user_encerrado = :0 ');
      dtmDados.IQuery1.sql.Add('where manifesto = ' +
        QuotedStr(QManifestoMANIFESTO.AsString));
      dtmDados.IQuery1.sql.Add('and filial = :1 ');
      dtmDados.IQuery1.sql.Add('and serie = :2');
      dtmDados.IQuery1.Parameters[0].value := GLBUser;
      dtmDados.IQuery1.Parameters[1].value := glbfilial;
      dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.ExecSQL;

      SP_Libera.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      SP_Libera.Parameters[1].value := 'U';
      SP_Libera.Parameters[2].value := QManifestoFILIAL.AsInteger;
      SP_Libera.ExecProc;
      ShowMessage('Documentos Liberados para Filial');
      // passei a libera��o dos volumes para procedure
      {dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('update cyber.itc_man_emb set manifesto_transf = manifesto, usu_conf_transf = cod_usu_conferencia, ');
      dtmDados.IQuery1.sql.Add
        ('dat_conf_transf = dat_conferencia, manifesto = null, cod_usu_conferencia = null, dat_conferencia = null ');
      dtmDados.IQuery1.sql.Add('where manifesto = ' +
        QuotedStr(QManifestoMANIFESTO.AsString));
      dtmDados.IQuery1.sql.Add('and cod_fil = :0 ');
      dtmDados.IQuery1.sql.Add('and serie_manif = :1');
      dtmDados.IQuery1.Parameters[0].value := glbfilial;
      dtmDados.IQuery1.Parameters[1].value :='U';
      dtmDados.IQuery1.ExecSQL;
      ShowMessage('Volumes Liberados para Confer�ncia nesta Filial');  }
    end;

  end;
end;

procedure TfrmConsManifesto.btnImprimirClick(Sender: TObject);
var
  nf: String;
begin
  qMapaCarga.close;
  qMapaCarga.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  qMapaCarga.Parameters[1].value := QManifestoSERIE.AsString;
  qMapaCarga.Parameters[2].value := glbfilial;
  qMapaCarga.open;
  QMapaItens.close;
  QMapaItens.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  QMapaItens.Parameters[1].value := QManifestoSERIE.AsString;
  QMapaItens.Parameters[2].value := glbfilial;
  QMapaItens.open;

  try
    Application.CreateForm(TfrmManifesto_serieU, frmManifesto_serieU);
    frmManifesto_serieU.mdManifesto.Active := false;
    frmManifesto_serieU.mdManifesto.Active := true;
    frmManifesto_serieU.tag := QMapaItens.RecordCount;
    While not QMapaItens.Eof do
    begin
      frmManifesto_serieU.mdManifesto.Insert;
      frmManifesto_serieU.mdManifestoManifesto.value := qMapaCargaNR_MANIFESTO.AsInteger;
      frmManifesto_serieU.mdManifestodatainc.value := qMapaCargaDATAINC.value;
      frmManifesto_serieU.mdManifestoplaca.value := qMapaCargaPLACA.AsString;
      frmManifesto_serieU.mdManifestonm_fornecedor.value := qMapaCargaNM_FORNECEDOR.AsString;
      frmManifesto_serieU.mdManifestonommot.value := qMapaCargaNOMMOT.AsString;
      frmManifesto_serieU.mdManifestoCNH.value := qMapaCargaCNH.AsString;
      frmManifesto_serieU.mdManifestoobs.AsString := qMapaCargaOBS.AsString;
      frmManifesto_serieU.mdManifestooperacao.value := QManifestoOPERACAO.value;
      frmManifesto_serieU.mdManifestoestado.value := QManifestoESTADO.value;
      frmManifesto_serieU.mdManifestoveiculo.Value := qMapaCargaVEICULO.AsString;

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('select distinct notfis from vw_manifesto_detalhe ');
      dtmDados.IQuery1.sql.Add
        ('where nr_ctrc = :0 and nr_manifesto = :1 order by 1 ');
      dtmDados.IQuery1.Parameters[0].value := QMapaItensNR_CTRC.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QMapaItensNR_MANIFESTO.AsInteger;
      dtmDados.IQuery1.open;
      nf := '';
      while not dtmDados.IQuery1.Eof do
      begin
        nf := dtmDados.IQuery1.fieldbyname('notfis').value + ' / ' + nf;
        dtmDados.IQuery1.Next;
      end;
      dtmDados.IQuery1.close;

      frmManifesto_serieU.mdManifestonota.value := nf;
      frmManifesto_serieU.mdManifestoremetente.value := QMapaItensNM_EMP_ORI_TA.value;
      frmManifesto_serieU.mdManifestodestino.value := QMapaItensNM_EMP_DEST_TA.value;
      frmManifesto_serieU.mdManifestocte.AsInteger := QMapaItensNR_CTRC.AsInteger;
      frmManifesto_serieU.mdManifestocidade.value := QMapaItensCIDADE.AsString;
      frmManifesto_serieU.mdManifestovolume.AsInteger := QMapaItensNR_QUANTIDADE.AsInteger;
      frmManifesto_serieU.mdManifestopeso.value := QMapaItensPESO.value;
      frmManifesto_serieU.mdManifestovalor.value := QMapaItensNR_VALOR.value;
      frmManifesto_serieU.mdManifesto.Post;

      QMapaItens.Next;
    end;
    frmManifesto_serieU.RlManifesto.Preview;
  finally
    frmManifesto_serieU.tag := 0;
    frmManifesto_serieU.Free;
  end;
  //RlManifesto.Preview;

  // QManifesto.close;
  // QManifesto.open;
end;

procedure TfrmConsManifesto.btnMapaClick(Sender: TObject);
var
  //roma, ped, vol, nf: string;
  //i: Integer;
  caminho: string;
begin
  // transfer�ncia para distribui��o
  if QManifestoTRANSFERIDO.value = 1 then
  begin

    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add('select romaneio from tb_manifesto ');
    dtmDados.IQuery1.sql.Add('where manifesto in (' +
      QManifestoROMANEIO.AsString + ')');
    dtmDados.IQuery1.sql.Add('and filial = :0 ');
    dtmDados.IQuery1.sql.Add('and serie = :1');
    dtmDados.IQuery1.Parameters[0].value := glbfilial;
    dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
    dtmDados.IQuery1.open;
   // i := 0;
    while not dtmDados.IQuery1.Eof do
    begin
      if dts = 'INTECDSV' then
        caminho := 'http://192.168.236.112/Relatorios.dsv/sim/sim0001.aspx?rom=' +
          dtmDados.IQuery1.fieldbyname('romaneio').value + '&fil=' + IntToStr(GlbFilial)
      else
        caminho := 'http://192.168.236.112/Relatorios/sim/sim0001.aspx?rom=' +
          dtmDados.IQuery1.fieldbyname('romaneio').value + '&fil=' + IntToStr(GlbFilial);

      dtmDados.IQuery1.Next;
    end;
    dtmDados.IQuery1.close;
    // QMapa.close;
    // QMapa.sql[7] := 'where r.nr_romaneio in (' + roma + ')';
    // QMapa.sql[16] := 'where r.nr_romaneio in (' + roma + ')';
    // QMapa.open;
  end
  else
  begin
    if edMapa.value = 0 then
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add('select nr_romaneio from tb_romaneio ');
      dtmDados.IQuery1.sql.Add('where nr_manifesto = :0 and fl_empresa = :1 ');
      dtmDados.IQuery1.sql.Add('and serie_manifesto = :2');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := glbfilial;
      dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.open;
      if not dtmDados.IQuery1.Eof then
      begin
        edMapa.value := dtmDados.IQuery1.fieldbyname('nr_romaneio').value;
        caminho := 'http://192.168.236.112/Relatorios/sim/sim0001.aspx?rom=' +
          dtmDados.IQuery1.fieldbyname('romaneio').value + '&fil=' +
          IntToStr(glbfilial);
      end
      else
      begin
        ShowMessage('N�o Encontrado o n� do Mapa');
        Exit;
      end;
    end
    else
    begin
      if not QManifesto.Eof then
      begin
        caminho := 'http://192.168.236.112/Relatorios/sim/sim0001.aspx?rom=' +
          edMapa.Text + '&fil=' + IntToStr(glbfilial);
      end
      else
      begin
        ShowMessage('N�o Encontrado o n� do Mapa');
        Exit;
      end;
    end;

    // QMapa.close;
    // QMapa.Parameters[0].value := edMapa.Value;
    // QMapa.Parameters[1].value := edMapa.Value;
    // QMapa.open;

  end;
  { Mdvolumes.Active := false;
    Mdvolumes.Active := true;
    while not qMapa.Eof do
    begin
    qNFs.close;
    qNFs.Parameters[0].value := qMapaCODFIL.AsInteger;
    qNFs.Parameters[1].value := qMapaCOD_TA.AsString;
    qNFs.Parameters[2].value := qMapaCODFIL.AsInteger;
    qNFs.Parameters[3].value := qMapaCOD_TA.AsString;
    qNFs.Open;
    nf := '';
    while not qNFs.Eof do
    begin
    nf := nf +' '+ qNFsNR_NOTA.AsString;
    qNFs.Next;
    end;
    QVols.close;
    QVols.Parameters[0].value := qMapaCOD_TA.AsInteger;
    QVols.Parameters[1].value := qMapaCODFIL.AsInteger;
    QVols.open;
    ped := '';
    vol := '';
    while not QVols.eof do
    begin
    if ped = '' then
    begin
    ped := QVolsORDCOM.AsString;
    vol := QVolsCODBAR.AsString;
    end
    else
    begin
    if QVolsORDCOM.AsString = ped then
    vol := vol +' '+QVolsCODBAR.AsString
    else
    begin
    Mdvolumes.Insert;
    Mdvolumesnr_romaneio.Value := qMapaNR_ROMANEIO.AsInteger;
    Mdvolumescod_ta.Value := qMapaCOD_TA.AsInteger;
    Mdvolumesdestino.Value := qMapaDESTINO.AsString;
    Mdvolumescliredes.Value := qMapaCLIREDES.AsString;
    Mdvolumeslocalentrega.Value := qMapaLOCALENTREGA.AsString;
    Mdvolumesrota.Value := qMapaROTA.AsString;
    Mdvolumesnf.Value := nf;
    Mdvolumespedido.Value := ped;
    Mdvolumesvolume.Value := vol;
    Mdvolumes.Post;
    ped := '';
    ped := QVolsORDCOM.AsString;
    vol := QVolsCODBAR.AsString;
    end;
    end;
    QVols.Next;
    end;
    Mdvolumes.Insert;
    Mdvolumesnr_romaneio.Value := qMapaNR_ROMANEIO.AsInteger;
    Mdvolumescod_ta.Value := qMapaCOD_TA.AsInteger;
    Mdvolumesdestino.Value := qMapaDESTINO.AsString;
    Mdvolumescliredes.Value := qMapaCLIREDES.AsString;
    Mdvolumeslocalentrega.Value := qMapaLOCALENTREGA.AsString;
    Mdvolumesrota.Value := qMapaROTA.AsString;
    Mdvolumesnf.Value := nf;
    Mdvolumespedido.Value := ped;
    Mdvolumesvolume.Value := vol;
    Mdvolumes.Post;
    qMapa.Next
    end;
    RlReport2.Preview;
    QMapa.Close; }
  Winexec(PAnsiChar(caminho), Sw_Show);
  ShellExecute(Handle, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);
end;

procedure TfrmConsManifesto.btnNEncClick(Sender: TObject);
var
  sCNPJ, chave, proto, texto: String;
  j: Integer;
begin
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;

  frmMenu.ACBrMDFe1.Configuracoes.Geral.SSLLib:= libCapicom;
  frmMenu.ACBrMDFe1.Configuracoes.Geral.RetirarAcentos := False;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select distinct codcgc from cyber.rodfil where codfil = :0');;
  dtmDados.IQuery1.Parameters[0].value := glbfilial;
  dtmDados.IQuery1.open;
  sCNPJ := ApCarac(dtmDados.IQuery1.fieldbyname('codcgc').value);
  frmMenu.ACBrMDFe1.Configuracoes.Geral.RetirarAcentos := false;
  frmMenu.ACBrMDFe1.ConsultarMDFeNaoEnc(sCNPJ);

  Memo1.Lines.Text :=
    UTF8Encode(frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.RetWS);
  Memo1.Visible := true;
  Memo1.Clear;
  Memo1.Lines.Add('');
  for j := 0 to frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.
    Count - 1 do
  begin
    chave := copy(frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.Items[j]
      .chMDFe, 1, 24) + ' ' +
      copy(frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.Items[j].chMDFe,
      25, 10) + ' ' + copy(frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.
      Items[j].chMDFe, 35, 10);
    proto := frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.Items
      [j].nProt;
    Memo1.Lines.Add('Chave : ' + chave + '        Protocolo : ' + proto);
  end;
  Memo1.SelStart := 0;
  Memo1.Lines.Strings[0] := Memo1.Lines.Strings[0] +
    'Manifestos N�o Encerrados - ' + IntToStr(j);
  texto := 'Manifestos N�o Encerrados - ' + IntToStr(j);
  frmMenu.ACBrMDFe1.Configuracoes.Geral.SSLLib := libCapicomDelphiSoap;
  frmMenu.ACBrMDFe1.Configuracoes.Geral.RetirarAcentos := true;
end;

procedure TfrmConsManifesto.btnReciboClick(Sender: TObject);
var arquivo, arqxml : String;
    cc, mensagem, xx : TStrings;
    sData, sCaminho, sCte, para : string;
    MS: TMemoryStream;
    i : Integer;
begin
  // Projeto 33 enviar os xmls do nossos CT-es ao transportador
  xx := TStringList.Create;
  cc := TStringList.Create;
  mensagem := TStringList.Create;
  // email
  QEmail.Close;
  QEmail.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
  QEmail.Parameters[1].Value := QManifestoserie.AsString;
  QEmail.Parameters[2].Value := glbFilial;
  QEmail.Open;
  while not QEmail.eof do
  begin
    cc.Add(QemailEMAIL.asstring);
    QEmail.Next;
  end;
  QEmail.Close;

  para := 'mensageiro@intecomlogistica.com.br';
  //cc.Add('paulo.cortez@intecomlogistica.com.br');

  QXML.Close;
  QXML.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
  QXML.Parameters[1].Value := QManifestoserie.AsString;
  QXML.Parameters[2].Value := glbFilial;
  QXML.Open;
  while not QXML.eof do
  begin
    sData := FormatDateTime('YYYY', QXMLDT_EMISSAO.value) +
             FormatDateTime('MM', QXMLDT_EMISSAO.value);
    sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sData + '\CTe\';
    arquivo := scaminho + QXMLCTE_CHAVE.asstring + '-cte.xml';
    arqxml :=  QXMLCTE_CHAVE.asstring + '-cte.xml';
    xx.Add(arquivo); //frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar +'\'+ QXMLCTE_CHAVE.asstring+'-cte.xml');

    MS := TMemoryStream.Create;
    try
      MS.LoadFromFile(arquivo);
      frmMenu.ACBrMail1.AddAttachment(MS, arqXML);
    finally
      MS.Free;
    end;

    Qxml.Next;
  end;
  mensagem.add('Estamos anexo os CT-e�s do Manifesto ' + QmanifestoManifesto.AsString);
  Mensagem.Add(' ');
  Mensagem.Add('>>> Sistema SIM - TI Intecom/IW <<<');


  frmMenu.ACBrCTe1.EnviarEmail(para //Para
                               , 'CT-e�s do Manifesto ' + QManifestoMANIFESTO.AsString //edtEmailAssunto.Text
                               , mensagem //mmEmailMsg.Lines
                               , cc //Lista com emails que ser�o enviado c�pias - TStrings
                               , xx // Lista de anexos - TStrings
                                );
  xx.Free;
  mensagem.Free;
  QXML.Close;
  ShowMessage('E-mail enviado com sucesso !!');

end;

procedure TfrmConsManifesto.btnSalvarCancelarClick(Sender: TObject);
var
  man: Integer;
begin
  if EdMotivo.Text = '' then
  begin
    Panel10.Visible := false;
    Exit;
  end;

  if Length(EdMotivo.Text) < 10 then
  begin
    ShowMessage('O Motivo deve ter mais que 10 caracteres');
    EdMotivo.SetFocus;
    Exit;
  end;

  man := QManifestoMANIFESTO.AsInteger;

  if QManifestoSERIE.value = '1' then
  begin
    frmMenu.CancelamentoMDFe(QManifestoMANIFESTO.AsInteger, EdMotivo.Text);

    QManifesto.close;
    QManifesto.open;
    QManifesto.locate('manifesto', man, []);

    Panel10.Visible := false;

    if QManifestoMDFE_CANC.IsNull then
    begin
      ShowMessage('Erro no envio ao Sefaz, processe novamente');
      Exit;
    end;

    // se for de transferencia para dstribui��o n�o pode apagar
    if (QManifestoTRANSFERIDO.IsNull) then
    begin
      if not QManifestoMDFE_CANC.IsNull then
      begin
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add('update tb_romaneio set nr_manifesto = null ');
        dtmDados.IQuery1.sql.Add
          ('where nr_manifesto = :0 and fl_empresa = :1 ');
        dtmDados.IQuery1.sql.Add('and serie_manifesto = ''1'' ');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := glbfilial;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;

        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add('delete from tb_roadnet where id_roadnet in (select distinct nr_romaneio from tb_romaneio where nr_manifesto = :0 and fl_empresa = :1 and serie_manifesto = :2) ');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := glbfilial;
        dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;

        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update cyber.rodcon set codman = '''', serman = '''', ');
        dtmDados.IQuery1.sql.Add('filman = '''', agecar = '''' ');
        dtmDados.IQuery1.sql.Add('where codman = :0 ');
        dtmDados.IQuery1.sql.Add('and serman = :1 ');
        dtmDados.IQuery1.sql.Add('and filman = :2 ');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update cyber.rodord set codman = '''', serman = '''', ');
        dtmDados.IQuery1.sql.Add('filman = '''', agecar = '''' ');
        dtmDados.IQuery1.sql.Add('where codman = :0 ');
        dtmDados.IQuery1.sql.Add('and serman = :1 ');
        dtmDados.IQuery1.sql.Add('and filman = :2 ');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        // limpa volumes da trip
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update dashboard.skf_tb_trip set user_exp = null, data_exp = null,data_fim_exp = null ');
        dtmDados.IQuery1.sql.Add('where box in (select distinct tp.box ');
        dtmDados.IQuery1.sql.Add('from tb_conhecimento ct left join dashboard.skf_tb_trip tp on ct.nr_conhecimento = tp.cte ');
        dtmDados.IQuery1.sql.Add('where ct.nr_manifesto = :0 and serie_man = :1 and filial_man = :2)');
        dtmDados.IQuery1.sql.Add('and fl_empresa = :3');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.Parameters[3].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        // limpa dados do conhecimento
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update tb_conhecimento set nr_romaneio = null, ');
        dtmDados.IQuery1.sql.Add
          ('nr_manifesto = null, serie_man = null, filial_man = null, ');
      dtmDados.IQuery1.sql.Add
        ('dt_prevista = null ');
        dtmDados.IQuery1.sql.Add('where nr_manifesto = :0 ');
        dtmDados.IQuery1.sql.Add('and serie_man = :1 ');
        dtmDados.IQuery1.sql.Add('and filial_man = :2 ');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        dtmDados.IQuery1.close;
        // ShowMessage('Manifesto Cancelado');
      end;
    end;
  end
  else
  begin
    Panel10.Visible := false;
    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add('update tb_manifesto set status = ''C'', ');
    dtmDados.IQuery1.sql.Add('MDFE_MOT_CANC = :0, MDFE_CAN_DATA = sysdate, ');
    dtmDados.IQuery1.sql.Add('USER_CANCELADO = :1 ');
    dtmDados.IQuery1.sql.Add
      ('Where manifesto = :2 and serie = :3 and filial = :4 ');
    dtmDados.IQuery1.Parameters[0].value := EdMotivo.Text;
    dtmDados.IQuery1.Parameters[1].value := GLBUser;
    dtmDados.IQuery1.Parameters[2].value := QManifestoMANIFESTO.AsInteger;
    dtmDados.IQuery1.Parameters[3].value := QManifestoSERIE.AsString;
    dtmDados.IQuery1.Parameters[4].value := QManifestoFILIAL.AsInteger;
    dtmDados.IQuery1.ExecSQL;

    {dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.Clear;
    dtmDados.IQuery1.sql.Add('update tb_tsil set dt_alt_mdfe = sysdate Where mdfe = :0 ');
    dtmDados.IQuery1.Parameters[0].value := QManifestoID_MAN.AsInteger;
    dtmDados.IQuery1.ExecSQL;}

    dtmDados.IQuery1.close;
    if QManifestoTRANSFERIDO.IsNull then
    begin
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add('update tb_romaneio set nr_manifesto = null ');
      dtmDados.IQuery1.sql.Add('where nr_manifesto = :0 and fl_empresa = :1 ');
      dtmDados.IQuery1.sql.Add('and serie_manifesto = ''U'' ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;

      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add('delete from tb_roadnet where id_roadnet in (select distinct nr_romaneio from tb_romaneio where nr_manifesto = :0 and fl_empresa = :1 and serie_manifesto = :2) ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := glbfilial;
      dtmDados.IQuery1.Parameters[2].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;

      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('update cyber.rodcon set codman = '''', serman = '''', ');
      dtmDados.IQuery1.sql.Add('filman = '''', agecar = '''' ');
      dtmDados.IQuery1.sql.Add('where codman = :0 ');
      dtmDados.IQuery1.sql.Add('and serman = :1 ');
      dtmDados.IQuery1.sql.Add('and filman = :2 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('update cyber.rodord set codman = '''', serman = '''', ');
      dtmDados.IQuery1.sql.Add('filman = '''', agecar = '''' ');
      dtmDados.IQuery1.sql.Add('where codman = :0 ');
      dtmDados.IQuery1.sql.Add('and serman = :1 ');
      dtmDados.IQuery1.sql.Add('and filman = :2 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.ExecSQL;
        // limpa volumes da trip
        dtmDados.IQuery1.close;
        dtmDados.IQuery1.sql.Clear;
        dtmDados.IQuery1.sql.Add
          ('update dashboard.skf_tb_trip set user_exp = null, data_exp = null,data_fim_exp = null ');
        dtmDados.IQuery1.sql.Add('where box in (select distinct tp.box ');
        dtmDados.IQuery1.sql.Add('from tb_conhecimento ct left join dashboard.skf_tb_trip tp on ct.nr_conhecimento = tp.cte ');
        dtmDados.IQuery1.sql.Add('where ct.nr_manifesto = :0 and serie_man = :1 and filial_man = :2)');
        dtmDados.IQuery1.sql.Add('and fl_empresa = :3');
        dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
        dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
        dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.Parameters[3].value := QManifestoFILIAL.AsInteger;
        dtmDados.IQuery1.ExecSQL;
        // limpa dados do conhecimento
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('update tb_conhecimento set nr_romaneio = null, ');
      dtmDados.IQuery1.sql.Add
        ('nr_manifesto = null, serie_man = null, filial_man = null, ');
      dtmDados.IQuery1.sql.Add
        ('dt_prevista = null ');
      dtmDados.IQuery1.sql.Add('where nr_manifesto = :0 ');
      dtmDados.IQuery1.sql.Add('and serie_man = :1 ');
      dtmDados.IQuery1.sql.Add('and filial_man = :2 ');
      dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
      dtmDados.IQuery1.Parameters[2].value := QManifestoFILIAL.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
      ShowMessage('Manifesto Cancelado');
    end;
  end;
end;

procedure TfrmConsManifesto.btnSefazClick(Sender: TObject);
var
  scaminho, sdata, sNota: string;
begin
  if QManifestoSTATUS.value <> 'C' then
  begin
    if QManifestoSERIE.value <> 'U' then
    begin
      if (QManifestoMDFE_PROTOCOLO.IsNull) and (QManifestoFL_CONTINGENCIA.IsNull) then
      begin
        ShowMessage('Erro na Transmiss�o � necess�rio o Envio Novamente !');
        Exit;
      end;

      sdata := formatdatetime('YYYY', QManifestoDATAINC.value) +
        formatdatetime('MM', QManifestoDATAINC.value);
      scaminho := glbPDF + '\' + sdata + '\Mdfe\';
      sNota := QManifestoMDFE_CHAVE.AsString + '-mdfe.xml';

      // arquivo := frmMenu.ACBrMDFe1.DAMDFe.PathPDF + QManifestoMDFE_CHAVE.AsString + '-mdfe.xml';
      frmMenu.ACBrMDFe1.Manifestos.Clear;
      frmMenu.ACBrMDFe1.Manifestos.LoadFromFile(scaminho + sNota);
      frmMenu.ACBrMDFe1.Manifestos.Imprimir;
    end;
  end;
end;

procedure TfrmConsManifesto.Button1Click(Sender: TObject);
begin
  QKM.close;
  QKM.Parameters[0].Value := QManifestoMANIFESTO.AsInteger;
  QKM.Parameters[1].Value := QManifestoFILIAL.AsInteger;
  QKM.Parameters[2].Value := QManifestoSERIE.AsString;
  QKM.Open;
  calculakm;
  button1.Caption := floattostr(km);
end;

procedure TfrmConsManifesto.dtInicialEnter(Sender: TObject);
begin
  edMan.value := 0;
  edCte.value := 0;
  edMapa.value := 0;
  edPlaca.Clear;
end;

procedure TfrmConsManifesto.edCteEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
  edMan.value := 0;
  edMapa.value := 0;
  edPlaca.Clear;
end;

procedure TfrmConsManifesto.edManEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
  edCte.value := 0;
  edMapa.value := 0;
  edPlaca.Clear;
end;

procedure TfrmConsManifesto.edMapaEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
  edMan.value := 0;
  edCte.value := 0;
  edPlaca.Clear;
end;

procedure TfrmConsManifesto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QManifesto.close;
end;

procedure TfrmConsManifesto.FormCreate(Sender: TObject);
begin
  if GLBUser = 'PCORTEZ' then
    BitBtn3.Visible := true
  else
    BitBtn3.Visible := false;
end;

procedure TfrmConsManifesto.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QManifestoSTATUS.value = 'C' then
  begin
    JvDBGrid1.canvas.Brush.Color := clRed;
    JvDBGrid1.canvas.Font.Color := clWhite;
  end;
  if QManifestoFL_CONTINGENCIA.value = 'S' then
  begin
    JvDBGrid1.canvas.Brush.Color := clYellow;
    JvDBGrid1.canvas.Font.Color := clBlack;
  end;
  JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol].field, State);
end;

procedure TfrmConsManifesto.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QManifesto.locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmConsManifesto.JvDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QItensAVERBA_PROTOCOLO.IsNull) and (not QManifestoDATA_S.IsNull) and
    (QItensDOC.value = 'CTE') then
  begin
    JvDBGrid2.canvas.Brush.Color := clYellow;
    JvDBGrid2.canvas.Font.Color := clBlack;
  end;
  JvDBGrid2.DefaultDrawDataCell(Rect, JvDBGrid2.Columns[DataCol].field, State);
end;

procedure TfrmConsManifesto.JvDBMaskEdit1Click(Sender: TObject);
var
  protocolo, data, hora, sdata, scaminho, rec: String;
  inic, fim, a: Integer;
  erro: Retorno;
  datahora : TDatetime;
  resultado : SuccessProcesso;
begin
  if (QItensAVERBA_PROTOCOLO.IsNull) and (not QManifestoDATA_S.IsNull) and
    (QItensDOC.value = 'CTE') then
  begin
    QAverba.close;
    QAverba.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    QAverba.Parameters[1].value := glbfilial;
    QAverba.Parameters[2].value := QManifestoSERIE.value;
    QAverba.open;
    while not QAverba.Eof do
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.Clear;
      dtmDados.IQuery1.sql.Add
        ('select COALESCE(user_ws,'''') user_ws, pass_ws, code_ws from tb_seguradora ');
      dtmDados.IQuery1.sql.Add('where fl_empresa = :0 and trunc(datafim) >= trunc(sysdate)');
      dtmDados.IQuery1.Parameters[0].value := QAverbaFL_EMPRESA.AsInteger;
      dtmDados.IQuery1.open;
      if dtmDados.IQuery1.fieldbyname('user_ws').value <> '' then
      begin

        rec := QAverbaCTE_CHAVE.AsString + '-cte.xml';;
        sdata := formatdatetime('YYYY', QAverbaDT_CONHECIMENTO.value) +
          formatdatetime('MM', QAverbaDT_CONHECIMENTO.value);
        scaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sdata
          + '\CTe\';

        frmMenu.ACBrCTe1.Conhecimentos.Clear;
        frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(scaminho + rec);
        frmMenu.ACBrCTe1.Consultar;

        erro := ATM.GetATMWebSvrPortType.averbaCTe
          (dtmDados.IQuery1.fieldbyname('user_ws').value,
          dtmDados.IQuery1.fieldbyname('pass_ws').value,
          dtmDados.IQuery1.fieldbyname('code_ws').value,
          frmMenu.ACBrCTe1.Conhecimentos.Items[0].XML);
          protocolo := resultado.protocolo;
          datahora := (resultado.dhAverbacao.AsDateTime);
        //resultado := StringReplace(resultado, #$D#$A#9, '', [rfReplaceAll]);
        //resultado := StringReplace(resultado, #9, '', [rfReplaceAll]);
        if protocolo <> '' then
        begin
          //inic := Pos('<PROTOCOLONUMERO>', resultado) + 17;
          //fim := Pos('</PROTOCOLONUMERO>', resultado);
          //a := fim - inic;
          //protocolo := copy(resultado, inic, a);

          {inic := Pos('<DATA>', resultado) + 6;
          fim := Pos('</DATA>', resultado);
          a := fim - inic;
          data := copy(resultado, inic, a);

          inic := Pos('<HORA>', resultado) + 6;
          fim := Pos('</HORA>', resultado);
          a := fim - inic;
          hora := copy(resultado, inic, a);
          data := buscatroca(data, '-', '/');   }

          frmMenu.prc_averba.Parameters[0].value := protocolo;
          frmMenu.prc_averba.Parameters[1].value := datahora;
          //  StrToDateTime(data + ' ' + hora);
          frmMenu.prc_averba.Parameters[2].value :=
            QAverbaCOD_CONHECIMENTO.value;
          frmMenu.prc_averba.ExecProc;
          JvDBMaskEdit1.Color := clWhite;
          JvDBDateEdit1.Color := clWhite;
        end;
      end;
      QAverba.Next;
    end;
    QItens.close;
    QItens.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
    QItens.Parameters[1].value := QManifestoSERIE.AsString;
    QItens.Parameters[2].value := glbfilial;
    QItens.open;
  end;
end;

procedure TfrmConsManifesto.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    Memo1.Clear;
    Memo1.Visible := false;
  end;
end;

procedure TfrmConsManifesto.QItensAfterScroll(DataSet: TDataSet);
begin
  QCtrc.close;
  QCtrc.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  QCtrc.Parameters[1].value := glbfilial;
  QCtrc.Parameters[2].value := QManifestoSERIE.AsString;
  QCtrc.Parameters[3].value := QItensNR_CTRC.AsInteger;
  QCtrc.open;
  if (QItensAVERBA_PROTOCOLO.IsNull) and (not QManifestoDATA_S.IsNull) and
    (QItensDOC.value = 'CTE') then
  begin
    JvDBMaskEdit1.Color := clYellow;
    JvDBDateEdit1.Color := clYellow;
  end
  else
  begin
    JvDBMaskEdit1.Color := clWhite;
    JvDBDateEdit1.Color := clWhite;
  end
end;

procedure TfrmConsManifesto.QManifestoAfterScroll(DataSet: TDataSet);
begin
  QItens.close;
  QItens.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  QItens.Parameters[1].value := QManifestoSERIE.AsString;
  QItens.Parameters[2].value := glbfilial;
  QItens.open;

  if (QManifestoSERIE.value = 'U') and (QManifestoPARAFILIAL.value = 'S') then
  begin
    btnfintran.Visible := true;
    btnEncerrar.Visible := false;
  end
  else
  begin
    btnEncerrar.Visible := true;
    btnfintran.Visible := false;
  end;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select distinct nvl(sum(T.vlrmer),0) nr_valor from vw_manifesto_detalhe t ');
  dtmDados.IQuery1.sql.Add
    ('where t.nr_manifesto = :0 and t.serie = :1 and t.fl_empresa = :2 ');
  dtmDados.IQuery1.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := QManifestoSERIE.AsString;
  dtmDados.IQuery1.Parameters[2].value := glbfilial;
  dtmDados.IQuery1.open;

  lblv.Caption := 'Valor : ' + FormatFloat('###,##0.00',
    dtmDados.IQuery1.fieldbyname('nr_valor').value);

  // mostra quem fez libera��o de mapa
  QLiberaMapa.close;
  QLiberaMapa.Parameters[0].value := QManifestoMANIFESTO.AsInteger;
  QLiberaMapa.Parameters[1].value := QManifestoSERIE.AsString;
  QLiberaMapa.Parameters[2].value := glbfilial;
  QLiberaMapa.open;
  RELiberado.Clear ;
  while not QLiberaMapa.Eof do
  begin
    RELiberado.Lines.Add(QLiberaMapaLIBERADO.AsString);
    QLiberaMapa.next;
  end;
  QLiberaMapa.close;
end;

procedure TfrmConsManifesto.QManifestoBeforeOpen(DataSet: TDataSet);
begin
  QManifesto.Parameters[0].value := glbfilial;
end;

procedure TfrmConsManifesto.qMapaAfterScroll(DataSet: TDataSet);
begin
  QVolume.close;
  QVolume.Parameters[0].value := qMapaCOD_TA.AsInteger;
  QVolume.Parameters[1].value := qMapaCODFIL.AsInteger;
  QVolume.open;
end;

{procedure TfrmConsManifesto.RLBand14BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  volume: Integer;
begin
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select distinct nvl(codbar,cod_barras_int) codbar from cyber.itc_man_emb where romaneio = :0');
  dtmDados.IQuery1.Parameters[0].value := qMapaNR_ROMANEIO.AsInteger;
  dtmDados.IQuery1.open;
  volume := dtmDados.IQuery1.RecordCount;
  RLLabel14.Caption := 'Total Volumes : ' + IntToStr(volume);
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select sum(peso) pescal, sum(qtd) volume, notfis nota, agecar from ');
  dtmDados.IQuery1.sql.Add
    ('(select distinct sum(n.pescub) cub, sum(n.pesokg) peso, sum(n.quanti) qtd, ');
  dtmDados.IQuery1.sql.Add('  count(n.notfis) notfis, c.agecar ');
  dtmDados.IQuery1.sql.Add
    ('  from cyber.rodnfc n left join cyber.rodcon c on (c.codcon = n.codcon and c.codfil = n.codfil) ');
  dtmDados.IQuery1.sql.Add('  group by agecar ');
  dtmDados.IQuery1.sql.Add('union all ');
  dtmDados.IQuery1.sql.Add
    ('select sum(n.pescub) cub, sum(n.pesokg) peso, sum(n.quanti) qtd, ');
  dtmDados.IQuery1.sql.Add('   count(n.notfis) notfis, c.agecar ');
  dtmDados.IQuery1.sql.Add
    ('  from cyber.rodios n left join cyber.rodord c on n.codord = c.codigo and n.filord = c.codfil ');
  dtmDados.IQuery1.sql.Add('     group by c.agecar) ');
  dtmDados.IQuery1.sql.Add('where agecar = :0  group by notfis, agecar');
  dtmDados.IQuery1.Parameters[0].value := qMapaNR_ROMANEIO.AsInteger;
  dtmDados.IQuery1.open;
  if volume = 0 then
    RLLabel14.Caption := 'Total Volumes : ' + dtmDados.IQuery1.fieldbyname
      ('volume').AsString;
  RLPeso.Caption := FormatFloat('#,##0.00',
    dtmDados.IQuery1.fieldbyname('pescal').value);
  RLLabel16.Caption := 'Total Notas : ' + dtmDados.IQuery1.fieldbyname
    ('nota').AsString;
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select doc, agecar from (select distinct count(c.codcon) doc, c.agecar ');
  dtmDados.IQuery1.sql.Add
    ('from cyber.rodcon c group by agecar union all select distinct count(c.codigo) doc, c.agecar ');
  dtmDados.IQuery1.sql.Add
    ('from cyber.rodord c group by c.agecar) where agecar = :0 ');
  dtmDados.IQuery1.Parameters[0].value := qMapaNR_ROMANEIO.AsInteger;
  dtmDados.IQuery1.open;
  RLLabel12.Caption := 'Total CTe/OST : ' + dtmDados.IQuery1.fieldbyname
    ('doc').AsString;
  RLLabel13.Caption := 'Ve�culo : ' + qMapaVEICULO.AsString;
  dtmDados.IQuery1.close;
end;

procedure TfrmConsManifesto.RLBand5BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select razsoc from cyber.rodfil where codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := glbfilial;
  dtmDados.IQuery1.open;
  RLLabel10.Caption := 'ROMANEIO DE CARGA - N� ' + IntToStr(glbfilial) + '-' +
    RetBran(mdManifestoManifesto.AsString, 6);
  RLLabel21.Caption := dtmDados.IQuery1.fieldbyname('razsoc').value;

  RLLabel27.Caption := 'Origem da Viagem : ' + mdManifestoestado.AsString;
  RLLabel28.Caption := 'Destino da Viagem : ' + qMapaCargaUF_DESTINO.AsString;
  RLLabel46.Caption := 'Tipo - ' + mdManifestooperacao.AsString;
  if copy(dtmDados.IQuery1.fieldbyname('razsoc').value, 1, 7) = 'INTECOM' then
  begin
    RLLogoInt.Visible := true;
    RLLogoInt.Enabled := true;
  end
  else
  begin
    RLLogoIw.Visible := true;
    RLLogoIw.Enabled := true;
  end;

  dtmDados.IQuery1.close;
end;

procedure TfrmConsManifesto.RLBand6BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if Length(mdManifestonota.value) > 20 then
    RLBand6.Height := 32;
end;

procedure TfrmConsManifesto.RLBand7BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel37.Caption := 'Quant. Documentos : ' +
    IntToStr(QMapaItens.RecordCount);
  RLRichText1.Lines.Add(mdManifestoobs.AsString);
end;

procedure TfrmConsManifesto.RLBand9BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel1.Caption := glbnmfilial;
end;
       }
procedure TfrmConsManifesto.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a, b: String;
  Met, i, x, j, k: Integer;
  Sec: TTime;
  t, klm: double;
  orig1, dest1: String;
begin
  x := 0;
  j := 0;
  t := 0;
  // pegar o cep da filial como ponto de partida
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add
    ('select (latitu ||''+''|| longit) cep from cyber.rodfil where codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := glbfilial;
  dtmDados.IQuery1.open;
  cep := dtmDados.IQuery1.fieldbyname('cep').value;
  dtmDados.IQuery1.close;

  if QKM.RecordCount > 0 then
  begin
    while not QKM.Eof do
    begin
      if x = 0 then
      begin
        orig := cep;
        orig1 := orig;
        if ApCarac(QKMGEO.value) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if ApCarac(QKMGEO.value) <> '' then
          dest := QKMGEO.AsString
        else
          dest := ApCarac(QKMCODCEP.AsString) + '+' + QKMDESCRI.AsString;
        dest1 := dest;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest; // ApCarac(QKMCODCEP.AsString)+'+'+QKMDESCRI.AsString;
      end;

      XMLFileName := 'C:\sim\temp.xml';

      URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

      XMLDoc.FileName := 'C:\sim\temp.xml';
      XMLDoc.Active := true;
      i := 0;
      j := 0;
      k := 0;
      for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
        if a = 'row' then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
              .ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName = 'distance' then
              begin
                // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
                // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

                km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].Text) + km;
                // edDistancia.Text := FloatToStr(t);

              end;
              b := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                ['status'].Text;
            end;
          end;
        end;
      end;
      // Memo1.Lines.Add(edit1.Text);
      // Memo1.Lines.Add('-----------------');
      XMLDoc.Active := false;
      x := x + 1;
      QKM.Next;
    end;
    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
    XMLFileName := 'C:\sim\temp.xml';
    URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

    XMLDoc.FileName := 'C:\sim\temp.xml';
    XMLDoc.Active := true;
    i := 0;
    j := 0;
    k := 0;
    for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
    begin
      a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
      if a = 'row' then
      begin
        for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
          for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].NodeName;
            if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k]
              .NodeName = 'distance' then
            begin
              // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
              // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

              km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
                .ChildNodes[k].ChildNodes['text'].Text) + km;
              // edDistancia.Text := FloatToStr(t);

            end;
            b := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              ['status'].Text;
          end;
        end;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

procedure TfrmConsManifesto.gravageo;
var
  XMLUrl, cep, orig, dest, XMLFileName, a: String;
  i, j, k: Integer;
  t: double;
begin
  dest := ApCarac(QKMDESCRI.AsString);
  XMLUrl := 'https://maps.googleapis.com/maps/api/geocode/xml?address=' +
    QKMDESCRI.value + '&postal_code:' + QKMCODCEP.AsString +
    '&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
  XMLFileName := 'C:\sim\geo.xml';

  URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

  XMLDoc.FileName := 'C:\sim\geo.xml';
  XMLDoc.Active := true;
  i := 0;
  j := 0;
  k := 0;
  for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
  begin
    a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
    if a = 'result' then
    begin
      for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
        .ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
        for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
            [k].NodeName;
          if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k]
            .NodeName = 'location' then
          begin
            lat := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].ChildNodes['lat'].Text;
            lon := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].ChildNodes['lng'].Text;
          end;
        end;
      end;
    end;
  end;
  XMLDoc.Active := false;
end;

end.
