unit RelatorioMeritor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ShellAPI, Gauges, DB, ADODB, ExtCtrls,
  FileCtrl, Buttons, Mask, JvExMask, JvToolEdit;

type
  TfrmRelatorioMeritor = class(TForm)
    DataSource1: TDataSource;
    BitBtn1: TBitBtn;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Label1: TLabel;
    QCtrc: TADOQuery;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    QCtrcDATA: TDateTimeField;
    QCtrcPEDIDO_WMS: TStringField;
    QCtrcORDER_: TStringField;
    QCtrcITEM: TStringField;
    QCtrcPRODUTO: TStringField;
    QCtrcQT: TBCDField;
    QCtrcDELIVERY: TStringField;
    QCtrcSTAT: TStringField;
    QCtrcDESTINO: TStringField;
    QCtrcFANTASIA: TStringField;
    QCtrcID_DETAIL: TStringField;
    QCtrcNF: TStringField;
    QCtrcTOTAL: TBCDField;
    btnExcel: TBitBtn;
    Gauge1: TGauge;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  frmRelatorioMeritor: TfrmRelatorioMeritor;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmRelatorioMeritor.BitBtn1Click(Sender: TObject);
begin
  Panel1.Visible := true;
  Application.ProcessMessages;
  Panel1.Visible := true;
  QCtrc.close;
  QCtrc.sql[19] := ' Where data between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmRelatorioMeritor.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  sarquivo := 'RelMeritor.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to DBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + DBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to DBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(DBGrid1.Columns[i]
        .FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

end.
