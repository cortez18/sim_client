unit CadPercurso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, ADODB, DB, ActnList,
  JvToolEdit, JvBaseEdits, JvExStdCtrls, JvCombobox, Mask, JvExMask, JvMaskEdit,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, System.Actions;

type
  TfrmCadPercurso = class(TForm)
    DataSource1: TDataSource;
    DataSource3: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    btnDupl: TBitBtn;
    QTabelaID: TBCDField;
    QTabelaUFO: TStringField;
    QTabelaUFD: TStringField;
    QTabelaSTATUS: TStringField;
    QTabelaDATINC: TDateTimeField;
    QTabelaUSERINC: TStringField;
    QItemID: TBCDField;
    QItemID_PERCURSO: TBCDField;
    QItemUF: TStringField;
    QItemORDEM: TBCDField;
    Panel2: TPanel;
    PnP: TPanel;
    UFO: TJvComboBox;
    UFD: TJvComboBox;
    dbgContrato: TJvDBUltimGrid;
    Panel1: TPanel;
    btnInserirI: TBitBtn;
    btnsalvarI: TBitBtn;
    btnAlterarI: TBitBtn;
    btnExcluirI: TBitBtn;
    DBGrid1: TJvDBUltimGrid;
    btnCancelarI: TBitBtn;
    Panel3: TPanel;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnFechar: TBitBtn;
    pnpi: TPanel;
    edOrdem: TJvCalcEdit;
    UFP: TJvComboBox;

    procedure btnFecharClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure btnExcluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure Restaura;
    procedure DetalhesShow(Sender: TObject);
    procedure RestauraI;
    procedure btnInserirIClick(Sender: TObject);
    procedure btnsalvarIClick(Sender: TObject);
    procedure btnAlterarIClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure Limpadados;
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure btnExcluirIClick(Sender: TObject);
    procedure btnCancelarIClick(Sender: TObject);
  private
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadPercurso: TfrmCadPercurso;

implementation

uses Dados, funcoes, menu;

{$R *.DFM}

procedure TfrmCadPercurso.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadPercurso.btnNovoClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  Limpadados;
  tipo := 'I';
  PnP.Visible := true;
  UFO.SetFocus;
end;

procedure TfrmCadPercurso.btnSalvarClick(Sender: TObject);
var
  col: integer;
begin
  col := QTabelaID.AsInteger;
  if UFO.Text = '' then
  begin
    ShowMessage('A UF de origem n�o foi cadastrado !');
    UFO.SetFocus;
    Exit;
  end;

  if UFD.Text = '' then
  begin
    ShowMessage('A UF de destino n�o foi cadastrado !');
    UFD.SetFocus;
    Exit;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select distinct t.* ');
  dtmDados.iQuery1.SQL.add('from tb_percurso t ');
  dtmDados.iQuery1.SQL.add('where t.UFo = :0');
  dtmDados.iQuery1.SQL.add('and t.UFd = :1');
  dtmDados.iQuery1.Parameters[0].Value := UFO.Text;
  dtmDados.iQuery1.Parameters[1].Value := UFD.Text;
  dtmDados.iQuery1.Open;
  if not dtmDados.iQuery1.FieldByName('id').IsNull then
  begin
    ShowMessage('Este Percurso j� est� cadastrado !!');
    btnCancelarClick(Sender);
    QTabela.Locate('id', dtmDados.iQuery1.FieldByName('id').Value, []);
  end;
  dtmDados.iQuery1.Close;

  try
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add
        ('insert into tb_percurso (ID, UFO, UFD, STATUS, USERINC) ');
      dtmDados.iQuery1.SQL.add
        ('values ((select max(id)+1 from tb_percurso), :0, :1, ''A'', :2 )');
      dtmDados.iQuery1.Parameters[0].Value := UFO.Text;
      dtmDados.iQuery1.Parameters[1].Value := UFD.Text;
      dtmDados.iQuery1.Parameters[2].Value := GLBUSER;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add('select max(id) id from tb_percurso');
      dtmDados.iQuery1.Open;
      col := dtmDados.iQuery1.FieldByName('id').Value;
    end
    else
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.SQL.Clear;
      dtmDados.iQuery1.SQL.add('update tb_percurso set UFO = :0, UFD = :1 ');
      dtmDados.iQuery1.SQL.add('where id = :3');
      dtmDados.iQuery1.Parameters[0].Value := UFO.Text;
      dtmDados.iQuery1.Parameters[1].Value := UFD.Text;
      dtmDados.iQuery1.Parameters[2].Value := QTabelaID.AsInteger;
      dtmDados.iQuery1.ExecSQL;
      dtmDados.iQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  tipo := '';
  Restaura;
  QTabela.Close;
  QTabela.Open;
  QTabela.Locate('id', col, []);
  PnP.Visible := false;
end;

procedure TfrmCadPercurso.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  tipo := '';
  if QItem.Active = false then
  begin
    QItem.Close;
    QItem.Parameters[0].Value := QTabelaID.Value;
    QItem.Open;
  end;
  PnP.Visible := false;
end;

procedure TfrmCadPercurso.btnCancelarIClick(Sender: TObject);
begin
  QItem.Cancel;
  RestauraI;
end;

procedure TfrmCadPercurso.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  PnP.Visible := true;

  UFO.SetFocus;
  tipo := 'A';

  UFO.ItemIndex := UFO.Items.IndexOf(QTabelaUFO.Value);
  UFD.ItemIndex := UFD.Items.IndexOf(QTabelaUFD.Value);
end;

procedure TfrmCadPercurso.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.SQL.Text) > 0 then
  begin
    if Column.FieldName = 'CODIGO' then
      QTabela.SQL.Text := Copy(QTabela.SQL.Text, 1,
        Pos('order by', QTabela.SQL.Text) - 1) + 'order by t.' +
        Column.FieldName
    else
      QTabela.SQL.Text := Copy(QTabela.SQL.Text, 1,
        Pos('order by', QTabela.SQL.Text) - 1) + 'order by ' + Column.FieldName
  end
  else
    QTabela.SQL.Text := QTabela.SQL.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadPercurso.btnExcluirClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Percurso?',
    'Apagar Percurso', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add
      ('delete from tb_percursoi where id_percurso = :cn');
    dtmDados.iQuery1.Parameters[0].Value := QTabelaID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_percurso where id = :cn');
    dtmDados.iQuery1.Parameters[0].Value := QTabelaID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QTabela.Close;
    QTabela.Open;
  end;
  dtmDados.Query1.Close;
end;

procedure TfrmCadPercurso.btnExcluirIClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item do Percurso?',
    'Apagar Item da Percurso', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    dtmDados.iQuery1.SQL.add('delete from tb_percursoi where id = :cn');
    dtmDados.iQuery1.Parameters[0].Value := QItemID.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QItem.Close;
    QItem.Open;
  end;
end;

procedure TfrmCadPercurso.FormCreate(Sender: TObject);
begin
  QTabela.Open;
  QItem.Open;
  QTabela.First;
end;

procedure TfrmCadPercurso.Limpadados;
begin
  UFO.ItemIndex := -1;
  UFD.ItemIndex := -1;
end;

procedure TfrmCadPercurso.QTabelaAfterScroll(DataSet: TDataSet);
begin
  QItem.Close;
  QItem.Parameters[0].Value := QTabelaID.Value;
  QItem.Open;
end;

procedure TfrmCadPercurso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QTabela.Close;
  QItem.Close;
end;

procedure TfrmCadPercurso.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadPercurso.Restaura;
begin
  btnNovo.Enabled := not btnNovo.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnExcluir.Enabled := not btnExcluir.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;
end;

procedure TfrmCadPercurso.DetalhesShow(Sender: TObject);
begin
  DBGrid1.top := 5;
  DBGrid1.Height := 308;
end;

procedure TfrmCadPercurso.RestauraI;
begin
  btnInserirI.Enabled := not btnInserirI.Enabled;
  btnAlterarI.Enabled := not btnAlterarI.Enabled;
  btnExcluirI.Enabled := not btnExcluirI.Enabled;
  btnsalvarI.Enabled := not btnsalvarI.Enabled;
  btnCancelarI.Enabled := not btnCancelarI.Enabled;
  if pnpi.Visible = true then
    pnpi.Visible := false
  else
    pnpi.Visible := true;
end;

procedure TfrmCadPercurso.btnInserirIClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  tipo := 'I';
  RestauraI;
  edOrdem.SetFocus;
  edOrdem.Value := 0;
  UFP.ItemIndex := -1;
end;

procedure TfrmCadPercurso.btnsalvarIClick(Sender: TObject);
begin
  if UFP.Text = '' then
  begin
    ShowMessage('A UF n�o foi cadastrado !');
    UFP.SetFocus;
    Exit;
  end;

  if edOrdem.Value = 0 then
  begin
    ShowMessage('A Ordem n�o foi cadastrado !');
    edOrdem.SetFocus;
    Exit;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.SQL.Clear;
  dtmDados.iQuery1.SQL.add('select distinct t.* ');
  dtmDados.iQuery1.SQL.add('from tb_percursoi t ');
  dtmDados.iQuery1.SQL.add('where t.UF = :0 and t.id_percurso = :1');
  dtmDados.iQuery1.Parameters[0].Value := UFP.Text;
  dtmDados.iQuery1.Parameters[1].Value := QTabelaID.AsInteger;
  dtmDados.iQuery1.Open;
  if not dtmDados.iQuery1.FieldByName('id').IsNull then
  begin
    ShowMessage('Este Percurso j� est� cadastrado !!');
    btnCancelarIClick(Sender);
    QItem.Locate('id', dtmDados.iQuery1.FieldByName('id').Value, []);
    Exit;
  end;
  dtmDados.iQuery1.Close;
  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.SQL.Clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.SQL.add
        ('insert into tb_percursoi (ID, UF, ORDEM, ID_PERCURSO)' + ' values ' +
        '((select max(id)+1 from tb_percursoi), :0, :1, :2)');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := UFP.Text;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := edOrdem.Value;
      dtmDados.iQuery1.Parameters.ParamByName('2').Value := QTabelaID.AsInteger;
    end
    else
    begin
      dtmDados.iQuery1.SQL.add('update tb_percursoi set ' +
        'UF = :0, Ordem = :1 where id = :2');
      dtmDados.iQuery1.Parameters.ParamByName('0').Value := UFP.Text;
      dtmDados.iQuery1.Parameters.ParamByName('1').Value := edOrdem.Value;
      dtmDados.iQuery1.Parameters.ParamByName('2').Value := QItemID.AsInteger;
    end;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QItem.Close;
    QItem.Parameters[0].Value := QTabelaID.Value;
    QItem.Open;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;

  tipo := '';
  RestauraI;
end;

procedure TfrmCadPercurso.btnAlterarIClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  tipo := 'A';
  RestauraI;
  edOrdem.Value := QItemORDEM.AsInteger;
  UFP.ItemIndex := UFP.Items.IndexOf(QItemUF.Value);
  edOrdem.SetFocus;
end;

procedure TfrmCadPercurso.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  Screen.Cursor := crDefault;
end;

end.
