object frmExportaRM: TfrmExportaRM
  Left = 0
  Top = 0
  Caption = 'Exporta'#231#227'o de CT-e'#180's emitidos - Layout RM'
  ClientHeight = 481
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 12
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 109
    Top = 4
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 12
    Top = 51
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 239
    Top = 449
    Width = 157
    Height = 28
    Progress = 0
  end
  object Label8: TLabel
    Left = 240
    Top = 39
    Width = 47
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 2
    Top = 444
    Width = 27
    Height = 13
    Caption = 'e-mail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object dtInicial: TJvDateEdit
    Left = 12
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 109
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 1
  end
  object btRelatorio: TBitBtn
    Left = 239
    Top = 3
    Width = 157
    Height = 32
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 79
    Top = 47
    Width = 145
    Height = 21
    LookupDisplay = 'nr_cnpj_cpf'
    LookupField = 'cod_cliente'
    LookupSource = DataSource1
    TabOrder = 3
    Text = '08.158.054/0002-72'
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 12
    Top = 70
    Width = 222
    Height = 21
    LookupDisplay = 'nm_cliente'
    LookupField = 'cod_cliente'
    LookupSource = DataSource1
    TabOrder = 4
    Text = 'MIRKA DO BRASIL'
    OnCloseUp = ledClienteCloseUp
    OnExit = ledClienteCloseUp
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 100
    Width = 224
    Height = 343
    DataSource = navnavig
    DrawingStyle = gdsClassic
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'NR_CONHECIMENTO'
        Title.Alignment = taCenter
        Title.Caption = 'CT-e'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 43
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DT_CONHECIMENTO'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLI_OS'
        Title.Alignment = taCenter
        Title.Caption = 'Cliente'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_EXP'
        Title.Alignment = taCenter
        Title.Caption = 'Expedidor'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_DES'
        Title.Alignment = taCenter
        Title.Caption = 'Destinat'#225'rio'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'VL_TOTAL_IMPRESSO'
        Title.Alignment = taCenter
        Title.Caption = 'Valor'
        Title.Color = clRed
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWhite
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 70
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = -26
    Top = 188
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 6
    Visible = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 239
    Top = 75
    Width = 157
    Height = 331
    TabOrder = 7
  end
  object DriveComboBox1: TDriveComboBox
    Left = 239
    Top = 55
    Width = 156
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 8
  end
  object Memo1: TMemo
    Left = 86
    Top = 282
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 10
    Visible = False
  end
  object btnExportar: TBitBtn
    Left = 239
    Top = 412
    Width = 157
    Height = 31
    Caption = 'Exportar'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFE2E2E2
      CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9518B555085
      54CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCFC5F9F65559E5C509856528D57CCCCCCFFFFFFFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8FAF864AB6B63AC6B88C99082C6
      8A529A58458349FAFCFAFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFB
      FBFB83C48A57AB616AB47390CE978ACB916AB0703A82416A9E6DFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFBFBFBFAFAFAFAFAFA54AB5E96D29F91CF
      99539F5BAABCACFEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFB
      FBFBFBFBFBFAFAFA5BB4659DD6A699D3A24B9E53C9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFB60BC6C5CB66757B0
      6152A85CC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFAFAFAF9F9F9F6F6F6F6F6F6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBF8F8F8F6F6F6F3F3F3F2F2
      F2FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFB
      FBFBF8F8F8F5F5F5F2F2F2EFEFEFEDEDEDFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFBFBFBFCFCFCFCFCFCFBFBFBF8F8F8F5F5F5F1F1F1ECECECEAEAEAE6E6
      E6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCF9F9F9F9F9F9F9F9F9F7F7F7F6
      F6F6F2F2F2EBEBEBFCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6F6F4F4
      F4C5C5C5DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5F5F5F1
      F1F1EFEFEFE9E9E9FCFCFCE7E7E7C3C3C3DFDFDFFDFDFDFFFFFFFFFFFFCCCCCC
      F8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2C2DFDF
      DFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9C9C9C9
      C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFFFFFFFF}
    TabOrder = 9
    OnClick = btnExportarClick
  end
  object edEmail: TEdit
    Left = 0
    Top = 459
    Width = 224
    Height = 21
    TabOrder = 11
    Text = 'crm10@intecomlogistica.com.br'
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select c.numerocte nr_conhecimento, c.emissaocte dt_conhecimento' +
        ', '#39'C'#39' fl_tipo, '#39#39' nr_print2, c.FRETE_PESO_XML vl_frete,'
      
        'c.AD_VALOREM_XML vl_ad_terrestre, c.OUTRAS_TAXAS_XML vl_outros, ' +
        'c.aliqimposto vl_aliquota, c.valorimmposto vl_imposto, c.PEDAGIO' +
        '_XML vl_pedagio,'
      
        'c.cfop, c.fl_empresa codfil, c.vltotprestacao vl_total_impresso,' +
        ' cl.razsoc nm_cli_os, cl.codcgc nr_cnpj_cli, c.cte_chave chavect' +
        'e,'
      
        'c.nr_serie, c.ufori ds_uf_origem, round((c.vltotprestacao * 0.01' +
        '65),2) pis, round((c.vltotprestacao * 0.0760),2) cofins,'
      
        'c.valorcarga VL_DECLARADO_NF, c.qtdmedida nr_qtde_vol, c.placa d' +
        's_placa, substr(c.codmunini,3,5) codmunini, c.ufini, substr(nvl(' +
        'c.codmunred, c.codmundes),3,5) codmundes, c.uffim'
      'from vw_cte c left join cyber.rodcli cl on cl.codcgc = c.cnpjtom'
      'where c.fl_empresa = 1'
      
        'and c.emissaocte between to_date('#39'19/04/16'#39','#39'dd/mm/yy'#39') and to_d' +
        'ate('#39'10/09/16'#39','#39'dd/mm/yy'#39')'
      ''
      ''
      ''
      'order by emissaocte')
    Left = 12
    Top = 192
    object QCtrcNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      ReadOnly = True
      Size = 50
    end
    object QCtrcNR_CONHECIMENTO: TBCDField
      FieldName = 'NR_CONHECIMENTO'
      ReadOnly = True
      Precision = 18
      Size = 0
    end
    object QCtrcVL_TOTAL_IMPRESSO: TBCDField
      FieldName = 'VL_TOTAL_IMPRESSO'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 9
      Size = 2
    end
    object QCtrcDT_CONHECIMENTO: TDateTimeField
      FieldName = 'DT_CONHECIMENTO'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QCtrcFL_TIPO: TStringField
      FieldName = 'FL_TIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QCtrcVL_DECLARADO_NF: TBCDField
      FieldName = 'VL_DECLARADO_NF'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_FRETE: TBCDField
      FieldName = 'VL_FRETE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_AD_TERRESTRE: TBCDField
      FieldName = 'VL_AD_TERRESTRE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_ALIQUOTA: TFloatField
      FieldName = 'VL_ALIQUOTA'
      ReadOnly = True
    end
    object QCtrcVL_IMPOSTO: TFloatField
      FieldName = 'VL_IMPOSTO'
      ReadOnly = True
    end
    object QCtrcVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNR_CNPJ_CLI: TStringField
      FieldName = 'NR_CNPJ_CLI'
      ReadOnly = True
      Size = 18
    end
    object QCtrcNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      Size = 5
    end
    object QCtrcDS_PLACA: TStringField
      FieldName = 'DS_PLACA'
      Size = 10
    end
    object QCtrcNR_QTDE_VOL: TBCDField
      FieldName = 'NR_QTDE_VOL'
      Precision = 32
    end
    object QCtrcDS_UF_ORIGEM: TStringField
      FieldName = 'DS_UF_ORIGEM'
      FixedChar = True
      Size = 2
    end
    object QCtrcCHAVECTE: TStringField
      FieldName = 'CHAVECTE'
      FixedChar = True
      Size = 44
    end
    object QCtrcPIS: TBCDField
      FieldName = 'PIS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcCOFINS: TBCDField
      FieldName = 'COFINS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcCODMUNINI: TStringField
      FieldName = 'CODMUNINI'
      Size = 10
    end
    object QCtrcUFINI: TStringField
      FieldName = 'UFINI'
      Size = 2
    end
    object QCtrcCODMUNDES: TStringField
      FieldName = 'CODMUNDES'
      Size = 10
    end
    object QCtrcUFFIM: TStringField
      FieldName = 'UFFIM'
      Size = 2
    end
  end
  object navnavig: TDataSource
    DataSet = QCtrc
    Left = 64
    Top = 196
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct c.codclifor cod_cliente, c.razsoc nm_cliente, c.' +
        'codcgc nr_cnpj_cpf '
      'from rodcli c left join rodcon P on p.codpag = c.codclifor '
      'where p.situac = '#39'E'#39' and p.codfil = 1  order by nm_cliente')
    Left = 12
    Top = 128
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 68
    Top = 128
  end
  object ACBrMail1: TACBrMail
    Host = 'outlook.office365.com'
    Port = '587'
    Username = 'mensageiro@intecomlogistica.com.br'
    Password = 'M3n$@geir0=123'
    SetSSL = False
    SetTLS = True
    Attempts = 3
    From = 'mensageiro@intecomlogistica.com.br'
    FromName = 'EDI Intecom'
    Subject = 'Interface RM '
    DefaultCharset = UTF_8
    IDECharset = CP1252
    Left = 140
    Top = 244
  end
end
