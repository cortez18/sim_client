object frmEDIGMD: TfrmEDIGMD
  Left = 0
  Top = 0
  Caption = 'EDI para GFLLOG'
  ClientHeight = 372
  ClientWidth = 980
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Label1'
    Visible = False
  end
  object Label14: TLabel
    Left = 224
    Top = 4
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label10: TLabel
    Left = 12
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 109
    Top = 4
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DriveComboBox1: TDriveComboBox
    Left = 521
    Top = 49
    Width = 157
    Height = 19
    DirList = DirectoryListBox1
    TabOrder = 2
    Visible = False
  end
  object BitBtn1: TBitBtn
    Left = 693
    Top = 8
    Width = 75
    Height = 31
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0041924E003D8F49003A8C44003689400032873C002F84
      37002C813300287F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF004999580045965300419950007DC28F0096D0A60096CFA60078BE
      8900368D42002C813400297F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00519F61004D9C5D0064B47800A8DBB50087CC980066BC7D0064BA7C0086CB
      9800A5D9B40058AA6B002C813400297F3000FF00FF00FF00FF00FF00FF0059A6
      6B0056A366006AB97D00A8DBB20060BC77005CBA730059B8700059B56F0058B5
      6F005BB77400A5D9B3005AAA6C002C823400297F3000FF00FF00FF00FF005DA9
      700053AB6800AADDB40064C179005FBE710060BC7700FFFFFF00FFFFFF0059B8
      700058B56E005CB77400A6DAB400388F43002C823400FF00FF00FF00FF0061AC
      75008ACC980089D396006BC67A0063C1700055AB6500FFFFFF00FFFFFF0059B8
      700059B870005BB9720085CC97007BBE8D0030853900FF00FF00FF00FF0065AF
      7A00A9DDB3007DCF8A0075CC8100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700067BE7D009CD4AB0034883D00FF00FF00FF00FF0069B2
      7E00B6E2BE008BD597007AC98600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700069C17E009DD4AA00388B4200FF00FF00FF00FF006DB5
      8300ACDDB600A6DFAF0081CB8C007CC986006EBD7900FFFFFF00FFFFFF005BAC
      6A0060BC77005CBA73008BD1990080C592003C8E4700FF00FF00FF00FF0070B8
      870085C79700D2EED70095D9A0008AD394007FC88900FFFFFF00FFFFFF0079CD
      85006BC37C006FC77E00ACDFB500459E570040914C00FF00FF00FF00FF0073BA
      8A0070B88700AADAB700D8F1DC0092D89D0088CD930084CC8E008BD496008AD4
      950083D28E00AFE0B7006BB97D004898560044945100FF00FF00FF00FF00FF00
      FF0073BB8B0070B88700AFDCBB00DCF2E000B6E4BD009BDBA50096D9A000A5DF
      AF00C0E8C50079C28A00509E5F004C9B5B00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0073BB8B0071B8870094CEA400C3E6CB00CFEBD400C9E9CE00AFDD
      B8006DB97F0058A5690054A16500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0074BB8B0071B988006EB684006AB3800067B17C0063AE
      770060AB73005CA86E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 521
    Top = 68
    Width = 157
    Height = 270
    TabOrder = 1
    Visible = False
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 220
    Top = 20
    Width = 169
    Height = 21
    LookupDisplay = 'NR_CNPJ_CPF'
    LookupField = 'NR_CNPJ_CPF'
    LookupSource = dtsCliente
    CharCase = ecUpperCase
    TabOrder = 3
    Text = ''
    Visible = False
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 395
    Top = 20
    Width = 280
    Height = 21
    LookupDisplay = 'nm_cliente'
    LookupField = 'NR_CNPJ_CPF'
    LookupSource = dtsCliente
    CharCase = ecUpperCase
    TabOrder = 4
    Text = ''
    Visible = False
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 48
    Width = 972
    Height = 324
    DataSource = DataSource1
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid1DrawColumnCell
  end
  object dtInicial: TJvDateEdit
    Left = 8
    Top = 21
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 6
  end
  object dtFinal: TJvDateEdit
    Left = 109
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 7
  end
  object Panel1: TPanel
    Left = 236
    Top = 152
    Width = 309
    Height = 41
    Caption = 'Gerando ......'
    Color = 4227327
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic]
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    Visible = False
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 232
    Top = 116
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select cod_cliente, nr_cnpj_cpf, nm_cliente'
      'from tb_cliente'
      'where fl_status = '#39'A'#39
      'and fl_os = '#39'S'#39
      'order by nm_cliente')
    Left = 468
    Top = 52
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      Precision = 32
      Size = 0
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
    end
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      Size = 50
    end
  end
  object dtsCliente: TDataSource
    DataSet = QCliente
    Left = 516
    Top = 52
  end
  object ADOQuery1: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select c.dt_conhecimento, e.codcgc nr_cnpj_cpf_EXP, sum(n.pesokg' +
        ') nr_peso_real, v.numvei ds_placa, c.nr_conhecimento,'
      
        'd.codcgc nr_cnpj_cpf_DES, sum(vlrmer) vl_declarado_nf, p.codcgc ' +
        'nr_cnpj_cli, cd.estado ds_uf_DES, cd.descri ds_cidade_EXP,'
      
        'cd.descri ds_cidade_DES, n.ordcom nr_pedido, '#39#39' ds_mvm, n.notfis' +
        ' nr_nf, vlrmer nr_valor, c.vl_total vl_total_impresso,'
      
        't.descri fl_tipo, f.datinc data, m.data_saida DT_DESTINO_SAIDA, ' +
        'c.nr_serie, f.numdup nr_fatura'
      
        'from tb_conhecimento c left join cyber.rodcli e on c.cod_remeten' +
        'te = e.codclifor'
      
        '                       left join cyber.rodmun co on co.codmun = ' +
        'c.codmuno'
      
        '                       left join cyber.rodcli d on c.cod_destina' +
        'tario = d.codclifor'
      
        '                       left join cyber.rodmun cd on cd.codmun = ' +
        'c.codmund'
      
        '                       left join tb_cte_nf n on n.codcon = c.nr_' +
        'conhecimento and n.sercon = c.nr_serie and n.fl_empresa = c.fl_e' +
        'mpresa'
      
        '                       left join cyber.rodcli p on c.cod_pagador' +
        ' = p.codclifor'
      
        '                       left join tb_manitem i on i.coddoc = c.nr' +
        '_conhecimento and i.fildoc = 9 and i.tipdoc = '#39'C'#39
      
        '                       left join tb_manifesto m on i.manifesto =' +
        ' m.manifesto and m.filial = i.filial'
      
        '                       left join cyber.rodvei v on v.codvei = m.' +
        'placa'
      
        '                       left join cyber.rodidu f on f.coddoc = c.' +
        'nr_conhecimento and f.codfil = 9 and f.tipdoc = '#39'CTRC'#39
      
        '                       left join cyber.rodfro t on t.codfro = v.' +
        'codfro'
      'where p.codclifor = 106 and c.fl_empresa = 9'
      
        'and c.dt_conhecimento between to_date('#39'01/11/13'#39','#39'dd/mm/yy'#39') and' +
        ' to_date('#39'16/05/15'#39','#39'dd/mm/yy'#39')'
      
        'and (fnc_cnpj(e.codcgc) = '#39'66975699000202'#39' or fnc_cnpj(d.codcgc)' +
        ' = '#39'66975699000202'#39')'
      'and n.ordcom is not null and c.fl_status = '#39'A'#39
      
        'group by c.dt_conhecimento, e.codcgc, v.numvei, c.nr_conheciment' +
        'o, d.codcgc, p.codcgc, cd.estado, cd.descri, co.descri, n.ordcom' +
        ', --m.lacres,'
      
        'n.notfis, vlrmer, c.vl_total, t.descri, f.datinc, m.datainc, c.n' +
        'r_serie, f.numdup, m.data_saida')
    Left = 228
    Top = 72
    object ADOQuery1DT_CONHECIMENTO: TDateTimeField
      DisplayWidth = 22
      FieldName = 'DT_CONHECIMENTO'
    end
    object ADOQuery1NR_CNPJ_CPF_EXP: TStringField
      DisplayWidth = 24
      FieldName = 'NR_CNPJ_CPF_EXP'
    end
    object ADOQuery1NR_PESO_REAL: TBCDField
      DisplayWidth = 16
      FieldName = 'NR_PESO_REAL'
      ReadOnly = True
      Precision = 32
    end
    object ADOQuery1DS_PLACA: TStringField
      DisplayWidth = 10
      FieldName = 'DS_PLACA'
      Size = 8
    end
    object ADOQuery1NR_CONHECIMENTO: TBCDField
      DisplayWidth = 40
      FieldName = 'NR_CONHECIMENTO'
      Precision = 32
    end
    object ADOQuery1NR_CNPJ_CPF_DES: TStringField
      DisplayWidth = 24
      FieldName = 'NR_CNPJ_CPF_DES'
    end
    object ADOQuery1VL_DECLARADO_NF: TBCDField
      DisplayWidth = 40
      FieldName = 'VL_DECLARADO_NF'
      ReadOnly = True
      Precision = 32
    end
    object ADOQuery1NR_CNPJ_CLI: TStringField
      DisplayWidth = 24
      FieldName = 'NR_CNPJ_CLI'
    end
    object ADOQuery1DS_UF_DES: TStringField
      DisplayWidth = 12
      FieldName = 'DS_UF_DES'
      FixedChar = True
      Size = 2
    end
    object ADOQuery1DS_CIDADE_EXP: TStringField
      DisplayWidth = 48
      FieldName = 'DS_CIDADE_EXP'
      Size = 40
    end
    object ADOQuery1DS_CIDADE_DES: TStringField
      DisplayWidth = 48
      FieldName = 'DS_CIDADE_DES'
      Size = 40
    end
    object ADOQuery1NR_PEDIDO: TStringField
      DisplayWidth = 60
      FieldName = 'NR_PEDIDO'
      Size = 50
    end
    object ADOQuery1DS_MVM: TStringField
      DisplayWidth = 72
      FieldName = 'DS_MVM'
      Size = 60
    end
    object ADOQuery1NR_NF: TStringField
      DisplayWidth = 24
      FieldName = 'NR_NF'
    end
    object ADOQuery1NR_VALOR: TBCDField
      DisplayWidth = 18
      FieldName = 'NR_VALOR'
      Precision = 14
      Size = 2
    end
    object ADOQuery1VL_TOTAL_IMPRESSO: TBCDField
      DisplayWidth = 21
      FieldName = 'VL_TOTAL_IMPRESSO'
      Precision = 14
      Size = 2
    end
    object ADOQuery1FL_TIPO: TStringField
      DisplayWidth = 120
      FieldName = 'FL_TIPO'
      Size = 100
    end
    object ADOQuery1DATA: TDateTimeField
      DisplayWidth = 22
      FieldName = 'DATA'
    end
    object ADOQuery1DT_DESTINO_SAIDA: TDateTimeField
      DisplayWidth = 22
      FieldName = 'DT_DESTINO_SAIDA'
    end
    object ADOQuery1NR_SERIE: TStringField
      DisplayWidth = 10
      FieldName = 'NR_SERIE'
      Size = 3
    end
    object ADOQuery1NR_FATURA: TBCDField
      DisplayWidth = 40
      FieldName = 'NR_FATURA'
      Precision = 32
    end
  end
  object nmSMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 564
    Top = 128
  end
  object IdMessage: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 628
    Top = 128
  end
end
