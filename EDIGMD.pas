unit EDIGMD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Mask, JvExMask, JvToolEdit, ExtCtrls, DB,
  ADODB, Buttons, ComCtrls, FileCtrl, Gauges, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, JvDBLookup, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase,
  IdSMTP, IdMessage;

type
  TfrmEDIGMD = class(TForm)
    DataSource1: TDataSource;
    BitBtn1: TBitBtn;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Label1: TLabel;
    Label14: TLabel;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    ADOQuery1: TADOQuery;
    DBGrid1: TDBGrid;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    nmSMTP1: TIdSMTP;
    IdMessage: TIdMessage;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNR_CNPJ_CPF: TStringField;
    QClienteNM_CLIENTE: TStringField;
    Panel1: TPanel;
    ADOQuery1DT_CONHECIMENTO: TDateTimeField;
    ADOQuery1NR_CNPJ_CPF_EXP: TStringField;
    ADOQuery1NR_PESO_REAL: TBCDField;
    ADOQuery1DS_PLACA: TStringField;
    ADOQuery1NR_CONHECIMENTO: TBCDField;
    ADOQuery1NR_CNPJ_CPF_DES: TStringField;
    ADOQuery1VL_DECLARADO_NF: TBCDField;
    ADOQuery1NR_CNPJ_CLI: TStringField;
    ADOQuery1DS_UF_DES: TStringField;
    ADOQuery1DS_CIDADE_EXP: TStringField;
    ADOQuery1DS_CIDADE_DES: TStringField;
    ADOQuery1NR_PEDIDO: TStringField;
    ADOQuery1DS_MVM: TStringField;
    ADOQuery1NR_NF: TStringField;
    ADOQuery1NR_VALOR: TBCDField;
    ADOQuery1VL_TOTAL_IMPRESSO: TBCDField;
    ADOQuery1FL_TIPO: TStringField;
    ADOQuery1DATA: TDateTimeField;
    ADOQuery1DT_DESTINO_SAIDA: TDateTimeField;
    ADOQuery1NR_SERIE: TStringField;
    ADOQuery1NR_FATURA: TBCDField;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private

  public
    { Public declarations }
  end;

var
  frmEDIGMD: TfrmEDIGMD;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmEDIGMD.BitBtn1Click(Sender: TObject);
var
  Info: TextFile;
  sArquivo, mes, ano, dia, tipo, tt: String;
  d, m, y: word;
  mail: TStringList;
begin
  DecodeDate(date, y, m, d);
  dia := inttostr(d);
  mes := inttostr(m);
  ano := inttostr(y);
  if Length(dia) = 1 then
    dia := '0' + dia;
  if Length(mes) = 1 then
    mes := '0' + mes;

  if copy(dtInicial.text, 1, 1) = ' ' then
  begin
    ShowMessage('N�o foi colocada a Data Inicial');
    exit;
  end;

  if copy(dtFinal.text, 1, 1) = ' ' then
  begin
    ShowMessage('N�o foi colocada a Data Final');
    exit;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  Panel1.Visible := true;

  ADOQuery1.Close;
  ADOQuery1.SQL[14] := 'and c.datcad between to_date(''' + dtInicial.text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  // Adoquery1.SQL[15] := 'and((fnc_cnpj(e.codcgc) = '+#39+ledIdCliente.LookupValue+#39+' or fnc_cnpj(d.codcgc) = '+#39+ledIdCliente.LookupValue+#39+') ';
  ADOQuery1.Open;
  // Adoquery1.sql.SaveToFile('c:\sql.txt');
  // showmessage(Adoquery1.sql.text);
  if ADOQuery1.IsEmpty then
    ShowMessage('N�o existem dados !!')
  else
  begin
    ADOQuery1.first;

    sArquivo := 'C:\SIM\' + ano + mes + dia + 'IW.txt';

    AssignFile(Info, sArquivo);
    if not FileExists(sArquivo) then
      Rewrite(Info)
    else
      Rewrite(Info);
    ADOQuery1.first;
    while not ADOQuery1.Eof do
    begin
      if ADOQuery1NR_VALOR.value > 0 then
      begin
        if ADOQuery1DS_UF_DES.value = 'MG' then
        begin
          if UpperCase(ADOQuery1DS_CIDADE_DES.value)
            = UpperCase(ADOQuery1DS_CIDADE_EXP.value) then
            tipo := '0001'
          else
            tipo := '0002';
        end
        else if ADOQuery1DS_UF_DES.value = 'SP' then
          tipo := '0003'
        else if ADOQuery1DS_UF_DES.value = 'RJ' then
          tipo := '0004'
        else if ADOQuery1DS_UF_DES.value = 'PR' then
          tipo := '0005'
        else if ADOQuery1DS_UF_DES.value = 'SC' then
          tipo := '0006'
        else if ADOQuery1DS_UF_DES.value = 'RS' then
          tipo := '0007'
        else
          tipo := '9999';

        tt := ADOQuery1VL_TOTAL_IMPRESSO.Asstring;

        WriteLn(Info, apcarac(apcarac(formatdatetime('dd/mm/yy',
          ADOQuery1DT_DESTINO_SAIDA.value))),
          RetbranE(apcarac(ADOQuery1NR_NF.Asstring), 10),
          RetbranE(apcarac(ADOQuery1NR_CNPJ_CPF_EXP.Asstring), 15),
          Retzero(apcarac(ADOQuery1NR_PESO_REAL.Asstring), 10),
          Retzero(apcarac(floattostrf(ADOQuery1NR_VALOR.value, ffnumber, 10, 2)
          ), 10), (formatdatetime('hh:mm', ADOQuery1DT_DESTINO_SAIDA.value)),
          RetbranE(apcarac(ADOQuery1DS_PLACA.Asstring), 07),
          RetbranE(copy(ADOQuery1FL_TIPO.Asstring, 1, 10), 10),
          RetbranE(ADOQuery1NR_CONHECIMENTO.Asstring, 10),
          RetbranE(apcarac(ADOQuery1NR_CNPJ_CPF_DES.Asstring), 15),
          Retzero(ADOQuery1NR_PEDIDO.Asstring, 10), Retzero('0', 10), // MVM
          Retzero(ADOQuery1NR_FATURA.Asstring, 10), ' 10761960000209',
          RetbranE(apcarac(ADOQuery1.FieldByName('nr_cnpj_cli').Asstring), 15),
          apcarac(apcarac(formatdatetime('dd/mm/yy', ADOQuery1DATA.value))),
          apcarac(apcarac(formatdatetime('dd/mm/yy',
          ADOQuery1DT_CONHECIMENTO.value))),
          Retzero(apcarac(floattostrf(ADOQuery1VL_TOTAL_IMPRESSO.value,
          ffnumber, 16, 2)), 16), tipo,
          RetbranE(apcarac(ADOQuery1.FieldByName('nr_serie').Asstring), 4));
      end;
      ADOQuery1.next;
    end;
    closefile(Info);
    Panel1.Visible := false;

    mail := TStringList.Create;
    try
      mail.values['to'] := '';
      /// AQUI VAI O EMAIL DO DESTINATARIO///
      mail.values['subject'] := 'CTRC�s emitidos ';
      /// AQUI O ASSUNTO///
      mail.values['body'] := '';
      /// AQUI O TEXTO NO CORPO DO EMAIL///
      mail.values['attachment0'] := sArquivo;
      /// /AQUI O ENDERE�O ONDE ENCONTRAM OS ARQUIVOS//
      mail.values['attachment1'] := '';
      /// IDEM  - NO ATTACHMENT1    TEM QUE COLOCAR A SEQUNCIA DO EMAIL A QUAL DESEJA ENVIAR EXEMPLO: ATTACHMENT1
      sendEMail(Application.Handle, mail);
    finally
      mail.Free;
    end;
  end;
end;

procedure TfrmEDIGMD.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if ADOQuery1NR_FATURA.value = 0 then
  begin
    DBGrid1.canvas.Brush.Color := clRed;
    DBGrid1.canvas.Font.Color := clWhite;
  end;
  DBGrid1.DefaultDrawDataCell(Rect, DBGrid1.columns[DataCol].field, State);
end;

procedure TfrmEDIGMD.FormCreate(Sender: TObject);
begin
  // QCliente.open;
end;

procedure TfrmEDIGMD.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
