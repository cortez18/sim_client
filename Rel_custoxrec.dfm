object frmRel_custoxrec: TfrmRel_custoxrec
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de Custo sobre Receita'
  ClientHeight = 531
  ClientWidth = 968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 12
    Top = 8
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 115
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 503
    Width = 755
    Height = 23
    Progress = 0
  end
  object Label8: TLabel
    Left = 809
    Top = 6
    Width = 47
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object dtInicial: TJvDateEdit
    Left = 12
    Top = 27
    Width = 90
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 115
    Top = 27
    Width = 90
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 1
  end
  object btRelatorio: TBitBtn
    Left = 211
    Top = 18
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btRelatorioClick
  end
  object btnExcel: TBitBtn
    Left = 300
    Top = 18
    Width = 75
    Height = 25
    Caption = 'Excel'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
      346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
      2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
      33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
      38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
      3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
      40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
      73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
      33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
      64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
      33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
      33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
      3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
      D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
      49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
      CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
      53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
      C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
      BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
      62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
      56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
      45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
      4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
      4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
      49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
      4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
      4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 3
    OnClick = btnExcelClick
  end
  object JvDBGrid1: TJvDBGrid
    Left = 1
    Top = 54
    Width = 801
    Height = 439
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = JvDBGrid1DblClick
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'DOC'
        Title.Caption = 'CT-e/NFs'
        Width = 59
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FILDOC'
        Title.Caption = 'Filial'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CLIENTE'
        Title.Caption = 'C'#243'd. Cliente'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE'
        Title.Caption = 'Nome Cliente'
        Width = 189
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO'
        Title.Caption = 'Custo'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RECEITA'
        Title.Caption = 'Receita'
        Width = 63
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'STATUS'
        Title.Caption = 'Status Gest'#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TRANSP'
        Title.Caption = 'C'#243'd. Transp.'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_TRANSP'
        Title.Caption = 'Nome Transportador'
        Width = 233
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MAN_ROM'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_DOC'
        Title.Caption = 'Data Man.'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FILIAL'
        Title.Caption = 'Filial'
        Width = 42
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TIP_DOC'
        Title.Caption = 'Tipo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPERACAO'
        Title.Caption = 'Opera'#231#227'o'
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COD_TAB'
        Title.Caption = 'C'#243'd. Tabela'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ESTADO'
        Title.Caption = 'UF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE'
        Title.Caption = 'Cidade'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOCAL'
        Title.Caption = 'Regi'#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fatura'
        Title.Caption = 'Fatura'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CTE_TRANSP'
        Title.Caption = 'CT-e Transp.'
        Width = 88
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 224
    Top = 176
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    Visible = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 808
    Top = 41
    Width = 157
    Height = 452
    TabOrder = 6
  end
  object DriveComboBox1: TDriveComboBox
    Left = 808
    Top = 20
    Width = 157
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 7
  end
  object Memo1: TMemo
    Left = 300
    Top = 244
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 8
    Visible = False
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 8
    Top = 499
    Width = 188
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 9
  end
  object RGC: TRadioGroup
    Left = 684
    Top = 8
    Width = 113
    Height = 40
    Caption = 'Calcular ?'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 10
  end
  object RGS: TRadioGroup
    Left = 381
    Top = 8
    Width = 284
    Height = 40
    Hint = 'Todos s'#243' serve para P e N, A e B ser'#227'o sempre separados.'
    Caption = 'Status'
    Columns = 6
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'P'
      'N'
      'A'
      'B'
      'Geral')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
  end
  object QCusto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct cu.doc, cu.filial, cu.cliente, cu.receita, case ' +
        'when cu.custo = -99 then 0 else cu.custo end custo, cu.status,'
      
        'cu.transp, cu.man_rom, cu.dt_doc, cu.tip_doc, cu.nr_nf, cu.opera' +
        'cao, cu.fildoc, cu.cod_tab , cu.fatura'
      
        ', cl.razsoc nm_cliente, tr.razsoc nm_transp, cd.descri cidade, c' +
        'd.estado, cd.codipv local, ct.coddes, cu.cte_transp'
      
        'from tb_custo_cliente cu left join cyber.rodcli cl on cu.cliente' +
        ' = cl.codclifor'
      
        '                         left join cyber.rodcli tr on cu.transp ' +
        ' = tr.codclifor'
      
        '                         left join vw_itens_manifesto_sim ct on ' +
        'ct.CODDOC = cu.doc and ct.fildoc = cu.fildoc and cu.man_rom = ct' +
        '.manifesto and cu.filial = ct.filial'
      
        '                         left join tb_conhecimento cte on cte.nr' +
        '_conhecimento = cu.doc and cte.fl_empresa = cu.fildoc'
      
        '                         left join cyber.rodcli ds on ds.codclif' +
        'or = nvl(ct.coddes, cte.cod_destinatario)'
      
        '                         left join cyber.rodmun cd on cd.codmun ' +
        '= ds.codmun'
      'where cu.status <> '#39'0'#39
      '                      '
      'order by dt_doc, tip_doc')
    Left = 224
    Top = 236
    object QCustoDOC: TBCDField
      FieldName = 'DOC'
      Precision = 32
      Size = 0
    end
    object QCustoFILIAL: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
      Size = 0
    end
    object QCustoCLIENTE: TBCDField
      FieldName = 'CLIENTE'
      Precision = 32
      Size = 0
    end
    object QCustoNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      Size = 80
    end
    object QCustoRECEITA: TBCDField
      FieldName = 'RECEITA'
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object QCustoCUSTO: TBCDField
      FieldName = 'CUSTO'
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object QCustoSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object QCustoTRANSP: TBCDField
      FieldName = 'TRANSP'
      Precision = 32
      Size = 0
    end
    object QCustoNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      Size = 80
    end
    object QCustoMAN_ROM: TBCDField
      FieldName = 'MAN_ROM'
      Precision = 32
      Size = 0
    end
    object QCustoDT_DOC: TDateTimeField
      FieldName = 'DT_DOC'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QCustoTIP_DOC: TStringField
      FieldName = 'TIP_DOC'
      Size = 10
    end
    object QCustoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 30
    end
    object QCustoFILDOC: TBCDField
      FieldName = 'FILDOC'
      Precision = 32
      Size = 0
    end
    object QCustoCOD_TAB: TBCDField
      FieldName = 'COD_TAB'
      Precision = 32
      Size = 0
    end
    object QCustoNR_NF: TBCDField
      FieldName = 'NR_NF'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCustoCIDADE: TStringField
      FieldName = 'CIDADE'
      ReadOnly = True
      Size = 40
    end
    object QCustoESTADO: TStringField
      FieldName = 'ESTADO'
      ReadOnly = True
      Size = 2
    end
    object QCustoLOCAL: TStringField
      FieldName = 'LOCAL'
      ReadOnly = True
      Size = 6
    end
    object QCustofatura: TStringField
      FieldName = 'fatura'
      Size = 30
    end
    object QCustoCTE_TRANSP: TFMTBCDField
      FieldName = 'CTE_TRANSP'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
  end
  object navnavig: TDataSource
    DataSet = QCusto__
    Left = 272
    Top = 236
  end
  object SP_custo: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_CUSTO'
    Parameters = <>
    Left = 248
    Top = 4
  end
  object SP_AUTO: TADOStoredProc
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_CUSTO_AUTO_DIA'
    Parameters = <
      item
        Name = 'vdataI'
        DataType = ftString
        Size = 10
        Value = '13/09/2020'
      end
      item
        Name = 'vDataF'
        DataType = ftString
        Size = 10
        Value = '13/09/2020'
      end>
    Prepared = True
    Left = 248
    Top = 80
  end
  object QCusto__: TADOQuery
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct cu.codcon doc, cu.filial, cl.codclifor cliente, ' +
        'cl.razsoc nm_cliente, cu.totfre receita, case when cu.custo_calc' +
        ' = -99 then 0 else cu.custo_calc end custo, cu.status,'
      
        'cu.transp, tr.razsoc nm_transp, cu.doc man_rom, cu.data dt_doc, ' +
        'cu.tipo tip_doc, cu.operacao, cu.codfil fildoc, cod_tab_i cod_ta' +
        'b,  '
      
        'cd.descri cidade, cd.estado, cd.codipv local, cu.fatura, ds.codc' +
        'lifor coddes, cu.cte_custo cte_transp, 0 nr_nf'
      
        'from vw_custo_transp_sim cu left join tb_manifesto m on m.manife' +
        'sto = cu.doc and m.serie = cu.serie and m.filial = cu.filial'
      
        '                           left join cyber.rodcli tr on cu.trans' +
        'p  = tr.codclifor                         '
      
        '                           left join tb_conhecimento cte on cte.' +
        'nr_conhecimento = cu.codcon and cte.fl_empresa = cu.codfil'
      
        '                           left join cyber.rodcli cl on cu.codpa' +
        'g = cl.codclifor'
      
        '                           left join cyber.rodcli ds on ds.codcl' +
        'ifor = nvl(cte.cod_redespacho, cte.cod_destinatario)'
      
        '                           left join cyber.rodmun cd on cd.codmu' +
        'n = ds.codmun'
      '                         '
      'where-- m.km_novocusto > 0 -- and m.manifesto = 21254'
      'id_man in ('
      '295849,'
      '294917,'
      '294914,'
      '294763,'
      '294744,'
      '296363,'
      '296303,'
      '296300,'
      '296161,'
      '296092,'
      '295954,'
      '295515,'
      '295401,'
      '295383,'
      '295159,'
      '294791,'
      '294664,'
      '294632)'
      'order by cu.data, cu.tipo')
    Left = 224
    Top = 300
    object BCDField1: TBCDField
      FieldName = 'DOC'
      Precision = 32
      Size = 0
    end
    object BCDField2: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
      Size = 0
    end
    object BCDField3: TBCDField
      FieldName = 'CLIENTE'
      Precision = 32
      Size = 0
    end
    object StringField1: TStringField
      FieldName = 'NM_CLIENTE'
      Size = 80
    end
    object BCDField4: TBCDField
      FieldName = 'RECEITA'
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object BCDField5: TBCDField
      FieldName = 'CUSTO'
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object StringField2: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object BCDField6: TBCDField
      FieldName = 'TRANSP'
      Precision = 32
      Size = 0
    end
    object StringField3: TStringField
      FieldName = 'NM_TRANSP'
      Size = 80
    end
    object BCDField7: TBCDField
      FieldName = 'MAN_ROM'
      Precision = 32
      Size = 0
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DT_DOC'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object StringField4: TStringField
      FieldName = 'TIP_DOC'
      Size = 10
    end
    object StringField5: TStringField
      FieldName = 'OPERACAO'
      Size = 30
    end
    object BCDField8: TBCDField
      FieldName = 'FILDOC'
      Precision = 32
      Size = 0
    end
    object BCDField9: TBCDField
      FieldName = 'COD_TAB'
      Precision = 32
      Size = 0
    end
    object BCDField10: TBCDField
      FieldName = 'NR_NF'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object StringField6: TStringField
      FieldName = 'CIDADE'
      ReadOnly = True
      Size = 40
    end
    object StringField7: TStringField
      FieldName = 'ESTADO'
      ReadOnly = True
      Size = 2
    end
    object StringField8: TStringField
      FieldName = 'LOCAL'
      ReadOnly = True
      Size = 6
    end
    object StringField9: TStringField
      FieldName = 'fatura'
      Size = 30
    end
    object FMTBCDField1: TFMTBCDField
      FieldName = 'CTE_TRANSP'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
  end
end
