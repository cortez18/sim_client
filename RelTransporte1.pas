unit RelTransporte1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRelTransporte1 = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    Memo1: TMemo;
    JvDBNavigator1: TJvDBNavigator;
    QClienteCOD_CLIENTE: TBCDField;
    ADOQuery1: TADOQuery;
    BCDField1: TBCDField;
    DateTimeField1: TDateTimeField;
    BCDField2: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    BCDField3: TBCDField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    BCDField6: TBCDField;
    BCDField7: TBCDField;
    BCDField8: TBCDField;
    QCtrcCODPAG: TBCDField;
    QCtrcDATINC: TDateTimeField;
    QCtrcCODFIL: TBCDField;
    QCtrcCIDADE_O: TStringField;
    QCtrcUF_O: TStringField;
    QCtrcCIDADE_D: TStringField;
    QCtrcUF_D: TStringField;
    QCtrcCODCON: TBCDField;
    QCtrcTOTFRE: TBCDField;
    QCtrcPESO: TBCDField;
    QCtrcVALOR_NF: TBCDField;
    QCtrcVOLUME: TBCDField;
    QCtrcCUSTO: TBCDField;
    QCtrcOPERACAO: TStringField;
    QCtrcMANIFESTO: TBCDField;
    QCtrcCIDADE_R: TStringField;
    QCtrcUF_R: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelTransporte1: TfrmRelTransporte1;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelTransporte1.btnExcelClick(Sender: TObject);
var
  coluna, linha: integer;
  excel: variant;
  valor: string;
begin
  try
    excel := CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
  except
    Application.MessageBox('Vers�o do Ms-Excel Incompat�vel', 'Erro',
      MB_OK + MB_ICONEXCLAMATION);
  end;
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  try
    for linha := 0 to QCtrc.RecordCount - 1 do
    begin
      for coluna := 1 to JvDBGrid1.Columns.Count do
      begin
        valor := QCtrc.Fields[coluna - 1].AsString;
        if QCtrc.Fields[coluna - 1].DataType = ftDateTime then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := 'dd/mm/aaaa';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsDateTime;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftString then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '@';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsString;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftBCD then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsFloat;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftFloat then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0,00';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsFloat;
        end
        else if QCtrc.Fields[coluna - 1].DataType = ftInteger then
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '#.###.##0';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsInteger;
        end
        else
        begin
          excel.cells[linha + 2, coluna].NumberFormat := '@';
          excel.cells[linha + 2, coluna] := QCtrc.Fields[coluna - 1].AsString;
        end;
      end;
      Gauge1.AddProgress(1);
      QCtrc.Next;
    end;
    for coluna := 1 to JvDBGrid1.Columns.Count do
    begin
      valor := JvDBGrid1.Columns[coluna - 1].Title.Caption;
      excel.cells[1, coluna] := valor;
    end;
    excel.Columns.AutoFit;
    excel.visible := true;
  except
    Application.MessageBox('Aconteceu um erro desconhecido durante a convers�o'
      + 'da tabela para o Ms-Excel', 'Erro', MB_OK + MB_ICONEXCLAMATION);
  end;
end;

procedure TfrmRelTransporte1.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    ShowMessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  if Trim(ledIdCliente.LookupValue) = '' then
  begin
    ShowMessage('O Cliente � Obrigat�rio');
    ledIdCliente.SetFocus;
    exit;
  end;
  Panel1.visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  QCtrc.SQL[3] := 'Where codpag = ''' + ledIdCliente.LookupValue + '''';
  QCtrc.SQL[4] := 'and datinc between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCtrc.open;
  Panel1.visible := false;
end;

procedure TfrmRelTransporte1.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelTransporte1.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelTransporte1.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelTransporte1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmRelTransporte1.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmRelTransporte1.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRelTransporte1.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmRelTransporte1.QClienteBeforeOpen(DataSet: TDataSet);
begin
  QCliente.Parameters[0].Value := GLBFilial;
  QCliente.Parameters[1].Value := GLBFilial;
end;

end.
