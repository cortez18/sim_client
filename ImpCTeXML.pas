unit ImpCTeXML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, Buttons, ComCtrls,
  ACBrCTe, ADODB, FileCtrl, Gauges, ExtCtrls, ACBrBase, ACBrDFe;

type
  TfrmEDICTRCXML = class(TForm)
    ACBrCTe1: TACBrCTe;
    PanelImp: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    PB1: TGauge;
    Label5: TLabel;
    Label7: TLabel;
    btnSair: TBitBtn;
    btnGravar: TBitBtn;
    cbTodos: TCheckBox;
    FLB: TFileListBox;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Edit1: TEdit;
    SP_Comp: TADOStoredProc;
    SP_carga: TADOStoredProc;
    SP_NF: TADOStoredProc;
    SP_XML: TADOStoredProc;
    QTabCusto: TADOQuery;
    QTransp: TADOQuery;
    QTranspCODCLIFOR: TBCDField;
    QTabCustoQUANT: TBCDField;
    Panel1: TPanel;
    Memo1: TMemo;
    Button1: TButton;
    Label2: TLabel;
    Label6: TLabel;
    QTranspRAZSOC: TStringField;
    Memo2: TMemo;
    Label8: TLabel;
    SP_ROD: TADOStoredProc;
    SP_TEM: TADOStoredProc;
    ConIntecom: TADOConnection;
    SP_INTEGRA: TADOStoredProc;
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGravarClick(Sender: TObject);
    procedure processar(arquivo: string);
    procedure FLBClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
  var
    erro: string;
  public
    { Public declarations }
  end;

var
  frmEDICTRCXML: TfrmEDICTRCXML;

implementation

uses funcoes, menu, Dados, pcteConversaoCTe;

{$R *.dfm}

procedure TfrmEDICTRCXML.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmEDICTRCXML.Button1Click(Sender: TObject);
begin
  Panel1.Visible := false;
  Memo1.Clear;
end;

procedure TfrmEDICTRCXML.FLBClick(Sender: TObject);
begin
  Edit1.text := FLB.FileName;
end;

procedure TfrmEDICTRCXML.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmEDICTRCXML.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfrmEDICTRCXML.processar(arquivo: String);
var
  i, n, teste, tem: integer;
  a: String;
begin
  ACBrCTe1.Conhecimentos.Clear;
  ACBrCTe1.Conhecimentos.LoadFromFile(arquivo);

  for n := 0 to ACBrCTe1.Conhecimentos.Count - 1 do
  begin
    with ACBrCTe1.Conhecimentos.Items[n].CTe do
    begin
      a := IntToStr(Ide.nCT) + IntToStr(Ide.Serie);
      erro := '';
      SP_TEM.Parameters[1].value := a;
      SP_TEM.Parameters[2].value := emit.CNPJ;
      SP_TEM.ExecProc;
      tem := SP_TEM.Parameters[0].value;

      QTransp.Close;
      QTransp.Parameters[0].value := Copy(emit.CNPJ, 1, 2) + '.' +
        Copy(emit.CNPJ, 3, 3) + '.' + Copy(emit.CNPJ, 6, 3) + '/' +
        Copy(emit.CNPJ, 9, 4) + '-' + Copy(emit.CNPJ, 13, 2);
      QTransp.Open;
      // teste := QTranspCODCLIFOR.AsInteger;
      QTabCusto.Close;
      QTabCusto.Parameters[0].value := Copy(emit.CNPJ, 1, 2) + '.' +
        Copy(emit.CNPJ, 3, 3) + '.' + Copy(emit.CNPJ, 6, 3);
      // QTranspCODCLIFOR.AsInteger;
      QTabCusto.Open;

      if (QTabCustoQUANT.AsInteger = 0) then
      Begin
        Memo1.Lines.Add(IntToStr(Ide.nCT) + '-' + QTranspRAZSOC.AsString);
        erro := 'S';
      End
      else
      begin
        if (tem = 0) then
        begin
          SP_XML.Parameters[0].value := StrToInt(a);
          SP_XML.Parameters[1].value := Rem.CNPJCPF;
          SP_XML.Parameters[2].value := dest.CNPJCPF;
          SP_XML.Parameters[3].value := Ide.Serie;
          SP_XML.Parameters[4].value := Ide.nCT;
          SP_XML.Parameters[5].value := Ide.dhEmi;
          SP_XML.Parameters[6].value := Ide.forPag;
          SP_XML.Parameters[7].value := VPrest.vTPrest;
          SP_XML.Parameters[8].value := Imp.ICMS.ICMS00.vBC;
          SP_XML.Parameters[9].value := Imp.ICMS.ICMS00.pICMS;
          SP_XML.Parameters[10].value := Imp.ICMS.ICMS00.vICMS;
          SP_XML.Parameters[11].value := emit.CNPJ;
          SP_XML.Parameters[12].value := procCTe.chCTe;
          SP_XML.Parameters[13].value := GLBUSER;
          if Ide.toma4.CNPJCPF <> '' then
            SP_XML.Parameters[14].value := Ide.toma4.CNPJCPF
          else if Ide.Toma03.Toma = tmExpedidor then
            SP_XML.Parameters[14].value := exped.CNPJCPF
          else if Ide.Toma03.Toma = tmRecebedor then
            SP_XML.Parameters[14].value := receb.CNPJCPF
          else if Ide.Toma03.Toma = tmDestinatario then
            SP_XML.Parameters[14].value := dest.CNPJCPF;
          SP_XML.Parameters[15].value := Memo2.text;
          SP_XML.ExecProc;

          for i := 0 to VPrest.Comp.Count - 1 do
          begin
            if VPrest.Comp.Items[i].vComp > 0 then
            begin
              SP_Comp.Parameters[0].value := StrToInt(a);
              SP_Comp.Parameters[1].value := emit.CNPJ;
              SP_Comp.Parameters[2].value := VPrest.Comp.Items[i].xNome;
              SP_Comp.Parameters[3].value := VPrest.Comp.Items[i].vComp;
              SP_Comp.ExecProc;
            end;
          end;

          for i := 0 to infCTeNorm.infDoc.infNF.Count - 1 do
          begin
            SP_NF.Parameters[0].value := StrToInt(a);
            SP_NF.Parameters[1].value := emit.CNPJ;
            SP_NF.Parameters[2].value := infCTeNorm.infDoc.infNF.Items[i].nDoc;
            SP_NF.Parameters[3].value := infCTeNorm.infDoc.infNF.Items[i].Serie;
            SP_NF.Parameters[4].value := '';
            SP_NF.Parameters[5].value := infCTeNorm.infDoc.infNF.Items[i].vNF;
            SP_NF.ExecProc;
          end;

          for i := 0 to infCTeNorm.infDoc.infNfe.Count - 1 do
          begin
            SP_NF.Parameters[0].value := StrToInt(a);
            SP_NF.Parameters[1].value := emit.CNPJ;
            SP_NF.Parameters[2].value :=
              IntToStr(StrToInt(Copy(infCTeNorm.infDoc.infNfe.Items[i]
              .chave, 26, 9)));
            SP_NF.Parameters[3].value := 'Nfe';
            SP_NF.Parameters[4].value := infCTeNorm.infDoc.infNfe.Items
              [i].chave;
            SP_NF.Parameters[5].value := 0;
            SP_NF.ExecProc;
          end;

          for i := 0 to infCTeNorm.infCarga.InfQ.Count - 1 do
          begin
            SP_carga.Parameters[0].value := StrToInt(a);
            SP_carga.Parameters[1].value := emit.CNPJ;
            SP_carga.Parameters[2].value := infCTeNorm.infCarga.vCarga;
            SP_carga.Parameters[3].value :=
              infCTeNorm.infCarga.InfQ.Items[i].tpMed;
            SP_carga.Parameters[4].value := infCTeNorm.infCarga.InfQ.Items
              [i].qCarga;
            SP_carga.ExecProc;
          end;

          // Integra com o controle de custo
          SP_INTEGRA.Parameters[0].value := Ide.nCT;
          SP_INTEGRA.Parameters[1].value := emit.CNPJ;
          SP_INTEGRA.ExecProc;

          try
            // alimenta o livro fiscal
            SP_ROD.Parameters[0].value := StrToInt(a);
            SP_ROD.Parameters[1].value := Rem.CNPJCPF;
            SP_ROD.Parameters[2].value := dest.CNPJCPF;
            SP_ROD.Parameters[3].value := Ide.Serie;
            SP_ROD.Parameters[4].value := Ide.nCT;
            SP_ROD.Parameters[5].value := Ide.dhEmi;
            SP_ROD.Parameters[6].value := Ide.forPag;
            SP_ROD.Parameters[7].value := VPrest.vTPrest;
            SP_ROD.Parameters[8].value := Imp.ICMS.ICMS00.vBC;
            SP_ROD.Parameters[9].value := Imp.ICMS.ICMS00.pICMS;
            SP_ROD.Parameters[10].value := Imp.ICMS.ICMS00.vICMS;
            SP_ROD.Parameters[11].value := emit.CNPJ;
            SP_ROD.Parameters[12].value := procCTe.chCTe;
            SP_ROD.Parameters[13].value := GLBUSER;
            if Ide.toma4.CNPJCPF <> '' then
              SP_ROD.Parameters[14].value := Ide.toma4.CNPJCPF
            else if Ide.Toma03.Toma = tmExpedidor then
              SP_ROD.Parameters[14].value := exped.CNPJCPF
            else if Ide.Toma03.Toma = tmRecebedor then
              SP_ROD.Parameters[14].value := receb.CNPJCPF
            else if Ide.Toma03.Toma = tmDestinatario then
              SP_ROD.Parameters[14].value := dest.CNPJCPF;
            SP_ROD.ExecProc;
          except
            on E: Exception do

          end;

        end
        else
          erro := 'S';
      end;
    end;
  end;
end;

procedure TfrmEDICTRCXML.btnGravarClick(Sender: TObject);
var
  i, y: integer;
  xml: TextFile;
begin
  y := 0;
  if cbTodos.checked = True then
  begin
    Edit1.text := FLB.FileName;
    FLB.ItemIndex := 0;
    PB1.MaxValue := FLB.Items.Count;
    for i := 0 to FLB.Items.Count - 1 do
    begin
      Assignfile(xml, FLB.FileName);
      Reset(xml);
      y := 1 + y;
      Application.ProcessMessages;
      PB1.Progress := y;
      processar(FLB.FileName);
      CloseFile(xml);
      if erro = '' then
        RenameFile(FLB.FileName, FLB.FileName + '.OK')
      else if erro = 'F' then
        RenameFile(FLB.FileName, FLB.FileName + '.ROD')
      else
        RenameFile(FLB.FileName, FLB.FileName + '.DUPL');
      FLB.ItemIndex := FLB.ItemIndex + 1;
      erro := '';
    end;
  end
  else
  begin
    processar(FLB.FileName);
    if erro = '' then
      RenameFile(FLB.FileName, FLB.FileName + '.OK')
    else
      RenameFile(FLB.FileName, FLB.FileName + '.DUPL');
    erro := '';
  end;
  FLB.Update;
  if Memo1.Lines.Count > 0 then
    Panel1.Visible := True;
end;

end.
