unit EtiquetaCross;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit,
  JvMemoryDataset;

type
  TfrmEtiquetaCross = class(TForm)
    QCTe: TADOQuery;
    sp_volume: TADOStoredProc;
    QVol: TADOQuery;
    Panel1: TPanel;
    edCTe: TJvCalcEdit;
    edSerie: TEdit;
    Label1: TLabel;
    Label9: TLabel;
    Panel2: TPanel;
    Gauge1: TGauge;
    QCTeNR_CONHECIMENTO: TFMTBCDField;
    QCTeNR_QTDE_VOL: TFloatField;
    QCTeFL_EMPRESA: TFMTBCDField;
    Label2: TLabel;
    edVolumes: TJvCalcEdit;
    btRelatorio: TBitBtn;
    btnEtiqueta: TBitBtn;
    QCTeNR_SERIE: TStringField;
    QVolNR_CONHECIMENTO: TFMTBCDField;
    QVolNR_QTDE_VOL: TFloatField;
    QVolCOD_CONHECIMENTO: TFMTBCDField;
    QVolVOLUME: TFMTBCDField;
    QVolRAZSOC: TStringField;
    QVolCIDADE: TStringField;
    QVolUF: TStringField;
    QCTenf: TStringField;
    procedure btRelatorioClick(Sender: TObject);
    procedure btnEtiquetaClick(Sender: TObject);
    procedure QVolBeforeOpen(DataSet: TDataSet);
    procedure QCTeCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtiquetaCross: TfrmEtiquetaCross;

implementation

uses Dados, Menu, funcoes, AlteraCteEtiqueta;

{$R *.dfm}

procedure TfrmEtiquetaCross.btnEtiquetaClick(Sender: TObject);
var Arq : TextFile;
    Ini: TIniFile;
    impr,razao,nr_order : string;
    d,qt,i,seq, vol : integer;
begin
  Screen.Cursor := crHourGlass;
  razao := glbnmfilial;
  // inserir a tabela de volumes
  sp_volume.Parameters[0].Value := QCTeNR_CONHECIMENTO.AsInteger;
  sp_volume.Parameters[1].Value := QCTeFL_EMPRESA.AsInteger;
  sp_volume.Parameters[2].Value := QCTeNR_SERIE.AsString;
  sp_volume.Parameters[3].Value := GLBCodUser;
  sp_volume.ExecProc;

  QVol.Parameters[0].Value := edCte.Value;
  QVol.Parameters[1].Value := GLBFilial;
  QVol.Parameters[2].Value := edSerie.text;
  QVol.Open;

  if glbimpr = '' then
    impr := 'C:/SIM/printetiqueta.txt'
  else
    impr := glbimpr;
  AssignFile(Arq,impr);
  ReWrite(Arq);
  Application.ProcessMessages;
  Application.ProcessMessages;
  Gauge1.MaxValue := 0;

  qt := QVolNR_QTDE_VOL.AsInteger;
  i := 0;
  seq := 0;
  Gauge1.MaxValue := qt;
  while not QVol.eof do
  begin
    i := i + 1;

    Writeln(Arq,''+'n');
    Writeln(Arq,''+'M0690');
    Writeln(Arq,''+'L');
    Writeln(Arq,'D11');
    Writeln(Arq,'R0000');
    Writeln(Arq,'ySW1');
    Writeln(Arq,'A2');
    Writeln(Arq,'1911S0102520015P009P009INTECOM');
    Writeln(Arq,'1911S0102520149P009P00916'+DateTimeToStr(now)+' - 1');
    Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolRAZSOC.AsString,1,30));
    Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolCIDADE.value,1,25)+'/'+copy(QVolUF.AsString,1,2));
    Writeln(Arq,'1911S0101670301P009P009Volumes');
    Writeln(Arq,'1911S0101150015P022P022'+QVolNR_CONHECIMENTO.AsString);
    Writeln(Arq,'1911S0101250314P020P018'+QVolVOLUME.AsString+'/'+IntToStr(qt));
    Writeln(Arq,'1X1100002470015L346001');
    Writeln(Arq,'1X1100001870015L346001');
    Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolRAZSOC.AsString,1,30));
    Writeln(Arq,'1eA504500590020C'+QVolCOD_CONHECIMENTO.AsString + '&E-' + QVolVOLUME.AsString);
    Writeln(Arq,'1911S0100320019P007P008NF: '+ QCtenf.AsString);
    Writeln(Arq,'1X1100000460020L346001');
    Writeln(Arq,'1911S0101610015P014P014CT- e'+ nr_order);
    Writeln(Arq,'1911S0102520291P009P009CROSS');
    Writeln(Arq,'Q0001');
    Writeln(Arq,'E');

    QVol.Next;
  end;
  CloseFile(Arq);
  Screen.Cursor := crDefault;
  if impr = 'C:/SIM/printetiqueta.txt' then
    ShellExecute(Application.Handle, nil, PChar(impr),nil, nil, SW_SHOWNORMAL);
  Qvol.Close;
  QCte.Close;
  edCTe.Clear;
end;

procedure TfrmEtiquetaCross.btRelatorioClick(Sender: TObject);
begin
  Gauge1.MaxValue := 0;
  btnEtiqueta.Enabled := false;

  if edCTe.Value > 0 then
  begin
    QCte.Parameters[0].Value := edCte.Value;
    QCte.Parameters[1].Value := GLBFilial;
    QCte.Parameters[2].Value := edSerie.text;
    QCte.Open;
    if QCTe.RecordCount > 0 then
    begin
      edVolumes.Text := QCTeNR_QTDE_VOL.AsString;
      btnEtiqueta.Enabled := true;
    end
    else
      showmessage('Sem volumes para imprimir');
  end;
end;

procedure TfrmEtiquetaCross.QCTeCalcFields(DataSet: TDataSet);
var d, t : Integer;
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select notfis from tb_cte_nf where codcon = :0 and fl_empresa = :1');
  dtmdados.IQuery1.Parameters[0].Value := QCteNR_CONHECIMENTO.AsInteger;
  dtmdados.IQuery1.Parameters[1].Value := QCteFL_EMPRESA.AsInteger;
  dtmdados.IQuery1.Open;
  d := 0;
  t := 0;
  While not dtmdados.IQuery1.eof do
  begin
    d := d + 1;
    t := t + Length(dtmdados.IQuery1.FieldByName('notfis').AsString+'-');
    if t > 55 then
    begin
      QCtenf.Value := QCtenf.Value + '...';
      exit;
    end;
    if d = 1 then
      QCtenf.Value := dtmdados.IQuery1.FieldByName('notfis').AsString
    else
      QCtenf.Value := QCtenf.Value + '-'+ dtmdados.IQuery1.FieldByName('notfis').AsString;

    dtmdados.IQuery1.Next;
  end
end;

procedure TfrmEtiquetaCross.QVolBeforeOpen(DataSet: TDataSet);
begin
  QVol.Parameters[1].Value := GLBFilial;
end;

end.
