object frmPortariaSai: TfrmPortariaSai
  Left = 332
  Top = 256
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Portaria - Sa'#237'da de Ve'#237'culo'
  ClientHeight = 349
  ClientWidth = 895
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 769
    Height = 54
    TabOrder = 0
    object JvGradient1: TJvGradient
      Left = 1
      Top = 1
      Width = 767
      Height = 20
      Align = alTop
      Style = grVertical
      StartColor = 16245453
      EndColor = clBtnFace
      ExplicitWidth = 789
    end
    object Label1: TLabel
      Left = 182
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Coleta'
      Transparent = True
      Visible = False
    end
    object lblqt: TLabel
      Left = 234
      Top = 24
      Width = 6
      Height = 20
      Caption = ' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 13
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Tipo de Sa'#237'da'
      Transparent = True
    end
    object Label3: TLabel
      Left = 310
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Placa'
      Transparent = True
    end
    object btnGerar: TBitBtn
      Left = 552
      Top = 15
      Width = 101
      Height = 25
      Caption = 'Gerar'
      Enabled = False
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
        FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
        96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
        92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
        BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
        CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
        D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
        EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
        E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
        E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
        8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
        E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
        FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
        C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
        CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
        CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
        D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
        9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 0
      TabStop = False
      OnClick = btnGerarClick
    end
    object btnImprimir: TBitBtn
      Left = 659
      Top = 15
      Width = 101
      Height = 25
      Caption = 'Imprimir'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 1
      OnClick = btnImprimirClick
    end
    object cbTipo: TComboBox
      Left = 13
      Top = 27
      Width = 139
      Height = 21
      Style = csDropDownList
      TabOrder = 2
      OnChange = cbTipoExit
      OnExit = cbTipoExit
    end
    object edManifesto: TJvCalcEdit
      Left = 177
      Top = 24
      Width = 88
      Height = 28
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      Visible = False
      DecimalPlacesAlwaysShown = False
      OnExit = edManifestoExit
    end
    object edPlaca1: TJvMaskEdit
      Left = 310
      Top = 24
      Width = 118
      Height = 28
      EditMask = '!>LLL-0A00;0;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      TabOrder = 4
      Text = ''
      OnExit = edPlaca1Exit
    end
  end
  object Panel2: TPanel
    Left = -3
    Top = 54
    Width = 769
    Height = 285
    BevelOuter = bvNone
    TabOrder = 1
    object Label11: TLabel
      Left = 70
      Top = 135
      Width = 31
      Height = 16
      Caption = 'CPF :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 63
      Top = 20
      Width = 39
      Height = 16
      Caption = 'Placa :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 13
      Top = 256
      Width = 88
      Height = 16
      Caption = 'Kilometragem :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 92
      Width = 62
      Height = 16
      Caption = 'Motorista :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 10
      Top = 56
      Width = 91
      Height = 16
      Caption = 'Transportador :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 334
      Top = 135
      Width = 25
      Height = 16
      Caption = 'RG :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 556
      Top = 135
      Width = 33
      Height = 16
      Caption = 'CNH :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 26
      Top = 195
      Width = 76
      Height = 16
      Caption = 'Observa'#231#227'o :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 352
      Top = 20
      Width = 39
      Height = 16
      Caption = 'Frota :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblKm: TLabel
      Left = 225
      Top = 22
      Width = 121
      Height = 20
      Caption = '                        '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edKM: TJvCalcEdit
      Left = 132
      Top = 250
      Width = 127
      Height = 27
      Color = clWhite
      DecimalPlaces = 0
      DisplayFormat = ',0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
      OnExit = edKMExit
    end
    object edPlaca: TJvMaskEdit
      Left = 132
      Top = 14
      Width = 85
      Height = 27
      Color = 15132390
      EditMask = '!>LLL-0A00;0;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 2
      Text = ''
    end
    object edTransp: TJvMaskEdit
      Left = 132
      Top = 50
      Width = 461
      Height = 27
      Hint = 'Pressione F3 para Consultar'
      Color = 15132390
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Text = ''
    end
    object edMotor: TJvMaskEdit
      Left = 132
      Top = 89
      Width = 461
      Height = 27
      Hint = 'Pressione F3 para Consultar'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Text = ''
      OnExit = edMotorExit
    end
    object edCfp: TJvMaskEdit
      Left = 132
      Top = 128
      Width = 145
      Height = 27
      Color = clWhite
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Text = ''
    end
    object edRG: TJvMaskEdit
      Left = 380
      Top = 128
      Width = 145
      Height = 27
      Color = clWhite
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      Text = ''
    end
    object edCNH: TJvMaskEdit
      Left = 604
      Top = 128
      Width = 145
      Height = 27
      Color = clWhite
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      Text = ''
    end
    object Memo1: TMemo
      Left = 132
      Top = 178
      Width = 631
      Height = 55
      MaxLength = 300
      TabOrder = 1
    end
    object edFrota: TJvMaskEdit
      Left = 421
      Top = 14
      Width = 87
      Height = 27
      TabStop = False
      Color = 15132390
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
      Text = ''
    end
  end
  object Panel1: TPanel
    Left = 778
    Top = 0
    Width = 117
    Height = 349
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object dbgManifesto: TJvDBUltimGrid
      Left = -3
      Top = 1
      Width = 117
      Height = 337
      Ctl3D = False
      DataSource = dsTemp
      Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentCtl3D = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dbgManifestoCellClick
      OnDrawColumnCell = dbgManifestoDrawColumnCell
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 16
      TitleRowHeight = 16
      Columns = <
        item
          Expanded = False
          FieldName = 'Inserida'
          Width = 25
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Manifesto'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'serie'
          Visible = True
        end>
    end
  end
  object QSaida1: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select p.* from tb_portaria p where placa = :0 and dt_saida is n' +
        'ull and motivo = '#39'E'#39)
    Left = 464
    object QSaida1REG: TBCDField
      FieldName = 'REG'
      Precision = 32
      Size = 0
    end
    object QSaida1TIPO: TStringField
      FieldName = 'TIPO'
    end
    object QSaida1FL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
      Size = 0
    end
    object QSaida1PLACA: TStringField
      FieldName = 'PLACA'
      Size = 7
    end
    object QSaida1COD_TRANSP: TBCDField
      FieldName = 'COD_TRANSP'
      Precision = 32
      Size = 0
    end
    object QSaida1COD_MOTORISTA: TBCDField
      FieldName = 'COD_MOTORISTA'
      Precision = 32
      Size = 0
    end
    object QSaida1DATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QSaida1KM: TBCDField
      FieldName = 'KM'
      Precision = 32
      Size = 0
    end
    object QSaida1MANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QSaida1OBS: TStringField
      FieldName = 'OBS'
      Size = 300
    end
    object QSaida1USUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QSaida1MOTIVO: TStringField
      FieldName = 'MOTIVO'
      Size = 1
    end
    object QSaida1NM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      Size = 50
    end
    object QSaida1NM_MOTOR: TStringField
      FieldName = 'NM_MOTOR'
      Size = 50
    end
    object QSaida1NR_CPF: TStringField
      FieldName = 'NR_CPF'
      Size = 30
    end
    object QSaida1NR_RG: TStringField
      FieldName = 'NR_RG'
      Size = 30
    end
    object QSaida1NR_CNH: TStringField
      FieldName = 'NR_CNH'
      Size = 30
    end
    object QSaida1DT_SAIDA: TDateTimeField
      FieldName = 'DT_SAIDA'
    end
  end
  object iml: TImageList
    Left = 104
    Top = 288
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nm'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'SELECT t.id_spediteur as codigo '
      'FROM spediteure t '
      'where t.name = :nm')
    Left = 512
    object QTranspCODIGO: TStringField
      FieldName = 'CODIGO'
      Size = 12
    end
  end
  object Qmanifesto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QmanifestoBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '1'
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select * from ('
      
        'select distinct r.nr_romaneio, r.motorista, r.placa, r.veiculo, ' +
        'r.dtinc, t.name transportadora'
      
        'from tb_romaneio@wmswebprd r left join spediteure@wmswebprd t on' +
        ' to_char(r.cod_transp) = t.id_spediteur and t.lager = :0'
      
        '                             left join auftraege@wmswebprd p on ' +
        'p.percent = r.nr_romaneio'
      'where (p.opid_bearb is not null)'
      ''
      'and r.dtexclusao is null and dtsaida is null and r.tms is null'
      'and r.fl_empresa = :1'
      'union'
      
        'select distinct r.nr_romaneio, r.motorista, r.placa, r.veiculo, ' +
        'r.dtinc, t.name transportadora'
      
        'from tb_romaneio_wms r left join spediteure@wmsv11 t on to_char(' +
        'r.cod_transp) = t.id_spediteur and t.lager = :2'
      
        '                       left join auftraege@wmsv11 p on p.percent' +
        ' = r.nr_romaneio'
      'where (p.opid_bearb is not null)'
      ''
      'and r.dtexclusao is null and dtsaida is null and r.tms is null'
      'and r.fl_empresa = :3)'
      ''
      'order by 1')
    Left = 216
    Top = 8
    object QmanifestoNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QmanifestoMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      ReadOnly = True
      Size = 50
    end
    object QmanifestoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 7
    end
    object QmanifestoVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
    end
    object QmanifestoDTINC: TDateTimeField
      FieldName = 'DTINC'
      ReadOnly = True
    end
    object QmanifestoTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      ReadOnly = True
      Size = 30
    end
  end
  object dtsManifesto: TDataSource
    DataSet = Qmanifesto
    Left = 248
    Top = 8
  end
  object SPSaida: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_PORTARIA_SAIDA'
    Parameters = <
      item
        Name = 'VMAN'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = Null
      end
      item
        Name = 'VTIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VFORN'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VMOTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VEMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = Null
      end
      item
        Name = 'VKM'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = Null
      end
      item
        Name = 'VOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VUSER'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = -1
        Value = ''
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VSERIE'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 672
    Top = 104
  end
  object mdTemp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'Coleta'
        DataType = ftInteger
      end
      item
        Name = 'Inserida'
        DataType = ftInteger
      end
      item
        Name = 'reg'
        DataType = ftInteger
      end>
    Left = 792
    Top = 96
    object mdTempManifesto: TIntegerField
      FieldName = 'Manifesto'
    end
    object mdTempInserida: TIntegerField
      FieldName = 'Inserida'
    end
    object mdTempreg: TIntegerField
      FieldName = 'reg'
    end
    object mdTempserie: TStringField
      FieldName = 'serie'
      Size = 1
    end
    object mdTempvalido: TStringField
      FieldName = 'valido'
      Size = 1
    end
  end
  object dsTemp: TDataSource
    DataSet = mdTemp
    Left = 840
    Top = 96
  end
  object QMan: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QManBeforeOpen
    Parameters = <
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct m.manifesto nr_romaneio, mma.nomeab motorista, v' +
        '.numvei placa, f.descri veiculo, m.datainc dtinc, fma.nomeab tra' +
        'nsportadora, m.serie serman,'
      'case when m.mdfe_protocolo is null then '#39'N'#39' else '#39'S'#39' end MDFEAUT'
      
        'from tb_manifesto m left join cyber.rodmot mma on m.motorista = ' +
        'mma.codmot'
      
        '                    left join cyber.rodcli fma on m.transp = fma' +
        '.codclifor'
      
        '                    left join cyber.rodvei v on v.numvei = m.pla' +
        'ca'
      
        '                    left join cyber.rodfro f on v.codfro = f.cod' +
        'fro  left join tb_operacao op on  op.operacao = m.operacao and o' +
        'p.fl_receita = '#39'N'#39
      
        '                    left join wms.tb_portaria p on p.manifesto =' +
        ' m.manifesto and tipo = '#39'Manifesto (Rodopar)'#39' and p.serie = m.se' +
        'rie'
      'where m.status = '#39'S'#39' and op.fl_portaria = '#39'S'#39' and m.filial = :1'
      ''
      ''
      'and m.data_saida is null'
      ''
      'order by 1')
    Left = 216
    Top = 64
    object QManNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
    end
    object QManMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      ReadOnly = True
    end
    object QManPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QManVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 100
    end
    object QManDTINC: TDateTimeField
      FieldName = 'DTINC'
      ReadOnly = True
    end
    object QManTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      ReadOnly = True
    end
    object QManSERMAN: TStringField
      FieldName = 'SERMAN'
      ReadOnly = True
      Size = 3
    end
    object QManMDFEAUT: TStringField
      FieldName = 'MDFEAUT'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
  end
  object dtsMan: TDataSource
    DataSet = QMan
    Left = 248
    Top = 64
  end
  object QPallet: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '4'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '3'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select distinct xtipo, codean, serie, codman from ('
      
        'SELECT distinct '#39'R'#39' xtipo, r.codean, '#39'0'#39' serie, p.percent codman' +
        ', r.codclifor codpag, r.razsoc cliente '
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient  '
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'R'#39' xtipo, r.codean, '#39'0'#39' serie, p.percent codman' +
        ', r.codclifor codpag, r.razsoc cliente'
      
        'FROM cyber.RODCLI R left join klienten@wmsv11 k on (replace(repl' +
        'ace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegriff'
      
        '                    left join auftraege@wmsv11 p on p.id_klient ' +
        '= k.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'M'#39' xtipo, r.codean, n.serie_man serie, n.nr_man' +
        'ifesto codman, r.codclifor codpag, r.razsoc cliente '
      'FROM cyber.RODCLI R, tb_conhecimento N  '
      'WHERE n.filial_man = :0'
      'and n.cod_pagador = R.CODCLIFOR'
      'and r.codean = '#39'PALLET'#39')'
      
        'left join tb_controle_pallet pp on codman = pp.manifesto and pp.' +
        'filial = :1'
      'where codman = :2'
      'and serie = :3'
      'and xtipo = :4'
      'and pp.id is null'
      '')
    Left = 460
    Top = 284
    object QPalletCODEAN: TStringField
      FieldName = 'CODEAN'
      ReadOnly = True
      Size = 13
    end
    object QPalletSERIE: TStringField
      FieldName = 'SERIE'
      ReadOnly = True
      Size = 3
    end
    object QPalletCODMAN: TBCDField
      FieldName = 'CODMAN'
      ReadOnly = True
      Precision = 32
    end
  end
  object QSMS: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select fnc_cnpj(f.codcgc) codcgc, ('#39'int-'#39' || F_LIMPA_MASCARA(m.p' +
        'laca)) usuario, m.manifesto pedido, a.nr_ctrc, a.serie, a.destin' +
        'o, a.endere, a.numero, a.codcep, nvl(mot.TELCEL,'#39'0000000000'#39') te' +
        'lcel, p.reg'
      
        'from vw_manifesto_ctrc_sim a left join tb_manifesto m on m.manif' +
        'esto = a.nr_manifesto and m.filial = a.fl_empresa and m.serie = ' +
        'a.serie'
      
        '                             left join cyber.rodmot mot on mot.c' +
        'odmot = m.motorista'
      
        '                             left join cyber.rodfil f on f.codfi' +
        'l = a.filial_doc'
      
        '                             left join tb_portaria p on p.manife' +
        'sto = m.manifesto and p.fl_empresa = m.filial and p.serie = m.se' +
        'rie and p.motivo = '#39'S'#39
      
        '                             left join cyber.rodcli tr on m.tran' +
        'sp = tr.codclifor'
      
        '                             left join cyber.RODCMO ag on ag.cod' +
        'cmo = tr.codcmo'
      
        'where ag.codcmo in(10,36) and m.manifesto = :0 and m.filial = :1' +
        ' and m.serie = :2'
      'and fnc_cnpj(f.codcgc) = :3'
      'order by 1, a.nr_ctrc')
    Left = 456
    Top = 82
    object QSMSCODCGC: TStringField
      FieldName = 'CODCGC'
    end
    object QSMSUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
      FixedChar = True
      Size = 13
    end
    object QSMSPEDIDO: TFMTBCDField
      FieldName = 'PEDIDO'
      Precision = 38
      Size = 0
    end
    object QSMSNR_CTRC: TFMTBCDField
      FieldName = 'NR_CTRC'
      Precision = 38
      Size = 4
    end
    object QSMSSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QSMSDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object QSMSENDERE: TStringField
      FieldName = 'ENDERE'
      Size = 80
    end
    object QSMSNUMERO: TFMTBCDField
      FieldName = 'NUMERO'
      Precision = 38
      Size = 0
    end
    object QSMSCODCEP: TStringField
      FieldName = 'CODCEP'
      Size = 10
    end
    object QSMSTELCEL: TStringField
      FieldName = 'TELCEL'
      ReadOnly = True
      Size = 15
    end
    object QSMSREG: TFMTBCDField
      FieldName = 'REG'
      Precision = 38
      Size = 0
    end
  end
  object QEnvia: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT distinct R.RAZSOC nm_emp_dest_ta, nf.notfis nr_nf, nf.qua' +
        'nti nr_quantidade, r.inffat ds_email, r.PERHOR fl_tempo_per'
      'FROM RODIMR, RODCLI R, RODCON N,  RODNFC nf  '
      
        'WHERE RODIMR.CODMAN = :0 AND RODIMR.FILMAN = :1 AND RODIMR.TIPDO' +
        'C = '#39'C'#39' AND RODIMR.FILDOC = 1 AND RODIMR.SERDOC = '#39'1'#39
      'and RODIMR.CODDOC = N.CODCON '
      'AND n.CODDES = R.CODCLIFOR AND RODIMR.NOTFIS = Nf.NOTFIS  '
      
        'AND RODIMR.CODDOC=Nf.CODCON  AND RODIMR.SERDOC=Nf.SERCON  AND RO' +
        'DIMR.FILDOC=Nf.CODFIL '
      'and r.inffat is not null'
      'and r.PERHOR is not null')
    Left = 528
    Top = 284
    object QEnviaNM_EMP_DEST_TA: TStringField
      FieldName = 'NM_EMP_DEST_TA'
      Size = 50
    end
    object QEnviaNR_QUANTIDADE: TFloatField
      FieldName = 'NR_QUANTIDADE'
    end
    object QEnviaNR_NF: TStringField
      FieldName = 'NR_NF'
      ReadOnly = True
    end
    object QEnviaFL_TEMPO_PER: TStringField
      FieldName = 'FL_TEMPO_PER'
      ReadOnly = True
      Size = 7
    end
    object QEnviaDS_EMAIL: TStringField
      FieldName = 'DS_EMAIL'
      ReadOnly = True
      Size = 4000
    end
  end
  object ADOQPallet_CTE: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '4'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct xtipo, codcgc, codclifor, serie, xmanifesto from' +
        ' ('
      
        'SELECT distinct '#39'R'#39' xtipo, d.suchbegriff codcgc,  r.codclifor, '#39 +
        '0'#39' serie, percent xmanifesto'
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient'
      
        '                    left join kunden@wmswebprd d on d.id_kunde =' +
        ' p.id_kunde_ware  and p.id_klient = d.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'R'#39' xtipo, d.suchbegriff codcgc,  r.codclifor, '#39 +
        '0'#39' serie, percent xmanifesto'
      
        'FROM cyber.RODCLI R left join klienten@wmsv11 k on (replace(repl' +
        'ace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegriff'
      
        '                    left join auftraege@wmsv11 p on p.id_klient ' +
        '= k.id_klient'
      
        '                    left join kunden@wmsv11 d on d.id_kunde = p.' +
        'id_kunde_ware  and p.id_klient = d.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'M'#39' xtipo, r.codcgc,  c.codclifor, m.serie, m.ma' +
        'nifesto xmanifesto'
      
        'FROM tb_manitem m inner join tb_conhecimento N on m.CODDOC = N.n' +
        'r_conhecimento and m.fildoc = n.fl_empresa'
      
        '                  left join cyber.RODCLI R on N.cod_destinatario' +
        ' = R.CODCLIFOR'
      
        '                  left join cyber.RODCLI C on N.cod_pagador = C.' +
        'CODCLIFOR'
      'WHERE m.filial = :0 and c.codean = '#39'PALLET'#39')'
      
        'left join tb_controle_pallet pp on xmanifesto = pp.manifesto and' +
        ' pp.filial = :1'
      'where xmanifesto = :2'
      'and serie = :3'
      'and xtipo = :4'
      'and pp.id is null'
      '')
    Left = 392
    Top = 284
    object ADOQPallet_CTECODCGC: TStringField
      FieldName = 'CODCGC'
      ReadOnly = True
    end
    object ADOQPallet_CTECODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
  end
  object Sp_SaidaSMS: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_PORTARIA_SAIDA_SMS'
    Parameters = <
      item
        Name = 'VMANIFESTO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VUSUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 672
    Top = 152
  end
  object QArquivoBaruel: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select ex.notfis, ex.serien, cli.codcgc, ex.datainc, ex.operacao' +
        ', ex.nr_manifesto, ex.serie from vw_expedicao ex inner join'
      'cyber.rodcli cli on ex.codrem = cli.codclifor '
      'where nr_manifesto = :0 and serie =:1 and codrem = 23049')
    Left = 696
    Top = 284
    object QArquivoBaruelNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QArquivoBaruelSERIEN: TStringField
      FieldName = 'SERIEN'
      Size = 3
    end
    object QArquivoBaruelCODCGC: TStringField
      FieldName = 'CODCGC'
    end
    object QArquivoBaruelDATAINC: TDateTimeField
      FieldName = 'DATAINC'
    end
    object QArquivoBaruelOPERACAO: TStringField
      FieldName = 'OPERACAO'
    end
    object QArquivoBaruelNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QArquivoBaruelSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
  end
  object QArquivoBaruelR: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select distinct r.nr_romaneio, p.nr_auf, p.nr_auf_ta, kl.name,'
      ' kl.suchbegriff from tb_romaneio r left join auftraege p on '
      'p.percent = r.nr_romaneio left join klienten kl'
      'on p.id_klient = kl.id_klient where percent is not null and '
      'p.ID_KLIENT = '#39'BNE'#39' and r.nr_romaneio =:0')
    Left = 216
    Top = 216
    object QArquivoBaruelRNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QArquivoBaruelRNR_AUF: TStringField
      FieldName = 'NR_AUF'
      ReadOnly = True
      Size = 12
    end
    object QArquivoBaruelRNR_AUF_TA: TIntegerField
      FieldName = 'NR_AUF_TA'
      ReadOnly = True
    end
    object QArquivoBaruelRNAME: TStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 40
    end
    object QArquivoBaruelRSUCHBEGRIFF: TStringField
      FieldName = 'SUCHBEGRIFF'
      ReadOnly = True
      Size = 30
    end
  end
  object Qverificaromaneio: TADOQuery
    Connection = dtmDados.ConWmsWeb
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM TB_ROMANEIO WHERE nvl(fl_arq,'#39' '#39') <> '#39'S'#39' and nr_ro' +
        'maneio =:0')
    Left = 216
    Top = 184
  end
  object QUpdateRomaneio: TADOQuery
    Connection = dtmDados.ConWmsWeb
    Parameters = <
      item
        Name = '0'
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'update tb_romaneio set fl_arq = '#39'S'#39', arquivo_retorno = :0 where ' +
        'nr_romaneio =:1')
    Left = 216
    Top = 152
  end
  object QAverba: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select cte_chave, dt_conhecimento, cod_conhecimento, fl_empresa'
      'from tb_conhecimento'
      'where nr_manifesto = :0'
      'and filial_man = :1'
      'and serie_man = :2'
      'and fl_status = '#39'A'#39' and cte_chave is not null'
      'and averba_protocolo is null'
      'and cte_data >= (sysdate-60)')
    Left = 548
    Top = 84
    object QAverbaCTE_CHAVE: TStringField
      FieldName = 'CTE_CHAVE'
      Size = 44
    end
    object QAverbaDT_CONHECIMENTO: TDateTimeField
      FieldName = 'DT_CONHECIMENTO'
    end
    object QAverbaCOD_CONHECIMENTO: TBCDField
      FieldName = 'COD_CONHECIMENTO'
      Precision = 32
      Size = 0
    end
    object QAverbaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
      Size = 0
    end
  end
  object QXML: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct cte_chave, t.dt_emissao'
      
        'from vw_manifesto_detalhe t left join tb_paramtransp p on p.codt' +
        'ransp = t.cod_transp'
      'where t.nr_manifesto = :0'
      'and t.serie = :1'
      'and t.fl_empresa = :2'
      'and cte_chave is not null and p.tipo = '#39'TRANSPORTADOR'#39
      'order by 1'
      '')
    Left = 580
    Top = 285
    object QXMLCTE_CHAVE: TStringField
      FieldName = 'CTE_CHAVE'
      ReadOnly = True
      Size = 44
    end
    object QXMLDT_EMISSAO: TDateTimeField
      FieldName = 'DT_EMISSAO'
      ReadOnly = True
    end
  end
  object Qemail: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct e.email'
      
        'from vw_manifesto_detalhe t left join cyber.rodctc e on e.codcli' +
        'for = t.cod_transp'
      
        '                            left join tb_paramtransp p on p.codt' +
        'ransp = e.codclifor'
      'where t.nr_manifesto = :0'
      'and t.serie = :1'
      'and t.fl_empresa = :2'
      'and cte_chave is not null and p.tipo = '#39'TRANSPORTADOR'#39
      'order by 1'
      '')
    Left = 628
    Top = 285
    object QemailEMAIL: TStringField
      FieldName = 'EMAIL'
      ReadOnly = True
      Size = 80
    end
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 
      'https://app.portaldotransportador.com/api/v1/nfe/CheckoutNfTrans' +
      'porte/post'
    Params = <>
    RaiseExceptionOn500 = False
    Left = 808
    Top = 248
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Method = rmPOST
    Params = <
      item
        Kind = pkHTTPHEADER
        Name = 'apikey'
        Value = '78asd4546d4sa687e1d1xzlcknhwyhuWMKPSJDpox8213njdOWnxxipW58547'
      end
      item
        Name = 'Auth_busca'
        Value = 'bb68093b11f2476491a8558a9a248a70'
      end
      item
        Name = 'cnpj'
        Value = '03857930000740'
      end
      item
        Name = 'usuario'
        Value = 'int-motorista'
      end
      item
        Name = 'dados[0][nfe]'
        Value = '139630'
      end
      item
        Name = 'dados[0][serie]'
        Value = '1'
      end
      item
        Name = 'dados[0][nome_cliente]'
        Value = 'REAL MOTO PECAS LTDA '
      end
      item
        Name = 'dados[0][destino_endereco]'
        Value = 'AVENIDA PEDRO LUDOVICO TEIXEIRA'
      end
      item
        Name = 'dados[0][numero_entrega]'
        Value = '3499'
      end
      item
        Name = 'dados[0][cep_entrega]'
        Value = '74375-750'
      end
      item
        Name = 'dados[0][celular]'
        Value = '0000000000'
      end
      item
        Name = 'dados[0][pedido]'
        Value = '2638'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 808
    Top = 200
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'application/json'
    Left = 808
    Top = 152
  end
  object QPortal: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct fnc_cnpj(f.codcgc) codcgc, ('#39'int-'#39' || F_LIMPA_MA' +
        'SCARA(m.placa)) usuario'
      
        'from vw_manifesto_ctrc_sim a left join tb_manifesto m on m.manif' +
        'esto = a.nr_manifesto and m.filial = a.fl_empresa and m.serie = ' +
        'a.serie'
      
        '                             left join cyber.rodfil f on f.codfi' +
        'l = a.filial_doc'
      
        '                             left join tb_portaria p on p.manife' +
        'sto = m.manifesto and p.fl_empresa = m.filial and p.serie = m.se' +
        'rie and p.motivo = '#39'S'#39
      
        '                             left join cyber.rodcli tr on m.tran' +
        'sp = tr.codclifor'
      
        '                             left join cyber.RODCMO ag on ag.cod' +
        'cmo = tr.codcmo'
      'where p.portal = '#39'N'#39' and ag.codcmo in(10,36)'
      'and m.manifesto = :0 and m.filial = :1 and m.serie = :2'
      'order by 1')
    Left = 456
    Top = 142
    object QPortalCODCGC: TStringField
      FieldName = 'CODCGC'
      ReadOnly = True
      Size = 4000
    end
    object QPortalUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
      Size = 4000
    end
  end
end
