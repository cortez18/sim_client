unit LiberaPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TfrmLiberaPagto = class(TForm)
    Panel9: TPanel;
    Label9: TLabel;
    edMotivo: TEdit;
    btnSalva_motivo: TBitBtn;
    Label1: TLabel;
    edValor: TJvCalcEdit;
    procedure btnSalva_motivoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLiberaPagto: TfrmLiberaPagto;

implementation

uses Dados, RelCusto_frete_sim;

{$R *.dfm}

procedure TfrmLiberaPagto.btnSalva_motivoClick(Sender: TObject);
begin
  if trim(edMotivo.Text) = '' then
  begin
    ShowMessage('� necess�rio digitar mo motivo');
    edMotivo.setfocus;
  end;
  self.Tag := 0;
  if edValor.Value = 0 then
  begin
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add('update tb_controle_custo set liberacao = :0 ');
    dtmdados.IQuery1.sql.add
      ('where nr_doc = :1 and tipo = :2 and codclifor = :3 ');
    dtmdados.IQuery1.sql.add('and filial = :4 and cte_rec = :5');
    dtmdados.IQuery1.Parameters[0].Value := edMotivo.Text;
    dtmdados.IQuery1.Parameters[1].Value :=
      frmRelCusto_frete_sim.mdRepredoc.AsInteger;
    dtmdados.IQuery1.Parameters[2].Value :=
      frmRelCusto_frete_sim.mdRepreTipo.Value;
    dtmdados.IQuery1.Parameters[3].Value :=
      frmRelCusto_frete_sim.mdRepreTransp.AsInteger;
    dtmdados.IQuery1.Parameters[4].Value :=
      frmRelCusto_frete_sim.mdRepreCodfil.AsInteger;
    dtmdados.IQuery1.Parameters[5].Value :=
      frmRelCusto_frete_sim.mdRepreCodcon.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
    self.Tag := 10;
  end
  else
  begin
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.add
      ('update tb_controle_custo set liberacao = :0, novo_custo = :1 ');
    dtmdados.IQuery1.sql.add
      ('where nr_doc = :2 and tipo = :3 and codclifor = :4 ');
    dtmdados.IQuery1.sql.add('and filial = :5 and cte_rec = :6');
    dtmdados.IQuery1.Parameters[0].Value := edMotivo.Text;
    dtmdados.IQuery1.Parameters[1].Value := edValor.Value;
    dtmdados.IQuery1.Parameters[2].Value :=
      frmRelCusto_frete_sim.mdRepredoc.AsInteger;
    dtmdados.IQuery1.Parameters[3].Value :=
      frmRelCusto_frete_sim.mdRepreTipo.Value;
    dtmdados.IQuery1.Parameters[4].Value :=
      frmRelCusto_frete_sim.mdRepreTransp.AsInteger;
    dtmdados.IQuery1.Parameters[5].Value :=
      frmRelCusto_frete_sim.mdRepreCodfil.AsInteger;
    dtmdados.IQuery1.Parameters[6].Value :=
      frmRelCusto_frete_sim.mdRepreCodcon.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
    self.Tag := 10;
  end;

  Close;
end;

procedure TfrmLiberaPagto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if trim(edMotivo.Text) = '' then
  begin
    ShowMessage('� Necess�rio Digitar o Motivo');
    edMotivo.setfocus;
    Action := caNone;
  end
  else
    Action := caFree;
end;

procedure TfrmLiberaPagto.FormCreate(Sender: TObject);
begin
  edMotivo.Text := frmRelCusto_frete_sim.mdRepreliberacao.Value;
  edValor.value := frmRelCusto_frete_sim.mdReprenovo_custo.Value;
end;

end.
