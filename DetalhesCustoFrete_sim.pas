unit DetalhesCustoFrete_sim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, JvBaseEdits, Buttons, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, DB, ADODB;

type
  TfrmDetalhesCustoFrete_sim = class(TForm)
    Panel2: TPanel;
    JvDBGrid1: TJvDBGrid;
    QManifesto: TADOQuery;
    dtsManifesto: TDataSource;
    QRomaneio: TADOQuery;
    dtsRomaneio: TDataSource;
    QDocViagem: TADOQuery;
    dtsViagem: TDataSource;
    QDocFracR: TADOQuery;
    dtsFracR: TDataSource;
    QDocViagemLOCALD: TStringField;
    QDocViagemUFD: TStringField;
    QDocViagemCODMUN: TBCDField;
    QDocViagemDESCRI: TStringField;
    QDocFracRVLRMER: TBCDField;
    QDocFracRPESCAL: TBCDField;
    QRomaneioVEICULO: TStringField;
    QRomaneioLOCALO: TStringField;
    QRomaneioUFO: TStringField;
    QManifestoVEICULO: TStringField;
    QManifestoLOCALO: TStringField;
    QManifestoUFO: TStringField;
    Panel1: TPanel;
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    QDocFracRUFD: TStringField;
    QDocFracRCODMUN: TBCDField;
    QDocFracRLOCALD: TStringField;
    QDocFracM: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    StringField1: TStringField;
    BCDField3: TBCDField;
    StringField2: TStringField;
    dtsFracM: TDataSource;
    QManifestoOPERACAO: TStringField;
    QManifestoDESCRI: TStringField;
    QManifestoFL_PESO: TStringField;
    QRomaneioOPERACAO: TStringField;
    QRomaneioDESCRI: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDetalhesCustoFrete_sim: TfrmDetalhesCustoFrete_sim;

implementation

uses RelCusto_frete_sim;

{$R *.dfm}

procedure TfrmDetalhesCustoFrete_sim.FormActivate(Sender: TObject);
begin
  // manifesto
  if self.tag = 1 then
  begin
    JvDBGrid1.DataSource := dtsManifesto;
    QManifesto.close;
    QManifesto.Parameters[0].value := frmRelCusto_frete_sim.mdRepreCodcon.AsInteger;
    QManifesto.Parameters[1].value := frmRelCusto_frete_sim.mdRepreCodfil.AsInteger;
    QManifesto.Parameters[2].value := frmRelCusto_frete_sim.mdRepreTransp.AsInteger;
    QManifesto.Parameters[3].value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
    QManifesto.Parameters[4].value := frmRelCusto_frete_sim.mdRepreserie.AsString;
    QManifesto.open;
    if QManifestofl_peso.value = 'S' then
    begin
      JvDBGrid2.Visible := false;
      JvDBGrid3.DataSource := dtsFracM;
      JvDBGrid3.Visible := true;
      QDocFracM.close;
      QDocFracM.Parameters[0].value := frmRelCusto_frete_sim.mdRepreCodcon.AsInteger;
      QDocFracM.Parameters[1].value := frmRelCusto_frete_sim.mdRepreCodfil.AsInteger;
      QDocFracM.Parameters[2].value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
      QDocFracM.Open;
    end
    else
    begin
      JvDBGrid2.Visible := true;
      JvDBGrid3.Visible := false;
      QDocViagem.close;
      QDocViagem.Parameters[0].value := frmRelCusto_frete_sim.mdRepreTransp.AsInteger;
      QDocViagem.Parameters[1].value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
      QDocViagem.Parameters[2].value := frmRelCusto_frete_sim.mdRepreFilial.AsInteger;
      QDocViagem.Parameters[3].value := frmRelCusto_frete_sim.mdRepreserie.AsString;
      QDocViagem.Open;
    end
  end
  else
  // romaneio
  begin
    JvDBGrid1.DataSource := dtsRomaneio;
    JvDBGrid2.Visible := false;
    JvDBGrid3.DataSource := dtsFracR;
    JvDBGrid3.Visible := true;
    QRomaneio.close;
    QRomaneio.Parameters[0].value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
    QRomaneio.Parameters[1].value := frmRelCusto_frete_sim.mdRepreFilial.AsInteger;
    QRomaneio.open;
    QDocFracR.close;
    QDocFracR.Parameters[0].value := frmRelCusto_frete_sim.mdRepreCodcon.AsInteger;
    QDocFracR.Parameters[1].value := frmRelCusto_frete_sim.mdRepreCodfil.AsInteger;
    QDocFracR.Parameters[2].value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
    QDocFracR.Open;
  end;
end;

procedure TfrmDetalhesCustoFrete_sim.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QManifesto.close;
  QRomaneio.Close;
  QDocViagem.close;
  QDocFracR.close;
  QDocFracM.close;
end;

end.
