unit CadCidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, JvCombobox,
  System.ImageList, System.Actions;

type
  TfrmCadCidade = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QOperacao: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    cbFluvial: TJvCheckBox;
    QOperacaoCODIGO: TBCDField;
    QOperacaoDESCRI: TStringField;
    QOperacaoESTADO: TStringField;
    QOperacaoDATATU: TDateTimeField;
    QOperacaoUSUATU: TBCDField;
    QOperacaoSTATUS: TStringField;
    QOperacaoCDIBGE: TBCDField;
    QOperacaoFLUVIA: TStringField;
    Panel1: TPanel;
    Label17: TLabel;
    Label16: TLabel;
    btProcurar: TBitBtn;
    QCidade: TADOQuery;
    QCidadeCODIGO: TBCDField;
    QCidadeDESCRI: TStringField;
    cbPesqUFD: TComboBox;
    cbCidade: TComboBox;
    QOperacaoregiao: TStringField;
    cbRegiao: TJvComboBox;
    QOperacaoCODIPV: TStringField;
    Qalteracidade: TADOQuery;
    QalteracidadeCODIGO: TFMTBCDField;
    QalteracidadeDESCRI: TStringField;
    QalteracidadeESTADO: TStringField;
    QalteracidadeDATATU: TDateTimeField;
    QalteracidadeUSUATU: TFMTBCDField;
    QalteracidadeSTATUS: TStringField;
    QalteracidadeCDIBGE: TFMTBCDField;
    QalteracidadeFLUVIA: TStringField;
    QalteracidadeREGIAO: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure dbgContratoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure cbPesqUFDChange(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadCidade: TfrmCadCidade;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadCidade.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadCidade.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  cbFluvial.Checked := false;
  cbRegiao.ItemIndex := 1;
  cbFluvial.SetFocus;
end;

procedure TfrmCadCidade.btnSalvarClick(Sender: TObject);
var cod: Integer;
begin
  cod := 0;
  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add
        ('select nvl(max(codigo),0) + 1 codigo from tb_cidade');
      dtmDados.IQuery1.open;

      Qalteracidade.Close;
      Qalteracidade.SQL.Clear;
      Qalteracidade.SQL.Add('select c.* from tb_cidade c ');
      Qalteracidade.Open;
      Qalteracidade.append;
      QalteracidadeCODIGO.AsInteger := dtmDados.IQuery1.FieldByName('codigo').value;
    end
    else
    begin
      cod := QOperacaoCODIGO.AsInteger;
      Qalteracidade.Close;
      Qalteracidade.SQL.Clear;
      Qalteracidade.SQL.Add('select c.* from tb_cidade c where c.codigo = '+  QuotedStr(QOperacaoCODIGO.AsString));
      Qalteracidade.Open;
      Qalteracidade.Edit;
    end;

    if cbFluvial.Checked = true then
      QalteracidadeFLUVIA.value := 'S'
    else
      QalteracidadeFLUVIA.value := 'N';

    if cbRegiao.text <> '' then
      Qalteracidaderegiao.value := cbRegiao.text
    else
      Qalteracidaderegiao.value := '';

    Qalteracidade.post;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  Qalteracidade.Close;
  QOperacao.Close;
  QOperacao.open;
  if tipo = 'A' then
    Qoperacao.Locate('codigo',cod,[]);
  tipo := '';
end;

procedure TfrmCadCidade.btProcurarClick(Sender: TObject);
begin
  if cbCidade.text <> '' then
  begin
    QCidade.locate('Descri', cbCidade.text, []);
    QOperacao.locate('Codigo', QCidadeCODIGO.AsInteger, []);
  end
  else
  begin
    QOperacao.Close;
    QOperacao.open;
  end;
end;

procedure TfrmCadCidade.cbPesqUFDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFD.text;
  QCidade.open;
  cbCidade.clear;
  cbCidade.Items.add('');
  while not QCidade.eof do
  begin
    cbCidade.Items.add(QCidadeDESCRI.AsString);
    QCidade.next;
  end;
end;

procedure TfrmCadCidade.dbgContratoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.Field = QOperacaoFLUVIA then
  begin
    dbgContrato.Canvas.FillRect(Rect);
    iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QOperacaoFLUVIA.value = 'N' then
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      iml.Draw(dbgContrato.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
end;

procedure TfrmCadCidade.dbgContratoTitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QOperacao.locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadCidade.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QOperacao.Cancel;
end;

procedure TfrmCadCidade.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';

  if QOperacaoFLUVIA.value = 'S' then
    cbFluvial.Checked := true
  else
    cbFluvial.Checked := false;

  if QOperacaoregiao.value <> '' then
    cbRegiao.ItemIndex := cbRegiao.Items.indexof(QOperacaoregiao.AsString)
  else
    cbRegiao.ItemIndex := -1;

  cbFluvial.SetFocus;
end;

procedure TfrmCadCidade.FormCreate(Sender: TObject);
begin
  QOperacao.open;
  quant := QOperacao.RecordCount;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadCidade.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QOperacao.Close;
  QCidade.Close;
end;

procedure TfrmCadCidade.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
    pnBotao.Visible := false
  else
    pnBotao.Visible := true;
end;

procedure TfrmCadCidade.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QOperacao.First;
    2:
      QOperacao.Prior;
    3:
      QOperacao.next;
    4:
      QOperacao.Last;
  end;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
