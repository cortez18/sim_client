object frmGeraRoma_wmsclient: TfrmGeraRoma_wmsclient
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Gerar Romaneio - WMS Client'
  ClientHeight = 601
  ClientWidth = 951
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 229
    Height = 601
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 229
      Height = 121
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object lblDescricao: TLabel
        Left = 6
        Top = 52
        Width = 57
        Height = 13
        Caption = 'Procurar NF'
        Transparent = True
      end
      object Label5: TLabel
        Left = 13
        Top = 31
        Width = 32
        Height = 13
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 27
        Top = 7
        Width = 18
        Height = 13
        Caption = 'Site'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edValor: TEdit
        Left = 6
        Top = 71
        Width = 110
        Height = 21
        TabOrder = 1
        OnExit = edValorExit
      end
      object ckRota: TCheckBox
        Left = 6
        Top = 98
        Width = 135
        Height = 17
        Caption = 'Por Transporador'
        TabOrder = 2
        OnClick = ckRotaClick
      end
      object cbCliente: TComboBox
        Left = 57
        Top = 28
        Width = 166
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = cbClienteChange
        OnExit = cbClienteExit
      end
      object cbTodos: TCheckBox
        Left = 147
        Top = 98
        Width = 64
        Height = 17
        Caption = 'Todos'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = cbTodosClick
      end
      object cbSite: TComboBox
        Left = 59
        Top = 1
        Width = 74
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnChange = cbSiteChange
        Items.Strings = (
          'MAR'
          'PBA'
          'BTM'
          'BRR'
          '')
      end
    end
    object pnTarefas: TPanel
      Left = 0
      Top = 121
      Width = 229
      Height = 480
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
      object pgTarefas: TPageControl
        Left = 1
        Top = 1
        Width = 227
        Height = 451
        ActivePage = tbRota
        Align = alClient
        TabOrder = 0
        object tbRota: TTabSheet
          Caption = 'Rota'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object dgRota: TJvDBUltimGrid
            Left = 0
            Top = 0
            Width = 219
            Height = 423
            Align = alClient
            Ctl3D = False
            DataSource = dsRota
            Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentCtl3D = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 16
            Columns = <
              item
                Expanded = False
                FieldName = 'NAME'
                Title.Caption = 'Rota'
                Visible = True
              end>
          end
        end
        object tbTarefa: TTabSheet
          Caption = 'Notas Fiscais'
          ImageIndex = 1
          OnShow = tbTarefaShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object dbgColeta: TJvDBUltimGrid
            Left = 0
            Top = 0
            Width = 219
            Height = 423
            Align = alClient
            Ctl3D = False
            DataSource = dsTemp
            Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentCtl3D = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnCellClick = dbgColetaCellClick
            OnDrawColumnCell = dbgColetaDrawColumnCell
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 16
            Columns = <
              item
                Expanded = False
                FieldName = 'pedido'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Inserida'
                Width = 20
                Visible = True
              end>
          end
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 452
        Width = 227
        Height = 27
        Align = alBottom
        TabOrder = 1
        object Label2: TLabel
          Left = 12
          Top = 6
          Width = 44
          Height = 13
          Caption = 'Pedidos :'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 229
    Top = 0
    Width = 722
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 722
      Height = 601
      ActivePage = tbImpresso
      Align = alClient
      TabOrder = 0
      object TabSheet4: TTabSheet
        Caption = 'Notas Fiscais'
        ImageIndex = 3
        OnShow = TabSheet4Show
        object lbTareFas_romaneio: TLabel
          Left = 8
          Top = 3
          Width = 6
          Height = 16
          Caption = '  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dgTarefa: TJvDBUltimGrid
          Left = 0
          Top = 31
          Width = 714
          Height = 481
          Align = alClient
          Ctl3D = False
          DataSource = ds
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'PERCENT'
              Title.Alignment = taCenter
              Title.Caption = 'Romaneio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_AUF'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Nota Fiscal'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME'
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TRANSPORTADORA'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Transportadora'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEQ_TOUR'
              Title.Alignment = taCenter
              Title.Caption = 'Volumes'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 105
              Visible = True
            end>
        end
        object Panel4: TPanel
          Left = 0
          Top = 512
          Width = 714
          Height = 61
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 1
          object Label16: TLabel
            Left = 8
            Top = 9
            Width = 67
            Height = 13
            Caption = 'Total Volumes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edTotVolumeTarefa: TJvCalcEdit
            Left = 8
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 714
          Height = 31
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 2
          object pnBotao: TPanel
            Left = 1
            Top = 1
            Width = 455
            Height = 29
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object btRomaneio: TBitBtn
              Left = 289
              Top = 2
              Width = 118
              Height = 25
              Hint = 'Gerar Romaneio a partir de Tarefas Selecionadas'
              Caption = 'Gerar Romaneio'
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
                FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
                96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
                92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
                BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
                CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
                D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
                EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
                E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
                E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
                8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
                E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
                FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
                C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
                CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
                CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
                D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
                9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              TabStop = False
              OnClick = btRomaneioClick
            end
            object btnCarregar_Tarefas: TBitBtn
              Left = 40
              Top = 2
              Width = 111
              Height = 25
              Hint = 'Carrega Tarefas para Gerar Romaneio'
              Caption = 'Carregar'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF0051A6
                58B84FA356AEFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00418E4517408D43EC3F8B42B3FF00FF0054AB5CB369B3
                6FFF67B16DFF4FA456AEFF00FF004C9F52FFFF00FF00FF00FF00FF00FF00FF00
                FF00459449FF43924817429046F15CA160FF569C5AFF3F8C42B856AD5FEC71B8
                77FF83BF89FF68B16EFF50A457FF4EA255FFFF00FF00FF00FF00FF00FF00FF00
                FF0046964BFF4A974EFF5FA462FF71B075FF579E5BFF408D44AE58B0611757AE
                5FF171B877FF74B87BFF80BD86FF50A457FFFF00FF00FF00FF00FF00FF00FF00
                FF0048994DFF73B377FF63A968FF5AA15EFF429147AEFF00FF00FF00FF0058B0
                61175CB064FF84C18AFF86C18BFF52A759FFFF00FF00FF00FF00FF00FF00FF00
                FF004A9B4FFF79B67EFF74B379FF45954AFFFF00FF00FF00FF00FF00FF005AB3
                63FF59B161FF57AE5FFF55AC5DFF54AA5BFFFF00FF00FF00FF00FF00FF00FF00
                FF004B9E51FF4A9C50FF48994EFF47974CFF45954AFFFF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0062BF
                6DFF61BD6CFF60BB6AFF5EB968FF5DB767FFFF00FF00FF00FF00FF00FF00FF00
                FF0055AB5DFF53A95BFF52A759FF50A457FF4EA255FFFF00FF00FF00FF00FF00
                FF0063BF6DFF97D19FFF98D0A0FF5FB969FFFF00FF00FF00FF00FF00FF00FF00
                FF0057AE5FFF8CC692FF87C38DFF57AA5EFF50A55717FF00FF00FF00FF0065C3
                70AE7ECA87FF8ECD98FF97D19FFF60BC6BFFFF00FF00FF00FF00FF00FF00FF00
                FF0059B161FF8CC793FF7DBF85FF72B979FF52A759F150A5571767C673AE80CD
                8AFF9ED6A7FF83CC8CFF69C273FF62BE6CFFFF00FF00FF00FF00FF00FF00FF00
                FF005AB364FF59B162FF70BB78FF8DC794FF73BA7AFF52A85AEC68C774B881CE
                8BFF86CF90FF65C371F164C26F1763C06EFFFF00FF00FF00FF00FF00FF00FF00
                FF005CB666FFFF00FF0059B162AE71BB79FF6FB977FF54AA5CB3FF00FF0068C7
                74B367C673EC66C57217FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF0059B262AE58AF60B8FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              TabStop = False
              OnClick = btnCarregar_TarefasClick
            end
            object btnVoltaPedido: TBitBtn
              Left = 171
              Top = 2
              Width = 100
              Height = 25
              Hint = 'Volta Pedido'
              Caption = 'Volta Pedido'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
                B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
                E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
                EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
                D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
                E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
                D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
                D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
                DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
                DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
                DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
                ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
                E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
                F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
                E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
                F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
                FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
                FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
                F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
                D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              TabStop = False
              OnClick = btnVoltaPedidoClick
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Romaneio'
        OnShow = TabSheet1Show
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object lblqt: TLabel
          Left = 8
          Top = 3
          Width = 6
          Height = 16
          Caption = '  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object pnManifesto: TPanel
          Left = 0
          Top = 414
          Width = 714
          Height = 159
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 0
          object Label3: TLabel
            Left = 274
            Top = 68
            Width = 73
            Height = 13
            Caption = 'Data de Sa'#237'da-'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object Label4: TLabel
            Left = 359
            Top = 5
            Width = 27
            Height = 13
            Caption = 'Placa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 12
            Top = 5
            Width = 43
            Height = 13
            Caption = 'Motorista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label18: TLabel
            Left = 12
            Top = 72
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 496
            Top = 6
            Width = 35
            Height = 13
            Caption = 'Veiculo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Obs: TRichEdit
            Left = 12
            Top = 87
            Width = 604
            Height = 42
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Lines.Strings = (
              '')
            MaxLength = 100
            ParentFont = False
            TabOrder = 3
            Zoom = 100
          end
          object edVcto: TJvDateEdit
            Left = 274
            Top = 84
            Width = 104
            Height = 21
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowNullDate = False
            TabOrder = 4
            Visible = False
          end
          object edPlaca: TJvMaskEdit
            Left = 359
            Top = 22
            Width = 68
            Height = 21
            Color = clWhite
            EditMask = '!>LLL-9999;0;_'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 8
            ParentFont = False
            TabOrder = 1
            Text = ''
          end
          object edVeiculo: TComboBox
            Left = 496
            Top = 22
            Width = 109
            Height = 21
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Items.Strings = (
              ''
              'Carreta'
              'Fiorino'
              'HR'
              'Toco'
              'Truck'
              'Van'
              '3/4')
          end
          object edMotorista: TJvMaskEdit
            Left = 12
            Top = 22
            Width = 331
            Height = 21
            TabStop = False
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Text = ''
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 714
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object btFatura: TBitBtn
            Left = 147
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Gerar Manifesto'
            Caption = 'Gerar'
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
              FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
              96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
              92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
              BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
              CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
              D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
              EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
              E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
              E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
              8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
              E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
              FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
              C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
              CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
              CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
              D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
              9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = btFaturaClick
          end
          object btnNovoManifesto: TBitBtn
            Left = 20
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Novo Manifesto'
            Caption = 'Novo'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
              0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
              33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnNovoManifestoClick
          end
          object btnImpressao: TBitBtn
            Left = 280
            Top = 5
            Width = 105
            Height = 25
            Caption = 'Impress'#227'o'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnImpressaoClick
          end
          object btnLimpaRomaneio: TBitBtn
            Left = 410
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Excluir Tarefas Romaneadas que ainda nao foram gerados Manifesto'
            Caption = 'Limpa Romaneio'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
              305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
              005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
              B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
              B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
              B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
              B0557777FF577777F7F500000E055550805577777F7555575755500000555555
              05555777775555557F5555000555555505555577755555557555}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = False
            OnClick = btnLimpaRomaneioClick
          end
        end
        object dbgFatura: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 714
          Height = 382
          Align = alClient
          Ctl3D = False
          DataSource = dsTempRomaneio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = dbgFaturaCellClick
          OnDrawColumnCell = dbgFaturaDrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SELECIONADO'
              Title.Alignment = taCenter
              Title.Caption = 'Selecionar'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUSUARIO'
              Title.Alignment = taCenter
              Title.Caption = 'Transportadora'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 326
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_ROMANEIO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESO'
              Title.Alignment = taCenter
              Title.Caption = 'Volume Total'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 117
              Visible = True
            end>
        end
      end
      object tbImpresso: TTabSheet
        Caption = 'Impressos'
        ImageIndex = 2
        OnHide = tbImpressoHide
        OnShow = tbImpressoShow
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object JvDBUltimGrid1: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 714
          Height = 508
          Align = alClient
          Ctl3D = False
          DataSource = dtsImpresso
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = JvDBUltimGrid1DrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DTINC'
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_ROMANEIO'
              Title.Alignment = taCenter
              Title.Caption = 'Romaneio'
              Width = 61
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TRANSPORTADORA'
              Title.Alignment = taCenter
              Title.Caption = 'Transportadora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOTORISTA'
              Title.Alignment = taCenter
              Title.Caption = 'Motorista'
              Width = 185
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PLACA'
              Title.Alignment = taCenter
              Title.Caption = 'Placa'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VEICULO'
              Title.Alignment = taCenter
              Title.Caption = 'Ve'#237'culo'
              Width = 83
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 714
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object Label6: TLabel
            Left = 577
            Top = 12
            Width = 57
            Height = 13
            Caption = 'Procurar NF'
            Transparent = True
          end
          object btnImpresso: TBitBtn
            Left = 7
            Top = 4
            Width = 105
            Height = 25
            Caption = 'Impress'#227'o'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnImpressoClick
          end
          object btnAlterar: TBitBtn
            Left = 118
            Top = 4
            Width = 118
            Height = 25
            Hint = 'Alterar'
            Caption = 'Alterar Romaneio'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00007D21EB037B1EFF00791504FF00FF00FF00FF00FF00FF00FF00FF001340
              58FF15425EFF25699CFF2C76B4FF3B8BBAADFF00FF00FF00FF00FF00FF00FF00
              FF0001832BEB43A15FFF007B1FCC00791906FF00FF00FF00FF00FF00FF001242
              59FF5D9CD4FFA6CFF5FFA9CFECFF488BC1FF219752FF1B9149FD158F43FD0F8B
              3BFD3A9F5EFF80C196FF46A362FF007D1FD100791907FF00FF00FF00FF001E6D
              93FFCBE3F9FF61AAECFF4098E8FF1567C2FF299B5BFF90CAA9FF8DC8A5FF8AC6
              A1FF88C59EFF6AB685FF82C297FF48A566FF007D21D700791B09FF00FF001E6D
              93FFC8E1F2FFD1E7FAFF347DB5FF3199C3FF319F63FF94CDADFF6FBA8EFF6BB8
              89FF66B685FF61B380FF67B582FF83C298FF3CA05CFF007F25F9FF00FF002063
              98202689B9FFB0CBE1FF67A9C8FF60DCF5FF37A36BFF96CEB0FF94CDADFF91CB
              AAFF90CBA8FF74BC90FF8AC7A1FF46A568FF078735FB01832D01FF00FF00FF00
              FF00FF00FF002689B9FFBEE6F2FFB3F4FCFF3DA56FFF37A46FFF34A269FF309D
              63FF55AF7CFF91CBAAFF4FAB74FF178F45FB118B3D01FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF002790BFFFC3EDF8FFB3F4FCFF60DCF5FF44D6F4FF8EEE
              FAFF34A16DFF5AB381FF289857FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DCF5FF44D6
              F4FF3EA976FF319F65FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DC
              F5FF44D6F4FF8EEEFAFF5DB4E6FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4
              FCFF68D9F5FF6FCFF3FF599DD0FF73ABDDFF4F91C9FFFF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3ED
              F8FFA8E2F8FF6CAEDDFFA5CFF4FFA5CFF4FFBDDBF7FF5393CBF7FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBA
              E4FFA7D4F4FFC5E1F8FFCCE3F9FFCCE3F9FFBDDBF7FF4F90C9FDFF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF0050A8D9FF6AA5D8FFC9E1F7FFCBE3F8FF4295CAFF3182C2AEFF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF002FBAE4094FAADBEA5093CAFD4E90C8FF2F9DD2DF35A4DE19FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnAlterarClick
          end
          object DBNavigator1: TDBNavigator
            Left = 336
            Top = 4
            Width = 224
            Height = 25
            DataSource = dtsImpresso
            VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
            TabOrder = 2
          end
          object Edit1: TEdit
            Left = 640
            Top = 7
            Width = 72
            Height = 21
            TabOrder = 3
            OnExit = Edit1Exit
          end
          object btnTMS: TBitBtn
            Left = 242
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Exporta NF da CLESS para TMS'
            Caption = 'Libera TMS'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
              1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
              96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
              98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
              36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
              6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
              3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
              6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
              42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
              96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
              42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
              FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
              4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
              FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
              54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
              C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
              597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
              71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
              5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
              75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
              FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
              9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
              A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
              52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            TabStop = False
            OnClick = btnTMSClick
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 540
          Width = 714
          Height = 33
          Align = alBottom
          TabOrder = 3
          object Edit2: TEdit
            Left = 12
            Top = 6
            Width = 140
            Height = 21
            Color = clGreen
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Text = 'Exportado para TMS'
          end
          object Edit3: TEdit
            Left = 172
            Top = 6
            Width = 140
            Height = 21
            Color = 33023
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Text = 'Em processamento no TMS'
          end
        end
        object Panel9: TPanel
          Left = 188
          Top = 74
          Width = 245
          Height = 483
          Color = 33023
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label38: TLabel
            Left = 16
            Top = 412
            Width = 82
            Height = 16
            Caption = 'Ocorr'#234'ncia  :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btnFechar: TBitBtn
            Left = 204
            Top = 455
            Width = 28
            Height = 23
            Hint = 'OK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
              1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
              96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
              98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
              36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
              6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
              3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
              6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
              42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
              96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
              42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
              FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
              4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
              FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
              54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
              C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
              597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
              71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
              5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
              75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
              FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
              9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
              A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
              52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnFecharClick
          end
          object cbOcorr: TJvComboBox
            Left = 14
            Top = 432
            Width = 218
            Height = 21
            TabOrder = 1
            Text = ''
            Items.Strings = (
              'Falta de Doctos (PIN; GNRE; Carta Corre'#231#227'o)'
              'Falta de Nota'
              'Transportadora n'#227'o compareceu'
              'Qtde de volumes superior capacidade do veiculo disponibilizado'
              'Veiculo Quebrado'
              'Transportadora n'#227'o faz a pra'#231'a'
              'Falta de volume (erro no pedido)'
              'Falta de Conferente (N'#227'o deu tempo)'
              'Falta de Sistema /Energia'
              'Transportadora Fora da Janela'
              'Falta de Doca/Excesso de Demora')
          end
          object JvDBUltimGrid2: TJvDBUltimGrid
            Left = 13
            Top = 4
            Width = 219
            Height = 402
            Ctl3D = False
            DataSource = dtsAlterar
            Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentCtl3D = False
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnCellClick = JvDBUltimGrid2CellClick
            OnDrawColumnCell = JvDBUltimGrid2DrawColumnCell
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 16
            Columns = <
              item
                Expanded = False
                FieldName = 'Reg'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'nf'
                Width = 50
                Visible = True
              end>
          end
          object btnSair: TBitBtn
            Left = 16
            Top = 455
            Width = 28
            Height = 23
            Hint = 'OK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
              EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
              F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
              F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
              F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
              F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
              FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
              FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
              FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
              FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
              FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
              FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
              FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
              FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
              FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
              FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
              FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
              FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
              FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
              FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
              FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
              FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
              FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
              FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = btnSairClick
          end
        end
      end
    end
  end
  object Query1: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <>
    Left = 16
    Top = 308
  end
  object mdTemp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'pedido'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Inserida'
        DataType = ftInteger
      end
      item
        Name = 'reg'
        DataType = ftInteger
      end
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end>
    AfterOpen = mdTempAfterOpen
    Left = 112
    Top = 148
    object mdTemppedido: TStringField
      FieldName = 'pedido'
      Size = 12
    end
    object mdTempInserida: TIntegerField
      FieldName = 'Inserida'
    end
    object mdTempreg: TIntegerField
      FieldName = 'reg'
    end
    object mdTempnr_romaneio: TIntegerField
      FieldName = 'nr_romaneio'
    end
  end
  object dsTemp: TDataSource
    AutoEdit = False
    DataSet = mdTemp
    Left = 160
    Top = 149
  end
  object iml: TImageList
    Left = 128
    Top = 8
    Bitmap = {
      494C010102000400240710001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object qrRomaneio: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = qrRomaneioBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select t.name transportadora, sum(seq_tour) ttvol, p.percent, p.' +
        'id_spediteur'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur and t.lager = p.lager'
      
        '                 left join kunden d on d.id_klient = p.id_klient' +
        '_ware'
      'where p.stat in (75,95)'
      'and p.id_klient = '#39'CLESS'#39
      'and p.lager = '#39'MAR'#39
      'and (p.opid_bearb is null)'
      'and p.percent is not null'
      'group by t.name, p.percent,p.id_spediteur'
      'order by 3')
    Left = 16
    Top = 360
    object qrRomaneioTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 30
    end
    object qrRomaneioTTVOL: TBCDField
      FieldName = 'TTVOL'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qrRomaneioPERCENT: TBCDField
      FieldName = 'PERCENT'
      Precision = 18
      Size = 9
    end
    object qrRomaneioID_SPEDITEUR: TStringField
      FieldName = 'ID_SPEDITEUR'
      Size = 12
    end
  end
  object dsRomaneio: TDataSource
    AutoEdit = False
    DataSet = qrRomaneio
    Left = 72
    Top = 360
  end
  object dsTempRomaneio: TDataSource
    AutoEdit = False
    DataSet = mdTempromaneio
    OnDataChange = dsTempRomaneioDataChange
    Left = 72
    Top = 411
  end
  object mdTempromaneio: TJvMemoryData
    FieldDefs = <
      item
        Name = 'SELECIONADO'
        DataType = ftInteger
      end
      item
        Name = 'NR_OS'
        DataType = ftInteger
      end
      item
        Name = 'NR_ROMANEIO'
        DataType = ftInteger
      end
      item
        Name = 'DEMISSAO'
        DataType = ftDateTime
      end
      item
        Name = 'PESO'
        DataType = ftFloat
      end
      item
        Name = 'SUSUARIO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NR_MANIFESTO'
        DataType = ftInteger
      end>
    Left = 13
    Top = 412
    object mdTempromaneioSELECIONADO: TBCDField
      FieldName = 'SELECIONADO'
      Precision = 32
    end
    object mdTempromaneioNR_ROMANEIO: TBCDField
      DisplayLabel = 'Romaneio'
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object mdTempromaneioDESTINO: TStringField
      DisplayLabel = 'UF Destino'
      FieldName = 'DESTINO'
      Size = 2
    end
    object mdTempromaneioPESO: TFloatField
      DisplayLabel = 'Peso Total'
      FieldName = 'PESO'
      DisplayFormat = '#,##0.00'
    end
    object mdTempromaneioDEMISSAO: TDateTimeField
      FieldName = 'DEMISSAO'
    end
    object mdTempromaneioSUSUARIO: TStringField
      DisplayWidth = 30
      FieldName = 'SUSUARIO'
      Size = 30
    end
    object mdTempromaneioPLACA: TStringField
      FieldName = 'PLACA'
      Size = 8
    end
    object mdTempromaneioVEICULO: TStringField
      FieldName = 'VEICULO'
      Size = 30
    end
    object mdTempromaneioselecionou: TIntegerField
      FieldName = 'selecionou'
    end
    object mdTempromaneiocod_transp: TStringField
      FieldName = 'cod_transp'
      Size = 30
    end
  end
  object QMapaItens: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = QMapaItensBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct p.nr_auf, case when seq_tour is null then (selec' +
        't count(v.lager) from pack v where v.nr_auf = p.nr_auf and v.ID_' +
        'KLIENT_AUF = p.id_klient) else seq_tour end seq_tour, d.name, p.' +
        'percent, e.ort cidade, e.plz_ort UF , c.nro_nf'
      
        'from auftraege p left join kunden d on d.id_kunde = p.id_kunde_w' +
        'are and d.id_klient = p.id_klient'
      
        '                 left join adressen e on e.ID_EIGNER_2  = d.id_K' +
        'UNDE AND e.id_eigner_1 = p.id_klient left join tb_compara c on c' +
        '.pedido_wms = p.nr_auf'
      
        'where p.id_klient = '#39'CSUICA'#39' and e.ID_EIGNER_1 = '#39'CSUICA'#39' and e.' +
        'KL_EIGNER_2 = '#39'KU'#39
      'and e.KL_EIGNER_1 = '#39'KL'#39
      'and p.LAGER = :0'
      'and p.PERCENT = :1'
      'order by p.nr_auf, d.name')
    Left = 12
    Top = 470
    object QMapaItensNR_AUF: TStringField
      FieldName = 'NR_AUF'
      Size = 12
    end
    object QMapaItensNAME: TStringField
      FieldName = 'NAME'
      Size = 30
    end
    object QMapaItensPERCENT: TBCDField
      FieldName = 'PERCENT'
      Precision = 18
      Size = 9
    end
    object QMapaItensCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 30
    end
    object QMapaItensUF: TStringField
      FieldName = 'UF'
      Size = 10
    end
    object QMapaItensnro_nf: TStringField
      FieldName = 'nro_nf'
    end
    object QMapaItensSEQ_TOUR: TBCDField
      FieldName = 'SEQ_TOUR'
      ReadOnly = True
      Precision = 32
    end
  end
  object ds: TDataSource
    AutoEdit = False
    DataSet = QTarefa
    Left = 56
    Top = 256
  end
  object QTarefa: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterOpen = QTarefaAfterOpen
    Parameters = <>
    SQL.Strings = (
      
        'select distinct p.nr_auf, t.name transportadora, p.percent, d.na' +
        'me, case when seq_tour is null then (select count(v.lager) from ' +
        'pack v where v.nr_auf = p.nr_auf and v.ID_KLIENT_AUF = p.id_klie' +
        'nt) else seq_tour end seq_tour'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur and p.lager = t.lager'
      
        '                 left join kunden d on d.id_kunde= p.id_kunde_wa' +
        're  and p.id_klient = d.id_klient and p.id_klient = d.id_klient'
      'where p.stat in(75,95)'
      'and p.id_klient = '#39'CLESS'#39
      'and p.lager = '#39'MAR'#39
      'and p.nr_auf in ('#39'36000'#39') '
      'and p.opid_bearb is null'
      
        'group by p.nr_auf, p.seq_tour, d.name, t.name, p.percent, seq_to' +
        'ur,p.id_klient'
      'order by p.percent, d.name, t.name, p.nr_auf')
    Left = 16
    Top = 256
    object QTarefaNR_AUF: TStringField
      FieldName = 'NR_AUF'
      Size = 12
    end
    object QTarefaNAME: TStringField
      FieldName = 'NAME'
      Size = 30
    end
    object QTarefaTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 30
    end
    object QTarefaPERCENT: TBCDField
      FieldName = 'PERCENT'
      Precision = 18
      Size = 9
    end
    object QTarefaSEQ_TOUR: TBCDField
      FieldName = 'SEQ_TOUR'
      ReadOnly = True
      Precision = 32
    end
  end
  object qMapaCarga: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct r.nr_romaneio, r.motorista, r.placa, r.veiculo, ' +
        'r.dtinc, t.name transportadora, R.OBSERVACAO, p.id_klient'
      
        'from tb_romaneio r left join spediteure t on to_char(r.cod_trans' +
        'p) = t.id_spediteur and t.lager = :0'
      'left join auftraege p on p.percent = r.nr_romaneio'
      'where r.nr_romaneio = :1'
      'and t.lager = :2'
      'and r.dtexclusao is null')
    Left = 71
    Top = 469
    object qMapaCargaNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object qMapaCargaMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      Size = 50
    end
    object qMapaCargaPLACA: TStringField
      FieldName = 'PLACA'
      Size = 7
    end
    object qMapaCargaVEICULO: TStringField
      FieldName = 'VEICULO'
    end
    object qMapaCargaDTINC: TDateTimeField
      FieldName = 'DTINC'
    end
    object qMapaCargaTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 30
    end
    object qMapaCargaOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 400
    end
    object qMapaCargaID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
  end
  object qrRota: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    AfterOpen = qrRotaAfterOpen
    Parameters = <>
    SQL.Strings = (
      'select t.name, p.id_spediteur'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in(75,95) and p.opid_bearb is null'
      'and id_klient = '#39'CLESS'#39
      'and t.lager = '#39'MAR'#39
      'group by t.name, p.id_spediteur')
    Left = 16
    Top = 200
    object qrRotaNAME: TStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 30
    end
    object qrRotaID_SPEDITEUR: TStringField
      FieldName = 'ID_SPEDITEUR'
      Size = 12
    end
  end
  object dsRota: TDataSource
    AutoEdit = False
    DataSet = qrRota
    Left = 60
    Top = 200
  end
  object QDestino: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = QDestinoBeforeOpen
    Parameters = <
      item
        Name = 'rota'
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    SQL.Strings = (
      
        'select distinct p.nr_auf, case when nvl(p.percent,0) > 0 then 1 ' +
        'else 0 end as inserida'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in (75,95) and p.opid_bearb is null'
      'and id_klient = '#39'CLESS'#39
      'and p.lager = '#39'MAR'#39
      'and p.id_spediteur = :rota'
      'order by p.nr_auf')
    Left = 16
    Top = 148
    object QDestinoNR_AUF: TStringField
      FieldName = 'NR_AUF'
      Size = 12
    end
    object QDestinoINSERIDA: TBCDField
      FieldName = 'INSERIDA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object QImpressos: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = QImpressosBeforeOpen
    Parameters = <
      item
        Name = '0'
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct r.nr_romaneio, r.motorista, (substr(r.placa,1,3)' +
        '||'#39'-'#39'||substr(r.placa,4,4)) placa, r.veiculo, r.dtinc, t.name tr' +
        'ansportadora, p.opid_bearb, p.id_klient, r.tms, r.dtsaida'
      
        'from tb_romaneio r left join spediteure t on to_char(r.cod_trans' +
        'p) = t.id_spediteur and t.lager = :0'
      
        '                   left join auftraege p on p.percent = r.nr_rom' +
        'aneio'
      'where (p.opid_bearb is not null)'
      'and p.lager = :1'
      'and r.dtexclusao is null'
      'and r.dtalteracao is null'
      'order by 1 desc')
    Left = 16
    Top = 532
    object QImpressosNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object QImpressosMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      Size = 50
    end
    object QImpressosPLACA: TStringField
      DisplayWidth = 8
      FieldName = 'PLACA'
      Size = 8
    end
    object QImpressosVEICULO: TStringField
      FieldName = 'VEICULO'
    end
    object QImpressosDTINC: TDateTimeField
      FieldName = 'DTINC'
      DisplayFormat = 'DD/MM/YY'
    end
    object QImpressosTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 30
    end
    object QImpressosOPID_BEARB: TStringField
      FieldName = 'OPID_BEARB'
      Size = 8
    end
    object QImpressosID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
    object QImpressosTMS: TStringField
      FieldName = 'TMS'
      ReadOnly = True
      Size = 1
    end
    object QImpressosDTSAIDA: TDateTimeField
      FieldName = 'DTSAIDA'
      ReadOnly = True
    end
  end
  object dtsImpresso: TDataSource
    AutoEdit = False
    DataSet = QImpressos
    Left = 72
    Top = 532
  end
  object QVolume: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QVolumeBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select sum(ttvol) ttvol from (select distinct case when sum(seq_' +
        'tour) is null then   (select count(v.lager) from pack v where v.' +
        'nr_auf = p.nr_auf and v.ID_KLIENT_AUF = p.id_klient)   else  sum' +
        '(seq_tour) end ttvol'
      'from auftraege p '
      'where p.stat in (75,95)'
      'and p.id_klient = '#39'CLESS'#39
      'and p.lager = '#39'MAR'#39
      'and p.percent = 2'
      'and p.opid_bearb is null'
      'group by seq_tour,p.id_klient, p.nr_auf)')
    Left = 116
    Top = 256
    object QVolumeTTVOL: TBCDField
      FieldName = 'TTVOL'
      ReadOnly = True
      Precision = 32
    end
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    AfterOpen = qrRotaAfterOpen
    Parameters = <>
    SQL.Strings = (
      'select distinct id_klient'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in(75,95) and p.opid_bearb is null'
      'and p.lager = '#39'MAR'#39)
    Left = 8
    Top = 28
    object QClienteID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
  end
  object mdAltera: TJvMemoryData
    FieldDefs = <
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end>
    AfterOpen = mdTempAfterOpen
    Left = 112
    Top = 200
    object mdAlteraReg: TIntegerField
      FieldName = 'Reg'
    end
    object mdAlteranr_romaneio: TIntegerField
      FieldName = 'nr_romaneio'
    end
    object mdAlteranf: TStringField
      FieldName = 'nf'
      Size = 15
    end
  end
  object dtsAlterar: TDataSource
    AutoEdit = False
    DataSet = mdAltera
    Left = 160
    Top = 201
  end
  object Qexporta: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'update tb_romaneio set tms = '#39'X'#39', user_tms = :0, dt_tms = sysdat' +
        'e'
      'where dtexclusao is null'
      'and nr_romaneio = :1')
    Left = 192
    Top = 318
    object QexportaCNPJ_PAGADOR: TStringField
      FieldName = 'CNPJ_PAGADOR'
      ReadOnly = True
      Size = 14
    end
    object QexportaCNPJ_EMITENTE: TStringField
      FieldName = 'CNPJ_EMITENTE'
      ReadOnly = True
      Size = 14
    end
    object QexportaCNPJ_DESTINATARIO: TStringField
      FieldName = 'CNPJ_DESTINATARIO'
      ReadOnly = True
      Size = 14
    end
    object QexportaCNPJ_REDESPACHO: TStringField
      FieldName = 'CNPJ_REDESPACHO'
      ReadOnly = True
      Size = 14
    end
    object QexportaNOTA_FISCAL: TStringField
      FieldName = 'NOTA_FISCAL'
      ReadOnly = True
      Size = 10
    end
    object QexportaSERIE_NOTA_FISCAL: TStringField
      FieldName = 'SERIE_NOTA_FISCAL'
      ReadOnly = True
      Size = 4
    end
    object QexportaEMISSAO_NOTA_FISCAL: TDateTimeField
      FieldName = 'EMISSAO_NOTA_FISCAL'
      ReadOnly = True
    end
    object QexportaQTD_VOLUME: TBCDField
      FieldName = 'QTD_VOLUME'
      ReadOnly = True
      Precision = 10
      Size = 0
    end
    object QexportaPESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 15
      Size = 6
    end
    object QexportaVALOR_TOTAL: TBCDField
      FieldName = 'VALOR_TOTAL'
      ReadOnly = True
      Precision = 11
      Size = 2
    end
    object QexportaCHAVE_NFE: TStringField
      FieldName = 'CHAVE_NFE'
      ReadOnly = True
      Size = 300
    end
  end
end
