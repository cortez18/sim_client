unit RelCT_Gerado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit,
  JvMemoryDataset, ImgList, System.ImageList, PDFSplitMerge_TLB;

type
  TfrmRelCT_Gerado = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    QDestino: TADOQuery;
    DataSource2: TDataSource;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    QDestinoNM_CLIENTE: TStringField;
    QDestinoNR_CNPJ_CPF: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    QDestinoCOD_CLIENTE: TBCDField;
    QCtrcNR_CONHECIMENTO: TBCDField;
    QCtrcFL_TIPO: TStringField;
    QCtrcDT_CONHECIMENTO: TDateTimeField;
    QCtrcVL_TOTAL_IMPRESSO: TFloatField;
    QCtrcNM_USUARIO: TStringField;
    QCtrcNM_CLI_OS: TStringField;
    QCtrcNR_FATURA: TBCDField;
    QCtrcNR_MANIFESTO: TBCDField;
    QCtrcFL_EMPRESA: TBCDField;
    QCtrcNR_SERIE: TStringField;
    QCtrcNM_DESTINATARIO: TStringField;
    QCtrcNM_REMETENTE: TStringField;
    QCtrcCTE_STATUS: TBCDField;
    mdCtrc: TJvMemoryData;
    mdCtrcCod_conhecimento: TBCDField;
    mdCtrcnr_conhecimento: TBCDField;
    mdCtrcfl_tipo: TStringField;
    mdCtrcDt_conhecimento: TDateTimeField;
    mdCtrcVl_total_impresso: TFloatField;
    mdCtrcNm_Usuario: TStringField;
    mdCtrcNm_cli_os: TStringField;
    mdCtrcNr_fatura: TBCDField;
    mdCtrcNr_manifesto: TBCDField;
    mdCtrcFl_empresa: TBCDField;
    mdCtrcNr_serie: TStringField;
    mdCtrcNm_destinatario: TStringField;
    mdCtrcNm_remetente: TStringField;
    mdCtrcCte_status: TBCDField;
    QCtrcCOD_CONHECIMENTO: TBCDField;
    QCtrcCTE_CHAVE: TStringField;
    mdCtrccte_chave: TStringField;
    mdCtrcEscol: TIntegerField;
    QCtrcNR_CONHECIMENTO_COMP: TBCDField;
    mdCtrcnr_conhecimento_comp: TIntegerField;
    Panel3: TPanel;
    Label3: TLabel;
    Gauge1: TGauge;
    btnExcel: TBitBtn;
    btRelatorio: TBitBtn;
    edTotal: TJvCalcEdit;
    DBNavigator1: TDBNavigator;
    btnDacte: TBitBtn;
    btnEnviar: TBitBtn;
    RGSaida: TRadioGroup;
    Panel4: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label13: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    RadioGroup1: TRadioGroup;
    edCtrc: TJvCalcEdit;
    CBO: TComboBox;
    ledIdDestino: TJvDBLookupEdit;
    ledDestino: TJvDBLookupEdit;
    edManifesto: TJvCalcEdit;
    Panel2: TPanel;
    edFatura: TJvCalcEdit;
    edSerie: TEdit;
    edNF: TJvCalcEdit;
    edCteF: TJvCalcEdit;
    iml: TImageList;
    Panel5: TPanel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Panel6: TPanel;
    JvDBGrid1: TJvDBGrid;
    Memo1: TMemo;
    Panel1: TPanel;
    lblqt: TLabel;
    btnXML: TBitBtn;
    QCtrcCTE_DATA: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dtInicialEnter(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure ledDestinoCloseUp(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnDacteClick(Sender: TObject);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure btnEnviarClick(Sender: TObject);
    procedure btnXMLClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelCT_Gerado: TfrmRelCT_Gerado;

implementation

uses Dados, Menu, CT_e_Gerados, funcoes, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmRelCT_Gerado.btnDacteClick(Sender: TObject);
var i : integer;
    path : String;
    //Pdir: PChar;
    sData, sCaminho, nCaminho, caminho : string;
begin

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
      ('select ambiente_cte, certificado from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := mdCtrcFl_empresa.AsInteger;
  dtmdados.IQuery1.open;

  GlbCteAmb := dtmdados.IQuery1.FieldByName('ambiente_cte').AsString;
  frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
  sData := FormatDateTime('YYYY', mdCtrcDt_conhecimento.value) +
    FormatDateTime('MM', mdCtrcDt_conhecimento.value);
  sCaminho := frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCTe + '\' + sData
    + '\CTe\';
  dtmdados.IQuery1.close;

  path := frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte;

  i := 0;
  if mdCtrc.RecordCount > 0 then
  begin
    if (edCteF.Value > 0) then
    begin
      mdCtrc.first;
      while not mdCtrc.Eof do
      begin
        if mdCtrcNR_SERIE.value = '1' then
        begin
          i := i + 1;
          frmMenu.QrCteEletronico.close;
          frmMenu.qrCteEletronico.Parameters[0].value := mdCtrcCOD_CONHECIMENTO.AsInteger;
          frmMenu.QrCteEletronico.open;
          frmMenu.QNF.close;
          frmMenu.QNF.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
          frmMenu.QNF.Parameters[1].value := frmMenu.qrCteEletronicoNR_SERIE.Value;
          frmMenu.QNF.Parameters[2].value := frmMenu.qrCteEletronicoFL_EMPRESA.AsInteger;
          frmMenu.QNF.Open;

          try
            frmMenu.ACBrCTe1.Conhecimentos.Clear;
            frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sCaminho + mdCtrccte_chave.AsString + '-cte.xml');
            if RGSaida.ItemIndex = 0 then
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir
            else if RGSaida.ItemIndex = 1 then
            begin
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := false;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := false;
              frmMenu.ACBrCTe1.Conhecimentos.Imprimir;
              frmMenu.ACBrCTeDACTeRL1.PrintDialog := true;
              frmMenu.ACBrCTeDACTeRL1.MostraPreview := true;
            end
            else
            begin
            caminho := '';
            //if SelectDirectory(caminho, [sdAllowCreate, sdPerformCreate, sdPrompt],0) then
            //begin
              frmMenu.ACBrCTeDACTeRL1.PathPDF := DirectoryListBox1.Directory + '\';
              frmMenu.ACBrCTe1.Conhecimentos.ImprimirPDF;
              //showmessage(caminho);
            //end;
              //frmMenu.ACBrCTeDACTeRL1.PathPDF := DirectoryListBox1.Directory;
              //Application.CreateForm(TfrmDacte_Cte_2, frmDacte_Cte_2);
              //frmDacte_Cte_2.sChaveCte := frmMenu.qrCteEletronicoCTE_CHAVE.AsString;
              //frmDacte_Cte_2.rlDacte.SaveToFile(DirectoryListBox1.Directory +'\CTE-'+frmMenu.qrCteEletronicoNUMEROCTE.AsString+'.pdf' );

            end;
          finally
            //frmDacte_Cte_2.Free;
          end;
        end;
        mdCtrc.next;
      end;
    end;

    frmMenu.ACBrCTeDACTeRL1.PathPDF := path;

  end;
end;

procedure TfrmRelCT_Gerado.btnEnviarClick(Sender: TObject);
var arquivo : String;
    cc, mensagem, xx : TStrings;
    email, smtp, senhaemail, porta, efiscal : string;
    sData, sCaminho : string;
   // stStreamNF: TStringStream;
    SSL : Boolean;
begin
  if Application.Messagebox('Enviar por e-mail os CT-e marcados ?',
    'Enviar e-mail CT-e', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    mdCtrc.first;
    while not mdCtrc.eof do
    begin
      cc := TStringList.Create;
      xx := TStringList.Create;
      mensagem := TStringList.Create;
      if (mdCtrcCte_status.value = 100) and (mdCtrcEscol.value = 1) then
      begin

        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.SQL.add('select ambiente_cte, certificado, logo, pdf_cte, ');
        dtmdados.IQuery1.SQL.add('schemma, proxy, porta, user_proxy, pass_proxy ');
        dtmdados.IQuery1.SQL.add('from tb_controle where doc = ''MDFE'' and filial = :0 ');
        dtmdados.IQuery1.Parameters[0].value := GLBFilial;
        dtmdados.IQuery1.open;
        if (dtmdados.IQuery1.Eof) or (dtmdados.IQuery1.FieldByName('ambiente_cte').IsNull) then
        begin
          ShowMessage('N�o Encontrado nenhuma configura��o de CT-e, avise o TI');
          exit;
        end
        else
        begin
          GlbCteAmb := dtmdados.IQuery1.FieldByName('ambiente_cte').AsString;
          GlbSchema  := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
          frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie := ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

          sData := FormatDateTime('YYYY', mdCtrcDt_conhecimento.value) +
                   FormatDateTime('MM', mdCtrcDt_conhecimento.value);
          sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathCTe + sData + '\CTe\';

          frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
          frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyHost := ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
          frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPort := ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
          frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyUser := ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
          frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPass := ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
          dtmdados.IQuery1.close;
        end;

        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.SQL.add('select * from tb_empresa where codfil = :0 ');
        dtmdados.IQuery1.Parameters[0].value := GLBFilial;
        dtmdados.IQuery1.open;

        smtp := dtmdados.IQuery1.FieldByName('smtp').value;
        porta := dtmdados.IQuery1.FieldByName('porta').value;
        senhaemail := dtmdados.IQuery1.FieldByName('senha').value;
        email := dtmdados.IQuery1.FieldByName('email').value;
        efiscal := dtmdados.IQuery1.FieldByName('efiscal').value;
        if dtmdados.IQuery1.FieldByName('reqaut').value = 'S' then
         SSL := TRUE
        else
         SSL := FALSE;
        dtmdados.IQuery1.close;

        if GLBCTEAMB = 'P' then
        begin
          dtmdados.IQuery1.close;
          dtmdados.IQuery1.SQL.clear;
          dtmdados.IQuery1.SQL.add('select email from cyber.rodctc e left join tb_conhecimento c on c.cod_pagador = e.codclifor ');
          dtmdados.IQuery1.SQL.add('where situac = ''A'' and c.cod_conhecimento = :0');
          dtmdados.IQuery1.Parameters[0].value := mdCtrcCOD_CONHECIMENTO.AsInteger;
          dtmdados.IQuery1.open;
          while not dtmdados.IQuery1.Eof do
          begin
            cc.add(dtmdados.IQuery1.FieldByName('email').AsString);
            dtmdados.IQuery1.next;
          end;
          dtmdados.IQuery1.close;
        end;
        //else
        // cc.add('paulo.cortez@iwlogistica.com.br');

        dtmdados.RQuery1.close;
        dtmdados.RQuery1.sql.clear;
        dtmdados.RQuery1.SQL.add('select distinct razsoc from cyber.rodfil f ');
        dtmdados.RQuery1.SQL.add('where codfil = :0');
        dtmdados.RQuery1.Parameters[0].value := mdCtrcFl_empresa.AsInteger;
        dtmdados.RQuery1.open;

        mensagem.add('A ' + dtmdados.RQuery1.FieldByName('razsoc').value  + ' emitiu o CT-e Anexo');
        Mensagem.Add(' ');
        Mensagem.Add('>>> Sistema SIM - TI Intecom/IW <<<');

        arquivo := scaminho+ mdCtrccte_chave.asstring + '-cte.xml'; //frmMenu.ACBrCte1.DACte.PathPDF + qrMonitorCHAVE_CTE.asstring + '-cte.xml';
        //xx.Add(frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar +'\'+mdCtrccte_chave.asstring+'-cte.pdf');
        frmMenu.ACBrCTe1.Conhecimentos.Clear;
        frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(arquivo);
        frmMenu.ACBrCTe1.Conhecimentos.Items[0].EnviarEmail(efiscal //Para
                                                   , 'CT-e' //edtEmailAssunto.Text
                                                   , mensagem //mmEmailMsg.Lines
                                                   , True //Enviar PDF junto
                                                   , cc //nil //Lista com emails que ser�o enviado c�pias - TStrings
                                                   , nil // Lista de anexos - TStrings
                                                    );

        CC.Free;
        xx.Free;
        mensagem.Free;
        dtmdados.Query1.Close;
      end;
      mdCtrc.next;
    end;
    ShowMessage('E-mail�s enviados com sucesso !!');
  end;
end;

procedure TfrmRelCT_Gerado.btnExcelClick(Sender: TObject);
var sarquivo :String;
    i: integer;
    memo1: TStringList;
begin
  sArquivo := DirectoryListBox1.Directory + '\' + ApCarac(DateToStr(date))+ glbuser + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>'+frmMenu.StatusBar1.Panels[3].Text+'</TITLE>');
  memo1.Add('         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add('                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add('                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  Memo1.Add('Relat�rio de CT-e�s e NFS Emitidos');
  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i:= 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>'+ JvDBGrid1.Columns[i].Title.Caption +'</th>');
  end;
  Memo1.Add('</tr>');
  mdCTRC.First;
  Gauge1.MaxValue := mdCTRC.RecordCount;
  while not mdCTRC.Eof do
  begin
    for i:= 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>'+ mdCTRC.fieldbyname(JvDBGrid1.Columns[i].FieldName).AsString +'</th>');
    Gauge1.AddProgress(1);
    mdCTRC.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sArquivo);
  ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil, SW_SHOWNORMAL);
  //showmessage('Planilha salva em ' + sarquivo);

end;

procedure TfrmRelCT_Gerado.btnXMLClick(Sender: TObject);
var sArquivo, sData, sCaminho, destino: String;   //, cte
    mail: TStringList;
    i: Integer;
begin
  if Application.Messagebox('Salvar XML dos CT-e da Tela ?',
    'Salvar XML�s', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QCtrc.first;
    i := 0;

    // coloca o arquivo no repositorio do FTP
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.SQL.Clear;
    dtmDados.IQuery1.SQL.add('select distinct a.caminho from tb_email_xml a ');
    dtmDados.IQuery1.SQL.add
      ('where a.codcli = (select cod_pagador from tb_conhecimento where cod_conhecimento = :0)');
    dtmDados.IQuery1.Parameters[0].value :=
      QCTRCCOD_CONHECIMENTO.AsInteger;
    dtmDados.IQuery1.Open;

    if dtmdados.IQuery1.RecNo > 0 then
    begin
      while not QCTrc.eof do
      begin
        SData := FormatDateTime('YYYY', QCtrcCTE_DATA.AsDateTime) +
          FormatDateTime('MM', QCtrcCTE_DATA.AsDateTime);
        sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + SData
          + '\CTe\';
        sArquivo := sCaminho + QCtrcCTE_CHAVE.AsString + '-cte.xml';
        i := i + 1;
        frmMenu.ACBrCTe1.Conhecimentos.LoadFromFile(sArquivo);
        frmMenu.ACBrCTe1.Consultar;
        destino := dtmDados.IQuery1.FieldByName('caminho').AsString +
          QCtrcCTE_CHAVE.AsString +
          '-cte.xml';
        CopyFile(PChar(sArquivo), PChar(destino), True);
        QCtrc.next;
      end;
      ShowMessage('Copiado(s) ' + IntToStr(i) + ' XML(s)');
    end
    else
    begin
      mail := TStringList.Create;
      Panel1.Visible := true;
      Application.ProcessMessages;
      while not QCTrc.eof do
      begin
        sData := FormatDateTime('YYYY', QCTRCDT_CONHECIMENTO.value) +
                 FormatDateTime('MM', QCTRCDT_CONHECIMENTO.value);
        sCaminho := frmMenu.ACBrCTe1.Configuracoes.Arquivos.PathSalvar + sData + '\CTe\';
        sarquivo := scaminho+ QCtrcCTE_CHAVE.asstring + '-cte.xml';
        i := i + 1;
        sarquivo := scaminho+ QCtrcCTE_CHAVE.asstring + '-cte.xml';
        destino := DirectoryListBox1.Directory + '\' + QCtrcCTE_CHAVE.asstring + '-cte.xml';
        CopyFile(PChar(sarquivo), PChar(Destino), true);
        ShowMessage('Copiado(s) ' + IntToStr(i) + ' XML(s)');
        QCtrc.next;
      end;
    end;
  end;
end;

procedure TfrmRelCT_Gerado.btRelatorioClick(Sender: TObject);
var x : integer;
begin
  if copy(dtInicial.Text,1,2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  if edCteF.value > 0 then
  begin
    if edCtrc.value = 0 then
    begin
      ShowMessage('N�o Foi Colocado CT-e Inicial !!');
      edCtrc.SetFocus;
      exit;
    end;
    if edCteF.value < edCtrc.value then
    begin
      ShowMessage('O CT-e Final n�o pode ser menor que o CT-e Inicial !!');
      exit;
    end;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  x := 0;
  QCTRC.close;
  if copy(dtInicial.Text,1,2) <> '  ' then
    QCTRC.SQL [9] := 'and c.dt_conhecimento between to_date(' + #39 + formatdatetime('dd/mm/yy',dtinicial.Date) + ' 00:00:01' + #39 + ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + formatdatetime('dd/mm/yy',dtfinal.Date) + ' 23:59:59' + #39+',''dd/mm/yy hh24:mi:ss'')'
  else
    QCTRC.SQL [9] := '';

  QCTRC.SQL [10] := '';

  if edCteF.value > 0 then
  begin
    QCTRC.SQL [10] := 'and c.nr_conhecimento between ' + QuotedStr(apcarac(edCtrc.text)) + ' and ' + QuotedStr(apcarac(edCteF.text));
  end;

  if (edCtrc.Value > 0) and (edCteF.value = 0) then
    QCTRC.SQL [10] := 'and c.nr_conhecimento = ' + #39 + IntToStr(edCtrc.AsInteger) + #39;

  if (edSerie.Text = '') then
  begin
    ShowMessage('Informe a Serie !!');
    edSerie.SetFocus;
    exit;
  end;

  QCTRC.SQL [11] :='  and nr_serie = '+ QuotedStr(edSerie.Text);

  if edManifesto.Value > 0 then
    QCTRC.SQL [12] := 'and c.nr_manifesto = ' + QuotedStr(apcarac(edManifesto.text))
  else
    QCTRC.SQL [12] := '';

  if edfatura.Value > 0 then
    QCTRC.SQL [13] := 'and c.nr_fatura = ' + QuotedStr(apcarac(edfatura.text))
  else
    QCTRC.SQL [13] := '';

  if RadioGroup1.ItemIndex = 0 then
    QCTRC.SQL [14] := 'and c.fl_status = ''A'' '
  else if RadioGroup1.ItemIndex = 1 then
    QCTRC.SQL [14] := 'and c.fl_status in (' + QuotedStr('X') + ', ' +  QuotedStr('I')+ ')'
  else
    QCTRC.SQL [14] := 'and c.fl_status = ''A'' and NR_SERIE = ''1'' and CTE_STATUS <> 100' ;

  if CBO.Text <> 'TODAS' then
    QCTRC.SQL [15] := 'and c.fl_empresa = ' + QuotedStr(alltrim(copy(cbo.Text,1,Pos('-', cbo.Text) - 1)))
  else
    QCTRC.SQL [15] := ' ';

  if Trim(ledIdCliente.LookupValue) <> '' then
    QCTRC.SQL [16] := 'AND c.cod_pagador = ' + QuotedStr(ledIdCliente.LookupValue)
  else
    QCTRC.SQL [16] := ' ';

  if Trim(ledIdDestino.LookupValue) <> '' then
    QCTRC.SQL [17] := 'AND c.cod_destinatario = ' + QuotedStr(ledIdDestino.LookupValue)
  else
    QCTRC.SQL [17] := ' ';

  if edNF.text <> '' then
    QCTRC.SQL [18] := 'AND n.notfis = ' + QuotedStr(apcarac(edNF.text))
  else
    QCTRC.SQL [18] := ' ';

//showmessage(Qctrc.sql.text);
  QCTRC.open;
  lblqt.caption := IntToStr(QCtrc.RecordCount) + ' Registros ';
  mdCtrc.Close;
  mdCtrc.Open;
  edTotal.Value := 0;
  while not QCtrc.Eof do
  begin
    mdCtrc.Append;
    mdCtrcCod_conhecimento.value := QCtrcCOD_CONHECIMENTO.value;
    mdCtrcnr_conhecimento.value := QCtrcnr_CONHECIMENTO.value;
    mdCtrcfl_tipo.value := QCtrcFL_TIPO.value;
    mdCtrcDt_conhecimento.value := QCtrcDT_CONHECIMENTO.value;
    mdCtrcVl_total_impresso.value := QCtrcVL_TOTAL_IMPRESSO.value;
    mdCtrcNm_Usuario.value := QCtrcNM_USUARIO.value;
    mdCtrcNm_cli_os.value := QCtrcNM_CLI_OS.value;
    mdCtrcNr_fatura.value := QCtrcNR_FATURA.value;
    mdCtrcNr_manifesto.value := QCtrcNR_MANIFESTO.value;
    mdCtrcFl_empresa.value := QCtrcFL_EMPRESA.value;
    mdCtrcNr_serie.value := QCtrcNR_SERIE.value;
    mdCtrcNm_destinatario.value := QCtrcNM_DESTINATARIO.value;
    mdCtrcNm_remetente.value := QCtrcNM_REMETENTE.value;
    mdCtrcCte_status.value := QCtrcCTE_STATUS.value;
    mdCtrccte_chave.value := QCtrcCTE_CHAVE.value;
    mdCtrcnr_conhecimento_comp.AsString := QCtrcNR_CONHECIMENTO_COMP.AsString;
    mdCtrcEscol.value := 1;
    mdCtrc.post;
    edTotal.Value := edTotal.Value + mdCtrcVl_total_impresso.value;
    QCtrc.next;
  end;

  if edManifesto.Value > 0 then
  begin
    QCtrc.First;
    edCtrc.Value := QCtrcNR_CONHECIMENTO.AsInteger;
    QCtrc.Last;
    edCteF.Value := QCtrcNR_CONHECIMENTO.AsInteger;
  end;

 {
  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.clear;
  dtmdados.iQuery1.sql.add('select coalesce(sum(c.vl_total),0) as total from tb_conhecimento c ');
  dtmdados.iQuery1.sql.add('where nr_conhecimento is not null ');

  if copy(dtInicial.Text,1,2) <> '  ' then
    dtmdados.iquery1.sql.add('and c.dt_conhecimento between to_date(' + #39 + formatdatetime('dd/mm/yy',dtinicial.Date) + ' 00:00:01' + #39 + ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + formatdatetime('dd/mm/yy',dtfinal.Date) + ' 23:59:59' + #39+',''dd/mm/yy hh24:mi:ss'')');

  if edCtrc.Value > 0 then
    dtmdados.iquery1.sql.add('and c.nr_conhecimento = ' + QuotedStr(edCtrc.text));

  if edManifesto.Value > 0 then
    dtmdados.iquery1.sql.add('and c.nr_manifesto = ' + QuotedStr(edManifesto.text));

  if edfatura.Value > 0 then
    dtmdados.iquery1.sql.add('and c.nr_fatura = ' + QuotedStr(edfatura.text));

  if RadioGroup1.ItemIndex = 0 then
    dtmdados.iquery1.sql.add('and c.fl_status = ''A'' ')
  else
    dtmdados.iquery1.sql.add('and c.fl_status in (' + QuotedStr('X') + ', ' +  QuotedStr('I')+ ')' );

  if CBO.Text <> 'TODAS' then
    dtmdados.iquery1.sql.add('and c.fl_empresa = ' + QuotedStr(copy(cbo.Text,1,1)));

  if Trim(ledIdCliente.LookupValue) <> '' then
    dtmdados.iquery1.sql.add('AND c.cod_pagador = ' + QuotedStr(ledIdCliente.LookupValue));

  if Trim(ledIdDestino.LookupValue) <> '' then
    dtmdados.iquery1.sql.add('AND c.cod_destinatario = ' + QuotedStr(ledIdDestino.LookupValue));

//showmessage(dtmdados.Query1.SQL.text);
  dtmdados.iQuery1.open;
  edTotal.Value := dtmdados.iQuery1.FieldByName('total').value;       }
  dtmdados.iQuery1.close;
  Panel1.Visible := false;
end;

procedure TfrmRelCT_Gerado.dtInicialEnter(Sender: TObject);
begin
  edCtrc.value := 0;
  edCtef.value := 0;
end;

procedure TfrmRelCT_Gerado.edCtrcEnter(Sender: TObject);
begin
  dtInicial.text := '';
  dtFinal.text := '';
  edCteF.value := 0;
  QCtrc.close;
end;

procedure TfrmRelCT_Gerado.edManifestoEnter(Sender: TObject);
begin
  dtInicial.text := '';
  dtFinal.text := '';
end;

procedure TfrmRelCT_Gerado.edosEnter(Sender: TObject);
begin
  dtInicial.text := '';
  dtFinal.text := '';
  edCtrc.value := 0;
end;

procedure TfrmRelCT_Gerado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCTRC.close;
  QCliente.close;
  QDestino.close;
end;

procedure TfrmRelCT_Gerado.FormCreate(Sender: TObject);
begin
  QCliente.open;
  QDestino.Open;
  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.Clear;
  dtmdados.iQuery1.sql.add('select distinct (fl_empresa ||''-''||f.nomeab) nm, fl_empresa from tb_conhecimento c left join cyber.rodfil f on c.fl_empresa = f.codfil order by fl_empresa' );
  dtmdados.iQuery1.open;
  cbO.Clear;
  cbO.Items.add('TODAS');
  while not dtmdados.iQuery1.eof do
  begin
    cbO.Items.add(dtmdados.iQuery1.FieldByName('nm').Value);
    dtmdados.iQuery1.Next;
  end;
  dtmdados.iQuery1.close;
  cbo.ItemIndex := 0;
end;

procedure TfrmRelCT_Gerado.JvDBGrid1CellClick(Column: TColumn);
begin
  if (Column.Field = mdCtrcESCOL) then
  begin
    MDCtrc.edit;
    if (mdCtrcESCOL.IsNull) or (mdCtrcESCOL.Value = 0) then
    begin
      mdCtrcESCOL.value := 1;
    end
    else
    begin
      mdCtrcESCOL.value := 0;
    end;
    mdCtrc.post;
  end;
end;

procedure TfrmRelCT_Gerado.JvDBGrid1DblClick(Sender: TObject);
begin
  frmMenu.tag := mdCtrcCod_conhecimento.AsInteger;
  Application.CreateForm(TfrmCT_e_Gerados, frmCT_e_Gerados);
  frmCT_e_Gerados.ShowModal;
  frmCT_e_Gerados.Free;
  frmMenu.tag := 0;
end;

procedure TfrmRelCT_Gerado.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (mdCtrcNR_SERIE.Value = '1') and (mdCtrcCTE_STATUS.value <> 100) then
  begin
    Jvdbgrid1.canvas.Brush.Color := clRed;
    Jvdbgrid1.Canvas.Font.Color  := clWhite;
    JvDbgrid1.DefaultDrawDataCell(Rect, Jvdbgrid1.columns[datacol].field, State);
  end;
  if (Column.Field = mdCtrcescol) then
  begin
    JvDBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdCtrcescol.Value = 1 then
      iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  //  JvDbgrid1.DefaultDrawDataCell(Rect, Jvdbgrid1.columns[datacol].field, State);
  end;

end;

procedure TfrmRelCT_Gerado.JvDBGrid1TitleClick(Column: TColumn);
var Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption + ' n�o localizado');
end;

procedure TfrmRelCT_Gerado.ledDestinoCloseUp(Sender: TObject);
begin
  ledIdDestino.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledDestino.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmRelCT_Gerado.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
