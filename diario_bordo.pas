unit diario_bordo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, Buttons, RLReport, DB,
  ADODB;

type
  TfrmDiario_bordo = class(TForm)
    Label1: TLabel;
    EdManifesto: TEdit;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLImage1: TRLImage;
    RLImage2: TRLImage;
    RLLabel1: TRLLabel;
    RLDraw43: TRLDraw;
    QManifesto: TADOQuery;
    dtsManifesto: TDataSource;
    QManifestoNR_ROMANEIO: TBCDField;
    QManifestoMOTORISTA: TStringField;
    QManifestoPLACA: TStringField;
    QManifestoVEICULO: TStringField;
    QManifestoDTINC: TDateTimeField;
    QManifestoTRANSPORTADORA: TStringField;
    btnImprimir: TBitBtn;
    QPallet: TADOQuery;
    QPalletCODEAN: TStringField;
    RLBand2: TRLBand;
    RLLabel39: TRLLabel;
    RLDraw40: TRLDraw;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLDraw3: TRLDraw;
    RLDraw4: TRLDraw;
    RLDraw5: TRLDraw;
    RLDraw6: TRLDraw;
    RLDraw7: TRLDraw;
    RLLabel2: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel3: TRLLabel;
    RLDraw9: TRLDraw;
    RLDraw10: TRLDraw;
    RLLabel11: TRLLabel;
    RLDraw11: TRLDraw;
    RLDraw12: TRLDraw;
    RLDraw14: TRLDraw;
    RLDraw15: TRLDraw;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLDraw16: TRLDraw;
    RLDraw17: TRLDraw;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLDraw18: TRLDraw;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLDraw19: TRLDraw;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw20: TRLDraw;
    RLDraw21: TRLDraw;
    RLDraw22: TRLDraw;
    RLLabel25: TRLLabel;
    RLDraw23: TRLDraw;
    RLDraw24: TRLDraw;
    RLDraw25: TRLDraw;
    RLDraw28: TRLDraw;
    RLLabel26: TRLLabel;
    RLDraw29: TRLDraw;
    RLDraw26: TRLDraw;
    RLDraw27: TRLDraw;
    RLLabel27: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw30: TRLDraw;
    RLDraw31: TRLDraw;
    RLDraw32: TRLDraw;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLDraw33: TRLDraw;
    RLDraw34: TRLDraw;
    RLDraw35: TRLDraw;
    RLDraw36: TRLDraw;
    RLLabel31: TRLLabel;
    RLDraw37: TRLDraw;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLDraw38: TRLDraw;
    RLDraw39: TRLDraw;
    RLDraw41: TRLDraw;
    RLDraw42: TRLDraw;
    RLMemo2: TRLMemo;
    RLLabel34: TRLLabel;
    RLLabel43: TRLLabel;
    RLDraw47: TRLDraw;
    RLLabel40: TRLLabel;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel37: TRLLabel;
    RLLabel45: TRLLabel;
    RLLabel44: TRLLabel;
    RLDraw46: TRLDraw;
    RLLabel35: TRLLabel;
    RLLabel36: TRLLabel;
    RLDraw44: TRLDraw;
    RLDraw45: TRLDraw;
    RLDraw50: TRLDraw;
    RLDraw49: TRLDraw;
    RLDraw48: TRLDraw;
    RLDraw52: TRLDraw;
    RLDraw53: TRLDraw;
    RLDraw51: TRLDraw;
    RLDraw54: TRLDraw;
    RLDraw57: TRLDraw;
    RLDraw63: TRLDraw;
    RLLabel52: TRLLabel;
    RLLabel53: TRLLabel;
    RLDraw64: TRLDraw;
    RLLabel54: TRLLabel;
    RLLabel55: TRLLabel;
    RLDraw65: TRLDraw;
    RLDraw66: TRLDraw;
    RLDraw67: TRLDraw;
    RLDraw68: TRLDraw;
    RLLabel58: TRLLabel;
    RLLabel59: TRLLabel;
    RLLabel57: TRLLabel;
    RLDraw70: TRLDraw;
    RLDraw71: TRLDraw;
    RLImage3: TRLImage;
    RLImage4: TRLImage;
    RLLabel60: TRLLabel;
    RLDraw72: TRLDraw;
    RLDraw73: TRLDraw;
    RLDraw74: TRLDraw;
    RLDraw75: TRLDraw;
    RLDraw76: TRLDraw;
    RLDraw77: TRLDraw;
    RLDraw78: TRLDraw;
    RLDraw79: TRLDraw;
    RLDraw82: TRLDraw;
    RLDraw83: TRLDraw;
    RLLabel61: TRLLabel;
    RLLabel62: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel63: TRLLabel;
    RLLabel64: TRLLabel;
    RLDraw84: TRLDraw;
    RLDraw85: TRLDraw;
    RLLabel68: TRLLabel;
    RLLabel67: TRLLabel;
    RLLabel66: TRLLabel;
    RLLabel65: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel23: TRLLabel;
    RLMemo3: TRLMemo;
    RLMemo4: TRLMemo;
    QPalletCODMAN: TBCDField;
    RLDraw86: TRLDraw;
    RLLabel56: TRLLabel;
    RLDraw13: TRLDraw;
    RLDraw55: TRLDraw;
    RLDraw56: TRLDraw;
    RLLabel47: TRLLabel;
    RLDraw58: TRLDraw;
    RLDraw59: TRLDraw;
    RLDraw60: TRLDraw;
    RLDraw61: TRLDraw;
    RLLabel48: TRLLabel;
    RLLabel49: TRLLabel;
    RLLabel50: TRLLabel;
    RLLabel51: TRLLabel;
    RLDraw69: TRLDraw;
    RLDraw80: TRLDraw;
    RLDraw8: TRLDraw;
    RLLabel10: TRLLabel;
    RLDraw62: TRLDraw;
    RLLabel46: TRLLabel;
    RLDraw81: TRLDraw;
    RLLabel69: TRLLabel;
    RLDraw87: TRLDraw;
    RLLabel70: TRLLabel;
    RLDraw88: TRLDraw;
    motorista: TRLLabel;
    transportadora: TRLLabel;
    veiculo: TRLLabel;
    NR_ROMANEIO: TRLLabel;
    placa: TRLLabel;
    DTIC: TRLLabel;
    motorista1: TRLLabel;
    transportadora1: TRLLabel;
    veiculo1: TRLLabel;
    nr_romaneio1: TRLLabel;
    placa1: TRLLabel;
    DTIC1: TRLLabel;
    ADOQRep: TADOQuery;
    DSRep: TDataSource;
    ADOQRepRAZSOC: TStringField;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLReport2: TRLReport;
    RLBand3: TRLBand;
    RLLabel71: TRLLabel;
    RLBand4: TRLBand;
    RLDraw89: TRLDraw;
    RLDraw90: TRLDraw;
    RLDraw91: TRLDraw;
    RLDraw92: TRLDraw;
    RLImage5: TRLImage;
    RLLabel72: TRLLabel;
    RLDraw93: TRLDraw;
    RLLabel74: TRLLabel;
    RLDraw94: TRLDraw;
    RLLabel76: TRLLabel;
    Ckbcrm: TCheckBox;
    AdoInfcrm: TADOQuery;
    DSInfcrm: TDataSource;
    RLLabel79: TRLLabel;
    RLDraw96: TRLDraw;
    RLBand7: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLLabel80: TRLLabel;
    RLLabel81: TRLLabel;
    RLLabel82: TRLLabel;
    QPalletCODPAG: TBCDField;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    AdoInfcrmNRO_MANIF: TBCDField;
    AdoInfcrmSERIE_MANIF: TStringField;
    AdoInfcrmEMISSAO_MANIF: TStringField;
    AdoInfcrmFILIAL: TBCDField;
    AdoInfcrmCNPJ_DESTINATARIO: TStringField;
    AdoInfcrmDESTINATARIO: TStringField;
    AdoInfcrmOBSERVACOES: TStringField;
    RLBand5: TRLBand;
    RLDraw98: TRLDraw;
    RLDraw99: TRLDraw;
    RLDraw97: TRLDraw;
    RLLabel73: TRLLabel;
    RLDraw102: TRLDraw;
    RLLabel77: TRLLabel;
    RLLabel78: TRLLabel;
    RLDBText7: TRLDBText;
    RLDBMemo1: TRLDBMemo;
    RLDBMemo2: TRLDBMemo;
    Edit1: TEdit;
    Label2: TLabel;
    AdoInfcrmPALLETIZACAO: TStringField;
    AdoInfcrmAJUDANTE: TStringField;
    AdoInfcrmEXIGE_AGEND: TStringField;
    RLDraw100: TRLDraw;
    RLDraw101: TRLDraw;
    RLDraw103: TRLDraw;
    RLLabel75: TRLLabel;
    RLLabel83: TRLLabel;
    RLLabel84: TRLLabel;
    pal: TRLLabel;
    ajud: TRLLabel;
    ag: TRLLabel;
    QManifestoCODFIL: TBCDField;
    QPalletCLIENTE: TStringField;
    QPalletTIPO: TStringField;
    cbRomaneio: TCheckBox;
    QManifestoTIPO: TStringField;
    QManifestoSERIE: TStringField;
    QQuant: TADOQuery;
    QQuantPALLET: TStringField;
    QQuantQUANT: TBCDField;
    ADOQRepcodcgc: TStringField;
    procedure btnImprimirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure CkbcrmClick(Sender: TObject);
    procedure AdoInfcrmAfterOpen(DataSet: TDataSet);
    procedure cbRomaneioClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDiario_bordo: TfrmDiario_bordo;

implementation

uses Dados, GeraManifesto, Vale_palete, Menu, diario_bordo_roldao;

{$R *.dfm}

procedure TfrmDiario_bordo.AdoInfcrmAfterOpen(DataSet: TDataSet);
begin
  if AdoInfcrmPALLETIZACAO.Value = 'S' then
    pal.Caption := 'SIM'
  else
    pal.Caption := 'N�O';

  if AdoInfcrmAJUDANTE.Value = 'S' then
    ajud.Caption := 'SIM'
  else
    ajud.Caption := 'N�O';

  if AdoInfcrmEXIGE_AGEND.Value = 'S' then
    ag.Caption := 'SIM'
  else
    ag.Caption := 'N�O';
end;

procedure TfrmDiario_bordo.btnImprimirClick(Sender: TObject);
begin
  if Ckbcrm.Checked then
  begin
    AdoInfcrm.close;
    AdoInfcrm.Parameters[0].Value := StrToInt(EdManifesto.Text);
    AdoInfcrm.Parameters[1].Value := Edit1.Text;
    AdoInfcrm.Parameters[2].Value := GLBFilial;
    AdoInfcrm.Open;
    if not AdoInfcrm.IsEmpty then
      RLReport2.Preview()
    else
      ShowMessage('Esse Manifesto n�o existe observa��es do CRM.');
  end
  else
  begin
    if EdManifesto.Text <> '' then
    begin
      QManifesto.close;
      QManifesto.Parameters[0].Value := StrToInt(EdManifesto.Text);
      if self.tag = 1 then
        QManifesto.Parameters[1].Value := 'M'
      else if self.tag = 2 then
        QManifesto.Parameters[1].Value := 'R';
      QManifesto.Parameters[2].Value := GLBFilial;
      QManifesto.Open;
      // verifica se tem pallet a controlar
      QPallet.close;
      QPallet.Parameters[0].Value := StrToInt(EdManifesto.Text);
      if self.tag = 1 then
        QPallet.Parameters[1].Value := 'M'
      else if self.tag = 2 then
        QPallet.Parameters[1].Value := 'R'
    end;
    QPallet.Open;
    // consulta quantidade de lojas
    ADOQRep.close;
    ADOQRep.Parameters[0].Value := StrToInt(EdManifesto.Text);
    ADOQRep.Parameters[1].Value := QManifestoSERIE.AsString;
    if self.tag = 1 then
      ADOQRep.Parameters[2].Value := 'M'
    else
      ADOQRep.Parameters[2].Value := 'R';
    ADOQRep.Open;
    if QManifestoNR_ROMANEIO.Value > 0 then
    begin
      if QPalletCODEAN.Value = 'PALLET' then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.SQL.clear;
        dtmdados.IQuery1.SQL.Add
          ('select diario_bordo from tb_parametro ');
        dtmdados.IQuery1.SQL.Add
          (' where codcli = :0 ');
        dtmdados.IQuery1.Parameters[0].value := QPalletCODPAG.AsInteger;
        dtmdados.IQuery1.open;
        if dtmdados.IQuery1.FieldByName('diario_bordo').AsString = 'S' then
        begin
          //RLReport1.Preview;
          Application.CreateForm(TfrmDiario_bordo_roldao, frmDiario_bordo_roldao);
          frmDiario_bordo_roldao.ADOQRep.close;
          frmDiario_bordo_roldao.ADOQRep.Parameters[0].Value := StrToInt(EdManifesto.Text);
          frmDiario_bordo_roldao.ADOQRep.Parameters[1].Value := QManifestoSERIE.AsString;
          if self.tag = 1 then
            frmDiario_bordo_roldao.ADOQRep.Parameters[2].Value := 'M'
          else
            frmDiario_bordo_roldao.ADOQRep.Parameters[2].Value := 'R';
          frmDiario_bordo_roldao.ADOQRep.Open;
          frmDiario_bordo_roldao.QManifesto.close;
          frmDiario_bordo_roldao.QManifesto.Parameters[0].Value := StrToInt(EdManifesto.Text);
          if self.tag = 1 then
            frmDiario_bordo_roldao.QManifesto.Parameters[1].Value := 'M'
          else if self.tag = 2 then
            frmDiario_bordo_roldao.QManifesto.Parameters[1].Value := 'R';
          frmDiario_bordo_roldao.QManifesto.Parameters[2].Value := GLBFilial;
          frmDiario_bordo_roldao.QManifesto.Open;
          frmDiario_bordo_roldao.QPallet.close;
          frmDiario_bordo_roldao.QPallet.Parameters[0].Value := StrToInt(EdManifesto.Text);
          if self.tag = 1 then
            frmDiario_bordo_roldao.QPallet.Parameters[1].Value := 'M'
          else if self.tag = 2 then
            frmDiario_bordo_roldao.QPallet.Parameters[1].Value := 'R';
          frmDiario_bordo_roldao.QPallet.open;
          frmDiario_bordo_roldao.RLReport1.Preview;
        end
        else
        begin
          while not ADOQRep.eof do
          begin

            Application.CreateForm(TfrmVale_palete, frmVale_palete);
            frmVale_palete.motorista.Caption := QManifestoMOTORISTA.AsString;
            frmVale_palete.RLLabel40.Caption := QManifestoMOTORISTA.AsString;
            frmVale_palete.transportadora.Caption :=
              QManifestoTRANSPORTADORA.AsString;
            frmVale_palete.RLLabel41.Caption := QManifestoTRANSPORTADORA.AsString;
            frmVale_palete.veiculo.Caption := QManifestoVEICULO.AsString;
            frmVale_palete.RLLabel44.Caption := QManifestoVEICULO.AsString;
            frmVale_palete.placa.Caption := QManifestoPLACA.AsString;
            frmVale_palete.RLLabel36.Caption := QManifestoPLACA.AsString;
            frmVale_palete.NR_ROMANEIO.Caption :=
              VarToStr(QManifestoNR_ROMANEIO.Value);
            frmVale_palete.RLLabel46.Caption :=
              VarToStr(QManifestoNR_ROMANEIO.Value);
            frmVale_palete.DTIC.Caption := DateTimeToStr(QManifestoDTINC.Value);
            frmVale_palete.RLLabel48.Caption :=
              DateTimeToStr(QManifestoDTINC.Value);
            frmVale_palete.RLLabel51.Caption := QPalletCLIENTE.AsString;
            frmVale_palete.RLLabel52.Caption := QPalletCLIENTE.AsString;
            frmVale_palete.RLLabel54.Caption := ADOQRepRAZSOC.AsString + ' - ' +
              ADOQRepcodcgc.AsString;
            frmVale_palete.RLLabel56.Caption := ADOQRepRAZSOC.AsString + ' - ' +
              ADOQRepcodcgc.AsString;

            QQuant.close;
            QQuant.Parameters[0].Value := QManifestoNR_ROMANEIO.AsInteger;
            if self.tag = 1 then
              QQuant.Parameters[1].Value := 'M'
            else
              QQuant.Parameters[1].Value := 'R';
            QQuant.Parameters[2].Value := GLBFilial;
            QQuant.Open;

            if QQuant.locate('Pallet', 'PBR', []) then
            begin
              frmVale_palete.RLLabel57.Caption := QQuantQUANT.AsString;
              frmVale_palete.RLLabel59.Caption := QQuantQUANT.AsString;
            end;
            if QQuant.locate('Pallet', 'CHP', []) then
            begin
              frmVale_palete.RLLabel58.Caption := QQuantQUANT.AsString;
              frmVale_palete.RLLabel60.Caption := QQuantQUANT.AsString;
            end;
            QQuant.close;

            if (QPalletCODPAG.Value = 7283) OR (QPalletCODPAG.Value = 13205)
            then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\cless.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\cless.bmp');
            end
            else if (QPalletCODPAG.Value = 21615) OR
              (QPalletCODPAG.Value = 21366) then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\casasui�a.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\casasui�a.bmp');
            end
            else if (QPalletCODPAG.Value = 305) OR (QPalletCODPAG.Value = 8119)
              OR (QPalletCODPAG.Value = 20765) OR (QPalletCODPAG.Value = 20934)
              OR (QPalletCODPAG.Value = 21642) then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\meritor.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\meritor.bmp');
            end
            else if (QPalletCODPAG.Value = 244) OR (QPalletCODPAG.Value = 7337)
            then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\mirka.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\mirka.bmp');
            end
            else if (QPalletCODPAG.Value = 7988) then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\baruel.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\baruel.bmp');
            end
            else if (QPalletCODPAG.Value = 21515) OR
              (QPalletCODPAG.Value = 21516) then
            begin
              frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage2.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\dicate.bmp');
              frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
                'BMP file', TBitmap);
              frmVale_palete.RLImage5.Picture.LoadFromFile
                ('\\192.168.236.112\sim\Atualizador\Relatorio\dicate.bmp');
            end;

            frmVale_palete.RLReport1.Preview;
            frmVale_palete.Free;
            ADOQRep.next;
          end;
        end;
      end
      else
      begin
        // ShowMessage('Manifesto N�o Pertence ao Rold�o !!');
        ShowMessage('O cliente desse manifesto N�o tem controle de Pallet !!');
        EdManifesto.setfocus;
      end;
    end
    else
    begin
      ShowMessage('Manifesto N�o Encontrado');
      EdManifesto.setfocus;
    end;
  end;
end;

procedure TfrmDiario_bordo.cbRomaneioClick(Sender: TObject);
begin
  if cbRomaneio.Checked then
    self.tag := 2
  else
    self.tag := 1;
end;

procedure TfrmDiario_bordo.CkbcrmClick(Sender: TObject);
begin
  if Ckbcrm.Checked then
  begin
    Label2.Visible := true;
    Edit1.Visible := true;
  end
  else
  begin
    Label2.Visible := FALSE;
    Edit1.Visible := FALSE;
  end;
end;

procedure TfrmDiario_bordo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QManifesto.close;
  QPallet.close;
  self.tag := 0;
end;

procedure TfrmDiario_bordo.FormCreate(Sender: TObject);
begin
  self.tag := 1;
end;

procedure TfrmDiario_bordo.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  motorista.Caption := QManifestoMOTORISTA.AsString;
  motorista1.Caption := QManifestoMOTORISTA.AsString;
  transportadora.Caption := QManifestoTRANSPORTADORA.AsString;
  transportadora1.Caption := QManifestoTRANSPORTADORA.AsString;
  veiculo.Caption := QManifestoVEICULO.AsString;
  veiculo1.Caption := QManifestoVEICULO.AsString;
  placa.Caption := QManifestoPLACA.AsString;
  placa1.Caption := QManifestoPLACA.AsString;
  NR_ROMANEIO.Caption := VarToStr(QManifestoNR_ROMANEIO.Value);
  nr_romaneio1.Caption := VarToStr(QManifestoNR_ROMANEIO.Value);
  DTIC.Caption := DateTimeToStr(QManifestoDTINC.Value);
  DTIC1.Caption := DateTimeToStr(QManifestoDTINC.Value);
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select diario_bordo from tb_parametro ');
  dtmdados.IQuery1.SQL.Add
    (' where codcli = :0 ');
  dtmdados.IQuery1.Parameters[0].value := QPalletCODPAG.AsInteger;
  dtmdados.IQuery1.open;
  if dtmdados.IQuery1.FieldByName('diario_bordo').AsString = 'S' then
  begin
    RLImage2.Picture.RegisterFileFormat('BMP', 'BMP file', TBitmap);
    RLImage2.Picture.LoadFromFile
      ('\\192.168.236.112\sim\Atualizador\Relatorio\rold�o.bmp');
    RLImage4.Picture.RegisterFileFormat('BMP', 'BMP file', TBitmap);
    RLImage4.Picture.LoadFromFile
      ('\\192.168.236.112\sim\Atualizador\Relatorio\rold�o.bmp');
  end;
  dtmdados.IQuery1.close;
end;

end.
