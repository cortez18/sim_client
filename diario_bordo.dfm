object frmDiario_bordo: TfrmDiario_bordo
  Left = 44
  Top = 186
  Caption = 'Di'#225'rio de Bordo'
  ClientHeight = 120
  ClientWidth = 229
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 91
    Height = 19
    Caption = 'Manifesto :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 126
    Top = 57
    Width = 28
    Height = 13
    Caption = 'S'#233'rie:'
    Visible = False
  end
  object EdManifesto: TEdit
    Left = 120
    Top = 8
    Width = 93
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object RLReport1: TRLReport
    Left = 44
    Top = 1000
    Width = 794
    Height = 1123
    Margins.LeftMargin = 5.000000000000000000
    Margins.TopMargin = 5.000000000000000000
    Margins.RightMargin = 5.000000000000000000
    Margins.BottomMargin = 5.000000000000000000
    AllowedBands = [btHeader, btTitle]
    AdjustableMargins = True
    DataSource = DSRep
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ShowDesigners = False
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      AlignWithMargins = True
      Left = 19
      Top = 19
      Width = 756
      Height = 31
      BandType = btHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        AlignWithMargins = True
        Left = 291
        Top = 6
        Width = 178
        Height = 22
        Alignment = taCenter
        Caption = 'DI'#193'RIO DE BORDO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLImage1: TRLImage
        Left = 8
        Top = 2
        Width = 83
        Height = 28
        Picture.Data = {
          0A544A504547496D61676514180000FFD8FFE000104A46494600010101006000
          600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC0001108006F010603012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7E0
          3145145001451450014514500145145001451450014138F5A290E3193C0F5A00
          E0BC65F16F41F056B306977B0DD5CDC3AABCBF670A44287A139239EF8F4FA8AE
          DAC6FADB52B082F6D2512DB5C46B2C5229E1948C835E23E02D120F1F5F78FB5D
          BD4F306A0EF636EC7F857A8DBF4C45F9574DF023549AEFC07269D704F9FA5DDC
          96C41EC3EF01F9923F0A00F50A28A2800A28A2800A28A2800A28A2800A28A280
          0A28A2800A28A2800A28A2800A28A2800A28A2802A6A9AA59E8DA6CFA85FCEB0
          5AC0A5E491BA015E6569FB41784AE35416B25BEA16F6ECDB45DC91AEC1EE4039
          03F0AE87E2DE877DE21F875A85969C864B9529308875902B0240F7C57C9367A4
          6A37DA88D3ED6CEE25BB67DA2258CEF073DFD3B7E7401F75C722CB1AC88C1918
          65594E411D8834EAC9F0C69D3693E17D2B4FB87DD3DB5A45139F75500FEB9AD6
          A430A28A3F0A0028A28A0028AF39F15FC68F0D785B50B9D35A3BBBCD42DDB6BC
          3047800E3A163F5ED9AB3F0CFE21CBF1061D52E1B4F4B38AD65448D4485D9B70
          279381E82803BDAC8F14DF7F66784B58BECE0DBD9CD203EE1091FAD6BD715F16
          EE0DB7C2CD7DC36D2D008FEBB9D57FAD0065FC0BB1FB1FC2EB1936E1AEE69666
          E319F98A8FD14564FC2E234CF8A1E3CD141010DC0B945C6382CDFF00C5AD769F
          0D6DFECBF0D7C3F1F5FF004347E9DD86EFEB5C5E921ACBF697D6A1DC36DE6981
          F18EB858CFF306811EBE28A404E7914B40C28A4CD00E7B5002D1499A01CD002D
          1466933400B451499A005A2933403400B45145001452679A33400B45213E9450
          03A8A28A62313C5BE2387C27E1BBAD6AE209278ED82931C64066CB05EFF5AF29
          1FB45E81E6994787AF44878DDBA3CFD335DA7C66FF009251AD73FC31FF00E8C5
          AF8FBBD007DED6974B75670DCA8C09515C29233C8CD790DE7ED11A3D95F5C5AC
          9A15F9782468D8874EA091FD2BD5F471FF00125B0E7FE5DA3FFD0457C4DE28C0
          F176B43D2FE7FF00D18D401F555F7C5BF0F699E10D3F5FBD32C66FE2324164B8
          695B9C74E98CF7AE01BF6954F3F0BE1763167BDE0DD8FA6CAF2DF07F82B5FF00
          8877A2D6CE402DECE308F7370C7CB854E485E33C9E78AB5E3BF85FACF80E282E
          6F2586E6CE76F2D67832007EBB581031C67F2A00FA5BC15F11743F1CDAB369D2
          3457718CCB693E0489EFC751EE2A7F1CF8D2DBC0DA126AB776935CC6F3AC0122
          601B24139E7FDDAF91BC23AFDC786BC57A76AD6EE50C332EF038DD19E194FB11
          9AFAAFE24783AE3C7BE168B4BB4BB8AD9BED093F992292080A78E3FDEA00F953
          C65AEC5E25F16EA3AC410BC315DCBE62C6E412BC639C7D2BB3F855F13EC3E1FD
          8EA56F79A7DCDD35DCA8EA616501768239CFD6B85F136872F86FC477BA34D324
          F25A49B1A4404293807807EB5D3FC3EF85F79F102D2F67B5D460B516AEA8C254
          2DBB7027231F4A00FA57C07E39B5F1E6913EA3696735AC70CE602B330249DA0E
          78FAD73FF1DAEA387E17DDC2D22ABCF3C28AB9196C382703BF4AD1F867E09B8F
          87FE1DBBB0BCBD86E0C970D71E646A55546D031CFF00BB9AF9BBE2478CEE7C6B
          E2DB9B932B7D820768ACE2CF0230701B1EA7AFFF00AA803D2B4DF8FB6BA7E9FA
          568FA7681248B04115B99669C27214038500FA7AD3EFBC556765F1CF49D7EFD0
          DB412E998942FCD8C8700FD3A554F86DF04ADF57D1AD75DD7AEAE62F3C096DED
          A0C29D9D998907AF503D2A97C5CD1D345F1BE8B04523BAFD80ED66FBD80CF4A4
          ECAE6B429AA956307D59F46DA5DC17D6A9736D2A4B0C8BB91D1B208F6353EEC5
          78B7C18F11C897D73E1F9DF313A19EDC1FE123EF01EC739FC2BA8F8A9E2E9BC3
          DA3C5656527977B7B901C758D00E587BF38AC9554E1CCCEAAB9754862BEAD1FE
          91A1E23F899A0F87A67B6777BABB5EB05B80C54FA13D0571ADF1CCEF3B341CA7
          62D7383FFA0D792430CD773243123CB34AC02AAF2598FF0033935E97A77C13D5
          2E6D965BDD4EDEDA52326258CBEDFA9E2B0F6B5277E53DC9E5D97E112FAC3BB7
          FD743BBF05FC4883C5F7D2590D367B69638CC858B064C640EBC7AD76575776F6
          503DC5CCA90C2832CEED800570FF000FFC0171E0EBCD426B8B986E3CF4444745
          20E0139041FA8AF3FF008B3E2C9B54D6E4D16090AD9599DAEAA7FD649DF3F4AD
          B9DC21791E547074F158B74F0FF07F5DCEC356F8D3A45A4CD1E99673DFED38F3
          33E5A1FA120923F0ACD87E39AB4AA27D05D63CF263B80C7F22A3F9D79FF83FC2
          379E2FD51EDE191608221BA79D86420F403B9AE9FC5BF09A7D07487D474FBE6B
          C8E0199E2910060BDC8F5FA560A75A4B996C7AB2C1E594A6A8547793F53D67C3
          7E34D1BC53016D3E7FDEA0CBC120DB22FE1FE15D0641AF9234DD4AE748D4A1BF
          B390C7710386523B8EE0FA8AFA8FC3DAC47AF787ECF538800B3C418A839DADDC
          7E0735BD2ABCEB5DCF2B33CB7EA724E2EF16711A97C64D3B4DD4EEAC64D2EED9
          EDE5688B065009071EBED5B33FC4AD16D3C3169ACDCF9886ED4986D460C8D838
          F5F5AF09F1780BE32D63D3ED721FD4D4DE1AF0C6ABE31BC16B68E0456EB86966
          24A4409271F5CE6B1F6D3E67147AB2C9F0AA8C6AC9F2ADDFDC7A1B7C735F3B0B
          A0318B38E6E006FCB6E3F5AEF3C33E35D2BC5362F73692344F08CCD0CD8568FD
          CFB7079AF06F16F81F52F083C2D752473DBCDC24B10E377A107A573D6F77716C
          9324133C42788C720538DEA71F29F63C7142AF38BB48B965184C4D253C33B799
          EE1AE7C66D26C2E9A0D36CDF512A70D287D91E7D8E0E699A27C68D3AFAF12DF5
          2B17B00E76ACDE66F407DCE060571769F08F5FBBD205E996D6195977A5B36771
          1D813D05707246D04AF14B1959118AB2B0C1041C1068955AB1777B0A8E5780AD
          170A6EF25BB3EBE465951591815232083D69F8E6B81F845ABCBA9F8356199CBC
          96729837139257AAE7F038FC2BBEEF5D709732B9F315E8BA35654DEE87514515
          462705F19FFE4946B7FEEC7FFA316BE3EAFB07E33FFC928D6FFDD8FF00F462D7
          C7D401F7868FFF00205B0FFAF68FFF004115F12F8A4E3C61AD1FFA7F9FFF0046
          357DB5A3FF00C816C3FEBDA3FF00D0457C4BE2AFF91BF5BFFAFF009FFF004635
          007D0BFB39C683C0DA8385018EA0C09C724044E3F9D6CFC758D5BE155FB32862
          B340413D8F98067F226B23F674FF009112FBFEC20FFF00A0256CFC74FF009251
          A8FF00D7683FF462D007C959E6BEEFD218BE8D60CC492D6F1927EAA2BE0FEF5F
          77E8DFF203D3BFEBDA3FFD045007C83F15BFE4A8F883FEBEBFF6515EB1FB358F
          F893EBFF00F5DE1FFD05ABCABE2D46D17C54D7D5C72670DF9A29FEB5DFFECEFE
          22D374E7D674BBDBA8ADE6B831CB0F9AE143ED0C0804F19E45007B4F8EEE24B3
          F006BF71090B247613107D3E435F120C8E715F716B96F07893C29AAD85ACD14E
          2EADA580346E194315200247B915F10491BDBCAF14AAC9223156561C823820FF
          002A00EF349F8B1E36865B2B18F5965B742912C6208F0146001F77D05743F12E
          F2E350F10786EE6EA4F3256B17DCD803F89BD2BB2F873A47C33F16E8764FFD93
          631EB30A22DC40F232BF983AB019E41C66A3F1BE8DA51F8C9E0ED325B741A7B5
          AC9E644C70B8F9CF393ED4A4AEAC6B426A9D58CDABD9A672DF0C8BAFC43D288C
          8CB383C76D8DFE15A7F18E7797C70226276456B1851F5249AF57D0FC25E11B4B
          F17FA359DB7DA2DD8A878A42DB091823AFA1AF34F8D5A6490F892CF510A7CBB8
          B7F2C9FF006D49FE8C3F2AE495371A5B9F4B86C6C31398A9D9AD2C51F841690D
          CF8DC492804DBDBBC8808FE2C819FC8D7D08073F4AF96FC1BE201E17F135AEA3
          22968798E651D761EBFD0D7D29A6EBBA66AB68B73657B04B130CE55C71EC7D2A
          F0F25CA71E7F4AA7D639EDA3468370A4FB57C8FA94CF73AADDCF2125E49DD9B3
          EA49AFABE1BFB4BB9A5860B98659100DEA92062B9F5C74E95F2EF8AF4D9348F1
          56A5652020A4ECCBC75563B94FE469627E14CDB875A8D59C5EF647AFFC13B78D
          7C29773AA8F31EED958FA80A31FCCD7A25EC2B71653C520051E365607A608AF2
          2F82DE21B7B65BDD0EE2458E4924F3A0DC701B8C30FAF02BD1FC61E20B6F0F78
          7AEAF27750DB0AC499E5D88C0007E35A5392F6679F8FA551E39C7AB7A1F2DC8B
          B66651C00C40C74AFA07E0E4AF27814A31C88EEA455F61C1C7EB5F3F1DC4927A
          F735F497C31D2E4D2FC096492A95927DD3B03D46E3C7E98AE7A17E66CF733E69
          61A317BDCF07F187FC8E7ACFFD7DC83FF1EAF57F8231AFFC237A83E06E377827
          BE022D79478C0FFC567AC7FD7E49FCCD7AD7C11FF9162FC7FD3E1FFD0168A5FC
          40CD2FFD9B1F916FE32283E07572391771E0FE06BC234EFF0090A5A7A79C9C7A
          FCC2BDE7E327FC88B8FF00A7B8FF00AD78369DFF00214B3FFAEE9FFA10A7597E
          F0592BFF006197ABFC8FAE15479600E98E95F2F78E5153C73AD2A00ABF6A7381
          EFCD7D46BC2815F2F78EFF00E47CD687FD3D37F4AD311F0A383879FF00B4CBD3
          F53D2FE0693FD8DAB0EDF6953FF8ED7ABFA57907C18BC82C3C3BADDD5CC82382
          2995DDC83F280BC9AED17E257841D95575A849638036373FA55D169415CE3CCE
          94E78BA8E116F5FD0EB68A6870C011D28AD8F2EE711F18209AE7E176B30DBC52
          4B2B2C7B52352C4FEF17B0AF92CE81AC77D2AFBFF019FF00C2BEC8F1DF8A4F83
          3C2B71AD8B4FB5F92E8BE56FD99DCC075C1F5CD791FF00C34AF1CF8638C63FE3
          F3FF00B0A00F74D2415D1EC4104116F183C74F9457C67E27D0F5693C59ACBA69
          97AC8D7D31565B77208321E7A57ACFFC34B7FD4B1FF939FF00D8527FC34AFF00
          D4B1FF00939FFD850074DFB3EDA5CD9F81EFA3BAB7960737EC42CA854E362738
          22B5FE365BCF77F0BB5086DA092694CB09091A1627122F615C1FFC34B63FE658
          FF00C9CFFEC28FF8696FFA963FF273FF00B0A00F12FEC0D6323FE2557DFF0080
          CFFE15F6F68E1868960194822DA3C823A1DA38AF0EFF008695E31FF08C7FE4E7
          FF006147FC34B73FF22B8FFC0CFF00EC2800F8E9F0E752BED54789F48B592E91
          E20977144BB9D4AF47C0E48C707D31EF5E06C8C8DB58107D08C57BE7FC34B7FD
          4B1FF939FF00D85567FDA1ACE49FCF7F06C0F2FF007DAE016FCF6500765FB3F4
          7227C3A9564474FF004E908C8C64155AE2FE3A7C39B7D34C9E2ED39D634B8942
          5DDBE38F31BF8D7EB8E47AF3DEAD0FDA57031FF08C7FE4E7FF00615CEF8E7E35
          AF8D3C2971A29D0BECA6574712FDA77E36B03D368F4C75EF401E716D6579A7EA
          967F6AB69EDC991594CA853232304671915DFF008E1A5B9F13E830C3E6492AD8
          67E5CB13967E98F6AEAFC37F1C92EA0B0D164F0DA3F930245E635C641DAA0671
          B3F1EB54FC41E301A7FC65B2D6D34D4616BA6853007DA3E60DCE71FED7A76A99
          6DA9BE1B9BDB47955DDCED7E0CD86A9A7DA6A697961716D6F2B472446542BB8F
          20E01E7FBB5DCF8ABC3769E28D165D3AE86DC9DD1C83AA38E86BCD47C7365181
          A00FFC09FF00EC697FE17A3FFD00067FEBE7FF00B1AC23529A5CAD9EB56C0E3A
          A5775942CF7DD1C17883C13AE7872E1D2EECE59615CEDB8854B211EB91D0FD6B
          9D0482769C1EE3FF00D55EBDFF000BC9C8E7C3EA7FEDE7FF00B1AAE7E325BF9B
          E69F0B5B993FBE6519FCF6D60E14EF7523D9A789C7A8A5528DDFA8DF82B1DE43
          AE5F6EB6996DE6B71FBD68C852C1B8E4F5E09AECBE21F80078AA15BDB22B16A5
          0AE14B70245FEE93FC8FBD72CBF1C990617C3EA07A7DA7FF00B1A53F1CDC9FF9
          000FFC09FF00EC6B652A7C9CAD9E655C363DE27EB14E1CAFD51E63A8E8FAAE89
          73E5DF59DC5ACA8721994803DC30A81A7BED52545796E6F24CE1016690FE02BD
          4E4F8E1E6A957F0EA329ECD7191FFA0D4717C698A1398BC310A1FF0066703F92
          D63CB0FE63D3588C6DAF2A377EA8ABE08F85B7D7D770DF6BD035AD9A10C2DD86
          1E5F623B0E95EE40044000C018181D2BC7BFE17A3FFD0007FE04FF00F6347FC2
          F47FFA000FFC09FF00EC6B784E9C15933C7C661331C5CF9AA476E9A1C278BB4E
          BF93C61ABBC76374EAD77210CB031046E3D0E2BD53E0BDBCF6DE1BBE5B882589
          8DD9204885491B57D6B10FC7263D7C3EBFF813FF00D8D28F8E6E3A787D7FF027
          FF00B1A883A719735CEAC4D3C757C3AA1ECEDF3EC74FF1820967F046C8229257
          FB54676A2927BFA5786E9DA66A0353B4CD8DD00274C930B71F30F6AF4B3F1C9C
          F5D007FE04FF00F63487E38923FE45F5FF00C09FFEC689BA7295EE183863F0B4
          9D254EF7F347B28CEC1F4AF99FC73A75F4BE37D6244B2BA646B862ACB0B1047B
          1C576BFF000BCDCFFCCBE3FF00027FFB1A4FF85E4FCE7401F5FB4FFF00635552
          74EA2B366180C263B0951CD53BDFCCADE04B3B98BE1CF8B6392DA6491E36D8AD
          190CDF21E8315E756BA66A02EE0DD6176079899CC0E3B8F6AFA13C0BE34FF84D
          2DAF656B01686DDD57024DFBB233E82BAF0817A629FB15349A644B34AB87AB51
          4E1EF4BF0D06C43080631453C023BD15D173C1391F89DE1BBFF16781EEB48D37
          CAFB54B246CBE6B6D5C0604F3835E0BFF0CFBE36FF00A86FFE041FFE26BEA934
          0A607CADFF000CFBE36FFA86FF00E041FF00E268FF00867DF1B7FD437FF020FF
          00F135F54D1401F2B7FC33EF8DBFEA1BFF008107FF0089A3FE19F7C6DFF50DFF
          00C083FF00C4D7D5345007CADFF0CFBE36FF00A86FFE041FFE268FF867DF1B7F
          D437FF00020FFF00135F54D1401F2B7FC33EF8DBFEA1BFF8107FF89A3FE19F7C
          6DFF0050DFFC083FFC4D7D5345007CADFF000CFBE36FFA86FF00E041FF00E26A
          96AFF047C5DA269177A9DD8B036F6B134D2797392DB5464E06DE6BEB6ACFD76C
          BFB4740D46C719FB4DACB163FDE523FAD007CCBE02F853E24D4E2B0F10DAB591
          B19D4B2E67C301920E463D455CB8F0A6ABE24F8A1ADE9B67E4B4F636F1AC9BDF
          0B8014119C7A9AF4CF80B7C6E3E1DFD91C9F32CAEE58594F6070DFD4D51F83FF
          00F136F17F8E75FCE52E2F8451B762A19CFF002DB4A4935666B46ACA9545523B
          A399FF00853BE2A3CEDB2FFBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F428
          1C52D61F5781EAFF006F62FCBEE3E79FF8539E29FEE597FDFEFF00EB51FF000A
          73C55FDCB2FF00BFDFFD6AFA1A8A3EAF00FEDEC5F97DC7CF3FF0A73C55FDCB2F
          FBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F435147D5E01FDBD8BF2FB8F9E
          7FE14E78ABFB965FF7FBFF00AD47FC29CF157F72CBFEFF007FF5ABE86A28FABC
          03FB7B17E5F71F3CFF00C29CF157F72CBFEFF7FF005A8FF8539E2AFEE597FDFE
          FF00EB57D0D451F57807F6F62FCBEE3E79FF008539E2AFEE597FDFEFFEB51FF0
          A73C55FDCB2FFBFDFF00D6AFA1A8A3EAF00FEDEC5F97DC7CF3FF000A73C55FDC
          B2FF00BFDFFD6A3FE14E78ABFB965FF7FBFF00AD5F435147D5E01FDBD8BF2FB8
          F9E7FE14E78ABFB965FF007FBFFAD49FF0A7BC54380965FF007FBFFAD5F43D21
          147D5E01FDBD8BF2FB8E03E18784753F0ADAEA09A97921A791193CB7DDD011E9
          5DFD18A3A56D18F2AB1E5D7AD3AF51D49EEC75145154622514668A0028A28A43
          0A28A2800A28A2800A28A2800A43D3814B471401F3CC7E235F855E28F1D69371
          B923BC8CDDE9BF2FCAD23676E3DBE6FF00C70D7A2FC18D024D0BE1D5A1B88CA5
          CDF3B5DC80820E1B1B73FF000103F3AEA757F0B687AF5C5BDC6ABA65B5E4D6FF
          00EA9E58C12BDF03DB2338AD64458C61781D801D298875145148614514500145
          145001451450014514500145145001451450014514500145145002D149914531
          1FFFD9}
        Stretch = True
      end
      object RLImage2: TRLImage
        Left = 103
        Top = 2
        Width = 68
        Height = 26
        Stretch = True
      end
    end
    object RLBand2: TRLBand
      Left = 19
      Top = 50
      Width = 756
      Height = 1046
      Margins.TopMargin = 1.000000000000000000
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = True
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLLabel39: TRLLabel
        Left = 16
        Top = 769
        Width = 16
        Height = 8
        Alignment = taCenter
        Caption = 'PBR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw40: TRLDraw
        Left = 3
        Top = 940
        Width = 743
        Height = 67
      end
      object RLDraw1: TRLDraw
        Left = 1
        Top = 25
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw2: TRLDraw
        Left = 1
        Top = 54
        Width = 500
        Height = 1
        DrawKind = dkLine
      end
      object RLDraw3: TRLDraw
        Left = 1
        Top = 77
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw4: TRLDraw
        Left = 500
        Top = 27
        Width = 1
        Height = 52
      end
      object RLDraw5: TRLDraw
        Left = 585
        Top = 5
        Width = 1
        Height = 22
      end
      object RLDraw6: TRLDraw
        Left = 265
        Top = 5
        Width = 1
        Height = 74
      end
      object RLDraw7: TRLDraw
        Left = 500
        Top = 39
        Width = 245
        Height = 1
        DrawKind = dkLine
      end
      object RLLabel2: TRLLabel
        Left = 2
        Top = 5
        Width = 74
        Height = 8
        Caption = 'NOME DO MOTORISTA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel4: TRLLabel
        Left = 589
        Top = 5
        Width = 20
        Height = 8
        Caption = 'DATA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel5: TRLLabel
        Left = 2
        Top = 28
        Width = 69
        Height = 8
        Caption = 'TRANSPORTADORA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel6: TRLLabel
        Left = 268
        Top = 28
        Width = 60
        Height = 8
        Caption = 'N'#186' DO MANIFESTO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel7: TRLLabel
        Left = 552
        Top = 28
        Width = 149
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 1
        Top = 55
        Width = 58
        Height = 8
        Caption = 'TIPO DE VE'#205'CULO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 267
        Top = 55
        Width = 70
        Height = 8
        Caption = 'PLACA DA CARRETA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel3: TRLLabel
        Left = 268
        Top = 5
        Width = 66
        Height = 8
        Caption = 'N'#218'MERO DO LACRE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw9: TRLDraw
        Left = 1
        Top = 103
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw10: TRLDraw
        Left = 120
        Top = 123
        Width = 625
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel11: TRLLabel
        Left = 42
        Top = 88
        Width = 32
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'LOJA:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw11: TRLDraw
        Left = 1
        Top = 159
        Width = 744
        Height = 4
        DrawKind = dkLine
      end
      object RLDraw12: TRLDraw
        Left = 539
        Top = 105
        Width = 1
        Height = 55
      end
      object RLDraw14: TRLDraw
        Left = 257
        Top = 105
        Width = 1
        Height = 55
      end
      object RLDraw15: TRLDraw
        Left = 404
        Top = 105
        Width = 1
        Height = 55
      end
      object RLLabel12: TRLLabel
        Left = 551
        Top = 112
        Width = 136
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel13: TRLLabel
        Left = 127
        Top = 110
        Width = 120
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'CHEGADA LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel14: TRLLabel
        Left = 279
        Top = 112
        Width = 100
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'SA'#205'DA LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel15: TRLLabel
        Left = 422
        Top = 112
        Width = 98
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'CHEGADA CD'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw16: TRLDraw
        Left = 1
        Top = 162
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw17: TRLDraw
        Left = 1
        Top = 189
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel16: TRLLabel
        Left = 8
        Top = 166
        Width = 732
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Caption = 'CONTROLE DE PALETES'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object RLLabel17: TRLLabel
        Left = 10
        Top = 177
        Width = 728
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Caption = 
          '(*) Os campos abaixo s'#227'o de preenchimento obrigat'#243'rio e os dados' +
          ' informados ser'#227'o utilizados para fins de medi'#231#227'o; controle e re' +
          'sponsabiliza'#231#227'o por poss'#237'veis desvios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic, fsUnderline]
        ParentFont = False
      end
      object RLDraw18: TRLDraw
        Left = 1
        Top = 203
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel18: TRLLabel
        Left = 1
        Top = 194
        Width = 166
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREENCHIMENTO INTECOM'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel19: TRLLabel
        Left = 172
        Top = 194
        Width = 168
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREENCHIMENTO LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel20: TRLLabel
        Left = 346
        Top = 194
        Width = 394
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'INFORMA'#199#213'ES COMPLEMENTARES'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw19: TRLDraw
        Left = 1
        Top = 216
        Width = 340
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel21: TRLLabel
        Left = 1
        Top = 207
        Width = 166
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'QTDE PALETES EXPEDIDOS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel22: TRLLabel
        Left = 173
        Top = 207
        Width = 168
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'QTDE PALETES DEVOLVIDOS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw20: TRLDraw
        Left = 1
        Top = 230
        Width = 341
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw21: TRLDraw
        Left = 517
        Top = 204
        Width = 1
        Height = 57
      end
      object RLDraw22: TRLDraw
        Left = 1
        Top = 261
        Width = 744
        Height = 3
        DrawKind = dkLine
      end
      object RLLabel25: TRLLabel
        Left = 347
        Top = 207
        Width = 102
        Height = 8
        Caption = 'N'#186' DO LACRE (RETORNO LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw23: TRLDraw
        Left = 49
        Top = 219
        Width = 1
        Height = 43
      end
      object RLDraw24: TRLDraw
        Left = 102
        Top = 217
        Width = 1
        Height = 45
      end
      object RLDraw25: TRLDraw
        Left = 169
        Top = 191
        Width = 1
        Height = 70
      end
      object RLDraw28: TRLDraw
        Left = 341
        Top = 191
        Width = 1
        Height = 70
      end
      object RLLabel26: TRLLabel
        Left = 520
        Top = 208
        Width = 136
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw29: TRLDraw
        Left = 2
        Top = 295
        Width = 744
        Height = 89
      end
      object RLDraw26: TRLDraw
        Left = 268
        Top = 218
        Width = 1
        Height = 44
      end
      object RLDraw27: TRLDraw
        Left = 218
        Top = 218
        Width = 1
        Height = 44
      end
      object RLLabel27: TRLLabel
        Left = 6
        Top = 298
        Width = 159
        Height = 14
        Caption = 'OBSERVA'#199#213'ES IMPORTANTES:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri Light'
        Font.Style = []
        ParentFont = False
      end
      object RLMemo1: TRLMemo
        Left = 4
        Top = 311
        Width = 726
        Height = 70
        Behavior = [beSiteExpander]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = []
        Lines.Strings = (
          '1) CHEGAR NO CD SEMPRE 15 MINUTOS ANTES DO INICIO DA VIAGEM'
          
            '2) AO CHEGAR NA LOJA O MONITORAMENTO DEVE SER AVISADO IMEDIATAME' +
            'NTE'
          
            '3) INFORMAR A CENTRAL DE MONITORAMENTO SOBRE QUALQUER PROBLEMA D' +
            'URANTE A OPERA'#199#195'O'
          
            '4) '#201' OBRIGAT'#211'RIO O RETORNO DE PALETES (LOJA X INTECOM) EM TODAS ' +
            'AS VIAGENS. AO SINAL DE PROBLEMAS O CRM DEVER'#193' SER AVISADO IMEDI' +
            'ATAMENTE'
          
            '5) '#201' OBRIGAT'#211'RIO O RETORNO DOS VE'#205'CULOS LACRADOS (LOJA X INTECOM' +
            ') EM TODAS AS VIAGENS'
          
            '6) ENTREGAR O DI'#193'RIO DE BORDO TODOS OS DIAS PARA O L'#205'DER DE TRAN' +
            'SPORTE DEVIDAMENTE PREENCHIDO'
          '7) RADIO DE CONTATO COM O MONITORAMENTO: 7*707358')
        ParentFont = False
      end
      object RLDraw30: TRLDraw
        Left = 2
        Top = 385
        Width = 744
        Height = 69
      end
      object RLDraw31: TRLDraw
        Left = 253
        Top = 387
        Width = 1
        Height = 65
      end
      object RLDraw32: TRLDraw
        Left = 512
        Top = 387
        Width = 1
        Height = 65
      end
      object RLLabel28: TRLLabel
        Left = 37
        Top = 439
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREVEN'#199#195'O DE PERDAS (LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel29: TRLLabel
        Left = 299
        Top = 439
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'RESP. TRANSPORTE (CD INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel30: TRLLabel
        Left = 559
        Top = 439
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'RESP. CRM (CD INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw33: TRLDraw
        Left = 10
        Top = 430
        Width = 226
        Height = 9
        DrawKind = dkLine
      end
      object RLDraw34: TRLDraw
        Left = 273
        Top = 430
        Width = 226
        Height = 9
        DrawKind = dkLine
      end
      object RLDraw35: TRLDraw
        Left = 525
        Top = 430
        Width = 218
        Height = 9
        DrawKind = dkLine
      end
      object RLDraw36: TRLDraw
        Left = 11
        Top = 984
        Width = 226
        Height = 9
        DrawKind = dkLine
      end
      object RLLabel31: TRLLabel
        Left = 37
        Top = 993
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREVEN'#199#195'O DE PERDAS (LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw37: TRLDraw
        Left = 267
        Top = 984
        Width = 226
        Height = 9
        DrawKind = dkLine
      end
      object RLLabel32: TRLLabel
        Left = 293
        Top = 993
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'RESP. TRANSPORTE (CD INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel33: TRLLabel
        Left = 552
        Top = 993
        Width = 166
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'RESP. CRM (CD INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw38: TRLDraw
        Left = 519
        Top = 984
        Width = 218
        Height = 9
        DrawKind = dkLine
      end
      object RLDraw39: TRLDraw
        Left = 253
        Top = 942
        Width = 1
        Height = 64
      end
      object RLDraw41: TRLDraw
        Left = 512
        Top = 942
        Width = 1
        Height = 63
      end
      object RLDraw42: TRLDraw
        Left = 2
        Top = 849
        Width = 743
        Height = 89
      end
      object RLMemo2: TRLMemo
        Left = 4
        Top = 867
        Width = 732
        Height = 70
        Behavior = [beSiteExpander]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = []
        Lines.Strings = (
          '1) CHEGAR NO CD SEMPRE 15 MINUTOS ANTES DO INICIO DA VIAGEM'
          
            '2) AO CHEGAR NA LOJA O MONITORAMENTO DEVE SER AVISADO IMEDIATAME' +
            'NTE'
          
            '3) INFORMAR A CENTRAL DE MONITORAMENTO SOBRE QUALQUER PROBLEMA D' +
            'URANTE A OPERA'#199#195'O'
          
            '4) '#201' OBRIGAT'#211'RIO O RETORNO DE PALETES (LOJA X INTECOM) EM TODAS ' +
            'AS VIAGENS. AO SINAL DE PROBLEMAS O CRM DEVER'#193' SER AVISADO IMEDI' +
            'ATAMENTE'
          
            '5) '#201' OBRIGAT'#211'RIO O RETORNO DOS VE'#205'CULOS LACRADOS (LOJA X INTECOM' +
            ') EM TODAS AS VIAGENS'
          
            '6) ENTREGAR O DI'#193'RIO DE BORDO TODOS OS DIAS PARA O L'#205'DER DE TRAN' +
            'SPORTE DEVIDAMENTE PREENCHIDO'
          '7) RADIO DE CONTATO COM O MONITORAMENTO: 7*707358')
        ParentFont = False
      end
      object RLLabel34: TRLLabel
        Left = 4
        Top = 851
        Width = 159
        Height = 14
        Caption = 'OBSERVA'#199#213'ES IMPORTANTES:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri Light'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel43: TRLLabel
        Left = 5
        Top = 712
        Width = 732
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Caption = 'CONTROLE DE PALETES'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object RLDraw47: TRLDraw
        Left = 1
        Top = 751
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel40: TRLLabel
        Left = 2
        Top = 756
        Width = 166
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'QTDE PALETES EXPEDIDOS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel41: TRLLabel
        Left = 2
        Top = 741
        Width = 166
        Height = 10
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREENCHIMENTO INTECOM'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel42: TRLLabel
        Left = 172
        Top = 741
        Width = 168
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'PREENCHIMENTO LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel37: TRLLabel
        Left = 172
        Top = 756
        Width = 168
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'QTDE PALETES DEVOLVIDOS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel45: TRLLabel
        Left = 345
        Top = 741
        Width = 394
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'INFORMA'#199#213'ES COMPLEMENTARES'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel44: TRLLabel
        Left = 2
        Top = 723
        Width = 728
        Height = 14
        Alignment = taCenter
        AutoSize = False
        Caption = 
          '(*) Os campos abaixo s'#227'o de preenchimento obrigat'#243'rio e os dados' +
          ' informados ser'#227'o utilizados para fins de medi'#231#227'o; controle e re' +
          'sponsabiliza'#231#227'o por poss'#237'veis desvios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic, fsUnderline]
        ParentFont = False
      end
      object RLDraw46: TRLDraw
        Left = 1
        Top = 811
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel35: TRLLabel
        Left = 347
        Top = 755
        Width = 102
        Height = 8
        Caption = 'N'#186' DO LACRE (RETORNO LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel36: TRLLabel
        Left = 520
        Top = 756
        Width = 136
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw44: TRLDraw
        Left = 1
        Top = 778
        Width = 341
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw45: TRLDraw
        Left = 1
        Top = 764
        Width = 340
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw50: TRLDraw
        Left = 102
        Top = 766
        Width = 1
        Height = 47
      end
      object RLDraw49: TRLDraw
        Left = 49
        Top = 766
        Width = 1
        Height = 47
      end
      object RLDraw48: TRLDraw
        Left = 1
        Top = 736
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw52: TRLDraw
        Left = 341
        Top = 740
        Width = 1
        Height = 73
      end
      object RLDraw53: TRLDraw
        Left = 517
        Top = 752
        Width = 1
        Height = 61
      end
      object RLDraw51: TRLDraw
        Left = 170
        Top = 738
        Width = 1
        Height = 78
      end
      object RLDraw54: TRLDraw
        Left = 1
        Top = 706
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw57: TRLDraw
        Left = 1
        Top = 628
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw63: TRLDraw
        Left = 0
        Top = 569
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel52: TRLLabel
        Left = 5
        Top = 574
        Width = 69
        Height = 8
        Caption = 'TRANSPORTADORA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel53: TRLLabel
        Left = 4
        Top = 602
        Width = 58
        Height = 8
        Caption = 'TIPO DE VE'#205'CULO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw64: TRLDraw
        Left = 0
        Top = 624
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel54: TRLLabel
        Left = 270
        Top = 602
        Width = 70
        Height = 8
        Caption = 'PLACA DA CARRETA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel55: TRLLabel
        Left = 271
        Top = 574
        Width = 60
        Height = 8
        Caption = 'N'#186' DO MANIFESTO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw65: TRLDraw
        Left = 500
        Top = 589
        Width = 246
        Height = 1
        DrawKind = dkLine
      end
      object RLDraw66: TRLDraw
        Left = 265
        Top = 551
        Width = 1
        Height = 75
      end
      object RLDraw67: TRLDraw
        Left = 500
        Top = 571
        Width = 1
        Height = 52
      end
      object RLDraw68: TRLDraw
        Left = 1
        Top = 599
        Width = 500
        Height = 1
        DrawKind = dkLine
      end
      object RLLabel58: TRLLabel
        Left = 555
        Top = 574
        Width = 149
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. INTECOM)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel59: TRLLabel
        Left = 592
        Top = 552
        Width = 20
        Height = 8
        Caption = 'DATA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel57: TRLLabel
        Left = 271
        Top = 552
        Width = 66
        Height = 8
        Caption = 'N'#218'MERO DO LACRE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw70: TRLDraw
        Left = 585
        Top = 549
        Width = 1
        Height = 22
      end
      object RLDraw71: TRLDraw
        Left = 2
        Top = 515
        Width = 743
        Height = 31
      end
      object RLImage3: TRLImage
        Left = 5
        Top = 517
        Width = 83
        Height = 28
        Picture.Data = {
          0A544A504547496D61676514180000FFD8FFE000104A46494600010101006000
          600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC0001108006F010603012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7E0
          3145145001451450014514500145145001451450014138F5A290E3193C0F5A00
          E0BC65F16F41F056B306977B0DD5CDC3AABCBF670A44287A139239EF8F4FA8AE
          DAC6FADB52B082F6D2512DB5C46B2C5229E1948C835E23E02D120F1F5F78FB5D
          BD4F306A0EF636EC7F857A8DBF4C45F9574DF023549AEFC07269D704F9FA5DDC
          96C41EC3EF01F9923F0A00F50A28A2800A28A2800A28A2800A28A2800A28A280
          0A28A2800A28A2800A28A2800A28A2800A28A2802A6A9AA59E8DA6CFA85FCEB0
          5AC0A5E491BA015E6569FB41784AE35416B25BEA16F6ECDB45DC91AEC1EE4039
          03F0AE87E2DE877DE21F875A85969C864B9529308875902B0240F7C57C9367A4
          6A37DA88D3ED6CEE25BB67DA2258CEF073DFD3B7E7401F75C722CB1AC88C1918
          65594E411D8834EAC9F0C69D3693E17D2B4FB87DD3DB5A45139F75500FEB9AD6
          A430A28A3F0A0028A28A0028AF39F15FC68F0D785B50B9D35A3BBBCD42DDB6BC
          3047800E3A163F5ED9AB3F0CFE21CBF1061D52E1B4F4B38AD65448D4485D9B70
          279381E82803BDAC8F14DF7F66784B58BECE0DBD9CD203EE1091FAD6BD715F16
          EE0DB7C2CD7DC36D2D008FEBB9D57FAD0065FC0BB1FB1FC2EB1936E1AEE69666
          E319F98A8FD14564FC2E234CF8A1E3CD141010DC0B945C6382CDFF00C5AD769F
          0D6DFECBF0D7C3F1F5FF004347E9DD86EFEB5C5E921ACBF697D6A1DC36DE6981
          F18EB858CFF306811EBE28A404E7914B40C28A4CD00E7B5002D1499A01CD002D
          1466933400B451499A005A2933403400B45145001452679A33400B45213E9450
          03A8A28A62313C5BE2387C27E1BBAD6AE209278ED82931C64066CB05EFF5AF29
          1FB45E81E6994787AF44878DDBA3CFD335DA7C66FF009251AD73FC31FF00E8C5
          AF8FBBD007DED6974B75670DCA8C09515C29233C8CD790DE7ED11A3D95F5C5AC
          9A15F9782468D8874EA091FD2BD5F471FF00125B0E7FE5DA3FFD0457C4DE28C0
          F176B43D2FE7FF00D18D401F555F7C5BF0F699E10D3F5FBD32C66FE2324164B8
          695B9C74E98CF7AE01BF6954F3F0BE1763167BDE0DD8FA6CAF2DF07F82B5FF00
          8877A2D6CE402DECE308F7370C7CB854E485E33C9E78AB5E3BF85FACF80E282E
          6F2586E6CE76F2D67832007EBB581031C67F2A00FA5BC15F11743F1CDAB369D2
          3457718CCB693E0489EFC751EE2A7F1CF8D2DBC0DA126AB776935CC6F3AC0122
          601B24139E7FDDAF91BC23AFDC786BC57A76AD6EE50C332EF038DD19E194FB11
          9AFAAFE24783AE3C7BE168B4BB4BB8AD9BED093F992292080A78E3FDEA00F953
          C65AEC5E25F16EA3AC410BC315DCBE62C6E412BC639C7D2BB3F855F13EC3E1FD
          8EA56F79A7DCDD35DCA8EA616501768239CFD6B85F136872F86FC477BA34D324
          F25A49B1A4404293807807EB5D3FC3EF85F79F102D2F67B5D460B516AEA8C254
          2DBB7027231F4A00FA57C07E39B5F1E6913EA3696735AC70CE602B330249DA0E
          78FAD73FF1DAEA387E17DDC2D22ABCF3C28AB9196C382703BF4AD1F867E09B8F
          87FE1DBBB0BCBD86E0C970D71E646A55546D031CFF00BB9AF9BBE2478CEE7C6B
          E2DB9B932B7D820768ACE2CF0230701B1EA7AFFF00AA803D2B4DF8FB6BA7E9FA
          568FA7681248B04115B99669C27214038500FA7AD3EFBC556765F1CF49D7EFD0
          DB412E998942FCD8C8700FD3A554F86DF04ADF57D1AD75DD7AEAE62F3C096DED
          A0C29D9D998907AF503D2A97C5CD1D345F1BE8B04523BAFD80ED66FBD80CF4A4
          ECAE6B429AA956307D59F46DA5DC17D6A9736D2A4B0C8BB91D1B208F6353EEC5
          78B7C18F11C897D73E1F9DF313A19EDC1FE123EF01EC739FC2BA8F8A9E2E9BC3
          DA3C5656527977B7B901C758D00E587BF38AC9554E1CCCEAAB9754862BEAD1FE
          91A1E23F899A0F87A67B6777BABB5EB05B80C54FA13D0571ADF1CCEF3B341CA7
          62D7383FFA0D792430CD773243123CB34AC02AAF2598FF0033935E97A77C13D5
          2E6D965BDD4EDEDA52326258CBEDFA9E2B0F6B5277E53DC9E5D97E112FAC3BB7
          FD743BBF05FC4883C5F7D2590D367B69638CC858B064C640EBC7AD76575776F6
          503DC5CCA90C2832CEED800570FF000FFC0171E0EBCD426B8B986E3CF4444745
          20E0139041FA8AF3FF008B3E2C9B54D6E4D16090AD9599DAEAA7FD649DF3F4AD
          B9DC21791E547074F158B74F0FF07F5DCEC356F8D3A45A4CD1E99673DFED38F3
          33E5A1FA120923F0ACD87E39AB4AA27D05D63CF263B80C7F22A3F9D79FF83FC2
          379E2FD51EDE191608221BA79D86420F403B9AE9FC5BF09A7D07487D474FBE6B
          C8E0199E2910060BDC8F5FA560A75A4B996C7AB2C1E594A6A8547793F53D67C3
          7E34D1BC53016D3E7FDEA0CBC120DB22FE1FE15D0641AF9234DD4AE748D4A1BF
          B390C7710386523B8EE0FA8AFA8FC3DAC47AF787ECF538800B3C418A839DADDC
          7E0735BD2ABCEB5DCF2B33CB7EA724E2EF16711A97C64D3B4DD4EEAC64D2EED9
          EDE5688B065009071EBED5B33FC4AD16D3C3169ACDCF9886ED4986D460C8D838
          F5F5AF09F1780BE32D63D3ED721FD4D4DE1AF0C6ABE31BC16B68E0456EB86966
          24A4409271F5CE6B1F6D3E67147AB2C9F0AA8C6AC9F2ADDFDC7A1B7C735F3B0B
          A0318B38E6E006FCB6E3F5AEF3C33E35D2BC5362F73692344F08CCD0CD8568FD
          CFB7079AF06F16F81F52F083C2D752473DBCDC24B10E377A107A573D6F77716C
          9324133C42788C720538DEA71F29F63C7142AF38BB48B965184C4D253C33B799
          EE1AE7C66D26C2E9A0D36CDF512A70D287D91E7D8E0E699A27C68D3AFAF12DF5
          2B17B00E76ACDE66F407DCE060571769F08F5FBBD205E996D6195977A5B36771
          1D813D05707246D04AF14B1959118AB2B0C1041C1068955AB1777B0A8E5780AD
          170A6EF25BB3EBE465951591815232083D69F8E6B81F845ABCBA9F8356199CBC
          96729837139257AAE7F038FC2BBEEF5D709732B9F315E8BA35654DEE87514515
          462705F19FFE4946B7FEEC7FFA316BE3EAFB07E33FFC928D6FFDD8FF00F462D7
          C7D401F7868FFF00205B0FFAF68FFF004115F12F8A4E3C61AD1FFA7F9FFF0046
          357DB5A3FF00C816C3FEBDA3FF00D0457C4BE2AFF91BF5BFFAFF009FFF004635
          007D0BFB39C683C0DA8385018EA0C09C724044E3F9D6CFC758D5BE155FB32862
          B340413D8F98067F226B23F674FF009112FBFEC20FFF00A0256CFC74FF009251
          A8FF00D7683FF462D007C959E6BEEFD218BE8D60CC492D6F1927EAA2BE0FEF5F
          77E8DFF203D3BFEBDA3FFD045007C83F15BFE4A8F883FEBEBFF6515EB1FB358F
          F893EBFF00F5DE1FFD05ABCABE2D46D17C54D7D5C72670DF9A29FEB5DFFECEFE
          22D374E7D674BBDBA8ADE6B831CB0F9AE143ED0C0804F19E45007B4F8EEE24B3
          F006BF71090B247613107D3E435F120C8E715F716B96F07893C29AAD85ACD14E
          2EADA580346E194315200247B915F10491BDBCAF14AAC9223156561C823820FF
          002A00EF349F8B1E36865B2B18F5965B742912C6208F0146001F77D05743F12E
          F2E350F10786EE6EA4F3256B17DCD803F89BD2BB2F873A47C33F16E8764FFD93
          631EB30A22DC40F232BF983AB019E41C66A3F1BE8DA51F8C9E0ED325B741A7B5
          AC9E644C70B8F9CF393ED4A4AEAC6B426A9D58CDABD9A672DF0C8BAFC43D288C
          8CB383C76D8DFE15A7F18E7797C70226276456B1851F5249AF57D0FC25E11B4B
          F17FA359DB7DA2DD8A878A42DB091823AFA1AF34F8D5A6490F892CF510A7CBB8
          B7F2C9FF006D49FE8C3F2AE495371A5B9F4B86C6C31398A9D9AD2C51F841690D
          CF8DC492804DBDBBC8808FE2C819FC8D7D08073F4AF96FC1BE201E17F135AEA3
          22968798E651D761EBFD0D7D29A6EBBA66AB68B73657B04B130CE55C71EC7D2A
          F0F25CA71E7F4AA7D639EDA3468370A4FB57C8FA94CF73AADDCF2125E49DD9B3
          EA49AFABE1BFB4BB9A5860B98659100DEA92062B9F5C74E95F2EF8AF4D9348F1
          56A5652020A4ECCBC75563B94FE469627E14CDB875A8D59C5EF647AFFC13B78D
          7C29773AA8F31EED958FA80A31FCCD7A25EC2B71653C520051E365607A608AF2
          2F82DE21B7B65BDD0EE2458E4924F3A0DC701B8C30FAF02BD1FC61E20B6F0F78
          7AEAF27750DB0AC499E5D88C0007E35A5392F6679F8FA551E39C7AB7A1F2DC8B
          B66651C00C40C74AFA07E0E4AF27814A31C88EEA455F61C1C7EB5F3F1DC4927A
          F735F497C31D2E4D2FC096492A95927DD3B03D46E3C7E98AE7A17E66CF733E69
          61A317BDCF07F187FC8E7ACFFD7DC83FF1EAF57F8231AFFC237A83E06E377827
          BE022D79478C0FFC567AC7FD7E49FCCD7AD7C11FF9162FC7FD3E1FFD0168A5FC
          40CD2FFD9B1F916FE32283E07572391771E0FE06BC234EFF0090A5A7A79C9C7A
          FCC2BDE7E327FC88B8FF00A7B8FF00AD78369DFF00214B3FFAEE9FFA10A7597E
          F0592BFF006197ABFC8FAE15479600E98E95F2F78E5153C73AD2A00ABF6A7381
          EFCD7D46BC2815F2F78EFF00E47CD687FD3D37F4AD311F0A383879FF00B4CBD3
          F53D2FE0693FD8DAB0EDF6953FF8ED7ABFA57907C18BC82C3C3BADDD5CC82382
          2995DDC83F280BC9AED17E257841D95575A849638036373FA55D169415CE3CCE
          94E78BA8E116F5FD0EB68A6870C011D28AD8F2EE711F18209AE7E176B30DBC52
          4B2B2C7B52352C4FEF17B0AF92CE81AC77D2AFBFF019FF00C2BEC8F1DF8A4F83
          3C2B71AD8B4FB5F92E8BE56FD99DCC075C1F5CD791FF00C34AF1CF8638C63FE3
          F3FF00B0A00F74D2415D1EC4104116F183C74F9457C67E27D0F5693C59ACBA69
          97AC8D7D31565B77208321E7A57ACFFC34B7FD4B1FF939FF00D8527FC34AFF00
          D4B1FF00939FFD850074DFB3EDA5CD9F81EFA3BAB7960737EC42CA854E362738
          22B5FE365BCF77F0BB5086DA092694CB09091A1627122F615C1FFC34B63FE658
          FF00C9CFFEC28FF8696FFA963FF273FF00B0A00F12FEC0D6323FE2557DFF0080
          CFFE15F6F68E1868960194822DA3C823A1DA38AF0EFF008695E31FF08C7FE4E7
          FF006147FC34B73FF22B8FFC0CFF00EC2800F8E9F0E752BED54789F48B592E91
          E20977144BB9D4AF47C0E48C707D31EF5E06C8C8DB58107D08C57BE7FC34B7FD
          4B1FF939FF00D85567FDA1ACE49FCF7F06C0F2FF007DAE016FCF6500765FB3F4
          7227C3A9564474FF004E908C8C64155AE2FE3A7C39B7D34C9E2ED39D634B8942
          5DDBE38F31BF8D7EB8E47AF3DEAD0FDA57031FF08C7FE4E7FF00615CEF8E7E35
          AF8D3C2971A29D0BECA6574712FDA77E36B03D368F4C75EF401E716D6579A7EA
          967F6AB69EDC991594CA853232304671915DFF008E1A5B9F13E830C3E6492AD8
          67E5CB13967E98F6AEAFC37F1C92EA0B0D164F0DA3F930245E635C641DAA0671
          B3F1EB54FC41E301A7FC65B2D6D34D4616BA6853007DA3E60DCE71FED7A76A99
          6DA9BE1B9BDB47955DDCED7E0CD86A9A7DA6A697961716D6F2B472446542BB8F
          20E01E7FBB5DCF8ABC3769E28D165D3AE86DC9DD1C83AA38E86BCD47C7365181
          A00FFC09FF00EC697FE17A3FFD00067FEBE7FF00B1AC23529A5CAD9EB56C0E3A
          A5775942CF7DD1C17883C13AE7872E1D2EECE59615CEDB8854B211EB91D0FD6B
          9D0482769C1EE3FF00D55EBDFF000BC9C8E7C3EA7FEDE7FF00B1AAE7E325BF9B
          E69F0B5B993FBE6519FCF6D60E14EF7523D9A789C7A8A5528DDFA8DF82B1DE43
          AE5F6EB6996DE6B71FBD68C852C1B8E4F5E09AECBE21F80078AA15BDB22B16A5
          0AE14B70245FEE93FC8FBD72CBF1C990617C3EA07A7DA7FF00B1A53F1CDC9FF9
          000FFC09FF00EC6B652A7C9CAD9E655C363DE27EB14E1CAFD51E63A8E8FAAE89
          73E5DF59DC5ACA8721994803DC30A81A7BED52545796E6F24CE1016690FE02BD
          4E4F8E1E6A957F0EA329ECD7191FFA0D4717C698A1398BC310A1FF0066703F92
          D63CB0FE63D3588C6DAF2A377EA8ABE08F85B7D7D770DF6BD035AD9A10C2DD86
          1E5F623B0E95EE40044000C018181D2BC7BFE17A3FFD0007FE04FF00F6347FC2
          F47FFA000FFC09FF00EC6B784E9C15933C7C661331C5CF9AA476E9A1C278BB4E
          BF93C61ABBC76374EAD77210CB031046E3D0E2BD53E0BDBCF6DE1BBE5B882589
          8DD9204885491B57D6B10FC7263D7C3EBFF813FF00D8D28F8E6E3A787D7FF027
          FF00B1A883A719735CEAC4D3C757C3AA1ECEDF3EC74FF1820967F046C8229257
          FB54676A2927BFA5786E9DA66A0353B4CD8DD00274C930B71F30F6AF4B3F1C9C
          F5D007FE04FF00F63487E38923FE45F5FF00C09FFEC689BA7295EE183863F0B4
          9D254EF7F347B28CEC1F4AF99FC73A75F4BE37D6244B2BA646B862ACB0B1047B
          1C576BFF000BCDCFFCCBE3FF00027FFB1A4FF85E4FCE7401F5FB4FFF00635552
          74EA2B366180C263B0951CD53BDFCCADE04B3B98BE1CF8B6392DA6491E36D8AD
          190CDF21E8315E756BA66A02EE0DD6176079899CC0E3B8F6AFA13C0BE34FF84D
          2DAF656B01686DDD57024DFBB233E82BAF0817A629FB15349A644B34AB87AB51
          4E1EF4BF0D06C43080631453C023BD15D173C1391F89DE1BBFF16781EEB48D37
          CAFB54B246CBE6B6D5C0604F3835E0BFF0CFBE36FF00A86FFE041FFE26BEA934
          0A607CADFF000CFBE36FFA86FF00E041FF00E268FF00867DF1B7FD437FF020FF
          00F135F54D1401F2B7FC33EF8DBFEA1BFF008107FF0089A3FE19F7C6DFF50DFF
          00C083FF00C4D7D5345007CADFF0CFBE36FF00A86FFE041FFE268FF867DF1B7F
          D437FF00020FFF00135F54D1401F2B7FC33EF8DBFEA1BFF8107FF89A3FE19F7C
          6DFF0050DFFC083FFC4D7D5345007CADFF000CFBE36FFA86FF00E041FF00E26A
          96AFF047C5DA269177A9DD8B036F6B134D2797392DB5464E06DE6BEB6ACFD76C
          BFB4740D46C719FB4DACB163FDE523FAD007CCBE02F853E24D4E2B0F10DAB591
          B19D4B2E67C301920E463D455CB8F0A6ABE24F8A1ADE9B67E4B4F636F1AC9BDF
          0B8014119C7A9AF4CF80B7C6E3E1DFD91C9F32CAEE58594F6070DFD4D51F83FF
          00F136F17F8E75FCE52E2F8451B762A19CFF002DB4A4935666B46ACA9545523B
          A399FF00853BE2A3CEDB2FFBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F428
          1C52D61F5781EAFF006F62FCBEE3E79FF8539E29FEE597FDFEFF00EB51FF000A
          73C55FDCB2FF00BFDFFD6AFA1A8A3EAF00FEDEC5F97DC7CF3FF0A73C55FDCB2F
          FBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F435147D5E01FDBD8BF2FB8F9E
          7FE14E78ABFB965FF7FBFF00AD47FC29CF157F72CBFEFF007FF5ABE86A28FABC
          03FB7B17E5F71F3CFF00C29CF157F72CBFEFF7FF005A8FF8539E2AFEE597FDFE
          FF00EB57D0D451F57807F6F62FCBEE3E79FF008539E2AFEE597FDFEFFEB51FF0
          A73C55FDCB2FFBFDFF00D6AFA1A8A3EAF00FEDEC5F97DC7CF3FF000A73C55FDC
          B2FF00BFDFFD6A3FE14E78ABFB965FF7FBFF00AD5F435147D5E01FDBD8BF2FB8
          F9E7FE14E78ABFB965FF007FBFFAD49FF0A7BC54380965FF007FBFFAD5F43D21
          147D5E01FDBD8BF2FB8E03E18784753F0ADAEA09A97921A791193CB7DDD011E9
          5DFD18A3A56D18F2AB1E5D7AD3AF51D49EEC75145154622514668A0028A28A43
          0A28A2800A28A2800A28A2800A43D3814B471401F3CC7E235F855E28F1D69371
          B923BC8CDDE9BF2FCAD23676E3DBE6FF00C70D7A2FC18D024D0BE1D5A1B88CA5
          CDF3B5DC80820E1B1B73FF000103F3AEA757F0B687AF5C5BDC6ABA65B5E4D6FF
          00EA9E58C12BDF03DB2338AD64458C61781D801D298875145148614514500145
          145001451450014514500145145001451450014514500145145002D149914531
          1FFFD9}
        Stretch = True
      end
      object RLImage4: TRLImage
        Left = 100
        Top = 517
        Width = 68
        Height = 26
        Stretch = True
      end
      object RLLabel60: TRLLabel
        AlignWithMargins = True
        Left = 288
        Top = 521
        Width = 178
        Height = 22
        Alignment = taCenter
        Caption = 'DI'#193'RIO DE BORDO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw72: TRLDraw
        Left = 1
        Top = 6
        Width = 1
        Height = 73
      end
      object RLDraw73: TRLDraw
        Left = 744
        Top = 4
        Width = 1
        Height = 75
      end
      object RLDraw74: TRLDraw
        Left = 1
        Top = 548
        Width = 1
        Height = 82
      end
      object RLDraw75: TRLDraw
        Left = 744
        Top = 547
        Width = 1
        Height = 79
      end
      object RLDraw76: TRLDraw
        Left = 744
        Top = 78
        Width = 1
        Height = 83
      end
      object RLDraw77: TRLDraw
        Left = 1
        Top = 82
        Width = 1
        Height = 79
      end
      object RLDraw78: TRLDraw
        Left = 1
        Top = 166
        Width = 1
        Height = 96
      end
      object RLDraw79: TRLDraw
        Left = 744
        Top = 165
        Width = 1
        Height = 97
      end
      object RLDraw82: TRLDraw
        Left = 744
        Top = 709
        Width = 1
        Height = 106
      end
      object RLDraw83: TRLDraw
        Left = 1
        Top = 707
        Width = 1
        Height = 106
      end
      object RLLabel61: TRLLabel
        Left = 63
        Top = 769
        Width = 20
        Height = 8
        Alignment = taCenter
        Caption = 'CHEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel62: TRLLabel
        Left = 111
        Top = 769
        Width = 50
        Height = 8
        Alignment = taCenter
        Caption = 'DESCART'#193'VEL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel38: TRLLabel
        Left = 185
        Top = 769
        Width = 16
        Height = 8
        Alignment = taCenter
        Caption = 'PBR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel63: TRLLabel
        Left = 236
        Top = 769
        Width = 20
        Height = 8
        Alignment = taCenter
        Caption = 'CHEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel64: TRLLabel
        Left = 284
        Top = 769
        Width = 50
        Height = 8
        Alignment = taCenter
        Caption = 'DESCART'#193'VEL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw84: TRLDraw
        Left = 218
        Top = 766
        Width = 1
        Height = 47
      end
      object RLDraw85: TRLDraw
        Left = 268
        Top = 766
        Width = 1
        Height = 47
      end
      object RLLabel68: TRLLabel
        Left = 282
        Top = 221
        Width = 50
        Height = 8
        Alignment = taCenter
        Caption = 'DESCART'#193'VEL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel67: TRLLabel
        Left = 234
        Top = 221
        Width = 20
        Height = 8
        Alignment = taCenter
        Caption = 'CHEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel66: TRLLabel
        Left = 183
        Top = 221
        Width = 16
        Height = 8
        Alignment = taCenter
        Caption = 'PBR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel65: TRLLabel
        Left = 109
        Top = 221
        Width = 50
        Height = 8
        Alignment = taCenter
        Caption = 'DESCART'#193'VEL'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel24: TRLLabel
        Left = 61
        Top = 221
        Width = 20
        Height = 8
        Alignment = taCenter
        Caption = 'CHEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel23: TRLLabel
        Left = 14
        Top = 221
        Width = 16
        Height = 8
        Alignment = taCenter
        Caption = 'PBR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLMemo3: TRLMemo
        Left = 3
        Top = 454
        Width = 742
        Height = 32
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          
            'Srs Motoristas se a Loja n'#227'o tiver palete para devolver, favor e' +
            'ntrar em contato, pois s'#243' ser'#225' aceito o vale palete com a autori' +
            'za'#231#227'o do Monitoramento. ')
        ParentFont = False
      end
      object RLMemo4: TRLMemo
        Left = 0
        Top = 1007
        Width = 742
        Height = 32
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Lines.Strings = (
          
            'Srs Motoristas se a Loja n'#227'o tiver palete para devolver, favor e' +
            'ntrar em contato, pois s'#243' ser'#225' aceito o vale palete com a autori' +
            'za'#231#227'o do Monitoramento. ')
        ParentFont = False
      end
      object RLDraw86: TRLDraw
        Left = 2
        Top = 80
        Width = 742
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel56: TRLLabel
        Left = 5
        Top = 551
        Width = 74
        Height = 8
        Caption = 'NOME DO MOTORISTA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw55: TRLDraw
        Left = 2
        Top = 649
        Width = 744
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw56: TRLDraw
        Left = 120
        Top = 668
        Width = 626
        Height = 5
        DrawKind = dkLine
      end
      object RLLabel47: TRLLabel
        Left = 37
        Top = 635
        Width = 32
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'LOJA:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw58: TRLDraw
        Left = 2
        Top = 703
        Width = 745
        Height = 5
        DrawKind = dkLine
      end
      object RLDraw59: TRLDraw
        Left = 545
        Top = 650
        Width = 1
        Height = 55
      end
      object RLDraw60: TRLDraw
        Left = 264
        Top = 650
        Width = 1
        Height = 55
      end
      object RLDraw61: TRLDraw
        Left = 408
        Top = 650
        Width = 1
        Height = 55
      end
      object RLLabel48: TRLLabel
        Left = 559
        Top = 658
        Width = 136
        Height = 8
        Caption = 'CARIMBO E NOME LEG'#205'VEL (RESP. LOJA)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel49: TRLLabel
        Left = 134
        Top = 656
        Width = 120
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'CHEGADA LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel50: TRLLabel
        Left = 293
        Top = 658
        Width = 100
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'SA'#205'DA LOJA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel51: TRLLabel
        Left = 435
        Top = 658
        Width = 98
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'CHEGADA CD'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw80: TRLDraw
        Left = 1
        Top = 631
        Width = 1
        Height = 74
      end
      object RLDraw8: TRLDraw
        Left = 120
        Top = 81
        Width = 1
        Height = 80
      end
      object RLLabel10: TRLLabel
        Left = 11
        Top = 130
        Width = 100
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'DADOS DA CARGA:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw62: TRLDraw
        Left = 120
        Top = 632
        Width = 1
        Height = 73
      end
      object RLLabel46: TRLLabel
        Left = 8
        Top = 674
        Width = 100
        Height = 11
        Alignment = taCenter
        AutoSize = False
        Caption = 'DADOS DA CARGA:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw81: TRLDraw
        Left = 2
        Top = 265
        Width = 744
        Height = 29
      end
      object RLLabel69: TRLLabel
        Left = 5
        Top = 266
        Width = 111
        Height = 8
        Caption = 'Observa'#231#245'es importantes:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw87: TRLDraw
        Left = 2
        Top = 816
        Width = 743
        Height = 31
      end
      object RLLabel70: TRLLabel
        Left = 8
        Top = 818
        Width = 111
        Height = 8
        Caption = 'Observa'#231#245'es importantes:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw88: TRLDraw
        Left = 2
        Top = 496
        Width = 744
        Height = 5
        DrawKind = dkLine
        Pen.Style = psDashDot
      end
      object motorista: TRLLabel
        Left = 14
        Top = 13
        Width = 57
        Height = 13
        Caption = 'MOTORISTA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object transportadora: TRLLabel
        Left = 14
        Top = 38
        Width = 90
        Height = 13
        Caption = 'TRANSPORTADORA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object veiculo: TRLLabel
        Left = 14
        Top = 64
        Width = 42
        Height = 13
        Caption = 'VEICULO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NR_ROMANEIO: TRLLabel
        Left = 272
        Top = 39
        Width = 72
        Height = 13
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object placa: TRLLabel
        Left = 276
        Top = 64
        Width = 33
        Height = 13
        Caption = 'PLACA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DTIC: TRLLabel
        Left = 603
        Top = 13
        Width = 23
        Height = 13
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object motorista1: TRLLabel
        Left = 10
        Top = 558
        Width = 57
        Height = 13
        Caption = 'MOTORISTA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object transportadora1: TRLLabel
        Left = 9
        Top = 584
        Width = 90
        Height = 13
        Caption = 'TRANSPORTADORA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object veiculo1: TRLLabel
        Left = 9
        Top = 609
        Width = 42
        Height = 13
        Caption = 'VEICULO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object nr_romaneio1: TRLLabel
        Left = 272
        Top = 583
        Width = 72
        Height = 13
        Caption = 'NR_ROMANEIO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object placa1: TRLLabel
        Left = 273
        Top = 611
        Width = 33
        Height = 13
        Caption = 'PLACA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DTIC1: TRLLabel
        Left = 603
        Top = 558
        Width = 23
        Height = 13
        Caption = 'DTIC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDBText1: TRLDBText
        Left = 134
        Top = 88
        Width = 37
        Height = 13
        DataField = 'RAZSOC'
        DataSource = DSRep
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBText2: TRLDBText
        Left = 127
        Top = 635
        Width = 37
        Height = 13
        DataField = 'RAZSOC'
        DataSource = DSRep
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
    end
    object RLDraw13: TRLDraw
      Left = 23
      Top = 598
      Width = 745
      Height = 5
      DrawKind = dkLine
    end
    object RLDraw69: TRLDraw
      Left = 767
      Top = 680
      Width = 1
      Height = 75
    end
  end
  object RLDraw43: TRLDraw
    Left = 17
    Top = 682
    Width = 744
    Height = 3
    DrawKind = dkLine
  end
  object btnImprimir: TBitBtn
    Left = 56
    Top = 80
    Width = 101
    Height = 25
    Caption = 'Imprimir'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
      52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
      FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
      D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
      FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
      FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
      FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
      BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
      FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
      FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
      A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
      B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
      A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
      FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
      CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
      B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
      FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
      FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
      F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
      A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
      F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
      F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
      8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
      F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
      F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
      F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
      90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
      D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
      BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    TabOrder = 3
    OnClick = btnImprimirClick
  end
  object RLReport2: TRLReport
    Left = -5
    Top = 1000
    Width = 794
    Height = 1123
    AllowedBands = [btTitle, btColumnHeader, btDetail, btFooter]
    DataSource = DSInfcrm
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    PrintDialog = False
    ShowDesigners = False
    JobTitle = 'Di'#225'rio de Bordo CRM'
    object RLBand3: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 78
      BandType = btTitle
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel71: TRLLabel
        AlignWithMargins = True
        Left = 261
        Top = 18
        Width = 178
        Height = 22
        Alignment = taCenter
        Caption = 'DI'#193'RIO DE BORDO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLImage5: TRLImage
        Left = 11
        Top = 14
        Width = 83
        Height = 28
        Picture.Data = {
          0A544A504547496D61676514180000FFD8FFE000104A46494600010101006000
          600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC0001108006F010603012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7E0
          3145145001451450014514500145145001451450014138F5A290E3193C0F5A00
          E0BC65F16F41F056B306977B0DD5CDC3AABCBF670A44287A139239EF8F4FA8AE
          DAC6FADB52B082F6D2512DB5C46B2C5229E1948C835E23E02D120F1F5F78FB5D
          BD4F306A0EF636EC7F857A8DBF4C45F9574DF023549AEFC07269D704F9FA5DDC
          96C41EC3EF01F9923F0A00F50A28A2800A28A2800A28A2800A28A2800A28A280
          0A28A2800A28A2800A28A2800A28A2800A28A2802A6A9AA59E8DA6CFA85FCEB0
          5AC0A5E491BA015E6569FB41784AE35416B25BEA16F6ECDB45DC91AEC1EE4039
          03F0AE87E2DE877DE21F875A85969C864B9529308875902B0240F7C57C9367A4
          6A37DA88D3ED6CEE25BB67DA2258CEF073DFD3B7E7401F75C722CB1AC88C1918
          65594E411D8834EAC9F0C69D3693E17D2B4FB87DD3DB5A45139F75500FEB9AD6
          A430A28A3F0A0028A28A0028AF39F15FC68F0D785B50B9D35A3BBBCD42DDB6BC
          3047800E3A163F5ED9AB3F0CFE21CBF1061D52E1B4F4B38AD65448D4485D9B70
          279381E82803BDAC8F14DF7F66784B58BECE0DBD9CD203EE1091FAD6BD715F16
          EE0DB7C2CD7DC36D2D008FEBB9D57FAD0065FC0BB1FB1FC2EB1936E1AEE69666
          E319F98A8FD14564FC2E234CF8A1E3CD141010DC0B945C6382CDFF00C5AD769F
          0D6DFECBF0D7C3F1F5FF004347E9DD86EFEB5C5E921ACBF697D6A1DC36DE6981
          F18EB858CFF306811EBE28A404E7914B40C28A4CD00E7B5002D1499A01CD002D
          1466933400B451499A005A2933403400B45145001452679A33400B45213E9450
          03A8A28A62313C5BE2387C27E1BBAD6AE209278ED82931C64066CB05EFF5AF29
          1FB45E81E6994787AF44878DDBA3CFD335DA7C66FF009251AD73FC31FF00E8C5
          AF8FBBD007DED6974B75670DCA8C09515C29233C8CD790DE7ED11A3D95F5C5AC
          9A15F9782468D8874EA091FD2BD5F471FF00125B0E7FE5DA3FFD0457C4DE28C0
          F176B43D2FE7FF00D18D401F555F7C5BF0F699E10D3F5FBD32C66FE2324164B8
          695B9C74E98CF7AE01BF6954F3F0BE1763167BDE0DD8FA6CAF2DF07F82B5FF00
          8877A2D6CE402DECE308F7370C7CB854E485E33C9E78AB5E3BF85FACF80E282E
          6F2586E6CE76F2D67832007EBB581031C67F2A00FA5BC15F11743F1CDAB369D2
          3457718CCB693E0489EFC751EE2A7F1CF8D2DBC0DA126AB776935CC6F3AC0122
          601B24139E7FDDAF91BC23AFDC786BC57A76AD6EE50C332EF038DD19E194FB11
          9AFAAFE24783AE3C7BE168B4BB4BB8AD9BED093F992292080A78E3FDEA00F953
          C65AEC5E25F16EA3AC410BC315DCBE62C6E412BC639C7D2BB3F855F13EC3E1FD
          8EA56F79A7DCDD35DCA8EA616501768239CFD6B85F136872F86FC477BA34D324
          F25A49B1A4404293807807EB5D3FC3EF85F79F102D2F67B5D460B516AEA8C254
          2DBB7027231F4A00FA57C07E39B5F1E6913EA3696735AC70CE602B330249DA0E
          78FAD73FF1DAEA387E17DDC2D22ABCF3C28AB9196C382703BF4AD1F867E09B8F
          87FE1DBBB0BCBD86E0C970D71E646A55546D031CFF00BB9AF9BBE2478CEE7C6B
          E2DB9B932B7D820768ACE2CF0230701B1EA7AFFF00AA803D2B4DF8FB6BA7E9FA
          568FA7681248B04115B99669C27214038500FA7AD3EFBC556765F1CF49D7EFD0
          DB412E998942FCD8C8700FD3A554F86DF04ADF57D1AD75DD7AEAE62F3C096DED
          A0C29D9D998907AF503D2A97C5CD1D345F1BE8B04523BAFD80ED66FBD80CF4A4
          ECAE6B429AA956307D59F46DA5DC17D6A9736D2A4B0C8BB91D1B208F6353EEC5
          78B7C18F11C897D73E1F9DF313A19EDC1FE123EF01EC739FC2BA8F8A9E2E9BC3
          DA3C5656527977B7B901C758D00E587BF38AC9554E1CCCEAAB9754862BEAD1FE
          91A1E23F899A0F87A67B6777BABB5EB05B80C54FA13D0571ADF1CCEF3B341CA7
          62D7383FFA0D792430CD773243123CB34AC02AAF2598FF0033935E97A77C13D5
          2E6D965BDD4EDEDA52326258CBEDFA9E2B0F6B5277E53DC9E5D97E112FAC3BB7
          FD743BBF05FC4883C5F7D2590D367B69638CC858B064C640EBC7AD76575776F6
          503DC5CCA90C2832CEED800570FF000FFC0171E0EBCD426B8B986E3CF4444745
          20E0139041FA8AF3FF008B3E2C9B54D6E4D16090AD9599DAEAA7FD649DF3F4AD
          B9DC21791E547074F158B74F0FF07F5DCEC356F8D3A45A4CD1E99673DFED38F3
          33E5A1FA120923F0ACD87E39AB4AA27D05D63CF263B80C7F22A3F9D79FF83FC2
          379E2FD51EDE191608221BA79D86420F403B9AE9FC5BF09A7D07487D474FBE6B
          C8E0199E2910060BDC8F5FA560A75A4B996C7AB2C1E594A6A8547793F53D67C3
          7E34D1BC53016D3E7FDEA0CBC120DB22FE1FE15D0641AF9234DD4AE748D4A1BF
          B390C7710386523B8EE0FA8AFA8FC3DAC47AF787ECF538800B3C418A839DADDC
          7E0735BD2ABCEB5DCF2B33CB7EA724E2EF16711A97C64D3B4DD4EEAC64D2EED9
          EDE5688B065009071EBED5B33FC4AD16D3C3169ACDCF9886ED4986D460C8D838
          F5F5AF09F1780BE32D63D3ED721FD4D4DE1AF0C6ABE31BC16B68E0456EB86966
          24A4409271F5CE6B1F6D3E67147AB2C9F0AA8C6AC9F2ADDFDC7A1B7C735F3B0B
          A0318B38E6E006FCB6E3F5AEF3C33E35D2BC5362F73692344F08CCD0CD8568FD
          CFB7079AF06F16F81F52F083C2D752473DBCDC24B10E377A107A573D6F77716C
          9324133C42788C720538DEA71F29F63C7142AF38BB48B965184C4D253C33B799
          EE1AE7C66D26C2E9A0D36CDF512A70D287D91E7D8E0E699A27C68D3AFAF12DF5
          2B17B00E76ACDE66F407DCE060571769F08F5FBBD205E996D6195977A5B36771
          1D813D05707246D04AF14B1959118AB2B0C1041C1068955AB1777B0A8E5780AD
          170A6EF25BB3EBE465951591815232083D69F8E6B81F845ABCBA9F8356199CBC
          96729837139257AAE7F038FC2BBEEF5D709732B9F315E8BA35654DEE87514515
          462705F19FFE4946B7FEEC7FFA316BE3EAFB07E33FFC928D6FFDD8FF00F462D7
          C7D401F7868FFF00205B0FFAF68FFF004115F12F8A4E3C61AD1FFA7F9FFF0046
          357DB5A3FF00C816C3FEBDA3FF00D0457C4BE2AFF91BF5BFFAFF009FFF004635
          007D0BFB39C683C0DA8385018EA0C09C724044E3F9D6CFC758D5BE155FB32862
          B340413D8F98067F226B23F674FF009112FBFEC20FFF00A0256CFC74FF009251
          A8FF00D7683FF462D007C959E6BEEFD218BE8D60CC492D6F1927EAA2BE0FEF5F
          77E8DFF203D3BFEBDA3FFD045007C83F15BFE4A8F883FEBEBFF6515EB1FB358F
          F893EBFF00F5DE1FFD05ABCABE2D46D17C54D7D5C72670DF9A29FEB5DFFECEFE
          22D374E7D674BBDBA8ADE6B831CB0F9AE143ED0C0804F19E45007B4F8EEE24B3
          F006BF71090B247613107D3E435F120C8E715F716B96F07893C29AAD85ACD14E
          2EADA580346E194315200247B915F10491BDBCAF14AAC9223156561C823820FF
          002A00EF349F8B1E36865B2B18F5965B742912C6208F0146001F77D05743F12E
          F2E350F10786EE6EA4F3256B17DCD803F89BD2BB2F873A47C33F16E8764FFD93
          631EB30A22DC40F232BF983AB019E41C66A3F1BE8DA51F8C9E0ED325B741A7B5
          AC9E644C70B8F9CF393ED4A4AEAC6B426A9D58CDABD9A672DF0C8BAFC43D288C
          8CB383C76D8DFE15A7F18E7797C70226276456B1851F5249AF57D0FC25E11B4B
          F17FA359DB7DA2DD8A878A42DB091823AFA1AF34F8D5A6490F892CF510A7CBB8
          B7F2C9FF006D49FE8C3F2AE495371A5B9F4B86C6C31398A9D9AD2C51F841690D
          CF8DC492804DBDBBC8808FE2C819FC8D7D08073F4AF96FC1BE201E17F135AEA3
          22968798E651D761EBFD0D7D29A6EBBA66AB68B73657B04B130CE55C71EC7D2A
          F0F25CA71E7F4AA7D639EDA3468370A4FB57C8FA94CF73AADDCF2125E49DD9B3
          EA49AFABE1BFB4BB9A5860B98659100DEA92062B9F5C74E95F2EF8AF4D9348F1
          56A5652020A4ECCBC75563B94FE469627E14CDB875A8D59C5EF647AFFC13B78D
          7C29773AA8F31EED958FA80A31FCCD7A25EC2B71653C520051E365607A608AF2
          2F82DE21B7B65BDD0EE2458E4924F3A0DC701B8C30FAF02BD1FC61E20B6F0F78
          7AEAF27750DB0AC499E5D88C0007E35A5392F6679F8FA551E39C7AB7A1F2DC8B
          B66651C00C40C74AFA07E0E4AF27814A31C88EEA455F61C1C7EB5F3F1DC4927A
          F735F497C31D2E4D2FC096492A95927DD3B03D46E3C7E98AE7A17E66CF733E69
          61A317BDCF07F187FC8E7ACFFD7DC83FF1EAF57F8231AFFC237A83E06E377827
          BE022D79478C0FFC567AC7FD7E49FCCD7AD7C11FF9162FC7FD3E1FFD0168A5FC
          40CD2FFD9B1F916FE32283E07572391771E0FE06BC234EFF0090A5A7A79C9C7A
          FCC2BDE7E327FC88B8FF00A7B8FF00AD78369DFF00214B3FFAEE9FFA10A7597E
          F0592BFF006197ABFC8FAE15479600E98E95F2F78E5153C73AD2A00ABF6A7381
          EFCD7D46BC2815F2F78EFF00E47CD687FD3D37F4AD311F0A383879FF00B4CBD3
          F53D2FE0693FD8DAB0EDF6953FF8ED7ABFA57907C18BC82C3C3BADDD5CC82382
          2995DDC83F280BC9AED17E257841D95575A849638036373FA55D169415CE3CCE
          94E78BA8E116F5FD0EB68A6870C011D28AD8F2EE711F18209AE7E176B30DBC52
          4B2B2C7B52352C4FEF17B0AF92CE81AC77D2AFBFF019FF00C2BEC8F1DF8A4F83
          3C2B71AD8B4FB5F92E8BE56FD99DCC075C1F5CD791FF00C34AF1CF8638C63FE3
          F3FF00B0A00F74D2415D1EC4104116F183C74F9457C67E27D0F5693C59ACBA69
          97AC8D7D31565B77208321E7A57ACFFC34B7FD4B1FF939FF00D8527FC34AFF00
          D4B1FF00939FFD850074DFB3EDA5CD9F81EFA3BAB7960737EC42CA854E362738
          22B5FE365BCF77F0BB5086DA092694CB09091A1627122F615C1FFC34B63FE658
          FF00C9CFFEC28FF8696FFA963FF273FF00B0A00F12FEC0D6323FE2557DFF0080
          CFFE15F6F68E1868960194822DA3C823A1DA38AF0EFF008695E31FF08C7FE4E7
          FF006147FC34B73FF22B8FFC0CFF00EC2800F8E9F0E752BED54789F48B592E91
          E20977144BB9D4AF47C0E48C707D31EF5E06C8C8DB58107D08C57BE7FC34B7FD
          4B1FF939FF00D85567FDA1ACE49FCF7F06C0F2FF007DAE016FCF6500765FB3F4
          7227C3A9564474FF004E908C8C64155AE2FE3A7C39B7D34C9E2ED39D634B8942
          5DDBE38F31BF8D7EB8E47AF3DEAD0FDA57031FF08C7FE4E7FF00615CEF8E7E35
          AF8D3C2971A29D0BECA6574712FDA77E36B03D368F4C75EF401E716D6579A7EA
          967F6AB69EDC991594CA853232304671915DFF008E1A5B9F13E830C3E6492AD8
          67E5CB13967E98F6AEAFC37F1C92EA0B0D164F0DA3F930245E635C641DAA0671
          B3F1EB54FC41E301A7FC65B2D6D34D4616BA6853007DA3E60DCE71FED7A76A99
          6DA9BE1B9BDB47955DDCED7E0CD86A9A7DA6A697961716D6F2B472446542BB8F
          20E01E7FBB5DCF8ABC3769E28D165D3AE86DC9DD1C83AA38E86BCD47C7365181
          A00FFC09FF00EC697FE17A3FFD00067FEBE7FF00B1AC23529A5CAD9EB56C0E3A
          A5775942CF7DD1C17883C13AE7872E1D2EECE59615CEDB8854B211EB91D0FD6B
          9D0482769C1EE3FF00D55EBDFF000BC9C8E7C3EA7FEDE7FF00B1AAE7E325BF9B
          E69F0B5B993FBE6519FCF6D60E14EF7523D9A789C7A8A5528DDFA8DF82B1DE43
          AE5F6EB6996DE6B71FBD68C852C1B8E4F5E09AECBE21F80078AA15BDB22B16A5
          0AE14B70245FEE93FC8FBD72CBF1C990617C3EA07A7DA7FF00B1A53F1CDC9FF9
          000FFC09FF00EC6B652A7C9CAD9E655C363DE27EB14E1CAFD51E63A8E8FAAE89
          73E5DF59DC5ACA8721994803DC30A81A7BED52545796E6F24CE1016690FE02BD
          4E4F8E1E6A957F0EA329ECD7191FFA0D4717C698A1398BC310A1FF0066703F92
          D63CB0FE63D3588C6DAF2A377EA8ABE08F85B7D7D770DF6BD035AD9A10C2DD86
          1E5F623B0E95EE40044000C018181D2BC7BFE17A3FFD0007FE04FF00F6347FC2
          F47FFA000FFC09FF00EC6B784E9C15933C7C661331C5CF9AA476E9A1C278BB4E
          BF93C61ABBC76374EAD77210CB031046E3D0E2BD53E0BDBCF6DE1BBE5B882589
          8DD9204885491B57D6B10FC7263D7C3EBFF813FF00D8D28F8E6E3A787D7FF027
          FF00B1A883A719735CEAC4D3C757C3AA1ECEDF3EC74FF1820967F046C8229257
          FB54676A2927BFA5786E9DA66A0353B4CD8DD00274C930B71F30F6AF4B3F1C9C
          F5D007FE04FF00F63487E38923FE45F5FF00C09FFEC689BA7295EE183863F0B4
          9D254EF7F347B28CEC1F4AF99FC73A75F4BE37D6244B2BA646B862ACB0B1047B
          1C576BFF000BCDCFFCCBE3FF00027FFB1A4FF85E4FCE7401F5FB4FFF00635552
          74EA2B366180C263B0951CD53BDFCCADE04B3B98BE1CF8B6392DA6491E36D8AD
          190CDF21E8315E756BA66A02EE0DD6176079899CC0E3B8F6AFA13C0BE34FF84D
          2DAF656B01686DDD57024DFBB233E82BAF0817A629FB15349A644B34AB87AB51
          4E1EF4BF0D06C43080631453C023BD15D173C1391F89DE1BBFF16781EEB48D37
          CAFB54B246CBE6B6D5C0604F3835E0BFF0CFBE36FF00A86FFE041FFE26BEA934
          0A607CADFF000CFBE36FFA86FF00E041FF00E268FF00867DF1B7FD437FF020FF
          00F135F54D1401F2B7FC33EF8DBFEA1BFF008107FF0089A3FE19F7C6DFF50DFF
          00C083FF00C4D7D5345007CADFF0CFBE36FF00A86FFE041FFE268FF867DF1B7F
          D437FF00020FFF00135F54D1401F2B7FC33EF8DBFEA1BFF8107FF89A3FE19F7C
          6DFF0050DFFC083FFC4D7D5345007CADFF000CFBE36FFA86FF00E041FF00E26A
          96AFF047C5DA269177A9DD8B036F6B134D2797392DB5464E06DE6BEB6ACFD76C
          BFB4740D46C719FB4DACB163FDE523FAD007CCBE02F853E24D4E2B0F10DAB591
          B19D4B2E67C301920E463D455CB8F0A6ABE24F8A1ADE9B67E4B4F636F1AC9BDF
          0B8014119C7A9AF4CF80B7C6E3E1DFD91C9F32CAEE58594F6070DFD4D51F83FF
          00F136F17F8E75FCE52E2F8451B762A19CFF002DB4A4935666B46ACA9545523B
          A399FF00853BE2A3CEDB2FFBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F428
          1C52D61F5781EAFF006F62FCBEE3E79FF8539E29FEE597FDFEFF00EB51FF000A
          73C55FDCB2FF00BFDFFD6AFA1A8A3EAF00FEDEC5F97DC7CF3FF0A73C55FDCB2F
          FBFDFF00D6A3FE14E78ABFB965FF007FBFFAD5F435147D5E01FDBD8BF2FB8F9E
          7FE14E78ABFB965FF7FBFF00AD47FC29CF157F72CBFEFF007FF5ABE86A28FABC
          03FB7B17E5F71F3CFF00C29CF157F72CBFEFF7FF005A8FF8539E2AFEE597FDFE
          FF00EB57D0D451F57807F6F62FCBEE3E79FF008539E2AFEE597FDFEFFEB51FF0
          A73C55FDCB2FFBFDFF00D6AFA1A8A3EAF00FEDEC5F97DC7CF3FF000A73C55FDC
          B2FF00BFDFFD6A3FE14E78ABFB965FF7FBFF00AD5F435147D5E01FDBD8BF2FB8
          F9E7FE14E78ABFB965FF007FBFFAD49FF0A7BC54380965FF007FBFFAD5F43D21
          147D5E01FDBD8BF2FB8E03E18784753F0ADAEA09A97921A791193CB7DDD011E9
          5DFD18A3A56D18F2AB1E5D7AD3AF51D49EEC75145154622514668A0028A28A43
          0A28A2800A28A2800A28A2800A43D3814B471401F3CC7E235F855E28F1D69371
          B923BC8CDDE9BF2FCAD23676E3DBE6FF00C70D7A2FC18D024D0BE1D5A1B88CA5
          CDF3B5DC80820E1B1B73FF000103F3AEA757F0B687AF5C5BDC6ABA65B5E4D6FF
          00EA9E58C12BDF03DB2338AD64458C61781D801D298875145148614514500145
          145001451450014514500145145001451450014514500145145002D149914531
          1FFFD9}
        Stretch = True
      end
      object RLLabel82: TRLLabel
        AlignWithMargins = True
        Left = 309
        Top = 46
        Width = 82
        Height = 22
        Alignment = taCenter
        Caption = 'Inf. CRM'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RLBand4: TRLBand
      Left = 38
      Top = 116
      Width = 718
      Height = 61
      BandType = btColumnHeader
      object RLDraw89: TRLDraw
        Left = 0
        Top = 49
        Width = 721
        Height = 15
        DrawKind = dkLine
      end
      object RLDraw90: TRLDraw
        Left = -3
        Top = 8
        Width = 7
        Height = 48
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLDraw91: TRLDraw
        Left = 714
        Top = 8
        Width = 8
        Height = 51
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLDraw92: TRLDraw
        Left = 0
        Top = 3
        Width = 716
        Height = 12
        DrawKind = dkLine
      end
      object RLLabel72: TRLLabel
        Left = 3
        Top = 13
        Width = 72
        Height = 14
        Caption = 'N'#186' Manifesto'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw93: TRLDraw
        Left = 207
        Top = 8
        Width = 8
        Height = 48
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLLabel74: TRLLabel
        Left = 221
        Top = 13
        Width = 32
        Height = 14
        Caption = 'S'#233'rie'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw94: TRLDraw
        Left = 375
        Top = 9
        Width = 8
        Height = 47
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLLabel76: TRLLabel
        Left = 493
        Top = 13
        Width = 50
        Height = 14
        Caption = 'Emiss'#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel79: TRLLabel
        Left = 389
        Top = 13
        Width = 27
        Height = 14
        Caption = 'Filial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw96: TRLDraw
        Left = 481
        Top = 9
        Width = 8
        Height = 48
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLDBText3: TRLDBText
        Left = 55
        Top = 33
        Width = 79
        Height = 16
        DataField = 'NRO_MANIF'
        DataSource = DSInfcrm
        Text = ''
      end
      object RLDBText4: TRLDBText
        Left = 259
        Top = 33
        Width = 90
        Height = 16
        DataField = 'SERIE_MANIF'
        DataSource = DSInfcrm
        Text = ''
      end
      object RLDBText5: TRLDBText
        Left = 400
        Top = 32
        Width = 41
        Height = 16
        DataField = 'FILIAL'
        DataSource = DSInfcrm
        Text = ''
      end
      object RLDBText6: TRLDBText
        Left = 537
        Top = 33
        Width = 111
        Height = 16
        DataField = 'EMISSAO_MANIF'
        DataSource = DSInfcrm
        Text = ''
      end
    end
    object RLBand7: TRLBand
      Left = 38
      Top = 259
      Width = 718
      Height = 48
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = False
      Borders.DrawRight = True
      Borders.DrawBottom = True
      Borders.FixedLeft = True
      Borders.FixedRight = True
      Borders.FixedBottom = True
      object RLSystemInfo1: TRLSystemInfo
        Left = 24
        Top = 15
        Width = 60
        Height = 16
        Info = itFullDate
        Text = ''
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 673
        Top = 16
        Width = 112
        Height = 16
        Info = itLastPageNumber
        Text = ''
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 632
        Top = 16
        Width = 87
        Height = 16
        Info = itPageNumber
        Text = ''
      end
      object RLLabel80: TRLLabel
        Left = 656
        Top = 16
        Width = 8
        Height = 16
        Caption = '/'
      end
      object RLLabel81: TRLLabel
        Left = 589
        Top = 16
        Width = 31
        Height = 16
        Caption = 'P'#225'g.'
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 177
      Width = 718
      Height = 82
      AutoSize = True
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = False
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLDraw98: TRLDraw
        Left = 104
        Top = 20
        Width = 2
        Height = 61
        Angle = 90.000000000000000000
        DrawKind = dkLine
        Pen.Style = psDot
      end
      object RLDraw99: TRLDraw
        Left = 254
        Top = 8
        Width = 6
        Height = 70
        Angle = 90.000000000000000000
        DrawKind = dkLine
        Pen.Style = psDot
      end
      object RLDraw97: TRLDraw
        Left = 0
        Top = -1
        Width = 716
        Height = 15
        DrawKind = dkLine
      end
      object RLLabel73: TRLLabel
        Left = 109
        Top = 8
        Width = 45
        Height = 14
        Caption = 'Destino'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw102: TRLDraw
        Left = 101
        Top = 7
        Width = 8
        Height = 19
        Angle = 90.000000000000000000
        DrawKind = dkLine
      end
      object RLLabel77: TRLLabel
        Left = 430
        Top = 9
        Width = 76
        Height = 14
        Caption = 'Observa'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel78: TRLLabel
        Left = 3
        Top = 9
        Width = 76
        Height = 14
        Caption = 'CNPJ Destino'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDBText7: TRLDBText
        Left = 3
        Top = 40
        Width = 101
        Height = 12
        DataField = 'CNPJ_DESTINATARIO'
        DataSource = DSInfcrm
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
      end
      object RLDBMemo1: TRLDBMemo
        Left = 434
        Top = 32
        Width = 275
        Height = 12
        Behavior = [beSiteExpander]
        DataField = 'OBSERVACOES'
        DataSource = DSInfcrm
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDBMemo2: TRLDBMemo
        Left = 111
        Top = 40
        Width = 135
        Height = 12
        Behavior = [beSiteExpander]
        DataField = 'DESTINATARIO'
        DataSource = DSInfcrm
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw100: TRLDraw
        Left = 420
        Top = 9
        Width = 6
        Height = 70
        Angle = 90.000000000000000000
        DrawKind = dkLine
        Pen.Style = psDot
      end
      object RLDraw101: TRLDraw
        Left = 306
        Top = 11
        Width = 6
        Height = 70
        Angle = 90.000000000000000000
        DrawKind = dkLine
        Pen.Style = psDot
      end
      object RLDraw103: TRLDraw
        Left = 363
        Top = 11
        Width = 6
        Height = 70
        Angle = 90.000000000000000000
        DrawKind = dkLine
        Pen.Style = psDot
      end
      object RLLabel75: TRLLabel
        Left = 263
        Top = 9
        Width = 42
        Height = 14
        Caption = 'Paletiz.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel83: TRLLabel
        Left = 313
        Top = 9
        Width = 52
        Height = 14
        Caption = 'Ajudante'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel84: TRLLabel
        Left = 373
        Top = 9
        Width = 42
        Height = 14
        Caption = 'Agend.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object pal: TRLLabel
        Left = 274
        Top = 40
        Width = 16
        Height = 12
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object ajud: TRLLabel
        Left = 327
        Top = 40
        Width = 20
        Height = 12
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object ag: TRLLabel
        Left = 384
        Top = 40
        Width = 14
        Height = 12
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object Ckbcrm: TCheckBox
    Left = 8
    Top = 51
    Width = 75
    Height = 17
    Caption = 'Inf. CRM'
    TabOrder = 5
    OnClick = CkbcrmClick
  end
  object Edit1: TEdit
    Left = 163
    Top = 53
    Width = 34
    Height = 21
    TabOrder = 6
    Visible = False
  end
  object cbRomaneio: TCheckBox
    Left = 8
    Top = 29
    Width = 75
    Height = 17
    Caption = 'Romaneio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = cbRomaneioClick
  end
  object QManifesto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 9212
      end
      item
        Name = '1'
        DataType = ftString
        Size = 6
        Value = Null
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct tipo, nr_romaneio, motorista, placa, veiculo, dt' +
        'inc, transportadora, codfil, serie'
      'from ('
      
        'select distinct '#39'R'#39' tipo, r.nr_romaneio, r.motorista, r.placa, r' +
        '.veiculo, r.dtinc, '
      'f.razsoc transportadora, 1 codfil, '#39'0'#39' serie'
      
        'from tb_romaneio@wmswebprd r left join cyber.rodcli f on r.cod_t' +
        'ransp = f.codclifor'
      'union all'
      
        'select distinct '#39'M'#39' tipo, m.manifesto nr_romaneio, nvl(mma.nomea' +
        'b, mma.nommot) motorista, v.numvei placa, f.descri veiculo, '
      
        'm.datainc dtinc, fma.nomeab transportadora, m.filial codfil, m.s' +
        'erie'
      
        'from tb_manifesto m left join cyber.rodmot mma on m.motorista = ' +
        'mma.codmot'
      
        '                    left join cyber.rodcli fma on m.transp = fma' +
        '.codclifor'
      
        '                    left join cyber.rodvei v on v.numvei = m.pla' +
        'ca and v.situac = 1'
      
        '                    left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      'order by 1)'
      'where nr_romaneio = :0'
      'and tipo = :1'
      'and codfil = :2'
      'order by 1')
    Left = 72
    Top = 98
    object QManifestoNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
    end
    object QManifestoMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      ReadOnly = True
    end
    object QManifestoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QManifestoVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 100
    end
    object QManifestoDTINC: TDateTimeField
      FieldName = 'DTINC'
      ReadOnly = True
    end
    object QManifestoTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      ReadOnly = True
    end
    object QManifestoCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
    object QManifestoTIPO: TStringField
      FieldName = 'TIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QManifestoSERIE: TStringField
      FieldName = 'SERIE'
      ReadOnly = True
      Size = 1
    end
  end
  object dtsManifesto: TDataSource
    DataSet = QManifesto
    Left = 88
    Top = 84
  end
  object QPallet: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select tipo, codean, codman, codpag, cliente from ('
      
        'SELECT distinct '#39'R'#39' tipo, r.codean, p.percent codman, r.codclifo' +
        'r codpag, r.razsoc cliente '
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient  '
      'where r.codean = '#39'PALLET'#39' '
      'union all'
      
        'SELECT distinct '#39'M'#39' tipo, r.codean, n.nr_manifesto codman, n.cod' +
        '_pagador codpag, r.razsoc cliente'
      'FROM cyber.RODCLI R, tb_conhecimento N  '
      'WHERE n.fl_empresa = 1 '
      'and n.cod_pagador = R.CODCLIFOR'
      'and r.codean = '#39'PALLET'#39')'
      'where codman = :0'
      'and tipo = :1')
    Left = 196
    Top = 65532
    object QPalletCODEAN: TStringField
      FieldName = 'CODEAN'
      ReadOnly = True
      Size = 13
    end
    object QPalletCODMAN: TBCDField
      FieldName = 'CODMAN'
      ReadOnly = True
      Precision = 32
    end
    object QPalletCODPAG: TBCDField
      FieldName = 'CODPAG'
      ReadOnly = True
      Precision = 32
    end
    object QPalletCLIENTE: TStringField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Size = 80
    end
    object QPalletTIPO: TStringField
      FieldName = 'TIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
  end
  object ADOQRep: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct xtipo, razsoc, codpag, serie, xmanifesto, codcgc' +
        ' from ('
      
        'SELECT distinct '#39'R'#39' xtipo, d.name razsoc,  r.codclifor codpag, '#39 +
        '0'#39' serie, percent xmanifesto, d.suchbegriff codcgc'
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient'
      
        '                    left join kunden@wmswebprd d on d.id_kunde =' +
        ' p.id_kunde_ware  and p.id_klient = d.id_klient                 ' +
        '     '
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'M'#39' xtipo, r.razsoc,  c.codclifor codpag, m.seri' +
        'e, m.manifesto xmanifesto, r.codcgc'
      
        'FROM tb_manitem m inner join tb_conhecimento N on m.CODDOC = N.n' +
        'r_conhecimento and m.fildoc = n.fl_empresa '
      
        '                  left join cyber.RODCLI R on N.cod_destinatario' +
        ' = R.CODCLIFOR '
      
        '                  left join cyber.RODCLI C on N.cod_pagador = C.' +
        'CODCLIFOR'
      'WHERE m.filial = 1 and c.codean = '#39'PALLET'#39')'
      'where xmanifesto = :0'
      'and serie = :1'
      'and xtipo = :2')
    Left = 164
    Top = 88
    object ADOQRepRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object ADOQRepcodcgc: TStringField
      FieldName = 'codcgc'
      Size = 15
    end
  end
  object DSRep: TDataSource
    DataSet = ADOQRep
    Left = 196
    Top = 96
  end
  object AdoInfcrm: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterOpen = AdoInfcrmAfterOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct(cm.cnpj_destinatario), cm.nro_manif, cm.serie_ma' +
        'nif, cm.emissao_manif, cm.filial, cm.destinatario, ca.palletizac' +
        'ao, ca.ajudante, ca.exige_agend, ca.observacoes'
      
        'from Vw_Crm_DIARIO_BORDO cm left join crm_tb_clientes_ag ca on c' +
        'a.cnpj_cliente = cm.cnpj_cliente and'
      
        'cm.cnpj_destinatario = Decode(ca.cnpj,NULL,NULL,REPLACE(REPLACE(' +
        'REPLACE(To_Char(LPad(REPLACE(ca.cnpj,'#39#39'),14 ,'#39'0'#39'),'#39'00,000,000,00' +
        '00,00'#39'),'#39','#39','#39'.'#39'),'#39' '#39') ,'#39'.'#39'||Trim(To_Char(Trunc(Mod(LPad(ca.cnpj,' +
        '14,'#39'0'#39'),1000000)/100),'#39'0000'#39'))||'#39'.'#39' ,'#39'/'#39'||Trim(To_Char(Trunc(Mod' +
        '(LPad(ca.cnpj,14,'#39'0'#39'),1000000)/100) ,'#39'0000'#39'))||'#39'-'#39'))'
      
        'where cm.nro_manif =:0 and cm.serie_manif=:1 and ca.observacoes ' +
        'is not null and cm.filial = :2')
    Left = 4
    Top = 76
    object AdoInfcrmNRO_MANIF: TBCDField
      FieldName = 'NRO_MANIF'
      Precision = 32
      Size = 0
    end
    object AdoInfcrmSERIE_MANIF: TStringField
      FieldName = 'SERIE_MANIF'
      Size = 1
    end
    object AdoInfcrmEMISSAO_MANIF: TStringField
      FieldName = 'EMISSAO_MANIF'
      Size = 19
    end
    object AdoInfcrmFILIAL: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
    end
    object AdoInfcrmCNPJ_DESTINATARIO: TStringField
      FieldName = 'CNPJ_DESTINATARIO'
    end
    object AdoInfcrmDESTINATARIO: TStringField
      FieldName = 'DESTINATARIO'
      Size = 80
    end
    object AdoInfcrmOBSERVACOES: TStringField
      FieldName = 'OBSERVACOES'
      Size = 3000
    end
    object AdoInfcrmPALLETIZACAO: TStringField
      FieldName = 'PALLETIZACAO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object AdoInfcrmAJUDANTE: TStringField
      FieldName = 'AJUDANTE'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object AdoInfcrmEXIGE_AGEND: TStringField
      FieldName = 'EXIGE_AGEND'
      ReadOnly = True
      Size = 300
    end
  end
  object DSInfcrm: TDataSource
    DataSet = AdoInfcrm
    Left = 8
    Top = 92
  end
  object QQuant: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select pallet, quant'
      'from tb_controle_pallet pp '
      'where manifesto = :0'
      'and doc = :1'
      'and filial = :2')
    Left = 200
    Top = 44
    object QQuantPALLET: TStringField
      FieldName = 'PALLET'
    end
    object QQuantQUANT: TBCDField
      FieldName = 'QUANT'
      Precision = 32
      Size = 0
    end
  end
end
