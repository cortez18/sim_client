unit Login;

interface

uses
  Windows, Messages, SysUtils, Classes, Forms, Graphics, Controls, DB, ADODB,
  Mask, Dialogs, JvExMask, JvToolEdit, JvDBLookup, StdCtrls, Buttons,
  inifiles, pcnConversao;

type
  TFrmLogin = class(TForm)
    edUsuario: TEdit;
    Label1: TLabel;
    QSenha: TADOQuery;
    QSenhaLOGIN: TStringField;
    QSenhaNOME: TStringField;
    btnOk: TBitBtn;
    btnCancelar: TBitBtn;
    Label2: TLabel;
    QSite: TADOQuery;
    dtsSite: TDataSource;
    QSiteCODFIL: TBCDField;
    IdSite: TJvDBLookupEdit;
    QSiteFILIAL: TStringField;
    Label3: TLabel;
    edSenha: TEdit;
    QPass: TADOQuery;
    QSenhaSITE: TStringField;
    QSiteUF: TStringField;
    QSenhaCOD_USUARIO: TBCDField;
    QParametro: TADOQuery;
    QParametroPDF: TStringField;
    QParametroSCHEMMA: TStringField;
    QParametroPDF_CTE: TStringField;
    QParametroCONTROLADORIA: TStringField;
    QParametroSEGURADORA: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edUsuarioExit(Sender: TObject);
    procedure edUsuarioEnter(Sender: TObject);
    procedure edSenhaExit(Sender: TObject);
    procedure edSenhaEnter(Sender: TObject);
  private
    { Private declarations }
    Site, Tentativa: Integer;

  public
    { Public declarations }
    Desligar: Integer;
  end;

var
  FrmLogin: TFrmLogin;

implementation

uses dados, menu, funcoes;

{$R *.DFM}

procedure TFrmLogin.FormCreate(Sender: TObject);
begin
  Tentativa := 0;
  Site := 0;
end;

procedure TFrmLogin.btnOkClick(Sender: TObject);
begin
  if Trim(IdSite.LookupValue) = '' then
  begin
    ShowMessage('Filial N�o Escolhida');
    IdSite.SetFocus;
    exit;
  end;
  if (edSenha.Text = 'MUDAR123') then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add
      ('update tb_usuarios set senha = (SELECT passwd_encrypt(:0,:1) new_senha ');
    dtmdados.IQuery1.sql.add('FROM DUAL) where COD_USUARIO = :2 ');
    dtmdados.IQuery1.Parameters[0].value := GLBCodUser;
    dtmdados.IQuery1.Parameters[1].value :=
      UpperCase(InputBox('Altera��o de senha', 'Digite a sua nova senha', ''));
    dtmdados.IQuery1.Parameters[2].value := GLBCodUser;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
  end;
  glbuser := QSenhaLOGIN.AsString;
  glbFilial := StrToInt(IdSite.LookupValue);
  frmMenu.Caption := 'SIM - Sistema de Integra��o e Monitoramento - Filial : ' +
    QSiteFILIAL.AsString;
  glbnmFilial := QSiteFILIAL.AsString;
  frmMenu.StatusBar1.Panels[3].Text := glbuser;
  GLBCodUser := QSenhaCOD_USUARIO.AsInteger;

  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := QSiteUF.AsString;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.sql.add
    ('select ambiente, ambiente_cte, certificado, logo, pdf, nvl(custo,0) custo, ');
  dtmdados.IQuery1.sql.add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.sql.add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := glbFilial;
  dtmdados.IQuery1.open;
  if (dtmdados.IQuery1.Eof) or (dtmdados.IQuery1.FieldByName('ambiente').IsNull)
  then
  begin
    ShowMessage('N�o Encontrado nenhuma configura��o de MDFe, avise o TI');
  end
  else
  begin
    QParametro.open;
    GlbSchema :=  ALLTRIM(QParametroSCHEMMA.AsString);
    GlbControler := ALLTRIM(QParametroCONTROLADORIA.AsString);
    glbSeguradora := ALLTRIM(QParametroSEGURADORA.AsString);
    glbPDF :=  ALLTRIM(QParametropdf.AsString);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    frmMenu.ACBrMDFe1.DAMDFe.PathPDF := IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathMDFe :=
      IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
      IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte :=
      IncludeTrailingPathDelimiter(ALLTRIM(QParametroPDF_CTE.AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar :=
      IncludeTrailingPathDelimiter(ALLTRIM(QParametroPDF_CTE.AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    QParametro.close;

    GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
    GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').value;
    GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
    frmMenu.ACBrMDFe1.DAMDFe.Logo :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('logo').AsString);
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
    if dtmdados.IQuery1.FieldByName('ambiente_cte').AsString = 'H' then
      frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
    else
      frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;
    // CTe
    frmMenu.ACBrCte1.Configuracoes.Certificados.NumeroSerie :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);
    if dtmdados.IQuery1.FieldByName('ambiente_cte').AsString = 'H' then
      frmMenu.ACBrCte1.Configuracoes.WebServices.Ambiente := taHomologacao
    else
      frmMenu.ACBrCte1.Configuracoes.WebServices.Ambiente := taProducao;
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyHost :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPort :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyUser :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
    frmMenu.ACBrCte1.Configuracoes.WebServices.ProxyPass :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
    // parametros da filial
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add('select * from tb_empresa where codfil = :0 ');
    dtmdados.IQuery1.Parameters[0].value := glbFilial;
    dtmdados.IQuery1.open;
    glbVerao := dtmdados.IQuery1.FieldByName('verao').AsString;

    frmMenu.ACBrMail1.Host := Trim(dtmdados.IQuery1.FieldByName('smtp')
      .AsString);
    frmMenu.ACBrMail1.Password := Trim(dtmdados.IQuery1.FieldByName('senha')
      .AsString);
    frmMenu.ACBrMail1.Username := Trim(dtmdados.IQuery1.FieldByName('email')
      .AsString);
    frmMenu.ACBrMail1.From := Trim(dtmdados.IQuery1.FieldByName('email')
      .AsString);
    frmMenu.ACBrMail1.Port := dtmdados.IQuery1.FieldByName('porta').value;

    if dtmdados.IQuery1.FieldByName('senha_a3').value <> ' ' then
      frmMenu.ACBrCte1.Configuracoes.Certificados.Senha :=
        ALLTRIM(dtmdados.IQuery1.FieldByName('senha_a3').AsAnsiString);
    glbtrip := dtmdados.IQuery1.FieldByName('trip').AsString;
    glbimpr := dtmdados.IQuery1.FieldByName('IMP_ETI_VOL').AsString;
    frmMenu.ACBrCTeDACTeRL1.Logo := dtmdados.IQuery1.FieldByName('logo').value;
    frmMenu.ACBrCTeDACTeRL1.Sistema := '__';
    dtmdados.IQuery1.close;
  end;

  close;
end;

procedure TFrmLogin.edSenhaEnter(Sender: TObject);
begin
  QSite.close;
  IdSite.LookupValue := '';
  btnOk.Enabled := false;
end;

procedure TFrmLogin.edSenhaExit(Sender: TObject);
begin
  if edSenha.Text <> '' then
  begin
    QPass.close;
    QPass.Parameters[0].value := edUsuario.Text;
    QPass.Parameters[1].value := GLBCodUser;
    QPass.Parameters[2].value := edSenha.Text;
    QPass.open;
    if QPass.Eof then
    begin
      ShowMessage('Senha n�o confere !!');
      edSenha.SetFocus;
    end
    else
      btnOk.Enabled := true;

    QSite.open;
  end;
end;

procedure TFrmLogin.edUsuarioEnter(Sender: TObject);
begin
  QSite.close;
  IdSite.LookupValue := '';
  btnOk.Enabled := false;
end;

procedure TFrmLogin.edUsuarioExit(Sender: TObject);
var
  emp: String;
begin
  if edUsuario.Text <> '' then
  begin
    QSenha.close;
    QSenha.Parameters[0].value := edUsuario.Text;
    QSenha.open;
    if QSenha.Eof then
    begin
      ShowMessage('Usu�rio N�o Cadastrado');
      edUsuario.SetFocus;
      exit;
    end;
    GLBCodUser := QSenhaCOD_USUARIO.AsInteger;
    emp := QSenhaSITE.AsString;
    QSite.close;
    QSite.sql[4]:= 'and f.codfil in (' + emp + ')';
    QSite.open;
  end;
end;

procedure TFrmLogin.btnCancelarClick(Sender: TObject);
begin
  dtmdados.Destroy;
  dtmdados := nil;
  Application.Terminate;
end;

procedure TFrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QSite.close;
  Action := caFree;
  FrmLogin := nil;
end;

end.
