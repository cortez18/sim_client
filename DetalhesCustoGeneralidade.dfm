object frmDetalhesCustoGeneralidade: TfrmDetalhesCustoGeneralidade
  Left = 0
  Top = 0
  Caption = 'Detalhes Custo da Generalidade'
  ClientHeight = 296
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 13
    Width = 62
    Height = 13
    Caption = 'CT-e Original'
    FocusControl = DBEdit1
  end
  object Label2: TLabel
    Left = 164
    Top = 13
    Width = 47
    Height = 13
    Caption = 'Manifesto'
    FocusControl = DBEdit2
  end
  object Label3: TLabel
    Left = 20
    Top = 71
    Width = 36
    Height = 13
    Caption = 'Usu'#225'rio'
    FocusControl = DBEdit3
  end
  object Label4: TLabel
    Left = 20
    Top = 131
    Width = 84
    Height = 13
    Caption = 'Tipo de Cobran'#231'a'
    FocusControl = DBEdit4
  end
  object Label5: TLabel
    Left = 20
    Top = 186
    Width = 33
    Height = 13
    Caption = 'Ve'#237'culo'
    FocusControl = DBEdit5
  end
  object Label6: TLabel
    Left = 20
    Top = 244
    Width = 129
    Height = 13
    Caption = 'C'#243'digo da Tabela de Custo'
    FocusControl = DBEdit6
  end
  object Label7: TLabel
    Left = 164
    Top = 244
    Width = 137
    Height = 13
    Caption = 'C'#243'digo da Tabela de Receita'
    FocusControl = DBEdit7
  end
  object DBEdit1: TDBEdit
    Left = 20
    Top = 29
    Width = 121
    Height = 21
    DataField = 'CTE'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 164
    Top = 29
    Width = 121
    Height = 21
    DataField = 'MANIFESTO'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 1
  end
  object DBEdit3: TDBEdit
    Left = 20
    Top = 86
    Width = 121
    Height = 21
    DataField = 'USUARIO_INC'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 2
  end
  object DBEdit4: TDBEdit
    Left = 20
    Top = 147
    Width = 121
    Height = 21
    DataField = 'TIPO'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 3
  end
  object DBEdit5: TDBEdit
    Left = 20
    Top = 203
    Width = 121
    Height = 21
    DataField = 'VEICULO'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 4
  end
  object DBEdit6: TDBEdit
    Left = 20
    Top = 263
    Width = 121
    Height = 21
    DataField = 'TABELA_CUSTO'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 5
  end
  object DBEdit7: TDBEdit
    Left = 164
    Top = 263
    Width = 121
    Height = 21
    DataField = 'TABELA_RECEITA'
    DataSource = dtsGener
    ReadOnly = True
    TabOrder = 6
  end
  object QGener: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Size = 3
        Value = 378
      end>
    SQL.Strings = (
      
        'select g.cte, g.manifesto, g.usuario_inc, case when g.tipo = '#39'A'#39 +
        ' then '#39'CUSTO E RECEITA'#39
      
        '                                               when g.tipo = '#39'C'#39 +
        ' then '#39'CUSTO'#39
      
        '                                               else '#39'RECEITA'#39' en' +
        'd tipo, '
      'g.veiculo, g.tabela_custo, g.tabela_receita'
      'from crm_generalidades g'
      'where g.id_generalidade = :id')
    Left = 9
    Top = 8
    object QGenerCTE: TFMTBCDField
      FieldName = 'CTE'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QGenerMANIFESTO: TFMTBCDField
      FieldName = 'MANIFESTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QGenerUSUARIO_INC: TStringField
      FieldName = 'USUARIO_INC'
      ReadOnly = True
      Size = 30
    end
    object QGenerTIPO: TStringField
      FieldName = 'TIPO'
      ReadOnly = True
      Size = 15
    end
    object QGenerVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 50
    end
    object QGenerTABELA_CUSTO: TFMTBCDField
      FieldName = 'TABELA_CUSTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QGenerTABELA_RECEITA: TFMTBCDField
      FieldName = 'TABELA_RECEITA'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
  end
  object dtsGener: TDataSource
    DataSet = QGener
    Left = 9
    Top = 64
  end
end
