unit impressao_custo_frete;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RLReport, Vcl.Imaging.jpeg, Data.DB,
  Data.Win.ADODB, RLParser, RLRichText;

type
  TfrmImpressao_custofrete = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLIMIW: TRLImage;
    RlImInt: TRLImage;
    RLLabel1: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel3: TRLLabel;
    RLBand3: TRLBand;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLTTPagarDes: TRLLabel;
    RLLabel17: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText3: TRLDBText;
    RLTTAdv: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RlObs: TRLRichText;
    RLDBResult6: TRLDBResult;
    RLGroup1: TRLGroup;
    RLBand5: TRLBand;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText6: TRLDBText;
    RLBand2: TRLBand;
    RlCodfic: TRLLabel;
    RlAd: TRLLabel;
    RlCodcon: TRLLabel;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLBand4: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    QRelatorio: TADOQuery;
    QRelatorioTIPO: TStringField;
    QRelatorioDATA: TDateTimeField;
    QRelatorioDOC: TBCDField;
    QRelatorioCODCON: TBCDField;
    QRelatorioTOTFRE: TBCDField;
    QRelatorioCTE_CUSTO: TBCDField;
    QRelatorioVALOR_COBRADO: TBCDField;
    QRelatorioCUSTO_CALC: TBCDField;
    QRelatorioNM_FORNECEDOR: TStringField;
    QRelatorioMARGEM: TBCDField;
    QRelatorioFATURA: TStringField;
    QRelatorioFILIAL: TBCDField;
    QRelatorioTRANSP: TBCDField;
    QRelatorioCNPJ: TStringField;
    QRelatorioDT_FATURA: TDateTimeField;
    QRelatorioCUSTO: TFMTBCDField;
    QRelatorioCODFIL: TFMTBCDField;
    dtsRelatorio: TDataSource;
    RLExpressionParser1: TRLExpressionParser;
    QDesconto: TADOQuery;
    QDescontoDESCONTO: TBCDField;
    QDescontoOBS: TStringField;
    QDescontoVL_IMPOSTOS: TBCDField;
    QDescontoPROCESSO: TBCDField;
    QResumo: TADOQuery;
    QResumoRECEITA: TBCDField;
    QResumoCUSTO: TBCDField;
    QResumoMARGEM: TBCDField;
    QAdiant: TADOQuery;
    QAdiantCODFIC: TBCDField;
    QAdiantOBSERV: TStringField;
    QAdiantVLRADI: TBCDField;
    dtsResumo: TDataSource;
    dtsDesconto: TDataSource;
    RLDBText14: TRLDBText;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLGroup1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    man: integer;
    vladi: real;
  public
    { Public declarations }
  end;

var
  frmImpressao_custofrete: TfrmImpressao_custofrete;

implementation

{$R *.dfm}

uses Dados;

procedure TfrmImpressao_custofrete.FormCreate(Sender: TObject);
begin
  man := 0;
end;

procedure TfrmImpressao_custofrete.RLBand2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if (man <> QRelatorioDOC.AsInteger) then
  begin
    QAdiant.close;
    QAdiant.Parameters[0].Value := QRelatorioDOC.AsInteger;
    QAdiant.Parameters[1].Value := QRelatorioFILIAL.AsInteger; // CODFIL.AsInteger;
    QAdiant.Parameters[2].Value := QRelatorioTRANSP.AsInteger;
    QAdiant.open;
    if not QAdiant.Eof then
    begin
      RlCodfic.Caption := 'REPOM  ' + QAdiantCODFIC.AsString;
      RlAd.Caption := FormatFloat('#,##0.00', QAdiantVLRADI.Value);
      vladi := vladi + QAdiantVLRADI.Value;
    end
    else
    begin
      RlCodfic.Caption := '';
      RlAd.Caption := '';
    end;
    man := QRelatorioDOC.AsInteger;
  end
  else
  begin
    RlCodfic.Caption := '';
    RlAd.Caption := '';
  end;
end;

procedure TfrmImpressao_custofrete.RLBand3BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  ttpg: real;
begin
  RlObs.Lines.Clear;
  ttpg := RLDBResult6.Value - vladi;
  RLTTAdv.Caption := FormatFloat('#,##0.00', vladi);
  //RLTTPagar.Caption := FormatFloat('#,##0.00', ttpg);
  RLTTPagarDes.Caption := FormatFloat('#,##0.00', ttpg - QDescontoDESCONTO.Value
    - QDescontoPROCESSO.Value);
  RlObs.Lines.Add(QDescontoOBS.AsString);
end;

procedure TfrmImpressao_custofrete.RLGroup1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  QResumo.close;
  QResumo.Parameters[0].Value := QRelatorioFATURA.Value;
  QResumo.Parameters[1].Value := QRelatorioDOC.AsInteger;
  QResumo.Parameters[2].Value := QRelatorioTRANSP.AsInteger;
  QResumo.open;

  QDesconto.close;
  QDesconto.Parameters[0].Value := QRelatorioFATURA.Value;
  QDesconto.Parameters[1].Value := QRelatorioTRANSP.AsInteger;
  QDesconto.Parameters[2].Value := QRelatorioFATURA.Value;
  QDesconto.Parameters[3].Value := QRelatorioTRANSP.AsInteger;
  QDesconto.open;
end;

procedure TfrmImpressao_custofrete.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLTTPagarDes.Caption := '';

  RLLabel1.Caption := 'Transportador : ' + QRelatorioNM_FORNECEDOR.Value + ' - '
    + QRelatorioCNPJ.AsString;
  RLLabel2.Caption := 'Relatório de Fornecedor - : ' +
    QRelatorioDT_FATURA.AsString;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add
    ('select substr(razsoc,1,instr(razsoc,'' '')-1) filial from rodfil ');
  dtmDados.RQuery1.sql.Add('where codfil = ' + QRelatorioFILIAL.AsString);
  dtmDados.RQuery1.open;
  if dtmDados.RQuery1.FieldByName('filial').Value = 'IW' then
  begin
    RLIMIW.Enabled := true;
    RlImInt.Enabled := false;
  end
  else
  begin
    RLIMIW.Enabled := false;
    RlImInt.Enabled := true;
  end;
  dtmDados.RQuery1.close;
  man := 0;
  vladi := 0;
  RlLabel3.Caption := 'Fatura : ' + QRelatorioFATURA.AsString;
end;

end.
