unit Dacte_Cte_2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, DBClient, RLReport, math, jpeg, RLBarcode,
  RLPreviewForm, RLParser, pcnConversao, RLFilters, RLPDFFilter,
  JvMemoryDataset, ACBrDelphiZXingQRCode;

type
  TfrmDacte_Cte_2 = class(TForm)
    dsCteEletronico: TDataSource;
    dsNf: TDataSource;
    rlDacte: TRLReport;
    RLBand1: TRLBand;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLDraw3: TRLDraw;
    RLDraw4: TRLDraw;
    RLDraw5: TRLDraw;
    RLLabel2: TRLLabel;
    RLMemo1: TRLMemo;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLDraw6: TRLDraw;
    RLDraw7: TRLDraw;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLDraw8: TRLDraw;
    RLLabel8: TRLLabel;
    RLDraw9: TRLDraw;
    RLLabel9: TRLLabel;
    RLLabel10: TRLLabel;
    RLDBBarcode1: TRLDBBarcode;
    RLDraw10: TRLDraw;
    RLDraw11: TRLDraw;
    RLDraw12: TRLDraw;
    RLDraw13: TRLDraw;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLDraw14: TRLDraw;
    RLDraw15: TRLDraw;
    RLLabel15: TRLLabel;
    RLLabel16: TRLLabel;
    RLDraw16: TRLDraw;
    RLLabel17: TRLLabel;
    RLDraw17: TRLDraw;
    RLDraw18: TRLDraw;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLDraw19: TRLDraw;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLDraw20: TRLDraw;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    rlImgIntecom: TRLImage;
    RLDBText1: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLLabel1: TRLLabel;
    RLLabel89: TRLLabel;
    edRemetente: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText15: TRLDBText;
    RLDBText16: TRLDBText;
    RLDBText17: TRLDBText;
    RLDBText18: TRLDBText;
    RLDBText23: TRLDBText;
    RLDBText24: TRLDBText;
    RLLabel91: TRLLabel;
    RLLabel92: TRLLabel;
    RLDBText19: TRLDBText;
    RLDBText20: TRLDBText;
    RLDBText21: TRLDBText;
    RLDBText22: TRLDBText;
    RLDBText25: TRLDBText;
    RLDBText26: TRLDBText;
    RLDBText27: TRLDBText;
    RLDBText28: TRLDBText;
    Munini: TRLDBText;
    Mundest: TRLDBText;
    RLLabel52: TRLLabel;
    RLLabel54: TRLLabel;
    RLLabel55: TRLLabel;
    RLLabel56: TRLLabel;
    RLLabel57: TRLLabel;
    RLLabel58: TRLLabel;
    RLLabel59: TRLLabel;
    RLLabel60: TRLLabel;
    RLDraw24: TRLDraw;
    RLDraw25: TRLDraw;
    RLDraw26: TRLDraw;
    RLLabel61: TRLLabel;
    RLLabel62: TRLLabel;
    RLLabel63: TRLLabel;
    RLDraw27: TRLDraw;
    RLDraw28: TRLDraw;
    RLDraw29: TRLDraw;
    RLDraw30: TRLDraw;
    RLLabel64: TRLLabel;
    RLLabel65: TRLLabel;
    RLLabel67: TRLLabel;
    RLDraw31: TRLDraw;
    RLLabel68: TRLLabel;
    RLDraw32: TRLDraw;
    RLLabel69: TRLLabel;
    RLDraw33: TRLDraw;
    RLLabel70: TRLLabel;
    RLDraw34: TRLDraw;
    RLLabel53: TRLLabel;
    RLDraw35: TRLDraw;
    RLLabel73: TRLLabel;
    RLLabel76: TRLLabel;
    RLDraw36: TRLDraw;
    RLLabel74: TRLLabel;
    RLLabel75: TRLLabel;
    RLDraw37: TRLDraw;
    RLLabel77: TRLLabel;
    RLLabel78: TRLLabel;
    RLDraw38: TRLDraw;
    RLLabel79: TRLLabel;
    RLDraw39: TRLDraw;
    RLLabel81: TRLLabel;
    RLDraw40: TRLDraw;
    RLLabel82: TRLLabel;
    RLDraw41: TRLDraw;
    RLLabel83: TRLLabel;
    RLDraw42: TRLDraw;
    RLLabel84: TRLLabel;
    RLDraw43: TRLDraw;
    RLLabel86: TRLLabel;
    RLDraw44: TRLDraw;
    RLLabel88: TRLLabel;
    RLDraw45: TRLDraw;
    RLLabel90: TRLLabel;
    RLDraw46: TRLDraw;
    RLLabel85: TRLLabel;
    RLDraw47: TRLDraw;
    RLLabel87: TRLLabel;
    RLDraw21: TRLDraw;
    RLLabel37: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel39: TRLLabel;
    RLLabel40: TRLLabel;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel43: TRLLabel;
    RLDraw22: TRLDraw;
    RLLabel45: TRLLabel;
    RLLabel46: TRLLabel;
    RLLabel47: TRLLabel;
    RLLabel48: TRLLabel;
    RLLabel49: TRLLabel;
    RLLabel50: TRLLabel;
    RLLabel51: TRLLabel;
    RLDBText30: TRLDBText;
    RLDBText31: TRLDBText;
    RLDBText32: TRLDBText;
    RLDBText33: TRLDBText;
    RLDBText34: TRLDBText;
    RLDBText35: TRLDBText;
    RLDBText36: TRLDBText;
    RLDraw48: TRLDraw;
    RLLabel71: TRLLabel;
    RLDraw49: TRLDraw;
    RLLabel72: TRLLabel;
    rlbTomador: TRLDBText;
    lgrTomador: TRLDBText;
    cnpjTomador: TRLDBText;
    munTomador: TRLDBText;
    cepTomador: TRLDBText;
    inscTomador: TRLDBText;
    foneTomador: TRLDBText;
    RLDBText47: TRLDBText;
    RLDBText41: TRLDBText;
    RLDBText48: TRLDBText;
    RLDBText49: TRLDBText;
    RLDBText50: TRLDBText;
    RLDBText52: TRLDBText;
    RLDBText64: TRLDBText;
    RLDBText65: TRLDBText;
    RLDBText66: TRLDBText;
    RLDBText67: TRLDBText;
    RLDBText68: TRLDBText;
    RLLabel36: TRLLabel;
    RLLabel44: TRLLabel;
    RLDraw54: TRLDraw;
    lbDocumentos: TRLLabel;
    RLDraw57: TRLDraw;
    RLLabel97: TRLLabel;
    RLLabel98: TRLLabel;
    RLLabel99: TRLLabel;
    RLLabel102: TRLLabel;
    RLLabel101: TRLLabel;
    RLLabel100: TRLLabel;
    RLDraw80: TRLDraw;
    RLDraw50: TRLDraw;
    RLDraw51: TRLDraw;
    RLDraw53: TRLDraw;
    RLDraw52: TRLDraw;
    RLSystemInfo1: TRLSystemInfo;
    RLSystemInfo2: TRLSystemInfo;
    RLLabel103: TRLLabel;
    rlbProtocolo: TRLLabel;
    RLDBText84: TRLDBText;
    RLDraw91: TRLDraw;
    RLDraw93: TRLDraw;
    RLDraw92: TRLDraw;
    RLDraw94: TRLDraw;
    RLDraw95: TRLDraw;
    RLLabel96: TRLLabel;
    RLDraw96: TRLDraw;
    RLLabel109: TRLLabel;
    RLLabel133: TRLLabel;
    RLLabel134: TRLLabel;
    RLDraw97: TRLDraw;
    RLDraw98: TRLDraw;
    RLLabel135: TRLLabel;
    RLDraw99: TRLDraw;
    RLDraw100: TRLDraw;
    RLLabel136: TRLLabel;
    RLLabel137: TRLLabel;
    RLLabel138: TRLLabel;
    RLLabel139: TRLLabel;
    RLDBText72: TRLDBText;
    RLDBText85: TRLDBText;
    RLBand2: TRLBand;
    RLDBText29: TRLDBText;
    RLDraw55: TRLDraw;
    RLDraw56: TRLDraw;
    RLDraw58: TRLDraw;
    RLDraw59: TRLDraw;
    RLDraw60: TRLDraw;
    RLDraw72: TRLDraw;
    RLDraw61: TRLDraw;
    RLDraw62: TRLDraw;
    RLDBText69: TRLDBText;
    RLDBText80: TRLDBText;
    RLDBText81: TRLDBText;
    RLDBText82: TRLDBText;
    RLDBText83: TRLDBText;
    RLBand3: TRLBand;
    RLDraw63: TRLDraw;
    RLLabel104: TRLLabel;
    RLDraw64: TRLDraw;
    RLDraw65: TRLDraw;
    RLDraw66: TRLDraw;
    RLLabel105: TRLLabel;
    RLDBText78: TRLDBText;
    RLDraw67: TRLDraw;
    RLLabel132: TRLLabel;
    RLLabel106: TRLLabel;
    RLLabel107: TRLLabel;
    RLDBText73: TRLDBText;
    RLLabel108: TRLLabel;
    RLDraw69: TRLDraw;
    RLDraw71: TRLDraw;
    RLDBText11: TRLDBText;
    RLDraw101: TRLDraw;
    rlDraw: TRLDraw;
    rlImgIW: TRLImage;
    RLDBText39: TRLDBText;
    RLDBText40: TRLDBText;
    RLDBText42: TRLDBText;
    RLDBText43: TRLDBText;
    RLDBText44: TRLDBText;
    RLDBText45: TRLDBText;
    RLDBText46: TRLDBText;
    RLDBText86: TRLDBText;
    RLDraw23: TRLDraw;
    RLDraw102: TRLDraw;
    RLLabel140: TRLLabel;
    RLDBText88: TRLDBText;
    RLLabel141: TRLLabel;
    RLDBText89: TRLDBText;
    RLLabel142: TRLLabel;
    RLLabel143: TRLLabel;
    RLDBText90: TRLDBText;
    RLLabel144: TRLLabel;
    RLDBText91: TRLDBText;
    RLDBMemo1: TRLDBMemo;
    RLLabel145: TRLLabel;
    RLDBText87: TRLDBText;
    RLLabel146: TRLLabel;
    RLDBText92: TRLDBText;
    RLLabel147: TRLLabel;
    RLDBText93: TRLDBText;
    RLLabel148: TRLLabel;
    RLDBText94: TRLDBText;
    RLLabel149: TRLLabel;
    RLDBText95: TRLDBText;
    RLLabel150: TRLLabel;
    RLDBText96: TRLDBText;
    RLDBText97: TRLDBText;
    RLDBText98: TRLDBText;
    RLLabel93: TRLLabel;
    RLDBText57: TRLDBText;
    RLDBText99: TRLDBText;
    RLExpressionParser1: TRLExpressionParser;
    RLDraw74: TRLDraw;
    RLLabel66: TRLLabel;
    RLDraw103: TRLDraw;
    RLDraw104: TRLDraw;
    RLLabel94: TRLLabel;
    RLLabel95: TRLLabel;
    RLDraw105: TRLDraw;
    RLLabel151: TRLLabel;
    RLDraw106: TRLDraw;
    RLDraw107: TRLDraw;
    RLLabel153: TRLLabel;
    RLDraw108: TRLDraw;
    RLLabel154: TRLLabel;
    RLDraw109: TRLDraw;
    RLLabel155: TRLLabel;
    RLDraw110: TRLDraw;
    RLLabel156: TRLLabel;
    RLDraw111: TRLDraw;
    RLLabel157: TRLLabel;
    RLDraw112: TRLDraw;
    RLDraw113: TRLDraw;
    RLDraw114: TRLDraw;
    RLDraw115: TRLDraw;
    RLDraw68: TRLDraw;
    RLDraw70: TRLDraw;
    RLDraw73: TRLDraw;
    RLDraw75: TRLDraw;
    RLDraw77: TRLDraw;
    RLLabel112: TRLLabel;
    RLLabel113: TRLLabel;
    RLDraw78: TRLDraw;
    RLLabel110: TRLLabel;
    RLDraw79: TRLDraw;
    RLLabel111: TRLLabel;
    RLDraw81: TRLDraw;
    RLLabel114: TRLLabel;
    RLLabel115: TRLLabel;
    RLDBText53: TRLDBText;
    RLDBText70: TRLDBText;
    RLDBText71: TRLDBText;
    RLDBText79: TRLDBText;
    RLMemo2: TRLMemo;
    RLMemo3: TRLMemo;
    RLMemo4: TRLMemo;
    RLMemo5: TRLMemo;
    RLLabel116: TRLLabel;
    RLMemo6: TRLMemo;
    RLMemo7: TRLMemo;
    RLMemo8: TRLMemo;
    RLMemo9: TRLMemo;
    RLEnd: TRLLabel;
    rllSitTrib: TRLLabel;
    RLDBMemo2: TRLDBMemo;
    RLLmsg: TRLLabel;
    RlPedido: TRLLabel;
    QPedido: TADOQuery;
    QPedidoORDCOM: TStringField;
    RLDBText3: TRLDBText;
    RLLabel117: TRLLabel;
    RLLabel118: TRLLabel;
    RLDBText37: TRLDBText;
    RLLabel119: TRLLabel;
    RLDBText38: TRLDBText;
    RLLabel120: TRLLabel;
    RLDBText54: TRLDBText;
    RLLabel121: TRLLabel;
    MdNF: TJvMemoryData;
    MdNFTIPODOC: TStringField;
    MdNFCNPJ_EMITE_CHAVE: TStringField;
    MdNFNUMERO: TStringField;
    MdNFTIPODOC2: TStringField;
    MdNFCNPJ_EMITE_CHAVE2: TStringField;
    MdNFNUMERO2: TStringField;
    rllMsgTeste: TRLLabel;
    imgQRCode: TRLImage;
    RLDraw76: TRLDraw;
    procedure FormCreate(Sender: TObject);

    procedure RLDBText11BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLDBText10BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLDBMemo1BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLDBText18BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLLabel91BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLLabel92BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLDBBarcode1BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure qrCteEletronicoCHAVECTEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure rlDacteBeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure PintarQRCode(const QRCodeData: String; APict: TPicture;
                           const AEncoding: TQRCodeEncoding);

  private
    { Private declarations }
  public
    { Public declarations }
    bContigencia: Boolean;
    sPath_contingecia: String;
    bGravaProt: String;
    sChaveCte: String;
  end;

var
  frmDacte_Cte_2: TfrmDacte_Cte_2;

implementation

uses Dados, menu, funcoes, ACBrUtil;
{$R *.dfm}

procedure TfrmDacte_Cte_2.FormCreate(Sender: TObject);
var
  sChaveNfe, impressora, sSerie, cgc: String;
  iTot, prnIndex, i, iAntes: integer;
  qrProt, qrTempo: TADOQuery;
  prnList: TStrings;
  bAchou: Boolean;
begin

  rllMsgTeste.Visible := False;
  rllMsgTeste.Enabled := False;

  dtmdados.rQuery1.Close;
  dtmdados.rQuery1.SQL.Clear;
  dtmdados.rQuery1.SQL.add
    ('Select codcgc nr_cnpj_cpf from rodfil where codfil = :0');
  dtmdados.rQuery1.Parameters[0].value := glbfilial;
  dtmdados.rQuery1.open;
  cgc := dtmdados.rQuery1.FieldByName('nr_cnpj_cpf').value;
  dtmdados.rQuery1.Close;
  Munini.Text := frmMenu.qrCteEletronicoMUNINI.AsString + ' - ' +
    frmMenu.qrCteEletronicoUFINI.AsString;
  Mundest.Text := frmMenu.qrCteEletronicoXMUNFIM.AsString + ' - ' +
    frmMenu.qrCteEletronicoUFFIM.AsString;

  if frmMenu.qrCteEletronicoPROTOCOLO.IsNull then
  begin
    RLDBBarcode1.Visible := False;
    RLLmsg.Visible := true;
  end
  else
  begin
    RLDBBarcode1.Visible := true;
    RLLmsg.Visible := False;
    rlbProtocolo.Caption := frmMenu.qrCteEletronicoPROTOCOLO.AsString;
  end;
  if (frmMenu.qrCteEletronicoTIPO.AsString = 'N') then
  begin
    exit;
    Close;
  end;


  if GlbCteAmb = 'H' then
  begin
    rllMsgTeste.Caption := ACBrStr('AMBIENTE DE HOMOLOGA��O - SEM VALOR FISCAL');
    rllMsgTeste.Visible := True;
    rllMsgTeste.Enabled := True;
  end
  else
  begin
    if frmMenu.qrCteEletronicoFL_STATUS.value = 'X' then
    begin
      rllMsgTeste.Caption := 'CT-e CANCELADO';
      rllMsgTeste.Visible := True;
      rllMsgTeste.Enabled := True;
    end
    else
    begin
      {if FCTe.procCTe.cStat = 110 then
       begin
        rllMsgTeste.Caption := 'CT-e DENEGADO';
        rllMsgTeste.Visible := True;
        rllMsgTeste.Enabled := True;
       end;

      if not FCTe.procCTe.cStat in [101, 110, 100] then
       begin
        rllMsgTeste.Caption := FCTe.procCTe.xMotivo;
        rllMsgTeste.Visible := True;
        rllMsgTeste.Enabled := True;
       end;
     end
     else
     begin }
     if frmMenu.qrCteEletronicoCTE_PROT.IsNull then
     begin
       rllMsgTeste.Caption := ACBrStr('CT-E N�O ENVIADO PARA SEFAZ');
       rllMsgTeste.Visible := True;
       rllMsgTeste.Enabled := True;
     end;
   end;
  end;


  frmMenu.QNF.first;
  MdNF.open;
  iTot := 0;
  while not frmMenu.QNF.Eof do
  begin
    inc(iTot);
    sChaveNfe := frmMenu.QNFNOTNFE.AsString;
    MdNF.Append;
    MdNFTIPODOC.AsString := 'NF-e';
    MdNFCNPJ_EMITE_CHAVE.AsString := frmMenu.qrCteEletronicoCNPJREM.AsString +
      ' / ' + frmMenu.QNFNOTNFE.AsString;
    MdNFNUMERO.AsString := RetZero(Alltrim(frmMenu.QNFSERIEN.AsString), 3) +
      ' / ' + RetZero(frmMenu.QNFNOTFIS.AsString, 9);
    frmMenu.QNF.Next;
    if (not frmMenu.QNF.Eof) then
    begin
      inc(iTot);
      MdNFTIPODOC2.AsString := 'NF-e';
      MdNFCNPJ_EMITE_CHAVE2.AsString := frmMenu.qrCteEletronicoCNPJREM.AsString
        + ' / ' + frmMenu.QNFNOTNFE.AsString;
      MdNFNUMERO2.AsString := RetZero(Alltrim(frmMenu.QNFSERIEN.AsString), 3) +
        ' / ' + RetZero(frmMenu.QNFNOTFIS.AsString, 9);
      frmMenu.QNF.Next;
    end;
    MdNF.Post;
  end;

  lbDocumentos.Caption := lbDocumentos.Caption + ': ' + IntToStr(iTot);
  if (copy(frmMenu.qrCteEletronicoNOMEEMI.AsString, 1, 2) = 'IW') then
  begin
    rlImgIntecom.Visible := False;
    rlImgIW.Left := rlImgIntecom.Left;
    rlImgIW.Visible := true;
  end
  else
  begin
    rlImgIntecom.Visible := true;
    rlImgIW.Visible := False;
  end;
  RLEnd.Caption := frmMenu.qrCteEletronicoLGREMI.value + ',' +
    frmMenu.qrCteEletronicoNROEMI.AsString + ' - ' +
    frmMenu.qrCteEletronicoBAIRROEMMI.AsString;
  RLDBText33.Visible := not frmMenu.qrCteEletronicoNOMERED.IsNull;
  RLDBText44.Visible := not frmMenu.qrCteEletronicoNOMEEXP.IsNull;
  RLExpressionParser1.Expression := 'RLDBResult1';
  case frmMenu.qrCteEletronicoCSTIMPOSTO.AsInteger of
    00:
      begin
        rllSitTrib.Caption := CSTICMSToStr(cst00) + '-' +
          CSTICMSToStrTagPosText(cst00);
      end;
    40:
      begin
        rllSitTrib.Caption := CSTICMSToStr(cst40) + '-' +
          CSTICMSToStrTagPosText(cst40);
      end;
    41:
      begin
        rllSitTrib.Caption := CSTICMSToStr(cst41) + '-' +
          CSTICMSToStrTagPosText(cst41);
      end;
    51:
      begin
        rllSitTrib.Caption := CSTICMSToStr(cst51) + '-' +
          CSTICMSToStrTagPosText(cst51);
      end;
    90:
      begin
        rllSitTrib.Caption := CSTICMSToStr(cst90) + '-' +
          CSTICMSToStrTagPosText(cst90);
      end;
  end;
  QPedido.Close;
  QPedido.Parameters[0].value := frmMenu.qrCteEletronicoNUMEROCTE.AsInteger;
  QPedido.Parameters[1].value := glbfilial;
  QPedido.open;
  RlPedido.Caption := 'PEDIDO :' + QPedidoORDCOM.AsString;
  QPedido.Close;
end;

procedure TfrmDacte_Cte_2.qrCteEletronicoCHAVECTEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if (sChaveCte <> '') then
    Text := sChaveCte;
end;

procedure TfrmDacte_Cte_2.RLBand1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  i, j: integer;
  s, s1, texto: string;
begin
  j := 0;
  for i := 182 to 202 - 1 do
  begin
    s := frmMenu.qrCteEletronico.Fields[i].FieldName;
    if frmMenu.qrCteEletronico.FieldByName(s).AsFloat <> 0 then
    begin
      case j of
        0, 4, 8, 12, 16:
          begin
            s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
            texto := StringReplace(s1, 'XML', ' ',
              [rfReplaceAll, rfIgnoreCase]);
            RLMemo2.Lines.add(texto);
            RLMemo3.Lines.add(FORMATFLOAT('#,##0.00',
              frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
          end;
        1, 5, 9, 13, 17:
          begin
            s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
            texto := StringReplace(s1, 'XML', ' ',
              [rfReplaceAll, rfIgnoreCase]);
            RLMemo4.Lines.add(texto);
            RLMemo5.Lines.add(FORMATFLOAT('#,##0.00',
              frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
          end;
        2, 6, 10, 14, 18:
          begin
            s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
            texto := StringReplace(s1, 'XML', ' ',
              [rfReplaceAll, rfIgnoreCase]);
            RLMemo6.Lines.add(texto);
            RLMemo7.Lines.add(FORMATFLOAT('#,##0.00',
              frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
          end;
        3, 7, 11, 15, 19:
          begin
            s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
            texto := StringReplace(s1, 'XML', ' ',
              [rfReplaceAll, rfIgnoreCase]);
            RLMemo8.Lines.add(texto);
            RLMemo9.Lines.add(FORMATFLOAT('#,##0.00',
              frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
          end;
      end;
      inc(j);
    end;
  end;
  RLLabel121.Caption := FORMATFLOAT('#,##0.0000',
    frmMenu.qrCteEletronicoCUBAGEM.value);
end;

procedure TfrmDacte_Cte_2.rlDacteBeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  i, j: integer;
  s, s1, texto: string;
begin
  { j :=0;
    for I := 182 to 202 - 1 do
    begin
    s := frmMenu.qrCteEletronico.Fields[I].FieldName;
    if frmMenu.qrCteEletronico.FieldByName(s).AsFloat <> 0 then
    begin
    case j of
    0,4,8,12,16:
    begin
    s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
    texto := StringReplace(s1, 'XML', ' ', [rfReplaceAll, rfIgnoreCase]);
    RLMemo2.Lines.Add(texto);
    RLMemo3.Lines.Add(FORMATFLOAT('#,##0.00',frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
    end;
    1,5,9,13,17:
    begin
    s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
    texto := StringReplace(s1, 'XML', ' ', [rfReplaceAll, rfIgnoreCase]);
    RLMemo4.Lines.Add(texto);
    RLMemo5.Lines.Add(FORMATFLOAT('#,##0.00',frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
    end;
    2,6,10,14,18:
    begin
    s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
    texto := StringReplace(s1, 'XML', ' ', [rfReplaceAll, rfIgnoreCase]);
    RLMemo6.Lines.Add(texto);
    RLMemo7.Lines.Add(FORMATFLOAT('#,##0.00',frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
    end;
    3,7,11,15,19:
    begin
    s1 := StringReplace(s, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
    texto := StringReplace(s1, 'XML', ' ', [rfReplaceAll, rfIgnoreCase]);
    RLMemo8.Lines.Add(texto);
    RLMemo9.Lines.Add(FORMATFLOAT('#,##0.00',frmMenu.qrCteEletronico.FieldByName(s).AsFloat));
    end;
    end;
    inc(j);
    end;
    end;
    RlLabel121.Caption := FORMATFLOAT('#,##0.0000',frmMenu.qrCteEletronicoCUBAGEM.Value); }
  //if not EstaVazio(Trim(fpCTe.infCTeSupl.qrCodCTe)) then
  //  PintarQRCode(fpCTe.infCTeSupl.qrCodCTe, imgQRCode.Picture, qrUTF8NoBOM);
  {else
  begin
    rlsLinhaV07.Height     := 26;
    rlsLinhaH03.Width      := 427;
    RLDraw99.Width         := 427;
    rlbCodigoBarras.Width  := 419;
    rllVariavel1.Width     := 419;
    RLLabel198.Width       := 419;
    imgQRCode.Visible      := False;
  end;  }

end;

procedure TfrmDacte_Cte_2.PintarQRCode(const QRCodeData: String; APict: TPicture;
  const AEncoding: TQRCodeEncoding);
var
  QRCode: TDelphiZXingQRCode;
  QRCodeBitmap: TBitmap;
  Row, Column: Integer;
begin
  QRCode       := TDelphiZXingQRCode.Create;
  QRCodeBitmap := TBitmap.Create;
  try
    QRCode.Encoding  := AEncoding;
    QRCode.QuietZone := 1;
    QRCode.Data      := widestring(QRCodeData);

    //QRCodeBitmap.SetSize(QRCode.Rows, QRCode.Columns);
    QRCodeBitmap.Width  := QRCode.Columns;
    QRCodeBitmap.Height := QRCode.Rows;

    for Row := 0 to QRCode.Rows - 1 do
    begin
      for Column := 0 to QRCode.Columns - 1 do
      begin
        if (QRCode.IsBlack[Row, Column]) then
          QRCodeBitmap.Canvas.Pixels[Column, Row] := clBlack
        else
          QRCodeBitmap.Canvas.Pixels[Column, Row] := clWhite;
      end;
    end;

    APict.Assign(QRCodeBitmap);
  finally
    QRCode.Free;
    QRCodeBitmap.Free;
  end;
end;

procedure TfrmDacte_Cte_2.RLDBBarcode1BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  if (sChaveCte <> '') then
    Text := sChaveCte;
end;

procedure TfrmDacte_Cte_2.RLDBMemo1BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
var
  qrPedido: TADOQuery;
  sPedido: String;
begin
  if (Alltrim(sPedido) <> '') then
    Text := Text + sPedido;

  if (bContigencia) or ((frmMenu.qrCteEletronicoFL_CONTINGENCIA.AsString = 'S')
    and (frmMenu.qrCteEletronicoPROTOCOLO.IsNull)) then
    Text := Text + '  -  DACTE EM CONTING�NCIA -';
end;

procedure TfrmDacte_Cte_2.RLDBText10BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := BuscaTroca(Text, '.', '');
  Text := BuscaTroca(Text, '-', '');
  Text := BuscaTroca(Text, ' ', '');
  //
  if (Length(Text) = 8) then
    Text := copy(Text, 1, 2) + '.' + copy(Text, 3, 3) + '-' + copy(Text, 6, 3);
end;

procedure TfrmDacte_Cte_2.RLDBText11BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := BuscaTroca(Text, '.', '');
  Text := BuscaTroca(Text, '-', '');
  Text := BuscaTroca(Text, '/', '');
  Text := BuscaTroca(Text, ' ', '');
  //
  if (Length(Text) = 14) then
    Text := copy(Text, 1, 2) + '.' + copy(Text, 3, 3) + '.' + copy(Text, 6, 3) +
      '/' + copy(Text, 9, 4) + '-' + copy(Text, 13, 2);
end;

procedure TfrmDacte_Cte_2.RLDBText18BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := copy(Text, 01, 4) + ' ' + copy(Text, 05, 4) + ' ' + copy(Text, 09, 4)
    + ' ' + copy(Text, 13, 4) + ' ' + copy(Text, 17, 4) + ' ' +
    copy(Text, 21, 4) + ' ' + copy(Text, 25, 4) + ' ' + copy(Text, 29, 4) + ' '
    + copy(Text, 33, 4) + ' ' + copy(Text, 37, 4) + ' ' + copy(Text, 41, 4);
end;

procedure TfrmDacte_Cte_2.RLLabel91BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  if (bContigencia) or ((frmMenu.qrCteEletronicoFL_CONTINGENCIA.AsString = 'S')
    and (frmMenu.qrCteEletronicoPROTOCOLO.IsNull)) then
  begin
    Text := Text + 'CONTING�NCIA';
    exit;
  end;
  //
  if (frmMenu.qrCteEletronicoTPCTE.AsString = '1') then
    Text := 'CT-e COMPLEMENTAR'
  else
    Text := 'CT-e NORMAL';
end;

procedure TfrmDacte_Cte_2.RLLabel92BeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  if (frmMenu.qrCteEletronicoTPSERV.AsString = '0') then
    Text := 'SERVI�O NORMAL'
  else if (frmMenu.qrCteEletronicoTPSERV.AsString = '1') then
    Text := 'SUBCONTRATA��O'
  else if (frmMenu.qrCteEletronicoTPSERV.AsString = '2') then
    Text := 'REDESPACHO';
end;

end.
