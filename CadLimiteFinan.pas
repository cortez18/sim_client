unit CadLimiteFinan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, JvExMask, JvToolEdit, JvBaseEdits, DB, ADODB,
  ImgList, ActnList, Mask, DBCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, ComCtrls, System.ImageList, System.Actions;

type
  TfrmCadLimiteFinan = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    Detalhes: TTabSheet;
    Label2: TLabel;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    lblEnde: TLabel;
    DBEdit1: TDBEdit;
    iml: TImageList;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnLimparFiltro: TBitBtn;
    QTabela: TADOQuery;
    dtstabela: TDataSource;
    GroupBox1: TGroupBox;
    Label27: TLabel;
    edLimite: TJvCalcEdit;
    QTabelaCODCLIENTE: TBCDField;
    QTabelaLIMITE: TBCDField;
    QTabelaNM_CLIENTE: TStringField;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    Prazo: TTabSheet;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    GroupBox2: TGroupBox;
    edPrazo: TJvCalcEdit;
    GroupBox3: TGroupBox;
    cbDia: TComboBox;
    QTabelaprazo_pgto: TBCDField;
    QTabeladia_semana: TBCDField;
    QTabelasemana: TStringField;
    GroupBox4: TGroupBox;
    cbprazo: TCheckBox;
    QTabelaPRAZO_ESPECIAL: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure QTabelaCalcFields(DataSet: TDataSet);
    procedure edLimiteExit(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  frmCadLimiteFinan: TfrmCadLimiteFinan;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadLimiteFinan.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadLimiteFinan.btnSalvarClick(Sender: TObject);
var
  dias, rec: Integer;
begin
  rec := QTabelaCODCLIENTE.AsInteger;

  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.add
    ('select count(*) tem from TB_LIMITE_FATURAMENTO where codcliente = :0');
  dtmdados.IQuery1.Parameters[0].value := QTabelaCODCLIENTE.AsInteger;
  dtmdados.IQuery1.open;

  if dtmdados.IQuery1.FieldByName('tem').value = 0 then
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('Insert into TB_LIMITE_FATURAMENTO (codcliente, limite, prazo_pgto, dia_semana, prazo_especial) values (:0, :1, :2, :3, :4) ');
    dtmdados.IQuery1.Parameters[0].value := QTabelaCODCLIENTE.AsInteger;
    dtmdados.IQuery1.Parameters[1].value := edLimite.value;
    dtmdados.IQuery1.Parameters[2].value := edPrazo.AsInteger;
    if cbDia.ItemIndex > 0 then
      dtmdados.IQuery1.Parameters[3].value := cbDia.ItemIndex + 1
    else
      dtmdados.IQuery1.Parameters[3].value := 0;
    if cbprazo.Checked = true then  //Se estiver checado grava na tabela
    dtmdados.IQuery1.Parameters[4].value := 'S'
    else
    dtmdados.IQuery1.Parameters[4].value := 'N';
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
  end
  else
  begin
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('Update TB_LIMITE_FATURAMENTO set limite = :0, prazo_pgto = :1, dia_semana = :2, prazo_especial = :3 where codcliente = :4');
    dtmdados.IQuery1.Parameters[0].value := edLimite.value;
    dtmdados.IQuery1.Parameters[1].value := edPrazo.AsInteger;
    if cbDia.ItemIndex > 0 then
      dtmdados.IQuery1.Parameters[2].value := cbDia.ItemIndex + 1
    else
      dtmdados.IQuery1.Parameters[2].value := 0;
    if cbprazo.Checked = true then  //Se estiver checado grava na tabela
    dtmdados.IQuery1.Parameters[3].value := 'S'
    else
    dtmdados.IQuery1.Parameters[3].value := 'N';
    dtmdados.IQuery1.Parameters[4].value := QTabelaCODCLIENTE.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
  end;

  QTabela.Close;
  QTabela.open;
  QTabela.locate('codcliente', rec, []);
  PageControl1.ActivePageIndex := 0;

  btnAlterar.Enabled := true;
  btnSalvar.Enabled := false;

end;

procedure TfrmCadLimiteFinan.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadLimiteFinan.dbgContratoTitleClick(Column: TColumn);
var
  icount: Integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.SQL.Text) > 0 then
  begin
    QTabela.SQL.Text := Copy(QTabela.SQL.Text, 1,
      Pos('order by', QTabela.SQL.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.SQL.Text := QTabela.SQL.Text + ' order by ' + Column.FieldName;
  QTabela.open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadLimiteFinan.edLimiteExit(Sender: TObject);
begin
  if edlimite.Value > 10000 then
  begin
    ShowMessage('N�o est� Exagerado a Quantidade de CT-e por fatura ?');
    edLimite.SetFocus;
  end;
end;

procedure TfrmCadLimiteFinan.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmCadLimiteFinan.QTabelaAfterScroll(DataSet: TDataSet);
begin
  if QTabelaLIMITE.AsInteger >= 0 then
    edLimite.value := QTabelaLIMITE.AsInteger
  else
    edLimite.clear;

  if QTabeladia_semana.AsInteger > 0 then
    cbDia.ItemIndex := QTabeladia_semana.AsInteger - 1;

  edPrazo.AsInteger := QTabelaprazo_pgto.AsInteger;

  if QTabelaPRAZO_ESPECIAL.AsString = 'S' then   //Se estiver checado exibe no formulario
  cbprazo.Checked := True
  else
  cbprazo.Checked := False;

end;

procedure TfrmCadLimiteFinan.QTabelaCalcFields(DataSet: TDataSet);
begin
  if QTabeladia_semana.AsInteger = 2 then
    QTabelasemana.value := 'Segunda-Feira'
  else if QTabeladia_semana.AsInteger = 3 then
    QTabelasemana.value := 'Ter�a-Feira'
  else if QTabeladia_semana.AsInteger = 4 then
    QTabelasemana.value := 'Quarta-Feira'
  else if QTabeladia_semana.AsInteger = 5 then
    QTabelasemana.value := 'Quinta-Feira'
  else if QTabeladia_semana.AsInteger = 6 then
    QTabelasemana.value := 'Sexta-Feira';
end;

procedure TfrmCadLimiteFinan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
end;

procedure TfrmCadLimiteFinan.FormCreate(Sender: TObject);
begin
  QTabela.open;
end;

procedure TfrmCadLimiteFinan.btnAlterarClick(Sender: TObject);
begin
  if not dtmdados.PodeAlterar(name) then
    Exit;
  PageControl1.ActivePageIndex := 1;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := true;
end;

procedure TfrmCadLimiteFinan.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  Screen.Cursor := crDefault;
end;

end.
