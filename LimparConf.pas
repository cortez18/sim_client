unit LimparConf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ADODB, Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TfrmLimparConf = class(TForm)
    QCtrc: TADOQuery;
    btnAlterar: TBitBtn;
    Label1: TLabel;
    edNF: TJvCalcEdit;
    Label5: TLabel;
    edTarefa: TJvCalcEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAlterarClick(Sender: TObject);
    procedure edTarefaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLimparConf: TfrmLimparConf;

implementation

uses Dados, Menu;

{$R *.dfm}

procedure TfrmLimparConf.btnAlterarClick(Sender: TObject);
var
  ms: PChar;
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if (edNF.value > 0) and (edTarefa.value = 0) then
  begin
    ShowMessage('N�o pode limpar uma NF sem escolher a OCS');
    Exit;
  end;

  if (edNF.value = 0) and (edTarefa.value > 0) then
  begin
    if Application.Messagebox('Vai Limpar Todos Volumes desta OCS ?',
      'Limpar Confer�ncia', MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      dtmDados.Query1.close;
      dtmDados.Query1.sql.clear;
      dtmDados.Query1.sql.add
        ('update cyber.itc_man_emb set dat_conferencia = null, ');
      dtmDados.Query1.sql.add
        ('cod_usu_conferencia = null where cod_ocs = :0 and cod_fil = :1 ');
      dtmDados.Query1.Parameters[0].value := edTarefa.value;
      dtmDados.Query1.Parameters[1].value := glbfilial;
      dtmDados.Query1.ExecSQL;
      dtmDados.Query1.close;
      ShowMessage('Efetuado !');
      edNF.value := 0;
      edTarefa.value := 0;
      btnAlterar.Enabled := false;
      Exit;
    end;
  end;

  if Application.Messagebox('Voc� Deseja Limpar a Confer�ncia desta NF ?',
    'Limpar Confer�ncia', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.Query1.close;
    dtmDados.Query1.sql.clear;
    dtmDados.Query1.sql.add
      ('update cyber.itc_man_emb set dat_conferencia = null, cod_usu_conferencia = null ');
    dtmDados.Query1.sql.add
      ('where cod_ocs = :0 and nr_nf = :1 and cod_fil = :2');
    dtmDados.Query1.Parameters[0].value := edTarefa.value;
    dtmDados.Query1.Parameters[1].value := edNF.value;
    dtmDados.Query1.Parameters[2].value := glbfilial;
    dtmDados.Query1.ExecSQL;
    dtmDados.Query1.close;
    ShowMessage('Efetuado !');
    edNF.value := 0;
    btnAlterar.Enabled := false;
  end;
end;

procedure TfrmLimparConf.edTarefaExit(Sender: TObject);
begin
  if edTarefa.value > 1 then
  begin
    QCtrc.close;
    QCtrc.sql.clear;
    QCtrc.sql.add('select * ');
    QCtrc.sql.add('from cyber.rodocs ');
    QCtrc.sql.add('where codocs = ' + #39 + edTarefa.text + #39);
    QCtrc.sql.add(' and filocs = ' + #39 + IntToStr(glbfilial) + #39);
    QCtrc.open;
    if QCtrc.IsEmpty then
    begin
      ShowMessage('OCS N�o Encontrada !!');
      edTarefa.value := 0;
      Exit;
    end
    else
    begin
      QCtrc.close;
      QCtrc.sql.clear;
      QCtrc.sql.add('select * ');
      QCtrc.sql.add('from cyber.rodioc ');
      QCtrc.sql.add('where codocs = ' + #39 + edTarefa.text + #39);
      QCtrc.sql.add(' and notis = ' + #39 + edNF.text + #39);
      QCtrc.sql.add(' and filocs = ' + #39 + IntToStr(glbfilial) + #39);
      QCtrc.open;
      if QCtrc.IsEmpty then
      begin
        ShowMessage('NF N�o Encontrada !!');
        edNF.value := 0;
        Exit;
      end
    end;
    btnAlterar.Enabled := true;
  end;
end;

procedure TfrmLimparConf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
end;

end.
