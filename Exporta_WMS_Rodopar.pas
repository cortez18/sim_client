unit Exporta_WMS_Rodopar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, Variants, Grids, DBGrids,
  inifiles, Mask, ADODB, JvExControls,
  JvGradient;

type
  TFrmExporta_WMS_Rodopar = class(TForm)
    Label1: TLabel;
    edPedido: TEdit;
    SP: TADOStoredProc;
    btnCancelar: TBitBtn;
    Label5: TLabel;
    cbCliente: TComboBox;
    QCliente: TADOQuery;
    QClienteID_KLIENT: TStringField;
    QConsultStat: TADOQuery;
    QConsultStatSTAT: TStringField;
    RadioGroup1: TRadioGroup;
    QClienteW: TADOQuery;
    SPW: TADOStoredProc;
    QConsultStatW: TADOQuery;
    QConsultStatWSTAT: TStringField;
    QClienteWID_KLIENT: TStringField;
    QclienteV11: TADOQuery;
    SPv11: TADOStoredProc;
    QConsultStatV11: TADOQuery;
    QclienteV11ID_KLIENT: TStringField;
    QConsultStatV11STAT: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edPedidoExit(Sender: TObject);
    procedure edPedidoEnter(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure QClienteBeforeOpen(DataSet: TDataSet);
    procedure QClienteWBeforeOpen(DataSet: TDataSet);
    procedure QclienteV11BeforeOpen(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmExporta_WMS_Rodopar: TFrmExporta_WMS_Rodopar;

implementation

uses dados, menu, funcoes;

{$R *.DFM}

procedure TFrmExporta_WMS_Rodopar.FormShow(Sender: TObject);
begin
  cbCliente.SetFocus;
end;

procedure TFrmExporta_WMS_Rodopar.QClienteBeforeOpen(DataSet: TDataSet);
begin
  if GLBFilial = 1 then
    QCliente.Parameters[0].value := 'MAR'
  else if GLBFilial = 21 then
    QCliente.Parameters[0].value := 'MAR'
  else if GLBFilial = 4 then
    QCliente.Parameters[0].value := 'PBA';
end;

procedure TFrmExporta_WMS_Rodopar.QclienteV11BeforeOpen(DataSet: TDataSet);
begin
  if GLBFilial = 1 then
    QClienteV11.sql[3] := 'and p.lager in (''BRR'')'
  else if GLBFilial = 16 then
    QClienteV11.sql[3] := 'and p.lager in (''MAR'')'
  else if GLBFilial = 20 then
    QClienteV11.sql[3] := 'and p.lager in (''MAR'')'
  else if GLBFilial = 4 then
    QClienteV11.sql[3] := 'and p.lager in (''PBA'')'
  else if GLBFilial = 12 then
    QClienteV11.sql[3] := 'and p.lager in (''PBA'')'
  else if GLBFilial = 21 then
    QClienteV11.sql[3] := 'and p.lager in (''ETM'')'
  else if GLBFilial = 25 then
    QClienteV11.sql[3] := 'and p.lager in (''PBA'')';
end;

procedure TFrmExporta_WMS_Rodopar.QClienteWBeforeOpen(DataSet: TDataSet);
begin
  if GLBFilial = 1 then
    QClienteW.sql[3] := 'and p.lager in (''MAR'',''BRR'')'
  else if GLBFilial = 16 then
    QClienteW.sql[3] := 'and p.lager in (''MAR'')'
  else if GLBFilial = 20 then
    QClienteW.sql[3] := 'and p.lager in (''MAR'')'
  else if GLBFilial = 4 then
    QClienteW.sql[3] := 'and p.lager in (''PBA'')'
  else if GLBFilial = 12 then
    QClienteW.sql[3] := 'and p.lager in (''PBA'')'
  else if GLBFilial = 21 then
    QClienteW.sql[3] := 'and p.lager in (''ETM'')'
  else if GLBFilial = 24 then
    QClienteW.sql[3] := 'and p.lager in (''MAR'')';
end;

procedure TFrmExporta_WMS_Rodopar.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex = 0 then
  begin
    QCliente.Close;
    QCliente.open;
    cbCliente.Clear;
    cbCliente.Items.add('');
    while not QCliente.eof do
    begin
      cbCliente.Items.add(QClienteID_KLIENT.value);
      QCliente.Next;
    end;
  end
  else if RadioGroup1.ItemIndex = 1 then
  begin
    QClienteW.close;
    QClienteW.open;
    cbCliente.Clear;
    cbCliente.Items.add('');
    while not QClienteW.eof do
    begin
      cbCliente.Items.add(QClienteWID_KLIENT.value);
      QClienteW.Next;
    end;
  end
  else
  begin
    QclienteV11.close;
    QclienteV11.open;
    cbCliente.Clear;
    cbCliente.Items.add('');
    while not QClienteV11.eof do
    begin
      cbCliente.Items.add(QClienteV11ID_KLIENT.value);
      QClienteV11.Next;
    end;
  end;

end;

procedure TFrmExporta_WMS_Rodopar.FormCreate(Sender: TObject);
begin
  Self.Caption := 'Exportar Pedido WMS -> Rodopar';
end;

procedure TFrmExporta_WMS_Rodopar.edPedidoEnter(Sender: TObject);
begin
  edPedido.Color := clYellow;
end;

procedure TFrmExporta_WMS_Rodopar.edPedidoExit(Sender: TObject);
begin
  if cbCliente.Text = '' then
  begin
    ShowMessage('Escolha o Cliente !!');
    cbCliente.SetFocus;
    exit;
  end;
  // Client
  if RadioGroup1.ItemIndex = 0 then
  begin
    if edPedido.Text <> '' then
    begin
      QConsultStat.Close;
      QConsultStat.Parameters[0].value := edPedido.Text;
      QConsultStat.Parameters[1].value := cbCliente.Text;
      QConsultStat.open;
      // Verifica se o pedido est� no Status 95
      if QConsultStatSTAT.AsString <> '95' then
      begin
        ShowMessage('O pedido ' + edPedido.Text +
          ' n�o est� no status 95 favor verificar.');
      end
      // Caso esteja no status 95 executa a rotina abaixo
      else
      begin
        if Alltrim(edPedido.Text) <> '' then
        begin
          edPedido.Color := clWhite;
          SP.Parameters[1].value := cbCliente.Text;
          SP.Parameters[0].value := edPedido.Text;
          SP.ExecProc;

          dtmdados.WQuery1.Close;
          dtmdados.WQuery1.sql.Clear;
          dtmdados.WQuery1.sql.add
            ('select codigo from cyber.wmsped where cliped = :0');
          dtmdados.WQuery1.Parameters[0].value := edPedido.Text;
          dtmdados.WQuery1.open;
          if dtmdados.WQuery1.eof then
            ShowMessage
              ('N�o exportado, verifique produtos no Rodopar ou se j� foi importado este Pedido !!')
          else
            ShowMessage('Exportado - Criado Pedido :' +
              dtmdados.WQuery1.FieldByName('codigo').AsString);
          edPedido.Clear;
          edPedido.SetFocus;
        end;
      end;
    end;
  end
  // WEB V9
  else if RadioGroup1.ItemIndex = 1 then
  begin
    if edPedido.Text <> '' then
    begin
      QConsultStatW.Close;
      QConsultStatW.Parameters[0].value := edPedido.Text;
      QConsultStatW.Parameters[1].value := cbCliente.Text;
      QConsultStatW.open;
      // Verifica se o pedido est� no Status 95
      if QConsultStatWSTAT.AsString <> '95' then
      begin
        ShowMessage('O pedido ' + edPedido.Text +
          ' n�o est� no status 95 favor verificar.');
      end
      // Caso esteja no status 95 executa a rotina abaixo
      else
      begin
        if Alltrim(edPedido.Text) <> '' then
        begin
          edPedido.Color := clWhite;
          SPW.Parameters[1].value := cbCliente.Text;
          SPW.Parameters[0].value := edPedido.Text;
         { if GLBFilial = 1 then
          begin
            if (cbCliente.Text = 'CLESS') or (cbCliente.Text = 'SNC') or (cbCliente.Text = 'SKF') then
              SPW.Parameters[2].value := 1
            else
              SPW.Parameters[2].value := 16;
          end
          else  }
          SPW.Parameters[2].value := GLBFilial;
          SPW.ExecProc;

          dtmdados.WQuery1.Close;
          dtmdados.WQuery1.sql.Clear;
          dtmdados.WQuery1.sql.add
            ('select codigo from cyber.wmsped where cliped = :0 and codfil = :1');
          dtmdados.WQuery1.Parameters[0].value := edPedido.Text;
          dtmdados.WQuery1.Parameters[1].value := GLBFilial;
          dtmdados.WQuery1.open;
          if dtmdados.WQuery1.eof then
            ShowMessage
              ('N�o exportado, verifique produtos no Rodopar ou se j� foi importado este Pedido !!')
          else
            ShowMessage('Exportado - Criado Pedido :' +
              dtmdados.WQuery1.FieldByName('codigo').AsString);
          edPedido.Clear;
          edPedido.SetFocus;
        end;
      end;
    end;
  end
  else
  // V11
  begin
    if edPedido.Text <> '' then
    begin
      QConsultStatV11.Close;
      QConsultStatV11.Parameters[0].value := edPedido.Text;
      QConsultStatV11.Parameters[1].value := cbCliente.Text;
      QConsultStatV11.open;
      // Verifica se o pedido est� no Status 95
      if QConsultStatV11STAT.AsString <> '95' then
      begin
        ShowMessage('O pedido ' + edPedido.Text +
          ' n�o est� no status 95 favor verificar.');
      end
      // Caso esteja no status 95 executa a rotina abaixo
      else
      begin
        if Alltrim(edPedido.Text) <> '' then
        begin
          edPedido.Color := clWhite;
          SPV11.Parameters[0].value := edPedido.Text;
          SPV11.Parameters[1].value := cbCliente.Text;
          SPV11.Parameters[2].value := GLBFilial;
          SPV11.ExecProc;

          dtmdados.WMSV11Query.Close;
          dtmdados.WMSV11Query.sql.Clear;
          dtmdados.WMSV11Query.sql.add
            ('select codigo from wmsped@cyberintecprd where cliped = :0 and codfil = :1');
          dtmdados.WMSV11Query.Parameters[0].value := edPedido.Text;
          dtmdados.WMSV11Query.Parameters[1].value := GLBFilial;
          dtmdados.WMSV11Query.open;
          if dtmdados.WMSV11Query.eof then
            ShowMessage
              ('N�o exportado, verifique produtos no Rodopar ou se j� foi importado este Pedido !!')
          else
            ShowMessage('Exportado - Criado Pedido :' +
              dtmdados.WMSV11Query.FieldByName('codigo').AsString);
          edPedido.Clear;
          edPedido.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TFrmExporta_WMS_Rodopar.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmExporta_WMS_Rodopar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  Close;
end;

end.
