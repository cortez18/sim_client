unit manutencao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvMaskEdit, ADODB, Grids, DBGrids, StdCtrls, Buttons,
  JvBaseEdits, Mask, JvExMask, JvToolEdit, JvExStdCtrls, JvCombobox, ExtCtrls;

type
  TfrmManutencao = class(TForm)
    dtsSolicitacao: TDataSource;
    Label12: TLabel;
    edtarefa: TJvCalcEdit;
    Label3: TLabel;
    edFilial: TJvCalcEdit;
    btnAlTa: TBitBtn;
    btnSaTa: TBitBtn;
    btnCancelar: TBitBtn;
    btnFechar: TBitBtn;
    QSol: TADOQuery;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    cbOcoEn: TJvComboBox;
    Qocorr: TADOQuery;
    Label4: TLabel;
    edChamado: TJvCalcEdit;
    Label5: TLabel;
    Panel1: TPanel;
    Label7: TLabel;
    JvCalcEdit1: TJvCalcEdit;
    QTabela: TADOQuery;
    DataSource2: TDataSource;
    QTabelaCOD_VENDA: TBCDField;
    QTabelaCOD_CLIENTE: TBCDField;
    QTabelaFL_LOCAL: TStringField;
    QTabelaDS_UF: TStringField;
    QTabelaFL_LOCAL_DES: TStringField;
    QTabelaDS_UF_DES: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_PEDAGIO: TBCDField;
    QTabelaVL_OUTROS: TBCDField;
    QTabelaVL_OUTROS_P: TFloatField;
    QTabelaVL_OUTROS_MINIMO: TBCDField;
    QTabelaVL_EXCEDENTE: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaVL_GRIS: TFloatField;
    QTabelaVL_GRIS_MINIMO: TFloatField;
    QTabelaVL_ENTREGA: TBCDField;
    QTabelaSEGURO_MINIMO: TFloatField;
    QTabelaFL_PESO: TStringField;
    QTabelaFL_IMPOSTO: TStringField;
    QTabelaMODAL: TStringField;
    QTabelaVEICULO: TStringField;
    QTabelaSITE: TBCDField;
    QTabelaUSUARIO: TStringField;
    QTabelaDT_CADASTRO: TDateTimeField;
    QTabelaCODMUN: TBCDField;
    QTabelaCODMUNO: TBCDField;
    QTabelaVL_TDE: TBCDField;
    QTabelaVL_TR: TFloatField;
    QTabelaVL_TDEP: TFloatField;
    QTabelaVL_TDEM: TBCDField;
    QTabelaFL_STATUS: TStringField;
    QTabelaOPERACAO: TStringField;
    QTabelaVL_TDEMAX: TBCDField;
    QTabelaOBS: TStringField;
    QTabelaREGIAO: TStringField;
    QTabelaVL_SEG_BALSA: TFloatField;
    QTabelaVL_REDEP_FLUVIAL: TFloatField;
    QTabelaVL_AGEND: TBCDField;
    QTabelaVL_PALLET: TBCDField;
    QTabelaVL_TAS: TBCDField;
    QTabelaVL_DESPACHO: TBCDField;
    QTabelaVL_ENTREGA_PORTO: TFloatField;
    QTabelaVL_ALFAND: TFloatField;
    QTabelaVL_CANHOTO: TBCDField;
    QTabelaVL_FLUVIAL: TBCDField;
    QTabelaVL_FLUVIAL_M: TBCDField;
    QTabelaDT_INICIAL: TDateTimeField;
    QTabelaDT_FINAL: TDateTimeField;
    QTabelaCONTROLER: TStringField;
    QTabelaDT_CONTROLER: TDateTimeField;
    QTabelaVL_DEVOLUCAO: TBCDField;
    QTabelaFL_RATEIO: TStringField;
    QTabelaVL_FLUVMIN: TBCDField;
    QTabelaVL_AJUDA: TBCDField;
    Label8: TLabel;
    JvCalcEdit2: TJvCalcEdit;
    DBGrid3: TDBGrid;
    btnTabela: TBitBtn;
    SPEmail: TADOStoredProc;
    Edit1: TEdit;
    QSolMANIFESTO: TBCDField;
    QSolSERIE: TStringField;
    QSolFILIAL: TBCDField;
    QSolMDFE_CHAVE: TStringField;
    QSolMDFE_ENCERRAMENTO: TStringField;
    QSolOPERACAO: TStringField;
    QocorrOPERACAO: TStringField;
    Panel2: TPanel;
    Label1: TLabel;
    Label6: TLabel;
    JvCalcEdit3: TJvCalcEdit;
    JvCalcEdit4: TJvCalcEdit;
    DBGrid2: TDBGrid;
    BitBtn1: TBitBtn;
    QOC: TADOQuery;
    DataSource1: TDataSource;
    QOCSEQUENCIA: TBCDField;
    QOCPAGADOR: TBCDField;
    QOCREMETENTE: TBCDField;
    QOCNRO_OCS: TBCDField;
    QOCUSER_CTE: TStringField;
    QOCNR_OC: TBCDField;
    Label9: TLabel;
    JvCalcEdit5: TJvCalcEdit;
    Label10: TLabel;
    JvCalcEdit6: TJvCalcEdit;
    Label11: TLabel;
    Label13: TLabel;
    JvCalcEdit7: TJvCalcEdit;
    JvCalcEdit8: TJvCalcEdit;
    DBGrid4: TDBGrid;
    QOST: TADOQuery;
    dtsOST: TDataSource;
    QOSTsituac: TStringField;
    Label14: TLabel;
    edStatus: TEdit;
    btnOST: TBitBtn;
    QOSTcodigo: TBCDField;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edManifesto: TJvCalcEdit;
    edFilialMan: TJvCalcEdit;
    edSerie: TEdit;
    BitBtn2: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botoes;
    procedure btnSaTaClick(Sender: TObject);
    procedure btnAlTaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure edFilialExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvCalcEdit1Change(Sender: TObject);
    procedure JvCalcEdit2Exit(Sender: TObject);
    procedure btnTabelaClick(Sender: TObject);
    procedure JvCalcEdit5Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnOSTClick(Sender: TObject);
    procedure edStatusExit(Sender: TObject);
    procedure JvCalcEdit8Exit(Sender: TObject);
    procedure edSerieExit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutencao: TfrmManutencao;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmManutencao.BitBtn1Click(Sender: TObject);
var
  men: string;
begin
  if JvCalcEdit4.value = 0 then
  begin
    ShowMessage('E o chamado ?');
    JvCalcEdit4.setfocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Alterar Este Registro ?', 'Alterar',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add('Update cyber.tb_coleta set nr_oc = :0 ');
    dtmdados.IQuery1.sql.add('Where sequencia = ' +
      QuotedStr(QOCSEQUENCIA.AsString));
    dtmdados.IQuery1.Parameters[0].value := JvCalcEdit6.value;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
    ShowMessage('Ordem de Coleta Alterada');

    QOC.close;
    JvCalcEdit3.value := 0;
    JvCalcEdit4.value := 0;
    JvCalcEdit5.value := 0;
    JvCalcEdit6.value := 0;
    BitBtn1.Enabled := false;
  end;
end;

procedure TfrmManutencao.botoes;
begin
  btnAlTa.Enabled := not btnAlTa.Enabled;
  btnSaTa.Enabled := not btnSaTa.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
end;

procedure TfrmManutencao.btnAlTaClick(Sender: TObject);
begin
  botoes;
  cbOcoEn.setfocus;
end;

procedure TfrmManutencao.btnCancelarClick(Sender: TObject);
begin
  botoes;
  QSol.close;
end;

procedure TfrmManutencao.btnFecharClick(Sender: TObject);
begin
  close;
end;

procedure TfrmManutencao.btnOSTClick(Sender: TObject);
begin
  if edStatus.text ='' then
  begin
    ShowMessage('Qual Status ?');
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Alterar O Status ?', 'Alterar',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.sql.clear;
    dtmdados.RQuery1.sql.add('Update rodord set situac = :0 ');
    dtmdados.RQuery1.sql.add('Where codigo = :1 and codfil = :2');
    dtmdados.RQuery1.Parameters[0].value := edStatus.Text;
    dtmdados.RQuery1.Parameters[1].value := JvCalcEdit7.value;
    dtmdados.RQuery1.Parameters[2].value := JvCalcEdit8.value;
    dtmdados.RQuery1.ExecSQL;
    dtmdados.RQuery1.close;

    ShowMessage('OST Alterada');
  end;
  edStatus.clear;
  JvCalcEdit8Exit(sender);
  btnOST.Enabled := false;
end;

procedure TfrmManutencao.btnSaTaClick(Sender: TObject);
begin
  if edChamado.value = 0 then
  begin
    ShowMessage('E o chamado ?');
    exit;
  end;

  if cbOcoEn.text = '' then
  begin
    ShowMessage('Qual Opera��o ?');
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Alterar Este Lan�amento ?', 'Alterar',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add('Update tb_manifesto set operacao = :0 ');
    dtmdados.IQuery1.sql.add
      ('Where manifesto = :1 and serie = :2 and filial = :3 ');
    dtmdados.IQuery1.Parameters[0].value := cbOcoEn.text;
    dtmdados.IQuery1.Parameters[1].value := QSolMANIFESTO.AsInteger;
    dtmdados.IQuery1.Parameters[2].value := QSolSERIE.AsString;
    dtmdados.IQuery1.Parameters[3].value := QSolFILIAL.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
    ShowMessage('Manifesto Alterado');
  end;
  QSol.close;
  botoes;
end;

procedure TfrmManutencao.btnTabelaClick(Sender: TObject);
var
  men: string;
begin
  if JvCalcEdit2.value = 0 then
  begin
    ShowMessage('E o chamado ?');
    JvCalcEdit2.setfocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Alterar Esta Tabela ?', 'Alterar',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.clear;
    dtmdados.IQuery1.sql.add
      ('Update tb_tabelavenda set dt_controler = null, controler = null ');
    dtmdados.IQuery1.sql.add('Where cod_venda = ' +
      QuotedStr(QTabelaCOD_VENDA.AsString));
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;

    men := '<html xmlns="http://www.w3.org/1999/xhtml"><head>';
    men := men +
      '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}';
    men := men +
      '.titulo2 {  FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}';
    men := men +
      '.titulo3 {  FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}';
    men := men +
      '.texto1 {  FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}';
    men := men +
      '.texto2 {  FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}';
    men := men +
      '.texto3 {  FONT: 08px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}';
    men := men + '</STYLE>';
    men := men +
      '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">';
    men := men + '<table border=0><title></title></head><body>';
    men := men + '<table border=1>';
    men := men + '<tr>';
    men := men +
      '<th colspan=4><FONT class=titulo1>Solicitado o Desbloqueio da Tabela : '
      + QTabelaCOD_VENDA.AsString + '</th></font>';
    men := men + '</tr><tr>';
    men := men + '<th colspan=4><FONT class=titulo1>Solicitado Pelo Chamado : '
      + JvCalcEdit2.text + '</th></font>';
    men := men + '</tr><tr>';
    men := men + '<th colspan=4><FONT class=titulo1>Executado Por : ' + GLBUSER
      + '</th></font>';
    men := men + '</tr></html>';

    SPEmail.Parameters[0].value := 'mensageiro@intecomlogistica.com.br';
    SPEmail.Parameters[1].value := 'suporte@intecomlogistica.com.br';
    SPEmail.Parameters[2].value := 'Desbloqueio da Tabela de Venda';
    SPEmail.Parameters[3].value := men;
    SPEmail.Parameters[4].value := '';
    SPEmail.Parameters[5].value := 'mensageiro@intecomlogistica.com.br';
    SPEmail.Parameters[6].value := 'M3n$@geir0=123';
    SPEmail.ExecProc;

    SPEmail.Parameters[0].value := 'mensageiro@intecomlogistica.com.br';
    SPEmail.Parameters[1].value := 'controladoria@intecomlogistica.com.br';
    SPEmail.Parameters[2].value := 'Desbloqueio da Tabela de Venda';
    SPEmail.Parameters[3].value := men;
    SPEmail.Parameters[4].value := '';
    SPEmail.Parameters[5].value := 'mensageiro@intecomlogistica.com.br';
    SPEmail.Parameters[6].value := 'M3n$@geir0=123';
    SPEmail.ExecProc;

    QTabela.close;
    JvCalcEdit2.value := 0;
    JvCalcEdit1.value := 0;
    btnTabela.Enabled := false;
  end;
end;

procedure TfrmManutencao.edFilialExit(Sender: TObject);
begin
  QSol.close;
  QSol.Parameters[0].value := edtarefa.value;
  QSol.Parameters[1].value := Edit1.text;
  QSol.Parameters[2].value := edFilial.value;
  QSol.Open;
  if QSol.eof then
  begin
    ShowMessage('Manifesto N�o Encontrado !!');
    QSol.close;
    exit;
  end;
  botoes;
end;

procedure TfrmManutencao.edSerieExit(Sender: TObject);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.sql.add('select * from tb_manifesto ');
  dtmdados.IQuery1.sql.add
    ('Where manifesto = :0 and serie = :1 and filial = :2 and operacao = ''TRANSFERENCIA FILIAL'' ');
  dtmdados.IQuery1.Parameters[0].value := edMANIFESTO.AsInteger;
  dtmdados.IQuery1.Parameters[1].value := edSERIE.text;
  dtmdados.IQuery1.Parameters[2].value := edFilialMan.AsInteger;
  dtmdados.IQuery1.open;
  if dtmdados.IQuery1.eof then
  begin
    ShowMessage('Manifesto n�o � de transfer�ncia de filial');
    exit;
  end;

  if Application.Messagebox('Liberar os documentos  ?', 'Liberar documentos',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    frmMenu.SP_Libera.Parameters[0].value := edMANIFESTO.AsInteger;
    frmMenu.SP_Libera.Parameters[1].value := 1;
    frmMenu.SP_Libera.Parameters[2].value := edFilialMan.AsInteger;
    frmMenu.SP_Libera.ExecProc;
    ShowMessage('Documentos Liberados para Filial');
  end;
  QSol.close;
  botoes;
end;

procedure TfrmManutencao.edStatusExit(Sender: TObject);
begin
  btnOST.Enabled := true;
end;

procedure TfrmManutencao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QSol.close;
  Qocorr.close;
  QOST.Close;
end;

procedure TfrmManutencao.FormCreate(Sender: TObject);
begin
  Qocorr.close;
  Qocorr.Open;
  cbOcoEn.clear;
  cbOcoEn.Items.add('');
  while not Qocorr.eof do
  begin
    cbOcoEn.Items.add(QocorrOPERACAO.AsString);
    Qocorr.Next;
  end;
end;

procedure TfrmManutencao.JvCalcEdit1Change(Sender: TObject);
begin
  QTabela.close;
  QTabela.Parameters[0].value := JvCalcEdit1.value;
  QTabela.Open;
end;

procedure TfrmManutencao.JvCalcEdit2Exit(Sender: TObject);
begin
  if not QTabela.eof then
    btnTabela.Enabled := true
  else
    btnTabela.Enabled := false;
end;

procedure TfrmManutencao.JvCalcEdit5Exit(Sender: TObject);
begin
  QOC.close;
  QOC.Parameters[0].value := JvCalcEdit3.value;
  QOC.Parameters[1].value := JvCalcEdit5.value;
  QOC.Open;
  if not QOC.eof then
    BitBtn1.Enabled := true
  else
    BitBtn1.Enabled := false;
end;

procedure TfrmManutencao.JvCalcEdit8Exit(Sender: TObject);
begin
  QOST.close;
  QOST.Parameters[0].value := JvCalcEdit7.value;
  QOST.Parameters[1].value := JvCalcEdit8.value;
  QOST.Open;
  if QOST.eof then
  begin
    ShowMessage('Manifesto N�o Encontrado !!');
    QOST.close;
    exit;
  end;
end;

end.
