unit CheckList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, RLReport;

type
  TfrmChecklist = class(TForm)
    RLReport1: TRLReport;
    RLMemo1: TRLMemo;
    RLMemo2: TRLMemo;
    RLMemo3: TRLMemo;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLMemo4: TRLMemo;
    RLMemo5: TRLMemo;
    RLMemo6: TRLMemo;
    RLMemo7: TRLMemo;
    RLDraw2: TRLDraw;
    RLLabel9: TRLLabel;
    RLMemo8: TRLMemo;
    RLMemo9: TRLMemo;
    RLMemo10: TRLMemo;
    RLDraw3: TRLDraw;
    RLLabel10: TRLLabel;
    RLMemo11: TRLMemo;
    RLMemo12: TRLMemo;
    RLMemo13: TRLMemo;
    RLDraw4: TRLDraw;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLDraw5: TRLDraw;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel18: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmChecklist: TfrmChecklist;

implementation

uses ConsManifesto;

{$R *.dfm}

procedure TfrmChecklist.RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
begin
  RLMemo1.Lines.Add('Eu, ' + frmConsManifesto.QManifestoNM_FORNECEDOR.value + ' Matricula, motorista da empresa declaro que recebi nesta data, o(s) ve�culos identificados abaixo, para desempenhar as fun��es relativas ao meu cargo, nas condi��es constantes do Check List abaixo. ');
end;

end.
