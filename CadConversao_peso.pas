unit CadConversao_peso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ImgList, DB, ADODB, ActnList, Buttons, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, System.ImageList, System.Actions;

type
  TfrmCadConversao_peso = class(TForm)
    dtsOperacao: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QOperacao: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    edCNPJ: TEdit;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Label1: TLabel;
    QOperacaoREG: TBCDField;
    QOperacaoCNPJ: TStringField;
    QOperacaoDE: TStringField;
    QOperacaoPARA: TStringField;
    QOperacaoDT_INC: TDateTimeField;
    Label2: TLabel;
    edDe: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    cbPara: TComboBox;
    QOperacaoTransp: TStringField;
    QOperacaoTIPO: TStringField;
    QOperacaoUSER_INC: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure edCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure QOperacaoCalcFields(DataSet: TDataSet);
    procedure dbgContratoTitleClick(Column: TColumn);
  private
    quant: integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadConversao_peso: TfrmCadConversao_peso;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadConversao_peso.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadConversao_peso.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir('frmCadConversao') then
    Exit;
  Restaura;
  tipo := 'I';
  // EdCNPJ.text := '';
  edDe.Text := '';
  cbPara.ItemIndex := -1;
  edCNPJ.SetFocus;
end;

procedure TfrmCadConversao_peso.btnSalvarClick(Sender: TObject);
VAR
  COD: integer;
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.Query1.Close;
      dtmDados.Query1.sql.clear;
      dtmDados.Query1.sql.add
        ('select (nvl(max(reg),0)+1) as codigo from tb_conversao');
      dtmDados.Query1.open;
      COD := dtmDados.Query1.FieldByName('codigo').value;
      QOperacao.append;
      QOperacaoREG.value := COD;
      QOperacaoUSER_INC.AsString := GLBUSER;
    end
    else
    begin
      QOperacao.edit;
    end;
    QOperacaoCNPJ.value := edCNPJ.Text;
    QOperacaoDE.value := edDe.Text;
    QOperacaoPARA.value := cbPara.Text;
    QOperacaoTIPO.value := 'P';
    QOperacao.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QOperacao.Close;
  QOperacao.open;
end;

procedure TfrmCadConversao_peso.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QOperacao.Close;
  if Pos('order by', QOperacao.sql.Text) > 0 then
    QOperacao.sql.Text := Copy(QOperacao.sql.Text, 1,
      Pos('order by', QOperacao.sql.Text) - 1) + 'order by ' + Column.FieldName
  else
    QOperacao.sql.Text := QOperacao.sql.Text + ' order by ' + Column.FieldName;
  QOperacao.open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadConversao_peso.edCNPJKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0' .. '9', #8]) then
    Key := #0;
end;

procedure TfrmCadConversao_peso.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QOperacao.Cancel;
end;

procedure TfrmCadConversao_peso.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar('frmCadConversao') then
    Exit;
  Restaura;
  tipo := 'A';
  //
  edCNPJ.Text := QOperacaoCNPJ.value;
  edDe.Text := QOperacaoDE.value;
  cbPara.ItemIndex := cbPara.Items.IndexOf(QOperacaoPARA.value);
  edCNPJ.SetFocus;
end;

procedure TfrmCadConversao_peso.FormCreate(Sender: TObject);
begin
  QOperacao.open;
  quant := QOperacao.RecordCount;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadConversao_peso.QOperacaoCalcFields(DataSet: TDataSet);
begin
  dtmDados.Query1.Close;
  dtmDados.Query1.sql.clear;
  dtmDados.Query1.sql.add('Select nm_fornecedor from tb_fornecedor ');
  dtmDados.Query1.sql.add('where substr(nr_cnpj_cpf,1,8) = :0');
  dtmDados.Query1.Parameters[0].value := QOperacaoCNPJ.value;
  dtmDados.Query1.open;
  QOperacaoTransp.value := dtmDados.Query1.FieldByName('nm_fornecedor')
    .AsString;
  dtmDados.Query1.Close;
end;

procedure TfrmCadConversao_peso.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QOperacao.Close;
end;

procedure TfrmCadConversao_peso.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := True;
  end;
end;

procedure TfrmCadConversao_peso.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QOperacao.First;
    2:
      QOperacao.Prior;
    3:
      QOperacao.Next;
    4:
      QOperacao.Last;
  end;
  lblQuant.Caption := IntToStr(QOperacao.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
