unit AlteraCteEtiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ImgList, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, JvComponentBase,
  JvBalloonHint, ADODB, ActnList, StdCtrls, Buttons, JvExStdCtrls, JvRichEdit,
  JvDBRichEdit, ComCtrls, JvExComCtrls, JvComCtrls, JvBaseEdits, JvDBControls,
  JvMaskEdit, DBCtrls, JvCombobox, Mask, JvExMask, JvToolEdit, ExtCtrls, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvExControls, JvGradient, Inifiles, ComObj;

type
  TfrmAlteraCteEtiqueta = class(TForm)
    QCTE: TADOQuery;
    dtsCTe: TDataSource;
    btnNFi: TBitBtn;
    btnNFa: TBitBtn;
    btnNFe: TBitBtn;
    btnNFs: TBitBtn;
    QAlterar: TADOQuery;
    Panel3: TPanel;
    btnAlTa: TBitBtn;
    btnSaTa: TBitBtn;
    btnCancelar: TBitBtn;
    btnFechar: TBitBtn;
    Panel2: TPanel;
    Label14: TLabel;
    JvDBMaskEdit3: TJvDBMaskEdit;
    edCte: TJvCalcEdit;
    JvDBMaskEdit2: TJvDBMaskEdit;
    DBGNF: TJvDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    JvDBMaskEdit1: TJvDBMaskEdit;
    QCTEDS_OBS: TStringField;
    QCTEBOX: TStringField;
    QCTECUST_NAME: TStringField;
    QCTECUST_NUMBER: TStringField;
    QCTENR_CTE_ORIGEM: TFMTBCDField;
    Label4: TLabel;
    DBRichEdit1: TDBRichEdit;
    QCTEINVOICE: TStringField;
    btnEtiqueta: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botoes;
    procedure btnSaTaClick(Sender: TObject);
    procedure btnAlTaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure edCteExit(Sender: TObject);

  private
    { Private declarations }
  public
    arChave: array [1 .. 500] of String;
    sCodigoBarras: String;
    sChaveNfe: String;
  end;

var
  frmAlteraCteEtiqueta: TfrmAlteraCteEtiqueta;

implementation

uses Dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmAlteraCteEtiqueta.botoes;
begin
  btnAlTa.enabled := not btnAlTa.enabled;
  btnSaTa.enabled := not btnSaTa.enabled;
  btnCancelar.enabled := not btnCancelar.enabled;
end;

procedure TfrmAlteraCteEtiqueta.btnAlTaClick(Sender: TObject);
begin
  if QCte.RecordCount = 0 then
  begin
    ShowMessage('N�o existe Registro para ser alterado !!');
    exit;
  end;
  botoes;
end;

procedure TfrmAlteraCteEtiqueta.btnCancelarClick(Sender: TObject);
begin
  botoes;
  QAlterar.Cancel;
end;

procedure TfrmAlteraCteEtiqueta.btnFecharClick(Sender: TObject);
begin
  close;
end;

procedure TfrmAlteraCteEtiqueta.btnSaTaClick(Sender: TObject);
begin
  try
    if Application.Messagebox('Voc� Deseja Alterar o CT-e destes BOX ?', 'Alterar CT-e',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      QAlterar.Parameters[0].Value := QCTENR_CTE_ORIGEM.AsInteger;
      QAlterar.ExecSQL;
    end;
    QAlterar.close;
    QCte.Close;
    edCte.clear;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  botoes;
end;

procedure TfrmAlteraCteEtiqueta.edCteExit(Sender: TObject);
begin
  QCte.Close;
  QCte.Parameters[0].Value := edCte.Value;
  QCte.Open;
end;

procedure TfrmAlteraCteEtiqueta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QAlterar.close;
  QCTE.close;
end;

end.
