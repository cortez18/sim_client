unit Rel_Analise_custo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRel_Analise_custo = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    QCtrcNR_CONHECIMENTO: TBCDField;
    QCtrcDT_CTRC: TDateTimeField;
    QCtrcNR_SERIE: TStringField;
    QCtrcNM_CLI: TStringField;
    QCtrcCNPJ_CLI: TStringField;
    QCtrcNM_CLIENTE_REM: TStringField;
    QCtrcNR_CNPJ_CPF_REM: TStringField;
    QCtrcCIDADE_REM: TStringField;
    QCtrcUF_REM: TStringField;
    QCtrcNM_CLIENTE_DES: TStringField;
    QCtrcNR_CNPJ_CPF_DES: TStringField;
    QCtrcCIDADE_DES: TStringField;
    QCtrcUF_DES: TStringField;
    QCtrcNM_CLIENTE_RED: TStringField;
    QCtrcNR_CNPJ_CPF_RED: TStringField;
    QCtrcCIDADE_RED: TStringField;
    QCtrcUF_RED: TStringField;
    QCtrcNM_CLIENTE_EXP: TStringField;
    QCtrcNR_CNPJ_CPF_EXP: TStringField;
    QCtrcCIDADE_EXP: TStringField;
    QCtrcUF_EXP: TStringField;
    QCtrcVL_NF: TBCDField;
    QCtrcPESO_KG: TBCDField;
    QCtrcPESO_CUB: TBCDField;
    QCtrcQT_VOLUME: TBCDField;
    QCtrcNR_NOTA: TStringField;
    QCtrcCFOP: TStringField;
    QCtrcVL_FRETE: TBCDField;
    QCtrcVL_PEDAGIO: TBCDField;
    QCtrcVL_SEGURO: TBCDField;
    QCtrcVL_GRIS: TBCDField;
    QCtrcVL_OUTROS: TBCDField;
    QCtrcVL_TOTAL: TFloatField;
    QCtrcTAS: TBCDField;
    QCtrcVL_TX_CTE: TBCDField;
    QCtrcVL_DESPACHO: TBCDField;
    QCtrcVL_ENTREGA: TBCDField;
    QCtrcVL_TDE: TBCDField;
    QCtrcVL_TR: TBCDField;
    QCtrcTX_EXCEDENTE: TBCDField;
    QCtrcVL_SEG_BALSA: TBCDField;
    QCtrcVL_RED_FLUV: TBCDField;
    QCtrcVL_AGENDA: TBCDField;
    QCtrcVL_PALLET: TBCDField;
    QCtrcVL_PORTO: TBCDField;
    QCtrcVL_ALFAND: TBCDField;
    QCtrcVL_CANHOTO: TBCDField;
    QCtrcNF_VL_FRETE: TBCDField;
    QCtrcNF_VL_PEDAGIO: TBCDField;
    QCtrcNF_VL_SEGURO: TBCDField;
    QCtrcNF_VL_GRIS: TBCDField;
    QCtrcNF_TAS: TBCDField;
    QCtrcNF_VL_DESPACHO: TBCDField;
    QCtrcNF_VL_TDE: TBCDField;
    QCtrcNF_VL_TR: TBCDField;
    QCtrcNF_TX_EXCEDENTE: TBCDField;
    QCtrcNF_VL_SEG_BALSA: TBCDField;
    QCtrcNF_VL_RED_FLUV: TBCDField;
    QCtrcNF_VL_AGENDA: TBCDField;
    QCtrcNF_VL_PALLET: TBCDField;
    QCtrcNF_VL_PORTO: TBCDField;
    QCtrcNF_VL_ALFAND: TBCDField;
    QCtrcNF_VL_CANHOTO: TBCDField;
    QCtrcVL_ALIQUOTA: TFloatField;
    QCtrcVL_IMPOSTO: TFloatField;
    QCtrcFL_TIPO: TStringField;
    QCtrcNR_FATURA: TBCDField;
    QCtrcCNPJ_TRANSPORTADORA: TStringField;
    QCtrcIESTADUAL: TStringField;
    QCtrcOPERACAO: TStringField;
    QCtrcFATURA: TBCDField;
    QCtrcCTE_ID: TStringField;
    QCtrcOBSCON: TStringField;
    QCtrcNR_TABELA: TBCDField;
    QCtrcREGIAO: TStringField;
    QCtrcFL_STATUS: TStringField;
    QCtrcNR_MANIFESTO: TBCDField;
    QCtrcCTE_COMPLEMNETADO: TBCDField;
    QCtrcVL_AJUDANTE: TBCDField;
    QCtrcNF_VL_AJUDANTE: TBCDField;
    QCtrcDATNOT: TDateTimeField;
    QCtrcFL_EMPRESA: TBCDField;
    QCtrcCUSTO: TBCDField;
    Panel2: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    Panel3: TPanel;
    Gauge1: TGauge;
    JvDBNavigator1: TJvDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRel_Analise_custo: TfrmRel_Analise_custo;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_Analise_custo.btnExcelClick(Sender: TObject);
var // coluna, linha: integer;
  // excel: variant;
  // valor: string;
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  { try
    excel:=CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
    except
    Application.MessageBox ('Vers�o do Ms-Excel Incompat�vel','Erro',MB_OK+MB_ICONEXCLAMATION);
    end;
    QCtrc.First;
    Gauge1.MaxValue := QCtrc.RecordCount;
    try
    for linha:=0 to QCTRC.RecordCount-1 do
    begin
    for coluna:=1 to JvDBGrid1.Columns.Count do
    begin
    valor:= QCtrc.Fields[coluna-1].AsString;
    if QCtrc.Fields[coluna-1].DataType = ftDateTime then
    begin
    excel.cells [linha+2,coluna].NumberFormat := 'dd/mm/aaaa';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsDateTime;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftString then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '@';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsString;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftBCD then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsFloat;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftFloat then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsFloat;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftInteger then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsInteger;
    end
    else
    begin
    excel.cells [linha+2,coluna].NumberFormat := '@';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsString;
    end;
    end;
    Gauge1.AddProgress(1);
    QCtrc.Next;
    end;
    for coluna:=1 to JvDBGrid1.Columns.Count do
    begin
    valor:= JvDBGrid1.Columns[coluna-1].Title.Caption;
    excel.cells[1,coluna]:=valor;
    end;
    excel.columns.AutoFit;
    excel.visible:=true;
    except
    Application.MessageBox ('Aconteceu um erro desconhecido durante a convers�o'+
    'da tabela para o Ms-Excel','Erro',MB_OK+MB_ICONEXCLAMATION);
    end;
  }

  sarquivo := 'C:/SIM/Rel_Analise_custo' + ApCarac(DateToStr(date)) + '.html';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);

end;

procedure TfrmRel_Analise_custo.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  if Trim(ledIdCliente.LookupValue) <> '' then
    QCtrc.SQL[8] := 'and cnpj_cli = ''' + ledIdCliente.LookupValue + ''''
  else
    QCtrc.SQL[8] := ' ';
  QCtrc.SQL[9] := 'and dt_ctrc between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmRel_Analise_custo.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Analise_custo.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Analise_custo.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRel_Analise_custo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmRel_Analise_custo.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmRel_Analise_custo.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_Analise_custo.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
