unit EtiquetaVolume_Reimp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit,
  JvMemoryDataset, Vcl.ComCtrls;

type
  TfrmEtiquetaVolume_Reimp = class(TForm)
    QCTe: TADOQuery;
    dtsCTe: TDataSource;
    mdTemp: TJvMemoryData;
    mdTempCTe: TBCDField;
    mdTempescol: TIntegerField;
    mdTempTrip: TStringField;
    QVolume: TADOQuery;
    QVolumeRAZSOC: TStringField;
    QVolumeCIDADE: TStringField;
    QVolumeESTADO: TStringField;
    QVolumeDESTINO: TStringField;
    QCTeCTE: TFMTBCDField;
    QCTeID_TRIP: TStringField;
    QCTeBOX: TStringField;
    QCTeORDEM: TFMTBCDField;
    mdTempordem: TIntegerField;
    mdTempbox: TStringField;
    Label1: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    edCTe: TJvCalcEdit;
    edSerie: TEdit;
    edTrip: TEdit;
    Label3: TLabel;
    edVoli: TJvCalcEdit;
    Label4: TLabel;
    edVolf: TJvCalcEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    JvDBGrid1: TJvDBGrid;
    btRelatorio: TBitBtn;
    btnEtiqueta: TBitBtn;
    Gauge1: TGauge;
    QVol: TADOQuery;
    QVolBOX: TStringField;
    QVolDATA_BIP: TDateTimeField;
    QVolLINHA: TFMTBCDField;
    QCTeETIQUETA: TFMTBCDField;
    mdTempetiqueta: TIntegerField;
    mdTempnf: TStringField;
    QRegEtiq: TADOQuery;
    FMTBCDField1: TFMTBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    FMTBCDField2: TFMTBCDField;
    FMTBCDField3: TFMTBCDField;
    cbTodos: TCheckBox;
    QOrder: TADOQuery;
    QOrderORDCOM: TStringField;
    QVolumeORDER_CLI: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure btnEtiquetaClick(Sender: TObject);
    procedure QCTeBeforeOpen(DataSet: TDataSet);
    procedure mdTempCalcFields(DataSet: TDataSet);
    function RetornaCTeSelecionado(): String;
    procedure cbTodosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtiquetaVolume_Reimp: TfrmEtiquetaVolume_Reimp;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmEtiquetaVolume_Reimp.btnEtiquetaClick(Sender: TObject);
var Arq : TextFile;
    Ini: TIniFile;
    impr,razao,motivo, scte, nr_order : string;
    d,qt,i,seq, vol : integer;
begin
  motivo := InputBox('Qual o Motivo da Reimpress�o ? :',
      'Motivo do Reimpress�o', '');
  if motivo <> '' then
  begin
    mdTemp.First;
    Gauge1.MaxValue := mdTemp.RecordCount;

    razao := glbnmfilial;

    if glbimpr = '' then
      impr := 'C:/SIM/printetiqueta.txt'
    else
      impr := glbimpr;

    AssignFile(Arq,impr);
    ReWrite(Arq);
    while not mdTemp.eof do
    begin
      if mdTempescol.AsInteger = 1 then
      begin
        seq := 0;
        vol := 0;
        qt  := 0;
        // retorna a sequencia do volume na trip
        QVol.close;
        QVol.Parameters[0].Value := mdTempTrip.AsString;
        QVol.Open;
        QVol.Locate('box',mdTempBOX.AsString,[]);
        vol := QVol.RecordCount;
        QVol.close;
        // retorna os dados do destinat�rio
        QVolume.Close;
        QVolume.Parameters[0].Value := mdTempCTe.AsInteger;
        QVolume.Parameters[1].Value := GLBFilial;
        QVolume.Open;

        if QVolumeORDER_CLI.AsString = 'S' then
        begin
          QOrder.close;
          QOrder.Parameters[0].Value := mdTempCTe.AsInteger;
          QOrder.Parameters[1].Value := GLBFilial;
          QOrder.Open;
          nr_order := QOrderORDCOM.AsString;
          QOrder.close;
        end;


        // retorna total de volumes do cte
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.SQL.add('select box, ROW_NUMBER() OVER(ORDER BY data_bip) Linha from (select distinct box, data_bip ');
        dtmdados.IQuery1.SQL.add('from dashboard.skf_tb_trip tr where cte = :0 order by 2)' );
        dtmdados.IQuery1.Parameters[0].Value := mdTempCTe.AsInteger;
        //dtmdados.IQuery1.Parameters[1].Value := mdTempTrip.AsString;
        dtmdados.IQuery1.Open;
        qt  := dtmdados.IQuery1.RecordCount;
        dtmdados.IQuery1.Locate('box',mdTempbox.AsString,[]);
        seq := dtmdados.IQuery1.FieldByName('linha').AsInteger;
        dtmdados.IQuery1.close;
 {
        Writeln(Arq,''+'n');
        Writeln(Arq,''+'M0690');
        Writeln(Arq,''+'d');
        Writeln(Arq,''+'L');
        Writeln(Arq,'D11');
        Writeln(Arq,'R0000');
        Writeln(Arq,'ySW1');
        Writeln(Arq,'A2');
        Writeln(Arq,'1911S0102520015P009P009INTECOM');
        Writeln(Arq,'1911S0102520149P009P009'+DateTimeToStr(now)+' - '+ IntToStr(mdTempetiqueta.asinteger+1));
        Writeln(Arq,'1911S0102520299P009P009'+mdTempordem.AsString+'/'+IntToStr(vol));
        Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolumeDESTINO.AsString,1,30));
        Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolumeCIDADE.value,1,25)+'/'+copy(QVolumeESTADO.AsString,1,2));
        //Writeln(Arq,'1911S0101710015P009P009BOX : '+mdTempBOX.AsString);
        Writeln(Arq,'1911S0101670015P018P018BOX : '+mdTempBOX.AsString);
        Writeln(Arq,'1911S0101470089P009P009CT-e');
        Writeln(Arq,'1911S0101470291P009P009Volumes');
        Writeln(Arq,'1911S0101040015P030P030'+mdTempCTe.AsString);
        Writeln(Arq,'1911S0101060250P024P022'+IntToStr(seq)+' / '+ intToStr(qt));
        Writeln(Arq,'1X1100002470015L346001');
        Writeln(Arq,'1X1100001590015L346001');
        Writeln(Arq,'1X1100001940015L346001');
        Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolumeRAZSOC.AsString,1,30));
        Writeln(Arq,'1eE705100340012C'+mdTempBOX.AsString);
        Writeln(Arq,'1911S0100110019P008P008NF: '+ mdTempnf.AsString);
        Writeln(Arq,'1X1100000310016L346001');
        Writeln(Arq,'1911S0101590224P009P009'+ mdTempTrip.AsString);
        Writeln(Arq,'Q0001');
        Writeln(Arq,'E');
 }
        Writeln(Arq,''+'n');
        Writeln(Arq,''+'M0690');
        //Writeln(Arq,''+'KW0401');
        Writeln(Arq,''+'L');
        Writeln(Arq,'D11');
        Writeln(Arq,'R0000');
        Writeln(Arq,'ySW1');
        Writeln(Arq,'A2');
        Writeln(Arq,'1911S0102520015P009P009INTECOM');
        Writeln(Arq,'1911S0102520149P009P009'+DateTimeToStr(now)+IntToStr(mdTempetiqueta.asinteger+1));
        Writeln(Arq,'1911S0102520299P009P009'+mdTempordem.AsString+'/'+IntToStr(vol));
        Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolumeDESTINO.AsString,1,30));
        Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolumeCIDADE.value,1,25)+'/'+copy(QVolumeESTADO.AsString,1,2));
        Writeln(Arq,'1911S0101730015P014P014BOX : '+mdTempBOX.AsString);
        Writeln(Arq,'1911S0101280314P009P009Volumes');
        Writeln(Arq,'1911S0100910015P022P022'+mdTempCTe.AsString);
        Writeln(Arq,'1911S0100910316P020P018'+IntToStr(seq)+' / '+ intToStr(qt));
        Writeln(Arq,'1X1100002470015L346001');
        Writeln(Arq,'1X1100001420015L346001');
        Writeln(Arq,'1X1100001940015L346001');
        Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolumeRAZSOC.AsString,1,30));
        Writeln(Arq,'1eA504500350019C'+mdTempCTe.AsString + '-' + IntToStr(GLBFilial));
        Writeln(Arq,'1911S0100110019P008P008NF: '+ mdTempnf.AsString);
        Writeln(Arq,'1X1100000280020L346001');
        Writeln(Arq,'1911S0101470015P010P010ORDER:'+ nr_order);
        Writeln(Arq,'1911S0101280019P009P009CTE/BOX');
        Writeln(Arq,'1911S0101480219P009P009TRIP'+ mdTempTrip.AsString);
        Writeln(Arq,'Q0001');
        Writeln(Arq,'E');
      end;
      QVolume.close;
      Gauge1.AddProgress(1);
      mdTemp.Next;
    end;
    CloseFile(Arq);
    if impr = 'C:/SIM/printetiqueta.txt' then
      ShellExecute(Application.Handle, nil, PChar(impr),nil, nil, SW_SHOWNORMAL);

    // atualizar a impress�o de etiqueta
    scte := RetornaCTeSelecionado;
    if (sCte <> '') then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.SQL.add('update tb_conhecimento set etiqueta = etiqueta+1, ');
      dtmdados.IQuery1.SQL.add('motivo_reimpressao = '+QuotedStr(motivo)+', ');
      dtmdados.IQuery1.SQL.add('user_reimpressao = '+QuotedStr(GLBUSER) );
      dtmdados.IQuery1.SQL.add('where fl_empresa = 20 ');
      dtmdados.IQuery1.SQL.add('and nr_conhecimento in ('+ sCte + ')');
 // showmessage( dtmdados.IQuery1.SQL.text);
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;
    end;
  end;

end;

procedure TfrmEtiquetaVolume_Reimp.btRelatorioClick(Sender: TObject);
begin
  Gauge1.MaxValue := 0;
  {if (edtrip.Text <> '') and (edVoli.Value = 0) then
  begin
    ShowMessage('Quando escolhido por Trip deve-se escolher o Volume Inicial');
    edvoli.SetFocus;
  end;         }

  if (edVoli.Value > 0) and (edVolf.Value = 0) then
  begin
    ShowMessage('Qual o Volume Final');
    edvolf.SetFocus;
  end;
  // tinhamos alterado para pegar por ordem mas estava errado na reimpress�o
  // voltamos para ordem de data de recebimento
  QCte.Close;

  if edCTe.Value > 0 then
    QCte.SQL[3] := 'and tr.cte = '+ QuotedStr(ApCarac(edCte.text))
  else
    QCte.SQL[3] := '';

  if edTrip.Text <> '' then
    QCte.SQL[4] := 'and tr.id_trip = ' + QuotedStr(edTrip.text)
  else
    QCte.SQL[4] := '';

  if edVoli.Value > 0 then
  begin
    QCte.SQL[5] := 'and tr.ordem >= '+QuotedStr(ApCarac(edVoli.text));
    QCte.SQL[6] := 'and tr.ordem <= '+QuotedStr(ApCarac(edVolf.text));
  end;
  QCte.Open;
  mdTemp.Close;
  mdTemp.open;
  while not Qcte.Eof do
  begin
    mdTemp.Append;
    mdTempCTe.Value      := QCTeCTE.AsInteger;
    mdTempTrip.Value     := QCTeid_TRIP.AsString;
    mdTempBox.Value      := QCTeBOX.AsString;
    mdTempOrdem.Value    := QCTeORDEM.AsInteger;
    mdTempEtiqueta.Value := QCTeETIQUETA.AsInteger;
    mdTempescol.Value := 1;
    Qcte.Next;
  end;
  QCte.close;
  cbTodos.Checked := true;
  mdTemp.First;
end;

procedure TfrmEtiquetaVolume_Reimp.cbTodosClick(Sender: TObject);
begin
  mdTemp.first;
  while not mdTemp.eof do
  begin
    if cbTodos.Checked = false then
    begin
      if (mdTempescol.Value = 1) then
      begin
        mdTemp.Edit;
        mdTempescol.Value := 0;
        mdTemp.Post;
      end;
    end
    else
    begin
      if (mdTempescol.Value = 0) then
      begin
        mdTemp.Edit;
        mdTempescol.Value := 1;
        mdTemp.Post;
      end;
    end;
    mdTemp.next;
  end;
  mdTemp.first;
end;

procedure TfrmEtiquetaVolume_Reimp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  mdTemp.close;
end;

procedure TfrmEtiquetaVolume_Reimp.JvDBGrid1CellClick(Column: TColumn);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;


  if (Column.Field = mdTempescol) then
  begin
    mdTemp.edit;
    if (mdTempEscol.IsNull) or (mdTempEscol.value = 0) then
    begin
      mdTempEscol.value := 1;
    end
    else
    begin
      mdTempEscol.value := 0;
    end;
    mdTemp.post;
  end;

end;

procedure TfrmEtiquetaVolume_Reimp.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempEscol) then
    begin
      JvDBGrid1.Canvas.FillRect(Rect);
      dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempEscol.value = 1 then
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;

end;

procedure TfrmEtiquetaVolume_Reimp.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not mdTemp.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmEtiquetaVolume_Reimp.mdTempCalcFields(DataSet: TDataSet);
var d, t : Integer;
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select notfis from tb_cte_nf where codcon = :0 and fl_empresa = :1');
  dtmdados.IQuery1.Parameters[0].Value := mdTempCTe.AsInteger;
  dtmdados.IQuery1.Parameters[1].Value := GLBFilial;
  dtmdados.IQuery1.Open;
  d := 0;
  t := 0;
  While not dtmdados.IQuery1.eof do
  begin
    d := d + 1;
    t := t + Length(dtmdados.IQuery1.FieldByName('notfis').AsString+'-');
    if t > 55 then
    begin
      mdTempnf.Value := mdTempnf.Value + '...';
      exit;
    end;
    if d = 1 then
      mdTempnf.Value := dtmdados.IQuery1.FieldByName('notfis').AsString
    else
      mdTempnf.Value := mdTempnf.Value + '-'+ dtmdados.IQuery1.FieldByName('notfis').AsString;
    dtmdados.IQuery1.Next;
  end;
end;

procedure TfrmEtiquetaVolume_Reimp.QCTeBeforeOpen(DataSet: TDataSet);
begin
  QCte.Parameters[0].Value := glbfilial;
end;

function TfrmEtiquetaVolume_Reimp.RetornaCTeSelecionado: String;
var sCteSelec: String;
    iCount, posi: Integer;
begin
  //relaciona as CT-e da TRIP
  mdTemp.First;
  iCount := 0;
  mdTemp.DisableControls;
  while not mdTemp.eof do
  begin
    if mdTempescol.value = 1 then
    begin
      if iCount = 0 then
        sCteSelec := QuotedStr(mdTempCTe.AsString)
      else
      begin
        posi := Pos (mdTempCTe.AsString, sCteSelec );
        if posi = 0 then
          sCteSelec := sCteSelec + ',' + QuotedStr(mdTempCTe.AsString);
      end;
      inc(iCount);
    end;
    mdTemp.next;
  end;
  mdTemp.EnableControls;
  result := sCteSelec;
end;

end.
