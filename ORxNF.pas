unit ORxNF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Mask, JvExMask, JvToolEdit, ExtCtrls, DB,
  ADODB, Buttons, ComCtrls, ACBrNFe, SHDocVw, XMLIntf, OleCtrls, pcnAuxiliar,
  XMLDoc, ACBrUtil, pcnNFeRTXT, pcnConversao, JvMemoryDataset, IniFiles,
  ACBrBase, ACBrDFe;

type
  TfrmORxNF = class(TForm)
    DataSource1: TDataSource;
    QOS: TADOQuery;
    Edit1: TEdit;
    SPNF: TADOStoredProc;
    ACBrNFe1: TACBrNFe;
    OpenDialog1: TOpenDialog;
    TreeView: TTreeView;
    btnXML: TBitBtn;
    Label2: TLabel;
    QOSID_ARTIKEL: TStringField;
    QOSQUANT: TBCDField;
    DBGrid1: TDBGrid;
    MData: TJvMemoryData;
    MDataProduto: TStringField;
    MDataQuant: TIntegerField;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    MDatacheck: TStringField;
    DataSource2: TDataSource;
    procedure btnXMLClick(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure comparar;
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Edit1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmORxNF: TfrmORxNF;

implementation

uses Dados, funcoes, FileCtrl, pcnNFe, ACBrNFeNotasFiscais, DateUtils,
  ACBrDFeUtil;

{$R *.dfm}

procedure TfrmORxNF.btnXMLClick(Sender: TObject);
var
  i, n: Integer;
  Nota, NodeItem: TTreeNode;
  NFeRTXT: TNFeRTXT;
  Ini: TIniFile;
  nfe: string;
begin
  Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
  nfe := Ini.ReadString('RELATORIO', 'nfe', 'c:\');
  Ini.Free;
  OpenDialog1.FileName := '';
  OpenDialog1.Title := 'Selecione a NFE';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter :=
    'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Arquivos TXT (*.TXT)|*.TXT|Todos os Arquivos (*.*)|*.*';
  OpenDialog1.InitialDir := nfe;
  if OpenDialog1.Execute then
  begin
    ACBrNFe1.NotasFiscais.Clear;
    // tenta TXT
    ACBrNFe1.NotasFiscais.Add;
    NFeRTXT := TNFeRTXT.Create(ACBrNFe1.NotasFiscais.Items[0].nfe);
    NFeRTXT.CarregarArquivo(OpenDialog1.FileName);
    if NFeRTXT.LerTxt then
      NFeRTXT.Free
    else
    begin
      NFeRTXT.Free;
      // tenta XML
      ACBrNFe1.NotasFiscais.Clear;
      try
        ACBrNFe1.NotasFiscais.LoadFromFile(OpenDialog1.FileName);
      except
        ShowMessage('Arquivo NFe Inv�lido');
        exit;
      end;
    end;

    TreeView.Items.Clear;

    for n := 0 to ACBrNFe1.NotasFiscais.Count - 1 do
    begin
      with ACBrNFe1.NotasFiscais.Items[n].nfe do
      begin
        Nota := TreeView.Items.Add(nil, infNFe.ID);
        TreeView.Items.AddChild(Nota, 'ID= ' + infNFe.ID);
        // Node := TreeView.Items.AddChild(Nota,'procNFe');
        // Node := TreeView.Items.AddChild(Nota,'Ide');
        Label1.caption := 'NFe : ' + IntToStr(Ide.nNF);
        MData.Close;
        MData.open;
        for i := 0 to Det.Count - 1 do
        begin
          with Det.Items[i] do
          begin
            NodeItem := TreeView.Items.AddChild(Nota,
              'Produto' + IntToStrZero(i + 1, 3));
            TreeView.Items.AddChild(NodeItem, 'nItem=' + IntToStr(Prod.nItem));
            TreeView.Items.AddChild(NodeItem, 'cProd=' + Prod.cProd);
            if MData.Locate('produto', Prod.cProd, []) then
            begin
              MData.edit;
              MDataQuant.Value := MDataQuant.Value +
                StrToInt(FloatToStr(Prod.qCom));
            end
            else
            begin
              MData.Append;
              MDataProduto.Value := Prod.cProd;
              MDataQuant.Value := StrToInt(FloatToStr(Prod.qCom));
            end;
            MData.Post;
            TreeView.Items.AddChild(NodeItem, 'qCom=' + FloatToStr(Prod.qCom));
          end;
        end;
      end;
    end;
    comparar;
  end;
end;

procedure TfrmORxNF.comparar;
var
  prob: String;
begin
  QOS.First;
  MData.First;
  prob := '';
  while not QOS.eof do
  begin
    if MData.Locate('produto', QOSID_ARTIKEL.Value, []) then
    begin
      MData.edit;
      if MDataQuant.Value = QOSQUANT.Value then
        MDatacheck.Value := 'O'
      else
      begin
        MDatacheck.Value := 'X';
        prob := 'X';
      end;
      MData.Post;
    end
    else
      prob := 'X';
    QOS.Next;
  end;
  if prob = 'X' then
  begin
    Self.Color := clRed;
    Self.caption := 'N�o Liberado para Importa��o';
  end
  else
  begin
    Self.Color := clGreen;
    Self.caption := 'Liberado para Importa��o';
  end;
  Label1.Font.Color := clWhite;
  Label2.Font.Color := clWhite;
end;

procedure TfrmORxNF.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if MDatacheck.Value = 'O' then
  begin
    DBGrid2.canvas.Brush.Color := clGreen;
    DBGrid2.canvas.Font.Color := clWhite;
  end;
  if MDatacheck.Value = 'X' then
  begin
    DBGrid2.canvas.Brush.Color := clRed;
    DBGrid2.canvas.Font.Color := clWhite;
  end;
  DBGrid2.DefaultDrawDataCell(Rect, DBGrid2.columns[DataCol].field, State);
end;

procedure TfrmORxNF.Edit1Enter(Sender: TObject);
begin
  Self.Color := clBtnFace;
  Label1.Font.Color := clBlack;
  Label2.Font.Color := clBlack;
  TreeView.Items.Clear;
  Self.caption := '';
  QOS.Close;
  MData.Close;
end;

procedure TfrmORxNF.Edit1Exit(Sender: TObject);
begin
  if Edit1.text <> '' then
  begin
    QOS.Close;
    QOS.Parameters[0].Value := StrToInt(Edit1.text);
    QOS.open;
  end;
end;

end.
