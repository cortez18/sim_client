object frmMonitImp_Cte: TfrmMonitImp_Cte
  Left = 0
  Top = 0
  Caption = 'Monitoramento de Imprta'#231#227'o de XML Transportadoras'
  ClientHeight = 678
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 1184
    Height = 641
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 1
    object DBGrid1: TJvDBGrid
      Left = 1
      Top = 1
      Width = 1182
      Height = 304
      Align = alTop
      DataSource = dtsTarefas
      DrawingStyle = gdsClassic
      ParentShowHint = False
      PopupMenu = pmLegenda
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      OnTitleClick = DBGrid1TitleClick
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'datainc'
          Title.Alignment = taCenter
          Title.Caption = 'Data Imp.'
          Title.Color = clMoneyGreen
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FILIAL'
          Title.Caption = 'Filial'
          Title.Color = clMoneyGreen
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_TRANSP'
          Title.Caption = 'Transportadora'
          Title.Color = clNavy
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWhite
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 126
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NUMERO'
          Title.Caption = 'CT-e'
          Title.Color = clNavy
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWhite
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'chave'
          Title.Caption = 'Chave'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_REM'
          Title.Caption = 'Remetente'
          Title.Color = clNavy
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWhite
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 121
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NM_DES'
          Title.Caption = 'Destino'
          Title.Color = clMoneyGreen
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 122
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor Merc.'
          Title.Color = clNavy
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWhite
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODMAN'
          Title.Alignment = taCenter
          Title.Caption = 'Manifesto'
          Title.Color = clMoneyGreen
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'usuario'
          Title.Alignment = taCenter
          Title.Caption = 'Usu'#225'rio'
          Title.Color = clNavy
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWhite
          Title.Font.Height = -11
          Title.Font.Name = 'Tahoma'
          Title.Font.Style = []
          Width = 154
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'obs'
          Title.Caption = 'Observa'#231#227'o'
          Visible = False
        end>
    end
    object Panel4: TPanel
      Left = 1
      Top = 497
      Width = 1182
      Height = 143
      Align = alBottom
      TabOrder = 1
      object Label6: TLabel
        Left = 287
        Top = 13
        Width = 81
        Height = 13
        Caption = 'Total Valor Merc.'
      end
      object Label2: TLabel
        Left = 939
        Top = 6
        Width = 129
        Height = 13
        Caption = 'Observa'#231#227'o na Importa'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object JvDBGrid4: TJvDBGrid
        Left = 419
        Top = 1
        Width = 514
        Height = 137
        DataSource = dtsXml_nf
        DrawingStyle = gdsClassic
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NF'
            Title.Caption = 'N.F.'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIE'
            Title.Caption = 'S'#233'rie'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Title.Caption = 'Valor'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHAVE'
            Title.Caption = 'Chave Eletr'#244'nica'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 301
            Visible = True
          end>
      end
      object JvDBGrid5: TJvDBGrid
        Left = 1
        Top = 1
        Width = 268
        Height = 141
        Align = alLeft
        DataSource = dtsXML_carga
        DrawingStyle = gdsClassic
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'MEDIDA'
            Title.Caption = 'Medida'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QUANT'
            Title.Caption = 'Quant.'
            Title.Color = clNavy
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWhite
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object edValorMerT: TJvCalcEdit
        Left = 287
        Top = 32
        Width = 81
        Height = 21
        Color = clSilver
        DisplayFormat = '##,##0.00'
        ReadOnly = True
        ShowButton = False
        TabOrder = 2
        DisabledTextColor = clSilver
        DisabledColor = clWhite
        DecimalPlacesAlwaysShown = False
      end
      object DBMemo1: TDBMemo
        Left = 939
        Top = 20
        Width = 234
        Height = 118
        DataField = 'obs'
        DataSource = dtsTarefas
        TabOrder = 3
      end
    end
    object Panel2: TPanel
      Left = 207
      Top = 158
      Width = 341
      Height = 41
      Caption = 'Gerando..............'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      Visible = False
    end
    object Panel3: TPanel
      Left = 1
      Top = 305
      Width = 1182
      Height = 192
      Align = alClient
      TabOrder = 3
      object JvDBGrid2: TJvDBGrid
        Left = 7
        Top = 6
        Width = 457
        Height = 180
        DataSource = dtsOcs
        DrawingStyle = gdsClassic
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'SEQUENCIA'
            Title.Color = clMoneyGreen
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NRO_OCS'
            Title.Caption = 'OCS'
            Title.Color = clMoneyGreen
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAINC'
            Title.Caption = 'Data OCS'
            Title.Color = clMoneyGreen
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_COLETA'
            Title.Caption = 'Data Coleta'
            Title.Color = clMoneyGreen
            Width = 89
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCON'
            Title.Caption = 'CT-e'
            Title.Color = clMoneyGreen
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_ENTREGA'
            Title.Caption = 'Data Entrega'
            Title.Color = clMoneyGreen
            Width = 93
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FL_PAGO'
            Title.Color = clMoneyGreen
            Visible = False
          end>
      end
      object JvDBGrid3: TJvDBGrid
        Left = 550
        Top = 6
        Width = 423
        Height = 180
        DataSource = dtsNF
        DrawingStyle = gdsClassic
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NOTFIS'
            Title.Caption = 'N.F.'
            Title.Color = clMoneyGreen
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QUANTI'
            Title.Caption = 'Volume'
            Title.Color = clMoneyGreen
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PESOKG'
            Title.Caption = 'Peso KG'
            Title.Color = clMoneyGreen
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PESCUB'
            Title.Caption = 'Peso Cub.'
            Title.Color = clMoneyGreen
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VLRMER'
            Title.Caption = 'Valor'
            Title.Color = clMoneyGreen
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ORDCOM'
            Title.Caption = 'Pedido Cliente'
            Title.Color = clMoneyGreen
            Width = 73
            Visible = True
          end>
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 641
    Width = 1184
    Height = 37
    Align = alBottom
    TabOrder = 0
    object lblQuant: TLabel
      Left = 99
      Top = 6
      Width = 57
      Height = 25
      Hint = 'Quantidade de registros'
      Alignment = taCenter
      AutoSize = False
      ParentShowHint = False
      ShowHint = True
      Layout = tlCenter
    end
    object Label1: TLabel
      Left = 8
      Top = 137
      Width = 108
      Height = 13
      Caption = 'Ocorr'#234'ncia de Entrega'
    end
    object Label10: TLabel
      Left = 589
      Top = 2
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 686
      Top = 2
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 246
      Top = 2
      Width = 66
      Height = 13
      Caption = 'Transportador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BitBtn18: TBitBtn
      Tag = 4
      Left = 192
      Top = 6
      Width = 40
      Height = 25
      Action = acUltimoApanha
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6511800FF00FF00FF00FF00FF00FF00FF00FF00BD41
        1000BD381000B5381000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800D6864200C6591800FF00FF00FF00FF00FF00FF00BD49
        1000D6864A00DE965A00B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400D6864200C6591800FF00FF00FF00FF00C651
        1000D6865200DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00E7AE8400D6864200C6611800FF00FF00C659
        1800D68E5200DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7AE7B00E7B68C00D6864A00CE611800CE61
        1800DE965A00DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7AE7B00E7AE8400E7B68C00DE9E6300CE69
        1800DEA67300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00E7B68400E7B68C00EFB69400DE9E6B00CE71
        2900E7AE8400E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFBE9400EFBE9C00DE9E6B00D6793900D679
        3900E7A67300E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFC7A500E7A67B00DE8E4A00FF00FF00DE86
        4200E7AE7B00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFB68C00DE966300FF00FF00FF00FF00DE8E
        5200E7AE8400EFB69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFA67B00E7A67300FF00FF00FF00FF00FF00FF00DE96
        5A00E7B68C00EFBE9C00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6300DE966300DE965A00DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object BitBtn17: TBitBtn
      Tag = 3
      Left = 158
      Top = 6
      Width = 38
      Height = 25
      Action = acProximoApanha
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6491000B5411000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00C6591800CE713900BD411000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE611800DEA67300CE713900BD491000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE692100E7AE7B00E7A67B00D6713900BD511000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6713100E7AE8400DEA67300E7AE7B00D6793900C651
        1800B5411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200E7B69400E7A67300E7A67300E7AE8400D679
        4200C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200EFBE9C00E7AE8400E7AE8400E7B68C00D68E
        4A00C6611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7966300EFC7A500EFB69400EFBE9C00DE966300CE71
        2900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A66B00EFC7AD00EFC7A500E7A67300D6864200FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00E7A67B00EFCFAD00EFB68400DE966300FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00EFAE8400EFAE7B00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object BitBtn16: TBitBtn
      Tag = 2
      Left = 57
      Top = 6
      Width = 40
      Height = 25
      Action = acAnteriorApanha
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B530
        1000B5301000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B5381000C659
        3100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD491000CE693100DE96
        6300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00C6591800D6713900DE9E6B00DE9E
        6B00BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00CE712900CE692100D6864A00E7AE7B00DE9E6300DEA6
        7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00D6864200DE965A00E7B68C00E7A67300DE9E6300E7A6
        7B00C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DE8E5200E79E6B00EFBE9400E7AE8400DEA67300E7AE
        8400CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DE8E5200E7A67300EFBE9C00E7AE8400E7B6
        8C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00DE965A00E7A67300EFBE9C00EFBE
        9400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE966300E7A67B00EFBE
        9C00DE864A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DE9E6300E796
        6300DE965A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E79E
        6B00E79E6300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object BitBtn15: TBitBtn
      Tag = 1
      Left = 17
      Top = 6
      Width = 40
      Height = 25
      Action = acPrimeiroApanha
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE611800C6591800C6511800C6511000FF00FF00FF00FF00FF00
        FF00FF00FF00B5301000B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE691800E7AE7B00DE965A00C6591800FF00FF00FF00FF00FF00
        FF00B5381000C6613100B5381000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CE712900E7AE8400DE966300CE611800FF00FF00FF00FF00BD49
        1000CE693900DE966300BD411000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00D6793900E7B68C00DE9E6B00CE691800FF00FF00C6591800D671
        3900DE9E6B00DE9E6B00BD491000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE864200EFB69400E7A67300CE712900CE691800D6864200E7A6
        7B00DE966300DE9E7300C6511000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200EFBE9C00E7B68C00D6793900DE9E6B00E7AE8400DEA6
        7300DE966300DEA67300C6591800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DE965A00EFC79C00EFBE9C00D6864200E7A67300E7B68C00E7A6
        7B00DE9E6B00E7AE7B00CE611800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E79E6300EFC7A500EFB68C00DE8E5200D6864200DE966300E7B6
        9400E7AE7B00E7AE8400CE691800FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300EFCFAD00EFBE9400DE965A00FF00FF00D6864200DE9E
        6300E7B68C00E7B68C00CE712900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00E7A67300F7CFB500EFBE9C00E79E6300FF00FF00FF00FF00D686
        4A00DE9E6B00E7B69400D6793900FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400F7CFB500EFC7A500E7A67300FF00FF00FF00FF00FF00
        FF00DE8E4A00DE8E4A00DE864200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00EFAE8400EFAE8400EFAE7B00E7A67300FF00FF00FF00FF00FF00
        FF00FF00FF00DE8E5200DE8E5200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object dtInicial: TJvDateEdit
      Left = 589
      Top = 14
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 4
    end
    object dtFinal: TJvDateEdit
      Left = 686
      Top = 14
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 5
    end
    object btRelatorio: TBitBtn
      Left = 794
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 6
      OnClick = btRelatorioClick
    end
    object btnExcel: TBitBtn
      Left = 883
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Excel'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
        346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
        2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
        33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
        EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
        38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
        3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
        40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
        73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
        33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
        64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
        33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
        33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
        3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
        D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
        49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
        CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
        53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
        C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
        BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
        62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
        56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
        45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
        4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
        BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
        4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
        B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
        49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
        ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
        4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
        4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 7
      OnClick = btnExcelClick
    end
    object ledCliente: TJvDBLookupEdit
      Left = 246
      Top = 14
      Width = 256
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'nm_fornecedor'
      LookupField = 'cnpj'
      LookupSource = dtsFornecedor
      CharCase = ecUpperCase
      TabOrder = 8
      Text = ''
    end
  end
  object QImp: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterScroll = QImpAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'select distinct v.fl_empresa filial, emi.razsoc nm_transp, rem.r' +
        'azsoc nm_rem, des.razsoc nm_des, cte.valor, cte.id, cte.emitente' +
        ', cte.numero, m.manifesto codman, emi.codclifor cod_trans, cte.d' +
        'atainc, cte.usuario, cte.chave, cte.obs,v.nr_conhecimento'
      
        'from tb_cte_xml cte left join cyber.rodcli rem on cte.remetente ' +
        '= fnc_cnpj(rem.codcgc)'
      
        '                    left join cyber.rodcli des on cte.destinatar' +
        'io = fnc_cnpj(des.codcgc)'
      
        '                    left join cyber.rodcli emi on substr(cte.emi' +
        'tente,1,8) = substr(fnc_cnpj(emi.codcgc),1,8)'
      
        '                    inner join tb_custofrac tab on (tab.cod_forn' +
        'ecedor = emi.codclifor and fl_status = '#39'S'#39')'
      
        '                    inner join tb_cte_xml_nf nf on (cte.id = nf.' +
        'id and cte.emitente = nf.emitente)'
      
        '                    left join tb_cte_nf rnf on (rnf.notfis = nf.' +
        'nf and rnf.emissor = rem.codclifor)'
      
        '                    left join tb_conhecimento v on (v.nr_conheci' +
        'mento = rnf.codcon and v.fl_empresa = rnf.fl_empresa)'
      
        '                    left join tb_manitem ima on ima.coddoc = v.n' +
        'r_conhecimento and ima.fildoc = v.fl_empresa'
      
        '                    left join tb_manifesto m on m.manifesto = im' +
        'a.manifesto and m.filial = ima.filial and m.serie = ima.serie an' +
        'd m.transp = emi.codclifor'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 424
    Top = 264
    object QImpFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
    end
    object QImpNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 60
    end
    object QImpNM_REM: TStringField
      FieldName = 'NM_REM'
      ReadOnly = True
      Size = 30
    end
    object QImpNM_DES: TStringField
      FieldName = 'NM_DES'
      ReadOnly = True
      Size = 30
    end
    object QImpVALOR: TBCDField
      FieldName = 'VALOR'
      ReadOnly = True
      Precision = 14
      Size = 2
    end
    object QImpID: TBCDField
      FieldName = 'ID'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QImpEMITENTE: TStringField
      FieldName = 'EMITENTE'
      ReadOnly = True
      Size = 14
    end
    object QImpNUMERO: TBCDField
      FieldName = 'NUMERO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QImpCODMAN: TBCDField
      FieldName = 'CODMAN'
      ReadOnly = True
      Precision = 32
    end
    object QImpCOD_TRANS: TBCDField
      FieldName = 'COD_TRANS'
      ReadOnly = True
      Precision = 32
    end
    object QImpdatainc: TDateField
      FieldName = 'datainc'
    end
    object QImpusuario: TStringField
      FieldName = 'usuario'
      Size = 30
    end
    object QImpchave: TStringField
      FieldName = 'chave'
      Size = 44
    end
    object QImpobs: TStringField
      FieldName = 'obs'
      Size = 100
    end
  end
  object dtsTarefas: TDataSource
    DataSet = QImp
    Left = 632
    Top = 200
  end
  object act: TActionList
    Images = dtmDados.IMBotoes
    Left = 40
    Top = 442
    object acPrimeiroApanha: TAction
      Tag = 1
      Category = 'Apanha'
      Hint = 'Primeiro registro'
      ImageIndex = 3
      OnExecute = acPrimeiroApanhaExecute
    end
    object acAnteriorApanha: TAction
      Tag = 2
      Category = 'Apanha'
      Hint = 'Registro anterior'
      ImageIndex = 2
      OnExecute = acPrimeiroApanhaExecute
    end
    object acProximoApanha: TAction
      Tag = 3
      Category = 'Apanha'
      Hint = 'Pr'#243'ximo registro'
      ImageIndex = 0
      OnExecute = acPrimeiroApanhaExecute
    end
    object acUltimoApanha: TAction
      Tag = 4
      Category = 'Apanha'
      Hint = #218'ltimo registro'
      ImageIndex = 1
      OnExecute = acPrimeiroApanhaExecute
    end
  end
  object iml: TImageList
    Left = 132
    Top = 440
    Bitmap = {
      494C010102000400A40110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object pmLegenda: TPopupMenu
    Left = 84
    Top = 440
    object Legenda1: TMenuItem
      Caption = 'Legenda'
      ImageIndex = 96
    end
  end
  object QComp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct nome comp, ct.valor vl_tra, para, c.valor'
      
        'from tb_cte_xml_comp ct left join tb_conversao c on (c.cnpj = su' +
        'bstr(ct.emitente,1,8) and upper(ct.nome) = upper(c.de))'
      'where id = :0'
      'and emitente = :1'
      'order by 1')
    Left = 424
    Top = 200
    object QCompCOMP: TStringField
      FieldName = 'COMP'
      ReadOnly = True
    end
    object QCompVALOR: TFloatField
      FieldName = 'VALOR'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
    end
    object QCompPARA: TStringField
      FieldName = 'PARA'
      ReadOnly = True
      Size = 50
    end
    object QCompVL_TRA: TBCDField
      FieldName = 'VL_TRA'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
  end
  object dtsComp: TDataSource
    DataSet = QComp
    Left = 468
    Top = 200
  end
  object QCustoTMS: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select'
      ' nvl(t.vl_frete_peso_custo,t.vl_frete_peso_sec) TMS_frete_peso,'
      
        ' nvl(t.vl_frete_valor_custo, t.vl_frete_valor_sec) TMS_frete_val' +
        'or,'
      ' nvl(t.vl_pedagio_custo,t.vl_pedagio_sec) TMS_pedagio,'
      ' nvl(t.vl_gris_custo,t.vl_gris_sec) TMS_gris,'
      ' nvl(t.vl_outros_custo,t.vl_outros_sec) TMS_outros,'
      ' nvl(t.vl_imposto_custo,t.vl_IMPOSTO_sec) TMS_imposto   '
      ' from tb_tarefas t where cod_ta = :0')
    Left = 528
    Top = 200
    object QCustoTMSTMS_FRETE_PESO: TBCDField
      FieldName = 'TMS_FRETE_PESO'
      ReadOnly = True
      Precision = 32
    end
    object QCustoTMSTMS_FRETE_VALOR: TBCDField
      FieldName = 'TMS_FRETE_VALOR'
      ReadOnly = True
      Precision = 32
    end
    object QCustoTMSTMS_PEDAGIO: TBCDField
      FieldName = 'TMS_PEDAGIO'
      ReadOnly = True
      Precision = 32
    end
    object QCustoTMSTMS_GRIS: TBCDField
      FieldName = 'TMS_GRIS'
      ReadOnly = True
      Precision = 32
    end
    object QCustoTMSTMS_OUTROS: TBCDField
      FieldName = 'TMS_OUTROS'
      ReadOnly = True
      Precision = 32
    end
    object QCustoTMSTMS_IMPOSTO: TBCDField
      FieldName = 'TMS_IMPOSTO'
      ReadOnly = True
      Precision = 32
    end
  end
  object Qini: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'fl_empresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      ' select t04.codarquivo, t04.cgcemissorconhe'
      '    from tb_conemb04_cem t04'
      
        '    inner join tb_conemb04_cem_nf t04nf on (t04.codarquivo = t04' +
        'nf.codarquivo and'
      
        '                                         t04.codsequunh = t04nf.' +
        'codsequunh and'
      
        '                                         t04.codsequtra = t04nf.' +
        'codsequtra and'
      
        '                                         t04.codsequcem = t04nf.' +
        'codsequcem)'
      '    inner join tb_tarefas_notas tn on (t04nf.numnf = tn.nr_nf)'
      '    inner join tb_tarefas o on tn.cod_ta = o.cod_ta'
      '    where ok = '#39'X'#39' and o.fl_empresa = :fl_empresa'
      '    group by t04.codarquivo, t04.cgcemissorconhe'
      '    order by cgcemissorconhe')
    Left = 360
    Top = 300
    object QiniCODARQUIVO: TBCDField
      FieldName = 'CODARQUIVO'
      Precision = 32
      Size = 0
    end
    object QiniCGCEMISSORCONHE: TBCDField
      FieldName = 'CGCEMISSORCONHE'
      Precision = 14
      Size = 0
    end
  end
  object QOcs: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterOpen = QOcsAfterOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct oc.sequencia, oc.nro_ocs, oc.dt_coleta, oc.dt_en' +
        'trega, i.datainc, c.nr_conhecimento codcon, i.filial'
      
        'from vw_imp_cte i left join cyber.tb_coleta oc on oc.doc = i.cod' +
        'doc and oc.filial = i.filial'
      
        '                  left join tb_conhecimento c on c.nr_conhecimen' +
        'to = i.coddoc and c.fl_empresa = i.filial'
      'where id = :0'
      'and emitente = :1'
      'and c.fl_status = '#39'A'#39
      'order by c.nr_conhecimento'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 468
    Top = 264
    object QOcsSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      ReadOnly = True
      Precision = 32
    end
    object QOcsNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      ReadOnly = True
      Precision = 32
    end
    object QOcsDT_COLETA: TDateTimeField
      FieldName = 'DT_COLETA'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QOcsDT_ENTREGA: TDateTimeField
      FieldName = 'DT_ENTREGA'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QOcsDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      ReadOnly = True
    end
    object QOcsCODCON: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QOcsFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsOcs: TDataSource
    DataSet = QOcs
    Left = 524
    Top = 264
  end
  object QNF: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select notfis, nvl(quanti,0) quanti, nvl(pesokg,0) pesokg, nvl(p' +
        'escub,0) pescub, '
      'nvl(vlrmer,0) vlrmer, ordcom '
      'from tb_cte_nf x '
      'where x.codcon = :0'
      'and x.fl_empresa = :1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 468
    Top = 324
    object QNFNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QNFQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
    object QNFPESOKG: TBCDField
      FieldName = 'PESOKG'
      DisplayFormat = '#,##0.000'
      Precision = 14
    end
    object QNFPESCUB: TBCDField
      FieldName = 'PESCUB'
      DisplayFormat = '#,##0.000'
      Precision = 16
      Size = 6
    end
    object QNFVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
    object QNFORDCOM: TStringField
      FieldName = 'ORDCOM'
      Size = 15
    end
  end
  object dtsNF: TDataSource
    DataSet = QNF
    Left = 528
    Top = 320
  end
  object dtsXML_carga: TDataSource
    DataSet = QXml_Carga
    Left = 476
    Top = 380
  end
  object QXml_Carga: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct *'
      'from tb_cte_xml_carga'
      'where id = :0'
      'and emitente = :1'
      'order by 4')
    Left = 428
    Top = 380
    object QXml_CargaID: TBCDField
      FieldName = 'ID'
      Precision = 32
      Size = 0
    end
    object QXml_CargaEMITENTE: TStringField
      FieldName = 'EMITENTE'
      Size = 14
    end
    object QXml_CargaCARGA: TStringField
      FieldName = 'CARGA'
    end
    object QXml_CargaMEDIDA: TStringField
      FieldName = 'MEDIDA'
    end
    object QXml_CargaQUANT: TBCDField
      FieldName = 'QUANT'
      DisplayFormat = '###,##0.000'
      Precision = 15
      Size = 3
    end
  end
  object QXml_nf: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct * from tb_cte_xml_nf'
      'where id = :0'
      'and emitente = :1'
      'order by 3')
    Left = 428
    Top = 436
    object QXml_nfID: TBCDField
      FieldName = 'ID'
      Precision = 32
      Size = 0
    end
    object QXml_nfEMITENTE: TStringField
      FieldName = 'EMITENTE'
      Size = 14
    end
    object QXml_nfNF: TStringField
      FieldName = 'NF'
      Size = 10
    end
    object QXml_nfSERIE: TStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QXml_nfCHAVE: TStringField
      FieldName = 'CHAVE'
      Size = 44
    end
    object QXml_nfVALOR: TBCDField
      FieldName = 'VALOR'
      DisplayFormat = '###,##0.00'
      Precision = 15
      Size = 2
    end
  end
  object dtsXml_nf: TDataSource
    DataSet = QXml_nf
    Left = 476
    Top = 436
  end
  object QFornecedor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct substr(cte.emitente,1,8) cnpj,'
      
        '(select fma.razsoc from cyber.rodcli fma where substr(cte.emiten' +
        'te,1,8) = substr(fnc_cnpj(fma.codcgc),1,8) and rownum = 1) nm_fo' +
        'rnecedor'
      'from tb_cte_xml cte '
      'order by 2')
    Left = 425
    Top = 316
    object QFornecedorcnpj: TStringField
      FieldName = 'cnpj'
      Size = 8
    end
    object QFornecedornm_fornecedor: TStringField
      FieldName = 'nm_fornecedor'
      Size = 30
    end
  end
  object dtsFornecedor: TDataSource
    DataSet = QFornecedor
    Left = 357
    Top = 352
  end
end
