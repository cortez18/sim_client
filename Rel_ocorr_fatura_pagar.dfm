object frmRel_ocorr_fatura_pagar: TfrmRel_ocorr_fatura_pagar
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de Ocorr'#234'ncia de Faturas a Pagar '
  ClientHeight = 544
  ClientWidth = 863
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 12
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 159
    Top = 4
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 519
    Width = 533
    Height = 23
    Progress = 0
  end
  object Label3: TLabel
    Left = 306
    Top = 4
    Width = 36
    Height = 13
    Caption = 'Fatura :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object JvDBGrid2: TJvDBGrid
    Left = 0
    Top = 45
    Width = 737
    Height = 466
    DataSource = dtsOcorr
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 9
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'FATURA'
        Title.Caption = 'Fatura'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OCOR'
        Title.Caption = 'Ocorr'#234'ncia'
        Width = 209
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_INC'
        Title.Caption = 'Data'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USER_INC'
        Title.Caption = 'Usu'#225'rio'
        Width = 106
        Visible = True
      end>
  end
  object dtInicial: TJvDateEdit
    Left = 12
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 159
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 1
  end
  object btRelatorio: TBitBtn
    Left = 735
    Top = 425
    Width = 124
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btRelatorioClick
  end
  object Panel1: TPanel
    Left = 204
    Top = 175
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
  object btnExcel: TBitBtn
    Left = 737
    Top = 456
    Width = 124
    Height = 25
    Caption = 'Listagem'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF196B37196B37196B
      37196B37196B37FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF196B37288C5364BA8D95D2B264BA8D288C53196B37CD6E23C9651B
      C8601AC65918C25317C04E16BD4715BB4115B93E14206A3862BA8B60BA87FFFF
      FF60B98767BC8F1E6936D27735E4AF87E3AB81E1A87BDFA376DEA171DC9D6DDB
      9968DA9763317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B37D68443E7B590
      E0A374DE9E6EDC9A67DB9560D9915AD78D53D5894D48875C90D3B192D6B1FFFF
      FF65BC8C67BC8F1E6936DB8E53EABB99FCF6F2E1A679FCF5F1DD9D6BFCF6F2DA
      945EF7E9DE938E5D61AB8195D4B4BAE6D06ABB8F2D8F57605326E19762ECC1A1
      FCF7F3E5AD84FCF6F2E1A477FCF7F3DD9B69F8EBE0F0D3BEA8BAA05F956F4F8E
      66478458868654B73C13E2A06EEEC7A8FEFDFCFDF7F3FEFAF8E3AC81FCF7F4E0
      A374F9ECE3FAF2EBFDF8F4E3AE86FAF1EAD5894DDA9966BD4315E6A779F0CBB0
      FDF8F5EABA98FDF8F4E7B38CFDF8F5E3AA80F9EEE5EECDB4FDF8F5E5B38EFDF9
      F6D88F57DD9E6DC04E16EAAB80F2CFB5FCF4EEECBF9FFBF3EDFDF8F4FDF7F4FC
      F7F3F4DBC9E7B48EF7E6DAE3AD83F6E4D6DB9762DFA376C45918EAAB80F3D0B7
      EFC6A9EFC4A6EEC2A2ECBF9EEBBC98E9B893E8B48EE6B088E3AC81E2A77BE0A3
      74DE9E6EE2AA80C9621AEAAB80F3D0B7F3D0B7F3D0B7F2D0B7F1CEB3F0CBB0EF
      C9ACEEC6A8EDC2A3EBC09EEABB99E8B794E6B48FE4B089CD6E23EAAB80EAAB80
      EAAB80EAAB80EAAB80EAAB80E8A97CE6A477E2A070E29B6BE19762DD9059D98B
      52D88549D6803ED27735FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 4
    OnClick = btnExcelClick
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 735
    Top = 30
    Width = 124
    Height = 389
    TabOrder = 5
  end
  object DriveComboBox1: TDriveComboBox
    Left = 735
    Top = 9
    Width = 126
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 6
  end
  object Memo1: TMemo
    Left = 300
    Top = 244
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Visible = False
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 7
    Top = 518
    Width = 190
    Height = 25
    DataSource = dtsOcorr
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
    TabOrder = 10
  end
  object edFatura: TJvEdit
    Left = 306
    Top = 21
    Width = 83
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 8
    Text = ''
    OnEnter = edFaturaEnter
  end
  object QOcorrencia: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * '
      'from tb_ocor_fat_pagar'
      'where id > 0'
      ''
      ''
      ''
      ''
      'order by 5')
    Left = 224
    Top = 236
    object QOcorrenciaID: TBCDField
      FieldName = 'ID'
      Precision = 32
      Size = 0
    end
    object QOcorrenciaFATURA: TStringField
      FieldName = 'FATURA'
      Size = 10
    end
    object QOcorrenciaOCOR: TStringField
      DisplayWidth = 40
      FieldName = 'OCOR'
      Size = 40
    end
    object QOcorrenciaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object QOcorrenciaDT_INC: TDateTimeField
      FieldName = 'DT_INC'
    end
    object QOcorrenciaUSER_INC: TStringField
      FieldName = 'USER_INC'
      Size = 30
    end
  end
  object dtsOcorr: TDataSource
    DataSet = QOcorrencia
    Left = 272
    Top = 236
  end
end
