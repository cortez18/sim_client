unit CadFuncionario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, ADODB, ImgList, ActnList, DBCtrls,
  System.ImageList, System.Actions, JvExExtCtrls, JvRadioGroup, JvExMask,
  JvToolEdit, JvMaskEdit, Vcl.Mask, Vcl.Grids, Vcl.DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, Vcl.ComCtrls, JvCheckBox;

type
  TfrmCadFuncionario = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    DataSource1: TDataSource;
    Detalhes: TTabSheet;
    Label2: TLabel;
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    dtsTelas: TDataSource;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    DBGrid1: TJvDBUltimGrid;
    QFuncionario: TADOQuery;
    lblEnde: TLabel;
    DBEdit1: TDBEdit;
    btnAtualizar: TBitBtn;
    iml: TImageList;
    QTelas: TADOQuery;
    Query1: TADOQuery;
    TabSheet1: TTabSheet;
    JvMaskEdit1: TJvMaskEdit;
    edCodigo: TJvMaskEdit;
    JvMaskEdit6: TJvMaskEdit;
    edCPF: TJvMaskEdit;
    JvMaskEdit2: TJvMaskEdit;
    EdIE: TJvMaskEdit;
    JvMaskEdit8: TJvMaskEdit;
    EdNome: TJvMaskEdit;
    JvMaskEdit10: TJvMaskEdit;
    Eduser: TJvMaskEdit;
    JvMaskEdit20: TJvMaskEdit;
    edEnd: TJvMaskEdit;
    JvMaskEdit16: TJvMaskEdit;
    JvMaskEdit18: TJvMaskEdit;
    EdFone: TJvMaskEdit;
    JvMaskEdit26: TJvMaskEdit;
    EdFax: TJvMaskEdit;
    JvMaskEdit30: TJvMaskEdit;
    EdEmail: TJvMaskEdit;
    edBai: TJvMaskEdit;
    JvMaskEdit22: TJvMaskEdit;
    EdCid: TJvMaskEdit;
    JvMaskEdit24: TJvMaskEdit;
    EdUf: TJvMaskEdit;
    JvMaskEdit28: TJvMaskEdit;
    EdCep: TJvMaskEdit;
    JvMaskEdit3: TJvMaskEdit;
    JvMaskEdit4: TJvMaskEdit;
    edDpto: TJvMaskEdit;
    edAniv: TJvMaskEdit;
    QDpto: TADOQuery;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    QcadFunc: TADOQuery;
    RGNivel: TJvRadioGroup;
    btnLimparFiltro: TBitBtn;
    btnDuplicar: TBitBtn;
    QDuplicar: TADOQuery;
    QDptoCODIGO: TBCDField;
    QDptoDEPARTAMENTO: TStringField;
    QDptoSTATUS: TStringField;
    btnResetar: TBitBtn;
    Empresa: TTabSheet;
    QEmpresa: TADOQuery;
    Panel3: TPanel;
    RBAtivo: TRadioButton;
    RbInativo: TRadioButton;
    SPFuncionario: TADOStoredProc;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    JvDBUltimGrid1: TJvDBUltimGrid;
    QPesqTela: TADOQuery;
    QPesqTelaDESCRICAO: TStringField;
    QPesqTelaNAME: TStringField;
    btnPesq: TBitBtn;
    RGPesq: TJvRadioGroup;
    dtsPesq: TDataSource;
    QFuncTela: TADOQuery;
    dtsFuncTela: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    QFuncionarioCOD_USUARIO: TBCDField;
    QFuncionarioNM_USUARIO: TStringField;
    QFuncionarioLOGIN: TStringField;
    QFuncionarioSENHA: TStringField;
    QFuncionarioNR_CPF: TStringField;
    QFuncionarioDS_RG: TStringField;
    QFuncionarioDS_ENDERECO: TStringField;
    QFuncionarioDS_BAIRRO: TStringField;
    QFuncionarioDS_CIDADE: TStringField;
    QFuncionarioDS_CEP: TStringField;
    QFuncionarioDS_UF: TStringField;
    QFuncionarioNR_TELEFONE: TStringField;
    QFuncionarioDS_EMAIL: TStringField;
    QFuncionarioDT_CADASTRO: TDateTimeField;
    QFuncionarioFL_STATUS: TStringField;
    QFuncionarioFL_NIVEL: TStringField;
    QFuncionarioCOD_DPTO: TBCDField;
    QFuncionarioSITE: TStringField;
    QFuncionarioDPTO: TStringField;
    QTelasIDPERMISSAO: TBCDField;
    QTelasIDTELA: TBCDField;
    QTelasCODFUNC: TStringField;
    QTelasINSERIR: TBCDField;
    QTelasALTERAR: TBCDField;
    QTelasAPAGAR: TBCDField;
    QTelasVISUALIZAR: TBCDField;
    QTelasCOD_USUARIO: TBCDField;
    QTelasDESCRICAO: TStringField;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    QEmpresaCODFIL: TBCDField;
    QEmpresaNOMEAB: TStringField;
    QEmpresaATIVA: TStringField;
    Query1IDTELA: TBCDField;
    Query1DESCRICAO: TStringField;
    Query1NAME: TStringField;
    QcadFuncCOD_USUARIO: TBCDField;
    QcadFuncNM_USUARIO: TStringField;
    QcadFuncLOGIN: TStringField;
    QcadFuncSENHA: TStringField;
    QcadFuncNR_CPF: TStringField;
    QcadFuncDS_RG: TStringField;
    QcadFuncDS_ENDERECO: TStringField;
    QcadFuncDS_BAIRRO: TStringField;
    QcadFuncDS_CIDADE: TStringField;
    QcadFuncDS_CEP: TStringField;
    QcadFuncDS_UF: TStringField;
    QcadFuncNR_TELEFONE: TStringField;
    QcadFuncDS_EMAIL: TStringField;
    QcadFuncDT_CADASTRO: TDateTimeField;
    QcadFuncFL_STATUS: TStringField;
    QcadFuncFL_NIVEL: TStringField;
    QcadFuncCOD_DPTO: TBCDField;
    QcadFuncSITE: TStringField;
    QPesqTelaIDTELA: TBCDField;
    cbAtivo: TCheckBox;
    QDuplicarIDPERMISSAO: TBCDField;
    QDuplicarIDTELA: TBCDField;
    QDuplicarCODFUNC: TStringField;
    QDuplicarINSERIR: TBCDField;
    QDuplicarALTERAR: TBCDField;
    QDuplicarAPAGAR: TBCDField;
    QDuplicarVISUALIZAR: TBCDField;
    QDuplicarCOD_USUARIO: TBCDField;
    QFuncTelaIDPERMISSAO: TBCDField;
    QFuncTelaIDTELA: TBCDField;
    QFuncTelaCODFUNC: TStringField;
    QFuncTelaINSERIR: TBCDField;
    QFuncTelaALTERAR: TBCDField;
    QFuncTelaAPAGAR: TBCDField;
    QFuncTelaVISUALIZAR: TBCDField;
    QFuncTelaCOD_USUARIO: TBCDField;
    QFuncTelaNM_USUARIO: TStringField;
    QFuncTelaIDTELA_1: TBCDField;
    QFuncTelaDESCRICAO: TStringField;
    QFuncTelaNAME: TStringField;
    cbdpto: TComboBox;

    procedure btnFecharClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure QFuncionarioAfterScroll(DataSet: TDataSet);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure edAnivExit(Sender: TObject);
    procedure btnLimparFiltroClick(Sender: TObject);
    procedure cbdptoExit(Sender: TObject);
    procedure btnDuplicarClick(Sender: TObject);
    procedure btnResetarClick(Sender: TObject);
    procedure EduserExit(Sender: TObject);
    procedure RbInativoClick(Sender: TObject);
    procedure RBAtivoClick(Sender: TObject);
    procedure btnPesqClick(Sender: TObject);
    procedure JvDBUltimGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBUltimGrid1CellClick(Column: TColumn);
    procedure TabSheet2Hide(Sender: TObject);
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadFuncionario: TfrmCadFuncionario;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadFuncionario.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadFuncionario.btnLimparFiltroClick(Sender: TObject);
begin
  QFuncionario.Close;
  QFuncionario.sql[3] := 'Where cod_usuario > 0 ';
  QFuncionario.open;
end;

procedure TfrmCadFuncionario.btnNovoClick(Sender: TObject);
var
  x: Integer;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  tipo := 'I';
  PageControl1.ActivePageIndex := 2;
  edCodigo.clear;
  EdIE.clear;
  EdNome.clear;
  edDpto.clear;
  Eduser.clear;
  edEnd.clear;
  EdFone.clear;
  EdCid.clear;
  edBai.clear;
  EdUf.clear;
  EdFax.clear;
  EdCep.clear;
  EdEmail.clear;
  edAniv.clear;
  edCPF.clear;
  btnNovo.Enabled := false;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := true;
  cbAtivo.Checked := true;
  cbdpto.Visible := true;
  RGNivel.ItemIndex := 2;
  QDpto.open;
  cbdpto.clear;
  while not QDpto.eof do
  begin
    cbdpto.Items.Add(QDptoDEPARTAMENTO.value);
    QDpto.next;
  end;
  QDpto.Close;
end;

procedure TfrmCadFuncionario.btnPesqClick(Sender: TObject);
begin
  QFuncTela.Close;
  if RGPesq.ItemIndex = 0 then
    QFuncTela.sql[3] := 'And Inserir = 1'
  else if RGPesq.ItemIndex = 1 then
    QFuncTela.sql[3] := 'And Alterar = 1'
  else if RGPesq.ItemIndex = 2 then
    QFuncTela.sql[3] := 'And Apagar = 1'
  else if RGPesq.ItemIndex = 3 then
    QFuncTela.sql[3] := 'And Visualizar = 1'
  else
    QFuncTela.sql[3] :=
      'And Inserir = 1 and Alterar = 1 and Apagar = 1 and Visualizar = 1 ';
  QFuncTela.Parameters[0].value := QPesqTelaNAME.value;
  QFuncTela.open;
  showmessage(QFunctela.SQL.Text);
end;

procedure TfrmCadFuncionario.btnResetarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(Name) then
    Exit;

  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add
    ('update tb_usuarios set senha = (SELECT passwd_encrypt(:0,''MUDAR123'') new_senha ');
  dtmDados.IQuery1.sql.Add('FROM DUAL) where COD_USUARIO = :1 ');
  dtmDados.IQuery1.Parameters[0].value := QFuncionarioCOD_USUARIO.AsInteger;
  dtmDados.IQuery1.Parameters[1].value := QFuncionarioCOD_USUARIO.AsInteger;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.Close;

  ShowMessage('Senha Resetada com Sucesso, para MUDAR123 !!');

  //senha base64
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add
    ('update tb_usuarios set senha2 = :0 where COD_USUARIO = :1 ');
  dtmDados.IQuery1.Parameters[0].value := DecodeBase64(QFuncionarioLogin.Value + 'MUDAR123');
  dtmDados.IQuery1.Parameters[1].value := QFuncionarioCOD_USUARIO.AsInteger;
  dtmDados.IQuery1.ExecSQL;
  dtmDados.IQuery1.Close;

end;

procedure TfrmCadFuncionario.btnSalvarClick(Sender: TObject);
var
  rec, x: Integer;
  lista: string;
begin
  // empresa
  lista := '';
  for x := 0 to ComponentCount - 1 do
    if Components[x].ClassType = TJvCheckBox then
      if TJvCheckBox(Components[x]).parent = Empresa then
      begin
        if TJvCheckBox(Components[x]).Checked then
        begin
          if lista <> '' then
            lista := lista + ',' +
              Retzero(FloatToStr(sonumero(TJvCheckBox(Components[x]).name)),2)
          else
            lista := Retzero(FloatToStr(sonumero(TJvCheckBox(Components[x]).name)),2);
        end;
      end;

  QcadFunc.Close;
  QcadFunc.sql[2] := 'where c.cod_usuario = ' + #39 +
    QFuncionarioCOD_USUARIO.AsString + #39;
  QcadFunc.open;

  if lista = '' then
  begin
    ShowMessage('N�o foi escolhido qual empresa faz parte o Funcion�rio !!');
    PageControl1.ActivePageIndex := 3;
    Exit;
  end;

  if (EdNome.text = '') then
  begin
    ShowMessage('N�o foi digitado o nome do Funcion�rio !!');
    EdNome.setfocus;
    Exit;
  end;

  if (Eduser.text = '') then
  begin
    ShowMessage('N�o foi digitado o usu�rio!!');
    Eduser.setfocus;
    Exit;
  end;

  if (edDpto.text = '') then
  begin
    ShowMessage('N�o foi digitado o departamento !!');
    edDpto.setfocus;
    Exit;
  end;

  try
    SPFuncionario.Parameters[1].value := Eduser.text;
    SPFuncionario.Parameters[2].value := EdNome.text;
    SPFuncionario.Parameters[3].value := EdUf.text;
    SPFuncionario.Parameters[4].value := edCPF.text;
    SPFuncionario.Parameters[5].value := EdIE.text;
    SPFuncionario.Parameters[6].value := edEnd.text;
    SPFuncionario.Parameters[7].value := EdCid.text;
    SPFuncionario.Parameters[8].value := edBai.text;
    SPFuncionario.Parameters[9].value := EdCep.text;
    SPFuncionario.Parameters[10].value := EdFone.text;
    if cbAtivo.Checked THEN
      SPFuncionario.Parameters[11].value := 'A'
    else
      SPFuncionario.Parameters[11].value := 'I';
    SPFuncionario.Parameters[12].value := cbdpto.text;
    if RGNivel.ItemIndex = 0 then
      SPFuncionario.Parameters[13].value := 'A'
    else if RGNivel.ItemIndex = 1 then
      SPFuncionario.Parameters[13].value := 'S'
    else
      SPFuncionario.Parameters[13].value := 'U';
    if tipo = 'I' then
    begin
      SPFuncionario.Parameters[14].value := 'I';
      SPFuncionario.Parameters[15].value := 0;
    end
    else
    begin
      SPFuncionario.Parameters[14].value := 'A';
      SPFuncionario.Parameters[15].value := QFuncionarioCOD_USUARIO.AsInteger;
    end;
    SPFuncionario.Parameters[16].value := EdEmail.text;
    SPFuncionario.Parameters[17].value := lista;
    SPFuncionario.ExecProc;

    if tipo = 'I' then
      edCodigo.text := IntToStr(SPFuncionario.Parameters[0].value);

    cbdpto.Visible := false;
    btnNovo.Enabled := true;
    btnAlterar.Enabled := true;
    btnSalvar.Enabled := false;
    QFuncionario.Close;
    QFuncionario.open;
    QFuncionario.locate('cod_usuario', edCodigo.text, []);
    //senha base64
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.Add
      ('update tb_usuarios set senha2 = :0 where COD_USUARIO = :1 ');
    dtmDados.IQuery1.Parameters[0].value := DecodeBase64(QFuncionarioLogin.Value + QFuncionarioSENHA.Value);
    dtmDados.IQuery1.Parameters[1].value := QFuncionarioCOD_USUARIO.AsInteger;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;

    tipo := '';
    PageControl1.ActivePageIndex := 0;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
end;

procedure TfrmCadFuncionario.cbdptoExit(Sender: TObject);
begin
  edDpto.text := cbdpto.text;
end;

procedure TfrmCadFuncionario.dbgContratoTitleClick(Column: TColumn);
var
  icount: Integer;
begin
  QFuncionario.Close;
  if Pos('order by', QFuncionario.sql.text) > 0 then
  begin
    if Column.FieldName = 'nivel' then
      QFuncionario.sql.text := Copy(QFuncionario.sql.text, 1,
        Pos('order by', QFuncionario.sql.text) - 1) + 'order by fl_nivel'
    else
      QFuncionario.sql.text := Copy(QFuncionario.sql.text, 1,
        Pos('order by', QFuncionario.sql.text) - 1) + 'order by ' +
        Column.FieldName
  end
  else
    QFuncionario.sql.text := QFuncionario.sql.text + ' order by ' +
      Column.FieldName;
  QFuncionario.open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadFuncionario.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePage := Consulta;
end;

procedure TfrmCadFuncionario.FormCreate(Sender: TObject);
var
  lista: array of TJvCheckBox;
  I, Topo, L: Integer;
begin
  QFuncionario.open;
  QTelas.open;
  quant := QFuncionario.RecordCount;
  lblQuant.Caption := IntToStr(QFuncionario.RecNo) + #13#10 + IntToStr(quant);
  QEmpresa.open;
  SetLength(lista, QEmpresa.RecordCount);
  I := 0;
  Topo := 10;
  L := 20;
  while not QEmpresa.eof do
  begin
    lista[I] := TJvCheckBox.Create(self);
    with lista[I] do
    begin
      parent := Empresa;
      Width := 200;
      Caption := QEmpresaNOMEAB.AsString;
      Hint := QEmpresaCODFIL.AsString;
      left := L;
      Top := Topo;
      ShowHint := true;
      name := 'E' + QEmpresaCODFIL.AsString;
    end;
    QEmpresa.next;
    I := I + 1;
    if I = 13 then
    begin
      Topo := 10;
      L := L + 200;
      I := 0;
    end
    else
      Topo := Topo + 30;
  end;
  QEmpresa.Close;

  QPesqTela.open;

end;

procedure TfrmCadFuncionario.JvDBUltimGrid1CellClick(Column: TColumn);
var
  Inserir, Apagar, Editar, Visualizar: Integer;
begin
  if not dtmDados.PodeAlterar(Name) then
    Exit;
  // Registro   := QFuncTelaOIDPERMISSAO.AsInteger;
  Inserir := QFuncTelaINSERIR.AsInteger;
  Apagar := QFuncTelaAPAGAR.AsInteger;
  Editar := QFuncTelaALTERAR.AsInteger;
  Visualizar := QFuncTelaVISUALIZAR.AsInteger;
  if Column.Field = QFuncTelaINSERIR then
  begin
    if QFuncTelaINSERIR.AsInteger = 0 then
      Inserir := 1
    else
      Inserir := 0;
  end;
  if Column.Field = QFuncTelaAPAGAR then
  begin
    if QFuncTelaAPAGAR.AsInteger = 0 then
      Apagar := 1
    else
      Apagar := 0;
  end;
  if Column.Field = QFuncTelaALTERAR then
  begin
    if QFuncTelaALTERAR.AsInteger = 0 then
      Editar := 1
    else
      Editar := 0;
  end;
  if Column.Field = QFuncTelaVISUALIZAR then
  begin
    if QFuncTelaVISUALIZAR.AsInteger = 0 then
      Visualizar := 1
    else
      Visualizar := 0;
  end;
  if (Column.Field = QFuncTelaINSERIR) or (Column.Field = QFuncTelaAPAGAR) or
    (Column.Field = QFuncTelaALTERAR) or (Column.Field = QFuncTelaVISUALIZAR)
  then
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.Add('call prc_altera_perm(:1,:2,:3,:4,:5)');
    dtmDados.IQuery1.Parameters[0].value := Inserir;
    dtmDados.IQuery1.Parameters[1].value := Apagar;
    dtmDados.IQuery1.Parameters[2].value := Editar;
    dtmDados.IQuery1.Parameters[3].value := Visualizar;
    dtmDados.IQuery1.Parameters[4].value := QFuncTelaIDPERMISSAO.AsInteger;
    dtmDados.IQuery1.ExecSQL;
  end;
  QFuncTela.Close;
  if RGPesq.ItemIndex = 0 then
    QFuncTela.sql[3] := 'And Inserir = 1'
  else if RGPesq.ItemIndex = 1 then
    QFuncTela.sql[3] := 'And Alterar = 1'
  else if RGPesq.ItemIndex = 2 then
    QFuncTela.sql[3] := 'And Apagar = 1'
  else if RGPesq.ItemIndex = 3 then
    QFuncTela.sql[3] := 'And Visualizar = 1'
  else
    QFuncTela.sql[3] := 'And Inserir = 1 and Alterar = 1 and Apagar = 1 and Visualizar = 1';
  QFuncTela.Parameters[0].value := QPesqTelaNAME.value;
  QFuncTela.open;
end;

procedure TfrmCadFuncionario.JvDBUltimGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.Field = QFuncTelaINSERIR then
  begin
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QFuncTelaINSERIR.AsInteger = 1 then
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QFuncTelaAPAGAR then
  begin
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QFuncTelaAPAGAR.AsInteger = 1 then
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QFuncTelaALTERAR then
  begin
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QFuncTelaALTERAR.AsInteger = 1 then
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QFuncTelaVISUALIZAR then
  begin
    JvDBUltimGrid1.Canvas.FillRect(Rect);
    iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QFuncTelaVISUALIZAR.AsInteger = 1 then
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(JvDBUltimGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
end;

procedure TfrmCadFuncionario.QFuncionarioAfterScroll(DataSet: TDataSet);
var a, b : String;
    x, y : Integer;
begin
  QTelas.Close;
  QTelas.sql[2] := 'where cod_usuario = ' + #39 +
    QFuncionarioCOD_USUARIO.AsString + #39;
  QTelas.open;
  if tipo = '' then
  begin
    edCodigo.text := QFuncionarioCOD_USUARIO.AsString;
    edCPF.text := QFuncionarioNR_CPF.value;
    Eduser.text := QFuncionarioLOGIN.value;
    EdNome.text := QFuncionarioNM_USUARIO.value;
    edEnd.text := QFuncionarioDS_ENDERECO.value;
    EdCid.text := QFuncionarioDS_CIDADE.value;
    EdIE.text := QFuncionarioDS_RG.value;
    EdFone.text := QFuncionarioNR_TELEFONE.AsString;
    EdUf.text := QFuncionarioDS_UF.text;
    edBai.text := QFuncionarioDS_BAIRRO.value;
    EdCep.text := QFuncionarioDS_CEP.value;
    edDpto.text := QFuncionarioDPTO.value;
    EdUf.text := QFuncionarioDS_UF.value;
    EdEmail.text := QFuncionarioDS_EMAIL.value;
    if QFuncionarioFL_STATUS.value = 'A' then
      cbAtivo.Checked := true
    else
      cbAtivo.Checked := false;
    if QFuncionarioFL_NIVEL.value = 'A' then
      RGNivel.ItemIndex := 0
    else if QFuncionarioFL_NIVEL.value = 'S' then
      RGNivel.ItemIndex := 1
    else
      RGNivel.ItemIndex := 2;
    lblQuant.Caption := IntToStr(QFuncionario.RecNo) + #13#10 +
      IntToStr(QFuncionario.RecordCount);
  end;

  for x := 0 to ComponentCount - 1 do
    if Components[x].ClassType = TJvCheckBox then
      if (TJvCheckBox(Components[x]).parent = Empresa) then
      begin
        if (not QFuncionarioSITE.IsNull) then
        begin
          if Pos(Retzero(FloatToStr(sonumero(TJvCheckBox(Components[x]).name)),2),
            QFuncionarioSITE.value) = 0 then

          begin
            TJvCheckBox(Components[x]).Checked := false;
          end
          else
          begin
            TJvCheckBox(Components[x]).Checked := true;
          end;
        end
        else
          TJvCheckBox(Components[x]).Checked := false;
      end;
end;

procedure TfrmCadFuncionario.RBAtivoClick(Sender: TObject);
begin
  RbInativo.Checked := false;
  QFuncionario.Close;
  QFuncionario.sql[3] := 'and c.fl_status = ''A''';
  QFuncionario.open;
end;

procedure TfrmCadFuncionario.RbInativoClick(Sender: TObject);
begin
  RBAtivo.Checked := false;
  QFuncionario.Close;
  QFuncionario.sql[3] := 'and c.fl_status = ''I''';
  QFuncionario.open;
end;

procedure TfrmCadFuncionario.TabSheet2Hide(Sender: TObject);
begin
  QFuncTela.Close;
end;

procedure TfrmCadFuncionario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QFuncionario.Close;
  QTelas.Close;
  QcadFunc.Close;
  QPesqTela.Close;
  QFuncTela.Close;
end;

procedure TfrmCadFuncionario.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QFuncionario.locate(dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadFuncionario.DBGrid1CellClick(Column: TColumn);
var
  Inserir, Apagar, Editar, Visualizar, Registro: Integer;
begin
  if not dtmDados.PodeAlterar(Name) then
    Exit;
  Registro := QTelasIDPERMISSAO.AsInteger;
  Inserir := QTelasINSERIR.AsInteger;
  Apagar := QTelasAPAGAR.AsInteger;
  Editar := QTelasALTERAR.AsInteger;
  Visualizar := QTelasVISUALIZAR.AsInteger;
  if Column.Field = QTelasINSERIR then
  begin
    if QTelasINSERIR.AsInteger = 0 then
      Inserir := 1
    else
      Inserir := 0;
  end;
  if Column.Field = QTelasAPAGAR then
  begin
    if QTelasAPAGAR.AsInteger = 0 then
      Apagar := 1
    else
      Apagar := 0;
  end;
  if Column.Field = QTelasALTERAR then
  begin
    if QTelasALTERAR.AsInteger = 0 then
      Editar := 1
    else
      Editar := 0;
  end;
  if Column.Field = QTelasVISUALIZAR then
  begin
    if QTelasVISUALIZAR.AsInteger = 0 then
      Visualizar := 1
    else
      Visualizar := 0;
  end;
  if (Column.Field = QTelasINSERIR) or (Column.Field = QTelasAPAGAR) or
    (Column.Field = QTelasALTERAR) or (Column.Field = QTelasVISUALIZAR) then
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.Add('call prc_altera_perm(:1,:2,:3,:4,:5)');
    dtmDados.IQuery1.Parameters[0].value := Inserir;
    dtmDados.IQuery1.Parameters[1].value := Apagar;
    dtmDados.IQuery1.Parameters[2].value := Editar;
    dtmDados.IQuery1.Parameters[3].value := Visualizar;
    dtmDados.IQuery1.Parameters[4].value := QTelasIDPERMISSAO.AsInteger;
    dtmDados.IQuery1.ExecSQL;
  end;
  QTelas.Close;
  QTelas.open;
  QTelas.locate('idPermissao', Registro, []);
end;

procedure TfrmCadFuncionario.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.Field = QTelasINSERIR then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QTelasINSERIR.AsInteger = 1 then
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QTelasAPAGAR then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QTelasAPAGAR.AsInteger = 1 then
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QTelasALTERAR then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QTelasALTERAR.AsInteger = 1 then
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
  if Column.Field = QTelasVISUALIZAR then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
    if QTelasVISUALIZAR.AsInteger = 1 then
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 1)
    else
      iml.Draw(DBGrid1.Canvas, Rect.left + 25, Rect.Top + 1, 0);
  end;
end;

procedure TfrmCadFuncionario.edAnivExit(Sender: TObject);
begin
  if Copy(edAniv.text, 1, 2) <> '  ' then
  begin
    if not ValidaData(edAniv.text) then
    begin
      ShowMessage(edAniv.text + ' N�O � data v�lida.');
      edAniv.setfocus;
    end;
  end;
end;

procedure TfrmCadFuncionario.EduserExit(Sender: TObject);
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.Add('select * from tb_usuarios where login = :0');
  dtmDados.IQuery1.Parameters[0].value := Eduser.text;
  dtmDados.IQuery1.open;
  if not dtmDados.IQuery1.eof then
  begin
    ShowMessage('J� Existe um Funcion�rio com este Login !!');
    Eduser.setfocus;
  end;
end;

procedure TfrmCadFuncionario.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  tipo := 'A';
  PageControl1.ActivePageIndex := 2;
  btnNovo.Enabled := false;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := true;
  cbdpto.Visible := true;
  QDpto.open;
  cbdpto.clear;
  while not QDpto.eof do
  begin
    cbdpto.Items.Add(QDptoDEPARTAMENTO.AsString);
    QDpto.next;
  end;
  QDpto.Close;
  cbdpto.ItemIndex := cbdpto.Items.IndexOf(edDpto.text);
end;

procedure TfrmCadFuncionario.btnAtualizarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(Name) then
    Exit;
  Query1.open;
  While not Query1.eof do
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.Add
      ('select * from tb_permissao_sis where cod_usuario = :cod and idtela = :tela');
    dtmDados.IQuery1.Parameters[0].value := QFuncionarioCOD_USUARIO.AsInteger;
    dtmDados.IQuery1.Parameters[1].value := Query1IDTELA.AsInteger;
    dtmDados.IQuery1.open;
    if dtmDados.IQuery1.FieldByName('idtela').IsNull then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.Add
        ('insert into tb_permissao_sis (cod_usuario, idtela, inserir, apagar, alterar, '
        + 'visualizar) values (:co, :oi, :in, :ap, :ed, :vs)');
      dtmDados.IQuery1.Parameters[0].value := QFuncionarioCOD_USUARIO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := Query1IDTELA.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := 0;
      dtmDados.IQuery1.Parameters[3].value := 0;
      dtmDados.IQuery1.Parameters[4].value := 0;
      dtmDados.IQuery1.Parameters[5].value := 0;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
    Query1.next;
  end;
  dtmDados.IQuery1.Close;
  Query1.Close;
  QTelas.Close;
  QTelas.open;
end;

procedure TfrmCadFuncionario.btnDuplicarClick(Sender: TObject);
var
  Retorno: String;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  Retorno := InserirValor('Duplicar Igual ao C�digo ? ', Caption);
  if Retorno = '' then
    Exit;
  QDuplicar.Close;
  QDuplicar.Parameters[0].value := Retorno;
  QDuplicar.open;
  if QDuplicar.RecordCount > 0 then
  begin
    dtmDados.IQuery1.Close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.Add
      ('delete from tb_permissao_sis where cod_usuario = :co');
    dtmDados.IQuery1.Parameters[0].value := QFuncionarioCOD_USUARIO.AsInteger;
    dtmDados.IQuery1.ExecSQL;
    dtmDados.IQuery1.Close;
    while not QDuplicar.eof do
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.Add
        ('insert into tb_permissao_sis (cod_usuario, idtela, inserir, apagar, alterar, '
        + 'visualizar) values (:co, :oi, :in, :ap, :ed, :vs)');
      dtmDados.IQuery1.Parameters[0].value := QFuncionarioCOD_USUARIO.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := QDuplicarIDTELA.AsInteger;
      dtmDados.IQuery1.Parameters[2].value := QDuplicarINSERIR.AsInteger;
      dtmDados.IQuery1.Parameters[3].value := QDuplicarALTERAR.AsInteger;
      dtmDados.IQuery1.Parameters[4].value := QDuplicarAPAGAR.AsInteger;
      dtmDados.IQuery1.Parameters[5].value := QDuplicarVISUALIZAR.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
      QDuplicar.next;
    end;
    ShowMessage('Processo Finalizado !!');
    QTelas.Close;
    QTelas.open;
  end
  else
    ShowMessage('Fucnion�rio n�o encontrado');
end;

procedure TfrmCadFuncionario.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QFuncionario.First;
    2:
      QFuncionario.Prior;
    3:
      QFuncionario.next;
    4:
      QFuncionario.Last;
  end;
  Screen.Cursor := crDefault;
end;

end.
