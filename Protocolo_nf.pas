unit Protocolo_nf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, Buttons, JvGradient, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExControls, JvEnterTab, ImgList,
  JvMaskEdit, DBCtrls, JvExStdCtrls, JvCombobox, System.ImageList;

type
  TfrmProtocolo_nf = class(TForm)
    Panel3: TPanel;
    JvGradient1: TJvGradient;
    btnGravar: TBitBtn;
    Panel2: TPanel;
    QEntrada: TADOQuery;
    iml: TImageList;
    Label11: TLabel;
    Label4: TLabel;
    QForne: TADOQuery;
    btnImprimir: TBitBtn;
    lblqt: TLabel;
    edNF: TJvMaskEdit;
    Label6: TLabel;
    Label9: TLabel;
    edPedido: TJvMaskEdit;
    edvcto: TJvMaskEdit;
    Label10: TLabel;
    SPEntrada: TADOStoredProc;
    cbForne: TComboBox;
    Memo1: TMemo;
    lblKm: TLabel;
    QForneCODCLIFOR: TBCDField;
    QForneRAZSOC: TStringField;
    QForneCODCGC: TStringField;
    Label1: TLabel;
    edDtRec: TJvMaskEdit;
    Label3: TLabel;
    edDtNF: TJvMaskEdit;
    Label2: TLabel;
    edReg: TJvMaskEdit;
    QEntradaCOD: TBCDField;
    QEntradaNR_NOTA: TStringField;
    QEntradaDT_NOTA: TDateTimeField;
    QEntradaFORNECEDOR: TStringField;
    QEntradaDT_VENCIMENTO: TDateTimeField;
    QEntradaDT_RECEBIMENTO: TDateTimeField;
    QEntradaDT_LANCAMENTO: TDateTimeField;
    QEntradaDT_ENTREGA: TDateTimeField;
    QEntradaNR_SOLCITACAO: TStringField;
    QEntradaUSUARIO: TStringField;
    QEntradaFL_STATUS: TStringField;
    QEntradaOBSERVACAO: TStringField;
    QEntradaCODCLIFOR: TBCDField;
    QEntradaRAZSOC: TStringField;
    QEntradaCODCGC: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  frmProtocolo_nf: TfrmProtocolo_nf;

implementation

uses Dados, menu, funcoes;

{$R *.dfm}

procedure TfrmProtocolo_nf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  frmProtocolo_nf := nil;
  QEntrada.close;
  QForne.close;
end;

procedure TfrmProtocolo_nf.FormCreate(Sender: TObject);
begin
  cbForne.Visible := true;
  QForne.open;
  cbForne.clear;
  cbForne.Items.add('');
  while not QForne.eof do
  begin
    cbForne.Items.add(QForneRAZSOC.AsString + ' - ' + QForneCODCGC.AsString);
    QForne.Next;
  end;
  QForne.close;
end;

procedure TfrmProtocolo_nf.btnGravarClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if edNF.text = '' then
  begin
    ShowMessage('N�o foi Digitado a NF !!');
    edNF.setfocus;
    Exit;
  end;

  if (cbForne.text = '') and (cbForne.Visible = true) then
  begin
    ShowMessage('Informe o Fornecedor');
    cbForne.setfocus;
    Exit;
  end;

  if (edvcto.text = '') and (edvcto.Enabled = true) then
  begin
    ShowMessage('Informe a Data de Vcto');
    edvcto.setfocus;
    Exit;
  end;

  if edDtRec.text = '' then
  begin
    ShowMessage('Informe a Data de Recebimento');
    edDtRec.setfocus;
    Exit;
  end;

  if (edDtNF.text = '') and (edvcto.Enabled = true) then
  begin
    ShowMessage('Informe a Data da NF');
    edDtNF.setfocus;
    Exit;
  end;

  if Application.Messagebox('Voc� Deseja Gerar Este Protocolo ?',
    'Protocolo - NF', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    SPEntrada.Parameters[0].Value := 0;
    SPEntrada.Parameters[1].Value := 0;
    QForne.close;
    QForne.sql[4] := 'and (razsoc || '' - '' || codcgc) = ' +
      QuotedStr(cbForne.text);
    QForne.open;

    SPEntrada.Parameters[2].Value := QForneCODCLIFOR.AsInteger;
    QForne.close;
    SPEntrada.Parameters[3].Value := StrToDate(edvcto.text);
    SPEntrada.Parameters[4].Value := StrToDate(edDtNF.text);
    SPEntrada.Parameters[5].Value := GlbUser;
    SPEntrada.Parameters[6].Value := StrToDate(edDtRec.text);
    SPEntrada.Parameters[7].Value := edPedido.text;
    SPEntrada.Parameters[8].Value := Memo1.text;
    SPEntrada.Parameters[9].Value := edNF.text;
    SPEntrada.Parameters[10].Value := 'E';

    SPEntrada.ExecProc;
    edReg.text := IntToStr(SPEntrada.Parameters[0].Value);
    ShowMessage('Protocolo ' + IntToStr(SPEntrada.Parameters[0].Value) +
      ' Registrado');
    edNF.clear;
    cbForne.ItemIndex := -1;
    edPedido.clear;
    edvcto.clear;
    edDtRec.clear;
    edDtNF.clear;
    Memo1.clear;
  end;
end;

end.
