unit CadControle_fiscal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, JvExControls, JvDBLookup, DB, ImgList, ADODB, ActnList, Buttons,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, JvExStdCtrls, JvGroupBox, Menus, ComCtrls,
  JvExComCtrls, JvComCtrls;

type
  TfrmCadControle_fiscal = class(TForm)
    QEmpresa: TADOQuery;
    dtsEmpresa: TDataSource;
    QEmpresaCODFIL: TBCDField;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    edCancelar: TJvCalcEdit;
    QEmpresaTMP_CANC_CTE: TBCDField;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn9: TBitBtn;
    Query1: TADOQuery;
    BCDField1: TBCDField;
    BCDField2: TBCDField;

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura1;
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadControle_fiscal: TfrmCadControle_fiscal;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadControle_fiscal.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadControle_fiscal.BitBtn2Click(Sender: TObject);
begin
  Restaura1;
  QEmpresa.Cancel;
end;

procedure TfrmCadControle_fiscal.BitBtn3Click(Sender: TObject);
begin
  try
    if tipo = 'I' then
    begin

    end
    else
    begin
      Query1.Close;
      Query1.Parameters[0].value := edCancelar.value;
      Query1.Parameters[1].value := QEmpresaCODFIL.value;
      Query1.ExecSQL;
      Query1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura1;
  tipo := '';
  QEmpresa.Close;
  QEmpresa.Open;
end;

procedure TfrmCadControle_fiscal.BitBtn4Click(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura1;
  tipo := 'A';
  edCancelar.value := QEmpresaTMP_CANC_CTE.value;
  edCancelar.SetFocus;
end;

procedure TfrmCadControle_fiscal.BitBtn9Click(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura1;
  tipo := 'I';
  edCancelar.value := 0;
  edCancelar.SetFocus;
end;

procedure TfrmCadControle_fiscal.FormCreate(Sender: TObject);
begin
  QEmpresa.Open;
end;

procedure TfrmCadControle_fiscal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresa.Close;
end;

procedure TfrmCadControle_fiscal.Restaura1;
begin
  // bitBtn9.Enabled := not bitBtn9.Enabled;
  BitBtn4.enabled := not BitBtn4.enabled;
  BitBtn3.enabled := not BitBtn3.enabled;
  BitBtn2.enabled := not BitBtn2.enabled;
  BitBtn1.enabled := not BitBtn1.enabled;

  if Panel1.Visible then
  begin
    Panel1.Visible := false;
  end
  else
  begin
    Panel1.Visible := True;
  end;
end;

end.
