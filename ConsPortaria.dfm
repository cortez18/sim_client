object frmConsPortaria: TfrmConsPortaria
  Left = 0
  Top = 0
  Caption = 'Movimento da Portaria'
  ClientHeight = 495
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 461
    Top = 184
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1081
    Height = 493
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'P'#225'tio'
      OnShow = TabSheet1Show
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1076
        Height = 437
        DataSource = dtsPatio
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 20
        TitleRowHeight = 20
        Columns = <
          item
            Expanded = False
            FieldName = 'DATA'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'MOTIVO'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PLACA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_TRANSP'
            Title.Caption = 'TRANSPORTADOR'
            Width = 241
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_MOTOR'
            Title.Caption = 'MOTORISTA'
            Width = 199
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MANIFESTO'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_RG'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NR_CNH'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS'
            Title.Caption = 'OBSERVA'#199#195'O'
            Width = 313
            Visible = True
          end>
      end
      object JvDBNavigator1: TJvDBNavigator
        Left = 7
        Top = 440
        Width = 190
        Height = 25
        DataSource = dtsPatio
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Entradas'
      ImageIndex = 1
      OnShow = TabSheet2Show
      object Label10: TLabel
        Left = 12
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Data Inicial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 109
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data Final'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dtInicial: TJvDateEdit
        Left = 12
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 0
      end
      object dtFinal: TJvDateEdit
        Left = 109
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 1
        OnExit = dtFinalExit
      end
      object btnEntrada: TBitBtn
        Left = 373
        Top = 8
        Width = 71
        Height = 25
        Caption = 'Consultar'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
          FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
          96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
          92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
          BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
          CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
          D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
          EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
          E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
          E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
          8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
          E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
          FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
          C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
          CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
          CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
          D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
          9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 2
        TabStop = False
        OnClick = btnEntradaClick
      end
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 47
        Width = 1069
        Height = 386
        DataSource = dtsEntrada
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = JvDBGrid2DblClick
        OnTitleClick = JvDBGrid2TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 20
        TitleRowHeight = 20
        Columns = <
          item
            Expanded = False
            FieldName = 'DATA'
            Width = 111
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'MOTIVO'
            Width = 111
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PLACA'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_TRANSP'
            Title.Caption = 'TRANSPORTADOR'
            Width = 241
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_MOTOR'
            Title.Caption = 'MOTORISTA'
            Width = 199
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_USUARIO'
            Title.Caption = 'USU'#193'RIO'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MANIFESTO'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIE'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'PALLET'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_CNH'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS'
            Title.Caption = 'OBSERVA'#199#195'O'
            Width = 313
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_SAIDA'
            Title.Caption = 'DATA SA'#205'DA'
            Visible = True
          end>
      end
      object JvDBNavigator2: TJvDBNavigator
        Left = 7
        Top = 440
        Width = 188
        Height = 25
        DataSource = dtsEntrada
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 4
      end
      object btnPallet: TBitBtn
        Left = 665
        Top = 8
        Width = 71
        Height = 25
        Caption = 'Pallet'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001B5989B71B58
          88FF1A5686FF1A5585FF195483FF195482FF185380FF18527FFF17517EFF1750
          7DFF164F7BFF164E7AFF164E79FF164D79FF154D78FF154C77C01C5A8BF07EAD
          E0FF81AFE4FF7FABE2FF7EA8E0FF7CA6DFFF7AA2DEFF79A0DDFF779EDCFF769B
          DBFF7599DAFF7498DAFF7498DAFF7498DAFF7498DAFF154D78FF1C5B8CF080B2
          E3FF5194DCFF448AD8FF4285D5FF4080D4FF3D7CD1FF3B77CFFF3873CDFF3670
          CCFF346DCAFF3269C9FF3167C8FF3065C8FF7498DAFF164D79FF1D5C8EF083B7
          E5FF559DDFFF4A92DBFF478DD9FF4489D7FF4183D5FF3F7FD3FF3C7BD0FF3A76
          CEFF3772CDFF356ECBFF336BCAFF3268C9FF7598DAFF164E7AFF1D5D8FF085BD
          E6FF59A4E3FF4F9BDEFF4C96DCFF4991DAFF478CD8FF4387D6FF4082D4FF3E7D
          D1FF3C7AD0FF3874CEFF3771CCFF346ECBFF769BDBFF164F7BFF1E5E91F088C1
          E8FF5EACE6FF54A3E3FF509EE1FF4D99DEFF4B94DCFF488FDAFF448AD8FF4285
          D5FF4080D4FF3D7CD1FF3B77CFFF3873CDFF789FDDFF17507CFF1E5F92F08BC6
          EAFF62B3E9FF58AAE6FF55A6E4FF52A2E2FF4F9CE0FF4D98DDFF4A92DBFF478D
          D9FF4489D7FF4183D5FF3F7FD3FF3C7BD0FF7BA3DEFF17517EFF1F6094F08DC8
          EDFF65B8EBFF5BB2E9FF59AEE7FF57A9E5FF54A5E3FF51A0E2FF4F9BDEFF4C96
          DCFF4991DAFF478CD8FF4387D6FF4082D4FF7EA8E0FF18527FFF1F6195F08FCC
          EEFF8FCCF4FF87C5F2FF83C3F1FF80BFEFFF7DBCEEFF7AB8EBFF75B4EAFF71B0
          E8FF6EABE6FF69A7E5FF66A3E3FF629EE1FF80ADE3FF185381FF1F6296F077B1
          D9FF296EA4FF17619BFF175E99FF175C96FF165B92FF15598EFF15568BFF1454
          87FF145182FF124E7EFF124C7AFF39688CFF678EB0FF195482FF206297DB417C
          AAFF6496BEFF2B6FA6FF2068A1FF20659FFF20649CFF1F6299FF1F6095FF1E5F
          91FF1E5D8FFF1D598AFF3F7098FF5B84A6FF386B94FF195584AF206398122062
          97EE3F7AA8FF6093BCFF6194BCFF6193BCFF6092BBFF6091B9FF6090B7FF608F
          B5FF5F8EB3FF5F8CB0FF5A88ABFF386D98FF1A578783FFFFFF00FFFFFF002063
          980C206297A41F6296F11F6196F31F6195F31F6094F31E5F92F31E5E91F31D5D
          90F31D5C8FF31C5B8DF31C5B8CE71B5A8A7CFFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        TabOrder = 5
        TabStop = False
        OnClick = btnPalletClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Sa'#237'das'
      ImageIndex = 2
      OnShow = TabSheet3Show
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Data Inicial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 109
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data Final'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dtInicialS: TJvDateEdit
        Left = 12
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 0
      end
      object dtFinalS: TJvDateEdit
        Left = 109
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 1
        OnExit = dtFinalSExit
      end
      object btSaida: TBitBtn
        Left = 373
        Top = 8
        Width = 71
        Height = 25
        Caption = 'Consultar'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
          FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
          96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
          92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
          BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
          CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
          D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
          EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
          E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
          E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
          8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
          E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
          FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
          C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
          CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
          CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
          D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
          9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 2
        TabStop = False
        OnClick = btSaidaClick
      end
      object JvDBGrid3: TJvDBGrid
        Left = 0
        Top = 47
        Width = 1069
        Height = 388
        DataSource = dtsSaida
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDrawColumnCell = JvDBGrid3DrawColumnCell
        OnTitleClick = JvDBGrid3TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 20
        TitleRowHeight = 20
        Columns = <
          item
            Expanded = False
            FieldName = 'DATA'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'MOTIVO'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PLACA'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_TRANSP'
            Title.Caption = 'TRANSPORTADOR'
            Width = 241
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_MOTOR'
            Title.Caption = 'MOTORISTA'
            Width = 199
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MANIFESTO'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PALLET'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_USUARIO'
            Title.Caption = 'USU'#193'RIO'
            Width = 107
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NR_RG'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NR_CNH'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS'
            Title.Caption = 'OBSERVA'#199#195'O'
            Width = 313
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RETORNO_PORTAL'
            Visible = True
          end>
      end
      object btnemail: TBitBtn
        Left = 469
        Top = 8
        Width = 71
        Height = 25
        Caption = 'E-mail'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF8E512BB06331BB7038BE773CC17B40C27E42C28045C3
          8247C38649C3864AC3874BC3874BC3874CBD8349AC74408E512BA35D31F8F3EE
          F5ECE4FBF5F0FBF7F1FBF7F3FBF8F4FCF9F5FCF9F5FCF9F6FCF9F7FCFAF7FCFA
          F7F7F1ECEAD9CCAB7642BE6F3CFCF9F5ECD0BCF9E4D6FEECDFFEEBDFFEEBDEFE
          EBDBFEEBDCFEEADDFDEADBFDE8D8F8E0CDEACBB3F3EBE3C78B50C27646FDFBF8
          F9E3D2ECCFB9F8E1D0FDE7D6F4D5BDE9BFA0E9BFA2F4D3BDFDE6D4F7DEC9EBCA
          B0F8DBC4F8F2ECC98C50C57D50FDFBF9FDE9D8F9E1D0EBCAB3ECC5A7E3B698F7
          E7DDF7E8DEE3B697ECC3A4EAC5A9F8DAC2FCDFC6F8F3EDC88D50C9865BFDFBF9
          FDE8D7FDE6D4EDC6ABDCAA89F9ECE3FFFBF9FFFCFAF9EEE6DCA887EDBF9CFCDB
          C0FCDBC0F8F3EDC88C50CC8E66FDFBF9FDE5D3F1CCB2E3B596F9EAE0FFF9F5FE
          F3EAFEF4EDFFFBF9F9EDE5E3B08DF0C19EFCD7B7F8F3EDC88C50D09670FDFBF9
          F1CDB1E3B596F9E9DEFEF7F1FDEDE1FEEFE4FEF1E7FEF3EAFFFAF7F9ECE3E2AE
          8AF0BC95F8F4ECC88C50D39D7BFBF6F2E3B496F9E8DCFEF5EEFDE9DAFDEADCFD
          ECDEFDEDE1FEEFE4FEF1E7FFFAF6F9EAE0E2AA85F1E4D9C88C50D7AB91FDFAF8
          FCF5F1FFFCF9FFFCF9FFFCF9FFFCF9FFFCFAFFFCFAFFFCFAFFFCFBFFFDFBFFFD
          FCFBF6F3F8EFEAAB7743C89A7CD5A484D09770CC8F64CC8E62CB8E60CA8C5DC9
          8B5BC88A58C78856C68653C58450C4824DC1834DB279488E512BFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 4
        TabStop = False
        Visible = False
        OnClick = btnemailClick
      end
      object JvDBNavigator3: TJvDBNavigator
        Left = 7
        Top = 440
        Width = 190
        Height = 25
        DataSource = dtsSaida
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        TabOrder = 5
      end
      object btnSMS: TBitBtn
        Left = 582
        Top = 8
        Width = 74
        Height = 25
        Caption = 'APP'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000EFF3FF70A5FF
          0077FF0075FF0071FF0069FF0063FF005DFF0055FF0051FF007B2A037B1E0048
          E10045FF709AFFEFF2FE70A9FF49AFFF7EC5FF7EC1FF79BEFF76B9FF73B4FF70
          AFFF6DA9FF6AA4FF05843343A15F0B7E37518FDC3371FF709AFF0081FF80C9FF
          6AB9FF67B2FF76B7FF4799FF2197521B914A158F440F8B3C3A9F5E80C19646A3
          62087E34508FD90045FF0087FF84CAFF6DB9FFF5FAFFFFFFFFAFD4FF299B5B90
          CAA98DC8A58AC6A188C59E6AB68582C29748A566087F33004DD40087FF83CAFF
          82C2FFFFFFFFFFFFFFD2E9FF319F6394CDAD6FBA8E6BB88966B68561B38067B5
          8283C2983CA05C007E280087FF84C8FF56ADFFCAE5FFD6EAFF97CCFF37A36B96
          CEB094CDAD91CBAA90CBA874BC908AC7A146A5680887370050F30089FF83C8FF
          57AAFF57ADFF58AEFF63B3FF3DA56F39A46E34A2682F9D6255AF7C91CBAA4FAB
          74178F4667A4F60053FF0087FF81C6FF55ABFF6DB6FF99CCFFEAF4FFFFFFFFF5
          FAFF74BAFF4FA1FF38A2695AB3812898573B85F96EABFF0059FF0085FF81C5FF
          84BDFFFFFFFFFFFFFFFFFFFFF5FAFF79BEFF59AEFFBFDFFF3FA771319F654F9A
          F9408CFF70B0FF005FFF0087FF7EC5FF84BCFFF4FAFFD6EAFFA4D0FF63B2FF58
          AEFFC1E1FFFFFFFFFFFFFF84C0FF499BFF4594FF73B5FF0067FF0085FF7EC3FF
          4FA4FF52A6FF53A8FF55ACFF85C0FFE1F0FFFFFFFFFFFFFF99CDFF54A9FF50A2
          FF499CFF77BAFF006DFF0083FF7DC1FF68AEFFAED2FFC3DDFFEAF4FFFFFFFFFF
          FFFFF6FAFF90C8FF59AFFF5AAFFF55AAFF50A2FF7ABFFF0075FF0085FF79C0FF
          81B8FFFFFFFFFFFFFFFFFFFFF4FAFFAFD5FF64B1FF58AEFF59AFFF59AFFF5AAF
          FF55AAFF80C3FF007BFF0085FF74BDFF77B6FFAECEFFAED1FF84BBFF52A7FF54
          A9FF56ACFF58ACFF58AFFF5AB0FF59AFFF6DBBFF80C7FF007DFF70B8FF41A6FF
          74BDFF79C0FF7DC2FF7EC3FF7FC5FF81C6FF82C8FF83CAFF86CAFF86CBFF86CC
          FF83CBFF4EB1FF70AAFFEFF5FF70B8FF0085FF0085FF0085FF0087FF0089FF00
          89FF0089FF008BFF008BFF008BFF008BFF0087FF70ADFFEFF4FF}
        TabOrder = 6
        OnClick = btnSMSClick
      end
      object btnPalletS: TBitBtn
        Left = 967
        Top = 8
        Width = 74
        Height = 25
        Caption = 'Pallet'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001B5989B71B58
          88FF1A5686FF1A5585FF195483FF195482FF185380FF18527FFF17517EFF1750
          7DFF164F7BFF164E7AFF164E79FF164D79FF154D78FF154C77C01C5A8BF07EAD
          E0FF81AFE4FF7FABE2FF7EA8E0FF7CA6DFFF7AA2DEFF79A0DDFF779EDCFF769B
          DBFF7599DAFF7498DAFF7498DAFF7498DAFF7498DAFF154D78FF1C5B8CF080B2
          E3FF5194DCFF448AD8FF4285D5FF4080D4FF3D7CD1FF3B77CFFF3873CDFF3670
          CCFF346DCAFF3269C9FF3167C8FF3065C8FF7498DAFF164D79FF1D5C8EF083B7
          E5FF559DDFFF4A92DBFF478DD9FF4489D7FF4183D5FF3F7FD3FF3C7BD0FF3A76
          CEFF3772CDFF356ECBFF336BCAFF3268C9FF7598DAFF164E7AFF1D5D8FF085BD
          E6FF59A4E3FF4F9BDEFF4C96DCFF4991DAFF478CD8FF4387D6FF4082D4FF3E7D
          D1FF3C7AD0FF3874CEFF3771CCFF346ECBFF769BDBFF164F7BFF1E5E91F088C1
          E8FF5EACE6FF54A3E3FF509EE1FF4D99DEFF4B94DCFF488FDAFF448AD8FF4285
          D5FF4080D4FF3D7CD1FF3B77CFFF3873CDFF789FDDFF17507CFF1E5F92F08BC6
          EAFF62B3E9FF58AAE6FF55A6E4FF52A2E2FF4F9CE0FF4D98DDFF4A92DBFF478D
          D9FF4489D7FF4183D5FF3F7FD3FF3C7BD0FF7BA3DEFF17517EFF1F6094F08DC8
          EDFF65B8EBFF5BB2E9FF59AEE7FF57A9E5FF54A5E3FF51A0E2FF4F9BDEFF4C96
          DCFF4991DAFF478CD8FF4387D6FF4082D4FF7EA8E0FF18527FFF1F6195F08FCC
          EEFF8FCCF4FF87C5F2FF83C3F1FF80BFEFFF7DBCEEFF7AB8EBFF75B4EAFF71B0
          E8FF6EABE6FF69A7E5FF66A3E3FF629EE1FF80ADE3FF185381FF1F6296F077B1
          D9FF296EA4FF17619BFF175E99FF175C96FF165B92FF15598EFF15568BFF1454
          87FF145182FF124E7EFF124C7AFF39688CFF678EB0FF195482FF206297DB417C
          AAFF6496BEFF2B6FA6FF2068A1FF20659FFF20649CFF1F6299FF1F6095FF1E5F
          91FF1E5D8FFF1D598AFF3F7098FF5B84A6FF386B94FF195584AF206398122062
          97EE3F7AA8FF6093BCFF6194BCFF6193BCFF6092BBFF6091B9FF6090B7FF608F
          B5FF5F8EB3FF5F8CB0FF5A88ABFF386D98FF1A578783FFFFFF00FFFFFF002063
          980C206297A41F6296F11F6196F31F6195F31F6094F31E5F92F31E5E91F31D5D
          90F31D5C8FF31C5B8DF31C5B8CE71B5A8A7CFFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        TabOrder = 7
        Visible = False
        OnClick = btnPalletSClick
      end
      object btnRegistro: TBitBtn
        Left = 687
        Top = 8
        Width = 119
        Height = 25
        Caption = 'Registro de Sa'#237'da'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
          52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
          FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
          D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
          FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
          FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
          BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
          FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
          FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
          A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
          B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
          A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
          CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
          B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
          FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
          F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
          A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
          F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
          F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
          8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
          F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
          F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
          F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
          90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
          D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
          BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 8
        OnClick = btnRegistroClick
      end
      object BitBtn1: TBitBtn
        Left = 839
        Top = 8
        Width = 74
        Height = 25
        Caption = 'Portal'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000EFF3FF70A5FF
          0077FF0075FF0071FF0069FF0063FF005DFF0055FF0051FF007B2A037B1E0048
          E10045FF709AFFEFF2FE70A9FF49AFFF7EC5FF7EC1FF79BEFF76B9FF73B4FF70
          AFFF6DA9FF6AA4FF05843343A15F0B7E37518FDC3371FF709AFF0081FF80C9FF
          6AB9FF67B2FF76B7FF4799FF2197521B914A158F440F8B3C3A9F5E80C19646A3
          62087E34508FD90045FF0087FF84CAFF6DB9FFF5FAFFFFFFFFAFD4FF299B5B90
          CAA98DC8A58AC6A188C59E6AB68582C29748A566087F33004DD40087FF83CAFF
          82C2FFFFFFFFFFFFFFD2E9FF319F6394CDAD6FBA8E6BB88966B68561B38067B5
          8283C2983CA05C007E280087FF84C8FF56ADFFCAE5FFD6EAFF97CCFF37A36B96
          CEB094CDAD91CBAA90CBA874BC908AC7A146A5680887370050F30089FF83C8FF
          57AAFF57ADFF58AEFF63B3FF3DA56F39A46E34A2682F9D6255AF7C91CBAA4FAB
          74178F4667A4F60053FF0087FF81C6FF55ABFF6DB6FF99CCFFEAF4FFFFFFFFF5
          FAFF74BAFF4FA1FF38A2695AB3812898573B85F96EABFF0059FF0085FF81C5FF
          84BDFFFFFFFFFFFFFFFFFFFFF5FAFF79BEFF59AEFFBFDFFF3FA771319F654F9A
          F9408CFF70B0FF005FFF0087FF7EC5FF84BCFFF4FAFFD6EAFFA4D0FF63B2FF58
          AEFFC1E1FFFFFFFFFFFFFF84C0FF499BFF4594FF73B5FF0067FF0085FF7EC3FF
          4FA4FF52A6FF53A8FF55ACFF85C0FFE1F0FFFFFFFFFFFFFF99CDFF54A9FF50A2
          FF499CFF77BAFF006DFF0083FF7DC1FF68AEFFAED2FFC3DDFFEAF4FFFFFFFFFF
          FFFFF6FAFF90C8FF59AFFF5AAFFF55AAFF50A2FF7ABFFF0075FF0085FF79C0FF
          81B8FFFFFFFFFFFFFFFFFFFFF4FAFFAFD5FF64B1FF58AEFF59AFFF59AFFF5AAF
          FF55AAFF80C3FF007BFF0085FF74BDFF77B6FFAECEFFAED1FF84BBFF52A7FF54
          A9FF56ACFF58ACFF58AFFF5AB0FF59AFFF6DBBFF80C7FF007DFF70B8FF41A6FF
          74BDFF79C0FF7DC2FF7EC3FF7FC5FF81C6FF82C8FF83CAFF86CAFF86CBFF86CC
          FF83CBFF4EB1FF70AAFFEFF5FF70B8FF0085FF0085FF0085FF0087FF0089FF00
          89FF0089FF008BFF008BFF008BFF008BFF0087FF70ADFFEFF4FF}
        TabOrder = 9
        Visible = False
        OnClick = BitBtn1Click
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Movimento'
      ImageIndex = 3
      OnShow = TabSheet4Show
      object Label3: TLabel
        Left = 12
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Data Inicial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 109
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data Final'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 219
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Placa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 915
        Top = 34
        Width = 59
        Height = 16
        Caption = 'Caminho :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Gauge1: TGauge
        Left = 203
        Top = 439
        Width = 706
        Height = 23
        Progress = 0
      end
      object Label6: TLabel
        Left = 312
        Top = 4
        Width = 66
        Height = 13
        Caption = 'Transportador'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 650
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Manifesto'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dtMovimI: TJvDateEdit
        Left = 12
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 0
      end
      object dtMovimF: TJvDateEdit
        Left = 109
        Top = 20
        Width = 90
        Height = 24
        ShowNullDate = False
        TabOrder = 1
        OnExit = dtMovimFExit
      end
      object BitBtn2: TBitBtn
        Left = 922
        Top = 8
        Width = 71
        Height = 25
        Caption = 'Consultar'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
          FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
          96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
          92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
          BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
          CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
          D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
          EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
          E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
          E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
          8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
          E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
          FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
          C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
          CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
          CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
          D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
          9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 2
        TabStop = False
        OnClick = BitBtn2Click
      end
      object JvDBGrid4: TJvDBGrid
        Left = 0
        Top = 47
        Width = 909
        Height = 382
        DataSource = dtsMovim
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnTitleClick = JvDBGrid4TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 20
        TitleRowHeight = 20
        Columns = <
          item
            Expanded = False
            FieldName = 'DATA'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MOTIVO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PLACA'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_TRANSP'
            Title.Caption = 'TRANSPORTADOR'
            Width = 241
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_MOTOR'
            Title.Caption = 'MOTORISTA'
            Width = 199
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'DESCRI'#199#195'O'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MANIFESTO'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_SAIDA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TEMPO'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JANELA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NM_USUARIO'
            Title.Caption = 'USU'#193'RIO'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KM'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS'
            Title.Caption = 'OBSERVA'#199#195'O'
            Width = 313
            Visible = True
          end>
      end
      object edPlaca: TJvMaskEdit
        Left = 219
        Top = 20
        Width = 85
        Height = 21
        EditMask = '!>LLL-0A00;0;_'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 8
        ParentFont = False
        TabOrder = 4
        Text = ''
      end
      object btnImprimir: TBitBtn
        Left = 999
        Top = 8
        Width = 70
        Height = 25
        Caption = 'Imprimir'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
          52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
          FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
          D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
          FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
          FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
          BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
          FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
          FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
          A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
          B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
          A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
          CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
          B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
          FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
          F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
          A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
          F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
          F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
          8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
          F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
          F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
          F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
          90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
          D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
          BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 5
        OnClick = btnImprimirClick
      end
      object JvDirectoryListBox1: TJvDirectoryListBox
        Left = 915
        Top = 72
        Width = 157
        Height = 390
        Directory = 'C:\WINDOWS\system32'
        DriveCombo = JvDriveCombo1
        ItemHeight = 21
        ScrollBars = ssVertical
        TabOrder = 6
      end
      object JvDriveCombo1: TJvDriveCombo
        Left = 915
        Top = 50
        Width = 157
        Height = 26
        DriveTypes = [dtFixed, dtRemote, dtCDROM]
        Offset = 4
        ItemHeight = 20
        TabOrder = 7
      end
      object JvDBNavigator4: TJvDBNavigator
        Left = 7
        Top = 440
        Width = 188
        Height = 25
        DataSource = dtsMovim
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        TabOrder = 8
      end
      object RGFilial: TRadioGroup
        Left = 736
        Top = 3
        Width = 173
        Height = 38
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Filial'
          'Todas')
        TabOrder = 9
      end
      object cbTransp: TComboBox
        Left = 312
        Top = 20
        Width = 328
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        CharCase = ecUpperCase
        DropDownCount = 40
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        Items.Strings = (
          'ESCOLHA O MODAL......')
      end
      object edManifesto: TJvCalcEdit
        Left = 650
        Top = 20
        Width = 59
        Height = 24
        DecimalPlaces = 0
        DisplayFormat = '0'
        ShowButton = False
        TabOrder = 11
        DecimalPlacesAlwaysShown = False
      end
    end
  end
  object RLReport1: TRLReport
    Left = 83
    Top = 2000
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel2: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel3: TRLLabel
        Left = 12
        Top = 212
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel4: TRLLabel
        Left = 9
        Top = 100
        Width = 42
        Height = 16
        Caption = 'Frota :'
      end
      object RLLabel11: TRLLabel
        Left = 12
        Top = 236
        Width = 31
        Height = 16
        Caption = 'RG :'
      end
      object RLLabel12: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel15: TRLLabel
        Left = 264
        Top = 27
        Width = 135
        Height = 16
        Caption = 'REGISTRO DE SA'#205'DA'
      end
      object RLLabel17: TRLLabel
        Left = 456
        Top = 236
        Width = 39
        Height = 16
        Caption = 'CNH :'
      end
      object RLLabel19: TRLLabel
        Left = 240
        Top = 236
        Width = 38
        Height = 16
        Alignment = taRightJustify
        Caption = 'CPF :'
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 676
        Top = 6
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
      object RLMemo1: TRLMemo
        AlignWithMargins = True
        Left = 8
        Top = 277
        Width = 702
        Height = 49
        Alignment = taJustify
        AutoSize = False
        Behavior = [beSiteExpander]
        Lines.Strings = (
          
            'Declaro, que recebi e conferi, para fins de entrega aos respecti' +
            'vos clientes, todas as mercadorias e volumes constantes das Nota' +
            's Fiscais relacionadas em anexo, assim como todos os documentos ' +
            'fiscais pertinentes, assumindo sobre os mesmos toda e qualquer r' +
            'esponsabilidade pela entrega aos destinat'#225'rios.')
      end
      object RLDraw1: TRLDraw
        AlignWithMargins = True
        Left = 0
        Top = 266
        Width = 717
        Height = 1
      end
      object RLLabel13: TRLLabel
        Left = 46
        Top = 411
        Width = 199
        Height = 16
        Caption = 'Data : _______/ ______/ _______'
      end
      object RLLabel22: TRLLabel
        Left = 297
        Top = 409
        Width = 392
        Height = 16
        Caption = 'Motorista : ______________________________________________'
      end
      object RLDraw2: TRLDraw
        Left = 1
        Top = 134
        Width = 717
        Height = 72
      end
      object RLLabel10: TRLLabel
        Left = 9
        Top = 138
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLLabel5: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLMemo2: TRLMemo
        Left = 12
        Top = 162
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 555
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel6: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel7: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel8: TRLLabel
        Left = 12
        Top = 212
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel9: TRLLabel
        Left = 9
        Top = 100
        Width = 42
        Height = 16
        Caption = 'Frota :'
      end
      object RLLabel14: TRLLabel
        Left = 12
        Top = 236
        Width = 31
        Height = 16
        Caption = 'RG :'
      end
      object RLLabel16: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel18: TRLLabel
        Left = 264
        Top = 27
        Width = 135
        Height = 16
        Caption = 'REGISTRO DE SA'#205'DA'
      end
      object RLLabel20: TRLLabel
        Left = 456
        Top = 236
        Width = 39
        Height = 16
        Caption = 'CNH :'
      end
      object RLLabel21: TRLLabel
        Left = 240
        Top = 236
        Width = 38
        Height = 16
        Alignment = taRightJustify
        Caption = 'CPF :'
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 676
        Top = 6
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
      object RLMemo3: TRLMemo
        AlignWithMargins = True
        Left = 8
        Top = 277
        Width = 702
        Height = 49
        Alignment = taJustify
        AutoSize = False
        Behavior = [beSiteExpander]
        Lines.Strings = (
          
            'Declaro, que recebi e conferi, para fins de entrega aos respecti' +
            'vos clientes, todas as mercadorias e volumes constantes das Nota' +
            's Fiscais relacionadas em anexo, assim como todos os documentos ' +
            'fiscais pertinentes, assumindo sobre os mesmos toda e qualquer r' +
            'esponsabilidade pela entrega aos destinat'#225'rios.')
      end
      object RLDraw3: TRLDraw
        AlignWithMargins = True
        Left = 0
        Top = 266
        Width = 717
        Height = 1
      end
      object RLLabel23: TRLLabel
        Left = 46
        Top = 411
        Width = 199
        Height = 16
        Caption = 'Data : _______/ ______/ _______'
      end
      object RLLabel24: TRLLabel
        Left = 297
        Top = 409
        Width = 392
        Height = 16
        Caption = 'Motorista : ______________________________________________'
      end
      object RLDraw4: TRLDraw
        Left = 1
        Top = 134
        Width = 717
        Height = 72
      end
      object RLLabel25: TRLLabel
        Left = 9
        Top = 138
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLLabel26: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLMemo4: TRLMemo
        Left = 12
        Top = 162
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
  end
  object RLReport2: TRLReport
    Left = 79
    Top = 1000
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport2BeforePrint
    object RLBand3: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel27: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel28: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel29: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel30: TRLLabel
        Left = 264
        Top = 27
        Width = 157
        Height = 16
        Caption = 'REGISTRO DE ENTRADA'
      end
      object RLLabel32: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLLabel33: TRLLabel
        Left = 9
        Top = 103
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel34: TRLLabel
        Left = 666
        Top = 27
        Width = 48
        Height = 14
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel35: TRLLabel
        Left = 649
        Top = 4
        Width = 65
        Height = 16
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel31: TRLLabel
        Left = 10
        Top = 130
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLMemo5: TRLMemo
        Left = 13
        Top = 154
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
  end
  object QPatio: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QPatioBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select p.data, p.obs, p.tipo descricao, p.placa, p.nm_transp nm_' +
        'transp, p.nm_motor nm_motor , p.manifesto, p.serie, p.usuario'
      'from tb_portaria p '
      'where motivo = '#39'E'#39' and dt_saida is null'
      'and fl_empresa = :0'
      'order by p.data')
    Left = 224
    Top = 384
    object QPatioDATA: TDateTimeField
      FieldName = 'DATA'
      ReadOnly = True
    end
    object QPatioOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 300
    end
    object QPatioDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
    end
    object QPatioPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 7
    end
    object QPatioNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 50
    end
    object QPatioNM_MOTOR: TStringField
      FieldName = 'NM_MOTOR'
      ReadOnly = True
      Size = 50
    end
    object QPatioMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QPatioSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QPatiousuario: TStringField
      FieldName = 'usuario'
      Size = 30
    end
  end
  object dtsPatio: TDataSource
    DataSet = QPatio
    Left = 280
    Top = 384
  end
  object QEntrada: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QEntradaBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select p.reg,p.data, p.obs, p.tipo descricao, p.placa, p.manifes' +
        'to, p.km, p.usuario nm_usuario,  p.nm_transp,  p.nm_motor, p.ser' +
        'ie , dt_saida, fl_empresa,'
      
        '(select '#39'S'#39' from tb_controle_pallet l where l.manifesto = p.mani' +
        'festo and l.tipo = '#39'E'#39' and rownum=1) pallet, (select distinct do' +
        'c from tb_controle_pallet l where l.manifesto = p.manifesto and ' +
        'tipo = '#39'S'#39' and l.manifesto > 0) doc'
      'from tb_portaria p '
      'where motivo = '#39'E'#39' '
      
        'and data between to_date('#39'18/11/11 00:00'#39','#39'dd/mm/yy hh24:mi'#39') an' +
        'd  to_date('#39'18/11/14 23:59'#39','#39'dd/mm/yy hh24:mi'#39')'
      'and dt_saida is not null'
      'and fl_empresa = :0'
      'order by data')
    Left = 224
    Top = 306
    object QEntradaDATA: TDateTimeField
      FieldName = 'DATA'
      ReadOnly = True
    end
    object QEntradaOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 300
    end
    object QEntradaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
    end
    object QEntradaNM_MOTOR: TStringField
      FieldName = 'NM_MOTOR'
      ReadOnly = True
      Size = 50
    end
    object QEntradaNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 50
    end
    object QEntradaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 7
    end
    object QEntradaMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QEntradaKM: TBCDField
      FieldName = 'KM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QEntradaNM_USUARIO: TStringField
      FieldName = 'NM_USUARIO'
      ReadOnly = True
      Size = 50
    end
    object QEntradaDT_SAIDA: TDateTimeField
      FieldName = 'DT_SAIDA'
      ReadOnly = True
    end
    object QEntradaREG: TBCDField
      FieldName = 'REG'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QEntradaPALLET: TStringField
      FieldName = 'PALLET'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QEntradaSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QEntradaDOC: TStringField
      FieldName = 'DOC'
      ReadOnly = True
      Size = 1
    end
    object QEntradafl_empresa: TBCDField
      FieldName = 'fl_empresa'
    end
  end
  object QSaida: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QSaidaBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select p.reg,p.data, p.obs, p.tipo descricao, p.placa, p.manifes' +
        'to, p.km, p.usuario nm_usuario,  p.nm_transp,  p.nm_motor, p.ser' +
        'ie, fl_empresa,'
      
        '(select '#39'S'#39' from tb_controle_pallet l where l.manifesto = p.mani' +
        'festo and l.tipo = '#39'E'#39' and rownum=1) pallet , p.portal, p.retorn' +
        'o_portal'
      'from tb_portaria p '
      'where motivo = '#39'S'#39' '
      
        'and data between to_date('#39'01/09/15 00:00'#39','#39'dd/mm/yy hh24:mi'#39') an' +
        'd  to_date('#39'24/09/15 23:59'#39','#39'dd/mm/yy hh24:mi'#39')'
      'and fl_empresa = :0'
      'order by data')
    Left = 220
    Top = 238
    object QSaidaDATA: TDateTimeField
      FieldName = 'DATA'
      ReadOnly = True
    end
    object QSaidaOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 300
    end
    object QSaidaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
    end
    object QSaidaNM_MOTOR: TStringField
      FieldName = 'NM_MOTOR'
      ReadOnly = True
      Size = 50
    end
    object QSaidaNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 50
    end
    object QSaidaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 7
    end
    object QSaidaMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QSaidaKM: TBCDField
      FieldName = 'KM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QSaidaNM_USUARIO: TStringField
      FieldName = 'NM_USUARIO'
      ReadOnly = True
      Size = 50
    end
    object QSaidaREG: TBCDField
      FieldName = 'REG'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QSaidaPALLET: TStringField
      FieldName = 'PALLET'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QSaidaSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QSaidaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
      Size = 0
    end
    object QSaidaPORTAL: TStringField
      FieldName = 'PORTAL'
      Size = 1
    end
    object QSaidaRETORNO_PORTAL: TStringField
      FieldName = 'RETORNO_PORTAL'
      Size = 300
    end
  end
  object dtsEntrada: TDataSource
    DataSet = QEntrada
    Left = 276
    Top = 308
  end
  object dtsSaida: TDataSource
    DataSet = QSaida
    Left = 276
    Top = 240
  end
  object Qmovimento: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select p.data, p.obs, p.tipo descricao, p.placa, p.manifesto,p.k' +
        'm, p.usuario nm_usuario, p.motivo, p.nm_transp nm_transp, p.nm_m' +
        'otor nm_motor, p.serie,case when motivo = '#39'E'#39' then p.dt_saida el' +
        'se p.data end dt_saida,case when motivo = '#39'E'#39' then round((p.dt_s' +
        'aida - p.data)*1440,2)  else 0 end tempo, case when motivo = '#39'E'#39 +
        ' then j.hora  else '#39#39' end janela  '
      
        'from tb_portaria p left join tb_janela_hora j on j.transportador' +
        ' = p.nm_transp'
      
        'where data between to_date('#39'09/01/12 00:00'#39','#39'dd/mm/yy hh24:mi'#39') ' +
        'and  to_date('#39'09/01/14 23:59'#39','#39'dd/mm/yy hh24:mi'#39')'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by p.data, motivo')
    Left = 416
    Top = 246
    object QmovimentoDATA: TDateTimeField
      FieldName = 'DATA'
      ReadOnly = True
    end
    object QmovimentoMOTIVO: TStringField
      FieldName = 'MOTIVO'
      ReadOnly = True
      Size = 1
    end
    object QmovimentoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 7
    end
    object QmovimentoNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 50
    end
    object QmovimentoNM_MOTOR: TStringField
      FieldName = 'NM_MOTOR'
      ReadOnly = True
      Size = 50
    end
    object QmovimentoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
    end
    object QmovimentoOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 300
    end
    object QmovimentoMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QmovimentoKM: TBCDField
      FieldName = 'KM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QmovimentoNM_USUARIO: TStringField
      FieldName = 'NM_USUARIO'
      ReadOnly = True
      Size = 50
    end
    object QmovimentoSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QmovimentoDT_SAIDA: TDateTimeField
      FieldName = 'DT_SAIDA'
    end
    object QmovimentoTEMPO: TBCDField
      FieldName = 'TEMPO'
      Precision = 32
    end
    object QmovimentoJANELA: TStringField
      FieldName = 'JANELA'
      ReadOnly = True
      Size = 5
    end
  end
  object dtsMovim: TDataSource
    DataSet = Qmovimento
    Left = 472
    Top = 244
  end
  object QEnvia: TADOQuery
    Connection = dtmDados.ConRodopar
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT distinct R.RAZSOC nm_emp_dest_ta, nf.notfis nr_nf, nf.qua' +
        'nti nr_quantidade, r.inffat ds_email, r.PERHOR fl_tempo_per'
      'FROM RODIMR, RODCLI R, RODCON N,  RODNFC nf  '
      
        'WHERE RODIMR.CODMAN = :0 AND RODIMR.FILMAN = 1 AND RODIMR.SERMAN' +
        ' = '#39'U'#39' AND RODIMR.TIPDOC = '#39'C'#39' AND RODIMR.FILDOC = 1 AND RODIMR.' +
        'SERDOC = '#39'1'#39
      'and RODIMR.CODDOC = N.CODCON '
      'AND n.CODDES = R.CODCLIFOR AND RODIMR.NOTFIS = Nf.NOTFIS  '
      
        'AND RODIMR.CODDOC=Nf.CODCON  AND RODIMR.SERDOC=Nf.SERCON  AND RO' +
        'DIMR.FILDOC=Nf.CODFIL '
      'and r.inffat is not null'
      'and r.PERHOR is not null')
    Left = 764
    Top = 88
    object QEnviaNM_EMP_DEST_TA: TStringField
      FieldName = 'NM_EMP_DEST_TA'
      Size = 50
    end
    object QEnviaNR_QUANTIDADE: TFloatField
      FieldName = 'NR_QUANTIDADE'
    end
    object QEnviaNR_NF: TStringField
      FieldName = 'NR_NF'
      ReadOnly = True
    end
    object QEnviaFL_TEMPO_PER: TStringField
      FieldName = 'FL_TEMPO_PER'
      ReadOnly = True
      Size = 7
    end
    object QEnviaDS_EMAIL: TStringField
      FieldName = 'DS_EMAIL'
      ReadOnly = True
      Size = 4000
    end
  end
  object SPSaida: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_PORTARIA_ROLDAO_UN'
    Parameters = <
      item
        Name = 'VROMANEIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 640
    Top = 84
  end
  object QPallet: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = '3'
        DataType = ftString
        Size = 1
        Value = ''
      end
      item
        Name = '4'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select distinct xtipo, codean, serie, codman from ('
      
        'SELECT distinct '#39'R'#39' xtipo, r.codean, '#39'0'#39' serie, p.percent codman' +
        ', r.codclifor codpag, r.razsoc cliente'
      
        'FROM cyber.RODCLI R left join klienten@wmsv11 k on (replace(repl' +
        'ace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegriff'
      
        '                    left join auftraege@wmsv11 p on p.id_klient ' +
        '= k.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'R'#39' xtipo, r.codean, '#39'0'#39' serie, p.percent codman' +
        ', r.codclifor codpag, r.razsoc cliente'
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'M'#39' xtipo, r.codean, m.serie serie, m.manifesto ' +
        'codman, r.codclifor codpag, r.razsoc cliente'
      
        'FROM tb_manitem m inner join tb_conhecimento N on m.CODDOC = N.n' +
        'r_conhecimento and m.fildoc = n.fl_empresa'
      
        '                  left join cyber.RODCLI R on N.cod_destinatario' +
        ' = R.CODCLIFOR'
      
        '                  left join cyber.RODCLI C on N.cod_pagador = C.' +
        'CODCLIFOR'
      'WHERE m.filial = :0 and c.codean = '#39'PALLET'#39')'
      
        'left join tb_controle_pallet pp on codman = pp.manifesto and pp.' +
        'filial = :1'
      'where codman = :2'
      'and serie = :3'
      'and xtipo = :4'
      '--and pp.id is not null')
    Left = 704
    Top = 84
    object QPalletCODEAN: TStringField
      FieldName = 'CODEAN'
      ReadOnly = True
      Size = 13
    end
  end
  object ADOQDestinos: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '4'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct xtipo, codcgc, codclifor, serie, xmanifesto from' +
        ' ('
      
        'SELECT distinct '#39'R'#39' xtipo, d.suchbegriff codcgc,  r.codclifor, '#39 +
        '0'#39' serie, percent xmanifesto'
      
        'FROM cyber.RODCLI R left join klienten@wmswebprd k on (replace(r' +
        'eplace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                    left join auftraege@wmswebprd p on p.id_klie' +
        'nt = k.id_klient'
      
        '                    left join kunden@wmswebprd d on d.id_kunde =' +
        ' p.id_kunde_ware  and p.id_klient = d.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'R'#39' xtipo, d.suchbegriff codcgc,  r.codclifor, '#39 +
        '0'#39' serie, percent xmanifesto'
      
        'FROM cyber.RODCLI R left join klienten@wmsv11 k on (replace(repl' +
        'ace(replace(r.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegriff'
      
        '                    left join auftraege@wmsv11 p on p.id_klient ' +
        '= k.id_klient'
      
        '                    left join kunden@wmsv11 d on d.id_kunde = p.' +
        'id_kunde_ware  and p.id_klient = d.id_klient'
      'where r.codean = '#39'PALLET'#39
      'union all'
      
        'SELECT distinct '#39'M'#39' xtipo, r.codcgc,  c.codclifor, m.serie, m.ma' +
        'nifesto xmanifesto'
      
        'FROM tb_manitem m inner join tb_conhecimento N on m.CODDOC = N.n' +
        'r_conhecimento and m.fildoc = n.fl_empresa '
      
        '                  left join cyber.RODCLI R on N.cod_destinatario' +
        ' = R.CODCLIFOR '
      
        '                  left join cyber.RODCLI C on N.cod_pagador = C.' +
        'CODCLIFOR '
      'WHERE m.filial = :0 and c.codean = '#39'PALLET'#39')'
      
        'left join tb_controle_pallet pp on xmanifesto = pp.manifesto and' +
        ' pp.filial = :1'
      'where xmanifesto = :2'
      'and serie = :3'
      'and xtipo = :4'
      '--and pp.id is not null'
      '')
    Left = 824
    Top = 88
    object ADOQDestinosXTIPO: TStringField
      FieldName = 'XTIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object ADOQDestinosCODCGC: TStringField
      FieldName = 'CODCGC'
      ReadOnly = True
      Size = 30
    end
    object ADOQDestinosCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object ADOQDestinosSERIE: TStringField
      FieldName = 'SERIE'
      ReadOnly = True
      Size = 3
    end
    object ADOQDestinosXMANIFESTO: TBCDField
      FieldName = 'XMANIFESTO'
      ReadOnly = True
      Precision = 32
    end
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct p.nm_transp  '
      'from tb_portaria p '
      'order by 1')
    Left = 336
    Top = 84
    object QTranspnm_transp: TStringField
      DisplayWidth = 40
      FieldName = 'nm_transp'
      Size = 40
    end
  end
  object QSMS: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select fnc_cnpj(f.codcgc) codcgc, ('#39'int-'#39' || F_LIMPA_MASCARA(m.p' +
        'laca)) usuario, m.manifesto pedido, a.nr_ctrc, a.serie, a.destin' +
        'o, a.endere, a.numero, a.codcep, nvl(mot.TELCEL,'#39'0000000000'#39') te' +
        'lcel, p.reg'
      
        'from vw_manifesto_ctrc_sim a left join tb_manifesto m on m.manif' +
        'esto = a.nr_manifesto and m.filial = a.fl_empresa and m.serie = ' +
        'a.serie'
      
        '                             left join cyber.rodmot mot on mot.c' +
        'odmot = m.motorista'
      
        '                             left join cyber.rodfil f on f.codfi' +
        'l = a.filial_doc'
      
        '                             left join tb_portaria p on p.manife' +
        'sto = m.manifesto and p.fl_empresa = m.filial and p.serie = m.se' +
        'rie and p.motivo = '#39'S'#39
      
        '                             left join cyber.rodcli tr on m.tran' +
        'sp = tr.codclifor'
      
        '                             left join cyber.RODCMO ag on ag.cod' +
        'cmo = tr.codcmo'
      'where p.portal = '#39'N'#39' and ag.codcmo in(10,36)'
      'and m.manifesto = :0 and m.filial = :1 and m.serie = :2'
      'and fnc_cnpj(f.codcgc) = :3'
      'order by a.nr_ctrc')
    Left = 456
    Top = 82
    object QSMSCODCGC: TStringField
      FieldName = 'CODCGC'
    end
    object QSMSUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
      FixedChar = True
      Size = 13
    end
    object QSMSPEDIDO: TFMTBCDField
      FieldName = 'PEDIDO'
      Precision = 38
      Size = 0
    end
    object QSMSNR_CTRC: TFMTBCDField
      FieldName = 'NR_CTRC'
      Precision = 38
      Size = 4
    end
    object QSMSSERIE: TStringField
      FieldName = 'SERIE'
      Size = 1
    end
    object QSMSDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object QSMSENDERE: TStringField
      FieldName = 'ENDERE'
      Size = 80
    end
    object QSMSNUMERO: TFMTBCDField
      FieldName = 'NUMERO'
      Precision = 38
      Size = 0
    end
    object QSMSCODCEP: TStringField
      FieldName = 'CODCEP'
      Size = 10
    end
    object QSMSTELCEL: TStringField
      FieldName = 'TELCEL'
      ReadOnly = True
      Size = 15
    end
    object QSMSREG: TFMTBCDField
      FieldName = 'REG'
      Precision = 38
      Size = 0
    end
  end
  object QPortal: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct fnc_cnpj(f.codcgc) codcgc, ('#39'int-'#39' || F_LIMPA_MA' +
        'SCARA(m.placa)) usuario'
      
        'from vw_manifesto_ctrc_sim a left join tb_manifesto m on m.manif' +
        'esto = a.nr_manifesto and m.filial = a.fl_empresa and m.serie = ' +
        'a.serie'
      
        '                             left join cyber.rodfil f on f.codfi' +
        'l = a.filial_doc'
      
        '                             left join tb_portaria p on p.manife' +
        'sto = m.manifesto and p.fl_empresa = m.filial and p.serie = m.se' +
        'rie and p.motivo = '#39'S'#39
      
        '                             left join cyber.rodcli tr on m.tran' +
        'sp = tr.codclifor'
      
        '                             left join cyber.RODCMO ag on ag.cod' +
        'cmo = tr.codcmo'
      'where p.portal = '#39'N'#39' and ag.codcmo in(10,36)'
      'and m.manifesto = :0 and m.filial = :1 and m.serie = :2'
      'order by 1')
    Left = 456
    Top = 142
    object QPortalCODCGC: TStringField
      FieldName = 'CODCGC'
      ReadOnly = True
      Size = 4000
    end
    object QPortalUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
      Size = 4000
    end
  end
  object RESTClient2: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 
      'https://app.portaldotransportador.com/api/v1/nfe/CheckoutNfTrans' +
      'porte'
    ContentType = 
      'multipart/form-data; boundary=-------Embt-Boundary--59F4BEE8337F' +
      'DE8D'
    Params = <>
    Left = 560
    Top = 304
  end
  object RESTPortal: TRESTRequest
    AssignedValues = [rvConnectTimeout, rvReadTimeout]
    Client = RESTClient2
    Method = rmPOST
    Params = <
      item
        Kind = pkHTTPHEADER
        Name = 'apikey'
        Value = '78asd4546d4sa687e1d1xzlcknhwyhuWMKPSJDpox8213njdOWnxxipW58547'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'Auth_busca'
        Value = 'bb68093b11f2476491a8558a9a248a70'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'cnpj'
        Value = '03857930000740'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'usuario'
        Value = 'int-EVO7E16'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][nfe]'
        Value = '286216'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][serie]'
        Value = '1'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][nome_cliente]'
        Value = 'PDT COMERCIO DE PE'#199'AS LTDA'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][destino_endereco]'
        Value = 'RUA EDUARDO FRONER , N'#176' 621 , GALPAO 01'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][numero_entrega]'
        Value = '0'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][cep_entrega]'
        Value = '07.243-590'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][celular]'
        Value = '(011)98046-7471'
      end
      item
        Kind = pkREQUESTBODY
        Name = 'dados[0][pedido]'
        Value = '29154'
      end>
    Response = RESTResponse1
    Left = 648
    Top = 304
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'application/json'
    Left = 736
    Top = 304
  end
end
