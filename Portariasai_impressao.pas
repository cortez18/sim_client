unit Portariasai_impressao;

interface

uses
  Winapi.Windows, RLReport, System.Classes, Vcl.Controls, Vcl.Forms;

type
  TfrmPortariasai_impressao = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel19: TRLLabel;
    RLSystemInfo3: TRLSystemInfo;
    RLMemo1: TRLMemo;
    RLDraw1: TRLDraw;
    RLLabel13: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw2: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel5: TRLLabel;
    RLMemo2: TRLMemo;
    RLBand2: TRLBand;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLMemo3: TRLMemo;
    RLDraw3: TRLDraw;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLDraw4: TRLDraw;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLMemo4: TRLMemo;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public

  end;

var
  frmPortariasai_impressao: TfrmPortariasai_impressao;

implementation

{$R *.dfm}

uses Dados, Portariasai;

procedure TfrmPortariasai_impressao.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel1.Caption := 'INTECOM';
  dtmdados.WQuery1.close;
  RLLabel2.Caption := 'Transportadora : ' + frmPortariaSai.edTransp.Text;
  RLLabel12.Caption := 'Placa : ' + frmPortariaSai.edPlaca.Text;
  RLLabel5.Caption := 'KM : ' + frmPortariaSai.edKM.Text;
  RLLabel4.Caption := 'Frota : ' + frmPortariaSai.edFrota.Text;
  RLMemo2.Lines.add(frmPortariaSai.Memo1.Text);
  RLLabel3.Caption := 'Motorista : ' + frmPortariaSai.edMotor.Text;
  RLLabel11.Caption := 'RG : ' + frmPortariaSai.edRG.Text;
  RLLabel19.Caption := 'CPF : ' + frmPortariaSai.edCfp.Text;
  RLLabel17.Caption := 'CNH : ' + frmPortariaSai.edCNH.Text;
  if frmPortariaSai.dbgManifesto.Visible = false then
    RLLabel15.Caption := 'REGISTRO DE SA�DA - ' + frmPortariaSai.edManifesto.Text
  else
    RLLabel15.Caption := 'REGISTRO DE SA�DA - ' + self.caption;
  // segunda p�gina
  RLLabel6.Caption := RLLabel1.Caption;
  RLLabel7.Caption := RLLabel2.Caption;
  RLLabel16.Caption := RLLabel12.Caption;
  RLLabel26.Caption := RLLabel5.Caption;
  RLLabel9.Caption := RLLabel4.Caption;
  RLMemo4.Lines.add(frmPortariaSai.Memo1.Text);
  RLLabel8.Caption := RLLabel3.Caption;
  RLLabel14.Caption := RLLabel11.Caption;
  RLLabel21.Caption := RLLabel19.Caption;
  RLLabel20.Caption := RLLabel17.Caption;
  RLLabel18.Caption := RLLabel15.Caption;
end;

end.
