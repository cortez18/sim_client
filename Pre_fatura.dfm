object frmPre_fatura: TfrmPre_fatura
  Left = 0
  Top = 0
  Caption = 'Pre Fatura '
  ClientHeight = 531
  ClientWidth = 565
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 101
    Top = 8
    Width = 116
    Height = 13
    Caption = 'Data Inicial da Quinzena'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 499
    Width = 349
    Height = 25
    Progress = 0
  end
  object Label11: TLabel
    Left = 236
    Top = 8
    Width = 111
    Height = 13
    Caption = 'Data Final da Quinzena'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object JvDBGrid2: TJvDBGrid
    Left = 1
    Top = 54
    Width = 560
    Height = 439
    DataSource = dtsFornecedor
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = JvDBGrid2CellClick
    OnDrawColumnCell = JvDBGrid2DrawColumnCell
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'seleciona'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBtnFace
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 21
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'codigo'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'transportador'
        Title.Caption = 'Transportador'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cnpj'
        Title.Caption = 'CNPJ'
        Visible = True
      end>
  end
  object dtInicial: TJvDateEdit
    Left = 101
    Top = 27
    Width = 89
    Height = 21
    ShowNullDate = False
    YearDigits = dyTwo
    TabOrder = 0
  end
  object btRelatorio: TBitBtn
    Left = 376
    Top = 16
    Width = 75
    Height = 25
    Caption = 'OK'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btRelatorioClick
  end
  object Panel1: TPanel
    Left = 116
    Top = 188
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    Visible = False
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 10
    Top = 499
    Width = 188
    Height = 25
    DataSource = dtsFornecedor
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 5
  end
  object cbT: TCheckBox
    Left = 20
    Top = 31
    Width = 15
    Height = 17
    Checked = True
    State = cbChecked
    TabOrder = 7
    OnClick = cbTClick
  end
  object dtFinal: TJvDateEdit
    Left = 236
    Top = 27
    Width = 89
    Height = 21
    ShowNullDate = False
    YearDigits = dyTwo
    TabOrder = 1
  end
  object BitBtn1: TBitBtn
    Left = 470
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Processar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF006D6D6D63585858BF515151BF5252
      5263FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF006F6F6F296A6A6A0E7A7A7A02818181EABDBDBDFFB2B2B2FF5B5B
      5BEA5A5A5A025C5C5C0E4E4E4E29FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008181819B6F6F6FFD646464E776767619838383E7CBCBCBFFC7C7C7FF6262
      62E75A5A5A19585858E74E4E4EFD4949499BFFFFFF00FFFFFF00FFFFFF00A4A4
      A47BBCBCBCFFDEDEDEFFA6A6A6FF838383F4858585FEC4C4C4FFC2C2C2FF6D6D
      6DFE6E6E6EF4A6A6A6FFD2D2D2FF808080FF5252527BFFFFFF00FFFFFF00ABAB
      AB7DA6A6A6FED5D5D5FFC5C5C5FFCBCBCBFFD1D1D1FFC9C9C9FFC7C7C7FFCCCC
      CCFFC5C5C5FFBDBDBDFFCBCBCBFF6E6E6EFE6767677DFFFFFF00FFFFFF00FFFF
      FF00ACACAC85C5C5C5FFC1C1C1FFC5C5C5FFC7C7C7FFAAAAAAFFA7A7A7FFC1C1
      C1FFBEBEBEFFB5B5B5FFAAAAAAFF69696985FFFFFF00FFFFFF00A3A3A3CD8F8F
      8FE3A0A0A0EECFCFCFFFC6C6C6FFCCCCCCFF9E9E9EC699999944949494448F8F
      8FC6C1C1C1FFBCBCBCFFB9B9B9FF646464EE585858E3535353CDBFBFBFFDE2E2
      E2FFD2D2D2FFC6C6C6FFCDCDCDFFB1B1B1FF93939344FFFFFF00FFFFFF009595
      9544A8A8A8FFC2C2C2FFB7B7B7FFC0C0C0FFD2D2D2FF616161FDC4C4C4FDE9E9
      E9FFD6D6D6FFC9C9C9FFCECECEFFA5A5A5FF84848444FFFFFF00FFFFFF009A9A
      9A44ACACACFFC4C4C4FFBABABAFFC6C6C6FFDDDDDDFF6B6B6BFDC8C8C8CDC4C4
      C4E3C0C0C0EED8D8D8FFCDCDCDFFBCBCBCFF828282C6777777447E7E7E448F8F
      8FC6C3C3C3FFC2C2C2FFCDCDCDFF8C8C8CEE878787E3838383CDFFFFFF00FFFF
      FF00C5C5C585D4D4D4FFCCCCCCFFC9C9C9FFBABABAFF9C9C9CFFA1A1A1FFC2C2
      C2FFC6C6C6FFC1C1C1FFB7B7B7FF89898985FFFFFF00FFFFFF00FFFFFF00CACA
      CA7DC4C4C4FEDCDCDCFFD4D4D4FFD9D9D9FFDBDBDBFFD6D6D6FFD4D4D4FFD9D9
      D9FFD2D2D2FFCBCBCBFFC8C8C8FF797979FE7171717DFFFFFF00FFFFFF00D0D0
      D07BDCDCDCFFEDEDEDFFDBDBDBFFC2C2C2F4BEBEBEFED6D6D6FFD4D4D4FFB0B0
      B0FEACACACF4CBCBCBFFE7E7E7FFB7B7B7FF8B8B8B7BFFFFFF00FFFFFF00FFFF
      FF00D1D1D19BCECECEFDCACACAE7C6C6C619C2C2C2E7DEDEDEFFDDDDDDFFB2B2
      B2E7B1B1B119ACACACE7A7A7A7FDA3A3A39BFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00D1D1D129CECECE0ECBCBCB02C7C7C7EAE5E5E5FFE4E4E4FFACAC
      ACEAB6B6B602B2B2B20EADADAD29FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CBCBCB63C7C7C7BFC4C4C4BFBFBF
      BF63FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 3
    OnClick = BitBtn1Click
  end
  object QFornecedor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct v.transp, f.razsoc nm_fornecedor, nvl(f.codcgc, ' +
        'f.codcpf) nr_cnpj_cpf'
      
        'from vw_custo_transp_sim v left join cyber.rodcli f on v.transp ' +
        '= f.codclifor left join cyber.RODCMO t on t.codcmo = f.codcmo'
      
        'where v.data between to_date('#39'01/01/2020 00:00:00'#39','#39'dd/mm/yyyy h' +
        'h24:mi:ss'#39') and to_date('#39'31/01/2020 23:59:59'#39','#39'dd/mm/yyyy hh24:m' +
        'i:ss'#39')'
      'and status <> '#39'P'#39
      'and t.codcmo = 10'
      'and transp  not in (1018, 1145)'
      'order by 1')
    Left = 257
    Top = 100
    object QFornecedorTRANSP: TBCDField
      FieldName = 'TRANSP'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QFornecedorNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      ReadOnly = True
      Size = 50
    end
    object QFornecedorNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
  end
  object dtsFornecedor: TDataSource
    DataSet = mdFatura
    Left = 325
    Top = 100
  end
  object mdFatura: TJvMemoryData
    FieldDefs = <
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end
      item
        Name = 'destino'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cod_ta'
        DataType = ftInteger
      end
      item
        Name = 'cliredes'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'localentrega'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rota'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'nf'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'pedido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'volume'
        DataType = ftString
        Size = 500
      end>
    Left = 272
    Top = 52
    object mdFaturaseleciona: TStringField
      FieldName = 'seleciona'
      Size = 1
    end
    object mdFaturacodigo: TIntegerField
      FieldName = 'codigo'
    end
    object mdFaturatransportador: TStringField
      FieldName = 'transportador'
      Size = 50
    end
    object mdFaturacnpj: TStringField
      FieldName = 'cnpj'
    end
  end
  object SP_PRE: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_PRE_FATURA_AGRE_SIM'
    Parameters = <>
    Left = 412
    Top = 96
  end
end
