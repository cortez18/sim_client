unit ConsPortaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB,
  DateUtils,
  DBCtrls, JvDBControls, ADODB, JvExStdCtrls, JvCombobox, JvDriveCtrls,
  JvListBox, JvMaskEdit, Gauges, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  ComCtrls,
  JvBaseEdits, RLReport, Rest.types, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, JvDBUltimGrid, System.JSON;

type
  TfrmConsPortaria = class(TForm)
    QPatio: TADOQuery;
    dtsPatio: TDataSource;
    Panel1: TPanel;
    QEntrada: TADOQuery;
    QSaida: TADOQuery;
    dtsEntrada: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    dtInicialS: TJvDateEdit;
    dtFinalS: TJvDateEdit;
    btSaida: TBitBtn;
    btnEntrada: TBitBtn;
    dtsSaida: TDataSource;
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    QEntradaDATA: TDateTimeField;
    QEntradaOBS: TStringField;
    QEntradaDESCRICAO: TStringField;
    QEntradaNM_MOTOR: TStringField;
    QSaidaDATA: TDateTimeField;
    QSaidaOBS: TStringField;
    QSaidaDESCRICAO: TStringField;
    QSaidaNM_MOTOR: TStringField;
    QSaidaNM_TRANSP: TStringField;
    QEntradaNM_TRANSP: TStringField;
    QEntradaPLACA: TStringField;
    QSaidaPLACA: TStringField;
    QPatioDATA: TDateTimeField;
    QPatioOBS: TStringField;
    QPatioDESCRICAO: TStringField;
    QPatioPLACA: TStringField;
    QPatioNM_TRANSP: TStringField;
    QPatioNM_MOTOR: TStringField;
    QSaidaMANIFESTO: TBCDField;
    QSaidaKM: TBCDField;
    QSaidaNM_USUARIO: TStringField;
    QEntradaMANIFESTO: TBCDField;
    QEntradaKM: TBCDField;
    QEntradaNM_USUARIO: TStringField;
    TabSheet4: TTabSheet;
    Qmovimento: TADOQuery;
    QmovimentoDATA: TDateTimeField;
    QmovimentoOBS: TStringField;
    QmovimentoPLACA: TStringField;
    QmovimentoMANIFESTO: TBCDField;
    QmovimentoKM: TBCDField;
    QmovimentoNM_USUARIO: TStringField;
    QmovimentoMOTIVO: TStringField;
    QmovimentoNM_TRANSP: TStringField;
    QmovimentoNM_MOTOR: TStringField;
    Label3: TLabel;
    dtMovimI: TJvDateEdit;
    Label4: TLabel;
    dtMovimF: TJvDateEdit;
    BitBtn2: TBitBtn;
    JvDBGrid4: TJvDBGrid;
    Label5: TLabel;
    edPlaca: TJvMaskEdit;
    dtsMovim: TDataSource;
    btnImprimir: TBitBtn;
    Label8: TLabel;
    JvDirectoryListBox1: TJvDirectoryListBox;
    JvDriveCombo1: TJvDriveCombo;
    QmovimentoDESCRICAO: TStringField;
    QEntradaDT_SAIDA: TDateTimeField;
    Gauge1: TGauge;
    QSaidaREG: TBCDField;
    QEntradaREG: TBCDField;
    btnemail: TBitBtn;
    QEnvia: TADOQuery;
    QEnviaNM_EMP_DEST_TA: TStringField;
    QEnviaNR_QUANTIDADE: TFloatField;
    QEnviaNR_NF: TStringField;
    QEnviaFL_TEMPO_PER: TStringField;
    QEnviaDS_EMAIL: TStringField;
    SPSaida: TADOStoredProc;
    JvDBNavigator1: TJvDBNavigator;
    JvDBNavigator2: TJvDBNavigator;
    JvDBNavigator3: TJvDBNavigator;
    JvDBNavigator4: TJvDBNavigator;
    btnSMS: TBitBtn;
    RGFilial: TRadioGroup;
    QEntradaPALLET: TStringField;
    btnPallet: TBitBtn;
    QPallet: TADOQuery;
    QPalletCODEAN: TStringField;
    QSaidaPALLET: TStringField;
    QPatioMANIFESTO: TBCDField;
    btnPalletS: TBitBtn;
    QSaidaSERIE: TStringField;
    QEntradaSERIE: TStringField;
    QmovimentoSERIE: TStringField;
    QPatioSERIE: TStringField;
    ADOQDestinos: TADOQuery;
    cbTransp: TComboBox;
    Label6: TLabel;
    QTransp: TADOQuery;
    QTranspnm_transp: TStringField;
    Label7: TLabel;
    edManifesto: TJvCalcEdit;
    QmovimentoDT_SAIDA: TDateTimeField;
    QmovimentoTEMPO: TBCDField;
    ADOQDestinosXTIPO: TStringField;
    ADOQDestinosCODCGC: TStringField;
    ADOQDestinosCODCLIFOR: TBCDField;
    ADOQDestinosSERIE: TStringField;
    ADOQDestinosXMANIFESTO: TBCDField;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel17: TRLLabel;
    RLLabel19: TRLLabel;
    RLSystemInfo3: TRLSystemInfo;
    RLMemo1: TRLMemo;
    RLDraw1: TRLDraw;
    RLLabel13: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw2: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel5: TRLLabel;
    RLMemo2: TRLMemo;
    RLBand2: TRLBand;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLMemo3: TRLMemo;
    RLDraw3: TRLDraw;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLDraw4: TRLDraw;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLMemo4: TRLMemo;
    btnRegistro: TBitBtn;
    QEntradaDOC: TStringField;
    QmovimentoJANELA: TStringField;
    QSaidaFL_EMPRESA: TBCDField;
    QEntradafl_empresa: TBCDField;
    RLReport2: TRLReport;
    RLBand3: TRLBand;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    RLLabel31: TRLLabel;
    RLMemo5: TRLMemo;
    QPatiousuario: TStringField;
    QSMS: TADOQuery;
    QSMSCODCGC: TStringField;
    QSMSUSUARIO: TStringField;
    QSMSPEDIDO: TFMTBCDField;
    QSMSNR_CTRC: TFMTBCDField;
    QSMSSERIE: TStringField;
    QSMSDESTINO: TStringField;
    QSMSENDERE: TStringField;
    QSMSNUMERO: TFMTBCDField;
    QSMSCODCEP: TStringField;
    QSMSTELCEL: TStringField;
    QSMSREG: TFMTBCDField;
    QSaidaPORTAL: TStringField;
    QSaidaRETORNO_PORTAL: TStringField;
    QPortal: TADOQuery;
    QPortalCODCGC: TStringField;
    QPortalUSUARIO: TStringField;
    BitBtn1: TBitBtn;
    RESTClient2: TRESTClient;
    RESTPortal: TRESTRequest;
    RESTResponse1: TRESTResponse;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSaidaClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure btnEntradaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dtFinalExit(Sender: TObject);
    procedure dtFinalSExit(Sender: TObject);
    procedure dtMovimFExit(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnemailClick(Sender: TObject);
    procedure enviar_email;
    procedure QSaidaBeforeOpen(DataSet: TDataSet);
    procedure QEntradaBeforeOpen(DataSet: TDataSet);
    procedure QPatioBeforeOpen(DataSet: TDataSet);
    procedure btnSMSClick(Sender: TObject);
    procedure JvDBGrid3TitleClick(Column: TColumn);
    procedure JvDBGrid2TitleClick(Column: TColumn);
    procedure JvDBGrid4TitleClick(Column: TColumn);
    procedure btnPalletClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnPalletSClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure btnRegistroClick(Sender: TObject);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure JvDBGrid2DblClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure RLReport2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure JvDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsPortaria: TfrmConsPortaria;

implementation

uses Dados, Menu, funcoes, uMensagensSms, Controle_pallet;

{$R *.dfm}

procedure TfrmConsPortaria.btSaidaClick(Sender: TObject);
begin
  QSaida.close;
  QSaida.SQL[4] := 'and data between to_date(''' + dtInicialS.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinalS.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QSaida.open;
end;

procedure TfrmConsPortaria.dtFinalExit(Sender: TObject);
begin
  if dtFinal.date < dtInicial.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
end;

procedure TfrmConsPortaria.dtFinalSExit(Sender: TObject);
begin
  if dtFinalS.date < dtInicialS.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
end;

procedure TfrmConsPortaria.dtMovimFExit(Sender: TObject);
begin
  if dtMovimF.date < dtMovimI.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
end;

procedure TfrmConsPortaria.BitBtn1Click(Sender: TObject);
var jv: TJSONValue;
    jsonObj: TJSONObject;
    men, retorno, r : String;
    i, f, a : Integer;
begin
    QPortal.Close;
    QPortal.Parameters[0].value := 111631;
    QPortal.Parameters[1].value := 1;
    QPortal.Parameters[2].value := '1';
    QPortal.open;
    if QPortal.RecordCount > 0 then
    begin
      begin
        try
          while not QPortal.eof do
          begin
            try
              RESTPortal.Params.Clear;
              RESTPortal.Params.Add;
              RESTPortal.Params[0].Kind := pkHTTPHEADER;
              RESTPortal.Params[0].name := 'apikey';
              RESTPortal.Params[0].Value := '78asd4546d4sa687e1d1xzlcknhwyhuWMKPSJDpox8213njdOWnxxipW58547';

              RESTPortal.Params.Add;
              RESTPortal.Params[1].Kind := pkGETorPOST;
              RESTPortal.Params[1].name := 'Auth_busca';
              RESTPortal.Params[1].Value := 'bb68093b11f2476491a8558a9a248a70';
              RESTPortal.Params.Add;
              RESTPortal.Params[2].Kind := pkGETorPOST;
              RESTPortal.Params[2].name := 'cnpj';
              RESTPortal.Params[2].Value := QPortalCODCGC.AsString;

              RESTPortal.Params.Add;
              RESTPortal.Params[3].Kind := pkGETorPOST;
              RESTPortal.Params[3].name := 'usuario';
              RESTPortal.Params[3].Value := QPortalUSUARIO.AsString;
              a := 3;
              //Enviar para Portal do Transportador para baixa no celular
              QSMS.close;
              QSMS.Parameters[0].value := 111631;
              QSMS.Parameters[1].value := 1;
              QSMS.Parameters[2].value := '1';
              QSMS.Parameters[3].value := QPortalCODCGC.AsString;
              QSMS.open;

              while not QSMS.eof do
              begin
                r := inttostr(i);
                a := a + 1;
                RESTPortal.Params.Add;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][nfe]';
                RESTPortal.Params[a].Value := QSMSNR_CTRC.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][serie]';
                RESTPortal.Params[a].Value := '99';//QSMSSERIE.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][nome_cliente]';
                RESTPortal.Params[a].Value := QSMSDESTINO.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][destino_endereco]';
                RESTPortal.Params[a].Value := QSMSENDERE.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][numero_entrega]';
                RESTPortal.Params[a].Value := QSMSNUMERO.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][cep_entrega]';
                RESTPortal.Params[a].Value := QSMSCODCEP.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][celular]';
                RESTPortal.Params[a].Value := QSMSTELCEL.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkGETorPOST;
                RESTPortal.Params[a].name := 'dados['+r+'][pedido]';
                RESTPortal.Params[a].Value := QSMSPEDIDO.AsString;
                QSms.Next;
                i := i + 1;
              end;
              RESTPortal.Execute;
              i := 0;
              if RestResponse1.StatusCode > 206 then
              begin
                jsonObj := TJSONObject.ParseJSONValue(RESTResponse1.Content) as TJSONObject;
                retorno := jsonObj.GetValue('success').Value;
                men     := jsonObj.GetValue('message').Value;

                dtmDados.IQuery1.close;
                dtmDados.IQuery1.sql.clear;
                dtmDados.IQuery1.sql.add
                  ('update tb_portaria set retorno_portal = :0 where reg = :1 ');
                dtmDados.IQuery1.Parameters[0].value := men;
                dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                dtmDados.IQuery1.ExecSQL;
                dtmDados.IQuery1.close;
                showmessage('Erro : ' + men);
              end
              else
              begin
                retorno := RESTResponse1.Content;
                i := pos('message":"', retorno);
                f := pos('"}', retorno);
                men := copy(retorno,i+length('message":"'), f-(i+length('message":"') ));

                dtmDados.IQuery1.close;
                dtmDados.IQuery1.sql.clear;
                dtmDados.IQuery1.sql.add
                  ('update tb_portaria set portal = ''S'', retorno_portal = :0 where reg = :1 ');
                dtmDados.IQuery1.Parameters[0].value := men;
                dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                dtmDados.IQuery1.ExecSQL;
                dtmDados.IQuery1.close;
                showmessage(men);
              end;
            except
              on e: Exception do
              begin
                showmessage('Erro: ' + e.Message + ' Retorno: ' + RESTResponse1.Content);
              end
            end;
            QPortal.Next;
          end;
        finally

        end;
      end;
    end;
end;

procedure TfrmConsPortaria.BitBtn2Click(Sender: TObject);
begin
  Qmovimento.close;
  if copy(dtMovimI.Text, 1, 2) <> '  ' then
    Qmovimento.SQL[2] := 'Where p.data between to_date(''' + dtMovimI.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtMovimF.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    Qmovimento.SQL[2] := 'Where p.data is not null';
  if edPlaca.Text <> '' then
    Qmovimento.SQL[3] := 'and p.placa = ' + #39 + edPlaca.Text + #39
  else
    Qmovimento.SQL[3] := '';

  if cbTransp.Text <> '' then
    Qmovimento.SQL[5] := 'and p.nm_transp = ' + #39 + cbTransp.Text + #39
  else
    Qmovimento.SQL[5] := '';

  if RGFilial.ItemIndex = 0 then
    Qmovimento.SQL[6] := 'and p.fl_empresa = ' + QuotedStr(InttoStr(GLBFilial))
  else
    Qmovimento.SQL[6] := '';

  if edManifesto.value > 0 then
    Qmovimento.SQL[7] := 'and p.manifesto = ' +
      QuotedStr(apcarac(edManifesto.Text))
  else
    Qmovimento.SQL[7] := '';

  Qmovimento.open;
end;

procedure TfrmConsPortaria.btnemailClick(Sender: TObject);
begin
  // enviar e-mail ao destinat�rio
  if (QSaidaDESCRICAO.value = 'Manifesto') then
  begin
    SPSaida.Parameters[0].value := QSaidaMANIFESTO.AsInteger;
    SPSaida.Parameters[1].value := '1';
    SPSaida.ExecProc;
    ShowMessage('Enviado');
  end;
end;

procedure TfrmConsPortaria.btnEntradaClick(Sender: TObject);
begin
  QEntrada.close;
  QEntrada.SQL[4] := 'and data between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QEntrada.open;
end;

procedure TfrmConsPortaria.btnImprimirClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  sarquivo := JvDirectoryListBox1.Directory + '\MovimentoPortaria' +
    glbuser + '.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid4.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid4.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  Qmovimento.First;
  while not Qmovimento.Eof do
  begin
    for i := 0 to JvDBGrid4.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + Qmovimento.fieldbyname
        (JvDBGrid4.Columns[i].FieldName).AsString + '</th>');
    Qmovimento.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  ShowMessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmConsPortaria.btnPalletClick(Sender: TObject);
var
  reg: integer;
begin
  if not dtmDados.PodeInserir('frmPortariaEnt') then
    exit;

  if QEntradaPALLET.IsNull then
  begin
    // verifica se tem pallet a controlar
    { QPallet.close;
      QPallet.Parameters[0].value := QEntradaMANIFESTO.AsInteger;
      QPallet.Parameters[1].value := QEntradaSERIE.value;
      QPallet.Open; }

    QPallet.close;
    QPallet.Parameters[0].value := QEntradafl_empresa.AsInteger;
    QPallet.Parameters[1].value := QEntradafl_empresa.AsInteger;
    QPallet.Parameters[2].value := QEntradaMANIFESTO.AsInteger;
    QPallet.Parameters[3].value := QEntradaSERIE.AsInteger;
    QPallet.Parameters[4].value := QEntradaDOC.value;
    QPallet.open;

    // verifica os destinos para carregar o combobox
    ADOQDestinos.close;
    ADOQDestinos.Parameters[0].value := QEntradafl_empresa.AsInteger;
    ADOQDestinos.Parameters[1].value := QEntradafl_empresa.AsInteger;
    ADOQDestinos.Parameters[2].value := QEntradaMANIFESTO.AsInteger;
    ADOQDestinos.Parameters[3].value := QEntradaSERIE.value;
    ADOQDestinos.Parameters[4].value := QEntradaDOC.value;
    ADOQDestinos.open;

    if not QPalletCODEAN.IsNull then
    begin
      frmMenu.Tag := 1;
      try
        Application.CreateForm(TfrmControle_pallet, frmControle_pallet);
        while not ADOQDestinos.Eof do
        begin
          frmControle_pallet.cbcte.Items.Add(ADOQDestinosCODCGC.AsString);
          ADOQDestinos.Next;
        end;
        frmControle_pallet.ShowModal;
      finally
        frmControle_pallet.Free;
      end;
      frmMenu.Tag := 0;
    end;
    if self.Tag = 0 then
    begin
      reg := QEntradaMANIFESTO.AsInteger;
      QEntrada.close;
      QEntrada.SQL[4] := 'and data between to_date(''' + dtInicial.Text +
        ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
        ' 23:59'',''dd/mm/yy hh24:mi'')';
      QEntrada.open;
      QEntrada.locate('manifesto', reg, []);
    end;
  end;
end;

procedure TfrmConsPortaria.btnPalletSClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    exit;

  if (QSaidaDESCRICAO.value = 'Manifesto') then
  begin
    // verifica se tem pallet a controlar
    QPallet.close;
    QPallet.Parameters[0].value := QSaidaFL_EMPRESA.AsInteger;
    QPallet.Parameters[1].value := QSaidaFL_EMPRESA.AsInteger;
    QPallet.Parameters[2].value := QSaidaMANIFESTO.AsInteger;
    QPallet.Parameters[3].value := QSaidaSERIE.AsString;
    QPallet.Parameters[4].value := 'M';
    QPallet.open;

    // procura os CT-es do Manifesto selecionado (Implementa��o Hugo)
    ADOQDestinos.close;
    ADOQDestinos.Parameters[0].value := QSaidaFL_EMPRESA.AsInteger;
    ADOQDestinos.Parameters[1].value := QSaidaFL_EMPRESA.AsInteger;
    ADOQDestinos.Parameters[2].value := QSaidaMANIFESTO.AsInteger;
    ADOQDestinos.Parameters[3].value := QSaidaSERIE.AsString;
    ADOQDestinos.Parameters[4].value := 'M';
    ADOQDestinos.open;
  end
  else if (QSaidaDESCRICAO.value = 'Romaneio') then
  begin
    // verifica se tem pallet a controlar
    QPallet.close;
    QPallet.Parameters[0].value := QSaidaFL_EMPRESA.AsInteger;
    QPallet.Parameters[1].value := QSaidaFL_EMPRESA.AsInteger;
    QPallet.Parameters[2].value := QSaidaMANIFESTO.AsInteger;
    QPallet.Parameters[3].value := '0';
    QPallet.Parameters[4].value := 'R';
    QPallet.open;

    // procura os CT-es do Manifesto selecionado (Implementa��o Hugo)
    ADOQDestinos.close;
    ADOQDestinos.Parameters[0].value := QSaidaFL_EMPRESA.AsInteger;
    ADOQDestinos.Parameters[1].value := QSaidaFL_EMPRESA.AsInteger;
    ADOQDestinos.Parameters[2].value := QSaidaMANIFESTO.AsInteger;
    ADOQDestinos.Parameters[3].value := '0';
    ADOQDestinos.Parameters[4].value := 'R';
    ADOQDestinos.open;
  end;

  if not QPallet.IsEmpty then
  begin
    frmMenu.Tag := 3;
    try
      Application.CreateForm(TfrmControle_pallet, frmControle_pallet);
      while not ADOQDestinos.Eof do
      begin
        frmControle_pallet.cbcte.Items.Add(ADOQDestinosCODCGC.AsString);
        ADOQDestinos.Next;
      end;
      frmControle_pallet.ShowModal;
    finally
      frmControle_pallet.Free;
    end;
    frmMenu.Tag := 0;
  end;
end;

procedure TfrmConsPortaria.btnRegistroClick(Sender: TObject);
begin
  RLReport1.Preview;
end;

procedure TfrmConsPortaria.btnSMSClick(Sender: TObject);
var jv: TJSONValue;
    jsonObj: TJSONObject;
    men, retorno, r : String;
    i, f, a : Integer;
begin
  while not QSaida.eof do
  begin
    QPortal.Close;
    QPortal.Parameters[0].value := QSaidaMANIFESTO.AsInteger;
    QPortal.Parameters[1].value := glbFilial;
    QPortal.Parameters[2].value := QSaidaSERIE.AsString;
    QPortal.open;
    if QPortal.RecordCount > 0 then
    begin
      begin
        try
          while not QPortal.eof do
          begin
            try
              RESTPortal.Params.Clear;
              RESTPortal.Params.Add;
              RESTPortal.Params[0].Kind := pkHTTPHEADER;
              RESTPortal.Params[0].name := 'apikey';
              RESTPortal.Params[0].Value := '78asd4546d4sa687e1d1xzlcknhwyhuWMKPSJDpox8213njdOWnxxipW58547';

              RESTPortal.Params.Add;
              RESTPortal.Params[1].Kind := pkREQUESTBODY;
              RESTPortal.Params[1].name := 'Auth_busca';
              RESTPortal.Params[1].Value := 'bb68093b11f2476491a8558a9a248a70';
              RESTPortal.Params.Add;
              RESTPortal.Params[2].Kind := pkREQUESTBODY;
              RESTPortal.Params[2].name := 'cnpj';
              RESTPortal.Params[2].Value := QPortalCODCGC.AsString;

              RESTPortal.Params.Add;
              RESTPortal.Params[3].Kind := pkREQUESTBODY;
              RESTPortal.Params[3].name := 'usuario';
              RESTPortal.Params[3].Value := QPortalUSUARIO.AsString;
              a := 3;
              //Enviar para Portal do Transportador para baixa no celular
              QSMS.close;
              QSMS.Parameters[0].value := QSaidaMANIFESTO.AsInteger;
              QSMS.Parameters[1].value := glbFilial;
              QSMS.Parameters[2].value := QSaidaSERIE.AsString;
              QSMS.Parameters[3].value := QPortalCODCGC.AsString;
              QSMS.open;

              while not QSMS.eof do
              begin
                r := inttostr(i);
                a := a + 1;
                RESTPortal.Params.Add;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][nfe]';
                RESTPortal.Params[a].Value := QSMSNR_CTRC.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][serie]';
                RESTPortal.Params[a].Value := QSMSSERIE.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][nome_cliente]';
                RESTPortal.Params[a].Value := QSMSDESTINO.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][destino_endereco]';
                RESTPortal.Params[a].Value := QSMSENDERE.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][numero_entrega]';
                RESTPortal.Params[a].Value := QSMSNUMERO.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][cep_entrega]';
                RESTPortal.Params[a].Value := QSMSCODCEP.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][celular]';
                RESTPortal.Params[a].Value := QSMSTELCEL.AsString;
                RESTPortal.Params.Add;
                a := a + 1;
                RESTPortal.Params[a].Kind := pkREQUESTBODY;
                RESTPortal.Params[a].name := 'dados['+r+'][pedido]';
                RESTPortal.Params[a].Value := QSMSPEDIDO.AsString;
                QSms.Next;
                i := i + 1;
              end;
              RESTPortal.Execute;
              i := 0;
              if RestResponse1.StatusCode > 206 then
              begin
                showmessage(RESTResponse1.Content);
                jsonObj := TJSONObject.ParseJSONValue(RESTResponse1.Content) as TJSONObject;
                retorno := jsonObj.GetValue('success').Value;
                men     := jsonObj.GetValue('message').Value;

                dtmDados.IQuery1.close;
                dtmDados.IQuery1.sql.clear;
                dtmDados.IQuery1.sql.add
                  ('update tb_portaria set retorno_portal = retorno_portal || '' - '' || :0 where reg = :1 ');
                dtmDados.IQuery1.Parameters[0].value := men;
                dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                dtmDados.IQuery1.ExecSQL;
                dtmDados.IQuery1.close;
                showmessage('Erro : ' + men);
              end
              else
              begin
                retorno := RESTResponse1.Content;
                i := pos('message":"', retorno);
                f := pos('"}', retorno);
                men := copy(retorno,i+length('message":"'), f-(i+length('message":"') ));

                dtmDados.IQuery1.close;
                dtmDados.IQuery1.sql.clear;
                dtmDados.IQuery1.sql.add
                  ('update tb_portaria set portal = ''S'', retorno_portal = retorno_portal || '' - '' || :0 where reg = :1 ');
                dtmDados.IQuery1.Parameters[0].value := men;
                dtmDados.IQuery1.Parameters[1].value := QSMSREG.AsInteger;
                dtmDados.IQuery1.ExecSQL;
                dtmDados.IQuery1.close;
                showmessage(men);
              end;
            except
              on e: Exception do
              begin
                showmessage('Erro: ' + e.Message + ' Retorno: ' + RESTResponse1.Content);
              end
            end;
            QPortal.Next;
          end;
        finally

        end;
      end;
    end;
    QSaida.Next;
  end;
end;

procedure TfrmConsPortaria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QPatio.close;
  QEntrada.close;
  QSaida.close;
end;

procedure TfrmConsPortaria.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  if dtmDados.PodeApagar(name) then
  begin
    btnPalletS.Visible := true;
    btnemail.Visible := true;
  end;
end;

procedure TfrmConsPortaria.JvDBGrid1DblClick(Sender: TObject);
begin
  RlReport2.Preview;
end;

procedure TfrmConsPortaria.JvDBGrid2DblClick(Sender: TObject);
var
  cod: string;
begin
  if not dtmDados.PodeAlterar(name) then
    exit;

  if Application.Messagebox('Voc� Deseja Alterar A Data de Entrada ?',
    'Alterar Entrada', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      cod := InputBox('Informe a Data dd/mm/yy hh:mm:ss', 'Data',
        QEntradaDATA.AsString);
      StrToDateTime(cod);
    Except
      cod := EmptyStr;
    end;
    if cod <> '' then
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.Add
        ('update tb_portaria set data = :0 where reg = :1');
      dtmDados.IQuery1.Parameters[0].value := StrToDateTime(cod);
      dtmDados.IQuery1.Parameters[1].value := QEntradaREG.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;
      ShowMessage('Data Alterada');
    end;
    QEntrada.close;
    QEntrada.open;
  end;
end;

procedure TfrmConsPortaria.JvDBGrid2TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', QEntrada.SQL.Text) > 0 then
    QEntrada.SQL.Text := copy(QEntrada.SQL.Text, 1,
      Pos('order by', QEntrada.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    QEntrada.SQL.Text := QEntrada.SQL.Text + ' order by ' + Column.FieldName;
  QEntrada.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid2.Columns.Count - 1 do
    JvDBGrid2.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmConsPortaria.JvDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QSaidaPORTAL.value = 'S') then
  begin
    JvDBGrid3.canvas.Brush.color := clMoneyGreen;
    JvDBGrid3.canvas.font.color := clBlack;
  end;

  JvDBGrid3.DefaultDrawDataCell(Rect, JvDBGrid3.Columns[DataCol].field, State);
end;

procedure TfrmConsPortaria.JvDBGrid3TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', QSaida.SQL.Text) > 0 then
    QSaida.SQL.Text := copy(QSaida.SQL.Text, 1, Pos('order by', QSaida.SQL.Text)
      - 1) + 'order by ' + Column.FieldName
  else
    QSaida.SQL.Text := QSaida.SQL.Text + ' order by ' + Column.FieldName;
  QSaida.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid3.Columns.Count - 1 do
    JvDBGrid3.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmConsPortaria.JvDBGrid4TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', Qmovimento.SQL.Text) > 0 then
    Qmovimento.SQL.Text := copy(Qmovimento.SQL.Text, 1,
      Pos('order by', Qmovimento.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    Qmovimento.SQL.Text := Qmovimento.SQL.Text + ' order by ' +
      Column.FieldName;
  Qmovimento.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid4.Columns.Count - 1 do
    JvDBGrid4.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmConsPortaria.QEntradaBeforeOpen(DataSet: TDataSet);
begin
  QEntrada.Parameters[0].value := GLBFilial;
end;

procedure TfrmConsPortaria.QPatioBeforeOpen(DataSet: TDataSet);
begin
  QPatio.Parameters[0].value := GLBFilial;
end;

procedure TfrmConsPortaria.QSaidaBeforeOpen(DataSet: TDataSet);
begin
  QSaida.Parameters[0].value := GLBFilial;
end;

procedure TfrmConsPortaria.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel1.Caption := 'INTECOM';
  dtmDados.WQuery1.close;
  RLLabel2.Caption := 'Transportadora : ' + QSaidaNM_TRANSP.AsString;
  RLLabel12.Caption := 'Placa : ' + QSaidaPLACA.AsString;
  RLLabel5.Caption := 'KM : ' + QSaidaKM.AsString;
  RLLabel4.Caption := 'Frota : ';
  RLMemo2.Lines.Add(QSaidaOBS.value);
  RLLabel3.Caption := 'Motorista : ' + QSaidaNM_MOTOR.AsString;
  RLLabel11.Caption := 'RG : ';
  RLLabel19.Caption := 'CPF : ';
  RLLabel17.Caption := 'CNH : ';
  RLLabel15.Caption := 'REGISTRO DE SA�DA - ' + QSaidaMANIFESTO.AsString;
  // segunda p�gina
  RLLabel6.Caption := RLLabel1.Caption;
  RLLabel7.Caption := RLLabel1.Caption;
  RLLabel16.Caption := RLLabel12.Caption;
  RLLabel26.Caption := RLLabel5.Caption;
  RLLabel9.Caption := RLLabel4.Caption;
  RLMemo4.Lines.Add(QSaidaOBS.AsString);
  RLLabel8.Caption := RLLabel3.Caption;
  RLLabel14.Caption := RLLabel11.Caption;
  RLLabel21.Caption := RLLabel19.Caption;
  RLLabel20.Caption := RLLabel17.Caption;
  RLLabel18.Caption := RLLabel15.Caption;
end;

procedure TfrmConsPortaria.RLReport2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel27.Caption := 'INTECOM';
  RLLabel28.Caption := 'Transportadora : ' + QPatioNM_TRANSP.AsString;
  RLLabel34.caption := QPatiousuario.AsString;
  RLLabel29.Caption := 'Placa : ' + QPatioPLACA.AsString;
  RLLabel33.Caption := 'Motorista : ' + QPatioNM_MOTOR.AsString;
  RLLabel35.Caption := 'Data : ' + QPatioDATA.AsString;
  RLMemo5.Lines.add(QPatioOBS.AsString);
end;

procedure TfrmConsPortaria.TabSheet1Show(Sender: TObject);
begin
  QPatio.close;
  QPatio.open;
end;

procedure TfrmConsPortaria.TabSheet2Show(Sender: TObject);
begin
  QEntrada.close;
  dtInicial.date := now;
  dtFinal.date := now;
  QEntrada.SQL[4] := 'and data between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QEntrada.open;
end;

procedure TfrmConsPortaria.TabSheet3Show(Sender: TObject);
begin
  QSaida.close;
  dtInicialS.date := now;
  dtFinalS.date := now;
  QSaida.SQL[4] := 'and data between to_date(''' + dtInicialS.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinalS.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QSaida.open;
end;

procedure TfrmConsPortaria.TabSheet4Show(Sender: TObject);
begin
  QTransp.open;
  cbTransp.clear;
  cbTransp.Items.Add('');
  while not QTransp.Eof do
  begin
    cbTransp.Items.Add(QTranspnm_transp.AsString);
    QTransp.Next;
  end;
  QTransp.close;
end;

procedure TfrmConsPortaria.enviar_email;
var
  mail, memo2: TStringList;
  tempo: integer;
  email, email2: string;
  // iRetorno:Integer;
  nPosicao, nContador: integer;
begin
  tempo := StrToInt(apcarac(QEnviaFL_TEMPO_PER.AsString));
  memo2 := TStringList.Create;
  memo2.Add('<HTML><HEAD><TITLE></TITLE>');
  memo2.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo2.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo2.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo2.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo2.Add(
    '       .texto3 {	FONT: 08px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo2.Add('</STYLE>');
  memo2.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo2.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo2.Add('<table border=0><title></title></head><body>');
  memo2.Add('<table border=1>');
  memo2.Add('<tr>');
  memo2.Add(
    '<th colspan=4><img src="http://iwlog.com.br/logo.jpg"><FONT class=texto3></font></th>');
  memo2.Add('</tr><tr>');
  memo2.Add('<th colspan=4><FONT class=titulo1>MANIFESTO DE SA�DA</th></font>');
  memo2.Add('</tr><tr>');
  memo2.Add(
    '<th height=70>TRANSPORTADORA&nbsp;&nbsp;<FONT class=texto1>INTECOM LOG�STICA</TH>');
  memo2.Add('<th> DATA&nbsp;&nbsp;' + DateToStr(date) + '</th>');
  memo2.Add('</tr><tr>');
  memo2.Add('<th>DESTINAT�RIO :&nbsp;&nbsp;' + QEnviaNM_EMP_DEST_TA.AsString + '</TH>');
  memo2.Add('<th>Previs�o de Chegada :&nbsp;&nbsp;<FONT class=texto1>' +
    DateTimeToStr(IncMinute(now, tempo)) + '</TH>');
  memo2.Add('</TR><TR FONT class=texto2>');
  memo2.Add('<TH>N� NF.</th>');
  memo2.Add('<th>Quantidade</th>');

  // ITENS
  QEnvia.First;
  while not QEnvia.Eof do
  begin
    memo2.Add('<TR FONT class=texto4>');
    memo2.Add('<th>' + QEnviaNR_NF.AsString + '</Th>');
    memo2.Add('<Th>' + QEnviaNR_QUANTIDADE.AsString + '</Th>');
    memo2.Add('</TR>');
    QEnvia.Next;
  end;
  memo2.Add('</table>');
  memo2.SaveToFile('envia' + glbuser + '.html');
  memo2.Free;

  { Lista := TStringList.Create;
    iRetorno := ExtractStrings([';'],[' '],PChar(QEnviaDS_EMAIL.AsString),Lista);
    sProcura := 'a;00:1A:73:DE:02:6A;10.60.0.3;loja;1500;200;'; }
  email2 := QEnviaDS_EMAIL.AsString;
  //nPosicao := 0;
  nContador := 1;
  While nContador < Length(email2) do
  begin
    nPosicao := Pos(';', email2); // ,nContador);
    if email2 <> '' then
    begin
      email := copy(email2, nContador - 1, nPosicao);
      mail := TStringList.Create;
      try
        mail.values['to'] := email;
        /// AQUI VAI O EMAIL DO DESTINATARIO///
        mail.values['subject'] := 'EXPEDI��O VE�CULO CD INTECOM BARUERI';
        /// AQUI O ASSUNTO///
        mail.values['body'] := '';
        /// AQUI O TEXTO NO CORPO DO EMAIL///
        mail.values['attachment0'] := 'envia' + glbuser + '.html';
        /// /AQUI O ENDERE�O ONDE ENCONTRAM OS ARQUIVOS//
        mail.values['attachment1'] := '';
        /// IDEM  - NO ATTACHMENT1    TEM QUE COLOCAR A SEQUNCIA DO EMAIL A QUAL DESEJA ENVIAR EXEMPLO: ATTACHMENT1
        sendEMail(Application.Handle, mail);
      finally
        mail.Free;
      end;
      // nContador := nPosicao;
      email2 := copy(email2, nPosicao + 1, Length(email2) - nPosicao);
    end;
    nContador := 2;
  end;
end;

end.
