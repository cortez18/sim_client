unit CadTelas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid;

type
  TfrmCadTelas = class(TForm)
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    DataSource1: TDataSource;
    btnExcluir: TBitBtn;
    QTelas: TADOQuery;
    dbgContrato: TJvDBUltimGrid;
    QTelasIDTELA: TBCDField;
    QTelasDESCRICAO: TStringField;
    QTelasNAME: TStringField;
    procedure Restaura;
    procedure btnFecharClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoTitleClick(Column: TColumn);
  private

  public
    { Public declarations }
  end;

var
  frmCadTelas: TfrmCadTelas;

implementation

uses Dados, funcoes;

{$R *.DFM}

procedure TfrmCadTelas.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTelas.btnNovoClick(Sender: TObject);
begin
  QTelas.Append;
  Restaura;
end;

procedure TfrmCadTelas.btnSalvarClick(Sender: TObject);
begin
  QTelas.Post;
  Restaura;
end;

procedure TfrmCadTelas.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QTelas.Close;
  if Pos('order by', QTelas.SQL.Text) > 0 then
  begin
    QTelas.SQL.Text := Copy(QTelas.SQL.Text, 1, Pos('order by', QTelas.SQL.Text)
      - 1) + 'order by ' + Column.FieldName
  end
  else
    QTelas.SQL.Text := QTelas.SQL.Text + ' order by ' + Column.FieldName;
  QTelas.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTelas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QTelas.Close;
end;

procedure TfrmCadTelas.FormCreate(Sender: TObject);
begin
  QTelas.Open;
end;

procedure TfrmCadTelas.btnAlterarClick(Sender: TObject);
begin
  Restaura;
  QTelas.edit;
end;

procedure TfrmCadTelas.btnCancelarClick(Sender: TObject);
begin
  QTelas.Cancel;
end;

procedure TfrmCadTelas.btnExcluirClick(Sender: TObject);
begin
  if Application.Messagebox('Voc� Deseja Apagar Esta Tela?', 'Apagar Tela',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.Query1.Close;
    dtmdados.Query1.SQL.clear;
    dtmdados.Query1.SQL.add('delete from tb_permissao_sis where idtela = :0');
    dtmdados.Query1.Parameters[0].value := QTelasIDTELA.AsInteger;
    dtmdados.Query1.ExecSQL;
    dtmdados.Query1.Close;
    QTelas.Delete
  end;
end;

procedure TfrmCadTelas.Restaura;
begin
  btnNovo.Enabled := not btnNovo.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnExcluir.Enabled := not btnExcluir.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;
end;

end.
