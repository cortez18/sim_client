object frmRelatorioMeritor: TfrmRelatorioMeritor
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio Geral Meritor'
  ClientHeight = 372
  ClientWidth = 980
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 48
    Width = 31
    Height = 13
    Caption = 'Label1'
    Visible = False
  end
  object Gauge1: TGauge
    Left = 412
    Top = 8
    Width = 560
    Height = 31
    Progress = 0
  end
  object Label10: TLabel
    Left = 200
    Top = 2
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 303
    Top = 2
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DriveComboBox1: TDriveComboBox
    Left = 521
    Top = 49
    Width = 157
    Height = 19
    DirList = DirectoryListBox1
    TabOrder = 2
    Visible = False
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 8
    Width = 75
    Height = 31
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0041924E003D8F49003A8C44003689400032873C002F84
      37002C813300287F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF004999580045965300419950007DC28F0096D0A60096CFA60078BE
      8900368D42002C813400297F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00519F61004D9C5D0064B47800A8DBB50087CC980066BC7D0064BA7C0086CB
      9800A5D9B40058AA6B002C813400297F3000FF00FF00FF00FF00FF00FF0059A6
      6B0056A366006AB97D00A8DBB20060BC77005CBA730059B8700059B56F0058B5
      6F005BB77400A5D9B3005AAA6C002C823400297F3000FF00FF00FF00FF005DA9
      700053AB6800AADDB40064C179005FBE710060BC7700FFFFFF00FFFFFF0059B8
      700058B56E005CB77400A6DAB400388F43002C823400FF00FF00FF00FF0061AC
      75008ACC980089D396006BC67A0063C1700055AB6500FFFFFF00FFFFFF0059B8
      700059B870005BB9720085CC97007BBE8D0030853900FF00FF00FF00FF0065AF
      7A00A9DDB3007DCF8A0075CC8100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700067BE7D009CD4AB0034883D00FF00FF00FF00FF0069B2
      7E00B6E2BE008BD597007AC98600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0059B8700069C17E009DD4AA00388B4200FF00FF00FF00FF006DB5
      8300ACDDB600A6DFAF0081CB8C007CC986006EBD7900FFFFFF00FFFFFF005BAC
      6A0060BC77005CBA73008BD1990080C592003C8E4700FF00FF00FF00FF0070B8
      870085C79700D2EED70095D9A0008AD394007FC88900FFFFFF00FFFFFF0079CD
      85006BC37C006FC77E00ACDFB500459E570040914C00FF00FF00FF00FF0073BA
      8A0070B88700AADAB700D8F1DC0092D89D0088CD930084CC8E008BD496008AD4
      950083D28E00AFE0B7006BB97D004898560044945100FF00FF00FF00FF00FF00
      FF0073BB8B0070B88700AFDCBB00DCF2E000B6E4BD009BDBA50096D9A000A5DF
      AF00C0E8C50079C28A00509E5F004C9B5B00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0073BB8B0071B8870094CEA400C3E6CB00CFEBD400C9E9CE00AFDD
      B8006DB97F0058A5690054A16500FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0074BB8B0071B988006EB684006AB3800067B17C0063AE
      770060AB73005CA86E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 521
    Top = 68
    Width = 157
    Height = 270
    TabOrder = 1
    Visible = False
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 48
    Width = 972
    Height = 324
    DataSource = DataSource1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DATA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PEDIDO_WMS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ORDER_'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ITEM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRODUTO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DELIVERY'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STAT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESTINO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_DETAIL'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 356
    Top = 160
    Width = 309
    Height = 41
    Caption = 'Gerando ......'
    Color = 4227327
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic]
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    Visible = False
  end
  object btnExcel: TBitBtn
    Left = 110
    Top = 8
    Width = 75
    Height = 31
    Caption = 'Excel'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
      346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
      2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
      33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
      38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
      3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
      40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
      73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
      33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
      64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
      33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
      33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
      3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
      D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
      49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
      CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
      53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
      C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
      BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
      62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
      56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
      45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
      4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
      4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
      49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
      4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
      4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 5
    OnClick = btnExcelClick
  end
  object dtInicial: TJvDateEdit
    Left = 200
    Top = 18
    Width = 90
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 6
  end
  object dtFinal: TJvDateEdit
    Left = 303
    Top = 18
    Width = 88
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 7
  end
  object DataSource1: TDataSource
    DataSet = QCtrc
    Left = 232
    Top = 116
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct Data, Pedido_WMS, Order_, Item, Produto, Qt, Del' +
        'ivery, stat, Destino, Fantasia, Id_Detail, NF,total'
      'from ('
      
        'select distinct p.time_neu Data, i.nr_auf Pedido_WMS, i.nr_zoll_' +
        'tarif Order_, i.id_artikel Item, e.bez_1 Produto, i.mng_best Qt,' +
        ' to_char(i.PREIS_EINZEL) Delivery,'
      
        'p.stat, d.name Destino, a.name_2 Fantasia, to_char(I.PREIS_EINZE' +
        'L_INTERN) Id_Detail, p.nr_liefers NF,'
      
        'nvl((select total from wms.tb_pedido_item pi where pi.id_detail ' +
        '= I.PREIS_EINZEL_INTERN and rownum = 1),0) total'
      
        'from aufpos i left join artikel e on e.id_klient = i.id_klient a' +
        'nd e.id_artikel = i.id_artikel'
      
        '              left join auftraege p on p.nr_auf = i.nr_auf and p' +
        '.id_klient = i.id_klient'
      
        '              left join kunden d on p.id_empfaenger_ware = d.id_' +
        'empfaenger and p.id_klient = d.id_klient'
      
        '              left join adressen a on a.id_eigner_1 = i.id_klien' +
        't and a.id_eigner_2 = d.id_kunde'
      'where i.id_klient = '#39'MRTR'#39
      ''
      'union'
      ''
      
        'select pe.data Data, pe.ped_wms Pedido_WMS, pe.pedido Order_, pi' +
        '.codpro Item, '#39#39' Produto, pi.quant Qt, pe.Delivery,'
      
        #39'Importar'#39' stat, di.name Destino, ai.name_2 Fantasia, pi.Id_Deta' +
        'il, '#39#39' NF, pi.total'
      
        'from tb_pedido_item pi left join tb_pedido_meritor pe on pe.id =' +
        ' pi.id_pedido'
      
        '                       left join kunden di on (pe.coddes = di.id' +
        '_empfaenger and di.id_klient = '#39'MRTR'#39')'
      
        '                       left join adressen ai on (ai.id_eigner_1 ' +
        '= di.id_klient and ai.id_eigner_2 = di.id_kunde)'
      'where pe.wms is null)'
      ''
      'order by 1')
    Left = 228
    Top = 72
    object QCtrcDATA: TDateTimeField
      FieldName = 'DATA'
      ReadOnly = True
    end
    object QCtrcPEDIDO_WMS: TStringField
      FieldName = 'PEDIDO_WMS'
      ReadOnly = True
    end
    object QCtrcORDER_: TStringField
      FieldName = 'ORDER_'
      ReadOnly = True
      Size = 22
    end
    object QCtrcITEM: TStringField
      FieldName = 'ITEM'
      ReadOnly = True
      Size = 40
    end
    object QCtrcPRODUTO: TStringField
      FieldName = 'PRODUTO'
      ReadOnly = True
      Size = 40
    end
    object QCtrcQT: TBCDField
      FieldName = 'QT'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcDELIVERY: TStringField
      FieldName = 'DELIVERY'
      ReadOnly = True
      Size = 40
    end
    object QCtrcSTAT: TStringField
      FieldName = 'STAT'
      ReadOnly = True
      Size = 8
    end
    object QCtrcDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
      Size = 30
    end
    object QCtrcFANTASIA: TStringField
      FieldName = 'FANTASIA'
      ReadOnly = True
      Size = 30
    end
    object QCtrcID_DETAIL: TStringField
      FieldName = 'ID_DETAIL'
      ReadOnly = True
      Size = 40
    end
    object QCtrcNF: TStringField
      FieldName = 'NF'
      ReadOnly = True
      Size = 12
    end
    object QCtrcTOTAL: TBCDField
      FieldName = 'TOTAL'
      ReadOnly = True
      Precision = 32
    end
  end
end
