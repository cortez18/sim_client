unit PainelControle_Volumes_retira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, DB, ADODB,
  Mask, JvExMask, JvToolEdit, JvMemoryDataset, JvDBLookup, ShellAPI, JvMaskEdit,
  DBCtrls, JvBaseEdits, Inifiles, ImgList, JvExExtCtrls, JvRadioGroup,
  System.ImageList;

type
  TfrmPainelCon_Vol_retira = class(TForm)
    Qaberto: TADOQuery;
    dtsAberto: TDataSource;
    QTransportador: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cbtransp: TComboBox;
    BTConsultar: TBitBtn;
    dtinicial: TJvDateEdit;
    dtfinal: TJvDateEdit;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBGrid2: TDBGrid;
    QExpedidos: TADOQuery;
    Dsexpedidos: TDataSource;
    QClienteExp: TADOQuery;
    dtinicialExp: TJvDateEdit;
    dtfinalexp: TJvDateEdit;
    dsMapa: TDataSource;
    QMapa: TADOQuery;
    DBGrid3: TDBGrid;
    btconsultarexp: TBitBtn;
    Panel3: TPanel;
    Label11: TLabel;
    BtConsultarMapa: TBitBtn;
    ledCliente: TJvDBLookupEdit;
    dtsClienteExp: TDataSource;
    ledTranspMapa: TJvDBLookupEdit;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    btnMapa: TBitBtn;
    Label13: TLabel;
    edVolumeE: TJvMaskEdit;
    btnRelExpedido: TBitBtn;
    Label14: TLabel;
    edPedidoM: TJvMaskEdit;
    QCliPen: TADOQuery;
    dtsCliPen: TDataSource;
    Label15: TLabel;
    btnRelAberto: TBitBtn;
    Label16: TLabel;
    edVolumeA: TJvMaskEdit;
    Label17: TLabel;
    edPedidoA: TJvMaskEdit;
    Label18: TLabel;
    edPedidoE: TJvMaskEdit;
    QabertoPALET: TBCDField;
    QabertoTRANSPORTADORA: TStringField;
    QTransportadorTRANSPORTADORA: TStringField;
    QExpedidosPALET: TBCDField;
    QExpedidosTRANSPORTADORA: TStringField;
    QClienteExpTRANSPORTADORA: TStringField;
    QMapaNR_AUF: TStringField;
    QMapaTRANSPORTADORA: TStringField;
    QMapaEXPEDIDO: TBCDField;
    QMapaaberto: TIntegerField;
    QMAberto: TADOQuery;
    QMAbertoABERTO: TBCDField;
    QMapaCLIENTE: TStringField;
    QCliPenTRANSPORTADORA: TStringField;
    TabSheet4: TTabSheet;
    Label4: TLabel;
    DBText1: TDBText;
    QPalet: TADOQuery;
    dtsPalet: TDataSource;
    QPaletPALET: TBCDField;
    Label8: TLabel;
    edEtiqueta: TJvCalcEdit;
    btnGerar: TBitBtn;
    mdTemp: TJvMemoryData;
    mdTempreg: TIntegerField;
    mdTempExpedicao: TIntegerField;
    mdTemppedido: TStringField;
    mdTemptransportadora: TStringField;
    iml: TImageList;
    mdTempAberto: TIntegerField;
    QabertoNR_LE_1: TStringField;
    QabertoPEDIDO: TStringField;
    QabertoNR_NF: TBCDField;
    QExpedidosNR_LE_1: TStringField;
    QExpedidosPEDIDO: TStringField;
    QExpedidosNR_NF: TBCDField;
    mdTempdestinatario: TStringField;
    QMapaDESTINATARIO: TStringField;
    QMapaQT: TBCDField;
    QMapaNR_NF: TBCDField;
    mdTempvolnf: TIntegerField;
    mdTempnota: TStringField;
    mdTempwms: TIntegerField;
    TabSheet5: TTabSheet;
    Panel7: TPanel;
    Label9: TLabel;
    Label12: TLabel;
    btnConsMapa: TBitBtn;
    JvDBLookupEdit1: TJvDBLookupEdit;
    BitBtn2: TBitBtn;
    DBGrid4: TDBGrid;
    Label19: TLabel;
    edMapa: TJvMaskEdit;
    QMapa_g: TADOQuery;
    dtsMapa_g: TDataSource;
    QMapa_gROMANEIO: TBCDField;
    QMapa_gTRANSPORTADORA: TStringField;
    QMapa_gCLIENTE: TStringField;
    mdTempcustom: TIntegerField;
    QMapaCUSTOM: TBCDField;
    btnRelMapa: TBitBtn;
    btnExcluirP: TBitBtn;
    btnAlterarNF: TBitBtn;
    btnExcluirVolume: TBitBtn;
    Label10: TLabel;
    edCarga: TJvMaskEdit;
    QMapaCARGA: TStringField;
    mdTempcarga: TStringField;
    TabSheet6: TTabSheet;
    Panel8: TPanel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    btnConsExp: TBitBtn;
    edVolume: TJvMaskEdit;
    BitBtn5: TBitBtn;
    edMapaExp: TJvMaskEdit;
    DBGrid5: TDBGrid;
    RGTipo: TJvRadioGroup;
    dtsExp: TDataSource;
    QExp: TADOQuery;
    QExpCODBAR: TStringField;
    QExpDAT_CONFERENCIA: TDateTimeField;
    QExpNM_CLIENTE: TStringField;
    QExpDAT_INC: TDateTimeField;
    QExpROMANEIO: TBCDField;
    QExpCOD_TRANSP: TStringField;
    Panel9: TPanel;
    btnVolume: TBitBtn;
    QMapaLIBERADO: TBCDField;
    QExpmanifesto: TStringField;
    TabSheet7: TTabSheet;
    Panel10: TPanel;
    Label24: TLabel;
    Label25: TLabel;
    btnConCan: TBitBtn;
    BitBtn3: TBitBtn;
    edPedidoC: TJvMaskEdit;
    QCancelado: TADOQuery;
    dtsCancelado: TDataSource;
    QCanceladohinw_zust: TStringField;
    QCanceladotext: TStringField;
    Label20: TLabel;
    Label26: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QCanceladonr_auf: TStringField;
    ADOQuery1: TADOQuery;
    BCDField1: TBCDField;
    StringField1: TStringField;
    StringField2: TStringField;
    procedure BTConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btconsultarexpClick(Sender: TObject);
    procedure BtConsultarMapaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRelExpedidoClick(Sender: TObject);
    procedure btnRelAbertoClick(Sender: TObject);
    procedure btnExcluirPClick(Sender: TObject);
    procedure QMapaCalcFields(DataSet: TDataSet);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabSheet4Show(Sender: TObject);
    procedure TabSheet4Exit(Sender: TObject);
    procedure btnGerarClick(Sender: TObject);
    procedure QPaletBeforeOpen(DataSet: TDataSet);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure btnMapaClick(Sender: TObject);
    procedure mdTempFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure TabSheet3Hide(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure TabSheet2Hide(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure QabertoBeforeOpen(DataSet: TDataSet);
    procedure QExpedidosBeforeOpen(DataSet: TDataSet);
    procedure QMapaBeforeOpen(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnConsMapaClick(Sender: TObject);
    procedure QClienteExpBeforeOpen(DataSet: TDataSet);
    procedure btnRelMapaClick(Sender: TObject);
    procedure btnAlterarNFClick(Sender: TObject);
    procedure btnExcluirVolumeClick(Sender: TObject);
    procedure QExpBeforeOpen(DataSet: TDataSet);
    procedure BitBtn5Click(Sender: TObject);
    procedure btnConsExpClick(Sender: TObject);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid5TitleClick(Column: TColumn);
    procedure QMapa_gBeforeOpen(DataSet: TDataSet);
    procedure btnVolumeClick(Sender: TObject);
    procedure btnConCanClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QCliPenBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPainelCon_Vol_retira: TfrmPainelCon_Vol_retira;

implementation

uses Dados, Menu, Funcoes, MapaRetira;

{$R *.dfm}

procedure TfrmPainelCon_Vol_retira.btconsultarexpClick(Sender: TObject);
begin
  Panel5.visible := true;
  Application.ProcessMessages;
  QExpedidos.Close;
  // if (copy(dtInicialExp.Text,1,2) <> '  ' ) and (copy(dtFinalExp.Text,1,2) <> '  ' ) then
  // QExpedidos.SQL [10] := 'and c.cte_data between to_date(''' + dtinicialExp.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtfinalexp.text + ' 23:59'',''dd/mm/yy hh24:mi'')'
  // else
  // QExpedidos.SQL [10] := '';

  if Trim(ledCliente.LookupValue) <> '' then
    QExpedidos.SQL[10] := 'and transportadora = ' +
      QuotedStr(ledCliente.LookupValue)
  else
    QExpedidos.SQL[10] := '';

  if Trim(edPedidoE.text) <> '' then
    QExpedidos.SQL[11] := 'and pedido = ' + QuotedStr(Trim(edPedidoE.text))
  else
    QExpedidos.SQL[11] := '';

  if Trim(edVolumeE.text) <> '' then
    QExpedidos.SQL[12] := 'and nr_le_1 = ' + QuotedStr(Trim(edVolumeE.text))
  else
    QExpedidos.SQL[13] := '';

  QExpedidos.Open;
  Panel5.visible := false;
end;

procedure TfrmPainelCon_Vol_retira.BtConsultarMapaClick(Sender: TObject);
var
  //qt: Integer;
  SQL: String;
begin
  Panel4.visible := true;
  Application.ProcessMessages;
  QMapa.Close;

  if Trim(ledTranspMapa.LookupValue) <> '' then
    QMapa.SQL[10] := 'and transportadora = ' +
      QuotedStr(ledTranspMapa.LookupValue)
  else
    QMapa.SQL[10] := '';

  if Trim(edPedidoM.text) <> '' then
    QMapa.SQL[11] := 'and nr_auf = ' + QuotedStr(Trim(edPedidoM.text))
  else
    QMapa.SQL[11] := '';

  if Trim(edCarga.text) <> '' then
    QMapa.SQL[12] := 'and carga = ' + QuotedStr(Trim(edCarga.text))
  else
    QMapa.SQL[12] := '';

  // showmessage(QMapa.sql.text);
  QMapa.Open;
  SQL := QMapa.SQL.text;
  while not QMapa.eof do
  begin
    if (QMapaNR_NF.AsString <> '') and (QMapaQT.Value > 0) then
    begin
      ADOQuery1.Close;
      ADOQuery1.Parameters[0].Value := QMapaNR_NF.AsString;
      ADOQuery1.Parameters[1].Value := QMapaQT.Value;
      ADOQuery1.Parameters[2].Value := QMapaNR_AUF.AsString;
      ADOQuery1.ExecSQL;
      ADOQuery1.Close;
    end;
    QMapa.Next;
  end;
  QMapa.Close;
  QMapa.SQL.Clear;
  QMapa.SQL.text := SQL;
  QMapa.Open;
  mdTemp.Close;
  mdTemp.Open;
  while not QMapa.eof do
  begin
    mdTemp.append;
    mdTemppedido.Value := QMapaNR_AUF.AsString;
    mdTemptransportadora.Value := QMapaTRANSPORTADORA.AsString;
    mdTempExpedicao.Value := QMapaEXPEDIDO.AsInteger;
    mdTempwms.Value := QMapaaberto.AsInteger;
    mdTempAberto.Value :=
      (QMapaaberto.AsInteger - (QMapaEXPEDIDO.AsInteger +
      QMapaLIBERADO.AsInteger));
    mdTempvolnf.Value := QMapaQT.AsInteger;
    mdTempnota.Value := QMapaNR_NF.AsString;
    mdTempdestinatario.Value := QMapaDESTINATARIO.AsString;
    mdTempcustom.Value := QMapaCUSTOM.AsInteger;
    mdTempcarga.Value := QMapaCARGA.AsString;
    mdTemp.Post;
    QMapa.Next;
  end;
  QMapa.Close;
  Panel4.visible := false;
end;

procedure TfrmPainelCon_Vol_retira.btnMapaClick(Sender: TObject);
var
  new: Integer;
begin
  mdTemp.Filtered := true;
  if mdTempreg.Value = 1 then
  begin
    if Application.Messagebox('Gerar Mapa de Separa��o ?', 'Gerar Mapa',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('select nvl(max(romaneio),0)+1 mapa from cyber.itc_man_emb where pedido_wms is not null and tp_grav = ''PBA'' ');
      dtmdados.IQuery1.Open;
      new := dtmdados.IQuery1.FieldByName('mapa').Value;
      dtmdados.IQuery1.Close;
      while not mdTemp.eof do
      begin
        dtmdados.IQuery1.SQL.Clear;
        dtmdados.IQuery1.SQL.add('update cyber.itc_man_emb set romaneio = :0 ');
        dtmdados.IQuery1.SQL.add('where pedido_wms = :1 and cod_fil = :2 ');
        dtmdados.IQuery1.Parameters[0].Value := new;
        dtmdados.IQuery1.Parameters[1].Value := mdTemppedido.AsString;
        dtmdados.IQuery1.Parameters[2].Value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
        dtmdados.IQuery1.Close;
        // atualizar wms
        dtmdados.WWQuery1.Close;
        dtmdados.WWQuery1.SQL.Clear;
        dtmdados.WWQuery1.SQL.add('update auftraege set nr_auf_rahmen = :0 ');
        dtmdados.WWQuery1.SQL.add
          ('where nr_auf = :1 and id_klient = ''BRSP'' ');
        dtmdados.WWQuery1.Parameters[0].Value := new;
        dtmdados.WWQuery1.Parameters[1].Value := mdTemppedido.AsString;
        dtmdados.WWQuery1.ExecSQL;
        dtmdados.WWQuery1.Close;
        mdTemp.Next;
      end;
      try
        Application.CreateForm(TfrmMapaRetira, frmMapaRetira);
        frmMapaRetira.Tag := new;
        // frmMapaRetira.ShowModal;
        frmMapaRetira.RLReport1.Prepare;
        frmMapaRetira.RLReport1.Preview;
        dtmdados.IQuery1.Close;
      finally
        frmMapaRetira.Free;
      end;
    end;
  end;
  mdTemp.Filtered := false;
  BtConsultarMapaClick(Sender);
end;

procedure TfrmPainelCon_Vol_retira.btnAlterarNFClick(Sender: TObject);
var
  nf: String;
begin
  if not dtmdados.PodeAlterar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Alterar a NF deste Volume ?',
    'Alterar NF', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    nf := InputBox('Nota Fiscal :', 'Digite o n� da NF ', '');
    try
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add('update cyber.itc_man_emb set nr_nf = :0 ');
      dtmdados.IQuery1.SQL.add('where pedido_wms = :1 '); // and codbar = :2 ');
      dtmdados.IQuery1.Parameters[0].Value := nf;
      dtmdados.IQuery1.Parameters[1].Value := QExpedidosPEDIDO.AsString;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    btconsultarexpClick(Sender);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnConCanClick(Sender: TObject);
begin
  QCancelado.Close;
  QCancelado.Parameters[0].Value := edPedidoC.text;
  QCancelado.Parameters[1].Value := GLBFilial;
  // showmessage(QExp.sql.text);
  QCancelado.Open;
end;

procedure TfrmPainelCon_Vol_retira.btnConsExpClick(Sender: TObject);
begin
  Panel9.visible := true;
  Application.ProcessMessages;
  QExp.Close;

  if Trim(edVolume.text) <> '' then
    QExp.SQL[4] := 'and codbar = ' + QuotedStr(Trim(edVolume.text))
  else
    QExp.SQL[4] := '';

  if Trim(edMapaExp.text) <> '' then
    QExp.SQL[5] := 'and romaneio = ' + QuotedStr(Trim(edMapaExp.text))
  else
    QExp.SQL[5] := '';

  if RGTipo.ItemIndex = 1 then
    QExp.SQL[6] := 'and dat_conferencia is not null'
  else if RGTipo.ItemIndex = 2 then
    QExp.SQL[6] := 'and dat_conferencia is null'
  else
    QExp.SQL[6] := '';

  // showmessage(QExp.sql.text);
  QExp.Open;
  Panel9.visible := false;
end;

procedure TfrmPainelCon_Vol_retira.btnExcluirPClick(Sender: TObject);
begin
  if not dtmdados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Excluir este Volume deste Palete ?',
    'Excluir Volume', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin

    try
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('update cyber.itc_man_emb set pallet_bip = 0, pallet_fim = null ');
      dtmdados.IQuery1.SQL.add
        ('where pedido_wms = :0 and codbar = :1 and pallet_bip = :2 ');
      dtmdados.IQuery1.Parameters[0].Value := QExpedidosPEDIDO.AsInteger;
      dtmdados.IQuery1.Parameters[1].Value := QExpedidosNR_LE_1.AsString;
      dtmdados.IQuery1.Parameters[2].Value := QExpedidosPALET.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    btconsultarexpClick(Sender);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnExcluirVolumeClick(Sender: TObject);
var
  nf: String;
begin
  if not dtmdados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Excluir o Pedido deste Mapa ?',
    'Excluir Pedido do Mapa', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    nf := InputBox('Pedido :', 'Digite o n� do Pedido ', '');
    try
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('update cyber.itc_man_emb set romaneio = null, pallet_bip = null, pallet_fim = null ');
      dtmdados.IQuery1.SQL.add('where romaneio = :0 and pedido_wms = :1 ');
      dtmdados.IQuery1.Parameters[0].Value := QMapa_gROMANEIO.AsInteger;
      dtmdados.IQuery1.Parameters[1].Value := nf;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;

      // atualizar wms
      dtmdados.WWQuery1.Close;
      dtmdados.WWQuery1.SQL.Clear;
      dtmdados.WWQuery1.SQL.add('update auftraege set nr_auf_rahmen = null ');
      dtmdados.WWQuery1.SQL.add
        ('where nr_auf_rahmen = :0 and nr_auf= :1 and id_klient = ''BRSP'' ');
      dtmdados.WWQuery1.Parameters[0].Value := QMapa_gROMANEIO.AsInteger;
      dtmdados.WWQuery1.Parameters[1].Value := nf;
      dtmdados.WWQuery1.ExecSQL;
      dtmdados.WWQuery1.Close;

      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('insert into itc_recebimento_log(id_seq, filial, id_cliente, pedido, mensagem, usuario_rec, chave_nfe, data_rec) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id_seq+1),1) from itc_recebimento_log), :0, :1, :2, :3, :4, :5, sysdate) ');
      dtmdados.IQuery1.Parameters[0].Value := GLBFilial;
      dtmdados.IQuery1.Parameters[1].Value := 'BRSP';
      dtmdados.IQuery1.Parameters[2].Value := QMapa_gROMANEIO.AsInteger;
      dtmdados.IQuery1.Parameters[3].Value := 'Exclu�do Volume do Mapa Retira';
      dtmdados.IQuery1.Parameters[4].Value := GLBCodUser;
      dtmdados.IQuery1.Parameters[5].Value := nf;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
      ShowMessage('Volume Exclu�do do Mapa');
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    btconsultarexpClick(Sender);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnGerarClick(Sender: TObject);
var
  i, x: Integer;
  Ini: TIniFile;
  impr: String;
  Arq: TextFile;
begin
  if not dtmdados.PodeInserir(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Gerar Etiquetas ?',
    'Gerar Etiqueta de Palet', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
    impr := Ini.ReadString('IMPRESSORA', 'codbarra', '');
    Ini.Free;
    // enter := str 13;
    AssignFile(Arq, impr);
    ReWrite(Arq);
    x := QPaletPALET.AsInteger;
    for i := 0 to edEtiqueta.AsInteger do
    begin
      Writeln(Arq, '' + 'n');
      Writeln(Arq, '' + 'M0680');
      Writeln(Arq, '' + 'd');
      Writeln(Arq, '' + 'L');
      Writeln(Arq, 'D11');
      Writeln(Arq, 'R0000');
      Writeln(Arq, 'ySU8');
      Writeln(Arq, 'FB+');
      Writeln(Arq, 'A2');
      Writeln(Arq, '1911S0101480102P016P016I N T E C O M');
      Writeln(Arq, '1e8405000760095C' + retzero(IntToStr(x), 8));
      Writeln(Arq, '1911S0100370059P015P014EXPEDI��O -');
      Writeln(Arq, '1911S0100370190P015P014' + retzero(IntToStr(x), 8));
      Writeln(Arq, 'Q0001');
      Writeln(Arq, 'E');
      x := x + 1;
    end;
    CloseFile(Arq);
    if impr = 'C:\SIM\etiquetaexpedicao.txt' then
      Winexec(PAnsiChar('notepad.exe ' + impr), Sw_Show);
    // atualiza o contador
    dtmdados.IQuery1.Close;
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.add('update tb_controle set palet = :0 ');
    dtmdados.IQuery1.SQL.add('where filial = :1 ');
    dtmdados.IQuery1.Parameters[0].Value := x;
    dtmdados.IQuery1.Parameters[1].Value := GLBFilial;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnRelExpedidoClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volumes_expedidos.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].text + '</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.add('Relat�rio de Volumes de Retira - Expedi��o');
  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to DBGrid2.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + DBGrid2.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.add('</tr>');
  QExpedidos.First;
  while not QExpedidos.eof do
  begin
    for i := 0 to DBGrid2.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + QExpedidos.FieldByName
        (DBGrid2.Columns[i].FieldName).AsString + '</th>');
    QExpedidos.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    ShowMessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnRelMapaClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volumes_expedidos_mapa.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].text + '</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.add('Relat�rio de Volumes de Retira - Mapa');
  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to DBGrid3.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + DBGrid3.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.add('</tr>');
  mdTemp.First;
  while not mdTemp.eof do
  begin
    for i := 0 to DBGrid3.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + mdTemp.FieldByName
        (DBGrid3.Columns[i].FieldName).AsString + '</th>');
    mdTemp.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    ShowMessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnVolumeClick(Sender: TObject);
begin
  if not dtmdados.PodeApagar(name) then
    Exit;
  if Application.Messagebox
    ('Voc� Deseja Liberar a Diferen�a de Volume deste Mapa ?',
    'Liberar Volume do Mapa', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('update cyber.itc_man_emb set vol_liberado = :0, vol_lib_user = :1 ');
      dtmdados.IQuery1.SQL.add('where pedido_wms = :2  and cod_fil = :3 ');
      dtmdados.IQuery1.Parameters[0].Value := mdTempAberto.AsInteger;
      dtmdados.IQuery1.Parameters[1].Value := GLBCodUser;
      dtmdados.IQuery1.Parameters[2].Value := mdTemppedido.AsString;
      dtmdados.IQuery1.Parameters[3].Value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.Close;
      mdTemp.Edit;
      mdTempAberto.Value := 0;
      mdTemp.Post;
      ShowMessage('Volume Liberado !!');
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    btconsultarexpClick(Sender);
  end;

end;

procedure TfrmPainelCon_Vol_retira.btnRelAbertoClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volume_retira_aberto.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].text + '</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.add('Relat�rio de Volume Em Aberto - Retira');
  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to DBGrid1.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + DBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.add('</tr>');
  Qaberto.First;
  while not Qaberto.eof do
  begin
    for i := 0 to DBGrid1.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + Qaberto.FieldByName
        (DBGrid1.Columns[i].FieldName).AsString + '</th>');
    Qaberto.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    ShowMessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelCon_Vol_retira.btnConsMapaClick(Sender: TObject);
begin
  Application.ProcessMessages;
  QMapa_g.Close;

  if (JvDBLookupEdit1.LookupValue <> '') then
    QMapa_g.SQL[11] := 'and transportadora = ' +
      QuotedStr(JvDBLookupEdit1.LookupValue)
  else
    QMapa_g.SQL[11] := '';

  if Trim(edMapa.text) <> '' then
    QMapa_g.SQL[12] := 'and romaneio = ' + QuotedStr(Trim(edMapa.text))
  else
    QMapa_g.SQL[12] := '';

  QMapa_g.Open;
end;

procedure TfrmPainelCon_Vol_retira.BitBtn2Click(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmMapaRetira, frmMapaRetira);
    frmMapaRetira.Tag := QMapa_gROMANEIO.AsInteger;
    // frmMapaRetira.ShowModal;
    frmMapaRetira.RLReport1.Preview;
    dtmdados.IQuery1.Close;
  finally
    frmMapaRetira.Free;
  end;
end;

procedure TfrmPainelCon_Vol_retira.BitBtn3Click(Sender: TObject);
begin
  if Application.Messagebox('Cancelar este Pedido ?', 'Cancelar Pedido',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.add('delete cyber.itc_man_emb where pedido_wms = :0 ');
    dtmdados.IQuery1.SQL.add('and cod_fil = :1 ');
    dtmdados.IQuery1.Parameters[0].Value := QCanceladonr_auf.AsString;
    dtmdados.IQuery1.Parameters[1].Value := GLBFilial;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.Close;
    // atualizar wms
    dtmdados.WWQuery1.Close;
    dtmdados.WWQuery1.SQL.Clear;
    dtmdados.WWQuery1.SQL.add('update auftraege set nr_auf_rahmen = :0 ');
    dtmdados.WWQuery1.SQL.add('where nr_auf = :1 and id_klient = ''BRSP'' ');
    dtmdados.WWQuery1.Parameters[0].Value := 'Cancelado';
    dtmdados.WWQuery1.Parameters[1].Value := QCanceladonr_auf.AsString;
    dtmdados.WWQuery1.ExecSQL;
    dtmdados.WWQuery1.Close;
    ShowMessage('Pedido Cancelado');
  end;
  edPedidoC.Clear;
  QCancelado.Close;
end;

procedure TfrmPainelCon_Vol_retira.BitBtn5Click(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Conf_volumes_expedidos.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].text + '</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.add('Confer�ncia de Volumes de Retira - Expedi��o');
  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to DBGrid5.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + DBGrid5.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.add('</tr>');
  QExp.First;
  while not QExp.eof do
  begin
    for i := 0 to DBGrid5.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + QExp.FieldByName(DBGrid5.Columns[i]
        .FieldName).AsString + '</th>');
    QExp.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    ShowMessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelCon_Vol_retira.BTConsultarClick(Sender: TObject);
begin
  Panel6.visible := true;
  Application.ProcessMessages;
  Qaberto.Close;
  // if (copy(dtinicial.Text,1,2) <> '  ' ) and (copy(dtfinal.Text,1,2) <> '  ' )then
  // QAberto.SQL [3] := 'and datainc between to_date(''' + dtinicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text + ' 23:59'',''dd/mm/yy hh24:mi'')'
  // else
  // QAberto.SQL [3] := '';

  if (cbtransp.text <> '') then
    Qaberto.SQL[10] := 'and transportadora = ' + QuotedStr(cbtransp.text)
  else
    Qaberto.SQL[10] := '';

  if Trim(edVolumeA.text) <> '' then
    Qaberto.SQL[11] := 'and nr_le_1 = ' + QuotedStr(Trim(edVolumeA.text))
  else
    Qaberto.SQL[11] := '';

  if Trim(edPedidoA.text) <> '' then
    Qaberto.SQL[12] := 'and pedido = ' + QuotedStr(Trim(edPedidoA.text))
  else
    Qaberto.SQL[12] := '';

  Qaberto.Open;
  Panel6.visible := false;
end;

procedure TfrmPainelCon_Vol_retira.DBGrid3CellClick(Column: TColumn);
//var qt: Integer;
begin
  if mdTempAberto.Value = 0 then
  begin
    if (Column = DBGrid3.Columns[0]) then
    begin
      mdTemp.Edit;
      if (mdTempreg.IsNull) or (mdTempreg.Value = 0) then
      begin
        mdTempreg.Value := 1;
      end
      else
      begin
        mdTempreg.Value := 0;
      end;
      mdTemp.Post;
    end;
  end;
end;

procedure TfrmPainelCon_Vol_retira.DBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if mdTempAberto.Value > 0 then
  begin
    DBGrid3.canvas.Brush.Color := clRed;
    DBGrid3.canvas.Font.Color := clWhite;
    DBGrid3.DefaultDrawDataCell(Rect, DBGrid3.Columns[DataCol].field, State);
  end
  else
  begin
    if mdTempvolnf.Value <> mdTempExpedicao.Value then
    begin
      DBGrid3.canvas.Brush.Color := clYellow;
      DBGrid3.canvas.Font.Color := clBlack;
      DBGrid3.DefaultDrawDataCell(Rect, DBGrid3.Columns[DataCol].field, State);
    end;
    if (Column.field = mdTempreg) then
    begin
      DBGrid3.canvas.FillRect(Rect);
      iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 0);
      //
      if mdTempreg.Value = 1 then
        iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
    if (Column.field = mdTempcustom) then
    begin
      DBGrid3.canvas.FillRect(Rect);
      iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 0);
      //
      if mdTempcustom.Value = 1 then
        iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        iml.Draw(DBGrid3.canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  end;
end;

procedure TfrmPainelCon_Vol_retira.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if QExpDAT_CONFERENCIA.IsNull then
  begin
    DBGrid5.canvas.Brush.Color := clRed;
    DBGrid5.canvas.Font.Color := clWhite;
    DBGrid5.DefaultDrawDataCell(Rect, DBGrid5.Columns[DataCol].field, State);
  end;
end;

procedure TfrmPainelCon_Vol_retira.DBGrid5TitleClick(Column: TColumn);
begin
  QExp.Close;
  if Pos('order by', QExp.SQL.text) > 0 then
    QExp.SQL.text := Copy(QExp.SQL.text, 1, Pos('order by', QExp.SQL.text) - 1)
      + 'order by ' + Column.FieldName
  else
    QExp.SQL.text := QExp.SQL.text + ' order by ' + Column.FieldName;
  QExp.Open;
end;

procedure TfrmPainelCon_Vol_retira.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QClienteExp.Close;
end;

procedure TfrmPainelCon_Vol_retira.FormCreate(Sender: TObject);
begin
  PageControl1.TabIndex := 0;
  cbtransp.Items.Clear;
  cbtransp.Items.add('');
  QTransportador.Open;
  while not QTransportador.eof do
  begin
    cbtransp.Items.add(QTransportadorTRANSPORTADORA.Value);
    QTransportador.Next;
  end;
  QTransportador.Close;

  // QAberto.Open;
end;

procedure TfrmPainelCon_Vol_retira.mdTempFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdTemp['reg'] = 1;
end;

procedure TfrmPainelCon_Vol_retira.QabertoBeforeOpen(DataSet: TDataSet);
begin
  Qaberto.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QClienteExpBeforeOpen(DataSet: TDataSet);
begin
  QClienteExp.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QCliPenBeforeOpen(DataSet: TDataSet);
begin
  QCliPen.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QExpBeforeOpen(DataSet: TDataSet);
begin
  QExp.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QExpedidosBeforeOpen(DataSet: TDataSet);
begin
  QExpedidos.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QMapaBeforeOpen(DataSet: TDataSet);
begin
  QMapa.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QMapaCalcFields(DataSet: TDataSet);
begin
  QMAberto.Close;
  QMAberto.Parameters[0].Value := QMapaNR_AUF.Value;
  QMAberto.Parameters[1].Value := QMapaCLIENTE.Value;
  QMAberto.Open;
  if QMAberto.eof then
    QMapaaberto.Value := 0
  else
    QMapaaberto.Value := QMAbertoABERTO.AsInteger;
end;

procedure TfrmPainelCon_Vol_retira.QMapa_gBeforeOpen(DataSet: TDataSet);
begin
  QMapa_g.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.QPaletBeforeOpen(DataSet: TDataSet);
begin
  QPalet.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet2Hide(Sender: TObject);
begin
  QExpedidos.Close;
  QClienteExp.Close;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet2Show(Sender: TObject);
begin
  // btconsultarexpClick(Sender);
  QClienteExp.Open;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet3Hide(Sender: TObject);
begin
  QMapa.Close;
  mdTemp.Close;
  mdTemp.Filtered := false;
  QCliPen.Close;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet3Show(Sender: TObject);
begin
  // BtConsultarMapaClick(Sender);
  QCliPen.Open;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet4Exit(Sender: TObject);
begin
  QPalet.Close;
end;

procedure TfrmPainelCon_Vol_retira.TabSheet4Show(Sender: TObject);
begin
  QPalet.Open;
end;

end.
