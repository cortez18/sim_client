object frmExpOcorren: TfrmExpOcorren
  Left = 0
  Top = 0
  Caption = 'Exporta'#231#227'o de Ocorr'#234'ncias - Proceda'
  ClientHeight = 518
  ClientWidth = 819
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 12
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 111
    Top = 4
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 213
    Top = 4
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 367
    Top = 456
    Width = 165
    Height = 23
    Progress = 0
  end
  object Label1: TLabel
    Left = 18
    Top = 47
    Width = 47
    Height = 13
    Caption = 'Opera'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Panel2: TPanel
    Left = 639
    Top = 74
    Width = 165
    Height = 336
    TabOrder = 11
    object Label8: TLabel
      Left = 4
      Top = 4
      Width = 47
      Height = 13
      Caption = 'Caminho :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DriveComboBox1: TDriveComboBox
      Left = 4
      Top = 23
      Width = 157
      Height = 19
      DirList = DirectoryListBox1
      TabOrder = 0
    end
    object DirectoryListBox1: TDirectoryListBox
      Left = 4
      Top = 40
      Width = 157
      Height = 264
      TabOrder = 1
    end
  end
  object dtInicial: TJvDateEdit
    Left = 12
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 109
    Top = 20
    Width = 90
    Height = 21
    ShowNullDate = False
    YearDigits = dyFour
    TabOrder = 1
    OnExit = dtFinalExit
  end
  object btRelatorio: TBitBtn
    Left = 700
    Top = 20
    Width = 93
    Height = 35
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 4
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 213
    Top = 20
    Width = 145
    Height = 21
    DropDownCount = 25
    LookupDisplay = 'nr_cnpj_cpf'
    LookupField = 'cod_cliente'
    LookupSource = dtsCliente
    TabOrder = 2
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 374
    Top = 20
    Width = 303
    Height = 21
    DropDownCount = 25
    LookupDisplay = 'nm_cliente'
    LookupField = 'cod_cliente'
    LookupSource = dtsCliente
    TabOrder = 3
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 74
    Width = 637
    Height = 440
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'NR_NF'
        Title.Alignment = taCenter
        Title.Caption = 'NF'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 43
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NR_SERIE'
        Title.Alignment = taCenter
        Title.Caption = 'S'#233'rie'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_DESTINO'
        Title.Alignment = taCenter
        Title.Caption = 'Data Entrega'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DS_OCOR_ENT'
        Title.Alignment = taCenter
        Title.Caption = 'C'#243'd Ocorr'#234'ncia'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 86
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'
        Title.Caption = 'Ocorr'#234'ncia'
        Width = 290
        Visible = True
      end>
  end
  object Memo1: TMemo
    Left = 78
    Top = 296
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 6
    Visible = False
  end
  object btnExportar: TBitBtn
    Left = 639
    Top = 421
    Width = 165
    Height = 27
    Caption = 'Exportar'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFE2E2E2
      CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9518B555085
      54CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCFC5F9F65559E5C509856528D57CCCCCCFFFFFFFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8FAF864AB6B63AC6B88C99082C6
      8A529A58458349FAFCFAFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFB
      FBFB83C48A57AB616AB47390CE978ACB916AB0703A82416A9E6DFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFBFBFBFAFAFAFAFAFA54AB5E96D29F91CF
      99539F5BAABCACFEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFB
      FBFBFBFBFBFAFAFA5BB4659DD6A699D3A24B9E53C9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFB60BC6C5CB66757B0
      6152A85CC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFAFAFAF9F9F9F6F6F6F6F6F6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBF8F8F8F6F6F6F3F3F3F2F2
      F2FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFB
      FBFBF8F8F8F5F5F5F2F2F2EFEFEFEDEDEDFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCFBFBFBFCFCFCFCFCFCFBFBFBF8F8F8F5F5F5F1F1F1ECECECEAEAEAE6E6
      E6FCFCFCC9C9C9FEFEFEFEFEFEC9C9C9FCFCFCF9F9F9F9F9F9F9F9F9F7F7F7F6
      F6F6F2F2F2EBEBEBFCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FEFEFEFEFEFEC9C9C9
      FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6F6F4F4
      F4C5C5C5DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5F5F5F1
      F1F1EFEFEFE9E9E9FCFCFCE7E7E7C3C3C3DFDFDFFDFDFDFFFFFFFFFFFFCCCCCC
      F8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2C2DFDF
      DFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9C9C9C9
      C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFFFFFFFF}
    TabOrder = 8
    OnClick = btnExportarClick
  end
  object cbOper: TJvComboBox
    Left = 86
    Top = 44
    Width = 236
    Height = 21
    Style = csDropDownList
    DropDownCount = 20
    TabOrder = 7
    Text = ''
    Visible = False
  end
  object DBNavigator1: TDBNavigator
    Left = 643
    Top = 469
    Width = 164
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 9
  end
  object Panel1: TPanel
    Left = 158
    Top = 188
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 10
    Visible = False
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QCtrcBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select * from ('
      
        'select distinct nvl(p.para, p.id_ocor) ds_ocor_ent, case when nv' +
        'l(p.para, p.id_ocor) = '#39'01'#39' then n.dt_entrega else t.dt_ocorrenc' +
        'ia end DT_DESTINO,'
      
        'n.notfis nr_nf, coalesce(n.serien,'#39'1'#39') nr_serie, cl.razsoc nm_cl' +
        'i_os,'
      
        'case when finalizadora = '#39'S'#39' then '#39'03'#39' else '#39'01'#39' end cod_obs, ca' +
        'se when p.obs = '#39'S'#39' then t.descricao else oc.descricao end nome,' +
        ' c.nr_conhecimento, c.cod_pagador, c.fl_empresa, t.dt_ocorrencia'
      
        'from tb_conhecimento c left join crm_tb_ocorrencias t on c.nr_co' +
        'nhecimento = t.nro_cte and c.fl_empresa = t.filial'
      
        '                       left join tb_cte_nf n on n.codcon = c.nr_' +
        'conhecimento and n.fl_empresa = c.fl_empresa'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.cod_pagador'
      
        '                       left join tb_ocorrencia_para p on p.id_oc' +
        'or = coalesce(t.id_tipo_ocor,1) and p.cliente = c.cod_pagador'
      
        '                       left join tb_ocorrencia oc on oc.id = p.i' +
        'd_ocor'
      
        '                       left join cyber.tb_coleta co on co.doc = ' +
        'c.nr_conhecimento and co.filial = c.fl_empresa'
      'where n.notfis is not null and oc.situacao = '#39'A'#39
      'and nvl(n.dt_entrega, t.dt_ocorrencia) is not null'
      ''
      'union all'
      ''
      
        'select distinct nvl(p.para, p.id_ocor) ds_ocor_ent, case when nv' +
        'l(p.para, p.id_ocor) = '#39'01'#39' then n.dt_entrega else t.dt_ocorrenc' +
        'ia end DT_DESTINO,'
      
        'n.notfis nr_nf, coalesce(n.serien,'#39'1'#39') nr_serie, cl.razsoc nm_cl' +
        'i_os,'
      
        'case when finalizadora = '#39'S'#39' then '#39'03'#39' else '#39'01'#39' end cod_obs, ca' +
        'se when p.obs = '#39'S'#39' then t.descricao else oc.descricao end nome,' +
        ' c.nr_conhecimento, c.cod_pagador, c.fl_empresa, t.dt_ocorrencia'
      
        'from tb_conhecimento c left join crm_tb_ocor_new t on c.nr_conhe' +
        'cimento = t.nro_cte and c.fl_empresa = t.filial'
      
        '                       left join tb_cte_nf n on n.codcon = c.nr_' +
        'conhecimento and n.fl_empresa = c.fl_empresa'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.cod_pagador'
      
        '                       left join tb_ocorrencia_para p on p.id_oc' +
        'or = coalesce(t.id_tipo_ocor,1) and p.cliente = c.cod_pagador'
      
        '                       left join tb_ocorrencia oc on oc.id = p.i' +
        'd_ocor'
      
        '                       left join cyber.tb_coleta co on co.doc = ' +
        'c.nr_conhecimento and co.filial = c.fl_empresa'
      'where n.notfis is not null and oc.situacao = '#39'A'#39
      'and nvl(n.dt_entrega, t.dt_ocorrencia) is not null)'
      'where cod_pagador = 36966'
      
        'and DT_DESTINO between to_date('#39'01/10/19 00:00'#39','#39'dd/mm/yy hh24:m' +
        'i'#39') and to_date('#39'11/10/19 23:59'#39','#39'dd/mm/yy hh24:mi'#39')'
      'and fl_empresa = :0'
      'order by 11')
    Left = 148
    Top = 236
    object QCtrcDT_DESTINO: TDateTimeField
      FieldName = 'DT_DESTINO'
    end
    object QCtrcNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      Size = 50
    end
    object QCtrcNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      ReadOnly = True
      Size = 3
    end
    object QCtrccod_obs: TStringField
      FieldName = 'cod_obs'
      ReadOnly = True
      Size = 2
    end
    object QCtrcnome: TStringField
      FieldName = 'nome'
      Size = 50
    end
    object QCtrcDS_OCOR_ENT: TBCDField
      FieldName = 'DS_OCOR_ENT'
      Precision = 32
      Size = 0
    end
    object QCtrcNR_NF: TStringField
      FieldName = 'NR_NF'
    end
  end
  object navnavig: TDataSource
    DataSet = QCtrc
    Left = 196
    Top = 236
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClienteBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct cod_pagador cod_cliente, c.razsoc nm_cliente, c.' +
        'codcgc nr_cnpj_cpf'
      
        'from cyber.rodcli c left join tb_conhecimento P on p.cod_pagador' +
        ' = c.codclifor'
      'where p.fl_status = '#39'A'#39' '
      'and p.fl_empresa = :0'
      ''
      'order by nm_cliente')
    Left = 16
    Top = 104
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object dtsCliente: TDataSource
    DataSet = QCliente
    Left = 68
    Top = 104
  end
  object QOperacao: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select *'
      'from tb_operacao p'
      'order by id_operacao'
      '')
    Left = 124
    Top = 104
    object QOperacaoID_OPERACAO: TBCDField
      FieldName = 'ID_OPERACAO'
      Precision = 32
      Size = 0
    end
    object QOperacaoOPERACAO: TStringField
      FieldName = 'OPERACAO'
    end
  end
  object SP_OCO_COLGATE: TADOStoredProc
    AutoCalcFields = False
    Connection = dtmDados.ADOConnection1
    ProcedureName = 'SP_EDI_OCORREN_COLGATE_MANUAL'
    Parameters = <
      item
        Name = 'VRESPOSTA'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 3000
        Value = 
          'ORA-29283: opera'#231#227'o de arquivo inv'#225'lida'#10'ORA-06512: em "SYS.UTL_F' +
          'ILE", line 536'#10'ORA-29283: opera'#231#227'o de arquivo inv'#225'lida'
      end
      item
        Name = 'VDATAINI'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/04/2020'
      end
      item
        Name = 'VDATAFIM'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '15/04/2020'
      end>
    Prepared = True
    Left = 248
    Top = 80
  end
end
