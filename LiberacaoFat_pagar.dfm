object frmLiberacaoFat_pagar: TfrmLiberacaoFat_pagar
  Left = 0
  Top = 0
  Caption = 'Libera'#231#227'o de Faturas '#224' Pagar'
  ClientHeight = 590
  ClientWidth = 983
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 304
    Top = 3
    Width = 72
    Height = 13
    Caption = 'Transportador :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 210
    Top = 3
    Width = 36
    Height = 13
    Caption = 'Fatura :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 177
    Top = 565
    Width = 604
    Height = 23
    Progress = 0
  end
  object Label2: TLabel
    Left = 8
    Top = 3
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 100
    Top = 3
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btRelatorio: TBitBtn
    Left = 730
    Top = 10
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FFDBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF6FFEAD9F2D6C0D6C5B7B7AFA89F
      9D9C9C9A98B3AAA3D0BFB1EFD3BCFFEAD9FFFAF6FFFFFFFFFFFFFFFFFFFFFAF6
      FFE6D3E4D2C5BEB8B1DCD1C8F1E8E1FBF6F2FAF3EEEDE0D6D2C3B8ADA49DDAC8
      BAFFE6D3FFFAF6FFFFFFFFFFFFFFEAD9E8D7C9C6BEB7F4EEE8FFFEFEFBF4EFF7
      EBE1F6EAE0FAF3EDFEFCFAEEE4DBACA196DBC9BBFFEAD9FFFFFFFFF4ECF7DAC4
      D2CCC8F7F1EDFFFFFFF6E8DDF6E8DDF9F1EBF5E6DBF5E5DAF4E5D8FFFEFEEFE4
      DCB1A9A2F0D4BEFFF4ECFFE9D8EBD9CCECE5E1FFFEFEF7EBE1F6EAE0F6E9DFBD
      8051F9F1EBF5E7DCF5E6DBF5E5DAFEFCFAD6C9BFD4C3B5FFE9D8FFE1C9DFD7D0
      F8F3EFFBF6F2F7ECE3F7ECE3F7EBE1C58655BE8051F9F1EBF6E8DDF5E7DCFAF3
      EEEFE4DBBBB3ACFFE1C9FFDBBED7D5D3FEFDFBF9F1EBF8EEE5F7EDE4F7ECE3C0
      8352C68655BE8051F9F1EBF6E8DDF7EBE1FAF5F1AAA9A7FFDBBEFFDBBED8D6D4
      FEFDFBFAF2ECF9EFE8F8EEE5F7EDE4C08352C08352C88755EDD3BEF6E9DFF7EC
      E3FDFBF9AFADABFFDBBEFFE1C9E2DAD4FAF6F3FCF8F4F9EFE8F9EFE8F8EFE6C1
      8352C18352E8C8ADF7ECE3F7EBE1FBF5F1F6EFEAC5BDB6FFE1C9FFE9D8EEDDD0
      F1ECE8FFFFFFF9F1EBF9F0E9F9EFE8C38453E9C9B0F8EEE5F7ECE3F7ECE3FFFF
      FFE7DFD8DFCEC0FFE9D8FFF4ECF9DDC7E2DDD9FBF8F7FFFFFFF9F1EBF9F0E9E9
      CAB1F9EFE8F8EFE6F8EEE5FFFFFFF9F6F3CFC9C4F5D9C3FFF4ECFFFFFFFFEAD9
      F2E1D3E0DAD6FBF8F7FFFFFFFCF9F6FAF3EEFAF3EDFCF8F4FFFFFFF9F6F3D3CC
      C7ECDBCDFFEAD9FFFFFFFFFFFFFFFAF6FFE6D3F2E1D3E2DDD9F1ECE8FAF6F3FE
      FDFBFEFDFBF9F5F2EEE9E5DAD4CFEEDCCEFFE6D3FFFAF6FFFFFFFFFFFFFFFFFF
      FFFAF6FFEAD9F9DDC7EFDED0E3DBD5DAD8D6D9D7D5E1D8D1ECDBCDF8DCC6FFEA
      D9FFFAF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FF
      DBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 5
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 304
    Top = 17
    Width = 140
    Height = 21
    LookupDisplay = 'NR_CNPJ_CPF'
    LookupField = 'COD_FORNECEDOR'
    LookupSource = DataSource1
    TabOrder = 3
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object ledCliente: TJvDBLookupEdit
    Left = 450
    Top = 17
    Width = 274
    Height = 21
    LookupDisplay = 'NM_FORNECEDOR'
    LookupField = 'COD_FORNECEDOR'
    LookupSource = DataSource1
    TabOrder = 4
    Text = ''
    OnCloseUp = ledIdClienteCloseUp
    OnExit = ledIdClienteCloseUp
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 44
    Width = 981
    Height = 470
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    WordWrap = True
    Columns = <
      item
        Expanded = False
        FieldName = 'DATAALT'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_FATURA_PGTO'
        Title.Alignment = taCenter
        Title.Caption = 'Fatura'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_FORNECEDOR'
        Title.Alignment = taCenter
        Title.Caption = 'Transportador'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 301
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RECEITA'
        Title.Alignment = taCenter
        Title.Caption = 'Receita'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Alignment = taCenter
        Title.Caption = 'Valor'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCONTO'
        Title.Alignment = taCenter
        Title.Caption = 'Desconto'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LIQUIDO'
        Title.Alignment = taCenter
        Title.Caption = 'Valor Liquido'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM'
        Title.Alignment = taCenter
        Title.Caption = 'Margem'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USUARIO'
        Title.Alignment = taCenter
        Title.Caption = 'Usu'#225'rio'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 79
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'vcto'
        Title.Alignment = taCenter
        Title.Caption = 'Vcto.'
        Width = 66
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 276
    Top = 240
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    Visible = False
  end
  object btnExcel: TBitBtn
    Left = 793
    Top = 564
    Width = 75
    Height = 25
    Caption = 'Listagem'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF196B37196B37196B
      37196B37196B37FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF196B37288C5364BA8D95D2B264BA8D288C53196B37CD6E23C9651B
      C8601AC65918C25317C04E16BD4715BB4115B93E14206A3862BA8B60BA87FFFF
      FF60B98767BC8F1E6936D27735E4AF87E3AB81E1A87BDFA376DEA171DC9D6DDB
      9968DA9763317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B37D68443E7B590
      E0A374DE9E6EDC9A67DB9560D9915AD78D53D5894D48875C90D3B192D6B1FFFF
      FF65BC8C67BC8F1E6936DB8E53EABB99FCF6F2E1A679FCF5F1DD9D6BFCF6F2DA
      945EF7E9DE938E5D61AB8195D4B4BAE6D06ABB8F2D8F57605326E19762ECC1A1
      FCF7F3E5AD84FCF6F2E1A477FCF7F3DD9B69F8EBE0F0D3BEA8BAA05F956F4F8E
      66478458868654B73C13E2A06EEEC7A8FEFDFCFDF7F3FEFAF8E3AC81FCF7F4E0
      A374F9ECE3FAF2EBFDF8F4E3AE86FAF1EAD5894DDA9966BD4315E6A779F0CBB0
      FDF8F5EABA98FDF8F4E7B38CFDF8F5E3AA80F9EEE5EECDB4FDF8F5E5B38EFDF9
      F6D88F57DD9E6DC04E16EAAB80F2CFB5FCF4EEECBF9FFBF3EDFDF8F4FDF7F4FC
      F7F3F4DBC9E7B48EF7E6DAE3AD83F6E4D6DB9762DFA376C45918EAAB80F3D0B7
      EFC6A9EFC4A6EEC2A2ECBF9EEBBC98E9B893E8B48EE6B088E3AC81E2A77BE0A3
      74DE9E6EE2AA80C9621AEAAB80F3D0B7F3D0B7F3D0B7F2D0B7F1CEB3F0CBB0EF
      C9ACEEC6A8EDC2A3EBC09EEABB99E8B794E6B48FE4B089CD6E23EAAB80EAAB80
      EAAB80EAAB80EAAB80EAAB80E8A97CE6A477E2A070E29B6BE19762DD9059D98B
      52D88549D6803ED27735FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 6
    OnClick = btnExcelClick
  end
  object dtInicial: TJvDateEdit
    Left = 8
    Top = 17
    Width = 90
    Height = 21
    Hint = 'Data do Servi'#231'o'
    ParentShowHint = False
    ShowHint = True
    ShowNullDate = False
    TabOrder = 0
  end
  object dtFinal: TJvDateEdit
    Left = 102
    Top = 17
    Width = 90
    Height = 21
    ShowNullDate = False
    TabOrder = 1
  end
  object DBNavigator1: TDBNavigator
    Left = 7
    Top = 565
    Width = 164
    Height = 23
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 9
  end
  object edCtrc: TJvEdit
    Left = 210
    Top = 17
    Width = 77
    Height = 21
    TabOrder = 2
    Text = ''
  end
  object btnLiberar: TBitBtn
    Left = 811
    Top = 10
    Width = 75
    Height = 25
    Caption = 'Liberar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 10
    OnClick = btnLiberarClick
  end
  object JvDBRichEdit1: TJvDBRichEdit
    Left = 0
    Top = 516
    Width = 873
    Height = 48
    DataField = 'OBS'
    DataSource = navnavig
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    PlainText = True
    ReadOnly = True
    TabOrder = 11
  end
  object QFatura: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    OnCalcFields = QFaturaCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select distinct c.fatura fl_fatura_pgto, c.transp, c.razsoc nm_f' +
        'ornecedor, sum(c.custo_calc) total, cc.usuario,'
      
        'to_char(c.dataalt,'#39'dd/mm/rrrr'#39') dataalt,  c.desconto, (sum(c.cus' +
        'to_calc)-c.desconto) liquido, c.obs,  sum(c.totfre) receita,'
      'case when sum(c.totfre) > 0 then'
      'round((1-((sum(c.custo_calc)-c.desconto)/sum(c.totfre)))*100,2)'
      'else'
      '  -100.00 end  margem'
      
        'FROM vw_custo_transp_sim c left join tb_controle_custo cc on c.f' +
        'atura = cc.fatura and c.transp = cc.codclifor'
      
        'where c.status = '#39'P'#39' and c.fatura is not null and c.dt_liberado ' +
        'is null'
      ''
      ''
      ''
      ''
      
        'group by c.fatura, c.transp, c.razsoc, cc.usuario, to_char(c.dat' +
        'aalt,'#39'dd/mm/rrrr'#39'), c.desconto, c.obs'
      'order by c.razsoc, c.fatura')
    Left = 28
    Top = 208
    object QFaturaFL_FATURA_PGTO: TStringField
      FieldName = 'FL_FATURA_PGTO'
      Size = 10
    end
    object QFaturaNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      Size = 50
    end
    object QFaturaTOTAL: TBCDField
      FieldName = 'TOTAL'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QFaturaUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QFaturaDATAALT: TStringField
      FieldName = 'DATAALT'
      ReadOnly = True
      Size = 10
    end
    object QFaturaDESCONTO: TBCDField
      FieldName = 'DESCONTO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 15
      Size = 2
    end
    object QFaturaLIQUIDO: TBCDField
      FieldName = 'LIQUIDO'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QFaturaOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 100
    end
    object QFaturaTRANSP: TBCDField
      FieldName = 'TRANSP'
      ReadOnly = True
      Precision = 32
    end
    object QFaturaMARGEM: TBCDField
      FieldName = 'MARGEM'
      ReadOnly = True
      DisplayFormat = '##0.00'
      Precision = 32
    end
    object QFaturaRECEITA: TBCDField
      FieldName = 'RECEITA'
      ReadOnly = True
      DisplayFormat = '##,##0.00'
      Precision = 32
    end
    object QFaturavcto: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'vcto'
      DisplayFormat = 'dd/mm/yyyy'
      Calculated = True
    end
  end
  object navnavig: TDataSource
    DataSet = QFatura
    Left = 24
    Top = 260
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct c.codclifor cod_fornecedor, nvl(f.codcgc,f.codcp' +
        'f) nr_cnpj_cpf, f.razsoc nm_fornecedor'
      
        'from tb_controle_custo c left join cyber.rodcli f on f.codclifor' +
        ' = c.codclifor'
      'where c.status = '#39'P'#39' and c.fatura is not null'
      'order by 3')
    Left = 28
    Top = 104
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
    object QClienteNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      ReadOnly = True
      Size = 50
    end
    object QClienteCOD_FORNECEDOR: TBCDField
      FieldName = 'COD_FORNECEDOR'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 28
    Top = 148
  end
  object SP_Exporta: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_EXPORTA_FATURA'
    Parameters = <
      item
        Name = 'V_FATURA'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = 16
        Value = '0'
      end
      item
        Name = 'V_FORNE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'V_VCTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = 0d
      end
      item
        Name = 'V_USUARIO'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 24
    Top = 320
  end
end
