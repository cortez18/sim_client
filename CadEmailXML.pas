unit CadEmailXML;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DB, ImgList, ADODB, ActnList, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, JvExControls, JvDBLookup, ExtCtrls, JvExStdCtrls,
  JvCheckBox, JvCombobox, ComCtrls, Mask, JvExMask, JvToolEdit, JvMaskEdit,
  System.ImageList, System.Actions;

type
  TfrmCadEmailXML = class(TForm)
    dtsEmail: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QRep: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    Label1: TLabel;
    dtsCliente: TDataSource;
    Label2: TLabel;
    QCliente: TADOQuery;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    CBLCli: TJvDBLookupCombo;
    edRepositorio: TJvMaskEdit;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    QRepCODCLI: TBCDField;
    QRepCAMINHO: TStringField;
    QRepDT_INC: TDateTimeField;
    QRepUSER_INC: TBCDField;
    QRepDT_ALT: TDateTimeField;
    QRepUSER_ALT: TBCDField;
    QRepNM_CLIENTE: TStringField;
    btnExcluirE: TBitBtn;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure CBLCliChange(Sender: TObject);
    procedure btnExcluirEClick(Sender: TObject);
  private
    tipo: String;
  public
    { Public declarations }
  end;

var
  frmCadEmailXML: TfrmCadEmailXML;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadEmailXML.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadEmailXML.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  CBLCli.KeyValue := '';
  edRepositorio.clear;
  CBLCli.SetFocus;
end;

procedure TfrmCadEmailXML.btnSalvarClick(Sender: TObject);
begin

  if CBLCli.KeyValue = '' then
  begin
    ShowMessage('N�o foi escolhido o Cliente');
    CBLCli.SetFocus;
    Exit;
  end;

  if edRepositorio.text = '' then
  begin
    ShowMessage('N�o foi escolhido o Reposit�rio');
    edRepositorio.SetFocus;
    Exit;
  end;

  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_email_xml (codcli, caminho, user_inc) ');
      dtmDados.IQuery1.SQL.add('values (:0, :1, :2 )');
      dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := edRepositorio.text;
      dtmDados.IQuery1.Parameters[2].value := GLBCodUser;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_email_xml set codcli = :0, caminho = :1,  ');
      dtmDados.IQuery1.SQL.add('user_alt = :2, dt_alt = sysdate ');
      dtmDados.IQuery1.SQL.add('where codcli = :3 ');
      dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.Parameters[1].value := edRepositorio.text;
      dtmDados.IQuery1.Parameters[2].value := GLBCodUser;
      dtmDados.IQuery1.Parameters[3].value := QClienteCOD_CLIENTE.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QRep.Close;
  QRep.Open;
end;

procedure TfrmCadEmailXML.CBLCliChange(Sender: TObject);
begin
  dtmDados.IQuery1.Close;
  dtmDados.IQuery1.SQL.clear;
  dtmDados.IQuery1.SQL.add
    ('select count(*) tem from tb_email_xml where codcli = :0 ');
  dtmDados.IQuery1.Parameters[0].value := QClienteCOD_CLIENTE.AsInteger;
  dtmDados.IQuery1.Open;
  if dtmDados.IQuery1.FieldByName('tem').value > 0 then
  begin
    dtmDados.IQuery1.Close;
    ShowMessage('J� existe Cadastro para este Cliente');
    Restaura;
    Exit;
  end;
  dtmDados.IQuery1.Close;
end;

procedure TfrmCadEmailXML.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QRep.Cancel;
end;

procedure TfrmCadEmailXML.btnExcluirEClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Registro?',
    'Apagar Registro', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.add('delete from tb_email_xml where codcli = :0 ');
      dtmDados.IQuery1.Parameters[0].value := QRepCODCLI.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
    Restaura;
    QRep.Close;
    QRep.Open;
  end;
end;

procedure TfrmCadEmailXML.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  edRepositorio.text := QRepCAMINHO.text;
  QCliente.locate('cod_cliente', QRepCODCLI.value, []);
  CBLCli.SetFocus;
end;

procedure TfrmCadEmailXML.FormCreate(Sender: TObject);
begin
  QCliente.Open;
  QRep.Open;
  lblQuant.Caption := IntToStr(QRep.RecNo) + #13#10 +
    IntToStr(QRep.RecordCount);
end;

procedure TfrmCadEmailXML.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QRep.Close;
  QCliente.Close;
end;

procedure TfrmCadEmailXML.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  // btnExcluir.enabled  := not btnExcluir.enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := True;
  end;
end;

procedure TfrmCadEmailXML.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QRep.First;
    2:
      QRep.Prior;
    3:
      QRep.Next;
    4:
      QRep.Last;
  end;
  lblQuant.Caption := IntToStr(QRep.RecNo) + #13#10 +
    IntToStr(QRep.RecordCount);
  Screen.Cursor := crDefault;
end;

end.
