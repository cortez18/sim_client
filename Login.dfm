object FrmLogin: TFrmLogin
  Left = 498
  Top = 301
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Acesso SIM'
  ClientHeight = 187
  ClientWidth = 292
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100001000400E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000007070000000000
    0000000000000000707070707000000000000000000000070707070707000000
    7000007070700070707070707070000707070707070700070707070707000070
    7070707070707070707070707070070707070707070707070707070007077070
    7070707070707070707070000070070707070707070707070707070007070070
    7070707070707070707070707070000000000000000700070707070707000000
    000000000070007070000000707000000000000000000007007B7B7007000000
    0000000000000000B7B7B7B7B0000000000000000000000B7B7B7B7B7B000000
    7700077770070007B7B7B7B7B7000000000000000000007B7B7B7B000B7000B7
    B7B7B7B7B7B7B7B7B7B7B00000B00B7B7B7B7B7B7B7B7B7B7B7B700000700BBB
    BBBBBBBBBBB7B7B7B7B7B07770B0000000000000000B007B7B7B7B000B700000
    00000000000B0007B7B7B7B7B7000000000000000000000B7B7B7B7B7B000000
    0000000000000000B7B7B7B7B00000000000000000000000007B7B7000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFEBFFFFFF557FFFFEAABF7D5D555EAAAEAABD555
    5555AAAAAABA5555557DAAAAAABAD5555555FFFEEAABFFFDD415FFFFE003FFFF
    E003F384C001E0004001C000000080000038000000380000000080000000FFFC
    4001FFFCC001FFFFE003FFFFF007FFFFFC1FFFFFFFFFFFFFFFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 3
    Width = 36
    Height = 13
    Caption = 'Usu'#225'rio'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 15
    Top = 92
    Width = 20
    Height = 13
    Caption = 'Filial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 15
    Top = 51
    Width = 31
    Height = 13
    Caption = 'Senha'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object edUsuario: TEdit
    Left = 15
    Top = 19
    Width = 206
    Height = 21
    CharCase = ecUpperCase
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 30
    ParentFont = False
    TabOrder = 0
    OnEnter = edUsuarioEnter
    OnExit = edUsuarioExit
  end
  object btnOk: TBitBtn
    Left = 31
    Top = 149
    Width = 75
    Height = 25
    Caption = '&Ok'
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      555555555555555555555555555555555555555555FF55555555555559055555
      55555555577FF5555555555599905555555555557777F5555555555599905555
      555555557777FF5555555559999905555555555777777F555555559999990555
      5555557777777FF5555557990599905555555777757777F55555790555599055
      55557775555777FF5555555555599905555555555557777F5555555555559905
      555555555555777FF5555555555559905555555555555777FF55555555555579
      05555555555555777FF5555555555557905555555555555777FF555555555555
      5990555555555555577755555555555555555555555555555555}
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 3
    OnClick = btnOkClick
  end
  object btnCancelar: TBitBtn
    Left = 126
    Top = 149
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
      993337777F777F3377F3393999707333993337F77737333337FF993399933333
      399377F3777FF333377F993339903333399377F33737FF33377F993333707333
      399377F333377FF3377F993333101933399377F333777FFF377F993333000993
      399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
      99333773FF777F777733339993707339933333773FF7FFF77333333999999999
      3333333777333777333333333999993333333333377777333333}
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 4
    OnClick = btnCancelarClick
  end
  object IdSite: TJvDBLookupEdit
    Left = 15
    Top = 111
    Width = 258
    Height = 21
    DropDownCount = 20
    LookupDisplay = 'FILIAL'
    LookupField = 'CODFIL'
    LookupSource = dtsSite
    TabOrder = 2
    Text = ''
  end
  object edSenha: TEdit
    Left = 15
    Top = 67
    Width = 206
    Height = 21
    CharCase = ecUpperCase
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 30
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 1
    OnEnter = edSenhaEnter
    OnExit = edSenhaExit
  end
  object QSenha: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select login, nm_usuario nome, site, cod_usuario'
      'from tb_usuarios'
      'where login = :0'
      'and fl_status = '#39'A'#39)
    Left = 120
    Top = 116
    object QSenhaLOGIN: TStringField
      FieldName = 'LOGIN'
    end
    object QSenhaNOME: TStringField
      FieldName = 'NOME'
      Size = 40
    end
    object QSenhaSITE: TStringField
      FieldName = 'SITE'
      Size = 50
    end
    object QSenhaCOD_USUARIO: TBCDField
      FieldName = 'COD_USUARIO'
      Precision = 32
      Size = 0
    end
  end
  object QSite: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct f.codfil, (f.codfil || '#39' - '#39' || f.nomeab || '#39' - ' +
        #39' ||f.codcgc) filial, m.estado UF'
      
        'from cyber.rodfil f left join cyber.rodmun m on f.codmun = m.cod' +
        'mun'
      
        '                    left join tb_empresa e on e.codfil = f.codfi' +
        'l'
      'where e.usar = '#39'S'#39
      'and f.codfil in (1,2)'
      'order by 1')
    Left = 32
    Top = 112
    object QSiteCODFIL: TBCDField
      FieldName = 'CODFIL'
      Precision = 32
    end
    object QSiteFILIAL: TStringField
      FieldName = 'FILIAL'
      ReadOnly = True
      Size = 106
    end
    object QSiteUF: TStringField
      FieldName = 'UF'
      ReadOnly = True
      Size = 2
    end
  end
  object dtsSite: TDataSource
    DataSet = QSite
    Left = 64
    Top = 112
  end
  object QPass: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 7
        Value = 'PCORTEZ'
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 2
        Value = 91
      end
      item
        Name = '2'
        DataType = ftString
        Size = 9
        Value = 'PAULO1304'
      end>
    SQL.Strings = (
      'select senha from tb_usuarios '
      'where login = :0'
      'and senha = (select '
      'passwd_encrypt(:1,:2) from dual)')
    Left = 84
    Top = 36
  end
  object QParametro: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from tb_parametro_sim')
    Left = 164
    Top = 40
    object QParametroPDF: TStringField
      FieldName = 'PDF'
      Size = 70
    end
    object QParametroSCHEMMA: TStringField
      FieldName = 'SCHEMMA'
      Size = 70
    end
    object QParametroPDF_CTE: TStringField
      FieldName = 'PDF_CTE'
      Size = 70
    end
    object QParametroCONTROLADORIA: TStringField
      FieldName = 'CONTROLADORIA'
      Size = 70
    end
    object QParametroSEGURADORA: TStringField
      FieldName = 'SEGURADORA'
      Size = 70
    end
  end
end
