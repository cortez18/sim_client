unit ConsultaCte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit;

type
  TfrmConsultaCte = class(TForm)
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    Label1: TLabel;
    edCtrc: TJvCalcEdit;
    CBO: TComboBox;
    Label2: TLabel;
    Gauge1: TGauge;
    Panel1: TPanel;
    Label6: TLabel;
    ledIdDestino: TJvDBLookupEdit;
    ledDestino: TJvDBLookupEdit;
    QDestino: TADOQuery;
    DataSource2: TDataSource;
    Label7: TLabel;
    edManifesto: TJvCalcEdit;
    Label5: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    JvDBGrid1: TJvDBGrid;
    Panel2: TPanel;
    Label8: TLabel;
    edFatura: TJvCalcEdit;
    Label9: TLabel;
    edSerie: TEdit;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    QDestinoNM_CLIENTE: TStringField;
    QDestinoNR_CNPJ_CPF: TStringField;
    DBNavigator1: TDBNavigator;
    QClienteCOD_CLIENTE: TBCDField;
    QDestinoCOD_CLIENTE: TBCDField;
    QCtrcNR_CONHECIMENTO: TBCDField;
    QCtrcFL_TIPO: TStringField;
    QCtrcDT_CONHECIMENTO: TDateTimeField;
    QCtrcVL_TOTAL_IMPRESSO: TFloatField;
    QCtrcNM_USUARIO: TStringField;
    QCtrcNM_CLI_OS: TStringField;
    QCtrcNR_FATURA: TBCDField;
    QCtrcNR_MANIFESTO: TBCDField;
    QCtrcFL_EMPRESA: TBCDField;
    QCtrcNR_SERIE: TStringField;
    QCtrcNM_DESTINATARIO: TStringField;
    QCtrcNM_REMETENTE: TStringField;
    QCtrcCOD_CONHECIMENTO: TBCDField;
    Label4: TLabel;
    edNF: TJvCalcEdit;
    QCtrcCTE_STATUS: TBCDField;
    Label12: TLabel;
    edRoma: TJvCalcEdit;
    QCtrcMANIFESTO: TBCDField;
    QCtrcNR_ROMANEIO: TBCDField;
    QCtrcNOTFIS: TStringField;
    Label3: TLabel;
    edOCS: TJvCalcEdit;
    Label13: TLabel;
    edOC: TJvCalcEdit;
    Label15: TLabel;
    QCtrcNRO_OCS: TBCDField;
    QCtrcNR_OC: TBCDField;
    QCtrcPEDIDO: TStringField;
    edPedido: TJvMaskEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dtInicialEnter(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure ledDestinoCloseUp(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaCte: TfrmConsultaCte;

implementation

uses Dados, Menu, CT_e_Gerados, funcoes, Dacte_Cte_2;

{$R *.dfm}

procedure TfrmConsultaCte.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  Memo1: TStringList;
begin
  sarquivo := DirectoryListBox1.Directory + '\' + ApCarac(DateToStr(date)) +
    glbuser + '.xls';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].Text + '</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  Memo1.Add('Relat�rio de CT-e�s e NFS Emitidos');
  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QCtrc.First;
  Gauge1.MaxValue := QCtrc.RecordCount;
  while not QCtrc.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCtrc.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;

end;

procedure TfrmConsultaCte.btRelatorioClick(Sender: TObject);
var
  x: Integer;
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Screen.Cursor := crHourglass;
  Panel1.Visible := true;
  Application.ProcessMessages;
  x := 0;
  QCtrc.close;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QCtrc.SQL[13] := 'and c.dt_conhecimento between to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtInicial.date) + ' 00:00:01' + #39 +
      ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 +
      formatdatetime('dd/mm/yy', dtFinal.date) + ' 23:59:59' + #39 +
      ',''dd/mm/yy hh24:mi:ss'')'
  else
    QCtrc.SQL[13] := '';

  if (edCtrc.Value > 0) then
    QCtrc.SQL[15] := 'and c.nr_conhecimento = ' + #39 +
      IntToStr(edCtrc.AsInteger) + #39
  else
    QCtrc.SQL[15] := '';

  if (edSerie.Text <> '') then
    QCtrc.SQL[16] := '  and nr_serie = ' + QuotedStr(edSerie.Text)
  else
    QCtrc.SQL[16] := '';

  if edManifesto.Value > 0 then
    QCtrc.SQL[17] := 'and c.nr_manifesto = ' +
      QuotedStr(ApCarac(edManifesto.Text))
  else
    QCtrc.SQL[17] := '';

  if edFatura.Value > 0 then
    QCtrc.SQL[18] := 'and c.nr_fatura = ' + QuotedStr(ApCarac(edFatura.Text))
  else
    QCtrc.SQL[18] := '';

  if CBO.Text <> 'TODAS' then
    QCtrc.SQL[20] := 'and c.fl_empresa = ' +
      QuotedStr(alltrim(copy(CBO.Text, 1, Pos('-', CBO.Text) - 1)))
  else
    QCtrc.SQL[20] := ' ';

  if Trim(ledIdCliente.LookupValue) <> '' then
    QCtrc.SQL[21] := 'AND c.cod_pagador = ' +
      QuotedStr(ledIdCliente.LookupValue)
  else
    QCtrc.SQL[21] := ' ';

  if Trim(ledIdDestino.LookupValue) <> '' then
    QCtrc.SQL[22] := 'AND c.cod_destinatario = ' +
      QuotedStr(ledIdDestino.LookupValue)
  else
    QCtrc.SQL[22] := ' ';

  if edNF.Text <> '' then
    QCtrc.SQL[23] := 'AND n.notfis = ' + QuotedStr(ApCarac(edNF.Text))
  else
    QCtrc.SQL[23] := ' ';

  if edRoma.Value > 0 then
    QCtrc.SQL[24] := 'AND r.nr_romaneio = ' + QuotedStr(ApCarac(edRoma.Text))
  else
    QCtrc.SQL[24] := ' ';

  if edOCS.Value > 0 then
    QCtrc.SQL[25] := 'AND oc.nro_ocs = ' + QuotedStr(ApCarac(edOCS.Text))
  else
    QCtrc.SQL[25] := ' ';

  if edOC.Value > 0 then
    QCtrc.SQL[26] := 'AND oc.nr_oc = ' + QuotedStr(ApCarac(edOC.Text))
  else
    QCtrc.SQL[26] := ' ';

  if edPedido.Text <> '' then
    QCtrc.SQL[27] := 'AND oc.pedido = ' + QuotedStr(edPedido.Text)
  else
    QCtrc.SQL[27] := ' ';
  Screen.Cursor := crDefault;
  // Qctrc.sql.SaveToFile('c:\sim\sql.text');
  QCtrc.open;
  Panel1.Visible := false;
end;

procedure TfrmConsultaCte.dtInicialEnter(Sender: TObject);
begin
  edCtrc.Value := 0;
end;

procedure TfrmConsultaCte.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
  QCtrc.close;
end;

procedure TfrmConsultaCte.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmConsultaCte.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
  edCtrc.Value := 0;
end;

procedure TfrmConsultaCte.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
  QDestino.close;
end;

procedure TfrmConsultaCte.FormCreate(Sender: TObject);
begin
  QCliente.open;
  QDestino.open;
  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.Clear;
  dtmdados.iQuery1.SQL.Add
    ('select distinct (fl_empresa ||''-''||f.nomeab) nm from tb_conhecimento c left join cyber.rodfil f on c.fl_empresa = f.codfil');
  dtmdados.iQuery1.open;
  CBO.Clear;
  CBO.Items.Add('TODAS');
  while not dtmdados.iQuery1.Eof do
  begin
    CBO.Items.Add(dtmdados.iQuery1.fieldbyname('nm').Value);
    dtmdados.iQuery1.Next;
  end;
  dtmdados.iQuery1.close;
  CBO.ItemIndex := 0;
end;

procedure TfrmConsultaCte.JvDBGrid1DblClick(Sender: TObject);
begin
  frmMenu.tag := QCtrcCOD_CONHECIMENTO.AsInteger;
  Application.CreateForm(TfrmCT_e_Gerados, frmCT_e_Gerados);
  frmCT_e_Gerados.ShowModal;
  frmCT_e_Gerados.Free;
  frmMenu.tag := 0;
end;

procedure TfrmConsultaCte.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (QCtrcNR_SERIE.Value = '1') and (QCtrcCTE_STATUS.Value <> 100) then
  begin
    JvDBGrid1.canvas.Brush.Color := clRed;
    JvDBGrid1.canvas.Font.Color := clWhite;
  end;
  JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol].field, State);
end;

procedure TfrmConsultaCte.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmConsultaCte.ledDestinoCloseUp(Sender: TObject);
begin
  ledIdDestino.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledDestino.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmConsultaCte.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
