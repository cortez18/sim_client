unit EtiquetaVolume;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvEdit,
  JvMemoryDataset;

type
  TfrmEtiquetaVolume = class(TForm)
    QCTe: TADOQuery;
    dtsCTe: TDataSource;
    QCTeNR_CONHECIMENTO: TFMTBCDField;
    QCTeVOLUME: TFMTBCDField;
    mdTemp: TJvMemoryData;
    mdTempCTe: TBCDField;
    mdTempescol: TIntegerField;
    mdTempTrip: TStringField;
    mdTempvolumes: TBCDField;
    QVolTrip: TADOQuery;
    QVolTripNR_CONHECIMENTO: TFMTBCDField;
    QVolTripTRIP: TStringField;
    QVolTripBOX: TStringField;
    QVolTripRAZSOC: TStringField;
    QVolTripCIDADE: TStringField;
    QVolTripESTADO: TStringField;
    QVolTripDATA_BIP: TDateTimeField;
    QVolTripLINHA: TFMTBCDField;
    QVolTripCTE_CHAVE: TStringField;
    QVolTripDESTINO: TStringField;
    QVolTripnf: TStringField;
    QVolTripFL_EMPRESA: TFMTBCDField;
    QVolTripID_TRIP: TStringField;
    mdTempaberto: TIntegerField;
    QCTeID_TRIP: TStringField;
    QCTeFL_EMPRESA: TFMTBCDField;
    sp_Box: TADOStoredProc;
    QVol: TADOQuery;
    QVolBOX: TStringField;
    QVolDATA_BIP: TDateTimeField;
    QVolLINHA: TFMTBCDField;
    Panel1: TPanel;
    edCTe: TJvCalcEdit;
    edSerie: TEdit;
    edTrip: TEdit;
    Label1: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    Gauge1: TGauge;
    Panel3: TPanel;
    JvDBGrid1: TJvDBGrid;
    btRelatorio: TBitBtn;
    btnEtiqueta: TBitBtn;
    lblQt: TLabel;
    QTrips: TADOQuery;
    QTripsID_TRIP: TStringField;
    QTripsQT: TFMTBCDField;
    btnEtiquetaRee: TBitBtn;
    QOrder: TADOQuery;
    QOrderORDCOM: TStringField;
    QVolTripORDER_CLI: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure btnEtiquetaClick(Sender: TObject);
    procedure QVolTripCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure mdTempFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QVolBeforeOpen(DataSet: TDataSet);
    procedure btnEtiquetaReeClick(Sender: TObject);
    procedure carregar;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtiquetaVolume: TfrmEtiquetaVolume;

implementation

uses Dados, Menu, funcoes, AlteraCteEtiqueta;

{$R *.dfm}

procedure TfrmEtiquetaVolume.btnEtiquetaClick(Sender: TObject);
var Arq : TextFile;
    Ini: TIniFile;
    impr,razao,nr_order : string;
    d,qt,i,seq, vol : integer;
begin
   Screen.Cursor := crHourGlass;
   razao := glbnmfilial;
   if glbimpr = '' then
    impr := 'C:/SIM/printetiqueta.txt'
   else
    impr := glbimpr;
    AssignFile(Arq,impr);
    ReWrite(Arq);
    Application.ProcessMessages;
    mdTemp.filtered := true;
    Application.ProcessMessages;
    mdTemp.First;
    while not mdTemp.eof do
    begin
      Gauge1.MaxValue := 0;
      // volumes da TRIP
      QVolTrip.Close;
      QVolTrip.Parameters[0].Value := mdTempTrip.AsString;
      QVolTrip.Open;
      qt := QVolTrip.RecordCount;
      i := 0;
      seq := 0;
      Gauge1.MaxValue := QVolTrip.RecordCount;
      while not QVolTrip.eof do
      begin
        i := i + 1;
        // volumes que est�o no cte independente da TRIP
        QVol.close;
        QVol.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
        //QVol.Parameters[1].Value := QVolumeID_TRIP.AsString;
        QVol.Open;
        QVol.Locate('box',QVolTripBOX.AsString,[]);
        seq := QVollinha.AsInteger;
        vol := QVol.RecordCount;
        QVol.close;

        if QVolTripORDER_CLI.AsString = 'S' then
        begin
          QOrder.close;
          QOrder.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
          QOrder.Parameters[1].Value := GLBFilial;
          QOrder.Open;
          nr_order := QOrderORDCOM.AsString;
          QOrder.close;
        end;


{        Writeln(Arq,''+'n');
        Writeln(Arq,''+'M0690');
        Writeln(Arq,''+'d');
        Writeln(Arq,''+'L');
        Writeln(Arq,'D11');
        Writeln(Arq,'R0000');
        Writeln(Arq,'ySW1');
        Writeln(Arq,'A2');
        Writeln(Arq,'1911S0102520015P009P009INTECOM');
        Writeln(Arq,'1911S0102520149P009P009'+DateTimeToStr(now)+' - 1');
        Writeln(Arq,'1911S0102520299P009P009'+QVolTripLINHA.AsString+'/'+IntToStr(qt));
        Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolTripDESTINO.AsString,1,30));
        Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolTripCIDADE.value,1,25)+'/'+copy(QVolTripESTADO.AsString,1,2));
        //Writeln(Arq,'1911S0101710015P009P009BOX : '+QVolumeBOX.AsString);
        Writeln(Arq,'1911S0101670015P018P018BOX : '+QVolTripBOX.AsString);
        Writeln(Arq,'1911S0101470089P009P009CT-e');
        Writeln(Arq,'1911S0101470291P009P009Volumes');
        Writeln(Arq,'1911S0101040015P030P030'+QVolTripNR_CONHECIMENTO.AsString);
        Writeln(Arq,'1911S0101060250P024P022'+IntToStr(seq)+' / '+ intToStr(vol));
        Writeln(Arq,'1X1100002470015L346001');
        Writeln(Arq,'1X1100001590015L346001');
        Writeln(Arq,'1X1100001940015L346001');
        Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolTripRAZSOC.AsString,1,30));
        Writeln(Arq,'1eE705100340012C'+QVolTripBOX.AsString);
        Writeln(Arq,'1911S0100110019P008P008NF: '+ QVolTripnf.AsString);
        Writeln(Arq,'1X1100000310016L346001');
        Writeln(Arq,'1911S0101590224P009P009'+ mdTempTrip.AsString);
        Writeln(Arq,'Q0001');
        Writeln(Arq,'E');
}
        Writeln(Arq,''+'n');
        Writeln(Arq,''+'M0690');
        //Writeln(Arq,''+'KW0401');
        Writeln(Arq,''+'L');
        Writeln(Arq,'D11');
        Writeln(Arq,'R0000');
        Writeln(Arq,'ySW1');
        Writeln(Arq,'A2');
        Writeln(Arq,'1911S0102520015P009P009INTECOM');
        Writeln(Arq,'1911S0102520149P009P009'+DateTimeToStr(now)+' - 1');
        Writeln(Arq,'1911S0102520299P009P009'+QVolTripLINHA.AsString+'/'+IntToStr(qt));
        Writeln(Arq,'1911S0102300015P009P009Destinatario : '+ copy(QVolTripDESTINO.AsString,1,30));
        Writeln(Arq,'1911S0102140015P009P009Cidade / UF : '+copy(QVolTripCIDADE.value,1,25)+'/'+copy(QVolTripESTADO.AsString,1,2));
        Writeln(Arq,'1911S0101730015P014P014BOX : '+QVolTripBOX.AsString);
        Writeln(Arq,'1911S0101280314P009P009Volumes');
        Writeln(Arq,'1911S0100910015P022P022'+QVolTripNR_CONHECIMENTO.AsString);
        Writeln(Arq,'1911S0100910316P020P018'+IntToStr(seq)+' / '+ intToStr(vol));
        Writeln(Arq,'1X1100002470015L346001');
        Writeln(Arq,'1X1100001420015L346001');
        Writeln(Arq,'1X1100001940015L346001');
        Writeln(Arq,'1911S0101990015P009P009Redespacho : '+ copy(QVolTripRAZSOC.AsString,1,30));
        Writeln(Arq,'1eA504500350019C'+QVolTripNR_CONHECIMENTO.AsString + '-' + QVolTripFL_EMPRESA.AsString);
        Writeln(Arq,'1911S0100110019P008P008NF: '+ QVolTripnf.AsString);
        Writeln(Arq,'1X1100000280020L346001');
        Writeln(Arq,'1911S0101470015P010P010ORDER:'+ nr_order);
        Writeln(Arq,'1911S0101280019P009P009CTE/BOX');
        Writeln(Arq,'1911S0101480219P009P009TRIP'+ mdTempTrip.AsString);

        Writeln(Arq,'Q0001');
        Writeln(Arq,'E');

        sp_Box.Parameters[0].Value :=  QVolTripNR_CONHECIMENTO.AsInteger;
        sp_Box.Parameters[1].Value :=  QVolTripBOX.AsString;
        sp_Box.Parameters[2].Value :=  seq;
        sp_box.ExecProc;
        Gauge1.AddProgress(1);
        QVolTrip.Next;
      end;
      QVolTrip.close;
      mdTemp.Next;
    end;
    CloseFile(Arq);
  Screen.Cursor := crDefault;
  if impr = 'C:/SIM/printetiqueta.txt' then
      //Winexec(PAnsiChar('notepad++.exe '+impr), Sw_Show);
    ShellExecute(Application.Handle, nil, PChar(impr),nil, nil, SW_SHOWNORMAL);
 // end
 // else
 //   ShowMessage('Este CT-e N�o teve todos os seus volumes recebidos !!');
  mdTemp.filtered := false;
  FormCreate(Sender);
end;

procedure TfrmEtiquetaVolume.btnEtiquetaReeClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar('frmEtiquetaVolume') then
    Exit;
  try
    Application.CreateForm(TfrmAlteraCteEtiqueta, frmAlteraCteEtiqueta);
    frmAlteraCteEtiqueta.ShowModal;
  finally
    frmAlteraCteEtiqueta.Free;
  end;
  carregar;
end;

procedure TfrmEtiquetaVolume.btRelatorioClick(Sender: TObject);
begin
  Gauge1.MaxValue := 0;
  lblqt.Caption := '';
  {
  if edCTe.Value > 0 then
  begin
    QCte.SQL[12] := 'and id_trip in (select distinct tr.id_trip ';
    QCte.SQL[13] := 'from dashboard.skf_tb_trip tr left join cyber.rodcli d on fnc_cnpj(d.codcgc) = tr.cust_number ';
    QCte.SQL[14] := '                              left join cyber.rodioc n on n.notfis = tr.invoice and d.codclifor = n.coddes ';
    QCte.SQL[15] := '                              left join cyber.rodocs o on o.codocs = n.codocs and o.filocs = n.filocs ';
    QCte.SQL[16] := '                              left join cyber.tb_coleta c on n.codocs = c.nro_ocs and c.remetente = o.codrem ';
    QCte.SQL[17] := '                              left join tb_conhecimento ct on (c.doc = ct.nr_conhecimento and c.filial = ct.fl_empresa) ';
    QCte.SQL[18] := 'where ct.nr_conhecimento = ' + QuotedStr(ApCarac(edCte.text))+' and fl_empresa = ' + IntToStr(glbfilial);
    QCte.SQL[19] := 'and c.doc is not null) ';
  end;

  QCte.SQL[20] := 'and fl_empresa = ' + IntToStr(glbfilial);

  if edTrip.Text <> '' then
    QCte.SQL[21] := 'And id_trip = ' + QuotedStr(edTrip.Text);
  QCte.Open;     }
  QTrips.close;
  QTrips.open;
  lblqt.Caption := IntToSTr(QTrips.RecordCount) + ' Trip�s';
  mdTemp.Close;
  mdTemp.open;

  //while not Qcte.Eof do
  while not Qtrips.Eof do
  begin
    mdTemp.Append;
    //mdTempCTe.Value := QCTeNR_CONHECIMENTO.AsInteger;
    mdTempTrip.Value := QTripsID_TRIP.AsString;// QCTeid_TRIP.AsString;
    //mdTempvolumes.Value := QCTeVOLUME.AsInteger;
    dtmdados.iQuery1.close;
    dtmdados.iQuery1.SQL.Clear;
    //dtmdados.iQuery1.SQL.Add('select fnc_tudo_conferido(:0, :1) sim from dual');
    dtmdados.iQuery1.SQL.Add('select fnc_tudo_conferido_trip(:0) sim from dual');
    dtmdados.iQuery1.Parameters[0].Value := QTripsID_TRIP.AsString;// QCTeNR_CONHECIMENTO.AsInteger;
    //dtmdados.iQuery1.Parameters[1].Value := GLBFilial;
    dtmdados.iQuery1.open;
    mdTempaberto.Value := dtmdados.iQuery1.FieldByName('sim').AsInteger;
    //if mdTempaberto.Value = 0 then
    //  mdTempescol.Value := 1
    //else
    //  mdTempescol.Value := 0;
    //Qcte.Next;
    QTrips.Next;
  end;
  //QCte.close;
  QTrips.Close;
  mdTemp.First;
end;

procedure TfrmEtiquetaVolume.carregar;
begin
  Qcte.Close;
  QVolTrip.Close;
  mdTemp.Close;
  sp_box.Close;

  Gauge1.MaxValue := 0;
  lblqt.Caption := '';

  QTrips.close;
  QTrips.open;
  lblqt.Caption := IntToSTr(QTrips.RecordCount) + ' Trip�s';
  mdTemp.Close;
  mdTemp.open;

  while not Qtrips.Eof do
  begin
    mdTemp.Append;
    mdTempTrip.Value := QTripsID_TRIP.AsString;
    mdTempCTe.Value  := QTripsQT.AsInteger;
    dtmdados.iQuery1.close;
    dtmdados.iQuery1.SQL.Clear;
    dtmdados.iQuery1.SQL.Add('select fnc_tudo_conferido_trip(:0) sim from dual');
    dtmdados.iQuery1.Parameters[0].Value := QTripsID_TRIP.AsString;
    dtmdados.iQuery1.open;
    mdTempaberto.Value := dtmdados.iQuery1.FieldByName('sim').AsInteger;
    QTrips.Next;
  end;
  QTrips.Close;
  mdTemp.First;
  btRelatorio.Visible := false;
end;

procedure TfrmEtiquetaVolume.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  mdTemp.close;
  Qtrips.close;
end;

procedure TfrmEtiquetaVolume.FormCreate(Sender: TObject);
begin
  carregar;
end;

procedure TfrmEtiquetaVolume.JvDBGrid1CellClick(Column: TColumn);
begin
  if not dtmdados.PodeInserir(name) then
    Exit;

  if (mdTempAberto.AsInteger = 0) then
  begin
    if (Column.Field = mdTempescol) then
    begin
      mdTemp.edit;
      if (mdTempEscol.IsNull) or (mdTempEscol.value = 0) then
      begin
        mdTempEscol.value := 1;
      end
      else
      begin
        mdTempEscol.value := 0;
      end;
      mdTemp.post;
    end;
  end;
end;

procedure TfrmEtiquetaVolume.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  try
    if (Column.Field = mdTempEscol) then
    begin
      JvDBGrid1.Canvas.FillRect(Rect);
      dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
      if mdTempEscol.value = 1 then
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
      else
        dtmdados.iml.Draw(JvDBGrid1.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    end;
  except
  end;
  if mdTempAberto.AsInteger > 0 then
  begin
    JvDBGrid1.canvas.Brush.color := clred;
    JvDBGrid1.canvas.font.color := clWhite;
    JvDBGrid1.DefaultDrawDataCell(Rect, JvDBGrid1.Columns[DataCol]
      .Field, State);
  end;
end;

procedure TfrmEtiquetaVolume.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not mdTemp.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmEtiquetaVolume.mdTempFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := mdTemp['escol'] = 1;
end;

procedure TfrmEtiquetaVolume.QVolBeforeOpen(DataSet: TDataSet);
begin
  QVol.Parameters[1].Value := GLBFilial;
end;

procedure TfrmEtiquetaVolume.QVolTripCalcFields(DataSet: TDataSet);
var d, t : Integer;
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.SQL.add('select notfis from tb_cte_nf where codcon = :0 and fl_empresa = :1');
  dtmdados.IQuery1.Parameters[0].Value := QVolTripNR_CONHECIMENTO.AsInteger;
  dtmdados.IQuery1.Parameters[1].Value := QVolTripFL_EMPRESA.AsInteger;
  dtmdados.IQuery1.Open;
  d := 0;
  t := 0;
  While not dtmdados.IQuery1.eof do
  begin
    d := d + 1;
    t := t + Length(dtmdados.IQuery1.FieldByName('notfis').AsString+'-');
    if t > 55 then
    begin
      QVolTripnf.Value := QVolTripnf.Value + '...';
      exit;
    end;
    if d = 1 then
      QVolTripnf.Value := dtmdados.IQuery1.FieldByName('notfis').AsString
    else
      QVolTripnf.Value := QVolTripnf.Value + '-'+ dtmdados.IQuery1.FieldByName('notfis').AsString;

    dtmdados.IQuery1.Next;
  end
end;

end.
