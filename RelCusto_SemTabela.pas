unit RelCusto_SemTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ShellApi, DB, ADODB, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Mask, JvExMask, JvToolEdit;

type
  TfrmRelCusto_SemTabela = class(TForm)
    QSemTabela: TADOQuery;
    dtsSemTabela: TDataSource;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    QSemTabelaTRANSP: TBCDField;
    QSemTabelaRAZSOC: TStringField;
    QSemTabelaDOC: TBCDField;
    QSemTabelaTIPO: TStringField;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    QSemTabelaOPERACAO: TStringField;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelCusto_SemTabela: TfrmRelCusto_SemTabela;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelCusto_SemTabela.BitBtn1Click(Sender: TObject);
var
  memo1: TStringList;
  buffer: string;
  i: integer;
begin
  if FileExists('RelSemCusto.html') then
    DeleteFile('RelSemCusto.html');
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QSemTabela.First;
  While not QSemTabela.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QSemTabela.fieldbyname
        (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
    memo1.Add('<tr>');
    QSemTabela.Next;
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('RelSemCusto.html');

  buffer := 'RelSemCusto.html';
  ShellExecute(Application.Handle, nil, PChar(buffer), nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmRelCusto_SemTabela.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    ShowMessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;

  QSemTabela.close;
  QSemTabela.SQL[2] := 'Where v.data between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QSemTabela.open;

  Panel1.Visible := false;
end;

procedure TfrmRelCusto_SemTabela.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QSemTabela.close;
end;

end.
