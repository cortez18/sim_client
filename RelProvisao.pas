unit RelProvisao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvToolEdit, DB, ADODB, jpeg,
  DBCtrls, JvDBControls, Mask, JvExMask, JvBaseEdits, Buttons, FileCtrl, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, DateUtils;

type
  TfrmRelProvisao = class(TForm)
    navnavig: TDataSource;
    Panel1: TPanel;
    QRepre: TADOQuery;
    Panel7: TPanel;
    JvDBGrid1: TJvDBGrid;
    Panel4: TPanel;
    Label2: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    btnGerarExcel: TBitBtn;
    Panel8: TPanel;
    btnExcel: TBitBtn;
    edCusto: TJvCalcEdit;
    edCalculado: TJvCalcEdit;
    QRepreRAZSOC: TStringField;
    QRepreTOTFRE: TBCDField;
    JvDBNavigator1: TJvDBNavigator;
    QRepreCUSTO: TBCDField;
    Panel2: TPanel;
    Label10: TLabel;
    dtInicial: TJvDateEdit;
    Label11: TLabel;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    QRepreFILIAL: TBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExcelClick(Sender: TObject);
    procedure btnGerarExcelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  frmRelProvisao: TfrmRelProvisao;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelProvisao.btnGerarExcelClick(Sender: TObject);
var
  sarquivo: String;
  memo1: TStringList;
  i: integer;
begin
  sarquivo := DirectoryListBox1.Directory + '\' + ApCarac(DateToStr(date)) +
    '_provisao.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QRepre.First;
  while not QRepre.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QRepre.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    QRepre.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  showmessage('Planilha salva em ' + sarquivo);
  Panel4.Visible := false;
end;

procedure TfrmRelProvisao.btRelatorioClick(Sender: TObject);
var
  d1, d2: string;
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
    d1 := formatdatetime('dd/mm/yy', dtInicial.date) + ' 00:00:01';
    d2 := formatdatetime('dd/mm/yy', dtFinal.date) + ' 23:59:59';
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QRepre.SQL[9] := 'and c.data between to_date(' + #39 + d1 + #39 +
    ',''dd/mm/yy hh24:mi:ss'') and to_date(' + #39 + d2 + #39 +
    ',''dd/mm/yy hh24:mi:ss'')';
  QRepre.Open;
  QRepre.DisableControls;
  while not QRepre.Eof do
  begin
    edCusto.value := edCusto.value + QRepreCUSTO.value;
    edCalculado.value := edCalculado.value + QRepreTOTFRE.value;
    QRepre.Next;
  end;
  QRepre.First;
  QRepre.EnableControls;
  Panel1.Visible := false;
end;

procedure TfrmRelProvisao.btnExcelClick(Sender: TObject);
begin
  Panel4.Visible := true;
end;

procedure TfrmRelProvisao.FormActivate(Sender: TObject);
begin
  dtInicial.date := StartOfTheMonth(date);
  dtFinal.date := EndOfTheMonth(date);
end;

procedure TfrmRelProvisao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QRepre.close;
end;

end.
