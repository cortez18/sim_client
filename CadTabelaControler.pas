unit CadTabelaControler;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, ActnList, JvExStdCtrls,
  JvCombobox, JvToolEdit, JvBaseEdits, Mask, JvExMask, JvMaskEdit, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, JvDBControls,
  JvExExtCtrls, JvImage, ComObj, ShellAPI, JvMemo, JvEdit, System.Actions,
  IdMessage, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP;

type
  TfrmCadTabelaControler = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    dtstabela: TDataSource;
    Detalhes: TTabSheet;
    Panel1: TPanel;
    dtsItem: TDataSource;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    DBGrid1: TJvDBUltimGrid;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    edTrans: TJvMaskEdit;
    Qcadtabela: TADOQuery;
    QTabelaCOD_CUSTO: TBCDField;
    QTabelaCOD_FORNECEDOR: TBCDField;
    QTabelaFL_LOCAL: TStringField;
    QTabelaDS_UF: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_PEDAGIO: TBCDField;
    QTabelaVL_OUTROS: TBCDField;
    QTabelaVL_OUTROS_REAIS: TBCDField;
    QTabelaVL_OUTROS_MINIMO: TBCDField;
    QTabelaVL_EXCEDENTE: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaVL_GRIS: TFloatField;
    QTabelaVL_GRIS_MINIMO: TFloatField;
    QTabelaDT_CADASTRO: TDateTimeField;
    QTabelaMODAL: TStringField;
    QTabelaNM_FORNECEDOR: TStringField;
    QTabelaLOCAL: TStringField;
    QItemCOD_TAB: TBCDField;
    QItemCOD_CUSTO: TBCDField;
    QItemNR_PESO_DE: TBCDField;
    QItemNR_PESO_ATE: TBCDField;
    QItemVL_RODOVIARIO: TBCDField;
    QItemDATAI: TDateTimeField;
    QItemDATAF: TDateTimeField;
    QItemID_REGISTRO: TStringField;
    QItemVR_CTRC: TBCDField;
    QItemVR_EXCED: TBCDField;
    Label14: TLabel;
    QTabelaFL_LOCAL_DES: TStringField;
    QTabelaDS_UF_DES: TStringField;
    QTabelaLOCAL_DES: TStringField;
    QTabelaVL_ENTREGA: TBCDField;
    cbImposto: TCheckBox;
    QTabelaFL_IMPOSTO: TStringField;
    QTabelaSEGURO_MINIMO: TFloatField;
    cbPeso: TCheckBox;
    QTabelaFL_PESO: TStringField;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnAprovar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Label20: TLabel;
    QTabelaSITE: TBCDField;
    QcadtabelaCOD_CUSTO: TBCDField;
    QcadtabelaCOD_FORNECEDOR: TBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_REAIS: TBCDField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TBCDField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QTabelaUSUARIO: TStringField;
    QcadtabelaUSUARIO: TStringField;
    edVeiculo: TJvMaskEdit;
    Label21: TLabel;
    QTabelaCODMUN: TBCDField;
    QcadtabelaCODMUN: TBCDField;
    Label1: TLabel;
    QTabelaCODMUNO: TBCDField;
    QcadtabelaCODMUNO: TBCDField;
    QTabelaVL_TDE: TBCDField;
    QTabelaVL_TR: TBCDField;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TBCDField;
    QTabelaVL_TDEP: TBCDField;
    QTabelaVL_TDEM: TBCDField;
    QcadtabelaVL_TDEP: TBCDField;
    QcadtabelaVL_TDEM: TBCDField;
    QTabelaVEICULO: TStringField;
    QTabelaDIAS: TBCDField;
    QcadtabelaDIAS: TBCDField;
    cbStatus: TCheckBox;
    QTabelaFL_STATUS: TStringField;
    QcadtabelaFL_STATUS: TStringField;
    Label28: TLabel;
    QTabelaOPERACAO: TStringField;
    QcadtabelaOPERACAO: TStringField;
    QTabelaVL_TDEMAX: TBCDField;
    QcadtabelaVL_TDEMAX: TBCDField;
    QTabelaCONTROLER: TStringField;
    QTabelaDT_CONTROLER: TDateTimeField;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    JvDBMaskEdit1: TJvDBMaskEdit;
    JvDBMaskEdit2: TJvDBMaskEdit;
    JvDBMaskEdit3: TJvDBMaskEdit;
    JvDBMaskEdit4: TJvDBMaskEdit;
    JvDBMaskEdit5: TJvDBMaskEdit;
    JvDBMaskEdit6: TJvDBMaskEdit;
    JvDBMaskEdit7: TJvDBMaskEdit;
    QTabelaCIDD: TStringField;
    QTabelaCIDO: TStringField;
    QcadtabelaDOC_CONTROLER: TStringField;
    OpenDialog: TOpenDialog;
    btnDcto: TBitBtn;
    btnVer: TBitBtn;
    QcadtabelaOBS: TStringField;
    QcadtabelaVL_TRP: TBCDField;
    QcadtabelaVL_TRM: TBCDField;
    QcadtabelaVL_HE: TBCDField;
    QTabelaOBS: TStringField;
    QTabelaVL_TRP: TBCDField;
    QTabelaVL_TRM: TBCDField;
    QTabelaVL_HE: TBCDField;
    Label4: TLabel;
    edFreteM: TJvCalcEdit;
    edPedagio: TJvCalcEdit;
    edOutros: TJvCalcEdit;
    edHE: TJvCalcEdit;
    Label33: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    edOutrosR: TJvCalcEdit;
    Label9: TLabel;
    edOutrosM: TJvCalcEdit;
    Label23: TLabel;
    edTR: TJvCalcEdit;
    Label31: TLabel;
    edTRp: TJvCalcEdit;
    Label32: TLabel;
    edTRm: TJvCalcEdit;
    Label10: TLabel;
    edExc: TJvCalcEdit;
    edTdeMax: TJvCalcEdit;
    edTDEm: TJvCalcEdit;
    edTDEp: TJvCalcEdit;
    edTDE: TJvCalcEdit;
    Label22: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label30: TLabel;
    edEntrega: TJvCalcEdit;
    EdGrisM: TJvCalcEdit;
    EdGris: TJvCalcEdit;
    edsegminimo: TJvCalcEdit;
    EdSeg: TJvCalcEdit;
    Label11: TLabel;
    Label19: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label18: TLabel;
    Label27: TLabel;
    Obs: TJvMemo;
    btnDesativar: TBitBtn;
    edUser: TJvEdit;
    edDtIns: TJvDateEdit;
    btnListagem: TBitBtn;
    Label15: TLabel;
    obsRepro: TJvMemo;
    Qcadtabelareprovado: TStringField;
    Label36: TLabel;
    edSuframa: TJvCalcEdit;
    Label41: TLabel;
    edTDA: TJvCalcEdit;
    Label42: TLabel;
    edTDAMin: TJvCalcEdit;
    Label43: TLabel;
    edTDAMax: TJvCalcEdit;
    Label37: TLabel;
    edPreceita: TJvCalcEdit;
    Label39: TLabel;
    edPorto: TJvCalcEdit;
    Label40: TLabel;
    edPortoMin: TJvCalcEdit;
    QTabelavl_sufra: TBCDField;
    QTabelavl_vd: TBCDField;
    QTabelavl_tdc: TBCDField;
    QTabelavl_porto: TBCDField;
    QTabelavl_portomin: TBCDField;
    QTabelavl_tda: TBCDField;
    QTabelavl_tdamin: TBCDField;
    QTabelavl_tdamax: TBCDField;
    IdSMTP1: TIdSMTP;
    IdMessage1: TIdMessage;
    qtelas: TADOQuery;
    qtelasDESCRICAO: TStringField;
    qtelasEMAIL: TStringField;
    Qcadtabelacontroler_repr: TStringField;
    Label49: TLabel;
    edPernoite: TJvCalcEdit;
    Label47: TLabel;
    edDiaria: TJvCalcEdit;
    cbRegiao: TComboBox;
    Label50: TLabel;
    QTabelaDOC_CONTROLER: TStringField;
    QTabelaREPROVADO: TStringField;
    QTabelaCONTROLER_REPR: TStringField;
    QTabelaVL_ARMAZENAGEM: TBCDField;
    QTabelaVL_AJUDANTES: TBCDField;
    QTabelaVL_PALETIZACAO: TBCDField;
    QTabelaVL_DIARIA: TBCDField;
    QTabelaVL_REENTREGA: TBCDField;
    QTabelaVL_PERNOITE: TBCDField;
    QTabelaVL_HORA_PARADA: TBCDField;
    QTabelaREGIAO: TStringField;
    QcadtabelaVL_SUFRA: TBCDField;
    QcadtabelaVL_VD: TBCDField;
    QcadtabelaVL_PORTO: TBCDField;
    QcadtabelaVL_PORTOMIN: TBCDField;
    QcadtabelaVL_TDC: TBCDField;
    QcadtabelaVL_TDA: TBCDField;
    QcadtabelaVL_TDAMIN: TBCDField;
    QcadtabelaVL_TDAMAX: TBCDField;
    QcadtabelaVL_ARMAZENAGEM: TBCDField;
    QcadtabelaVL_AJUDANTES: TBCDField;
    QcadtabelaVL_PALETIZACAO: TBCDField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    QcadtabelaREGIAO: TStringField;
    TabSheet1: TTabSheet;
    Panel9: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    Label34: TLabel;
    LBLCanhoto: TLabel;
    JvDBGrid1: TJvDBGrid;
    edPalet: TJvCalcEdit;
    edAjuda: TJvCalcEdit;
    edReentrega: TJvCalcEdit;
    edArmazem: TJvCalcEdit;
    edHrParada: TJvCalcEdit;
    edCanhoto: TJvCalcEdit;
    QGenItem: TADOQuery;
    QGenItemCOD_ID: TFMTBCDField;
    QGenItemCOD_CUSTO: TFMTBCDField;
    QGenItemVL_PERNOITE: TBCDField;
    QGenItemVL_VD: TBCDField;
    QGenItemVL_DIARIA: TBCDField;
    QGenItemVEICULO: TStringField;
    dsGenItem: TDataSource;
    QItemVL_MINIMO: TBCDField;
    QTabelavl_preceita: TBCDField;
    Qcadtabelaregiaoo: TStringField;
    Label35: TLabel;
    cbRegiaoO: TComboBox;

    procedure btnFecharClick(Sender: TObject);
    procedure btnAprovarClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure btnDctoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure QcadtabelaAfterOpen(DataSet: TDataSet);
    procedure btnDesativarClick(Sender: TObject);
    procedure btnListagemClick(Sender: TObject);
  private
    quant: integer;
  public
    { Public declarations }
  end;

var
  frmCadTabelaControler: TfrmCadTabelaControler;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadTabelaControler.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTabelaControler.btnVerClick(Sender: TObject);
var
  caminho: string;
begin
  if QcadtabelaDOC_CONTROLER.AsString <> '' then
  begin
    caminho := glbcontroler + QcadtabelaDOC_CONTROLER.AsString;
    ShellExecute(Handle, nil, Pchar(caminho), nil, nil, SW_SHOWNORMAL);
  end
  else
    ShowMessage('Tabela Sem Documenta��o Vinculada !!');
end;

procedure TfrmCadTabelaControler.btnListagemClick(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
  wb: Variant;
  i: integer;
begin
  memo1 := TStringList.Create;
  texto := self.Caption;

  memo1.Clear();
  memo1.Add('<HTML><HEAD><TITLE>' + texto + '</TITLE>');
  memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add('</STYLE>');
  memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo1.Add('<table border=0><title></title></head><body>');
  memo1.Add('<table border=1 align=center>');
  memo1.Add('<tr>');
  memo1.Add('<th colspan=2><FONT class=titulo1>' + texto + '</th></font>');
  memo1.Add('<tr>');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to dbgContrato.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + dbgContrato.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QTabela.First;
  while not QTabela.Eof do
  begin
    memo1.Add('<tr>');
    for i := 0 to dbgContrato.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QTabela.fieldbyname
        (dbgContrato.Columns[i].FieldName).AsString + '</th>');
    QTabela.Next;
    memo1.Add('</tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('C:/SIM/ControlerCusto.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := true;
  wb.Navigate('C:/SIM/ControlerCusto.html');
end;

procedure TfrmCadTabelaControler.btnAprovarClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  if Application.Messagebox('Aprova esta Tabela ?', 'Aprovar Tabela de Custo',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Qcadtabela.edit;
    Qcadtabelareprovado.Value := '';
    QcadtabelaCONTROLER.Value := glbuser;
    QcadtabelaDT_CONTROLER.Value := now;
    Qcadtabela.Post;
  end;
end;

procedure TfrmCadTabelaControler.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo, altera: string;
begin
  caminho := glbcontroler;
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      arquivo := caminho + 'C_' + QcadtabelaCOD_CUSTO.AsString +
        ExtractFileName(OpenDialog.FileName);
      // verifica se existe este arquivo j� gravado, sim renomeia
      if FileExists(arquivo) then
      begin
        altera := copy(arquivo, 1, length(arquivo) - 4) + 'A' +
          apcarac(DateToStr(now())) + '.pdf';
        RenameFile(arquivo, altera);
      end;
      CopyFile(Pchar(ori), Pchar(des), true);

      RenameFile(des, arquivo);

      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.Add
        ('update tb_custofrac set doc_controler = :c where cod_custo = :n');
      dtmDados.IQuery1.Parameters[0].Value := 'C_' +
        QcadtabelaCOD_CUSTO.AsString + ExtractFileName(OpenDialog.FileName);
      dtmDados.IQuery1.Parameters[1].Value := QcadtabelaCOD_CUSTO.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadTabelaControler.btnDesativarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if length(obsRepro.Text) < 10 then
  begin
    ShowMessage
      ('� Obrigat�rio a Explica��o do Porque est� Sendo Desativada !!');
    Exit;
  end;

  if Application.Messagebox('Desativar esta Tabela ?',
    'Desativar Tabela de Custo', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Qcadtabela.edit;
    Qcadtabelareprovado.Value := obsRepro.Text;
    QcadtabelaFL_STATUS.Value := 'N';
    Qcadtabelacontroler_repr.Value := glbuser;
    Qcadtabela.Post;
  end;

  // Envio de email com a mensagem da tabela que foi reprovada pela controladoria
  qtelas.Open;
  IdMessage1.Recipients.EMailAddresses := qtelasEMAIL.AsString;
  IdMessage1.Subject := 'Tabela de Custo ' +
    IntToStr(QcadtabelaCOD_CUSTO.AsInteger) + ' foi Reprovada';
  IdMessage1.Body.Add('<html>');
  IdMessage1.Body.Add('    <body>');
  IdMessage1.Body.Add('<table border="1">');
  IdMessage1.Body.Add('<center><tr>');
  IdMessage1.Body.Add('<td bgcolor=''#00FFFF''>Numero da Tabela</td>');
  IdMessage1.Body.Add
    ('<td bgcolor=''#00FFFF''>Motivo da Reprova&ccedil;&atilde;o</td>');
  IdMessage1.Body.Add('<td bgcolor=''#00FFFF''>Reprovada por</td>');
  IdMessage1.Body.Add('</tr></center>');
  IdMessage1.Body.Add('<tr>');
  IdMessage1.Body.Add('<center><td>' + IntToStr(QcadtabelaCOD_CUSTO.AsInteger)
    + '</td>');
  IdMessage1.Body.Add('<td>' + obsRepro.Text + '</td></center>');
  IdMessage1.Body.Add('<td>' + glbuser + '</td></center>');
  IdMessage1.Body.Add('</tr>');
  IdMessage1.Body.Add('</table>');
  IdMessage1.Body.Add('</body>');
  IdMessage1.Body.Add('</html>');
  IdMessage1.MessageParts.Clear;
  qtelas.Close;

  IdSMTP1.Connect;
  Try
    IdSMTP1.Send(IdMessage1);
  Finally
    IdSMTP1.Disconnect;
  End;
end;

procedure TfrmCadTabelaControler.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QTabela.Close;
  if Pos('order by', QTabela.SQL.Text) > 0 then
  begin
    QTabela.SQL.Text := copy(QTabela.SQL.Text, 1,
      Pos('order by', QTabela.SQL.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.SQL.Text := QTabela.SQL.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTabelaControler.FormCreate(Sender: TObject);
begin
  QTabela.Open;
  QTabela.First;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadTabelaControler.QcadtabelaAfterOpen(DataSet: TDataSet);
begin
  if QcadtabelaDOC_CONTROLER.Value <> '' then
  begin
    btnVer.Visible := true;
  end
  else
  begin
    btnVer.Visible := false;
  end;
end;

procedure TfrmCadTabelaControler.QTabelaAfterScroll(DataSet: TDataSet);
begin
  Qcadtabela.Close;
  Qcadtabela.Parameters[0].Value := QTabelaCOD_CUSTO.Value;
  Qcadtabela.Open;
  QItem.Close;
  QItem.Parameters[0].Value := QTabelaCOD_CUSTO.Value;
  QItem.Open;

  edVeiculo.Text := QTabelaVEICULO.AsString;
  edTrans.Text := QTabelaNM_FORNECEDOR.Value;
  edFreteM.Value := QTabelaVL_FRETE_MINIMO.Value;
  edPedagio.Value := QTabelaVL_PEDAGIO.Value;
  edOutros.Value := QTabelaVL_OUTROS.Value;
  edOutrosR.Value := QTabelaVL_OUTROS_REAIS.Value;
  edOutrosM.Value := QTabelaVL_OUTROS_MINIMO.Value;
  edExc.Value := QTabelaVL_EXCEDENTE.Value;
  EdSeg.Value := QTabelaVL_AD.Value;
  EdGris.Value := QTabelaVL_GRIS.Value;
  EdGrisM.Value := QTabelaVL_GRIS_MINIMO.Value;
  edEntrega.Value := QTabelaVL_ENTREGA.Value;
  edsegminimo.Value := QcadtabelaSEGURO_MINIMO.Value;
  edTDE.Value := QTabelaVL_TDE.Value;
  edTDEp.Value := QTabelaVL_TDEP.Value;
  edTDEm.Value := QTabelaVL_TDEM.Value;
  edTR.Value := QcadtabelaVL_TR.Value;
  edTRp.Value := QcadtabelaVL_TRP.Value;
  edTRm.Value := QcadtabelaVL_TRM.Value;
  edTdeMax.Value := QcadtabelaVL_TDEMAX.Value;
  edHE.Value := QcadtabelaVL_HE.Value;
  // Implementa��o do projeto generalidades
  edArmazem.Value := QcadtabelaVL_ARMAZENAGEM.Value;
  edAjuda.Value := QcadtabelaVL_AJUDANTES.Value;
  edPalet.Value := QcadtabelaVL_PALETIZACAO.Value;
  edDiaria.Value := QcadtabelaVL_DIARIA.Value;
  edReentrega.Value := QcadtabelaVL_REENTREGA.Value;
  edPernoite.Value := QcadtabelaVL_PERNOITE.Value;
  edHrParada.Value := QcadtabelaVL_HORA_PARADA.Value;
  cbRegiao.Text := QcadtabelaREGIAO.Value;
  cbRegiaoO.Text := QcadtabelaREGIAOO.Value;

  if QTabelaFL_IMPOSTO.Value = 'S' then
    cbImposto.Checked := true
  else
    cbImposto.Checked := false;
  if QTabelaFL_PESO.Value = 'S' then
    cbPeso.Checked := true
  else
    cbPeso.Checked := false;

  if QTabelaFL_STATUS.Value = 'S' then
    cbStatus.Checked := true
  else
    cbStatus.Checked := false;

  edUser.Text := QcadtabelaUSUARIO.AsString;
  edDtIns.Date := QcadtabelaDT_CADASTRO.Value;

  Obs.Clear;
  Obs.Lines.Add(QcadtabelaOBS.AsString);

  obsRepro.Clear;
  obsRepro.Lines.Add(Qcadtabelareprovado.AsString);
  edSuframa.Value := QTabelavl_sufra.Value;
  edPreceita.Value := QTabelavl_preceita.Value;
  edCanhoto.Value := QTabelavl_tdc.Value;
  edPorto.Value := QTabelavl_porto.Value;
  edPortoMin.Value := QTabelavl_portomin.Value;
  edTDA.Value := QTabelavl_tda.Value;
  edTDAMin.Value := QTabelavl_tdamin.Value;
  edTDAMax.Value := QTabelavl_tdamax.Value;

  QGenItem.Close;
  QGenItem.Parameters[0].Value := QTabelaCOD_CUSTO.Value;
  QGenItem.Open;

end;

procedure TfrmCadTabelaControler.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
  QItem.Close;
  Qcadtabela.Close;
end;

procedure TfrmCadTabelaControler.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadTabelaControler.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
