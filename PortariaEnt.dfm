object frmPortariaEnt: TfrmPortariaEnt
  Left = 332
  Top = 256
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Portaria - Entrada de Ve'#237'culo'
  ClientHeight = 357
  ClientWidth = 825
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 825
    Height = 54
    Align = alTop
    TabOrder = 1
    object JvGradient1: TJvGradient
      Left = 1
      Top = 1
      Width = 823
      Height = 20
      Align = alTop
      Style = grVertical
      StartColor = 16245453
      EndColor = clBtnFace
      ExplicitWidth = 789
    end
    object lblqt: TLabel
      Left = 234
      Top = 24
      Width = 6
      Height = 20
      Caption = ' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 26
      Top = 5
      Width = 76
      Height = 13
      Caption = 'Tipo de Entrada'
      Transparent = True
    end
    object btnGerar: TBitBtn
      Left = 556
      Top = 15
      Width = 101
      Height = 25
      Caption = 'Gerar'
      Enabled = False
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
        FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
        96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
        92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
        BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
        CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
        D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
        EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
        E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
        E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
        8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
        E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
        FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
        C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
        CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
        CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
        D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
        9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 1
      TabStop = False
      OnClick = btnGerarClick
    end
    object cbTipo: TComboBox
      Left = 26
      Top = 24
      Width = 139
      Height = 21
      TabOrder = 0
      OnExit = cbTipoExit
      Items.Strings = (
        ''
        'Coleta'
        'Retira'
        'Entrega'
        'Devolu'#231#227'o'
        'Coleta/Devolu'#231#227'o')
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 54
    Width = 825
    Height = 303
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label11: TLabel
      Left = 556
      Top = 93
      Width = 31
      Height = 16
      Caption = 'CPF :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel
      Left = 63
      Top = 20
      Width = 39
      Height = 16
      Caption = 'Placa :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 136
      Width = 62
      Height = 16
      Caption = 'Motorista :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 10
      Top = 56
      Width = 91
      Height = 16
      Caption = 'Transportador :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 345
      Top = 93
      Width = 25
      Height = 16
      Caption = 'RG :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label9: TLabel
      Left = 68
      Top = 93
      Width = 33
      Height = 16
      Caption = 'CNH :'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label10: TLabel
      Left = 26
      Top = 195
      Width = 76
      Height = 16
      Caption = 'Observa'#231#227'o :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblKm: TLabel
      Left = 225
      Top = 18
      Width = 121
      Height = 20
      Caption = '                        '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 340
      Top = 20
      Width = 64
      Height = 16
      Caption = 'Manifesto :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 576
      Top = 20
      Width = 67
      Height = 16
      Caption = 'KM Ve'#237'culo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edPlaca: TJvMaskEdit
      Left = 132
      Top = 17
      Width = 85
      Height = 27
      AutoSelect = False
      Color = 15132390
      EditMask = '!>LLL-0A00;0;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      TabOrder = 0
      Text = ''
      OnExit = edPlacaExit
    end
    object edTransp: TJvMaskEdit
      Left = 132
      Top = 50
      Width = 461
      Height = 27
      TabStop = False
      Color = 15132390
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = ''
    end
    object edMotor: TJvMaskEdit
      Left = 132
      Top = 133
      Width = 461
      Height = 27
      CharCase = ecUpperCase
      Color = 15132390
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      Text = ''
    end
    object edCfp: TJvMaskEdit
      Left = 599
      Top = 87
      Width = 145
      Height = 27
      TabStop = False
      Color = 15132390
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Text = ''
      Visible = False
    end
    object edRG: TJvMaskEdit
      Left = 383
      Top = 87
      Width = 145
      Height = 27
      TabStop = False
      Color = 15132390
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Text = ''
      Visible = False
    end
    object edCNH: TJvMaskEdit
      Left = 132
      Top = 89
      Width = 145
      Height = 27
      TabStop = False
      Color = 15132390
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = ''
      Visible = False
    end
    object cbTransp: TComboBox
      Left = 133
      Top = 50
      Width = 461
      Height = 27
      Style = csDropDownList
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Visible = False
      Items.Strings = (
        'ESCOLHA O MODAL......')
    end
    object Memo1: TMemo
      Left = 132
      Top = 178
      Width = 628
      Height = 55
      MaxLength = 300
      TabOrder = 7
    end
    object edManifesto: TJvCalcEdit
      Left = 410
      Top = 14
      Width = 88
      Height = 28
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 8
      DecimalPlacesAlwaysShown = False
    end
    object edserie: TJvMaskEdit
      Left = 504
      Top = 14
      Width = 24
      Height = 28
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
      Text = ''
    end
    object edKm: TJvCalcEdit
      Left = 649
      Top = 14
      Width = 95
      Height = 28
      Color = clWhite
      DecimalPlaces = 0
      DisplayFormat = ',0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 10
      DecimalPlacesAlwaysShown = False
    end
  end
  object RLReport1: TRLReport
    Left = 23
    Top = 1000
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel2: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel12: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel15: TRLLabel
        Left = 264
        Top = 27
        Width = 157
        Height = 16
        Caption = 'REGISTRO DE ENTRADA'
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 676
        Top = 6
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
      object RLLabel5: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLLabel22: TRLLabel
        Left = 9
        Top = 103
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel3: TRLLabel
        Left = 666
        Top = 27
        Width = 48
        Height = 14
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel10: TRLLabel
        Left = 9
        Top = 128
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLMemo2: TRLMemo
        Left = 12
        Top = 152
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
  end
  object JvEnterAsTab1: TJvEnterAsTab
    Left = 400
    Top = 232
  end
  object QEntrada: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from tb_portaria')
    Left = 308
    Top = 4
    object QEntradaREG: TBCDField
      FieldName = 'REG'
      Precision = 32
      Size = 0
    end
    object QEntradaTIPO: TBCDField
      FieldName = 'TIPO'
      Precision = 32
      Size = 0
    end
    object QEntradaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
      Size = 0
    end
    object QEntradaPLACA: TStringField
      FieldName = 'PLACA'
      Size = 7
    end
    object QEntradaCOD_TRANSP: TBCDField
      FieldName = 'COD_TRANSP'
      Precision = 32
      Size = 0
    end
    object QEntradaCOD_MOTORISTA: TBCDField
      FieldName = 'COD_MOTORISTA'
      Precision = 32
      Size = 0
    end
    object QEntradaDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QEntradaKM: TBCDField
      FieldName = 'KM'
      Precision = 32
      Size = 0
    end
    object QEntradaMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QEntradaOBS: TStringField
      FieldName = 'OBS'
      Size = 300
    end
    object QEntradaUSUARIO: TBCDField
      FieldName = 'USUARIO'
      Precision = 32
      Size = 0
    end
  end
  object iml: TImageList
    Left = 16
    Top = 160
    Bitmap = {
      494C010102000400180110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = QTranspBeforeOpen
    Parameters = <>
    SQL.Strings = (
      'SELECT distinct t.name NM_FORNECEDOR'
      'FROM spediteure t where t.lager = '#39'MAR'#39' and stat = '#39'00'#39
      'and length(t.name) > 5'
      ''
      'union'
      ''
      'SELECT C.NOMEAB NM_FORNECEDOR'
      'FROM cyber.RODCLI C  '
      'WHERE C.CODPAD IN (1)  '
      'AND c.situac = '#39'A'#39' and UPPER(C.CLASSI) IN ('#39'4'#39','#39'9'#39','#39'11'#39')'
      ''
      'order by 1')
    Left = 384
    Top = 4
    object QTranspNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      Size = 50
    end
  end
  object SPEntrada: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_PORTARIA_ENTRADA'
    Parameters = <
      item
        Name = 'VMAN'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VTIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VFORN'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VMOTO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VEMP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VKM'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VUSER'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = 16
        Value = '0'
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'VCPF'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VRG'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VCNH'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'vserie'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 672
    Top = 80
  end
end
