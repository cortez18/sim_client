object frmAuditoria: TfrmAuditoria
  Left = 0
  Top = 0
  Caption = 'Auditoria'
  ClientHeight = 562
  ClientWidth = 1084
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 244
    Top = 12
    Width = 67
    Height = 13
    Caption = 'Campo Chave'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btnAlterar: TBitBtn
    Left = 337
    Top = 18
    Width = 75
    Height = 25
    Caption = 'Procurar'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF145D95105A92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22669E1B629A2267
      9D115B93FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF3272AA2B6DA5558DBC89B5DD185F97FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF447FB73C79B16497C59DC1E46699
      C71F659DFFFFFFFFFFFFFFFFFFFFFFFFDDB28FD9AE8AD6A985D3A57FD0A07BCD
      9C76A2938A75A2CCABCBE876A4CE3070A8286BA3FFFFFFFFFFFFFFFFFFE4BD9B
      E1B896E8C9AEF5E1CDF7E5D3F7E5D1F3DDC8DFBA9CC7A89186AED5417DB53977
      AFFFFFFFFFFFFFFFFFFFFFFFFFE8C3A2EDD0B7F8E8D9F5DEC8F3D8BDF3D6BBF4
      DBC2F7E4D2DFBB9D9D94924B84BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECC8A8
      F7E7D7F6E1CCF4DBC2F4DAC0F3D8BDF3D7BBF4DBC2F3DEC9CD9F7BFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFF0CEAEF9ECDFF5DFC8F5DDC6F4DCC3F4DAC1F3
      D9BEF3D7BDF8E6D3D3A57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D3B4
      F9EDE1F6E1CCF5DFC9F5DEC7F4DCC4F4DBC2F4DAC0F8E7D6D7AA86FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFF7D7B9F9EBDEF7E7D6F6E1CCF5E0CAF5DEC8F5
      DDC5F6E1CBF5E2D0DBB08CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADBBD
      F8E2CCFAEEE3F7E7D6F6E2CEF6E1CBF6E3D0F9EADDECCFB5DFB693FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFCDEC0FADBBEF9E2CDFAECDEF9EEE2F9EDE2F8
      E9DAF0D5BDE7C09FE3BC9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FCDEC1FADCBFF9D9BBF6D6B8F4D3B4F1CFAFEECBABEBC6A6FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 2
    OnClick = btnAlterarClick
  end
  object edProcura: TJvCalcEdit
    Left = 244
    Top = 28
    Width = 73
    Height = 21
    DecimalPlaces = 0
    DisplayFormat = '0'
    ShowButton = False
    TabOrder = 1
    DecimalPlacesAlwaysShown = False
  end
  object RGDoc: TRadioGroup
    Left = 2
    Top = 2
    Width = 236
    Height = 558
    Caption = 'Tabela'
    ItemIndex = 0
    Items.Strings = (
      'CTRC'
      'NF Servi'#231'o'
      'Romaneio'
      'Tabela de Custo')
    TabOrder = 0
  end
  object DBGrid1: TDBGrid
    Left = 242
    Top = 61
    Width = 842
    Height = 293
    DataSource = dtsAudit
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TABELA'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ACAO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CHAVE_INT'
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CHAVE_CHAR'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO1'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO2'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO3'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO4'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'MAQUINA'
        Width = 226
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USUARIO'
        Width = 122
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DAT_INC'
        Visible = True
      end>
  end
  object DBRichEdit1: TDBRichEdit
    Left = 238
    Top = 360
    Width = 210
    Height = 200
    DataField = 'DESCRICAO1'
    DataSource = dtsAudit
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    TabOrder = 4
    Zoom = 100
  end
  object DBRichEdit2: TDBRichEdit
    Left = 448
    Top = 360
    Width = 210
    Height = 200
    DataField = 'DESCRICAO2'
    DataSource = dtsAudit
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    TabOrder = 5
    Zoom = 100
  end
  object DBRichEdit3: TDBRichEdit
    Left = 659
    Top = 360
    Width = 210
    Height = 200
    DataField = 'DESCRICAO3'
    DataSource = dtsAudit
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    TabOrder = 6
    Zoom = 100
  end
  object DBRichEdit4: TDBRichEdit
    Left = 870
    Top = 360
    Width = 210
    Height = 200
    DataField = 'DESCRICAO4'
    DataSource = dtsAudit
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    TabOrder = 7
    Zoom = 100
  end
  object DBNavigator1: TDBNavigator
    Left = 444
    Top = 26
    Width = 556
    Height = 25
    DataSource = dtsAudit
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 8
  end
  object QAudit: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from tb_auditoria_sim'
      'where tabela is not null'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by dat_inc')
    Left = 48
    Top = 8
    object QAuditCOD_AUDITORIA: TBCDField
      FieldName = 'COD_AUDITORIA'
      Precision = 32
      Size = 0
    end
    object QAuditTABELA: TStringField
      FieldName = 'TABELA'
      FixedChar = True
      Size = 30
    end
    object QAuditACAO: TStringField
      FieldName = 'ACAO'
      FixedChar = True
      Size = 1
    end
    object QAuditCHAVE_INT: TBCDField
      FieldName = 'CHAVE_INT'
      Precision = 32
      Size = 0
    end
    object QAuditCHAVE_CHAR: TStringField
      FieldName = 'CHAVE_CHAR'
      Size = 10
    end
    object QAuditDESCRICAO1: TStringField
      FieldName = 'DESCRICAO1'
      Size = 4000
    end
    object QAuditDESCRICAO2: TStringField
      FieldName = 'DESCRICAO2'
      Size = 4000
    end
    object QAuditDESCRICAO3: TStringField
      FieldName = 'DESCRICAO3'
      Size = 4000
    end
    object QAuditDESCRICAO4: TStringField
      FieldName = 'DESCRICAO4'
      Size = 4000
    end
    object QAuditMAQUINA: TStringField
      FieldName = 'MAQUINA'
      Size = 200
    end
    object QAuditUSUARIO: TStringField
      FieldName = 'USUARIO'
      Size = 200
    end
    object QAuditDAT_INC: TDateTimeField
      FieldName = 'DAT_INC'
    end
  end
  object dtsAudit: TDataSource
    DataSet = QAudit
    Left = 92
    Top = 12
  end
end
