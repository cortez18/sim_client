object frmRelOR_Entrada: TfrmRelOR_Entrada
  Left = 0
  Top = 0
  Caption = 'Rela'#231#227'o de OR WMS x NF Entrada Rodopar'
  ClientHeight = 544
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 201
    Top = 1
    Width = 38
    Height = 13
    Caption = 'Cliente :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Gauge1: TGauge
    Left = 204
    Top = 519
    Width = 448
    Height = 23
    Progress = 0
  end
  object Label8: TLabel
    Left = 491
    Top = 39
    Width = 47
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 4
    Top = 0
    Width = 53
    Height = 13
    Caption = 'Data Inicial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 101
    Top = 0
    Width = 48
    Height = 13
    Caption = 'Data Final'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btRelatorio: TBitBtn
    Left = 380
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Gerar'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 1
    OnClick = btRelatorioClick
  end
  object ledIdCliente: TJvDBLookupEdit
    Left = 201
    Top = 16
    Width = 145
    Height = 21
    LookupDisplay = 'NM_CLIENTE'
    LookupField = 'NM_CLIENTE'
    LookupSource = dtsCliente
    TabOrder = 0
    Text = ''
  end
  object btnExcel: TBitBtn
    Left = 491
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Excel'
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
      346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
      2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
      33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
      EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
      38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
      3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
      D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
      40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
      73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
      33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
      64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
      33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
      33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
      3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
      D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
      49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
      CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
      53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
      C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
      BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
      62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
      56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
      45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
      4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
      BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
      4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
      B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
      49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
      ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
      4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
      4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = btnExcelClick
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 43
    Width = 485
    Height = 470
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'TIME_AEN'
        Title.Caption = 'Data'
        Width = 107
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'NR_LIEFERSCHEIN'
        Title.Caption = 'OR WMS'
        Width = 107
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATINC'
        Title.Caption = 'Data OS'
        Width = 104
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODOSA'
        Title.Caption = 'OS Rodopar'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOTFIS'
        Title.Caption = 'NF'
        Width = 65
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 52
    Top = 184
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 490
    Top = 76
    Width = 165
    Height = 437
    TabOrder = 5
  end
  object DriveComboBox1: TDriveComboBox
    Left = 490
    Top = 58
    Width = 165
    Height = 19
    DirList = DirectoryListBox1
    Enabled = False
    TabOrder = 6
  end
  object Memo1: TMemo
    Left = 40
    Top = 240
    Width = 289
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Visible = False
  end
  object dtInicial: TJvDateEdit
    Left = 4
    Top = 16
    Width = 90
    Height = 21
    Hint = 'Data de Finaliza'#231#227'o de OR'
    ParentShowHint = False
    ShowHint = True
    ShowNullDate = False
    TabOrder = 8
  end
  object dtFinal: TJvDateEdit
    Left = 101
    Top = 16
    Width = 90
    Height = 21
    Hint = 'Data de Finaliza'#231#227'o de OR'
    ParentShowHint = False
    ShowHint = True
    ShowNullDate = False
    TabOrder = 9
  end
  object JvDBNavigator1: TJvDBNavigator
    Left = 7
    Top = 518
    Width = 188
    Height = 25
    DataSource = navnavig
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    TabOrder = 10
  end
  object QEntrada: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 7283
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = 5
        Value = 7283
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = 16
        Value = 7283
      end
      item
        Name = '3'
        DataType = ftString
        Size = 5
        Value = 'CLESS'
      end>
    SQL.Strings = (
      'SELECT distinct b.nr_lieferschein , B.time_neu, b.time_aen,'
      '(select distinct codosa from cyber.wmsosa o'
      
        'where o.refcli = to_char(b.nr_lieferschein) and o.codclifor = :0' +
        ' and situac <> '#39'C'#39' and o.TIPDOC= '#39'E'#39
      'and rownum=1) codosa,'
      '(select distinct datinc from cyber.wmsosa o'
      
        'where o.refcli = to_char(b.nr_lieferschein) and o.codclifor = :1' +
        ' and situac <> '#39'C'#39' and o.TIPDOC= '#39'E'#39
      'and rownum=1) datinc,'
      '(select distinct i.notfis '
      
        'from cyber.wmsosa o left join cyber.WMSIOS I on i.codosa = o.cod' +
        'osa and i.filosa = o.codfil '
      
        'left join cyber.wmsmda d on d.numdoc = i.notfis and d.codclifor ' +
        '= o.codclifor and d.codfil = o.codfil'
      
        'where o.refcli = to_char(b.nr_lieferschein) and o.codclifor = :2' +
        ' and o.situac <> '#39'C'#39' and o.TIPDOC= '#39'E'#39' and rownum=1'
      'and i.ID_IOS > 0 AND i.FILOSA = 1) notfis'
      'FROM we B'
      'WHERE B.ID_KLIENT = :3'
      'and b.stat = '#39'90'#39
      
        'and B.time_aen between to_date('#39'01/12/13 00:00'#39','#39'dd/MM/yy HH24:m' +
        'i'#39') and to_date('#39'10/12/13 23:59'#39','#39'dd/MM/yy HH24:mi'#39')'
      'order by 3,1')
    Left = 56
    Top = 176
    object QEntradaNR_LIEFERSCHEIN: TStringField
      FieldName = 'NR_LIEFERSCHEIN'
      ReadOnly = True
      Size = 12
    end
    object QEntradaTIME_NEU: TDateTimeField
      FieldName = 'TIME_NEU'
      ReadOnly = True
    end
    object QEntradaTIME_AEN: TDateTimeField
      FieldName = 'TIME_AEN'
      ReadOnly = True
    end
    object QEntradaCODOSA: TBCDField
      FieldName = 'CODOSA'
      ReadOnly = True
      Precision = 32
    end
    object QEntradaDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
    end
    object QEntradaNOTFIS: TStringField
      FieldName = 'NOTFIS'
      ReadOnly = True
      Size = 15
    end
  end
  object navnavig: TDataSource
    DataSet = QEntrada
    Left = 100
    Top = 176
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QClienteBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      'select distinct c.nome_wms nm_cliente, codclifor, c.base_wms'
      
        'from tb_limite_faturamento c left join cyber.rodcli cl on cl.cod' +
        'clifor = c.codcliente'
      
        '                             left join tb_empresa e on e.lager =' +
        ' c.site_wms '
      'where e.codfil = :0'
      'order by 1')
    Left = 20
    Top = 104
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object QClienteBASE_WMS: TStringField
      FieldName = 'BASE_WMS'
      ReadOnly = True
    end
  end
  object dtsCliente: TDataSource
    DataSet = QCliente
    Left = 76
    Top = 104
  end
end
