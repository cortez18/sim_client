unit Manifesto_serieU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg, RLRichText, Vcl.Imaging.pngimage, RLParser, Data.DB,
  JvMemoryDataset;

type
  TfrmManifesto_serieU = class(TForm)
    RlManifesto: TRLReport;
    RLBand5: TRLBand;
    RLLogoInt: TRLImage;
    RLLogoIw: TRLImage;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel35: TRLLabel;
    RLLabel36: TRLLabel;
    RLDBText17: TRLDBText;
    RLDBText18: TRLDBText;
    RLDBText19: TRLDBText;
    RLDBText20: TRLDBText;
    RLDBText21: TRLDBText;
    RLDBText22: TRLDBText;
    RLLabel46: TRLLabel;
    RLBand6: TRLBand;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBText15: TRLDBText;
    RLDBMemo1: TRLDBMemo;
    RLBand7: TRLBand;
    RLLabel37: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel39: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDraw2: TRLDraw;
    RLMemo1: TRLMemo;
    RLLabel40: TRLLabel;
    RLDBText16: TRLDBText;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel43: TRLLabel;
    RLLabel44: TRLLabel;
    RLRichText1: TRLRichText;
    RLBand8: TRLBand;
    RLLabel45: TRLLabel;
    RLExpressionParser1: TRLExpressionParser;
    mdManifesto: TJvMemoryData;
    mdManifestoManifesto: TIntegerField;
    mdManifestodatainc: TDateField;
    mdManifestoplaca: TStringField;
    mdManifestoveiculo: TStringField;
    mdManifestonm_fornecedor: TStringField;
    mdManifestonommot: TStringField;
    mdManifestoCNH: TStringField;
    mdManifestocte: TIntegerField;
    mdManifestoremetente: TStringField;
    mdManifestodestino: TStringField;
    mdManifestocidade: TStringField;
    mdManifestovolume: TIntegerField;
    mdManifestopeso: TFloatField;
    mdManifestovalor: TFloatField;
    mdManifestoestado: TStringField;
    mdManifestooperacao: TStringField;
    mdManifestoobs: TMemoField;
    mdManifestonota: TMemoField;
    dtsmdManifesto: TDataSource;
    procedure RlManifestoBeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand6BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand7BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManifesto_serieU: TfrmManifesto_serieU;

implementation

{$R *.dfm}

uses Dados, Menu, funcoes;

procedure TfrmManifesto_serieU.RLBand6BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if Length(mdManifestonota.value) > 20 then
    RLBand6.Height := 32;
end;

procedure TfrmManifesto_serieU.RLBand7BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel37.caption := 'Quant. Documentos : ' + IntToStr(self.tag);
  RLRichText1.Lines.clear;
  RLRichText1.Lines.add(mdManifestoobs.AsString);
end;

procedure TfrmManifesto_serieU.RlManifestoBeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.clear;
  dtmDados.IQuery1.sql.add
    ('select razsoc from cyber.rodfil where codfil = :0 ');
  dtmDados.IQuery1.Parameters[0].value := GlbFilial;
  dtmDados.IQuery1.Open;
  RLLabel10.caption := 'ROMANEIO DE CARGA - N� ' + IntToStr(GlbFilial) + '-' +
    RetBran(mdManifestoManifesto.asstring, 6);
  RLLabel21.caption := dtmDados.IQuery1.FieldByName('razsoc').value;

  RLLabel27.caption := 'Origem da Viagem : ' + mdManifestoestado.AsString;
  RLLabel46.caption := 'Tipo - ' + mdManifestooperacao.asstring;
  if copy(dtmDados.IQuery1.FieldByName('razsoc').value, 1, 7) = 'INTECOM' then
  begin
    RLLogoInt.Visible := true;
    RLLogoInt.Enabled := true;
  end
  else
  begin
    RLLogoIw.Visible := true;
    RLLogoIw.Enabled := true;
  end;

  dtmDados.IQuery1.close;
end;

end.
