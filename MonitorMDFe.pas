unit MonitorMDFe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, ExtCtrls, StdCtrls, Mask, JvExMask,
  JvToolEdit, Buttons, DBCtrls, SHDocVw;

type
  TfrmMonitorMDFe = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    grMonitor: TDBGrid;
    Panel3: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qrMonitor: TADOQuery;
    dsMonitor: TDataSource;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    gbData: TGroupBox;
    Label4: TLabel;
    btnPesquisa: TBitBtn;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btnImprimir: TBitBtn;
    edMotivo: TDBMemo;
    Label37: TLabel;
    mCte: TMemo;
    btnGerarCtrc: TBitBtn;
    cbTipo: TComboBox;
    lbTotReg: TLabel;
    QSite: TADOQuery;
    dtsSite: TDataSource;
    btnEnviar: TBitBtn;
    cbFilial: TComboBox;
    btnStatus: TBitBtn;
    QSiteCODFIL: TBCDField;
    QSiteFILIAL: TStringField;
    qrMonitorMANIFESTO: TBCDField;
    qrMonitorDREGISTRO: TDateTimeField;
    qrMonitorCIA_NAME: TStringField;
    qrMonitorMDFE_MOTIVO: TStringField;
    qrMonitorCONFIGURACAO: TStringField;
    qrMonitorMDFE_STATUS: TBCDField;
    qrMonitorMDFE_PROTOCOLO: TStringField;
    qrMonitorFILIAL: TBCDField;
    qrMonitorMDFE_CHAVE: TStringField;
    qrMonitorSTATUS: TStringField;
    OpenDialog1: TOpenDialog;
    QSiteUF: TStringField;
    btnXML: TBitBtn;
    btnNEnc: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure grMonitorDblClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnGerarCtrcClick(Sender: TObject);
    procedure grMonitorDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure mCteClick(Sender: TObject);
    procedure mCteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEnviarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnStatusClick(Sender: TObject);
    procedure LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser);
    procedure btnXMLClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnNEncClick(Sender: TObject);
  private
  var
    ok, glbUF: string;
    { Private declarations }
    procedure Pesquisa();
  public
    { Public declarations }
    iEmpresa: Integer;
    bContingencia: Boolean;
  end;

var
  frmMonitorMDFe: TfrmMonitorMDFe;

implementation

uses Dados, menu, funcoes, pcnConversao;

{$R *.dfm}

procedure TfrmMonitorMDFe.btnPesquisaClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TfrmMonitorMDFe.btnStatusClick(Sender: TObject);
begin
  frmMenu.ACBrMDFe1.WebServices.StatusServico.Executar;
  mCte.Lines.Text :=
    UTF8Encode(frmMenu.ACBrMDFe1.WebServices.StatusServico.RetWS);
  mCte.Visible := true;
  mCte.Clear;
  mCte.Lines.Add('Status Servi�o');
  mCte.Lines.Add('tpAmb: ' +
    TpAmbToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.tpAmb));
  mCte.Lines.Add('verAplic: ' + frmMenu.ACBrMDFe1.WebServices.
    StatusServico.verAplic);
  mCte.Lines.Add('cStat: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.cStat));
  mCte.Lines.Add('xMotivo: ' + frmMenu.ACBrMDFe1.WebServices.
    StatusServico.xMotivo);
  mCte.Lines.Add('cUF: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.cUF));
  mCte.Lines.Add('dhRecbto: ' + DateTimeToStr
    (frmMenu.ACBrMDFe1.WebServices.StatusServico.dhRecbto));
  mCte.Lines.Add('tMed: ' +
    IntToStr(frmMenu.ACBrMDFe1.WebServices.StatusServico.TMed));
  mCte.Lines.Add('dhRetorno: ' + DateTimeToStr
    (frmMenu.ACBrMDFe1.WebServices.StatusServico.dhRetorno));
  mCte.Lines.Add('xObs: ' + frmMenu.ACBrMDFe1.WebServices.StatusServico.xObs);
end;

procedure TfrmMonitorMDFe.btnXMLClick(Sender: TObject);
begin
  if qrMonitorMDFE_CHAVE.AsString = '' then
  begin
    ShowMessage('Consulta Pela Chave, este est� sem !!');
    exit;
  end;

  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := QSiteUF.AsString;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.Add
    ('select ambiente, certificado, logo, nvl(custo,0) custo, ');
  dtmdados.IQuery1.sql.Add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.sql.Add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := QSiteCODFIL.asinteger;
  dtmdados.IQuery1.open;

  GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
  GlbSchema := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
  GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').value;
  GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;
  frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

  frmMenu.ACBrMDFe1.DAMDFe.Logo := ALLTRIM(dtmdados.IQuery1.FieldByName('logo')
    .AsString);
  {frmMenu.ACBrMDFe1.DAMDFe.PathPDF :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathMDFe :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString); }
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
  dtmdados.IQuery1.close;

  frmMenu.ACBrMDFe1.Configuracoes.WebServices.Visualizar := true;
  if OpenDialog1.Execute then
  begin
    frmMenu.ACBrMDFe1.Manifestos.Clear;
    frmMenu.ACBrMDFe1.Manifestos.LoadFromFile(OpenDialog1.FileName, False);
    frmMenu.ACBrMDFe1.Consultar;
  end;

  if (qrMonitorMDFE_CHAVE.AsString = frmMenu.ACBrMDFe1.WebServices.Consulta.
    MDFeChave) and (qrMonitorMDFE_PROTOCOLO.IsNull) then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.Add('update tb_manifesto set mdfe_protocolo = :0, ');
    dtmdados.IQuery1.sql.Add('mdfe_data = :1, mdfe_status = 100, ');
    dtmdados.IQuery1.sql.Add('mdfe_motivo = ''Autorizado o uso do MDF-e'', ');
    dtmdados.IQuery1.sql.Add('mdfe_mensagem = ''Autorizado o uso do MDF-e'', ');
    dtmdados.IQuery1.sql.Add('status = ''S'' where mdfe_chave = :2 ');
    dtmdados.IQuery1.Parameters[0].value :=
      frmMenu.ACBrMDFe1.WebServices.Consulta.Protocolo;
    dtmdados.IQuery1.Parameters[1].value :=
      frmMenu.ACBrMDFe1.WebServices.Consulta.dhRecbto;
    dtmdados.IQuery1.Parameters[2].value := qrMonitorMDFE_CHAVE.AsString;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
  end;

end;

procedure TfrmMonitorMDFe.BitBtn1Click(Sender: TObject);
begin
  // frmMenu.ACBrMDFe1.Configuracoes.WebServices.Visualizar := true;
  // frmMenu.ACBrMDFe1.WebServices.Retorno.MDFeChave := qrMonitorMDFE_CHAVE.AsString;
  // frmMenu.ACBrMDFe1.WebServices.Consulta.Executar;
end;

procedure TfrmMonitorMDFe.btnEnviarClick(Sender: TObject);
begin
  if qrMonitorMDFE_CHAVE.AsString = '' then
  begin
    ShowMessage('Consulta Pela Chave, este est� sem !!');
    exit;
  end;

  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := QSiteUF.AsString;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.Add
    ('select ambiente, certificado, logo, pdf, nvl(custo,0) custo, ');
  dtmdados.IQuery1.sql.Add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.sql.Add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := QSiteCODFIL.asinteger;
  dtmdados.IQuery1.open;

  GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
  GlbSchema := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
  GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').value;
  GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;
  frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

  frmMenu.ACBrMDFe1.DAMDFe.Logo := ALLTRIM(dtmdados.IQuery1.FieldByName('logo')
    .AsString);
  frmMenu.ACBrMDFe1.DAMDFe.PathPDF :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
  dtmdados.IQuery1.close;

  frmMenu.ACBrMDFe1.Configuracoes.WebServices.Visualizar := true;
  frmMenu.ACBrMDFe1.WebServices.Consulta.MDFeChave :=
    qrMonitorMDFE_CHAVE.AsString;
  frmMenu.ACBrMDFe1.WebServices.Consulta.Executar;
  if (qrMonitorMDFE_STATUS.value = 204) or (qrMonitorMDFE_STATUS.value = 609) or
    (qrMonitorMDFE_STATUS.value = 100) then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.sql.Clear;
    dtmdados.IQuery1.sql.Add('update tb_manifesto set mdfe_protocolo = :0, ');
    dtmdados.IQuery1.sql.Add
      ('mdfe_data = :1, mdfe_status = 100, mdfe_motivo = ''Autorizado o uso do MDF-e'',  ');
    dtmdados.IQuery1.sql.Add('mdfe_mensagem = ''Autorizado o uso do MDF-e'', ');
    dtmdados.IQuery1.sql.Add('status = ''S'' where mdfe_chave = :2 ');
    dtmdados.IQuery1.Parameters[0].value :=
      frmMenu.ACBrMDFe1.WebServices.Consulta.Protocolo;
    dtmdados.IQuery1.Parameters[1].value :=
      frmMenu.ACBrMDFe1.WebServices.Consulta.dhRecbto;
    dtmdados.IQuery1.Parameters[2].value := qrMonitorMDFE_CHAVE.AsString;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
  end;

  // recibo    := frmMenu.ACBrMDFe1.WebServices.Consulta.RetWS.Recibo;
  { dtmdados.IQuery1.Parameters[0].value := ACBrMDFe1.WebServices.Retorno.cStat;
    dtmdados.IQuery1.Parameters[1].value := ACBrMDFe1.WebServices.Retorno.xMotivo;
    dtmdados.IQuery1.Parameters[2].value := ACBrMDFe1.WebServices.Retorno.Msg;
    dtmdados.IQuery1.Parameters[3].value := ACBrMDFe1.WebServices.Retorno.Recibo;
    dtmdados.IQuery1.Parameters[4].value :=

    if OpenDialog1.Execute then
    begin
    frmMenu.ACBrMDFe1.Manifestos.Clear;
    frmMenu.ACBrMDFe1.Manifestos.LoadFromFile(OpenDialog1.FileName);
    frmMenu.ACBrMDFe1.Consultar;

    ShowMessage(frmMenu.ACBrMDFe1.WebServices.Consulta.Protocolo);
    end; }
end;

procedure TfrmMonitorMDFe.LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser);
begin
  MyMemo.Lines.SaveToFile(ExtractFileDir(application.ExeName) + 'temp.xml');
  MyWebBrowser.Navigate(ExtractFileDir(application.ExeName) + 'temp.xml');
end;

procedure TfrmMonitorMDFe.btnGerarCtrcClick(Sender: TObject);
begin
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := QSiteUF.AsString;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.Add
    ('select ambiente, certificado, logo, pdf, nvl(custo,0) custo, ');
  dtmdados.IQuery1.sql.Add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.sql.Add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := QSiteCODFIL.asinteger;
  dtmdados.IQuery1.open;

  GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
  GlbSchema := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
  GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').value;
  GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;
  frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

  frmMenu.ACBrMDFe1.DAMDFe.Logo := ALLTRIM(dtmdados.IQuery1.FieldByName('logo')
    .AsString);
  frmMenu.ACBrMDFe1.DAMDFe.PathPDF :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
  dtmdados.IQuery1.close;

  // j� enviou e autorizou, n�o envia de novo
  if (qrMonitorMDFE_STATUS.value = 100) then
  begin
    ShowMessage('MDF-e j� enviado e autorizado pelo SEFAZ !');
    exit;
  end;
  iEmpresa := qrMonitorFILIAL.asinteger;
  frmMenu.GeraMDFe(qrMonitorMANIFESTO.asinteger);
  Pesquisa;
end;

procedure TfrmMonitorMDFe.btnImprimirClick(Sender: TObject);
var
  sData, sCaminho, sMDFe: string;
begin
  sData := '';
  sCaminho := '';
  sMDFe := '';
  if (qrMonitorMDFE_PROTOCOLO.IsNull) then
  begin
    ShowMessage('Erro na Transmiss�o � necess�rio o Envio Novamente !');
    exit;
  end;

  sData := FormatDateTime('YYYY', qrMonitorDREGISTRO.value) +
    FormatDateTime('MM', qrMonitorDREGISTRO.value);
  sCaminho := glbPDF + '\' + sData + '\Mdfe\';
  sMDFe := qrMonitorMDFE_CHAVE.AsString + '-mdfe.xml';

  // arquivo := frmMenu.ACBrMDFe1.DAMDFe.PathPDF + QManifestoMDFE_CHAVE.AsString + '-mdfe.xml';
  frmMenu.ACBrMDFe1.Manifestos.Clear;
  frmMenu.ACBrMDFe1.Manifestos.LoadFromFile(sCaminho + sMDFe, False);;
  frmMenu.ACBrMDFe1.Manifestos.Imprimir;
end;

procedure TfrmMonitorMDFe.btnNEncClick(Sender: TObject);
var
  sCNPJ, chave, proto: String;
  j: Integer;
begin
  if cbFilial.ItemIndex > -1 then
  begin
    sCNPJ := apcarac(copy(cbFilial.Text, length(cbFilial.Text) - 18, 19));
  end;

  //frmMenu.ACBrMDFe1.Configuracoes.Geral.RetirarAcentos := False;
  frmMenu.ACBrMDFe1.WebServices.ConsultaMDFeNaoEnc(sCNPJ);

  mCte.Lines.Text :=
    UTF8Encode(frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.RetWS);
  mCte.Visible := true;
  mCte.Clear;
  mCte.Lines.Add('Manifestos N�o Encerrados');
  for j := 0 to frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.
    Count - 1 do
  begin
    chave := frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.Items
      [j].chMDFe;
    proto := frmMenu.ACBrMDFe1.WebServices.ConsMDFeNaoEnc.InfMDFe.Items
      [j].nProt;
    mCte.Lines.Add('Chave : ' + chave + ' Protocolo : ' + proto);
  end;
 {
  try
    ACBrMDFe1.WebServices.ConsultaMDFeNaoEnc( vCNPJ );
  finally
    MemoResp.Lines.Text := ACBrUTF8ToAnsi(ACBrMDFe1.WebServices.ConsMDFeNaoEnc.RetWS);
    memoRespWS.Lines.Text := ACBrUTF8ToAnsi(ACBrMDFe1.WebServices.ConsMDFeNaoEnc.RetornoWS);
  end;

  LoadXML(MemoResp, WBResposta);           }

end;

procedure TfrmMonitorMDFe.grMonitorDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + grMonitor.Columns.Items
    [grMonitor.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not qrMonitor.Locate(grMonitor.Columns.Items[grMonitor.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(grMonitor.Columns.Items[grMonitor.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmMonitorMDFe.grMonitorDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (DataCol <> 3) then
    exit;
  if (qrMonitorMDFE_STATUS.value = 100) then
  begin
    grMonitor.Canvas.Font.Color := clWhite;
    grMonitor.Canvas.Brush.Color := clGreen;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := '';
  end
  else if (qrMonitorMDFE_STATUS.asinteger = 101) and
    (qrMonitorMDFE_STATUS.value = 104) and (qrMonitorMDFE_STATUS.value <> 100)
  then
  begin
    grMonitor.Canvas.Font.Color := clWhite;
    grMonitor.Canvas.Brush.Color := clRed;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := 'vermelho';
  end
  else if (qrMonitorMDFE_STATUS.asinteger = 94) then
  begin
    grMonitor.Canvas.Font.Color := clWhite;
    grMonitor.Canvas.Brush.Color := clRed;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := 'vermelho';
  end
  else if (qrMonitorMDFE_STATUS.asinteger = 94) and (qrMonitorMDFE_STATUS.IsNull)
  then
  begin
    grMonitor.Canvas.Font.Color := clWhite;
    grMonitor.Canvas.Brush.Color := clYellow;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := '';
  end
  else if ((qrMonitorMDFE_PROTOCOLO.IsNull) or
    (qrMonitorMDFE_STATUS.value = 103) or (qrMonitorMDFE_STATUS.value = 105) and
    (qrMonitorMDFE_STATUS.asinteger = 94)) then
  begin
    grMonitor.Canvas.Font.Color := clBlack;
    grMonitor.Canvas.Brush.Color := clYellow;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := '';
  end
  else if (qrMonitorMDFE_STATUS.asinteger = 6) and
    (qrMonitorMDFE_PROTOCOLO.IsNull) then
  begin
    grMonitor.Canvas.Font.Color := clBlack;
    grMonitor.Canvas.Brush.Color := clYellow;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := '';
  end
  else if (qrMonitorMDFE_STATUS.AsString = '101') then
  begin
    grMonitor.Canvas.Font.Color := clWhite;
    grMonitor.Canvas.Brush.Color := clRed;
    grMonitor.Canvas.FillRect(Rect);
    grMonitor.DefaultDrawDataCell(Rect, Column.Field, State);
    ok := 'vermelho';
  end;
end;

procedure TfrmMonitorMDFe.mCteClick(Sender: TObject);
begin
  mCte.Color := clWhite;
  mCte.Font.Style := [fsBold];
end;

procedure TfrmMonitorMDFe.mCteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    mCte.Clear;
    mCte.Visible := False;
  end;
end;

procedure TfrmMonitorMDFe.Pesquisa();
var
  a: String;
  filial: Integer;
begin
  // retornando o cStat com um dos valores, 100 (�Autorizado o Uso do MDF-e�), 101
  // (�Cancelamento de MDF-e homologado�), 132 (�Encerramento de MDF-e homologado�) e tamb�m o
  // respectivo protocolo de autoriza��o de uso de uso e registro de eventos.
  qrMonitor.close;
  qrMonitor.Parameters[0].value :=
    StrToDateTime(DateToStr(dtInicial.Date) + ' 00:00:00');
  qrMonitor.Parameters[1].value :=
    StrToDateTime(DateToStr(dtFinal.Date) + ' 23:59:00');
  filial := 0;
  if cbFilial.ItemIndex > -1 then
  begin
    a := copy(cbFilial.Text, 1, Pos('-', cbFilial.Text) - 1);
    if QSite.Locate('codfil', a, []) then
      filial := QSiteCODFIL.asinteger;
  end;

  if (cbFilial.ItemIndex > -1) then
    qrMonitor.sql[13] := ' and f.codfil = ' + QuotedStr(IntToStr(filial))
  else
    qrMonitor.sql[13] := ' ';

  // limpa consulta por Status
  qrMonitor.sql[14] := '';
  // monitorando
  if (cbTipo.ItemIndex = 1) then
    qrMonitor.sql[14] := 'and (((mdfe_protocolo is null) or (mdfe_status = ' +
      QuotedStr('103') + ') ' + ' or (mdfe_status = ' + QuotedStr('105') +
      ')) and (mdfe_status = 6)) '
    // Definitivo com Sucesso
  else if (cbTipo.ItemIndex = 2) then
    qrMonitor.sql[14] := 'and ((mdfe_status = 6) or (mdfe_status = ' +
      QuotedStr('104') + ') or ' + '(mdfe_status = ' + QuotedStr('100') + '))'
    //
    // Definitivo com Erro
  else if (cbTipo.ItemIndex = 3) then
    qrMonitor.sql[14] := ' and ((mdfe_status = 6) or (mdfe_status = ' +
      QuotedStr('104') + ') ' + ' or (mdfe_status <> ' + QuotedStr('100') + '))'
    // Erro Schema
  else if (cbTipo.ItemIndex = 4) then
    qrMonitor.sql[14] := ' and (mdfe_status = 94)';
  // QrMonitor.SQL.SaveToFile('c:\sql.txt');
  qrMonitor.open;

  qrMonitor.First;
  lbTotReg.Caption := 'Total de Registros: ' + IntToStr(qrMonitor.RecordCount);
end;

procedure TfrmMonitorMDFe.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QSite.close;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := glbUF;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.Clear;
  dtmdados.IQuery1.sql.Add
    ('select ambiente, certificado, logo, pdf, nvl(custo,0) custo, ');
  dtmdados.IQuery1.sql.Add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.sql.Add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].value := glbFilial;
  dtmdados.IQuery1.open;

  GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
  GlbSchema := ALLTRIM(dtmdados.IQuery1.FieldByName('schemma').AsString);
  GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').value;
  GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
  if GlbMdfeamb = 'H' then
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taHomologacao
  else
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.Ambiente := taProducao;
  frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('certificado').AsString);

  frmMenu.ACBrMDFe1.DAMDFe.Logo := ALLTRIM(dtmdados.IQuery1.FieldByName('logo')
    .AsString);
  frmMenu.ACBrMDFe1.DAMDFe.PathPDF :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pdf').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('porta').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('user_proxy').AsString);
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
    ALLTRIM(dtmdados.IQuery1.FieldByName('pass_proxy').AsString);
  dtmdados.IQuery1.close;
end;

procedure TfrmMonitorMDFe.FormCreate(Sender: TObject);
begin
  QSite.open;
  cbFilial.Clear;
  cbFilial.Items.Add('');
  while not QSite.eof do
  begin
    cbFilial.Items.Add(QSiteFILIAL.value);
    QSite.Next;
  end;
  if QSite.RecordCount = 1 then
  begin
    cbFilial.ItemIndex := 1;
  end;
  glbUF := frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF;
end;

procedure TfrmMonitorMDFe.FormShow(Sender: TObject);
begin
  dtInicial.Date := Date - 1;
  dtFinal.Date := Date;
  bContingencia := False;
  grMonitor.SetFocus;
  cbTipo.ItemIndex := 0;
end;

end.
