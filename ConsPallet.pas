unit ConsPallet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB,
  DateUtils,
  DBCtrls, JvDBControls, ADODB, JvExStdCtrls, JvCombobox, JvDriveCtrls,
  JvListBox, JvMaskEdit, Gauges, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  ComCtrls,
  JvDBLookup, JvBaseEdits, JvExExtCtrls, JvRadioGroup;

type
  TfrmConsPallet = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    Qmovimento: TADOQuery;
    Label3: TLabel;
    dtMovimI: TJvDateEdit;
    Label4: TLabel;
    dtMovimF: TJvDateEdit;
    BitBtn2: TBitBtn;
    JvDBGrid4: TJvDBGrid;
    dtsMovim: TDataSource;
    Label8: TLabel;
    JvDirectoryListBox1: TJvDirectoryListBox;
    JvDriveCombo1: TJvDriveCombo;
    Gauge1: TGauge;
    JvDBNavigator4: TJvDBNavigator;
    QmovimentoMANIFESTO: TBCDField;
    QmovimentoPALLET: TStringField;
    QmovimentoDATINC: TDateTimeField;
    QmovimentoTIPO: TStringField;
    QmovimentoUSUARIO: TStringField;
    QmovimentoQUANT: TBCDField;
    QmovimentoRAZSOC: TStringField;
    QmovimentoCLIENTE: TStringField;
    Label14: TLabel;
    ledIdCliente: TJvDBLookupEdit;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    QClienteCLIENTE: TStringField;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    edPBR: TJvCalcEdit;
    Label5: TLabel;
    edChep: TJvCalcEdit;
    btnConfirma: TBitBtn;
    edOrigem: TJvMaskEdit;
    lblNome: TLabel;
    Label6: TLabel;
    edDcto: TJvCalcEdit;
    Label7: TLabel;
    dbcliente: TJvDBLookupEdit;
    QmovimentoID: TBCDField;
    QQuant: TADOQuery;
    QQuantPALLET: TStringField;
    QQuantQUANT: TBCDField;
    QmovimentoTRANSP: TStringField;
    TabSheet2: TTabSheet;
    Label9: TLabel;
    edDestino: TJvMaskEdit;
    Label10: TLabel;
    JvDBLookupEdit1: TJvDBLookupEdit;
    Label12: TLabel;
    JvCalcEdit2: TJvCalcEdit;
    Label13: TLabel;
    JvCalcEdit3: TJvCalcEdit;
    btnConfirmaS: TBitBtn;
    Label15: TLabel;
    JvCalcEdit1: TJvCalcEdit;
    RGDcto: TJvRadioGroup;
    lblData: TLabel;
    Label11: TLabel;
    dbTRansp: TJvDBLookupEdit;
    QTRansp: TADOQuery;
    dtsTransp: TDataSource;
    QTRanspNM_FORNECEDOR: TStringField;
    btnEntrada: TBitBtn;
    QmovimentoDESTINO: TStringField;
    btnExcluir: TBitBtn;
    edSaldo: TJvCalcEdit;
    Label16: TLabel;
    RGEntrada: TRadioGroup;
    Label17: TLabel;
    edDesc: TJvCalcEdit;
    edOutro: TJvCalcEdit;
    Label18: TLabel;
    Label19: TLabel;
    JvCalcEdit4: TJvCalcEdit;
    JvCalcEdit5: TJvCalcEdit;
    Label20: TLabel;
    btnImprimir: TBitBtn;
    QmovimentoMOTORISTA: TStringField;
    QmovimentoPLACA: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure dtMovimFExit(Sender: TObject);
    procedure JvDBGrid4TitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure btnConfirmaClick(Sender: TObject);
    procedure edOrigemExit(Sender: TObject);
    procedure JvDBGrid4DblClick(Sender: TObject);
    procedure QmovimentoBeforeOpen(DataSet: TDataSet);
    procedure btnConfirmaSClick(Sender: TObject);
    procedure edDestinoExit(Sender: TObject);
    procedure JvCalcEdit1Exit(Sender: TObject);
    procedure RGDctoClick(Sender: TObject);
    procedure btnEntradaClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    serie: String;
  public
    { Public declarations }
  end;

var
  frmConsPallet: TfrmConsPallet;

implementation

uses Dados, Menu, funcoes, VALE_PALETE_ENTRADA;

{$R *.dfm}

procedure TfrmConsPallet.dtMovimFExit(Sender: TObject);
begin
  if dtMovimF.date < dtMovimI.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    exit;
  end;
end;

procedure TfrmConsPallet.BitBtn2Click(Sender: TObject);
begin
  if ledIdCliente.LookupValue = '  ' then
  begin
    ShowMessage('� preciso escolher o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;

  if RGEntrada.ItemIndex = -1 then
  begin
    ShowMessage('� preciso escolher o Tipo de Entrada !!');
    RGEntrada.setfocus;
    exit;
  end;

  Qmovimento.close;
  if RGEntrada.ItemIndex = 0 then
    Qmovimento.SQL[0] :=
      'select distinct p.id, p.manifesto, p.pallet, p.datinc, p.tipo, p.usuario, p.quant, d.nomeab razsoc, c.razsoc cliente, t.razsoc transp, p.destino, mo.nommot motorista, ma.placa '
  else
    Qmovimento.SQL[0] :=
      'select distinct p.id, p.manifesto, p.pallet, p.datinc, p.tipo, p.usuario, (p.quant * -1) quant, d.nomeab razsoc, c.razsoc cliente, t.razsoc transp, p.destino, mo.nommot motorista, ma.placa ';

  if copy(dtMovimI.Text, 1, 2) <> '  ' then
  begin
    Qmovimento.SQL[7] := 'Where p.datinc between to_date(''' + dtMovimI.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtMovimF.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'') and d.codcgc = p.destino  and p.vale is null and p.filial = :0 ';
    Qmovimento.SQL[14] := 'Where p.datinc between to_date(''' + dtMovimI.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtMovimF.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'') and p.filial = :1  and p.vale = ''S'' ';
    Qmovimento.SQL[26] := 'Where p.datinc between to_date(''' + dtMovimI.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtMovimF.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'') and p.filial = :2 and p.vale is null and r.razsoc is not null ';
  end
  else
  begin
    Qmovimento.SQL[7] := 'Where d.codcgc = p.destino  and p.vale is null and p.filial = :0 ';
    Qmovimento.SQL[14] := 'Where p.filial = :1  and p.vale = ''S''  ';
    Qmovimento.SQL[26] := 'Where  p.filial = :2 and p.vale is null and r.razsoc is not null ';
  end;

  if ledIdCliente.LookupValue <> '' then
  begin
    Qmovimento.SQL[8] := 'And c.razsoc = ' +
      QuotedStr(ledIdCliente.LookupValue);
    Qmovimento.SQL[15] := 'And p.cliente = ' +
      QuotedStr(ledIdCliente.LookupValue);
    Qmovimento.SQL[27] := 'And p.cliente = ' +
      QuotedStr(ledIdCliente.LookupValue);
  end
  else
  begin
    Qmovimento.SQL[8] := ' ';
    Qmovimento.SQL[15] := '';
    Qmovimento.SQL[27] := '';
  end;
  //howmessage(qmovimento.sql.text);
  Qmovimento.open;
  edSaldo.value := 0;
  QMovimento.DisableControls;
  while not QMovimento.eof do
  begin
    if QmovimentoTIPO.value = 'E' then
      edSaldo.value := edSaldo.value + QmovimentoQUANT.AsInteger
    else
      edSaldo.value := edSaldo.value - QmovimentoQUANT.AsInteger;
    QMovimento.Next;
  end;
  Qmovimento.first;
  QMovimento.EnableControls;
end;

procedure TfrmConsPallet.btnConfirmaClick(Sender: TObject);
begin
  if lblNome.caption = '' then
  begin
    ShowMessage('Qual a Origem dos Paletes ?');
    edOrigem.setfocus;
    exit;
  end;

  if edDcto.value = 0 then
  begin
    ShowMessage('Qual o Documento dos Paletes ?');
    edDcto.setfocus;
    exit;
  end;

  if dbcliente.LookupValue = '  ' then
  begin
    ShowMessage('Qual o Cliente dos Paletes ?');
    dbcliente.setfocus;
    exit;
  end;

  if (edPBR.value = 0) and (edChep.value = 0) and (edDesc.value = 0) and (edOutro.value = 0) then
  begin
    ShowMessage('Qual Quantidade de Palete ?');
    edPBR.setfocus;
    exit;
  end;

  if Application.Messagebox('Confirma o Entrada de Pallet ?',
    'Entrada de Vale Palete', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if (edPBR.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale, transp) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add
        (':0, :1, sysdate, :2, :3, :4, :5, :6, :7, ''S'', :8) ');
      dtmdados.IQuery1.Parameters[0].value := apcarac(edDcto.Text);
      dtmdados.IQuery1.Parameters[1].value := 'PBR';
      dtmdados.IQuery1.Parameters[2].value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].value := edPBR.value;
      dtmdados.IQuery1.Parameters[4].value := 'E';
      dtmdados.IQuery1.Parameters[5].value := edOrigem.Text;
      dtmdados.IQuery1.Parameters[6].value := dbcliente.LookupValue;
      dtmdados.IQuery1.Parameters[7].value := GLBFilial;
      dtmdados.IQuery1.Parameters[8].value := dbTRansp.LookupValue;;
      dtmdados.IQuery1.ExecSQL;
    end;
    if (edChep.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale, transp) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add
        (':0, :1, sysdate, :2, :3, :4, :5, :6, :7, ''S'', :8) ');
      dtmdados.IQuery1.Parameters[0].value := apcarac(edDcto.Text);
      dtmdados.IQuery1.Parameters[1].value := 'CHEP';
      dtmdados.IQuery1.Parameters[2].value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].value := edChep.value;
      dtmdados.IQuery1.Parameters[4].value := 'E';
      dtmdados.IQuery1.Parameters[5].value := edOrigem.Text;
      dtmdados.IQuery1.Parameters[6].value := dbcliente.LookupValue;
      dtmdados.IQuery1.Parameters[7].value := GLBFilial;
      dtmdados.IQuery1.Parameters[8].value := dbTRansp.LookupValue;;
      dtmdados.IQuery1.ExecSQL;
    end;
    if (edDesc.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale, transp) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add
        (':0, :1, sysdate, :2, :3, :4, :5, :6, :7, ''S'', :8) ');
      dtmdados.IQuery1.Parameters[0].value := apcarac(edDcto.Text);
      dtmdados.IQuery1.Parameters[1].value := 'DESCART�VEL';
      dtmdados.IQuery1.Parameters[2].value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].value := edDesc.value;
      dtmdados.IQuery1.Parameters[4].value := 'E';
      dtmdados.IQuery1.Parameters[5].value := edOrigem.Text;
      dtmdados.IQuery1.Parameters[6].value := dbcliente.LookupValue;
      dtmdados.IQuery1.Parameters[7].value := GLBFilial;
      dtmdados.IQuery1.Parameters[8].value := dbTRansp.LookupValue;;
      dtmdados.IQuery1.ExecSQL;
    end;
    if (edOutro.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale, transp) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add
        (':0, :1, sysdate, :2, :3, :4, :5, :6, :7, ''S'', :8) ');
      dtmdados.IQuery1.Parameters[0].value := apcarac(edDcto.Text);
      dtmdados.IQuery1.Parameters[1].value := 'OUTROS';
      dtmdados.IQuery1.Parameters[2].value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].value := edOutro.value;
      dtmdados.IQuery1.Parameters[4].value := 'E';
      dtmdados.IQuery1.Parameters[5].value := edOrigem.Text;
      dtmdados.IQuery1.Parameters[6].value := dbcliente.LookupValue;
      dtmdados.IQuery1.Parameters[7].value := GLBFilial;
      dtmdados.IQuery1.Parameters[8].value := dbTRansp.LookupValue;
      dtmdados.IQuery1.ExecSQL;
    end;

    Application.CreateForm(TfrmVale_palete_ENTRADA, frmVale_palete_ENTRADA);
    frmVale_palete_ENTRADA.NR_ROMANEIO.caption := edDcto.Text;
    frmVale_palete_ENTRADA.RlLabel3.caption := DateToStr(now());
    frmVale_palete_ENTRADA.DTIC.caption := DateToStr(now());
    frmVale_palete_ENTRADA.RlLabel8.caption := edDcto.Text;;
    frmVale_palete_ENTRADA.RlLabel51.caption := QClienteCLIENTE.AsString;
    frmVale_palete_ENTRADA.RlLabel52.caption := QClienteCLIENTE.AsString;
    frmVale_palete_ENTRADA.RlLabel54.caption := lblNome.caption;
    frmVale_palete_ENTRADA.RlLabel56.caption := lblNome.caption;
    frmVale_palete_ENTRADA.RlLabel28.caption := dbTRansp.LookupValue;
    frmVale_palete_ENTRADA.RlLabel31.caption := dbTRansp.LookupValue;

    QQuant.close;
    QQuant.Parameters[0].value := apcarac(edDcto.Text);
    QQuant.Parameters[1].value := GLBFilial;
    QQuant.Parameters[2].value := edOrigem.Text;
    QQuant.Parameters[3].value := dbcliente.LookupValue;
    QQuant.open;

    if QQuant.locate('PALLET', 'PBR', []) then
    begin
      frmVale_palete_ENTRADA.RlLabel57.caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel59.caption := QQuantQUANT.AsString;
    end;
    if QQuant.locate('PALLET', 'CHEP', []) then
    begin
      frmVale_palete_ENTRADA.RlLabel58.caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel60.caption := QQuantQUANT.AsString;
    end;
    if QQuant.locate('Pallet', 'DESCART�VEL', []) then
    begin
      frmVale_palete_ENTRADA.RlLabel15.caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel19.caption := QQuantQUANT.AsString;
    end;
    if QQuant.locate('Pallet', 'OUTROS', []) then
    begin
      frmVale_palete_ENTRADA.RlLabel17.caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel22.caption := QQuantQUANT.AsString;
    end;
    QQuant.close;

    frmVale_palete_ENTRADA.RLReport1.Preview;
    frmVale_palete_ENTRADA.Free;

    ShowMessage('Entrada de Palete Realizada !!');
    lblNome.caption := '';
    edDcto.Enabled := false;
    edChep.Enabled := false;
    edPBR.Enabled := false;
    edDesc.Enabled := false;
    edOutro.Enabled := false;
    edOrigem.clear;
    edDcto.clear;
    edChep.clear;
    edPBR.clear;
    edOutro.clear;
    edDesc.clear;
    edDestino.Clear;
    edOutro.Clear;
  end;
end;

procedure TfrmConsPallet.btnConfirmaSClick(Sender: TObject);
begin
  if Label15.caption = '' then
  begin
    ShowMessage('Qual o Destino dos Paletes ?');
    edDestino.setfocus;
    exit;
  end;

  if RGDcto.ItemIndex = -1 then
  begin
    ShowMessage('Qual o Tipo do Documento dos Paletes ?');
    RGDcto.setfocus;
    exit;
  end;

  if JvCalcEdit1.value = 0 then
  begin
    ShowMessage('Qual o Documento dos Paletes ?');
    edDcto.setfocus;
    exit;
  end;

  if dbcliente.LookupValue = '  ' then
  begin
    ShowMessage('Qual o Cliente dos Paletes ?');
    dbcliente.setfocus;
    exit;
  end;

  if (JvCalcEdit2.value = 0) and (JvCalcEdit3.value = 0) and (JvCalcEdit4.value = 0) and (JvCalcEdit5.value = 0)then
  begin
    ShowMessage('Qual Quantidade de Palete ?');
    JvCalcEdit2.setfocus;
    exit;
  end;

  if Application.Messagebox('Confirma a Sa�da Manual de Palete ?',
    'Sa�da Manual de Palete', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    if (JvCalcEdit2.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add(':0, :1, :2, :3, :4, :5, :6, :7, :8, ''S'') ');
      dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
      dtmdados.IQuery1.Parameters[1].value := 'PBR';
      dtmdados.IQuery1.Parameters[2].value := StrToDateTime(lblData.caption);
      dtmdados.IQuery1.Parameters[3].value := GLBUSER;
      dtmdados.IQuery1.Parameters[4].value := JvCalcEdit2.value;
      dtmdados.IQuery1.Parameters[5].value := 'S';
      dtmdados.IQuery1.Parameters[6].value := edDestino.Text;
      dtmdados.IQuery1.Parameters[7].value := JvDBLookupEdit1.LookupValue;
      dtmdados.IQuery1.Parameters[8].value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;
    if (JvCalcEdit3.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add(':0, :1, :2, :3, :4, :5, :6, :7, :8, ''S'') ');
      dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
      dtmdados.IQuery1.Parameters[1].value := 'CHEP';
      dtmdados.IQuery1.Parameters[2].value := StrToDateTime(lblData.caption);
      dtmdados.IQuery1.Parameters[3].value := GLBUSER;
      dtmdados.IQuery1.Parameters[4].value := JvCalcEdit3.value;
      dtmdados.IQuery1.Parameters[5].value := 'S';
      dtmdados.IQuery1.Parameters[6].value := edDestino.Text;
      dtmdados.IQuery1.Parameters[7].value := JvDBLookupEdit1.LookupValue;
      dtmdados.IQuery1.Parameters[8].value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;

    if (JvCalcEdit4.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add(':0, :1, :2, :3, :4, :5, :6, :7, :8, ''S'') ');
      dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
      dtmdados.IQuery1.Parameters[1].value := 'DESCART�VEL';
      dtmdados.IQuery1.Parameters[2].value := StrToDateTime(lblData.caption);
      dtmdados.IQuery1.Parameters[3].value := GLBUSER;
      dtmdados.IQuery1.Parameters[4].value := JvCalcEdit2.value;
      dtmdados.IQuery1.Parameters[5].value := 'S';
      dtmdados.IQuery1.Parameters[6].value := edDestino.Text;
      dtmdados.IQuery1.Parameters[7].value := JvDBLookupEdit1.LookupValue;
      dtmdados.IQuery1.Parameters[8].value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;
    if (JvCalcEdit5.value > 0) then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.clear;
      dtmdados.IQuery1.SQL.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.SQL.add
        ('datinc, usuario, quant, tipo, destino, cliente, filial, vale) ');
      dtmdados.IQuery1.SQL.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.SQL.add(':0, :1, :2, :3, :4, :5, :6, :7, :8, ''S'') ');
      dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
      dtmdados.IQuery1.Parameters[1].value := 'OUTROS';
      dtmdados.IQuery1.Parameters[2].value := StrToDateTime(lblData.caption);
      dtmdados.IQuery1.Parameters[3].value := GLBUSER;
      dtmdados.IQuery1.Parameters[4].value := JvCalcEdit3.value;
      dtmdados.IQuery1.Parameters[5].value := 'S';
      dtmdados.IQuery1.Parameters[6].value := edDestino.Text;
      dtmdados.IQuery1.Parameters[7].value := JvDBLookupEdit1.LookupValue;
      dtmdados.IQuery1.Parameters[8].value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;
    {
      Application.CreateForm(TfrmVale_palete_ENTRADA, frmVale_palete_ENTRADA);
      frmVale_palete_ENTRADA.NR_ROMANEIO.Caption := eddcto.text;
      frmVale_palete_ENTRADA.RlLabel3.Caption := DateToStr(now());
      frmVale_palete_ENTRADA.DTIC.Caption := DateToStr(now());
      frmVale_palete_ENTRADA.RlLabel8.Caption := eddcto.text;;
      frmVale_palete_ENTRADA.RlLabel51.Caption := QClienteCLIENTE.AsString;
      frmVale_palete_ENTRADA.RlLabel52.Caption := QClienteCLIENTE.AsString;
      frmVale_palete_ENTRADA.RlLabel54.Caption := lblNome.caption;
      frmVale_palete_ENTRADA.RlLabel56.Caption := lblNome.caption;

      QQuant.close;
      QQuant.Parameters[0].value := eddcto.text;
      QQuant.Parameters[1].value := glbfilial;
      QQuant.Parameters[2].value := edOrigem.text;
      QQuant.Parameters[3].value := dbCliente.LookupValue;
      QQuant.Open;

      if QQuant.locate('PALLET','PBR',[]) then
      begin
      frmVale_palete_ENTRADA.RlLabel57.Caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel59.Caption := QQuantQUANT.AsString;
      end;
      if QQuant.locate('PALLET','CHEP',[]) then
      begin
      frmVale_palete_ENTRADA.RlLabel58.Caption := QQuantQUANT.AsString;
      frmVale_palete_ENTRADA.RlLabel60.Caption := QQuantQUANT.AsString;
      end;
      QQuant.close;


      frmVale_palete_ENTRADA.RLReport1.Preview;
      frmVale_palete_ENTRADA.Free;

    }
    ShowMessage('Sa�da Manual de Palete Realizada !!');
    Label15.caption := '';
    JvCalcEdit1.Enabled := false;
    JvCalcEdit2.Enabled := false;
    JvCalcEdit3.Enabled := false;
    JvCalcEdit4.Enabled := false;
    JvCalcEdit5.Enabled := false;
    edDestino.clear;
    JvCalcEdit1.clear;
    JvCalcEdit2.clear;
    JvCalcEdit3.clear;
    JvCalcEdit4.clear;
    JvCalcEdit5.clear;
    lblData.caption := '';
  end;

end;

procedure TfrmConsPallet.btnEntradaClick(Sender: TObject);
begin
  Application.CreateForm(TfrmVale_palete_ENTRADA, frmVale_palete_ENTRADA);
  frmVale_palete_ENTRADA.NR_ROMANEIO.caption := QmovimentoMANIFESTO.AsString;
  frmVale_palete_ENTRADA.RlLabel3.caption := QmovimentoDATINC.AsString;
  frmVale_palete_ENTRADA.DTIC.caption := QmovimentoDATINC.AsString;
  frmVale_palete_ENTRADA.RlLabel8.caption := '';
  frmVale_palete_ENTRADA.RlLabel51.caption := QmovimentoCLIENTE.AsString;
  frmVale_palete_ENTRADA.RlLabel52.caption := QmovimentoCLIENTE.AsString;
  frmVale_palete_ENTRADA.RlLabel54.caption := QmovimentoRAZSOC.AsString;
  frmVale_palete_ENTRADA.RlLabel56.caption := QmovimentoRAZSOC.AsString;

  QQuant.close;
  QQuant.Parameters[0].value := QmovimentoMANIFESTO.value;
  QQuant.Parameters[1].value := GLBFilial;
  QQuant.Parameters[2].value := QmovimentoDESTINO.AsString;
  QQuant.Parameters[3].value := QmovimentoCLIENTE.AsString;
  QQuant.open;

  if QQuant.locate('PALLET', 'PBR', []) then
  begin
    frmVale_palete_ENTRADA.RlLabel57.caption := QQuantQUANT.AsString;
    frmVale_palete_ENTRADA.RlLabel59.caption := QQuantQUANT.AsString;
  end;
  if QQuant.locate('PALLET', 'CHEP', []) then
  begin
    frmVale_palete_ENTRADA.RlLabel58.caption := QQuantQUANT.AsString;
    frmVale_palete_ENTRADA.RlLabel60.caption := QQuantQUANT.AsString;
  end;
  if QQuant.locate('Pallet', 'DESCART�VEL', []) then
  begin
    frmVale_palete_ENTRADA.RlLabel15.caption := QQuantQUANT.AsString;
    frmVale_palete_ENTRADA.RlLabel19.caption := QQuantQUANT.AsString;
  end;
  if QQuant.locate('Pallet', 'OUTROS', []) then
  begin
    frmVale_palete_ENTRADA.RlLabel17.caption := QQuantQUANT.AsString;
    frmVale_palete_ENTRADA.RlLabel22.caption := QQuantQUANT.AsString;
  end;
  QQuant.close;

  frmVale_palete_ENTRADA.RLReport1.Preview;
  frmVale_palete_ENTRADA.Free;
end;

procedure TfrmConsPallet.btnExcluirClick(Sender: TObject);
begin
  if dtmdados.PodeApagar(name) then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add('delete from tb_controle_pallet where id = :0');
    dtmdados.IQuery1.Parameters[0].value := QmovimentoID.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    ShowMessage('Registro Exclu�do');
    dtmdados.IQuery1.close;
    Qmovimento.close;
    Qmovimento.open;
  end;
end;

procedure TfrmConsPallet.btnImprimirClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  sarquivo := JvDirectoryListBox1.Directory + '\MovimentoPortaria' +
    GLBUSER + '.xls';
  memo1 := TStringList.Create;
  memo1.add('  <HTML>');
  memo1.add('    <HEAD>');
  memo1.add('      <TITLE>IW - Intecom</TITLE>');
  memo1.add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add('         </STYLE>');
  memo1.add('    </HEAD>');
  memo1.add('    <BODY <Font Color="#004080">');
  memo1.add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('</B></font>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to JvDBGrid4.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + JvDBGrid4.Columns[i].Title.caption
      + '</th>');
  end;
  memo1.add('</tr>');
  Qmovimento.First;
  while not Qmovimento.Eof do
  begin
    for i := 0 to JvDBGrid4.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + Qmovimento.fieldbyname
        (JvDBGrid4.Columns[i].FieldName).AsString + '</th>');
    Qmovimento.Next;
    memo1.add('<tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile(sarquivo);
  ShowMessage('Planilha salva em ' + sarquivo);
end;

procedure TfrmConsPallet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Qmovimento.close;
  QCliente.close;
end;

procedure TfrmConsPallet.FormCreate(Sender: TObject);
begin
  QCliente.open;
  QTransp.open;
  PageControl1.TabIndex := 0;
end;

procedure TfrmConsPallet.JvCalcEdit1Exit(Sender: TObject);
begin
  if (RGDcto.ItemIndex = 0) and (serie = '') then
  begin
    ShowMessage('N�o escolhido a S�rie do Manifesto');
  end;

  if RGDcto.ItemIndex = 0 then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select * from tb_portaria where manifesto = :0 and motivo = ''S'' and serie = :1 and tipo = ''Manifesto'' ');
    dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
    dtmdados.IQuery1.Parameters[1].value := serie;
    dtmdados.IQuery1.open;
  end
  else
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select * from tb_portaria where manifesto = :0 and motivo = ''S'' and tipo = ''Romaneio'' ');
    dtmdados.IQuery1.Parameters[0].value := JvCalcEdit1.value;
    dtmdados.IQuery1.open;
  end;

  if dtmdados.IQuery1.Eof then
  begin
    ShowMessage('Este N�mero de Documento N�o Teve Sa�da !!');
    JvCalcEdit1.setfocus;
  end
  else
    lblData.caption := FormatDateTime('dd/mm/yyyy hh:mm:ss',
      dtmdados.IQuery1.fieldbyname('Data').value);
  dtmdados.IQuery1.close;
end;

procedure TfrmConsPallet.JvDBGrid4DblClick(Sender: TObject);
begin
  if not dtmdados.PodeApagar(name) then
    exit;

  if Application.Messagebox('Voc� Deseja Excluir Este Lan�amento ?',
    'Excluir Lan�amento Vale Palete', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('update tb_controle_pallet set filial = -9, usuario = :0 where id = :1 ');
    dtmdados.IQuery1.Parameters[0].value := GLBUSER;
    dtmdados.IQuery1.Parameters[1].value := QmovimentoID.AsInteger;
    dtmdados.IQuery1.ExecSQL;
    dtmdados.IQuery1.close;
    ShowMessage('Lan�amento Exclu�do ');
    Qmovimento.close;
    Qmovimento.open;
  end;
end;

procedure TfrmConsPallet.JvDBGrid4TitleClick(Column: TColumn);
var
  icount: integer;
begin
  if Pos('order by', Qmovimento.SQL.Text) > 0 then
    Qmovimento.SQL.Text := copy(Qmovimento.SQL.Text, 1,
      Pos('order by', Qmovimento.SQL.Text) - 1) + 'order by ' + Column.FieldName
  else
    Qmovimento.SQL.Text := Qmovimento.SQL.Text + ' order by ' +
      Column.FieldName;
  Qmovimento.open;
  // Muda a cor da coluna do grid
  for icount := 0 to JvDBGrid4.Columns.Count - 1 do
    JvDBGrid4.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmConsPallet.edDestinoExit(Sender: TObject);
begin
  if copy(edDestino.Text, 1, 1) <> ' ' then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select razsoc from cyber.rodcli where codcgc = :0 ');
    dtmdados.IQuery1.Parameters[0].value := edDestino.Text;
    dtmdados.IQuery1.open;
    if dtmdados.IQuery1.Eof then
    begin
      ShowMessage('CNPJ N�o Encontrado !!');
      edDestino.setfocus;
      Label15.caption := '';
      JvCalcEdit1.Enabled := false;
      JvCalcEdit2.Enabled := false;
      JvCalcEdit3.Enabled := false;
      JvCalcEdit4.Enabled := false;
      JvCalcEdit5.Enabled := false;
    end
    else
    begin
      Label15.caption := dtmdados.IQuery1.fieldbyname('razsoc').value;
      JvCalcEdit1.Enabled := true;
      JvCalcEdit2.Enabled := true;
      JvCalcEdit3.Enabled := true;
      JvCalcEdit4.Enabled := false;
      JvCalcEdit5.Enabled := false;
    end;
  end;
end;

procedure TfrmConsPallet.QmovimentoBeforeOpen(DataSet: TDataSet);
begin
  Qmovimento.Parameters[0].value := GLBFilial;
  Qmovimento.Parameters[1].value := GLBFilial;
  Qmovimento.Parameters[2].value := GLBFilial;
end;

procedure TfrmConsPallet.RGDctoClick(Sender: TObject);
begin
  if RGDcto.ItemIndex = 0 then
    serie := InputBox('Qual � a S�rie ? ', 'Digite a s�rie do Manifesto', '')
  else
    serie := '';
end;

procedure TfrmConsPallet.edOrigemExit(Sender: TObject);
begin
  if copy(edOrigem.Text, 1, 1) <> ' ' then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.clear;
    dtmdados.IQuery1.SQL.add
      ('select razsoc from cyber.rodcli where codcgc = :0 ');
    dtmdados.IQuery1.Parameters[0].value := edOrigem.Text;
    dtmdados.IQuery1.open;
    if dtmdados.IQuery1.Eof then
    begin
      ShowMessage('CNPJ N�o Encontrado !!');
      edOrigem.setfocus;
      lblNome.caption := '';
      edDcto.Enabled := false;
      edChep.Enabled := false;
      edPBR.Enabled := false;
      edDesc.Enabled := false;
      edOutro.Enabled := false;
    end
    else
    begin
      lblNome.caption := dtmdados.IQuery1.fieldbyname('razsoc').value;
      edDcto.Enabled := true;
      edChep.Enabled := true;
      edPBR.Enabled := true;
      edDesc.Enabled := true;
      edOutro.Enabled := true;
    end;
  end;
end;

end.
