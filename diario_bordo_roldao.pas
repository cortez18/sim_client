unit diario_bordo_roldao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RLReport, Vcl.Imaging.jpeg, Data.DB,
  Data.Win.ADODB;

type
  TfrmDiario_bordo_roldao = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLImage1: TRLImage;
    RLImage2: TRLImage;
    RLBand2: TRLBand;
    RLLabel39: TRLLabel;
    RLDraw40: TRLDraw;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLDraw3: TRLDraw;
    RLDraw4: TRLDraw;
    RLDraw5: TRLDraw;
    RLDraw6: TRLDraw;
    RLDraw7: TRLDraw;
    RLLabel2: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel3: TRLLabel;
    RLDraw9: TRLDraw;
    RLDraw10: TRLDraw;
    RLLabel11: TRLLabel;
    RLDraw11: TRLDraw;
    RLDraw12: TRLDraw;
    RLDraw14: TRLDraw;
    RLDraw15: TRLDraw;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel15: TRLLabel;
    RLDraw16: TRLDraw;
    RLDraw17: TRLDraw;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLDraw18: TRLDraw;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLDraw19: TRLDraw;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw20: TRLDraw;
    RLDraw21: TRLDraw;
    RLDraw22: TRLDraw;
    RLLabel25: TRLLabel;
    RLDraw23: TRLDraw;
    RLDraw24: TRLDraw;
    RLDraw25: TRLDraw;
    RLDraw28: TRLDraw;
    RLLabel26: TRLLabel;
    RLDraw29: TRLDraw;
    RLDraw26: TRLDraw;
    RLDraw27: TRLDraw;
    RLLabel27: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw30: TRLDraw;
    RLDraw31: TRLDraw;
    RLDraw32: TRLDraw;
    RLLabel28: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel30: TRLLabel;
    RLDraw33: TRLDraw;
    RLDraw34: TRLDraw;
    RLDraw35: TRLDraw;
    RLDraw36: TRLDraw;
    RLLabel31: TRLLabel;
    RLDraw37: TRLDraw;
    RLLabel32: TRLLabel;
    RLLabel33: TRLLabel;
    RLDraw38: TRLDraw;
    RLDraw39: TRLDraw;
    RLDraw41: TRLDraw;
    RLDraw42: TRLDraw;
    RLMemo2: TRLMemo;
    RLLabel34: TRLLabel;
    RLLabel43: TRLLabel;
    RLDraw47: TRLDraw;
    RLLabel40: TRLLabel;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel37: TRLLabel;
    RLLabel45: TRLLabel;
    RLLabel44: TRLLabel;
    RLDraw46: TRLDraw;
    RLLabel35: TRLLabel;
    RLLabel36: TRLLabel;
    RLDraw44: TRLDraw;
    RLDraw45: TRLDraw;
    RLDraw50: TRLDraw;
    RLDraw49: TRLDraw;
    RLDraw48: TRLDraw;
    RLDraw52: TRLDraw;
    RLDraw53: TRLDraw;
    RLDraw51: TRLDraw;
    RLDraw54: TRLDraw;
    RLDraw57: TRLDraw;
    RLDraw63: TRLDraw;
    RLLabel52: TRLLabel;
    RLLabel53: TRLLabel;
    RLDraw64: TRLDraw;
    RLLabel54: TRLLabel;
    RLLabel55: TRLLabel;
    RLDraw65: TRLDraw;
    RLDraw66: TRLDraw;
    RLDraw67: TRLDraw;
    RLDraw68: TRLDraw;
    RLLabel58: TRLLabel;
    RLLabel59: TRLLabel;
    RLLabel57: TRLLabel;
    RLDraw70: TRLDraw;
    RLDraw71: TRLDraw;
    RLImage3: TRLImage;
    RLImage4: TRLImage;
    RLLabel60: TRLLabel;
    RLDraw72: TRLDraw;
    RLDraw73: TRLDraw;
    RLDraw74: TRLDraw;
    RLDraw75: TRLDraw;
    RLDraw76: TRLDraw;
    RLDraw77: TRLDraw;
    RLDraw78: TRLDraw;
    RLDraw79: TRLDraw;
    RLDraw82: TRLDraw;
    RLDraw83: TRLDraw;
    RLLabel61: TRLLabel;
    RLLabel62: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel63: TRLLabel;
    RLLabel64: TRLLabel;
    RLDraw84: TRLDraw;
    RLDraw85: TRLDraw;
    RLLabel68: TRLLabel;
    RLLabel67: TRLLabel;
    RLLabel66: TRLLabel;
    RLLabel65: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel23: TRLLabel;
    RLMemo3: TRLMemo;
    RLMemo4: TRLMemo;
    RLDraw86: TRLDraw;
    RLLabel56: TRLLabel;
    RLDraw55: TRLDraw;
    RLDraw56: TRLDraw;
    RLLabel47: TRLLabel;
    RLDraw58: TRLDraw;
    RLDraw59: TRLDraw;
    RLDraw60: TRLDraw;
    RLDraw61: TRLDraw;
    RLLabel48: TRLLabel;
    RLLabel49: TRLLabel;
    RLLabel50: TRLLabel;
    RLLabel51: TRLLabel;
    RLDraw80: TRLDraw;
    RLDraw8: TRLDraw;
    RLLabel10: TRLLabel;
    RLDraw62: TRLDraw;
    RLLabel46: TRLLabel;
    RLDraw81: TRLDraw;
    RLLabel69: TRLLabel;
    RLDraw87: TRLDraw;
    RLLabel70: TRLLabel;
    RLDraw88: TRLDraw;
    motorista: TRLLabel;
    transportadora: TRLLabel;
    veiculo: TRLLabel;
    NR_ROMANEIO: TRLLabel;
    placa: TRLLabel;
    DTIC: TRLLabel;
    motorista1: TRLLabel;
    transportadora1: TRLLabel;
    veiculo1: TRLLabel;
    nr_romaneio1: TRLLabel;
    placa1: TRLLabel;
    DTIC1: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLDraw13: TRLDraw;
    RLDraw69: TRLDraw;
    ADOQRep: TADOQuery;
    ADOQRepRAZSOC: TStringField;
    ADOQRepcodcgc: TStringField;
    DSRep: TDataSource;
    QManifesto: TADOQuery;
    QManifestoNR_ROMANEIO: TBCDField;
    QManifestoMOTORISTA: TStringField;
    QManifestoPLACA: TStringField;
    QManifestoVEICULO: TStringField;
    QManifestoDTINC: TDateTimeField;
    QManifestoTRANSPORTADORA: TStringField;
    QManifestoCODFIL: TBCDField;
    QManifestoTIPO: TStringField;
    QManifestoSERIE: TStringField;
    dtsManifesto: TDataSource;
    QPallet: TADOQuery;
    QPalletTIPO: TStringField;
    QPalletCODEAN: TStringField;
    QPalletCODMAN: TFMTBCDField;
    QPalletCODPAG: TFMTBCDField;
    QPalletCLIENTE: TStringField;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDiario_bordo_roldao: TfrmDiario_bordo_roldao;

implementation

{$R *.dfm}

uses Dados;

procedure TfrmDiario_bordo_roldao.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  motorista.Caption := QManifestoMOTORISTA.AsString;
  motorista1.Caption := QManifestoMOTORISTA.AsString;
  transportadora.Caption := QManifestoTRANSPORTADORA.AsString;
  transportadora1.Caption := QManifestoTRANSPORTADORA.AsString;
  veiculo.Caption := QManifestoVEICULO.AsString;
  veiculo1.Caption := QManifestoVEICULO.AsString;
  placa.Caption := QManifestoPLACA.AsString;
  placa1.Caption := QManifestoPLACA.AsString;
  NR_ROMANEIO.Caption := VarToStr(QManifestoNR_ROMANEIO.Value);
  nr_romaneio1.Caption := VarToStr(QManifestoNR_ROMANEIO.Value);
  DTIC.Caption := DateTimeToStr(QManifestoDTINC.Value);
  DTIC1.Caption := DateTimeToStr(QManifestoDTINC.Value);
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.Add
    ('select diario_bordo from tb_parametro ');
  dtmdados.IQuery1.SQL.Add
    (' where codcli = :0 ');
  dtmdados.IQuery1.Parameters[0].value := QPalletCODPAG.AsInteger;
  dtmdados.IQuery1.open;
  if dtmdados.IQuery1.FieldByName('diario_bordo').AsString = 'S' then
  begin
    RLImage2.Picture.RegisterFileFormat('BMP', 'BMP file', TBitmap);
    RLImage2.Picture.LoadFromFile
      ('\\192.168.236.112\sim\Atualizador\Relatorio\rold�o.bmp');
    RLImage4.Picture.RegisterFileFormat('BMP', 'BMP file', TBitmap);
    RLImage4.Picture.LoadFromFile
      ('\\192.168.236.112\sim\Atualizador\Relatorio\rold�o.bmp');
  end;
  dtmdados.IQuery1.close;
end;

end.
