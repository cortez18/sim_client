unit DetalhesCustoFrete_km;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, JvBaseEdits, Buttons, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, DB, ADODB, xmldom, XMLIntf,
  msxmldom, XMLDoc, JvMemoryDataset;

type
  TfrmDetalhesCustoFrete_km = class(TForm)
    dtskm: TDataSource;
    QKM: TADOQuery;
    XMLDoc: TXMLDocument;
    QKMDESCRI: TStringField;
    QKMESTADO: TStringField;
    QKMKM: TFloatField;
    JvDBGrid3: TJvDBGrid;
    mdKm: TJvMemoryData;
    mdKmkm: TFloatField;
    mdKMESTADO: TStringField;
    mdkmdescri: TStringField;
    QKMGEO: TStringField;
    mdKmgeo: TStringField;
    mdKmcodcep: TStringField;
    QKMCODCEP: TStringField;
    QKMCODCLIFOR: TBCDField;
    mdKmcodclifor: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    procedure calculakm;
  public
    { Public declarations }
  end;

var
  frmDetalhesCustoFrete_km: TfrmDetalhesCustoFrete_km;

implementation

uses RelCusto_frete_sim, Dados, Menu, MSHTML, MSXML, UrlMon, funcoes;

{$R *.dfm}

procedure TfrmDetalhesCustoFrete_km.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QKM.close;
end;

procedure TfrmDetalhesCustoFrete_km.FormCreate(Sender: TObject);
begin
  QKM.close;
  QKM.SQL.Clear;
  if frmRelCusto_frete_sim.mdRepreTipo.Value = 'Romaneio' then
  begin
    QKM.SQL.Add('select distinct lpad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)),8,0) codcep, m.descri, m.estado, (d.latitu ||''+''|| d.longit) geo, d.codclifor ');
    QKM.SQL.Add('from cyber.tb_coleta i left join tb_conhecimento c on i.doc = c.nr_conhecimento and i.filial = c.fl_empresa ');
    QKM.SQL.Add('left join cyber.rodcli d on d.codclifor = (case when c.cod_redespacho > 0 then c.cod_redespacho else c.cod_destinatario end) ');
    QKM.SQL.Add('left join cyber.rodmun m on m.codmun = d.codmun ');
    QKM.SQL.Add('where i.nr_oc = :0 ');
    QKM.Parameters[0].Value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
  end
  else
  begin
    QKM.SQL.Add('select manifesto, filial, serie, codcep, descri, estado, geo, codclifor from (select distinct i.manifesto, i.filial, i.serie, lpad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)),8,0) codcep, m.descri, m.estado, (d.latitu ||''+''|| d.longit) geo, d.codclifor ');
    QKM.SQL.Add('from tb_manitem i left join tb_conhecimento c on i.coddoc = c.nr_conhecimento and i.fildoc = c.fl_empresa and i.tipdoc = ''C'' ');
    QKM.SQL.Add('left join cyber.rodcli d on d.codclifor = (case when c.cod_redespacho > 0 then c.cod_redespacho else c.cod_destinatario end) ');
    QKM.SQL.Add('left join cyber.rodmun m on m.codmun = d.codmun ');
    QKM.SQL.Add('union all ');
    QKM.SQL.Add('select distinct i.manifesto, i.filial, i.serie, lpad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)),8,0) codcep, m.descri, m.estado, (d.latitu ||''+''|| d.longit) geo, d.codclifor ');
    QKM.SQL.Add('from tb_manitem i left join cyber.rodord c on i.coddoc = c.codigo and i.fildoc = c.codfil and i.tipdoc = ''O'' ');
    QKM.SQL.Add('left join cyber.rodcli d on d.codclifor = (case when c.terent > 0 then c.terent else c.coddes end) ');
    QKM.SQL.Add('left join cyber.rodmun m on m.codmun = d.codmun) ');
    QKM.SQL.Add('where manifesto = :0 and filial = :1 and serie = :2 and codclifor is not null order by 4 ');
    QKM.Parameters[0].Value := frmRelCusto_frete_sim.mdRepreDOC.AsInteger;
    QKM.Parameters[1].Value := frmRelCusto_frete_sim.mdRepreFilial.AsInteger;
    QKM.Parameters[2].Value := frmRelCusto_frete_sim.mdRepreserie.AsString;
  end;
//showmessage(QKM.SQL.Text);
  QKM.open;
  if not QKM.eof then
  begin
    mdKm.close;
    mdKm.open;
    while not QKM.eof do
    begin
      mdKm.Append;
      mdKmcodcep.Value := QKMCODCEP.Value;
      mdkmdescri.Value := QKMDESCRI.Value;
      mdKMESTADO.Value := QKMESTADO.Value;
      mdKmgeo.Value := QKMGEO.Value;
      mdKmcodclifor.Value := QKMCODCLIFOR.AsInteger;
      QKM.Next;
    end;
    QKM.close;
    mdKm.First;
    calculakm;
    // if km > 0 then
    // km := (km * 2)
    // else
    // Km := 0;
  end;
end;

procedure TfrmDetalhesCustoFrete_km.calculakm;
var
  XMLUrl, cep, orig, dest, XMLFileName, a: String;
  origem, descri, uf: String;
  Met, i, x, j, k: Integer;
  Sec: TTime;
  t, km: Double;
begin
  x := 0;
  j := 0;
  t := 0;
  // pegar o cep da filial como ponto de partida
  if frmRelCusto_frete_sim.mdRepreTipo.Value = 'Romaneio' then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.add
      ('select d.codcep, m.descri, m.estado, (d.latitu ||''+''|| d.longit) cep from cyber.tb_coleta f left join cyber.rodcli d on d.codclifor = f.remetente ');
    dtmdados.IQuery1.SQL.add
      ('left join cyber.rodmun m on d.codmun = m.codmun where f.nr_oc = :0 ');
    dtmdados.IQuery1.Parameters[0].Value :=
      frmRelCusto_frete_sim.mdRepreDOC.AsInteger; // glbfilial;
    dtmdados.IQuery1.open;
  end
  else
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.add
      ('select f.codcep, m.descri, m.estado, (f.latitu ||''+''|| f.longit) cep ');
    dtmdados.IQuery1.SQL.add
      ('from cyber.rodfil f left join cyber.rodmun m on f.codmun = m.codmun where f.codfil = :0 ');
    dtmdados.IQuery1.Parameters[0].Value :=
      frmRelCusto_frete_sim.mdRepreFILIAL.AsInteger; // glbfilial;
    dtmdados.IQuery1.open;
  end;
  cep := dtmdados.IQuery1.FieldByName('cep').Value;
  origem := dtmdados.IQuery1.FieldByName('codcep').AsString;
  descri := dtmdados.IQuery1.FieldByName('descri').Value;
  uf := dtmdados.IQuery1.FieldByName('estado').Value;
  dtmdados.IQuery1.close;

  if mdKm.RecordCount > 0 then
  begin
    while not mdKm.eof do
    begin
      if x = 0 then
      begin
        orig := cep;

        if ApCarac(mdKmgeo.Value) <> '' then
          dest := mdKmgeo.AsString
        else
          dest := mdKmcodcep.AsString + '+' + mdkmdescri.AsString;

        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end
      else
      begin
        if ApCarac(mdKmgeo.Value) <> '' then
          dest := mdKmgeo.AsString
        else
          dest := mdKmcodcep.AsString + '+' + mdkmdescri.AsString;
        XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
          + orig + '&destinations=' + dest +
          '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
        orig := dest;
      end;

      XMLFileName := 'C:\sim\temp.xml';

      URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

      XMLDoc.FileName := 'C:\sim\temp.xml';
      XMLDoc.Active := true;
      i := 0;
      j := 0;
      k := 0;
      for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
      begin
        a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
        if a = 'row' then
        begin
          for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
            for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
              .ChildNodes.Count - 1) do
            begin
              a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName;
              if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
                [k].NodeName = 'distance' then
              begin
                // edDistancia.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['value'].Text;
                // edit1.Text := XMLDoc.DocumentElement.ChildNodes[I].ChildNodes[j].ChildNodes[k].ChildNodes['text'].Text;

                km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes
                  [j].ChildNodes[k].ChildNodes['text'].Text) + km;
                // edDistancia.Text := FloatToStr(t);
                mdKm.edit;
                mdKmkm.Value :=
                  sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
                  .ChildNodes[k].ChildNodes['text'].Text);
                mdKm.Post;
              end;
            end;
          end;
        end;
      end;
      XMLDoc.Active := false;
      x := x + 1;
      mdKm.Next;
    end;
    // pegar o �ltimo km e colocar o retorno a origem
    XMLUrl := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins='
      + orig + '&destinations=' + cep +
      '&mode=driving&language=br-BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJhg';
    XMLFileName := 'C:\sim\temp.xml';
    URLDownloadToFile(Nil, PChar(XMLUrl), PChar(XMLFileName), 0, Nil);

    XMLDoc.FileName := 'C:\sim\temp.xml';
    XMLDoc.Active := true;
    // i := 0;
    // j := 0;
    // k := 0;
    for i := 0 to (XMLDoc.DocumentElement.ChildNodes.Count - 1) do
    begin
      a := XMLDoc.DocumentElement.ChildNodes[i].NodeName;
      if a = 'row' then
      begin
        for j := 0 to (XMLDoc.DocumentElement.ChildNodes[i]
          .ChildNodes.Count - 1) do
        begin
          a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].NodeName;
          for k := 0 to (XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
            .ChildNodes.Count - 1) do
          begin
            a := XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes
              [k].NodeName;
            if XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j].ChildNodes[k]
              .NodeName = 'distance' then
            begin
              km := sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
                .ChildNodes[k].ChildNodes['text'].Text) + km;

              mdKm.Append;
              mdKMESTADO.Value := uf;
              mdKmcodcep.AsString := ApCarac(origem);
              mdkmdescri.Value := descri;

              mdKmkm.Value :=
                sonumero(XMLDoc.DocumentElement.ChildNodes[i].ChildNodes[j]
                .ChildNodes[k].ChildNodes['text'].Text);
              mdKm.Post;
            end;
          end;
        end;
      end;
    end;
    XMLDoc.Active := false;
  end;
end;

end.
