object frmOrdem_Coleta: TfrmOrdem_Coleta
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Gerar Ordem de Coleta'
  ClientHeight = 601
  ClientWidth = 1194
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 345
    Height = 601
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 345
      Height = 69
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object lblDescricao: TLabel
        Left = 6
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Procurar OCS'
        Transparent = True
      end
      object Label45: TLabel
        Left = 244
        Top = 4
        Width = 58
        Height = 13
        Caption = 'A partir de :'
        Transparent = True
      end
      object edValor: TEdit
        Left = 6
        Top = 23
        Width = 110
        Height = 21
        TabOrder = 0
        OnEnter = edValorEnter
        OnExit = edValorExit
      end
      object edData: TJvDateEdit
        Left = 244
        Top = 20
        Width = 91
        Height = 21
        ShowNullDate = False
        YearDigits = dyFour
        TabOrder = 1
        OnChange = edDataChange
      end
    end
    object pnTarefas: TPanel
      Left = 0
      Top = 69
      Width = 345
      Height = 532
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
      object pgTarefas: TPageControl
        Left = 1
        Top = 1
        Width = 343
        Height = 530
        ActivePage = tbTarefa
        Align = alClient
        TabOrder = 0
        object tbTarefa: TTabSheet
          Caption = 'OCS'
          ImageIndex = 1
          OnShow = tbTarefaShow
          object dbgColeta: TJvDBUltimGrid
            Left = 0
            Top = 0
            Width = 335
            Height = 502
            Align = alClient
            Ctl3D = False
            DataSource = dsTemp
            Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentCtl3D = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnCellClick = dbgColetaCellClick
            OnDrawColumnCell = dbgColetaDrawColumnCell
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 16
            Columns = <
              item
                Expanded = False
                FieldName = 'Coleta'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'filial'
                Visible = False
              end
              item
                Expanded = False
                FieldName = 'Inserida'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'data'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'destino'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 345
    Top = 0
    Width = 849
    Height = 601
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 849
      Height = 601
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Gerar OC'
        OnShow = TabSheet1Show
        object lblqt: TLabel
          Left = 8
          Top = 3
          Width = 6
          Height = 16
          Caption = '  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object pnManifesto: TPanel
          Left = 0
          Top = 428
          Width = 841
          Height = 145
          Align = alBottom
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 0
          object Label5: TLabel
            Left = 396
            Top = 101
            Width = 37
            Height = 13
            Caption = 'Ve'#237'culo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TLabel
            Left = 8
            Top = 101
            Width = 49
            Height = 13
            Caption = 'Cap. Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 87
            Top = 101
            Width = 70
            Height = 13
            Caption = 'Cap. Cubagem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 171
            Top = 101
            Width = 42
            Height = 13
            Caption = 'Carga % '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 233
            Top = 101
            Width = 64
            Height = 13
            Caption = 'Qtd. Destinos'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label58: TLabel
            Left = 8
            Top = 9
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 186
            Top = 9
            Width = 72
            Height = 13
            Caption = 'Transportadora'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 6
            Top = 57
            Width = 43
            Height = 13
            Caption = 'Motorista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 357
            Top = 57
            Width = 27
            Height = 13
            Caption = 'Placa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 480
            Top = 57
            Width = 64
            Height = 13
            Caption = 'Placa Carreta'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label27: TLabel
            Left = 313
            Top = 101
            Width = 27
            Height = 13
            Caption = 'Custo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TLabel
            Left = 632
            Top = 57
            Width = 67
            Height = 13
            Caption = 'Total Volumes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label25: TLabel
            Left = 632
            Top = 101
            Width = 51
            Height = 13
            Caption = 'Total Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TLabel
            Left = 738
            Top = 101
            Width = 64
            Height = 13
            Caption = 'Total Cubado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label26: TLabel
            Left = 739
            Top = 57
            Width = 41
            Height = 13
            Caption = 'Total NF'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label49: TLabel
            Left = 564
            Top = 100
            Width = 16
            Height = 13
            Caption = 'KM'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label50: TLabel
            Left = 634
            Top = 9
            Width = 29
            Height = 13
            Caption = 'Isca 1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label51: TLabel
            Left = 737
            Top = 9
            Width = 29
            Height = 13
            Caption = 'Isca 2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edPesoLimiteVeiculo: TJvCalcEdit
            Left = 8
            Top = 117
            Width = 69
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object edQtdCubagem: TJvCalcEdit
            Left = 87
            Top = 117
            Width = 70
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object edPercentCarga: TJvCalcEdit
            Left = 171
            Top = 117
            Width = 51
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object edQtdDestino: TJvCalcEdit
            Left = 233
            Top = 117
            Width = 66
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
          end
          object cbOperacao: TComboBox
            Left = 8
            Top = 25
            Width = 172
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnChange = cbOperacaoChange
            Items.Strings = (
              ''
              'DISTRIBUI'#199#195'O'
              'FRACIONADO'
              'TRANSFER'#202'NCIA')
          end
          object cbTransp: TJvDBLookupEdit
            Left = 188
            Top = 25
            Width = 329
            Height = 21
            DropDownCount = 20
            LookupDisplay = 'NOME'
            LookupField = 'CODIGO'
            LookupSource = dtsTrans
            TabOrder = 5
            Text = ''
            OnExit = cbTranspExit
          end
          object cbMotorista: TComboBox
            Left = 6
            Top = 73
            Width = 342
            Height = 21
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object cbplaca: TComboBox
            Left = 356
            Top = 73
            Width = 118
            Height = 21
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            OnChange = cbplacaChange
          end
          object cbPlaca_carreta: TComboBox
            Left = 480
            Top = 73
            Width = 108
            Height = 21
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object edVeiculo: TJvMaskEdit
            Left = 397
            Top = 117
            Width = 160
            Height = 21
            TabStop = False
            Color = clSilver
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 9
            Text = ''
          end
          object edCusto: TJvCalcEdit
            Left = 313
            Top = 117
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 10
            DecimalPlacesAlwaysShown = False
          end
          object edTotVolumeTarefa: TJvCalcEdit
            Left = 632
            Top = 73
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 11
            DecimalPlacesAlwaysShown = False
          end
          object edTotPesoTarefa: TJvCalcEdit
            Left = 632
            Top = 117
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 12
            DecimalPlacesAlwaysShown = False
          end
          object edTotCubadoTarefa: TJvCalcEdit
            Left = 738
            Top = 117
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 13
            DecimalPlacesAlwaysShown = False
          end
          object edTotValorTarefa: TJvCalcEdit
            Left = 739
            Top = 73
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 14
            DecimalPlacesAlwaysShown = False
          end
          object edKm: TJvCalcEdit
            Left = 563
            Top = 117
            Width = 62
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '#,###0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 15
            DecimalPlacesAlwaysShown = False
          end
          object cbIsca1: TJvComboBox
            Left = 635
            Top = 25
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 16
            Text = ''
          end
          object cbIsca2: TJvComboBox
            Left = 736
            Top = 25
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 17
            Text = ''
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 841
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object btnNovoManifesto: TBitBtn
            Left = 276
            Top = 3
            Width = 105
            Height = 25
            Hint = 'Novo Manifesto'
            Caption = 'Novo'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
              0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
              33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = btnNovoManifestoClick
          end
          object btnGerarMan: TBitBtn
            Left = 390
            Top = 4
            Width = 105
            Height = 25
            Hint = 'Gerar Manifesto'
            Caption = 'Gerar Ordem'
            Enabled = False
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
              FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
              96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
              92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
              BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
              CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
              D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
              EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
              E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
              E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
              8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
              E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
              FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
              C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
              CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
              CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
              D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
              9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnGerarManClick
          end
          object btnCarregar_Tarefas: TBitBtn
            Left = 159
            Top = 4
            Width = 111
            Height = 25
            Hint = 'Carrega Tarefas para Gerar Romaneio'
            Caption = 'Carregar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF0051A6
              58B84FA356AEFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00418E4517408D43EC3F8B42B3FF00FF0054AB5CB369B3
              6FFF67B16DFF4FA456AEFF00FF004C9F52FFFF00FF00FF00FF00FF00FF00FF00
              FF00459449FF43924817429046F15CA160FF569C5AFF3F8C42B856AD5FEC71B8
              77FF83BF89FF68B16EFF50A457FF4EA255FFFF00FF00FF00FF00FF00FF00FF00
              FF0046964BFF4A974EFF5FA462FF71B075FF579E5BFF408D44AE58B0611757AE
              5FF171B877FF74B87BFF80BD86FF50A457FFFF00FF00FF00FF00FF00FF00FF00
              FF0048994DFF73B377FF63A968FF5AA15EFF429147AEFF00FF00FF00FF0058B0
              61175CB064FF84C18AFF86C18BFF52A759FFFF00FF00FF00FF00FF00FF00FF00
              FF004A9B4FFF79B67EFF74B379FF45954AFFFF00FF00FF00FF00FF00FF005AB3
              63FF59B161FF57AE5FFF55AC5DFF54AA5BFFFF00FF00FF00FF00FF00FF00FF00
              FF004B9E51FF4A9C50FF48994EFF47974CFF45954AFFFF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0062BF
              6DFF61BD6CFF60BB6AFF5EB968FF5DB767FFFF00FF00FF00FF00FF00FF00FF00
              FF0055AB5DFF53A95BFF52A759FF50A457FF4EA255FFFF00FF00FF00FF00FF00
              FF0063BF6DFF97D19FFF98D0A0FF5FB969FFFF00FF00FF00FF00FF00FF00FF00
              FF0057AE5FFF8CC692FF87C38DFF57AA5EFF50A55717FF00FF00FF00FF0065C3
              70AE7ECA87FF8ECD98FF97D19FFF60BC6BFFFF00FF00FF00FF00FF00FF00FF00
              FF0059B161FF8CC793FF7DBF85FF72B979FF52A759F150A5571767C673AE80CD
              8AFF9ED6A7FF83CC8CFF69C273FF62BE6CFFFF00FF00FF00FF00FF00FF00FF00
              FF005AB364FF59B162FF70BB78FF8DC794FF73BA7AFF52A85AEC68C774B881CE
              8BFF86CF90FF65C371F164C26F1763C06EFFFF00FF00FF00FF00FF00FF00FF00
              FF005CB666FFFF00FF0059B162AE71BB79FF6FB977FF54AA5CB3FF00FF0068C7
              74B367C673EC66C57217FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF0059B262AE58AF60B8FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = btnCarregar_TarefasClick
          end
          object btnVoltaTarefa: TBitBtn
            Left = 45
            Top = 4
            Width = 100
            Height = 25
            Hint = 'Volta Tarefa'
            Caption = 'Volta OCS'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
              B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
              E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
              EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
              D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
              E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
              D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
              E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
              D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
              E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
              DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
              DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
              DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
              ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
              E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
              F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
              E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
              F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
              FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
              FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
              F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
              D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = False
            OnClick = btnVoltaTarefaClick
          end
        end
        object dgTarefa: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 841
          Height = 396
          Align = alClient
          Ctl3D = False
          DataSource = dstTarefa
          DrawingStyle = gdsClassic
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'NRO_OCS'
              Title.Alignment = taCenter
              Title.Caption = 'OCS'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FILIAL'
              Title.Alignment = taCenter
              Title.Caption = 'Filial'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CLI'
              Title.Alignment = taCenter
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REM'
              Title.Alignment = taCenter
              Title.Caption = 'Origem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Title.Alignment = taCenter
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DES'
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESO'
              Title.Alignment = taCenter
              Title.Caption = 'Peso'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 68
              Visible = True
            end>
        end
        object RLReport1: TRLReport
          Left = 20
          Top = 1000
          Width = 794
          Height = 1123
          DataSource = dtsIt
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          BeforePrint = RLReport1BeforePrint
          object RLBand1: TRLBand
            Left = 38
            Top = 38
            Width = 718
            Height = 113
            BandType = btColumnHeader
            Borders.Sides = sdCustom
            Borders.DrawLeft = True
            Borders.DrawTop = True
            Borders.DrawRight = True
            Borders.DrawBottom = True
            object RLLabel1: TRLLabel
              Left = 3
              Top = 12
              Width = 39
              Height = 16
              Caption = 'Filial :'
            end
            object RLLabel2: TRLLabel
              Left = 19
              Top = 45
              Width = 91
              Height = 16
              Caption = 'Transportador :'
            end
            object RLLabel3: TRLLabel
              Left = 35
              Top = 72
              Width = 75
              Height = 16
              Caption = 'Remetente :'
            end
            object RLLabel4: TRLLabel
              Left = 531
              Top = 45
              Width = 58
              Height = 16
              Caption = 'Data OC:'
            end
            object RLLabel5: TRLLabel
              Left = 543
              Top = 72
              Width = 45
              Height = 16
              Caption = 'Placa :'
            end
            object RLLabel6: TRLLabel
              Left = 247
              Top = 12
              Width = 232
              Height = 18
              Alignment = taCenter
              Caption = 'Ordem de Coleta - Descarga N'#186' :'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object RLLabel7: TRLLabel
              Left = 29
              Top = 94
              Width = 74
              Height = 16
              Caption = 'Nota Fiscal'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object RLLabel8: TRLLabel
              Left = 216
              Top = 94
              Width = 53
              Height = 16
              Caption = 'Volume'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object RLLabel9: TRLLabel
              Left = 429
              Top = 94
              Width = 65
              Height = 16
              Caption = 'Cubagem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object RLDraw1: TRLDraw
              Left = 0
              Top = 90
              Width = 715
              Height = 2
              DrawKind = dkLine
            end
            object RLDBText1: TRLDBText
              Left = 124
              Top = 48
              Width = 57
              Height = 16
              DataField = 'RAZSOC'
              DataSource = dtsOCa
              Text = ''
            end
            object RLDBText2: TRLDBText
              Left = 124
              Top = 70
              Width = 33
              Height = 16
              DataField = 'REM'
              DataSource = dtsOCa
              Text = ''
            end
            object RLDBText3: TRLDBText
              Left = 595
              Top = 45
              Width = 50
              Height = 16
              DataField = 'DATINC'
              DataSource = dtsOCa
              Text = ''
            end
            object RLDBText4: TRLDBText
              Left = 594
              Top = 70
              Width = 47
              Height = 16
              DataField = 'PLACA'
              DataSource = dtsOCa
              Text = ''
            end
            object RLDBText5: TRLDBText
              Left = 48
              Top = 11
              Width = 45
              Height = 16
              DataField = 'FILIAL'
              DataSource = dtsOCa
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              Text = ''
            end
            object RLSystemInfo1: TRLSystemInfo
              Left = 674
              Top = 14
              Width = 37
              Height = 16
              Alignment = taRightJustify
              Info = itNow
              Text = ''
            end
          end
          object RLBand2: TRLBand
            Left = 38
            Top = 151
            Width = 718
            Height = 30
            object RLDBText6: TRLDBText
              Left = 42
              Top = 6
              Width = 50
              Height = 16
              DataField = 'NOTFIS'
              DataSource = dtsIt
              Text = ''
            end
            object RLDBText7: TRLDBText
              Left = 219
              Top = 6
              Width = 51
              Height = 16
              DataField = 'QUANTI'
              DataSource = dtsIt
              Text = ''
            end
            object RLDraw2: TRLDraw
              Left = 408
              Top = 24
              Width = 161
              Height = 5
              DrawKind = dkLine
            end
          end
          object RLBand3: TRLBand
            Left = 38
            Top = 181
            Width = 718
            Height = 64
            BandType = btFooter
            Borders.Sides = sdCustom
            Borders.DrawLeft = True
            Borders.DrawTop = True
            Borders.DrawRight = True
            Borders.DrawBottom = True
            object RLLabel10: TRLLabel
              Left = 86
              Top = 42
              Width = 118
              Height = 16
              Caption = 'Data da Confer'#234'ncia'
            end
            object RLLabel11: TRLLabel
              Left = 458
              Top = 42
              Width = 144
              Height = 16
              Caption = 'Assinatura Conferente'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object RLLabel12: TRLLabel
              Left = 48
              Top = 20
              Width = 180
              Height = 16
              Caption = '_______/________/_________'
            end
            object RLLabel13: TRLLabel
              Left = 416
              Top = 20
              Width = 242
              Height = 16
              Caption = '__________________________________'
            end
          end
        end
      end
      object tsOrdemG: TTabSheet
        Caption = 'Ordem de Coleta - Aberta'
        ImageIndex = 4
        OnShow = tsOrdemGShow
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 841
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label7: TLabel
            Left = 6
            Top = 9
            Width = 81
            Height = 13
            Caption = 'Ordem de Coleta'
          end
          object Label12: TLabel
            Left = 187
            Top = 9
            Width = 21
            Height = 13
            Caption = 'N.F.'
          end
          object btnDescarga: TBitBtn
            Left = 622
            Top = 5
            Width = 81
            Height = 25
            Hint = 'Imprime Descarga'
            Caption = 'Descarga'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnDescargaClick
          end
          object btnCanTransf: TBitBtn
            Left = 508
            Top = 6
            Width = 105
            Height = 25
            Hint = 'Cancela Manifesto Transfer'#234'ncia'
            Caption = 'Cancelar'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
              33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
              993337777F777F3377F3393999707333993337F77737333337FF993399933333
              399377F3777FF333377F993339903333399377F33737FF33377F993333707333
              399377F333377FF3377F993333101933399377F333777FFF377F993333000993
              399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
              99333773FF777F777733339993707339933333773FF7FFF77333333999999999
              3333333777333777333333333999993333333333377777333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnCanTransfClick
          end
          object btnGeraTransf: TBitBtn
            Left = 375
            Top = 5
            Width = 124
            Height = 25
            Hint = 'Gerar Recebimento'
            Caption = 'Gerar Recebimento'
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
              FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
              96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
              92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
              BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
              CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
              D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
              EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
              E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
              E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
              8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
              E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
              FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
              C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
              CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
              CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
              D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
              9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = btnGeraTransfClick
          end
          object edOCa: TJvCalcEdit
            Left = 92
            Top = 5
            Width = 66
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
            OnEnter = edOCaEnter
            OnExit = edOCaExit
          end
          object edNF: TJvCalcEdit
            Left = 214
            Top = 5
            Width = 82
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 4
            DecimalPlacesAlwaysShown = False
            OnEnter = edNFEnter
            OnExit = edNFExit
          end
        end
        object JvDBUltimGrid1: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 841
          Height = 352
          Align = alClient
          Ctl3D = False
          DataSource = dtsOCa
          DrawingStyle = gdsClassic
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = JvDBUltimGrid1DrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          AutoSizeRows = False
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DATINC'
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_ORDEM'
              Title.Alignment = taCenter
              Title.Caption = 'Ordem Coleta'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 81
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FILIAL'
              Title.Alignment = taCenter
              Title.Caption = 'Filial'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CLI'
              Title.Alignment = taCenter
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REM'
              Title.Alignment = taCenter
              Title.Caption = 'Origem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Title.Alignment = taCenter
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DES'
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RAZSOC'
              Title.Alignment = taCenter
              Title.Caption = 'Transportador'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERACAO'
              Title.Caption = 'Opera'#231#227'o'
              Visible = True
            end>
        end
        object DBGItens: TJvDBUltimGrid
          Left = 0
          Top = 384
          Width = 841
          Height = 189
          Align = alBottom
          Ctl3D = False
          DataSource = dtsIt
          DrawingStyle = gdsClassic
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          AlternateRowColor = 14089981
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'CODOCS'
              Title.Alignment = taCenter
              Title.Caption = 'OCS'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 67
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOTFIS'
              Title.Alignment = taCenter
              Title.Caption = 'Nota Fiscal'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESCUB'
              Title.Alignment = taCenter
              Title.Caption = 'Peso Cubado'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESOKG'
              Title.Alignment = taCenter
              Title.Caption = 'Peso KG'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESCAL'
              Title.Alignment = taCenter
              Title.Caption = 'Peso Calculado'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VLRMER'
              Title.Alignment = taCenter
              Title.Caption = 'Valor NF.'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTI'
              Title.Alignment = taCenter
              Title.Caption = 'Volume'
              Title.Color = 8404992
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWhite
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 60
              Visible = True
            end>
        end
      end
      object tbOCR: TTabSheet
        Caption = 'Ordem de Coleta - Recebida'
        ImageIndex = 2
        object dgCtrc: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 841
          Height = 313
          Align = alTop
          Ctl3D = False
          DataSource = dtsOCr
          DrawingStyle = gdsClassic
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'ID_ORDEM'
              Title.Caption = 'Ordem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 53
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATINC'
              Title.Caption = 'Data'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TRANSP'
              Title.Caption = 'Transportador'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 144
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRI'
              Title.Caption = 'Ve'#237'culo'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 61
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CLI'
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REM'
              Title.Caption = 'Remetente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 231
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODOCS'
              Title.Caption = 'OCS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERACAO'
              Title.Caption = 'Opera'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUINC'
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CONFERIDO'
              Title.Caption = 'Conferido Por'
              Width = 58
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 841
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object Label3: TLabel
            Left = 6
            Top = 9
            Width = 81
            Height = 13
            Caption = 'Ordem de Coleta'
          end
          object Label10: TLabel
            Left = 165
            Top = 9
            Width = 53
            Height = 13
            Caption = 'Data Inicial'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 316
            Top = 9
            Width = 48
            Height = 13
            Caption = 'Data Final'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label17: TLabel
            Left = 472
            Top = 9
            Width = 21
            Height = 13
            Caption = 'OCS'
          end
          object btnImprMan: TBitBtn
            Left = 676
            Top = 5
            Width = 74
            Height = 25
            Caption = 'Consultar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
              1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
              96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
              98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
              36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
              6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
              3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
              6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
              42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
              96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
              42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
              FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
              4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
              FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
              54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
              C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
              597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
              71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
              5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
              75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
              FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
              9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
              A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
              52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnImprManClick
          end
          object edOC: TJvCalcEdit
            Left = 92
            Top = 5
            Width = 65
            Height = 21
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object dtInicial: TJvDateEdit
            Left = 225
            Top = 5
            Width = 86
            Height = 21
            ShowNullDate = False
            YearDigits = dyTwo
            TabOrder = 2
          end
          object dtFinal: TJvDateEdit
            Left = 372
            Top = 5
            Width = 90
            Height = 21
            ShowNullDate = False
            TabOrder = 3
          end
          object btnDescarregado: TBitBtn
            Left = 582
            Top = 5
            Width = 81
            Height = 25
            Hint = 'Imprime Descarga'
            Caption = 'Descarga'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = btnDescarregadoClick
          end
          object edOCS: TJvCalcEdit
            Left = 500
            Top = 5
            Width = 65
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 5
            DecimalPlacesAlwaysShown = False
          end
          object btnEstornar: TBitBtn
            Left = 760
            Top = 5
            Width = 81
            Height = 25
            Caption = 'Estornar Baixa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            TabStop = False
            OnClick = btnEstornarClick
          end
        end
        object JvDBUltimGrid2: TJvDBUltimGrid
          Left = 0
          Top = 344
          Width = 841
          Height = 229
          Align = alBottom
          Ctl3D = False
          DataSource = dtsOCi
          DrawingStyle = gdsClassic
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'CODOCS'
              Title.Caption = 'OCS'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOTFIS'
              Title.Caption = 'Nota Fiscal'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESOKG'
              Title.Caption = 'Peso KG'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESCUB'
              Title.Caption = 'Peso Cub.'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESCAL'
              Title.Caption = 'Peso Calc.'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VLRMER'
              Title.Caption = 'Valor'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTI'
              Title.Caption = 'Volume'
              Width = 60
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Gerada e N'#227'o recebida'
        ImageIndex = 3
        OnHide = TabSheet2Hide
        OnShow = TabSheet2Show
        object JvDBUltimGrid3: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 841
          Height = 541
          Align = alClient
          Ctl3D = False
          DataSource = dtsOC
          Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'ID_ORDEM'
              Title.Caption = 'Ordem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATINC'
              Title.Caption = 'Data'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 106
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TRANSP'
              Title.Caption = 'Transportador'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 248
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRI'
              Title.Caption = 'Ve'#237'culo'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 57
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CLI'
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 159
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REM'
              Title.Caption = 'Remetente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 136
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERACAO'
              Title.Caption = 'Operacao'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODOCS'
              Title.Caption = 'OCS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUINC'
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 67
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 841
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object Label18: TLabel
            Left = 6
            Top = 9
            Width = 81
            Height = 13
            Caption = 'Ordem de Coleta'
          end
          object Label21: TLabel
            Left = 193
            Top = 9
            Width = 21
            Height = 13
            Caption = 'OCS'
          end
          object BitBtn1: TBitBtn
            Left = 315
            Top = 4
            Width = 105
            Height = 25
            Caption = 'Filtrar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
              1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
              96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
              98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
              36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
              6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
              3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
              6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
              42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
              96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
              42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
              FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
              4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
              FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
              54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
              C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
              597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
              71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
              5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
              75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
              FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
              9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
              A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
              52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object JvCalcEdit1: TJvCalcEdit
            Left = 92
            Top = 5
            Width = 65
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit2: TJvCalcEdit
            Left = 221
            Top = 5
            Width = 65
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object BitBtn2: TBitBtn
            Left = 443
            Top = 4
            Width = 105
            Height = 25
            Caption = 'Retirar Filtro'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
              EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
              F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
              F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
              F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
              F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
              FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
              FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
              FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
              FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
              FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
              FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
              FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
              FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
              FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
              FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
              FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
              FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
              FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
              FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
              FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
              FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
              FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
              FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BitBtn2Click
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'OC Canceladas'
        ImageIndex = 5
        OnHide = TabSheet4Hide
        OnShow = TabSheet4Show
        object JvDBGrid1: TJvDBGrid
          Left = 0
          Top = 0
          Width = 841
          Height = 573
          Align = alClient
          DataSource = dtsOCC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DATINC'
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_ORDEM'
              Title.Alignment = taCenter
              Title.Caption = 'O.C.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NRO_OCS'
              Title.Alignment = taCenter
              Title.Caption = 'OCS'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'datatu'
              Title.Alignment = taCenter
              Title.Caption = 'Data Canc.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CLI'
              Title.Alignment = taCenter
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'REM'
              Title.Alignment = taCenter
              Title.Caption = 'Remetente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DES'
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RAZSOC'
              Title.Alignment = taCenter
              Title.Caption = 'Transportador'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODCON'
              Title.Alignment = taCenter
              Title.Caption = 'CT-e'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'usuatu'
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Manuten'#231#227'o'
        ImageIndex = 4
        object Panel4: TPanel
          Left = 3
          Top = 11
          Width = 810
          Height = 327
          Color = 8224125
          ParentBackground = False
          TabOrder = 0
          object Label19: TLabel
            Left = 16
            Top = 7
            Width = 23
            Height = 13
            Caption = 'CT-e'
          end
          object Label22: TLabel
            Left = 139
            Top = 7
            Width = 20
            Height = 13
            Caption = 'Filial'
          end
          object Label23: TLabel
            Left = 214
            Top = 7
            Width = 66
            Height = 13
            Caption = 'Ordem Coleta'
          end
          object Label20: TLabel
            Left = 368
            Top = 7
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object JvCalcEdit3: TJvCalcEdit
            Left = 51
            Top = 6
            Width = 70
            Height = 21
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object DBGrid2: TDBGrid
            Left = 16
            Top = 33
            Width = 645
            Height = 280
            DataSource = dtsQM
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQUENCIA'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PAGADOR'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'REMETENTE'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NRO_OCS'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'USER_CTE'
                Width = 175
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OPERACAO'
                Title.Caption = 'OPERA'#199#195'O'
                Visible = True
              end>
          end
          object BitBtn3: TBitBtn
            Left = 614
            Top = 4
            Width = 71
            Height = 25
            Caption = 'Salvar'
            Enabled = False
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000BA6A368FB969
              35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
              32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
              ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
              B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
              B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
              C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
              B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
              BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
              BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
              76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
              C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
              77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
              C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
              77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
              C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
              65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
              C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
              E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
              CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
              EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
              D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
              F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
              D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
              F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
              D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
              F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
              3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
              39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BitBtn3Click
          end
          object JvCalcEdit5: TJvCalcEdit
            Left = 165
            Top = 6
            Width = 31
            Height = 21
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
            OnExit = JvCalcEdit5Exit
          end
          object JvCalcEdit6: TJvCalcEdit
            Left = 300
            Top = 6
            Width = 62
            Height = 21
            DecimalPlaces = 0
            DisplayFormat = '0'
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object cbom: TComboBox
            Left = 428
            Top = 6
            Width = 172
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            OnChange = cbOperacaoChange
            Items.Strings = (
              ''
              'DISTRIBUI'#199#195'O'
              'FRACIONADO'
              'TRANSFER'#202'NCIA')
          end
        end
      end
    end
  end
  object RLReport2: TRLReport
    Left = 351
    Top = 1000
    Width = 794
    Height = 1123
    DataSource = dtsOCi
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport2BeforePrint
    object RLBand4: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 113
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel14: TRLLabel
        Left = 3
        Top = 12
        Width = 39
        Height = 16
        Caption = 'Filial :'
      end
      object RLLabel15: TRLLabel
        Left = 19
        Top = 45
        Width = 91
        Height = 16
        Caption = 'Transportador :'
      end
      object RLLabel16: TRLLabel
        Left = 35
        Top = 72
        Width = 75
        Height = 16
        Caption = 'Remetente :'
      end
      object RLLabel17: TRLLabel
        Left = 531
        Top = 45
        Width = 58
        Height = 16
        Caption = 'Data OC:'
      end
      object RLLabel18: TRLLabel
        Left = 543
        Top = 72
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel19: TRLLabel
        Left = 247
        Top = 12
        Width = 232
        Height = 18
        Alignment = taCenter
        Caption = 'Ordem de Coleta - Descarga N'#186' :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel20: TRLLabel
        Left = 29
        Top = 94
        Width = 74
        Height = 16
        Caption = 'Nota Fiscal'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel21: TRLLabel
        Left = 216
        Top = 94
        Width = 53
        Height = 16
        Caption = 'Volume'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel22: TRLLabel
        Left = 429
        Top = 94
        Width = 65
        Height = 16
        Caption = 'Cubagem'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw3: TRLDraw
        Left = 0
        Top = 90
        Width = 715
        Height = 2
        DrawKind = dkLine
      end
      object RLDBText8: TRLDBText
        Left = 124
        Top = 48
        Width = 56
        Height = 16
        DataField = 'TRANSP'
        DataSource = dtsOCr
        Text = ''
      end
      object RLDBText9: TRLDBText
        Left = 124
        Top = 70
        Width = 33
        Height = 16
        DataField = 'REM'
        DataSource = dtsOCr
        Text = ''
      end
      object RLDBText10: TRLDBText
        Left = 595
        Top = 45
        Width = 50
        Height = 16
        DataField = 'DATINC'
        DataSource = dtsOCr
        Text = ''
      end
      object RLDBText11: TRLDBText
        Left = 594
        Top = 70
        Width = 47
        Height = 16
        DataField = 'PLACA'
        DataSource = dtsOCr
        Text = ''
      end
      object RLDBText12: TRLDBText
        Left = 48
        Top = 11
        Width = 45
        Height = 16
        DataField = 'FILIAL'
        DataSource = dtsOCr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Text = ''
      end
      object RLSystemInfo2: TRLSystemInfo
        Left = 674
        Top = 14
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
    end
    object RLBand5: TRLBand
      Left = 38
      Top = 151
      Width = 718
      Height = 30
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = False
      Borders.DrawRight = True
      Borders.DrawBottom = False
      object RLDBText13: TRLDBText
        Left = 43
        Top = 6
        Width = 50
        Height = 16
        DataField = 'NOTFIS'
        DataSource = dtsOCi
        Text = ''
      end
      object RLDBText14: TRLDBText
        Left = 218
        Top = 6
        Width = 51
        Height = 16
        DataField = 'QUANTI'
        DataSource = dtsOCi
        Text = ''
      end
      object RLDBText15: TRLDBText
        Left = 442
        Top = 6
        Width = 56
        Height = 16
        DataField = 'PESCAL'
        DataSource = dtsOCi
        Text = ''
      end
    end
    object RLBand6: TRLBand
      Left = 38
      Top = 181
      Width = 718
      Height = 64
      BandType = btFooter
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel23: TRLLabel
        Left = 86
        Top = 42
        Width = 118
        Height = 16
        Caption = 'Data da Confer'#234'ncia'
      end
      object RLLabel24: TRLLabel
        Left = 498
        Top = 42
        Width = 66
        Height = 16
        Caption = 'Conferido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDBText16: TRLDBText
        Left = 106
        Top = 20
        Width = 72
        Height = 16
        DataField = 'DT_RECEB'
        DataSource = dtsOCr
        Text = ''
      end
      object RLDBText17: TRLDBText
        Left = 495
        Top = 20
        Width = 80
        Height = 16
        DataField = 'CONFERIDO'
        DataSource = dtsOCr
        Text = ''
      end
    end
  end
  object mdTemp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'Coleta'
        DataType = ftInteger
      end
      item
        Name = 'Inserida'
        DataType = ftInteger
      end
      item
        Name = 'reg'
        DataType = ftInteger
      end
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end>
    AfterOpen = mdTempAfterOpen
    Left = 20
    Top = 120
    object mdTempColeta: TIntegerField
      FieldName = 'Coleta'
    end
    object mdTempInserida: TIntegerField
      FieldName = 'Inserida'
    end
    object mdTempreg: TIntegerField
      FieldName = 'reg'
    end
    object mdTempnr_romaneio: TIntegerField
      FieldName = 'nr_romaneio'
    end
    object mdTempdestino: TStringField
      FieldName = 'destino'
      Size = 30
    end
    object mdTempdata: TDateField
      FieldName = 'data'
      DisplayFormat = 'DD/MM/YY'
    end
    object mdTempfilial: TIntegerField
      FieldName = 'filial'
    end
  end
  object dsTemp: TDataSource
    AutoEdit = False
    DataSet = mdTemp
    Left = 68
    Top = 121
  end
  object iml: TImageList
    Left = 12
    Top = 344
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object QVeiculo: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct placa, descri, kg, cub, sitras, codclifor from ('
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras,codclifor'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      ''
      
        'where (propri <> '#39'S'#39' and tipvin <> '#39'A'#39' and tipvei <> 7 and situa' +
        'c=1)'
      'group by numvei, f.descri, sitras,codclifor'
      ''
      'union all'
      ''
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras, codclifor'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      ''
      'where v.datant >= trunc(sysdate)'
      'and v.vendua >= trunc(sysdate)'
      'and propri = '#39'N'#39
      'and tipvin = '#39'A'#39' and tipvei <> 7'
      'group by numvei, f.descri, sitras, codclifor)'
      'where codclifor = :0'
      'order by 1,2'
      '')
    Left = 120
    Top = 460
    object QVeiculoDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object QVeiculoKG: TBCDField
      FieldName = 'KG'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
  end
  object dtsVeiculo: TDataSource
    DataSet = QVeiculo
    Left = 167
    Top = 460
  end
  object dstTarefa: TDataSource
    AutoEdit = False
    DataSet = QTarefa
    OnDataChange = dstTarefaDataChange
    Left = 64
    Top = 232
  end
  object QTarefa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct o.codocs nro_ocs, o.filocs filial, o.datinc data' +
        'inc, rm.nomeab rem, de.nomeab des, cl.nomeab cli, mu.estado uf, ' +
        'nvl(sum(i.pesokg),0) peso '
      
        'from cyber.rodocs o left join cyber.rodcli rm on rm.codclifor = ' +
        '(case when o.tercol > 0 then o.tercol else o.codrem end)'
      
        '                    left join cyber.rodmun mu on mu.codmun = rm.' +
        'codmun'
      
        '                    left join cyber.rodioc i on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs                    '
      
        '                    left join cyber.rodcli de on de.codclifor = ' +
        'nvl(o.terent, i.coddes)'
      
        '                    left join cyber.rodcli cl on cl.codclifor = ' +
        'o.codpag'
      
        'where o.codrmc is null and i.notfis is not null and o.codcon is ' +
        'null and o.tipocs = '#39'C'#39
      'and o.codocs in (221799)'
      'and o.filocs  = :0'
      
        'group by o.codocs, o.filocs, o.datinc, rm.nomeab, de.nomeab, cl.' +
        'nomeab, mu.estado'
      'order by 3')
    Left = 16
    Top = 232
    object QTarefaNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      ReadOnly = True
      Precision = 32
    end
    object QTarefaFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
    end
    object QTarefaDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QTarefaREM: TStringField
      FieldName = 'REM'
      ReadOnly = True
    end
    object QTarefaDES: TStringField
      FieldName = 'DES'
      ReadOnly = True
    end
    object QTarefaCLI: TStringField
      FieldName = 'CLI'
      ReadOnly = True
    end
    object QTarefaUF: TStringField
      FieldName = 'UF'
      ReadOnly = True
      Size = 2
    end
    object QTarefaPESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      DisplayFormat = '###,##0.000'
      Precision = 32
    end
  end
  object QOCS: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QOCSBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'select distinct o.codocs nro_ocs, o.filocs filial, o.datinc data' +
        'inc, cl.nomeab, o.tipocs'
      
        'from cyber.rodocs o left join cyber.rodcli cl on cl.codclifor = ' +
        'o.codrem'
      
        '                    left join cyber.rodioc i on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs'
      
        'right join cyber.tb_coleta c on c.nro_ocs = o.codocs and c.filia' +
        'l = o.filocs'
      
        'where o.codrmc is null and i.notfis is not null and o.codcon is ' +
        'null and o.tipocs = '#39'C'#39' and c.nr_oc is null'
      'and o.filocs = :0'
      
        'and o.datinc > to_date('#39'30/09/2014 23:59:00'#39','#39'dd/mm/yyyy hh24:mi' +
        ':ss'#39')'
      'order by 3')
    Left = 20
    Top = 64
    object QOCSNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      Precision = 32
    end
    object QOCSFILIAL: TBCDField
      FieldName = 'FILIAL'
      Precision = 32
    end
    object QOCSDATAINC: TDateTimeField
      FieldName = 'DATAINC'
    end
    object QOCSNOMEAB: TStringField
      FieldName = 'NOMEAB'
      ReadOnly = True
    end
  end
  object qrCalcula_Frete: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'tarefa'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'emp'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select nvl(sum(i.pescub),0) cub, nvl(sum(i.pesokg),0) peso, nvl(' +
        'sum(i.quanti),0) qtd, nvl(sum(i.vlrmer),0) valor'
      
        'from cyber.rodioc i left join cyber.rodocs o on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs'
      'where i.codocs = :tarefa'
      'and i.filocs = :emp'
      '')
    Left = 20
    Top = 172
    object qrCalcula_FreteCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FretePESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FreteQTD: TBCDField
      FieldName = 'QTD'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FreteVALOR: TBCDField
      FieldName = 'VALOR'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsTrans: TDataSource
    DataSet = QTransp
    Left = 167
    Top = 396
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct CODIGO, NOME from ('
      'select dataf, CODIGO, NOME from ('
      
        'select distinct t.cod_custo, max(i.dataf) dataf, cod_fornecedor ' +
        'CODIGO, fma.razsoc NOME'
      
        'from tb_custofrac_item i left join tb_custofrac t on i.cod_custo' +
        ' = t.cod_custo'
      
        '                         left join cyber.rodcli fma on t.cod_for' +
        'necedor = fma.codclifor'
      
        '                         left join tb_operacao o on t.operacao =' +
        ' o.operacao'
      
        'where t.fl_status = '#39'S'#39' and fma.razsoc is not null and fma.situa' +
        'c = '#39'A'#39
      'and o.fl_oc = '#39'S'#39' and o.fl_receita = '#39'N'#39
      ''
      'group by t.cod_custo, t.operacao, cod_fornecedor, fma.razsoc)'
      'where trunc(dataf) >= trunc(sysdate)'
      ')'
      'order by 2')
    Left = 120
    Top = 396
    object QTranspNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QTranspCODIGO: TBCDField
      FieldName = 'CODIGO'
      Precision = 32
      Size = 0
    end
  end
  object SPOrdem: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_GRAVA_ORDEM'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VOPERACAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTRANSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VMOTOR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VROMA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCUSTO'
        Attributes = [paNullable]
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'VPLACAC'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = ''
      end>
    Left = 12
    Top = 292
  end
  object QOCi: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QOCiBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select codocs, notfis, pesokg, pescub, pescal, vlrmer, quanti, c' +
        'odrmc, filocs from ('
      
        'select i.codocs, i.notfis, i.pesokg, i.pescub, i.pescal, i.vlrme' +
        'r, i.quanti, o.codrmc, i.filocs'
      
        'from cyber.rodioc i left join cyber.rodocs o on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs'
      'union all'
      
        'select i.codocs, i.notfis, i.pesokg, i.pescub, i.pescal, i.vlrme' +
        'r, i.quanti, o.nr_oc codrmc, i.filocs'
      
        'from cyber.rodioc i left join cyber.tb_coleta o on i.codocs = o.' +
        'nro_ocs and i.filocs = o.filial)'
      'where codrmc = :0'
      'and filocs = :1')
    Left = 12
    Top = 520
    object QOCiCODOCS: TBCDField
      FieldName = 'CODOCS'
      Precision = 32
    end
    object QOCiNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QOCiPESOKG: TBCDField
      FieldName = 'PESOKG'
      DisplayFormat = '###,##0.000'
      Precision = 14
    end
    object QOCiPESCUB: TBCDField
      FieldName = 'PESCUB'
      DisplayFormat = '###,##0.000'
      Precision = 16
      Size = 6
    end
    object QOCiPESCAL: TBCDField
      FieldName = 'PESCAL'
      DisplayFormat = '###,##0.000'
      Precision = 14
    end
    object QOCiVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
    object QOCiQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
  end
  object dtsOCi: TDataSource
    AutoEdit = False
    DataSet = QOCi
    Left = 59
    Top = 520
  end
  object QCusto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'PLACA'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'operacao'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'forne'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filial'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'ocs'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select nvl(fnc_custo_oc(:placa, :operacao, :forne, :filial, :ocs' +
        '),0) custo'
      'from dual')
    Left = 273
    Top = 60
    object QCustoCUSTO: TBCDField
      FieldName = 'CUSTO'
      ReadOnly = True
      Precision = 32
    end
  end
  object QOCa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QOCaBeforeOpen
    AfterScroll = QOCaAfterScroll
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, nvl(sum(i.pesokg),' +
        '0) peso,'
      'fo.razsoc, oc.placa, c.remetente, o.codcon, oc.operacao'
      
        'from cyber.tb_coleta c left join cyber.rodocs o on c.nro_ocs = o' +
        '.codocs and c.filial = o.filocs'
      
        '                       left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join cyber.rodioc i on i.codocs = o.' +
        'codocs and i.filocs = o.filocs'
      
        '                       left join tb_ordem oc on oc.id_ordem = o.' +
        'codrmc'
      
        '                       left join cyber.rodcli fo on fo.codclifor' +
        ' = oc.codtransp'
      
        'where o.codrmc is not null and o.tipocs = '#39'C'#39' and oc.fl_status i' +
        's null'
      'and oc.dt_receb is null'
      'and c.filial = :0'
      
        'group by oc.id_ordem, oc.filial, oc.datinc, rm.nomeab, de.nomeab' +
        ', cl.nomeab, mu.estado,fo.razsoc, oc.placa, c.remetente, o.codco' +
        'n , oc.operacao'
      'union all'
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, nvl(sum(i.pesokg),' +
        '0) peso,'
      'fo.razsoc, oc.placa, c.remetente, c.doc codcon, oc.operacao'
      
        'from cyber.tb_coleta c left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join cyber.rodioc i on i.codocs = c.' +
        'nro_ocs and i.filocs = c.filial'
      
        '                       right join tb_ordem oc on oc.id_ordem = c' +
        '.nr_oc'
      
        '                       left join cyber.rodcli fo on fo.codclifor' +
        ' = oc.codtransp'
      'where c.nr_oc is not null '
      'and oc.dt_receb is null and oc.fl_status is null'
      'and c.filial = :1'
      
        'group by oc.id_ordem, oc.filial, oc.datinc, rm.nomeab, de.nomeab' +
        ', cl.nomeab, mu.estado,fo.razsoc, oc.placa, c.remetente, c.doc, ' +
        'oc.operacao'
      ''
      'order by 3')
    Left = 12
    Top = 396
    object QOCaID_ORDEM: TBCDField
      FieldName = 'ID_ORDEM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QOCaFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QOCaDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QOCaREM: TStringField
      FieldName = 'REM'
      ReadOnly = True
    end
    object QOCaDES: TStringField
      FieldName = 'DES'
      ReadOnly = True
    end
    object QOCaCLI: TStringField
      FieldName = 'CLI'
      ReadOnly = True
    end
    object QOCaUF: TStringField
      FieldName = 'UF'
      ReadOnly = True
      Size = 2
    end
    object QOCaPESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 32
    end
    object QOCaRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object QOCaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QOCaREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object QOCaCODCON: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QOCaOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
  end
  object dtsOCa: TDataSource
    AutoEdit = False
    DataSet = QOCa
    Left = 60
    Top = 396
  end
  object QOCr: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QOCrBeforeOpen
    AfterScroll = QOCrAfterScroll
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct id_ordem, filial, datinc, rem, des, cli, uf, tra' +
        'nsp, descri, usuinc, conferido, dt_receb, placa, remetente, codo' +
        'cs, operacao'
      'from ('
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, f.razsoc transp,fr' +
        '.descri, oc.usuinc, oc.conferido, oc.dt_receb, oc.placa, c.remet' +
        'ente, o.codocs, oc.operacao'
      
        'from cyber.tb_coleta c left join cyber.rodocs o on c.nro_ocs = o' +
        '.codocs and c.filial = o.filocs'
      
        '                       left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join tb_ordem oc on oc.id_ordem = o.' +
        'codrmc'
      
        '                       left join cyber.rodcli f on oc.codtransp ' +
        '= f.codclifor'
      
        '                       left join cyber.rodvei v on oc.placa = v.' +
        'numvei'
      
        '                       left join cyber.rodfro fr on fr.codfro = ' +
        'v.codfro'
      
        'where o.codrmc is not null  and o.tipocs = '#39'C'#39' and oc.fl_status ' +
        'is null'
      'and oc.dt_receb is not null'
      'union all'
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, f.razsoc transp,fr' +
        '.descri, oc.usuinc, oc.conferido, oc.dt_receb, oc.placa, c.remet' +
        'ente, c.nro_ocs codocs, oc.operacao'
      
        'from cyber.tb_coleta c left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join tb_ordem oc on oc.id_ordem = c.' +
        'nr_oc'
      
        '                       left join cyber.rodcli f on oc.codtransp ' +
        '= f.codclifor'
      
        '                       left join cyber.rodvei v on oc.placa = v.' +
        'numvei'
      
        '                       left join cyber.rodfro fr on fr.codfro = ' +
        'v.codfro'
      'where c.nr_oc is not null and oc.fl_status is null'
      'and oc.dt_receb is not null)'
      'where filial = :0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by 3')
    Left = 16
    Top = 460
    object QOCrID_ORDEM: TBCDField
      FieldName = 'ID_ORDEM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QOCrFILIAL: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QOCrDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QOCrREM: TStringField
      FieldName = 'REM'
      ReadOnly = True
    end
    object QOCrDES: TStringField
      FieldName = 'DES'
      ReadOnly = True
    end
    object QOCrCLI: TStringField
      FieldName = 'CLI'
      ReadOnly = True
    end
    object QOCrUF: TStringField
      FieldName = 'UF'
      ReadOnly = True
      Size = 2
    end
    object QOCrTRANSP: TStringField
      FieldName = 'TRANSP'
      ReadOnly = True
      Size = 80
    end
    object QOCrUSUINC: TStringField
      FieldName = 'USUINC'
      ReadOnly = True
    end
    object QOCrDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object QOCrCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      ReadOnly = True
      Size = 30
    end
    object QOCrDT_RECEB: TDateTimeField
      FieldName = 'DT_RECEB'
      ReadOnly = True
    end
    object QOCrPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QOCrREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object QOCrCODOCS: TFMTBCDField
      FieldName = 'CODOCS'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QOCrOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
  end
  object dtsOCr: TDataSource
    AutoEdit = False
    DataSet = QOCr
    OnDataChange = dstTarefaDataChange
    Left = 60
    Top = 460
  end
  object dtsIt: TDataSource
    AutoEdit = False
    DataSet = QIt
    Left = 167
    Top = 524
  end
  object QIt: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 23
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select codocs, notfis, pesokg, pescub, pescal, vlrmer, quanti, c' +
        'odrmc, filocs from ('
      
        'select i.codocs, i.notfis, i.pesokg, i.pescub, i.pescal, i.vlrme' +
        'r, i.quanti, o.codrmc, i.filocs'
      
        'from cyber.rodioc i left join cyber.rodocs o on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs'
      'union all'
      
        'select i.codocs, i.notfis, i.pesokg, i.pescub, i.pescal, i.vlrme' +
        'r, i.quanti, o.nr_oc codrmc, i.filocs'
      
        'from cyber.rodioc i left join cyber.tb_coleta o on i.codocs = o.' +
        'nro_ocs and i.filocs = o.filial)'
      'where codrmc = :0'
      'and filocs = :1')
    Left = 120
    Top = 524
    object QITCODOCS: TBCDField
      FieldName = 'CODOCS'
      Precision = 32
    end
    object QITNOTFIS: TStringField
      FieldName = 'NOTFIS'
    end
    object QITPESOKG: TBCDField
      FieldName = 'PESOKG'
      DisplayFormat = '###,##0.000'
      Precision = 14
    end
    object QITPESCUB: TBCDField
      FieldName = 'PESCUB'
      DisplayFormat = '###,##0.000'
      Precision = 16
      Size = 6
    end
    object QITPESCAL: TBCDField
      FieldName = 'PESCAL'
      DisplayFormat = '###,##0.000'
      Precision = 14
    end
    object QITVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
    object QItQUANTI: TBCDField
      FieldName = 'QUANTI'
      Precision = 32
    end
  end
  object QOc: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QOcBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 8
      end>
    SQL.Strings = (
      
        'select distinct id_ordem, filial, datinc, rem, des, cli, uf, tra' +
        'nsp, descri, usuinc, conferido, dt_receb, placa, remetente, codo' +
        'cs, operacao'
      'from ('
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, f.razsoc transp,fr' +
        '.descri, oc.usuinc, oc.conferido, oc.dt_receb, oc.placa, c.remet' +
        'ente, c.nro_ocs codocs, operacao'
      
        'from cyber.tb_coleta c left join cyber.rodocs o on c.nro_ocs = o' +
        '.codocs and c.filial = o.filocs'
      
        '                       left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join tb_ordem oc on oc.id_ordem = o.' +
        'codrmc'
      
        '                       left join cyber.rodcli f on oc.codtransp ' +
        '= f.codclifor'
      
        '                       left join cyber.rodvei v on oc.placa = v.' +
        'numvei'
      
        '                       left join cyber.rodfro fr on fr.codfro = ' +
        'v.codfro'
      'where o.codrmc is not null  and o.tipocs = '#39'C'#39
      'and oc.dt_receb is null'
      'union all'
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, mu.estado uf, f.razsoc transp,fr' +
        '.descri, oc.usuinc, oc.conferido, oc.dt_receb, oc.placa, c.remet' +
        'ente, c.nro_ocs codocs, operacao'
      
        'from cyber.tb_coleta c left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join tb_ordem oc on oc.id_ordem = c.' +
        'nr_oc'
      
        '                       left join cyber.rodcli f on oc.codtransp ' +
        '= f.codclifor'
      
        '                       left join cyber.rodvei v on oc.placa = v.' +
        'numvei'
      
        '                       left join cyber.rodfro fr on fr.codfro = ' +
        'v.codfro'
      'where c.nr_oc is not null'
      'and oc.dt_receb is null)'
      'where filial = :0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by 3')
    Left = 232
    Top = 396
    object BCDField1: TBCDField
      FieldName = 'ID_ORDEM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object BCDField2: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YYYY'
    end
    object StringField1: TStringField
      FieldName = 'REM'
      ReadOnly = True
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'DES'
      ReadOnly = True
    end
    object StringField3: TStringField
      FieldName = 'CLI'
      ReadOnly = True
    end
    object StringField4: TStringField
      FieldName = 'UF'
      ReadOnly = True
      Size = 2
    end
    object StringField5: TStringField
      FieldName = 'TRANSP'
      ReadOnly = True
      Size = 80
    end
    object StringField6: TStringField
      FieldName = 'USUINC'
      ReadOnly = True
    end
    object StringField7: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object StringField8: TStringField
      FieldName = 'CONFERIDO'
      ReadOnly = True
      Size = 30
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'DT_RECEB'
      ReadOnly = True
    end
    object StringField9: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object BCDField3: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object QOcCODOCS: TFMTBCDField
      FieldName = 'CODOCS'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QOcOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
  end
  object dtsOC: TDataSource
    AutoEdit = False
    DataSet = QOc
    OnDataChange = dstTarefaDataChange
    Left = 276
    Top = 396
  end
  object QM: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 14
        Value = 292541
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select c.sequencia, c.pagador, c.remetente, c.nro_ocs, c.user_ct' +
        'e, c.nr_oc, o.operacao'
      
        'from cyber.tb_coleta c left join tb_ordem o on c.nr_oc = o.id_or' +
        'dem'
      
        '                       left join tb_controle_custo cc on cc.nr_d' +
        'oc = o.id_ordem and cc.filial = c.filial'
      'where c.doc = :0  and c.filial = :1'
      'and cc.status is null'
      'and cc.tipo = '#39'Romaneio'#39)
    Left = 936
    Top = 65534
    object QOCSEQUENCIA: TBCDField
      FieldName = 'SEQUENCIA'
      Precision = 32
    end
    object QOCPAGADOR: TBCDField
      FieldName = 'PAGADOR'
      Precision = 32
    end
    object QOCREMETENTE: TBCDField
      FieldName = 'REMETENTE'
      Precision = 32
    end
    object QOCNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
      Precision = 32
    end
    object QOCUSER_CTE: TStringField
      FieldName = 'USER_CTE'
      Size = 30
    end
    object QOCNR_OC: TBCDField
      FieldName = 'NR_OC'
      Precision = 32
      Size = 0
    end
    object QMOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 30
    end
  end
  object dtsQM: TDataSource
    DataSet = QM
    Left = 968
    Top = 65534
  end
  object dtsOCC: TDataSource
    DataSet = QOCC
    Left = 980
    Top = 146
  end
  object QOCC: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    BeforeOpen = QOCCBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, oc.usuatu, oc.datatu,'
      'fo.razsoc, oc.placa, c.remetente, o.codcon, oc.motivo, c.nro_ocs'
      
        'from cyber.tb_coleta c left join cyber.rodocs o on c.nro_ocs = o' +
        '.codocs and c.filial = o.filocs'
      
        '                       left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join cyber.rodioc i on i.codocs = o.' +
        'codocs and i.filocs = o.filocs'
      
        '                       left join tb_ordem oc on oc.id_ordem = o.' +
        'codrmc'
      
        '                       left join cyber.rodcli fo on fo.codclifor' +
        ' = oc.codtransp'
      
        'where o.codrmc is not null and o.tipocs = '#39'C'#39' and oc.fl_status =' +
        ' '#39'C'#39
      'and c.filial = :0'
      
        'group by oc.id_ordem, oc.filial, oc.datinc, rm.nomeab, de.nomeab' +
        ', cl.nomeab, oc.usuatu, oc.datatu, fo.razsoc, oc.placa, c.remete' +
        'nte, o.codcon, oc.motivo, c.nro_ocs'
      'union all'
      
        'select distinct oc.id_ordem, oc.filial, oc.datinc, rm.nomeab rem' +
        ', de.nomeab des, cl.nomeab cli, oc.usuatu, oc.datatu,'
      
        'fo.razsoc, oc.placa, c.remetente, c.doc codcon, oc.motivo, c.nro' +
        '_ocs'
      
        'from cyber.tb_coleta c left join cyber.rodcli rm on rm.codclifor' +
        ' = c.remetente'
      
        '                       left join cyber.rodmun mu on mu.codmun = ' +
        'rm.codmun'
      
        '                       left join cyber.rodcli de on de.codclifor' +
        ' = nvl(c.redespachante, c.destinatario)'
      
        '                       left join cyber.rodcli cl on cl.codclifor' +
        ' = c.pagador'
      
        '                       left join cyber.rodioc i on i.codocs = c.' +
        'nro_ocs and i.filocs = c.filial'
      
        '                       left join tb_ordem oc on oc.id_ordem = c.' +
        'nr_oc'
      
        '                       left join cyber.rodcli fo on fo.codclifor' +
        ' = oc.codtransp'
      'where oc.fl_status = '#39'C'#39
      'and c.filial = :1'
      
        'group by oc.id_ordem, oc.filial, oc.datinc, rm.nomeab, de.nomeab' +
        ', cl.nomeab, oc.usuatu, oc.datatu,fo.razsoc, oc.placa, c.remeten' +
        'te, c.doc, oc.motivo, c.nro_ocs'
      ''
      'order by datatu desc'
      '')
    Left = 936
    Top = 144
    object BCDField4: TBCDField
      FieldName = 'ID_ORDEM'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object BCDField5: TBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
      DisplayFormat = 'DD/MM/YYYY'
    end
    object StringField10: TStringField
      FieldName = 'REM'
      ReadOnly = True
    end
    object StringField11: TStringField
      FieldName = 'DES'
      ReadOnly = True
    end
    object StringField12: TStringField
      FieldName = 'CLI'
      ReadOnly = True
    end
    object StringField14: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object StringField15: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object BCDField7: TBCDField
      FieldName = 'REMETENTE'
      ReadOnly = True
      Precision = 32
    end
    object BCDField8: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QOCCdatatu: TDateField
      FieldName = 'datatu'
    end
    object QOCCusuatu: TStringField
      FieldName = 'usuatu'
      Size = 30
    end
    object QOCCNRO_OCS: TBCDField
      FieldName = 'NRO_OCS'
    end
  end
  object QKM: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QKMBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct lpad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)' +
        '),8,0) codcep, m.descri, m.estado, (d.latitu ||'#39'+'#39'|| d.longit) g' +
        'eo, d.codclifor'
      'from cyber.tb_coleta i '
      'left join cyber.rodcli d on d.codclifor = i.remetente'
      'left join cyber.rodmun m on m.codmun = d.codmun'
      'where i.nro_ocs in (520892) '
      'and filial = :0'
      'order by 1')
    Left = 896
    Top = 468
    object QKMDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
    object QKMCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object QKMGEO: TStringField
      FieldName = 'GEO'
      ReadOnly = True
      Size = 81
    end
    object QKMCODCEP: TStringField
      FieldName = 'CODCEP'
      ReadOnly = True
      Size = 8
    end
  end
  object XMLDoc: TXMLDocument
    Left = 936
    Top = 468
    DOMVendorDesc = 'MSXML'
  end
  object QVeiculoCasa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras,codpro'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      'where v.datant >= trunc(sysdate)'
      'and (propri = '#39'S'#39' )'
      'group by numvei, f.descri, sitras,codpro'
      'order by 1,2')
    Left = 228
    Top = 460
    object QVeiculoCasaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QVeiculoCasaDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object QVeiculoCasaKG: TBCDField
      FieldName = 'KG'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCasaCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCasaSITRAS: TStringField
      FieldName = 'SITRAS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QVeiculoCasaCODPRO: TBCDField
      FieldName = 'CODPRO'
      ReadOnly = True
      Precision = 32
    end
  end
  object QPGR_M: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct sum(i.vlrmer) vlrmer, pgr.id_regra, o.codpag cli' +
        'ente, cl.razsoc'
      
        'from cyber.rodocs o left join cyber.rodcli rm on rm.codclifor = ' +
        '(case when o.tercol > 0 then o.tercol else o.codrem end)'
      
        '                    left join cyber.rodioc i on i.codocs = o.cod' +
        'ocs and i.filocs = o.filocs'
      
        '                    left join cyber.rodcli cl on cl.codclifor = ' +
        'o.codpag'
      
        '                    left join tb_regra_pgr pgr on o.codpag = pgr' +
        '.cod_cliente and pgr.destino = (case when rm.codcmo = 12 then '#39'M' +
        'ONTADORA'#39' else '#39'DIVERSOS'#39' end)'
      
        'where o.codrmc is null and i.notfis is not null and o.codcon is ' +
        'null and o.tipocs = '#39'C'#39
      'and o.codocs in (607982) and o.filocs = :0'
      'group by o.codpag, pgr.id_regra, cl.razsoc'
      'order by o.codpag')
    Left = 436
    Top = 112
    object QPGR_MVLRMER: TFMTBCDField
      FieldName = 'VLRMER'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_MID_REGRA: TFMTBCDField
      FieldName = 'ID_REGRA'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QPGR_MCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_MRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
  end
  object SP_PGR: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_PGR_M'
    Parameters = <
      item
        Name = 'R_RETORNO'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 300
        Value = #201' necess'#225'ria a An'#225'lise de Risco !'
      end
      item
        Name = 'R_ERRO'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 1
        Value = Null
      end
      item
        Name = 'VREGRA'
        Attributes = [paNullable]
        DataType = ftFloat
        Value = 1.000000000000000000
      end
      item
        Name = 'VISCA1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = ' '
      end
      item
        Name = 'VISCA2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = ' '
      end
      item
        Name = 'VTRANSP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 10728.000000000000000000
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        NumericScale = 2
        Precision = 15
        Value = 21000.000000000000000000
      end>
    Left = 500
    Top = 112
  end
  object QIsca: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select nm_isca from tb_iscas where nm_isca is not null order by ' +
        '1')
    Left = 436
    Top = 204
    object QIscaNM_ISCA: TStringField
      FieldName = 'NM_ISCA'
      Size = 10
    end
  end
end
