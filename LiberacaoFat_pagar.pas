unit LiberacaoFat_pagar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, jpeg, JvMaskEdit,
  JvMemo, JvEdit, JvRichEdit, JvDBRichEdit;

type
  TfrmLiberacaoFat_pagar = class(TForm)
    QFatura: TADOQuery;
    navnavig: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    Label1: TLabel;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    btnExcel: TBitBtn;
    QClienteNR_CNPJ_CPF: TStringField;
    QClienteNM_FORNECEDOR: TStringField;
    QClienteCOD_FORNECEDOR: TBCDField;
    QFaturaFL_FATURA_PGTO: TStringField;
    QFaturaNM_FORNECEDOR: TStringField;
    QFaturaTOTAL: TBCDField;
    Label2: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    DBNavigator1: TDBNavigator;
    QFaturaUSUARIO: TStringField;
    QFaturaDATAALT: TStringField;
    edCtrc: TJvEdit;
    SP_Exporta: TADOStoredProc;
    btnLiberar: TBitBtn;
    QFaturaDESCONTO: TBCDField;
    QFaturaLIQUIDO: TBCDField;
    JvDBRichEdit1: TJvDBRichEdit;
    QFaturaOBS: TStringField;
    QFaturaTRANSP: TBCDField;
    QFaturaMARGEM: TBCDField;
    QFaturaRECEITA: TBCDField;
    QFaturavcto: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLiberarClick(Sender: TObject);
    procedure QFaturaCalcFields(DataSet: TDataSet);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmLiberacaoFat_pagar: TfrmLiberacaoFat_pagar;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmLiberacaoFat_pagar.btnExcelClick(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
  wb: Variant;
begin
  memo1 := TStringList.Create;
  texto := 'Relat�rio de Faturamento - A Pagar';
  memo1.Clear();
  memo1.Add('<HTML><HEAD><TITLE>Relat�rio de Faturamento</TITLE>');
  memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add('</STYLE>');
  memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo1.Add('<table border=0><title></title></head><body>');
  memo1.Add('<table border=1 align=center>');
  memo1.Add('<tr>');
  memo1.Add('<th colspan=2></th></font>');
  memo1.Add('</tr><tr>');
  memo1.Add(
    '<TABLE borderColor=#ebebeb cellSpacing=0 cellPadding=2 width="100%" border=1>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr bgColor=#ebebeb>');

  memo1.Add('<TD align=middle width=50><FONT class=texto2>Fatura</FONT></TD>');
  memo1.Add(
    '<TD align=middle width=500><FONT class=texto2>Transportador</FONT></TD>');
  memo1.Add('<TD align=middle width=150><FONT class=texto2>Valor</FONT></TD>');
  memo1.Add('</TR>');

  QFatura.First;
  Gauge1.MaxValue := QFatura.RecordCount;
  while not QFatura.Eof do
  begin
    memo1.Add('<TR>');
    memo1.Add('<TD align=middle width=50><FONT class=texto2>' +
      QFaturaFL_FATURA_PGTO.AsString + '</FONT></TD>');
    memo1.Add('<TD align=left width=500><FONT class=texto2>' +
      QFaturaNM_FORNECEDOR.value + '</FONT></TD>');
    memo1.Add('<TD align=right width=150><FONT class=texto2>' + format('%10.2n',
      [QFaturaTOTAL.value]) + '</FONT></TD>');
    QFatura.Next;
    Gauge1.AddProgress(1);
  end;
  memo1.Add('<tr bgColor=#ebebeb>');
  memo1.Add('<tr>');

  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('rel_fat_pagar.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := true;
  wb.Navigate('rel_fat_pagar.html');
end;

procedure TfrmLiberacaoFat_pagar.btnLiberarClick(Sender: TObject);
var
  sql: String;
begin
  if Application.Messagebox('Liberar para o Financeiro esta Fatura ?',
    'Liberar Fatura', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    SP_Exporta.Parameters[0].value := QFaturaFL_FATURA_PGTO.AsString;
    SP_Exporta.Parameters[1].value := QFaturaTRANSP.AsInteger;
    SP_Exporta.Parameters[2].value := QFaturavcto.value;
    SP_Exporta.Parameters[3].value := GLBUSER;
    SP_Exporta.ExecProc;

    btRelatorioClick(Sender);

  end;
end;

procedure TfrmLiberacaoFat_pagar.btRelatorioClick(Sender: TObject);
begin
  Panel1.Visible := true;
  Application.ProcessMessages;
  QFatura.close;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QFatura.sql[8] := ' and dataalt between to_date(''' + dtInicial.Text +
      ' 00:00:00'',''dd/mm/yy hh24:mi:ss'') and to_date(''' + dtFinal.Text +
      ' 23:59:59'',''dd/mm/yy hh24:mi:ss'')';
  if edCtrc.Text <> '' then
    QFatura.sql[9] := ' and c.fatura = ' + #39 + edCtrc.Text + #39;
  if Trim(ledIdCliente.LookupValue) <> '' then
    QFatura.sql[10] := ' and c.transp = ''' + ledIdCliente.LookupValue + '''';
  // Qfatura.SQL.savetofile('c:\sim\sql.txt');
  QFatura.open;
  { edCusto.Value := 0;
    while not QFatura.eof do
    begin
    edCusto.Value := edCusto.Value + QFaturaTOTAL.Value;
    QFatura.next;
    end;
    QFatura.first; }
  Panel1.Visible := false;
end;

procedure TfrmLiberacaoFat_pagar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QFatura.close;
  QCliente.close;
end;

procedure TfrmLiberacaoFat_pagar.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmLiberacaoFat_pagar.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QFatura.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmLiberacaoFat_pagar.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmLiberacaoFat_pagar.QFaturaCalcFields(DataSet: TDataSet);
var
  vcto: TdateTime;
  codtar: integer;
  pfj: String;
begin
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.sql.Add
    ('select * from (select codtar, adidia from cyber.rodtar_cfr where codclifor = :0 ');
  dtmdados.RQuery1.sql.Add('order by 2 desc) where rownum = 1 ');
  dtmdados.RQuery1.Parameters[0].value := QFaturaTRANSP.AsInteger;
  dtmdados.RQuery1.open;
  vcto := dtmdados.RQuery1.FieldByName('adidia').value + date;

  codtar := dtmdados.RQuery1.FieldByName('codtar').value;
  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.sql.Add('select * from cyber.tarrat where codtar = :0');
  dtmdados.RQuery1.Parameters[0].value := codtar;
  dtmdados.RQuery1.open;
  if dtmdados.RQuery1.Eof then
  begin
    ShowMessage('Fornecedor sem Classifica��o !!');
    Exit;
  end;

  dtmdados.RQuery1.close;
  dtmdados.RQuery1.sql.Clear;
  dtmdados.RQuery1.sql.Add
    ('select fisjur from cyber.rodcli where codclifor = :0');
  dtmdados.RQuery1.Parameters[0].value := QFaturaTRANSP.AsInteger;
  dtmdados.RQuery1.open;
  pfj := dtmdados.RQuery1.FieldByName('fisjur').value;
  dtmdados.RQuery1.close;

  if DayOfWeek(vcto) = 1 then
    vcto := vcto + 1;

  if DayOfWeek(vcto) = 7 then
    vcto := vcto + 2;

  QFaturavcto.value := vcto;

end;

end.
