unit Importa_Meritor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, Buttons, ComCtrls,
  FileCtrl, Gauges, ADODB, JvMemoryDataset, ExtCtrls, DBCtrls, Mask, JvExMask,
  JvToolEdit, JvBaseEdits, JvMaskEdit, FMTBcd, SqlExpr, DBClient, Provider,
  JvDBGridExport, JvComponentBase, JvgExportComponents, xmldom, XMLIntf,
  msxmldom, XMLDoc, ComObj, IniFiles, ShellAPI;

type
  TfrmImporta_Meritor = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    FLB: TFileListBox;
    btnImportar: TBitBtn;
    btnSair: TBitBtn;
    DriveComboBox1: TDriveComboBox;
    PB1: TGauge;
    QImp: TADOQuery;
    Label5: TLabel;
    QCliente: TADOQuery;
    Query: TADOQuery;
    QueryTEM: TBCDField;
    Label1: TLabel;
    QClienteID_EMPFAENGER: TStringField;
    QExporta: TADOQuery;
    QExportaPEDIDO_WMS: TStringField;
    QExportaORDER_: TStringField;
    QExportaITEM: TStringField;
    QExportaPRODUTO: TStringField;
    QExportaQT: TBCDField;
    QExportaDELIVERY: TStringField;
    QExportaSTAT: TStringField;
    QExportaDESTINO: TStringField;
    QExportaFANTASIA: TStringField;
    QExportaID_DETAIL: TStringField;
    QExportaNF: TStringField;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    QExportaDATA: TDateTimeField;
    cbLimpar: TCheckBox;
    procedure btnSairClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FLBClick(Sender: TObject);
  private
    function BuscaTrocaValor(sText: string): string;
    function VerificaFormato(sText: string): string;
  public
    { Public declarations }
  end;

var
  frmImporta_Meritor: TfrmImporta_Meritor;

implementation

uses Dados, funcoes, menu;

{$R *.dfm}

procedure TfrmImporta_Meritor.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImporta_Meritor.FLBClick(Sender: TObject);
begin
  Label5.caption := FLB.FileName;
end;

procedure TfrmImporta_Meritor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QImp.Close;
end;

procedure TfrmImporta_Meritor.btnImportarClick(Sender: TObject);
var
  nm, ped: string;
  i, id, x, z: integer;
  xlsObj: OleVariant;
  wb: variant;
  u, t, tra, des, cad: string;
begin
  btnSair.Enabled := false;
  btnImportar.Enabled := false;
  x := 0;
  xlsObj := CreateOleObject('Excel.Application');
  wb := xlsObj.Workbooks.open(Label5.caption);
  i := 2; // linha que come�a na planilha
  nm := xlsObj.Workbooks[1].Sheets[1].Cells[i, 1];
  while trim(nm) <> '' do
  begin
    x := x + 1;
    nm := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]);
    inc(i);
  end;
  PB1.MaxValue := x;
  PB1.Progress := 0;
  x := 0;
  z := 0;
  ped := '';
  i := 2; // linha que come�a na planilha
  nm := xlsObj.Workbooks[1].Sheets[1].Cells[i, 1];
  if cbLimpar.Checked = true then
  begin
    QImp.Close;
    QImp.sql.Clear;
    QImp.sql.add
      ('delete from tb_pedido_item where id_pedido in (select id from tb_pedido_meritor where ped_wms is null)');
    QImp.ExecSQL;
    QImp.Close;
    QImp.sql.Clear;
    QImp.sql.add('delete from tb_pedido_meritor where ped_wms is null');
    QImp.ExecSQL;
    QImp.Close;
  end;

  while trim(nm) <> '' do
  begin
    x := x + 1;
    PB1.AddProgress(1);
    Application.ProcessMessages;
    nm := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]);
    if trim(nm) = '' then
    begin
      Showmessage('Processo terminado!!!');
      xlsObj.quit;
      RenameFile(Label5.caption, Label5.caption + '.OK');
      FLB.Update;
      btnSair.Enabled := true;
      btnImportar.Enabled := false;
      exit;
    end;

    Query.Close;
    Query.Parameters[0].value :=
      alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 12]);
    // Query.Parameters[1].value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,5]);
    Query.open;

    des := copy(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 2]), 1, 30);
    QCliente.Close;
    QCliente.Parameters[0].value := des;
    QCliente.open;
    if QCliente.Eof then
    begin
      Showmessage('Destinat�rio : ' + des +
        ' N�o Cadastrado no WMS - Importa��o n�o executada');
      cad := '';
    end
    else
      cad := 'OK';

    if (QueryTEM.value = 0) and (cad = 'OK') then
    begin
      z := z + 1;
      Label1.caption := 'Itens Processados = ' + inttostr(z);
      if ped <> alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 7]) then
      // alterado de pediod para delivery
      begin
        tra := copy(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 8]), 1,
          Pos('-', alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 8])) - 1);

        QImp.Close;
        QImp.sql.Clear;
        QImp.sql.add
          ('insert into tb_pedido_meritor (id, pedido, coddes, usuario, ');
        QImp.sql.add
          ('tipo, retira, transp, delivery, prio, regra) values (seq_pedido_meritor.nextval, :0, ');
        QImp.sql.add(':1, :2, :3, :4, :5, :6, :7, :8)');
        QImp.Parameters[0].value :=
          alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 1]);
        QImp.Parameters[1].value := QClienteID_EMPFAENGER.AsString;
        QImp.Parameters[2].value := glbuser;
        QImp.Parameters[3].value :=
          alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 9]);
        if apcarac(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 10])) = ''
        then
          QImp.Parameters[4].value := ''
        else
          QImp.Parameters[4].value := 'X';
        QImp.Parameters[5].value := copy(tra, 1, 20);
        QImp.Parameters[6].value :=
          alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 7]);
        if apcarac(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 14])) = 'Dealer'
        then
          QImp.Parameters[7].value := 'X'
        else
          QImp.Parameters[7].value := '';
        QImp.Parameters[8].value :=
          UpperCase(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 15]));
        QImp.ExecSQL;
        QImp.Close;
        QImp.sql.Clear;
        QImp.sql.add('select max(id) id from tb_pedido_meritor');
        QImp.open;
        id := QImp.FieldByName('id').value;
        QImp.Close;
        ped := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 7]);
      end;
      if VerificaFormato(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 3])) = 'USA'
      then
      begin
        u := StrFloatUSA(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 3]));
        t := StrFloatUSA(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 4]));
      end
      else
      begin
        u := (alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 3]));
        t := (alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 4]));
      end;
      QImp.Close;
      QImp.sql.Clear;
      QImp.sql.add
        ('insert into tb_pedido_item (id, id_pedido, codpro, quant, ');
      QImp.sql.add
        ('valor, total, id_detail) values (seq_pedido_item.nextval, :0, ');
      QImp.sql.add(':1, :2, :3, :4, :5)');
      QImp.Parameters[0].value := id;
      QImp.Parameters[1].value :=
        (alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 5]));
      QImp.Parameters[2].value :=
        StrToInt(alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 6]));
      QImp.Parameters[3].value := StrToFloat(BuscaTrocaValor(u));
      QImp.Parameters[4].value := StrToFloat(BuscaTrocaValor(t));
      QImp.Parameters[5].value :=
        (alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i, 12]));
      u := '';
      t := '';
      // QImp.SQL.savetofile('c:\iw\sql.txt');
      QImp.ExecSQL;
      QImp.Close;
    end;
    inc(i);
  end;
end;

function TfrmImporta_Meritor.BuscaTrocaValor(sText: string): string;
begin
  sText := BuscaTroca(sText, '.', '');
  if copy(copy(sText, length(sText) - 1, 2), 1, 1) = '.' then
    sText := sText + '0';
  Result := sText;
end;

function TfrmImporta_Meritor.VerificaFormato(sText: string): string;
begin
  if copy(sText, length(sText) - 2, 1) = '.' then
    Result := 'USA'
  else
    Result := 'BRA';
end;

end.
