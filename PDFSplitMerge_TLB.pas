unit PDFSplitMerge_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2007/5/24 8:24:45 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Dogfood\PDF Split Merge Component\PDFSplitMerge.dll (1)
// LIBID: {18441CDF-26A6-424C-9FCB-3435B5FA8C37}
// LCID: 0
// Helpfile: 
// HelpString: PDFSplitMerge 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (D:\Windows\system32\stdole2.tlb)
// Errors:
//   Error creating palette bitmap of (TCPDFSplitMergeObj) : Server D:\Dogfood\PDF Split Merge Component\PDFSplitMerge.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PDFSplitMergeMajorVersion = 1;
  PDFSplitMergeMinorVersion = 0;

  LIBID_PDFSplitMerge: TGUID = '{18441CDF-26A6-424C-9FCB-3435B5FA8C37}';

  IID_IPDFSplitMerge: TGUID = '{49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}';
  CLASS_CPDFSplitMergeObj: TGUID = '{50D41702-B618-41BA-8153-7AD7E8535574}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IPDFSplitMerge = interface;
  IPDFSplitMergeDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CPDFSplitMergeObj = IPDFSplitMerge;


// *********************************************************************//
// Interface: IPDFSplitMerge
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}
// *********************************************************************//
  IPDFSplitMerge = interface(IDispatch)
    ['{49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}']
    procedure Split(const strInPdf: WideString; const strSplit: WideString; 
                    const strOutPdf: WideString); safecall;
    procedure Merge(const strInFiles: WideString; const strOutFile: WideString); safecall;
    procedure SetCode(const strCode: WideString); safecall;
    function GetNumberOfPages(const strFile: WideString; const strPasswd: WideString): Integer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IPDFSplitMergeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}
// *********************************************************************//
  IPDFSplitMergeDisp = dispinterface
    ['{49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}']
    procedure Split(const strInPdf: WideString; const strSplit: WideString; 
                    const strOutPdf: WideString); dispid 1;
    procedure Merge(const strInFiles: WideString; const strOutFile: WideString); dispid 2;
    procedure SetCode(const strCode: WideString); dispid 19;
    function GetNumberOfPages(const strFile: WideString; const strPasswd: WideString): Integer; dispid 20;
  end;

// *********************************************************************//
// The Class CoCPDFSplitMergeObj provides a Create and CreateRemote method to          
// create instances of the default interface IPDFSplitMerge exposed by              
// the CoClass CPDFSplitMergeObj. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCPDFSplitMergeObj = class
    class function Create: IPDFSplitMerge;
    class function CreateRemote(const MachineName: string): IPDFSplitMerge;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCPDFSplitMergeObj
// Help String      : PDFSplitMerge Class
// Default Interface: IPDFSplitMerge
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TCPDFSplitMergeObjProperties= class;
{$ENDIF}
  TCPDFSplitMergeObj = class(TOleServer)
  private
    FIntf:        IPDFSplitMerge;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TCPDFSplitMergeObjProperties;
    function      GetServerProperties: TCPDFSplitMergeObjProperties;
{$ENDIF}
    function      GetDefaultInterface: IPDFSplitMerge;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IPDFSplitMerge);
    procedure Disconnect; override;
    procedure Split(const strInPdf: WideString; const strSplit: WideString; 
                    const strOutPdf: WideString);
    procedure Merge(const strInFiles: WideString; const strOutFile: WideString);
    procedure SetCode(const strCode: WideString);
    function GetNumberOfPages(const strFile: WideString; const strPasswd: WideString): Integer;
    property DefaultInterface: IPDFSplitMerge read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TCPDFSplitMergeObjProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TCPDFSplitMergeObj
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TCPDFSplitMergeObjProperties = class(TPersistent)
  private
    FServer:    TCPDFSplitMergeObj;
    function    GetDefaultInterface: IPDFSplitMerge;
    constructor Create(AServer: TCPDFSplitMergeObj);
  protected
  public
    property DefaultInterface: IPDFSplitMerge read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoCPDFSplitMergeObj.Create: IPDFSplitMerge;
begin
  Result := CreateComObject(CLASS_CPDFSplitMergeObj) as IPDFSplitMerge;
end;

class function CoCPDFSplitMergeObj.CreateRemote(const MachineName: string): IPDFSplitMerge;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CPDFSplitMergeObj) as IPDFSplitMerge;
end;

procedure TCPDFSplitMergeObj.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{50D41702-B618-41BA-8153-7AD7E8535574}';
    IntfIID:   '{49E9FF30-6AB9-4B64-A345-CA4B8F3F255F}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCPDFSplitMergeObj.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IPDFSplitMerge;
  end;
end;

procedure TCPDFSplitMergeObj.ConnectTo(svrIntf: IPDFSplitMerge);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCPDFSplitMergeObj.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCPDFSplitMergeObj.GetDefaultInterface: IPDFSplitMerge;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TCPDFSplitMergeObj.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TCPDFSplitMergeObjProperties.Create(Self);
{$ENDIF}
end;

destructor TCPDFSplitMergeObj.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TCPDFSplitMergeObj.GetServerProperties: TCPDFSplitMergeObjProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TCPDFSplitMergeObj.Split(const strInPdf: WideString; const strSplit: WideString; 
                                   const strOutPdf: WideString);
begin
  DefaultInterface.Split(strInPdf, strSplit, strOutPdf);
end;

procedure TCPDFSplitMergeObj.Merge(const strInFiles: WideString; const strOutFile: WideString);
begin
  DefaultInterface.Merge(strInFiles, strOutFile);
end;

procedure TCPDFSplitMergeObj.SetCode(const strCode: WideString);
begin
  DefaultInterface.SetCode(strCode);
end;

function TCPDFSplitMergeObj.GetNumberOfPages(const strFile: WideString; const strPasswd: WideString): Integer;
begin
  Result := DefaultInterface.GetNumberOfPages(strFile, strPasswd);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TCPDFSplitMergeObjProperties.Create(AServer: TCPDFSplitMergeObj);
begin
  inherited Create;
  FServer := AServer;
end;

function TCPDFSplitMergeObjProperties.GetDefaultInterface: IPDFSplitMerge;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TCPDFSplitMergeObj]);
end;

end.
