unit ConsFat_pagar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj,
  jpeg, JvMaskEdit, JvMemo, JvEdit, RLReport, RLRichText, RLParser, ACBrBase,
  ACBrMail, Vcl.ComCtrls;

type
  TfrmConsFat_pagar = class(TForm)
    QFatura: TADOQuery;
    dtsFatura: TDataSource;
    Label14: TLabel;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    Label1: TLabel;
    JvDBGrid1: TJvDBGrid;
    Gauge1: TGauge;
    Panel1: TPanel;
    QCliente: TADOQuery;
    dtsCliente: TDataSource;
    btnImprimir: TBitBtn;
    QRelatorio: TADOQuery;
    btnExcluir: TBitBtn;
    btnExcel: TBitBtn;
    QClienteNR_CNPJ_CPF: TStringField;
    QClienteNM_FORNECEDOR: TStringField;
    QClienteCOD_FORNECEDOR: TBCDField;
    QFaturaFL_FATURA_PGTO: TStringField;
    QFaturaNM_FORNECEDOR: TStringField;
    QFaturaTOTAL: TBCDField;
    QFaturaCOD_TRANSP: TBCDField;
    Label2: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    DBNavigator1: TDBNavigator;
    QRelatorioTIPO: TStringField;
    QRelatorioDATA: TDateTimeField;
    QRelatorioDOC: TBCDField;
    QRelatorioCODCON: TBCDField;
    QRelatorioTOTFRE: TBCDField;
    QRelatorioCTE_CUSTO: TBCDField;
    QRelatorioVALOR_COBRADO: TBCDField;
    QRelatorioCUSTO_CALC: TBCDField;
    QRelatorioNM_FORNECEDOR: TStringField;
    QRelatorioMARGEM: TBCDField;
    QRelatorioFATURA: TStringField;
    QFaturaUSUARIO: TStringField;
    QResumo: TADOQuery;
    QResumoRECEITA: TBCDField;
    QResumoCUSTO: TBCDField;
    QResumoMARGEM: TBCDField;
    Panel5: TPanel;
    Label6: TLabel;
    btnConf: TBitBtn;
    Obs: TJvMemo;
    SPExcluir: TADOStoredProc;
    QAdiant: TADOQuery;
    QAdiantCODFIC: TBCDField;
    QAdiantOBSERV: TStringField;
    QAdiantVLRADI: TBCDField;
    QRelatorioFILIAL: TBCDField;
    QRelatorioTRANSP: TBCDField;
    QDesconto: TADOQuery;
    QDescontoDESCONTO: TBCDField;
    QDescontoOBS: TStringField;
    QRelatorioCNPJ: TStringField;
    edCusto: TJvCalcEdit;
    QFaturaDATAALT: TStringField;
    QDescontoVL_IMPOSTOS: TBCDField;
    Button1: TButton;
    ADOQuery1: TADOQuery;
    ADOQuery1XML_CUSTO: TStringField;
    ADOQuery1FATURA: TStringField;
    edCtrc: TJvEdit;
    QDescontoPROCESSO: TBCDField;
    QRelatorioDT_FATURA: TDateTimeField;
    RGTipo: TRadioGroup;
    QFaturaCTE_REC: TBCDField;
    dtsRelatorio: TDataSource;
    dtsResumo: TDataSource;
    dtsDesconto: TDataSource;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLIMIW: TRLImage;
    RlImInt: TRLImage;
    RLLabel1: TRLLabel;
    RLDraw1: TRLDraw;
    RLLabel10: TRLLabel;
    RLLabel18: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel26: TRLLabel;
    RLLabel27: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel3: TRLLabel;
    RLBand3: TRLBand;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLTTPagarDes: TRLLabel;
    RLLabel17: TRLLabel;
    RLDBText1: TRLDBText;
    RLDBText3: TRLDBText;
    RLTTAdv: TRLLabel;
    RLDBResult1: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RlObs: TRLRichText;
    RLGroup1: TRLGroup;
    RLBand5: TRLBand;
    RLDBText11: TRLDBText;
    RLDBText12: TRLDBText;
    RLDBText13: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText6: TRLDBText;
    RLBand2: TRLBand;
    RlCodfic: TRLLabel;
    RlAd: TRLLabel;
    RlCodcon: TRLLabel;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBResult5: TRLDBResult;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLBand4: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLExpressionParser1: TRLExpressionParser;
    QRelatorioCUSTO: TFMTBCDField;
    RLDBResult6: TRLDBResult;
    QRelatorioCODFIL: TFMTBCDField;
    bEnviar: TButton;
    ACBrMail1: TACBrMail;
    mLog: TJvMemo;
    mensagem: TJvMemo;
    QFaturaDT_VENCIMENTO: TDateTimeField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure QRelatorioBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure QRelatorioAfterScroll(DataSet: TDataSet);
    procedure btnConfClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLGroup1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure bEnviarClick(Sender: TObject);
    procedure ACBrMail1AfterMailProcess(Sender: TObject);
    procedure ACBrMail1BeforeMailProcess(Sender: TObject);
    procedure ACBrMail1MailException(const AMail: TACBrMail; const E: Exception; var ThrowIt: Boolean);
    procedure ACBrMail1MailProcess(const AMail: TACBrMail; const aStatus: TMailStatus);
    procedure mLogClick(Sender: TObject);
  private
    { Private declarations }
  var
    man: integer;
    vladi: real;
  public
    { Public declarations }
  end;

var
  frmConsFat_pagar: TfrmConsFat_pagar;

implementation

uses Dados, Menu, funcoes, impressao_custo_frete;

{$R *.dfm}

procedure TfrmConsFat_pagar.bEnviarClick(Sender: TObject);
var
  Dir, ArqXML, v_email, texto: string;
  MS: TMemoryStream;
  P, N, i: Integer;
begin
  mLog.Visible := true;
  mLog.Lines.Clear;
  mensagem.Lines.Clear;

  //dtmDados.IQuery1.close;
  //dtmDados.IQuery1.sql.Clear;
  //dtmDados.IQuery1.sql.Add('select distinct email from tb_cadmensageiro where servico = ''JOB - SP_ENVIO_FATURA'' ');
  //dtmDados.IQuery1.open;
  //v_email := dtmDados.IQuery1.FieldByName('email').AsString;
  v_email := 'paulo.cortez@intecomlogistica.com.br';

 { dtmDados.IQuery1.close;
  dtmDados.IQuery1.sql.Clear;
  dtmDados.IQuery1.sql.Add('select email from cyber.RODCTC e ');
  dtmDados.IQuery1.sql.Add('where e.CODCLIFOR = :0 and e.situac = ''A'' ');
  dtmDados.IQuery1.Parameters[0].Value := QFaturaCOD_TRANSP.AsInteger;
  dtmDados.IQuery1.open;
  i := 0;
  while not dtmDados.IQuery1.eof do
  begin  }
    ACBrMail1.Clear;
    ACBrMail1.IsHTML := true;
    ACBrMail1.Subject := 'Programa��o de Pagamento';

    ACBrMail1.From := 'mensageiro@intecomlogistica.com.br';
    ACBrMail1.FromName := 'Controladoria de Frete';
    ACBrMail1.Host := 'smtp.office365.com';
    ACBrMail1.Username := 'mensageiro@intecomlogistica.com.br';
    ACBrMail1.Password := 'M3n$@geir0=123';
    ACBrMail1.Port := '587';
  //  texto :=  dtmDados.IQuery1.FieldByName('email').AsString;
    ACBrMail1.AddAddress(v_email);
  //  ACBrMail1.AddCC('tiago.silva@intecomlogistica.com.br');
  {  if i = 0 then
    begin
      ACBrMail1.AddCC(v_email);
      i := i + 1;
    end;
    dtmDados.IQuery1.next;
  end;   }

  mensagem.Lines.add('<html xmlns=http://www.w3.org/1999/xhtml><head>');
  mensagem.Lines.add('<meta http-equiv=Content-Type content=text/html; charset=iso-8859-1><title></title></head><body><table border=1><tr>');
  mensagem.Lines.add('<th><FONT class=titulo1>FATURA : ' + QFaturaFL_FATURA_PGTO.AsString + ' </font></th></tr><tr>');
  mensagem.Lines.add('<th>TRANSPORTADORA : <FONT class=texto1>'+QFaturaNM_FORNECEDOR.AsString+'</font></th></tr><tr>');
  mensagem.Lines.add('<th>Data Emiss&atilde;o : <FONT class=texto1>'+ QFaturaDATAALT.AsString + '</font></th></tr><tr>');
  mensagem.Lines.add('<th>Valor : <FONT class=texto1>' + format('%10.2n',[QFaturaTOTAL.Value]) +'</font></th></tr><tr>');
  mensagem.Lines.add('<th>Programada para Pagamento na Data de : <FONT class=texto1>'+ QFaturaDT_VENCIMENTO.AsString +'</font></th></tr>');
  mensagem.Lines.add('</table></body></html>');

  ACBrMail1.Body.Assign(mensagem.Lines);
  ACBrMail1.Send(false);

end;

procedure TfrmConsFat_pagar.ACBrMail1BeforeMailProcess(Sender: TObject);
begin
  mLog.Lines.Add('Antes de Enviar o email: ' + TACBrMail(Sender).Subject);
end;

procedure TfrmConsFat_pagar.ACBrMail1MailException(const AMail: TACBrMail; const E: Exception; var ThrowIt: Boolean);
begin
  ShowMessage(E.Message);
  ThrowIt := False;
  mLog.Lines.Add('*** Erro ao Enviar o email: ' + AMail.Subject);
end;

procedure TfrmConsFat_pagar.ACBrMail1MailProcess(const AMail: TACBrMail; const aStatus: TMailStatus);
begin

  case aStatus of
    pmsStartProcess:
      mLog.Lines.Add('Iniciando processo de envio.');
    pmsConfigHeaders:
      mLog.Lines.Add('Configurando o cabe�alho do e-mail.');
    pmsLoginSMTP:
      mLog.Lines.Add('Logando no servidor de e-mail.');
    pmsStartSends:
      mLog.Lines.Add('Iniciando os envios.');
    pmsSendTo:
      mLog.Lines.Add('Processando lista de destinat�rios.');
    pmsSendCC:
      mLog.Lines.Add('Processando lista CC.');
    pmsSendBCC:
      mLog.Lines.Add('Processando lista BCC.');
    pmsSendReplyTo:
      mLog.Lines.Add('Processando lista ReplyTo.');
    pmsSendData:
      mLog.Lines.Add('Enviando dados.');
    pmsLogoutSMTP:
      mLog.Lines.Add('Fazendo Logout no servidor de e-mail.');
    pmsDone:
      begin
        mLog.Lines.Add('Terminando e limpando.');
      end;
  end;

  mLog.Lines.Add('   ' + AMail.Subject);

  Application.ProcessMessages;
end;

procedure TfrmConsFat_pagar.ACBrMail1AfterMailProcess(Sender: TObject);
begin
  mLog.Lines.Add('Depois de Enviar o email: ' + TACBrMail(Sender).Subject);
end;

procedure TfrmConsFat_pagar.btnConfClick(Sender: TObject);
begin
  if Length(Obs.Text) < 10 then
  begin
    ShowMessage('Favor descrever o porque da Exclus�o !!');
    Obs.SetFocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Apagar Esta Fatura ?', 'Apagar Fatura',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    SPExcluir.Parameters[0].Value := QFaturaFL_FATURA_PGTO.AsString;
    SPExcluir.Parameters[1].Value := QFaturaCOD_TRANSP.AsInteger;
    SPExcluir.Parameters[2].Value := Obs.Text;
    SPExcluir.Parameters[3].Value := GLBUSER;
    SPExcluir.ExecProc;
    Panel5.Visible := false;
    ShowMessage('Fatura Excluida !!');
    QFatura.close;
    QFatura.open;
  end;
end;

procedure TfrmConsFat_pagar.btnExcelClick(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
  wb: Variant;
begin
  memo1 := TStringList.Create;
  texto := 'Relat�rio de Faturamento - A Pagar';
  memo1.Clear();
  memo1.Add('<HTML><HEAD><TITLE>Relat�rio de Faturamento</TITLE>');
  memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add('</STYLE>');
  memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo1.Add('<table border=0><title></title></head><body>');
  memo1.Add('<table border=1 align=center>');
  memo1.Add('<tr>');
  memo1.Add('<th colspan=2></th></font>');
  memo1.Add('</tr><tr>');
  memo1.Add(
    '<TABLE borderColor=#ebebeb cellSpacing=0 cellPadding=2 width="100%" border=1>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr bgColor=#ebebeb>');

  memo1.Add('<TD align=middle width=50><FONT class=texto2>Fatura</FONT></TD>');
  memo1.Add(
    '<TD align=middle width=500><FONT class=texto2>Transportador</FONT></TD>');
  memo1.Add('<TD align=middle width=150><FONT class=texto2>Valor</FONT></TD>');
  memo1.Add('</TR>');

  QFatura.First;
  Gauge1.MaxValue := QFatura.RecordCount;
  while not QFatura.Eof do
  begin
    memo1.Add('<TR>');
    memo1.Add('<TD align=middle width=50><FONT class=texto2>' +
      QFaturaFL_FATURA_PGTO.AsString + '</FONT></TD>');
    memo1.Add('<TD align=left width=500><FONT class=texto2>' +
      QFaturaNM_FORNECEDOR.AsString + '</FONT></TD>');
    memo1.Add('<TD align=right width=150><FONT class=texto2>' + format('%10.2n',
      [QFaturaTOTAL.Value]) + '</FONT></TD>');
    QFatura.Next;
    Gauge1.AddProgress(1);
  end;
  memo1.Add('<tr bgColor=#ebebeb>');
  memo1.Add('<tr>');

  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('rel_fat_pagar.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := true;
  wb.Navigate('rel_fat_pagar.html');
end;

procedure TfrmConsFat_pagar.btnExcluirClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    exit;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add
    ('select * from cyber.pagdoc where numdoc = :0 and codclifor = :1 ');
  dtmDados.RQuery1.Parameters[0].Value := QFaturaFL_FATURA_PGTO.Value;
  dtmDados.RQuery1.Parameters[1].Value := QFaturaCOD_TRANSP.AsInteger;
  dtmDados.RQuery1.open;
  if not dtmDados.RQuery1.Eof then
  begin
    ShowMessage('Esta Fatura ainda n�o foi cancelada no Financeiro');
    exit;
  end;

  Panel5.Visible := true;

end;

procedure TfrmConsFat_pagar.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm(TfrmImpressao_custofrete, frmImpressao_custofrete);
  frmImpressao_custofrete.QRelatorio.close;
  frmImpressao_custofrete.QRelatorio.Parameters[0].Value := QFaturaFL_FATURA_PGTO.AsString;
  frmImpressao_custofrete.QRelatorio.Parameters[1].Value := QFaturaCOD_TRANSP.AsInteger;
  frmImpressao_custofrete.QRelatorio.open;
  frmImpressao_custofrete.RLReport1.Preview;
  {QRelatorio.close;
  QRelatorio.Parameters[0].Value := QFaturaFL_FATURA_PGTO.AsString;
  QRelatorio.Parameters[1].Value := QFaturaCOD_TRANSP.AsInteger;
  QRelatorio.open;
  man := 0;
  RLReport1.Preview;  }
end;

procedure TfrmConsFat_pagar.btRelatorioClick(Sender: TObject);
begin
  Panel1.Visible := true;
  Application.ProcessMessages;
  QFatura.close;
  if RGTipo.ItemIndex = 0 then
  begin
    QFatura.sql[1] :=
      'select distinct c.fatura fl_fatura_pgto, c.codclifor cod_transp, f.razsoc nm_fornecedor, ';
    QFatura.sql[2] :=
      'sum(c.custo_calc)-(nvl(c.desconto,0)+nvl(c.desconto2,0)) total, c.usuario, to_char(c.dataalt,''dd/mm/rrrr'') dataalt,  ';
    QFatura.sql[3] :=
      'c.dt_vcto dt_vencimento, 0 cte_rec  ';
    QFatura.sql[6] :=
      'group by fatura, c.codclifor, f.razsoc, c.usuario, to_char(c.dataalt,''dd/mm/rrrr''), cte_rec, c.dt_vcto, c.desconto,c.desconto2 ';
    QFatura.sql[12] :=
      'select distinct g.fatura fl_fatura_pgto, g.transp cod_transp, f.razsoc nm_fornecedor, (g.vlr_total_c+g.vl_imposto_nfs) total, g.user_erp usuario, to_char(g.dt_erp,''dd/mm/rrrr'') dataalt, nvl(g.nf_servico, g.cte_gen) cte_rec, g.dt_vencimento ';
    QFatura.sql[15] :=
      'group by fl_fatura_pgto, cod_transp, nm_fornecedor, usuario, dataalt, cte_rec, dt_vencimento ';
    JvDBGrid1.Columns[5].Visible := true;
  end
  else
  begin
    QFatura.sql[1] :=
      'select distinct c.fatura fl_fatura_pgto, c.codclifor cod_transp, f.razsoc nm_fornecedor, ';
    QFatura.sql[2] :=
      '(sum(c.custo_calc)-(nvl(c.desconto,0)+nvl(c.desconto2,0)) - fa.descob) total, ';
    QFatura.sql[3] :=
      'c.usuario, to_char(c.dataalt,''dd/mm/rrrr'') dataalt, c.dt_vcto dt_vencimento, 0 cte_rec ';
    QFatura.sql[6] :=
      'group by fatura, c.codclifor, f.razsoc, c.usuario, to_char(c.dataalt,''dd/mm/rrrr''), c.dt_vcto, c.desconto,c.desconto2, fa.descob ';
    QFatura.sql[12] :=
      'select distinct g.fatura fl_fatura_pgto, g.transp cod_transp, f.razsoc nm_fornecedor, g.vlr_total_c total, nvl(g.user_fatura,g.user_erp) usuario, to_char(nvl(g.dataalt, g.dt_erp),''dd/mm/rrrr'') dataalt, g.dt_vencimento, 0 cte_rec';
    QFatura.sql[15] :=
      'group by fl_fatura_pgto, cod_transp, nm_fornecedor, usuario, dataalt, dt_vencimento ';
    JvDBGrid1.Columns[5].Visible := false;
  end;

  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QFatura.sql[19] := ' and dataalt between to_date(''' + dtInicial.Text +
      ' 00:00:00'',''dd/mm/yy hh24:mi:ss'') and to_date(''' + dtFinal.Text +
      ' 23:59:59'',''dd/mm/yy hh24:mi:ss'')';
  if edCtrc.Text <> '' then
    QFatura.sql[20] := ' and fl_fatura_pgto = ' + #39 + edCtrc.Text + #39;
  if Trim(ledIdCliente.LookupValue) <> '' then
    QFatura.sql[21] := ' and cod_transp = ''' + ledIdCliente.LookupValue + '''';
  //SHOWMESSAGE(Qfatura.SQL.Text);
  QFatura.open;
  edCusto.Value := 0;
  while not QFatura.Eof do
  begin
    edCusto.Value := edCusto.Value + QFaturaTOTAL.Value;
    QFatura.Next;
  end;
  QFatura.First;
  Panel1.Visible := false;
  btnImprimir.Enabled := true;
end;

procedure TfrmConsFat_pagar.Button1Click(Sender: TObject);
var
  caminho, origem, destino: String;
begin
  ADOQuery1.open;
  while not ADOQuery1.Eof do
  begin
    // pegar os xmls e gravar na pasta
    if not ADOQuery1XML_CUSTO.IsNull then
    begin
      caminho := 'C:\SIM\fatura\' + ADOQuery1FATURA.AsString;
      if not DirectoryExists(caminho) then
        CreateDir(caminho);

      origem := copy(ADOQuery1XML_CUSTO.AsString, 1, 86);
      destino := caminho + '\' + copy(ADOQuery1XML_CUSTO.AsString, 31, 56);
      CopyFile(PChar(origem), PChar(destino), true);
    end;
    ADOQuery1.Next;
  end;
  ADOQuery1.close;
end;

procedure TfrmConsFat_pagar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QFatura.close;
  QCliente.close;
  QRelatorio.close;
end;

procedure TfrmConsFat_pagar.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmConsFat_pagar.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QFatura.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmConsFat_pagar.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmConsFat_pagar.mLogClick(Sender: TObject);
begin
  mLog.Visible := false;
end;

procedure TfrmConsFat_pagar.QRelatorioAfterScroll(DataSet: TDataSet);
begin
  RlCodcon.Caption := QRelatorioCODCON.AsString;
end;

procedure TfrmConsFat_pagar.QRelatorioBeforeOpen(DataSet: TDataSet);
begin
  QRelatorio.Parameters[0].Value := QFaturaFL_FATURA_PGTO.Value;
  QRelatorio.Parameters[1].Value := QFaturaCOD_TRANSP.Value;
end;

procedure TfrmConsFat_pagar.RLBand2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if (man <> QRelatorioDOC.AsInteger) then
  begin
    QAdiant.close;
    QAdiant.Parameters[0].Value := QRelatorioDOC.AsInteger;
    QAdiant.Parameters[1].Value := QRelatorioFILIAL.AsInteger; // CODFIL.AsInteger;
    QAdiant.Parameters[2].Value := QRelatorioTRANSP.AsInteger;
    QAdiant.open;
    if not QAdiant.Eof then
    begin
      RlCodfic.Caption := 'REPOM  ' + QAdiantCODFIC.AsString;
      RlAd.Caption := FormatFloat('#,##0.00', QAdiantVLRADI.Value);
      vladi := vladi + QAdiantVLRADI.Value;
    end
    else
    begin
      RlCodfic.Caption := '';
      RlAd.Caption := '';
    end;
    man := QRelatorioDOC.AsInteger;
  end
  else
  begin
    RlCodfic.Caption := '';
    RlAd.Caption := '';
  end;
end;

procedure TfrmConsFat_pagar.RLBand3BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  ttpg: real;
begin
  RlObs.Lines.Clear;
  ttpg := RLDBResult6.Value - vladi;
  RLTTAdv.Caption := FormatFloat('#,##0.00', vladi);
  //RLTTPagar.Caption := FormatFloat('#,##0.00', ttpg);
  RLTTPagarDes.Caption := FormatFloat('#,##0.00', ttpg - QDescontoDESCONTO.Value
    - QDescontoPROCESSO.Value);
  RlObs.Lines.Add(QDescontoOBS.AsString);
end;

procedure TfrmConsFat_pagar.RLGroup1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  QResumo.close;
  QResumo.Parameters[0].Value := QRelatorioFATURA.Value;
  QResumo.Parameters[1].Value := QRelatorioDOC.AsInteger;
  QResumo.Parameters[2].Value := QRelatorioTRANSP.AsInteger;
  QResumo.open;

  QDesconto.close;
  QDesconto.Parameters[0].Value := QRelatorioFATURA.Value;
  QDesconto.Parameters[1].Value := QRelatorioTRANSP.AsInteger;
  QDesconto.Parameters[2].Value := QRelatorioFATURA.Value;
  QDesconto.Parameters[3].Value := QRelatorioTRANSP.AsInteger;
  QDesconto.open;
end;

procedure TfrmConsFat_pagar.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLTTPagarDes.Caption := '';

  RLLabel1.Caption := 'Transportador : ' + QRelatorioNM_FORNECEDOR.Value + ' - '
    + QRelatorioCNPJ.AsString;
  RLLabel2.Caption := 'Relat�rio de Fornecedor - : ' +
    QRelatorioDT_FATURA.AsString;

  dtmDados.RQuery1.close;
  dtmDados.RQuery1.sql.Clear;
  dtmDados.RQuery1.sql.Add
    ('select substr(razsoc,1,instr(razsoc,'' '')-1) filial from rodfil ');
  dtmDados.RQuery1.sql.Add('where codfil = ' + QRelatorioFILIAL.AsString);
  dtmDados.RQuery1.open;
  if dtmDados.RQuery1.FieldByName('filial').Value = 'IW' then
  begin
    RLIMIW.Enabled := true;
    RlImInt.Enabled := false;
  end
  else
  begin
    RLIMIW.Enabled := false;
    RlImInt.Enabled := true;
  end;
  dtmDados.RQuery1.close;
  man := 0;
  vladi := 0;
  RlLabel3.Caption := 'Fatura : ' + QRelatorioFATURA.AsString;
end;

end.
