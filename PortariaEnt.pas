unit PortariaEnt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, Buttons, JvGradient, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExControls, JvEnterTab, ImgList,
  JvMaskEdit, DBCtrls, JvExStdCtrls, JvCombobox, System.ImageList,
  JvComponentBase, RLReport;

type
  TfrmPortariaEnt = class(TForm)
    Panel3: TPanel;
    JvGradient1: TJvGradient;
    btnGerar: TBitBtn;
    Panel2: TPanel;
    JvEnterAsTab1: TJvEnterAsTab;
    QEntrada: TADOQuery;
    iml: TImageList;
    Label11: TLabel;
    Label4: TLabel;
    QTransp: TADOQuery;
    lblqt: TLabel;
    Label2: TLabel;
    edPlaca: TJvMaskEdit;
    Label6: TLabel;
    edTransp: TJvMaskEdit;
    edMotor: TJvMaskEdit;
    Label8: TLabel;
    Label9: TLabel;
    edCfp: TJvMaskEdit;
    edRG: TJvMaskEdit;
    edCNH: TJvMaskEdit;
    Label10: TLabel;
    Label13: TLabel;
    cbTipo: TComboBox;
    QEntradaREG: TBCDField;
    QEntradaTIPO: TBCDField;
    QEntradaFL_EMPRESA: TBCDField;
    QEntradaPLACA: TStringField;
    QEntradaCOD_TRANSP: TBCDField;
    QEntradaCOD_MOTORISTA: TBCDField;
    QEntradaDATA: TDateTimeField;
    QEntradaKM: TBCDField;
    QEntradaMANIFESTO: TBCDField;
    QEntradaOBS: TStringField;
    QEntradaUSUARIO: TBCDField;
    SPEntrada: TADOStoredProc;
    cbTransp: TComboBox;
    QTranspNM_FORNECEDOR: TStringField;
    Memo1: TMemo;
    lblKm: TLabel;
    Label1: TLabel;
    edManifesto: TJvCalcEdit;
    edserie: TJvMaskEdit;
    Label3: TLabel;
    edKm: TJvCalcEdit;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel15: TRLLabel;
    RLSystemInfo3: TRLSystemInfo;
    RLLabel5: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel10: TRLLabel;
    RLMemo2: TRLMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGerarClick(Sender: TObject);
    procedure cbTipoExit(Sender: TObject);
    procedure edPlacaExit(Sender: TObject);
    procedure QTranspBeforeOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private

  public
    { Public declarations }
  end;

var
  frmPortariaEnt: TfrmPortariaEnt;

implementation

uses Dados, menu, funcoes;

{$R *.dfm}

procedure TfrmPortariaEnt.FormActivate(Sender: TObject);
begin
  Panel3.setfocus;
end;

procedure TfrmPortariaEnt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  frmPortariaEnt := nil;
  QEntrada.close;
  QTransp.close;
end;

procedure TfrmPortariaEnt.QTranspBeforeOpen(DataSet: TDataSet);
begin
  // QTransp.Parameters[0].value := glbFilial;
end;

procedure TfrmPortariaEnt.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel1.Caption := 'INTECOM';
  if (cbTransp.text = '') then
    RLLabel2.Caption := 'Transportadora : ' + edTransp.Text
  else
    RLLabel2.Caption := 'Transportadora : ' + cbTransp.text;
  RLLabel3.caption := GlbUser;
  RLLabel12.Caption := 'Placa : ' + edPlaca.Text;
  RLLabel22.Caption := 'Motorista : ' + edMotor.text;
  RLLabel5.Caption := 'KM : ' + edKM.Text;
  RLMemo2.Lines.add(Memo1.Text);
end;

procedure TfrmPortariaEnt.btnGerarClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if edPlaca.text = '' then
  begin
    ShowMessage('N�o foi Digitado a Placa !!');
    edPlaca.setfocus;
    Exit;
  end;

  if edMotor.text = '' then
  begin
    ShowMessage('Informe o nome do motorista');
    edMotor.setfocus;
    Exit;
  end;

  if (cbTransp.text = '') and (cbTransp.Visible = true) then
  begin
    ShowMessage('Informe a Transportadora');
    cbTransp.setfocus;
    Exit;
  end;

  if (edTransp.text = '') and (cbTransp.Visible = FALSE) then
  begin
    ShowMessage('Informe a Transportadora');
    edTransp.setfocus;
    Exit;
  end;
  //
  if (edManifesto.value > 0) and (edserie.text = '') then
  begin
    ShowMessage('Informe a S�rie do Manifesto');
    edserie.setfocus;
    Exit;
  end;
  btnGerar.Enabled := FALSE;
  if Application.Messagebox('Voc� Deseja Gerar Esta Entrada ?', 'Gerar Entrada',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    SPEntrada.Parameters[0].value := edManifesto.value;
    SPEntrada.Parameters[1].value := cbTipo.text;
    if (cbTransp.text = '') then
      SPEntrada.Parameters[2].value := edTransp.text
    else
      SPEntrada.Parameters[2].value := cbTransp.text;
    SPEntrada.Parameters[3].value := edMotor.text;
    SPEntrada.Parameters[4].value := GLBFilial;
    SPEntrada.Parameters[5].value := edKm.Value;
    SPEntrada.Parameters[6].value := Memo1.text;
    SPEntrada.Parameters[7].value := GlbUser;
    SPEntrada.Parameters[8].value := edPlaca.text;
    SPEntrada.Parameters[9].value := edCfp.text;
    SPEntrada.Parameters[10].value := edRG.text;
    SPEntrada.Parameters[11].value := edCNH.text;
    SPEntrada.Parameters[12].value := edserie.text;

    SPEntrada.ExecProc;

    ShowMessage('Entrada Registrada');

    RLreport1.preview;
    //btnImprimir.Enabled := true;
    //
    close;
  end;
end;

procedure TfrmPortariaEnt.cbTipoExit(Sender: TObject);
begin
  Panel2.setfocus;

  edPlaca.Enabled := true;
  // edTransp.Enabled := true;
  edMotor.Enabled := true;
  edCfp.Enabled := true;
  edRG.Enabled := true;
  edCNH.Enabled := true;
  edPlaca.color := clWhite;
  // edTransp.color := clWhite;
  edMotor.color := clWhite;
  edCfp.color := clWhite;
  edRG.color := clWhite;
  edCNH.color := clWhite;
  edPlaca.setfocus;
  cbTransp.Visible := true;
  QTransp.open;
  cbTransp.clear;
  cbTransp.Items.add('');
  while not QTransp.eof do
  begin
    cbTransp.Items.add(QTranspNM_FORNECEDOR.value);
    QTransp.Next;
  end;
  QTransp.close;

  btnGerar.Enabled := true;
end;

procedure TfrmPortariaEnt.edPlacaExit(Sender: TObject);
begin
  if edPlaca.text <> '' then
  begin

    if Length(alltrim(copy(edPlaca.text, 1, 3))) <> 3 then
    begin
      ShowMessage('Placa sem as letras completas !!');
      edPlaca.setfocus;
      Exit;
    end;

    if Length(alltrim(copy(edPlaca.text, 4, 4))) <> 4 then
    begin
      ShowMessage('Placa sem os n�meros completos !!');
      edPlaca.setfocus;
      Exit;
    end;

    dtmDados.IQuery1.close;
    dtmDados.IQuery1.sql.clear;
    dtmDados.IQuery1.sql.add
      ('select * from tb_portaria where placa = :0 and motivo = ''E'' and dt_saida is null ');
    dtmDados.IQuery1.Parameters[0].value := apcarac(edPlaca.text);
    dtmDados.IQuery1.open;
    if not dtmDados.IQuery1.eof then
    begin
      ShowMessage
        ('Esta Placa J� Teve sua Entrada Registrada e Ainda n�o Teve Sa�da !!');
      edPlaca.setfocus;
      Exit;
    end
    else
    begin
      dtmDados.IQuery1.close;
      dtmDados.IQuery1.sql.clear;
      dtmDados.IQuery1.sql.add('select transportadora, motorista, cpf from ( ');
      dtmDados.IQuery1.sql.add
        ('select p.nm_transp transportadora , p.nm_motor motorista, p.nr_cpf cpf ');
      dtmDados.IQuery1.sql.add('from tb_portaria p ');
      dtmDados.IQuery1.sql.add('where p.placa = :0 order by data desc ) ');
      dtmDados.IQuery1.sql.add('where rownum = 1 ');
      dtmDados.IQuery1.Parameters[0].value := apcarac(edPlaca.text);
      dtmDados.IQuery1.open;
      if not dtmDados.IQuery1.eof then
      begin
        edMotor.text := dtmDados.IQuery1.FieldByName('motorista').AsString;
        edCfp.text := dtmDados.IQuery1.FieldByName('cpf').AsString;
        try
          cbTransp.ItemIndex := cbTransp.Items.IndexOf
            (dtmDados.IQuery1.FieldByName('transportadora').AsString);
        except
        end;
      end;
      dtmDados.IQuery1.close;
    end;
  end;
end;

end.
