object frmCodigoBarras: TfrmCodigoBarras
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Codigo de Barras'
  ClientHeight = 132
  ClientWidth = 599
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 599
    Height = 132
    Align = alClient
    TabOrder = 0
    object Label63: TLabel
      Left = 8
      Top = 46
      Width = 33
      Height = 13
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 7
      Width = 63
      Height = 13
      Caption = 'N'#250'mero Nota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edCcliente: TLabel
      Left = 8
      Top = 62
      Width = 63
      Height = 14
      Caption = 'edCcliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edNota: TLabel
      Left = 8
      Top = 23
      Width = 44
      Height = 14
      Caption = 'edNota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 104
      Top = 7
      Width = 24
      Height = 13
      Caption = 'S'#233'rie'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edSerie: TLabel
      Left = 104
      Top = 23
      Width = 44
      Height = 14
      Caption = 'edNota'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 86
      Width = 82
      Height = 13
      Caption = 'C'#243'digo de Barras'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lbAlerta: TLabel
      Left = 296
      Top = 6
      Width = 266
      Height = 18
      Caption = ' Inconsist'#234'ncia na Chave, verifique '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object edBarras: TEdit
      Left = 5
      Top = 105
      Width = 473
      Height = 21
      TabOrder = 0
      OnChange = edBarrasChange
    end
    object btnFechar: TBitBtn
      Left = 484
      Top = 102
      Width = 25
      Height = 22
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnFecharClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
        FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
        96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
        92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
        BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
        CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
        D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
        EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
        E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
        E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
        8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
        E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
        FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
        C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
        CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
        CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
        D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
        9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  object qrNf_tarefa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'chave'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) qtdrec'
      '  from tb_cte_nf'
      ' where notnfe = :chave'
      '')
    Left = 392
    Top = 40
  end
end
