unit MapaRetira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, JvMemoryDataset, Grids,
  DBGrids, RLReport, RLRichText;

type
  TfrmMapaRetira = class(TForm)
    qMapa: TADOQuery;
    qMapaROMANEIO: TBCDField;
    qMapaDESTINATARIO: TStringField;
    qMapaTRANSPORTADORA: TStringField;
    qMapaROTA: TStringField;
    QVolume: TADOQuery;
    QVolumeVOLUME: TStringField;
    QNF: TADOQuery;
    QNFNR_NF: TBCDField;
    Mdvolumes: TJvMemoryData;
    Mdvolumesnr_romaneio: TIntegerField;
    Mdvolumesnf: TStringField;
    Mdvolumesdestino: TStringField;
    Mdvolumesrota: TStringField;
    Mdvolumespedido: TStringField;
    Mdvolumesvolume: TStringField;
    Mdvolumestransportadora: TStringField;
    Mdvolumespalet: TIntegerField;
    qMapaPALET: TBCDField;
    dtsmdvol: TDataSource;
    Mdvolumestvol: TIntegerField;
    qMapaPEDIDO: TStringField;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLDBText6: TRLDBText;
    RLBand3: TRLBand;
    RLLabel14: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLBand4: TRLBand;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLGroup1: TRLGroup;
    RLBand2: TRLBand;
    RLDBText7: TRLDBText;
    RLLabel8: TRLLabel;
    RLDBRichText1: TRLDBRichText;
    RLLabel9: TRLLabel;
    RLDBRichText3: TRLDBRichText;
    RLLabel20: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel19: TRLLabel;
    RLDBText3: TRLDBText;
    RLDraw1: TRLDraw;
    RLDBMemo1: TRLDBMemo;
    RLBand5: TRLBand;
    RLLabel4: TRLLabel;
    RLDBText2: TRLDBText;
    RLLabel7: TRLLabel;
    RLDBText5: TRLDBText;
    RLLabel3: TRLLabel;
    RLDBText1: TRLDBText;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private

  public
    { Public declarations }
  end;

var
  frmMapaRetira: TfrmMapaRetira;

implementation

uses Dados, Menu;

{$R *.dfm}

procedure TfrmMapaRetira.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self.tag := 0;
end;

procedure TfrmMapaRetira.RLBand2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if Length(Mdvolumesvolume.Value) > 20 then
    RLBand2.Height := 32;
end;

procedure TfrmMapaRetira.RLBand3BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.sql.add
    ('select distinct count(distinct nr_nf) notas, count(codbar) volume ');
  dtmdados.IQuery1.sql.add('from cyber.itc_man_emb r ');
  dtmdados.IQuery1.sql.add('where r.romaneio = :0 ');
  dtmdados.IQuery1.sql.add('and r.cod_fil = :1 ');
  dtmdados.IQuery1.Parameters[0].Value := qMapaROMANEIO.AsInteger;
  dtmdados.IQuery1.Parameters[1].Value := glbfilial;
  dtmdados.IQuery1.open;
  RLLabel14.caption := 'Total Volumes : ' + dtmdados.IQuery1.FieldByName
    ('volume').AsString;
  RLLabel16.caption := 'Total Notas : ' + dtmdados.IQuery1.FieldByName
    ('notas').AsString;
  dtmdados.IQuery1.close;
end;

procedure TfrmMapaRetira.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var
  mapa, tvol: Integer;
  nf, vol: String;
begin
  mapa := self.tag;
  qMapa.close;
  qMapa.Parameters[0].Value := mapa;
  qMapa.Parameters[1].Value := glbfilial;
  qMapa.open;

  RLLabel1.caption := glbnmfilial;
  Mdvolumes.close;
  Mdvolumes.open;
  while not qMapa.Eof do
  begin
    QNF.close;
    QNF.Parameters[0].Value := qMapaROMANEIO.AsInteger;
    QNF.Parameters[1].Value := glbfilial;
    QNF.Parameters[2].Value := qMapaPEDIDO.AsInteger;
    QNF.open;
    nf := '';
    while not QNF.Eof do
    begin
      nf := nf + ' ' + QNFNR_NF.AsString;
      QNF.Next;
    end;
    QNF.close;
    QVolume.close;
    QVolume.Parameters[0].Value := qMapaROMANEIO.AsInteger;
    QVolume.Parameters[1].Value := glbfilial;
    QVolume.Parameters[2].Value := qMapaPALET.AsInteger;
    QVolume.Parameters[3].Value := qMapaPEDIDO.AsInteger;
    QVolume.open;
    vol := '';
    tvol := 0;
    while not QVolume.Eof do
    begin
      if vol = '' then
        vol := QVolumeVOLUME.AsString
      else
        vol := vol + '  ' + QVolumeVOLUME.AsString;
      tvol := tvol + 1;
      QVolume.Next;
    end;
    QVolume.close;
    Mdvolumes.Insert;
    Mdvolumesnr_romaneio.Value := qMapaROMANEIO.AsInteger;
    Mdvolumestransportadora.Value := qMapaTRANSPORTADORA.AsString;
    Mdvolumesdestino.Value := qMapaDESTINATARIO.AsString;
    Mdvolumesrota.Value := qMapaROTA.AsString;
    Mdvolumesnf.Value := nf;
    Mdvolumespedido.Value := qMapaPEDIDO.AsString;
    Mdvolumesvolume.Value := vol;
    Mdvolumespalet.Value := qMapaPALET.AsInteger;
    Mdvolumestvol.Value := tvol;
    Mdvolumes.Post;
    Mdvolumes.SortOnFields('pedido');
    qMapa.Next
  end;
end;

end.
