unit CadIsca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ImgList, DB, ADODB, ActnList, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid, System.ImageList, System.Actions,
  Vcl.Mask, JvExMask, JvToolEdit, JvBaseEdits;

type
  TfrmCadIsca = class(TForm)
    dtsIsca: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QIsca: TADOQuery;
    iml: TImageList;
    pnBotao: TPanel;
    pnGrid: TPanel;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    edDes: TEdit;
    QIscaID_ISCA: TFMTBCDField;
    QIscaNM_ISCA: TStringField;
    edCod: TJvCalcEdit;
    QIscaCOD_USUARIO: TFMTBCDField;
    QIscaDT_INC: TDateTimeField;
    QIscaCOD_ALTERA: TFMTBCDField;
    QIscaDT_ALTERA: TDateTimeField;
    QIscaFL_STATUS: TStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure edCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure dbgContratoDblClick(Sender: TObject);
  private
    quant: integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadIsca: TfrmCadIsca;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadIsca.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadIsca.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  edDes.Text := '';
  edCod.Value := 0;
  edDes.SetFocus;
end;

procedure TfrmCadIsca.btnSalvarClick(Sender: TObject);
VAR
  COD: integer;
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add
        ('select (nvl(max(id_isca),0)+1) as codigo from tb_iscas');
      dtmDados.iQuery1.open;
      COD := dtmDados.iQuery1.FieldByName('codigo').value;
      QIsca.append;
      QIscaID_ISCA.value := COD;
      QIscaCOD_USUARIO.Value := glbcoduser;
    end
    else
    begin
      QIsca.edit;
    end;
    QIscaNM_ISCA.value := edDes.Text;
    QIscaCOD_ALTERA.value := glbcoduser;
    QIscaDT_ALTERA.value := now;
    QIsca.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QIsca.Close;
  QIsca.open;
end;

procedure TfrmCadIsca.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QIsca.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadIsca.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QIsca.Close;
  if Pos('order by', QIsca.sql.Text) > 0 then
    QIsca.sql.Text := Copy(QIsca.sql.Text, 1,
      Pos('order by', QIsca.sql.Text) - 1) + 'order by ' + Column.FieldName
  else
    QIsca.sql.Text := QIsca.sql.Text + ' order by ' + Column.FieldName;
  QIsca.open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadIsca.edCNPJKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0' .. '9', #8]) then
    Key := #0;
end;

procedure TfrmCadIsca.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QIsca.Cancel;
end;

procedure TfrmCadIsca.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  edCod.value := QIscaID_ISCA.AsInteger;
  edDes.Text := QIscaNM_ISCA.value;
  edDes.SetFocus;
end;

procedure TfrmCadIsca.FormCreate(Sender: TObject);
begin
  QIsca.open;
  quant := QIsca.RecordCount;
  lblQuant.Caption := IntToStr(QIsca.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadIsca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QIsca.Close;
end;

procedure TfrmCadIsca.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := True;
  end;
end;

procedure TfrmCadIsca.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QIsca.First;
    2:
      QIsca.Prior;
    3:
      QIsca.Next;
    4:
      QIsca.Last;
  end;
  lblQuant.Caption := IntToStr(QIsca.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
