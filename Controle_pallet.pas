unit Controle_pallet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, JvExMask, JvToolEdit, JvBaseEdits, DB,
  ADODB;

type
  TfrmControle_pallet = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edPBR: TJvCalcEdit;
    edChep: TJvCalcEdit;
    btnConfirma: TBitBtn;
    Label4: TLabel;
    cbcte: TComboBox;
    lblrazao: TLabel;
    ADOQdes_razao: TADOQuery;
    ADOQdes_razaoNOMEAB: TStringField;
    QCliente: TADOQuery;
    QClienteCODCLIFOR: TBCDField;
    QManifesto: TADOQuery;
    QManifestoNR_ROMANEIO: TBCDField;
    QManifestoMOTORISTA: TStringField;
    QManifestoPLACA: TStringField;
    QManifestoVEICULO: TStringField;
    QManifestoDTINC: TDateTimeField;
    QManifestoTRANSPORTADORA: TStringField;
    QManifestoCODFIL: TBCDField;
    QClienteCLIENTE: TStringField;
    QQuant: TADOQuery;
    QQuantPALLET: TStringField;
    QQuantQUANT: TBCDField;
    Label5: TLabel;
    Label6: TLabel;
    edDesc: TJvCalcEdit;
    edOutro: TJvCalcEdit;
    procedure btnConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cbcteChange(Sender: TObject);
  private
  var
    mov, vazio: string;
    man: integer;
  public
    { Public declarations }
  end;

var
  frmControle_pallet: TfrmControle_pallet;

implementation

uses Dados, Portariasai, Menu, ConsPortaria, Vale_palete;

{$R *.dfm}

procedure TfrmControle_pallet.btnConfirmaClick(Sender: TObject);
var
  indexnum: integer;
  site : String;
begin
  if glbFilial = 1 then
  begin
    site := 'MAR';
  end
  else if glbFilial = 4 then
  begin
    site := 'PBA';
  end
  else if glbFilial = 16 then
  begin
    site := 'MAR';
  end
  else if glbFilial = 12 then
  begin
    site := 'PBA';
  end
  else if glbFilial = 9 then
  begin
    site := 'BTM';
  end
  else if glbFilial = 21 then
  begin
    site := 'ETM';
  end;

  vazio := '';
  if mov = '' then
    mov := 'S';

  if mov = 'S' then
  begin
    if (edPBR.Value = 0) and (edChep.Value = 0) and
      (QClienteCODCLIFOR.Value = 550) then
    begin
      ShowMessage('N�o foi colocado as quantidades');
      edPBR.setfocus;
      exit;
    end;
  end;

  if mov = 'E' then
  begin
    if (edPBR.value = 0) and (edChep.value = 0) and (edDesc.value = 0) and (edOutro.value = 0) then
    begin
      if Application.Messagebox('Confirma o Retorno sem Pallet ?',
        'Controle de Pallets', MB_YESNO + MB_ICONQUESTION) = IDNO then
      begin
        edPBR.setfocus;
        exit;
      end
      else
        vazio := 'S';
    end;
  end;

  if mov = 'E' then
  begin
    if (edPBR.Value > 0) or (vazio = 'S') then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.sql.add
        ('datinc, usuario, quant, tipo, destino, filial) ');
      dtmdados.IQuery1.sql.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6) ');
      dtmdados.IQuery1.Parameters[0].Value := man;
      dtmdados.IQuery1.Parameters[1].Value := 'PBR';
      dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].Value := edPBR.Value;
      dtmdados.IQuery1.Parameters[4].Value := mov;
      dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
      dtmdados.IQuery1.Parameters[6].Value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;

    if (edChep.Value > 0) or (vazio = 'S') then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.sql.add
        ('datinc, usuario, quant, tipo, destino, filial) ');
      dtmdados.IQuery1.sql.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6) ');
      dtmdados.IQuery1.Parameters[0].Value := man;
      dtmdados.IQuery1.Parameters[1].Value := 'CHEP';
      dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].Value := edChep.Value;
      dtmdados.IQuery1.Parameters[4].Value := mov;
      dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
      dtmdados.IQuery1.Parameters[6].Value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;

    if (edDesc.Value > 0) or (vazio = 'S') then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.sql.add
        ('datinc, usuario, quant, tipo, destino, filial) ');
      dtmdados.IQuery1.sql.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6) ');
      dtmdados.IQuery1.Parameters[0].Value := man;
      dtmdados.IQuery1.Parameters[1].Value := 'DESCART�VEL';
      dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].Value := edDesc.Value;
      dtmdados.IQuery1.Parameters[4].Value := mov;
      dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
      dtmdados.IQuery1.Parameters[6].Value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;

    if (edOutro.Value > 0) or (vazio = 'S') then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.sql.clear;
      dtmdados.IQuery1.sql.add
        ('insert into tb_controle_pallet (id, manifesto, pallet, ');
      dtmdados.IQuery1.sql.add
        ('datinc, usuario, quant, tipo, destino, filial) ');
      dtmdados.IQuery1.sql.add
        ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
      dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6) ');
      dtmdados.IQuery1.Parameters[0].Value := man;
      dtmdados.IQuery1.Parameters[1].Value := 'OUTROS';
      dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
      dtmdados.IQuery1.Parameters[3].Value := edOutro.Value;
      dtmdados.IQuery1.Parameters[4].Value := mov;
      dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
      dtmdados.IQuery1.Parameters[6].Value := GLBFilial;
      dtmdados.IQuery1.ExecSQL;
    end;
    // close;
    // verifica o index que j� foi lan�ado para excluir do combobox
    cbcte.ItemIndex := integer(cbcte.Items.IndexOf(cbcte.Text));
    indexnum := cbcte.ItemIndex;
    cbcte.Items.Delete(indexnum);
    edPBR.Text := '';
    edChep.Text := '';
    edDesc.Text := '';
    edOutro.Text := '';
    edPBR.Enabled := false;
    edChep.Enabled := false;
    edDesc.Enabled := false;
    edOutro.Enabled := false;
    btnConfirma.Enabled := false;
    lblrazao.Visible := false;
    if cbcte.Items.Count = 0 then
      // cbcte.Text := '';
      close;
  end;

  if mov = 'S' then
  begin
    if Application.Messagebox('Confirma a Quantidade ?', 'Controle de Pallets',
      MB_YESNO + MB_ICONQUESTION) = IDYES then
    begin
      if edPBR.Value > 0 then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add
          ('insert into tb_controle_pallet (id, manifesto, pallet, ');
        dtmdados.IQuery1.sql.add
          ('datinc, usuario, quant, tipo, destino, doc, filial) ');
        dtmdados.IQuery1.sql.add
          ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
        dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6, :7) ');
        dtmdados.IQuery1.Parameters[0].Value := man;
        dtmdados.IQuery1.Parameters[1].Value := 'PBR';
        dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
        dtmdados.IQuery1.Parameters[3].Value := edPBR.Value;
        dtmdados.IQuery1.Parameters[4].Value := mov;
        dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
        dtmdados.IQuery1.Parameters[6].Value := copy(Label3.caption, 1, 1);
        dtmdados.IQuery1.Parameters[7].Value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
      end;

      if edChep.Value > 0 then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add
          ('insert into tb_controle_pallet (id, manifesto, pallet, ');
        dtmdados.IQuery1.sql.add
          ('datinc, usuario, quant, tipo, destino, doc, filial) ');
        dtmdados.IQuery1.sql.add
          ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
        dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6, :7) ');
        dtmdados.IQuery1.Parameters[0].Value := man;
        dtmdados.IQuery1.Parameters[1].Value := 'CHEP';
        dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
        dtmdados.IQuery1.Parameters[3].Value := edChep.Value;
        dtmdados.IQuery1.Parameters[4].Value := mov;
        dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
        dtmdados.IQuery1.Parameters[6].Value := copy(Label3.caption, 1, 1);
        dtmdados.IQuery1.Parameters[7].Value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
      end;

      if edDesc.Value > 0 then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add
          ('insert into tb_controle_pallet (id, manifesto, pallet, ');
        dtmdados.IQuery1.sql.add
          ('datinc, usuario, quant, tipo, destino, doc, filial) ');
        dtmdados.IQuery1.sql.add
          ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
        dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6, :7) ');
        dtmdados.IQuery1.Parameters[0].Value := man;
        dtmdados.IQuery1.Parameters[1].Value := 'DESCART�VEL';
        dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
        dtmdados.IQuery1.Parameters[3].Value := edDesc.Value;
        dtmdados.IQuery1.Parameters[4].Value := mov;
        dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
        dtmdados.IQuery1.Parameters[6].Value := copy(Label3.caption, 1, 1);
        dtmdados.IQuery1.Parameters[7].Value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
      end;

      if edOutro.Value > 0 then
      begin
        dtmdados.IQuery1.close;
        dtmdados.IQuery1.sql.clear;
        dtmdados.IQuery1.sql.add
          ('insert into tb_controle_pallet (id, manifesto, pallet, ');
        dtmdados.IQuery1.sql.add
          ('datinc, usuario, quant, tipo, destino, doc, filial) ');
        dtmdados.IQuery1.sql.add
          ('values ((select nvl(max(id)+1,1) from tb_controle_pallet), ');
        dtmdados.IQuery1.sql.add(':0, :1, sysdate, :2, :3, :4, :5, :6, :7) ');
        dtmdados.IQuery1.Parameters[0].Value := man;
        dtmdados.IQuery1.Parameters[1].Value := 'OUTROS';
        dtmdados.IQuery1.Parameters[2].Value := GLBUSER;
        dtmdados.IQuery1.Parameters[3].Value := edOutro.Value;
        dtmdados.IQuery1.Parameters[4].Value := mov;
        dtmdados.IQuery1.Parameters[5].Value := cbcte.Text;
        dtmdados.IQuery1.Parameters[6].Value := copy(Label3.caption, 1, 1);
        dtmdados.IQuery1.Parameters[7].Value := GLBFilial;
        dtmdados.IQuery1.ExecSQL;
      end;
      // close;
      // verifica o index que j� foi lan�ado para excluir do combobox

      // Inclu�do processo de impress�o do Vale Palete  na portaria de sa�da
      if ((edPBR.Value + edChep.Value + edDesc.Value + edOutro.value) > 0) and (QClienteCODCLIFOR.Value <> 550)
      then
      begin
        QManifesto.close;
        QManifesto.Parameters[0].Value := site;
        QManifesto.Parameters[1].Value := site;
        QManifesto.Parameters[2].Value := man;
        if copy(Label3.caption, 1, 1) = 'M' then
          QManifesto.Parameters[3].Value := 'M'
        else
          QManifesto.Parameters[3].Value := 'R';
        QManifesto.Parameters[4].Value := GLBFilial;
        QManifesto.Open;
        Application.CreateForm(TfrmVale_palete, frmVale_palete);
        frmVale_palete.motorista.caption := QManifestoMOTORISTA.AsString;
        frmVale_palete.RlLabel40.caption := QManifestoMOTORISTA.AsString;
        frmVale_palete.transportadora.caption := QManifestoTRANSPORTADORA.AsString;
        frmVale_palete.RlLabel41.caption := QManifestoTRANSPORTADORA.AsString;
        frmVale_palete.veiculo.caption := QManifestoVEICULO.AsString;
        frmVale_palete.RlLabel44.caption := QManifestoVEICULO.AsString;
        frmVale_palete.placa.caption := QManifestoPLACA.AsString;
        frmVale_palete.RlLabel36.caption := QManifestoPLACA.AsString;
        frmVale_palete.NR_ROMANEIO.caption :=
          VarToStr(QManifestoNR_ROMANEIO.Value);
        frmVale_palete.RlLabel46.caption :=
          VarToStr(QManifestoNR_ROMANEIO.Value);
        frmVale_palete.DTIC.caption := DateTimeToStr(QManifestoDTINC.Value);
        frmVale_palete.RlLabel48.caption :=
          DateTimeToStr(QManifestoDTINC.Value);
        frmVale_palete.RlLabel51.caption := QClienteCLIENTE.AsString;
        frmVale_palete.RlLabel52.caption := QClienteCLIENTE.AsString;
        frmVale_palete.RlLabel54.caption := ADOQdes_razaoNOMEAB.AsString + ' - '
          + cbcte.Text;
        frmVale_palete.RlLabel56.caption := ADOQdes_razaoNOMEAB.AsString + ' - '
          + cbcte.Text;

        QQuant.close;
        QQuant.Parameters[0].Value := QManifestoNR_ROMANEIO.AsInteger;
        if copy(Label3.caption, 1, 1) = 'M' then
          QQuant.Parameters[1].Value := 'M'
        else
          QQuant.Parameters[1].Value := 'R';
        QQuant.Parameters[2].Value := GLBFilial;
        QQuant.Open;

        if QQuant.locate('Pallet', 'PBR', []) then
        begin
          frmVale_palete.RlLabel57.caption := QQuantQUANT.AsString;
          frmVale_palete.RlLabel59.caption := QQuantQUANT.AsString;
        end;
        if QQuant.locate('Pallet', 'CHEP', []) then
        begin
          frmVale_palete.RlLabel58.caption := QQuantQUANT.AsString;
          frmVale_palete.RlLabel60.caption := QQuantQUANT.AsString;
        end;
        if QQuant.locate('Pallet', 'DESCART�VEL', []) then
        begin
          frmVale_palete.RlLabel61.caption := QQuantQUANT.AsString;
          frmVale_palete.RlLabel62.caption := QQuantQUANT.AsString;
        end;
        if QQuant.locate('Pallet', 'OUTROS', []) then
        begin
          frmVale_palete.RlLabel71.caption := QQuantQUANT.AsString;
          frmVale_palete.RlLabel72.caption := QQuantQUANT.AsString;
        end;
        QQuant.close;

        if (QClienteCODCLIFOR.Value = 7283) OR (QClienteCODCLIFOR.Value = 13205)
        then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\cless.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\cless.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 21615) OR
          (QClienteCODCLIFOR.Value = 21366) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\casasui�a.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\casasui�a.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 305) OR
          (QClienteCODCLIFOR.Value = 8119) OR (QClienteCODCLIFOR.Value = 20765)
          OR (QClienteCODCLIFOR.Value = 20934) OR
          (QClienteCODCLIFOR.Value = 21642) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\meritor.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\meritor.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 244) OR
          (QClienteCODCLIFOR.Value = 7337) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\mirka.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\mirka.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 7988) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\baruel.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\baruel.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 21515) OR
          (QClienteCODCLIFOR.Value = 21516) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\dicate.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\dicate.bmp');
        end
        else if (QClienteCODCLIFOR.Value = 55783) OR
          (QClienteCODCLIFOR.Value = 7148) then
        begin
          frmVale_palete.RLImage2.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage2.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\colgate.bmp');
          frmVale_palete.RLImage5.Picture.RegisterFileFormat('BMP',
            'BMP file', TBitmap);
          frmVale_palete.RLImage5.Picture.LoadFromFile
            ('\\192.168.236.112\sim\Atualizador\Relatorio\colgate.bmp');
        end;

        frmVale_palete.RLReport1.Preview;
        frmVale_palete.Free;
      end;
      cbcte.ItemIndex := integer(cbcte.Items.IndexOf(cbcte.Text));
      indexnum := cbcte.ItemIndex;
      cbcte.Items.Delete(indexnum);
      edPBR.Value := 0;
      edChep.Value := 0;
      edDesc.Value := 0;
      edOutro.Value := 0;
      edPBR.Enabled := false;
      edChep.Enabled := false;
      edDesc.Enabled := false;
      edOutro.Enabled := false;
      btnConfirma.Enabled := false;
      lblrazao.Visible := false;
      if cbcte.Items.Count = 0 then
      begin
        edPBR.Value := -1;
        edChep.Value := -1;
        edDesc.Value := -1;
        edOutro.Value := -1;
        close;
      end;
    end
    else
      edPBR.setfocus;
  end;
end;

procedure TfrmControle_pallet.cbcteChange(Sender: TObject);
var site : String;
begin
  if glbFilial = 1 then
  begin
    site := 'MAR';
  end
  else if glbFilial = 4 then
  begin
    site := 'PBA';
  end
  else if glbFilial = 16 then
  begin
    site := 'MAR';
  end
  else if glbFilial = 12 then
  begin
    site := 'PBA';
  end
  else if glbFilial = 9 then
  begin
    site := 'BTM';
  end
  else if glbFilial = 21 then
  begin
    site := 'ETM';
  end;
  ADOQdes_razao.close;
  ADOQdes_razao.Parameters[0].Value := cbcte.Text;
  ADOQdes_razao.Open;
  lblrazao.caption := ADOQdes_razaoNOMEAB.AsString;
  lblrazao.Visible := true;
  // verifica se o combobox est� vazia (Implementa��o Hugo)
  if cbcte.Text <> '' then
  begin
    edPBR.Enabled := true;
    edChep.Enabled := true;
    edDesc.Enabled := true;
    edOutro.Enabled := true;
    btnConfirma.Enabled := true;
    QCliente.close;
    QCliente.Parameters[0].Value := glbfilial;
    QCliente.Parameters[1].Value := man;
    QCliente.Parameters[2].Value := cbcte.Text;
    QCliente.Parameters[3].Value := man;
    QCliente.Parameters[4].Value := cbcte.Text;
    QCliente.Parameters[5].Value := man;
    QCliente.Parameters[6].Value := cbcte.Text;
    QCliente.Open;
  end;
end;

procedure TfrmControle_pallet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if (edPBR.Value = 0) and (edChep.Value = 0) and
    (QClienteCODCLIFOR.Value = 550) then
  begin
    if mov = 'S' then
    begin
      ShowMessage('N�o ser� poss�vel dar Sa�da neste Manifesto');
      frmPortariaSai.tag := 1;
    end
    else
    begin
      if mov = '' then
      begin
        ShowMessage('N�o ser� poss�vel dar Entrada neste Manifesto');
        frmConsPortaria.tag := 1;
      end;
    end
  end;
end;

procedure TfrmControle_pallet.FormCreate(Sender: TObject);
begin
  if frmMenu.tag = 2 then
  begin
    if frmPortariaSai.cbtipo.Text = 'Manifesto' then
    begin
      Label3.caption := 'Manifesto : ' +
        IntToStr(frmPortariaSai.QManNR_ROMANEIO.AsInteger);
      man := frmPortariaSai.QManNR_ROMANEIO.AsInteger;
    end
    else
    begin
      Label3.caption := 'Romaneio : ' +
        IntToStr(frmPortariaSai.QManifestoNR_ROMANEIO.AsInteger);
      man := frmPortariaSai.QManifestoNR_ROMANEIO.AsInteger;
    end;
    mov := 'S';
    cbcte.ItemIndex := 0;
  end
  else if frmMenu.tag = 3 then
  begin
    if frmConsPortaria.QSaidadescricao.Value = 'Manifesto' then
      Label3.caption := 'Manifesto : ' +
        IntToStr(frmConsPortaria.QSaidaMANIFESTO.AsInteger)
    else
      Label3.caption := 'Romaneio : ' +
        IntToStr(frmConsPortaria.QSaidaMANIFESTO.AsInteger);
    mov := 'S';
    man := frmConsPortaria.QSaidaMANIFESTO.AsInteger;
  end
  else
  begin
    Label3.caption := 'Manifesto : ' +
      IntToStr(frmConsPortaria.QEntradaMANIFESTO.AsInteger);
    mov := 'E';
    man := frmConsPortaria.QEntradaMANIFESTO.AsInteger;
  end;
end;

end.
