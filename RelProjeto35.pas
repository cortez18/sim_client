unit RelProjeto35;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Data.DB, Data.Win.ADODB, Vcl.DBCtrls,
  JvDBControls, Vcl.Samples.Gauges, Vcl.Mask, JvExMask, JvToolEdit, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid, ShellApi;

type
  TfrmRelProjeto35 = class(TForm)
    QControl: TADOQuery;
    navnavig: TDataSource;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    btnExcel: TBitBtn;
    Panel3: TPanel;
    Gauge1: TGauge;
    JvDBNavigator1: TJvDBNavigator;
    Label1: TLabel;
    QControlFILIAL: TFMTBCDField;
    QControlFATURA: TStringField;
    QControlDESTINO: TStringField;
    QControlCLIENTE: TStringField;
    QControlTRANSPORTADOR: TStringField;
    QControlMANIFESTO: TFMTBCDField;
    QControlCT_E: TFMTBCDField;
    QControlFILIAL_CTE: TFMTBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelProjeto35: TfrmRelProjeto35;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelProjeto35.btnExcelClick(Sender: TObject);
var // coluna, linha: integer;
  // excel: variant;
  // valor: string;
  //memo1: TStringList;
  //sarquivo: String;
  sarquivo: TextFile;
  ValorDoCampo,NomeDoCampo,Nome:String;
  i: integer;
  //memo1: TStringList;
begin
  nome := 'C:/SIM/RelProjeto35_' + ApCarac(DateToStr(date)) + '.csv';
  AssignFile(sarquivo,nome);
  Rewrite(sarquivo);
  QControl.First;
  //Escreve Nome dos Campos
  For i:=0 to QControl.FieldCount-1 Do
	Begin
	  NomeDoCampo:=QControl.Fields[i].FieldName;
	  Write(sArquivo,NomeDoCampo+';');
	End;
  Writeln(sArquivo,'');
  //Adicionar Valores separados por ';'
  Gauge1.MaxValue := QControl.RecordCount;
  While Not QControl.Eof dO
	Begin
	  for i:=0 TO QControl.FieldCount-1 do
		Begin
		  ValorDoCampo:=QControl.Fields[I].AsString;
		  Write(sArquivo,ValorDoCampo+';');
		End;
	  Writeln(sArquivo,'');
    Gauge1.AddProgress(1);
	  QControl.Next;
	End;
  CloseFile(sArquivo);
  ShellExecute(Application.Handle, nil, PChar(nome), nil, nil,
    SW_SHOWNORMAL);
{  memo1 := TStringList.Create;
  sarquivo := 'C:/SIM/RelControladoria' + ApCarac(DateToStr(date)) + '.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '       <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}//');
 { memo1.Add(
    '              .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}//');
 { memo1.Add(
    '              .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}//');
 { memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QControl.First;
  Gauge1.MaxValue := QControl.RecordCount;
  while not QControl.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QControl.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QControl.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  memo1.Free;
  ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
    SW_SHOWNORMAL);
           }
end;

procedure TfrmRelProjeto35.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;

  Panel1.Visible := true;
  Application.ProcessMessages;
  QControl.close;
  QControl.sql [4] := 'and c.dt_fatura between to_date(''' + dtInicial.text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.text +
      ' 23:59'',''dd/mm/yy hh24:mi'')';
  QControl.Open;
  Panel1.Visible := false;
  Label1.caption := IntToStr(QControl.RecordCount) + ' Registros';
end;

procedure TfrmRelProjeto35.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QControl.close;
end;

procedure TfrmRelProjeto35.FormCreate(Sender: TObject);
begin
  Panel1.Left := trunc((self.Width / 2) - (Panel1.Width / 2));
  Panel1.Top := trunc((self.Height / 2) - (Panel1.Height / 2));
end;

procedure TfrmRelProjeto35.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QControl.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

end.
