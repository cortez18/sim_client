object FrmExporta_WMS_Rodopar: TFrmExporta_WMS_Rodopar
  Left = 498
  Top = 301
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Exportar Pedido WMS -> Rodopar'
  ClientHeight = 165
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100001000400E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000007070000000000
    0000000000000000707070707000000000000000000000070707070707000000
    7000007070700070707070707070000707070707070700070707070707000070
    7070707070707070707070707070070707070707070707070707070007077070
    7070707070707070707070000070070707070707070707070707070007070070
    7070707070707070707070707070000000000000000700070707070707000000
    000000000070007070000000707000000000000000000007007B7B7007000000
    0000000000000000B7B7B7B7B0000000000000000000000B7B7B7B7B7B000000
    7700077770070007B7B7B7B7B7000000000000000000007B7B7B7B000B7000B7
    B7B7B7B7B7B7B7B7B7B7B00000B00B7B7B7B7B7B7B7B7B7B7B7B700000700BBB
    BBBBBBBBBBB7B7B7B7B7B07770B0000000000000000B007B7B7B7B000B700000
    00000000000B0007B7B7B7B7B7000000000000000000000B7B7B7B7B7B000000
    0000000000000000B7B7B7B7B00000000000000000000000007B7B7000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFEBFFFFFF557FFFFEAABF7D5D555EAAAEAABD555
    5555AAAAAABA5555557DAAAAAABAD5555555FFFEEAABFFFDD415FFFFE003FFFF
    E003F384C001E0004001C000000080000038000000380000000080000000FFFC
    4001FFFCC001FFFFE003FFFFF007FFFFFC1FFFFFFFFFFFFFFFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 147
    Top = 61
    Width = 33
    Height = 13
    Caption = 'Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 60
    Width = 32
    Height = 13
    Caption = 'Cliente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object RadioGroup1: TRadioGroup
    Left = 20
    Top = 9
    Width = 233
    Height = 45
    Caption = 'WMS'
    Columns = 3
    Items.Strings = (
      'Client'
      'WEB v9'
      'WEB v11')
    TabOrder = 3
    OnClick = RadioGroup1Click
  end
  object edPedido: TEdit
    Left = 147
    Top = 80
    Width = 74
    Height = 21
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 30
    ParentFont = False
    TabOrder = 1
    OnEnter = edPedidoEnter
    OnExit = edPedidoExit
  end
  object btnCancelar: TBitBtn
    Left = 74
    Top = 113
    Width = 75
    Height = 25
    Caption = 'Sair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
      9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
      71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
      9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
      FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
      A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
      FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
      AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
      FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
      B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
      FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
      B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
      3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
      BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
      66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
      BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
      47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
      C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
      FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
      C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
      FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
      CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
      FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
      D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
      FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
      D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
      FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
      D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
    ParentFont = False
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object cbCliente: TComboBox
    Left = 8
    Top = 77
    Width = 90
    Height = 21
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object SP: TADOStoredProc
    Connection = dtmDados.ConWms
    ProcedureName = 'SP_ATU_RODOPAR'
    Parameters = <
      item
        Name = 'VPEDIDO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VCLIENTE'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end>
    Left = 16
    Top = 73
  end
  object QCliente: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    BeforeOpen = QClienteBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct id_klient'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in(75,95) and p.opid_bearb is null'
      'and p.lager = :0'
      'order by 1')
    Left = 16
    Top = 12
    object QClienteID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
  end
  object QConsultStat: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select stat from wms.auftraege where nr_auf =:0 and id_klient =:' +
        '1')
    Left = 12
    Top = 116
    object QConsultStatSTAT: TStringField
      FieldName = 'STAT'
      Size = 2
    end
  end
  object QClienteW: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    BeforeOpen = QClienteWBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct id_klient'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in(75,95)'
      'and p.lager in (0)'
      'order by 1')
    Left = 104
    Top = 8
    object QClienteWID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
  end
  object SPW: TADOStoredProc
    Connection = dtmDados.ConWmsWeb
    ProcedureName = 'SP_ATU_RODOPAR'
    Parameters = <
      item
        Name = 'VPEDIDO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VCLIENTE'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VFILIAL'
        DataType = ftInteger
        Value = 0
      end>
    Left = 108
    Top = 65
  end
  object QConsultStatW: TADOQuery
    Connection = dtmDados.ConWmsWeb
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select stat from auftraege where nr_auf =:0 and id_klient =:1')
    Left = 108
    Top = 120
    object QConsultStatWSTAT: TStringField
      FieldName = 'STAT'
      Size = 2
    end
  end
  object QclienteV11: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConWMSV11
    CursorType = ctStatic
    BeforeOpen = QclienteV11BeforeOpen
    Parameters = <>
    SQL.Strings = (
      'select distinct id_klient'
      
        'from auftraege p left join spediteure t on p.id_spediteur = t.id' +
        '_spediteur'
      'where p.stat in('#39'75'#39','#39'95'#39')'
      'and p.lager in ('#39#39')'
      'order by 1')
    Left = 176
    Top = 8
    object QclienteV11ID_KLIENT: TStringField
      FieldName = 'ID_KLIENT'
      ReadOnly = True
      Size = 12
    end
  end
  object SPv11: TADOStoredProc
    Connection = dtmDados.ConWMSV11
    ProcedureName = 'SP_ATU_RODOPAR'
    Parameters = <
      item
        Name = 'VPEDIDO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VCLIENTE'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VFILIAL'
        DataType = ftInteger
        Value = 0
      end>
    Left = 180
    Top = 65
  end
  object QConsultStatV11: TADOQuery
    Connection = dtmDados.ConWMSV11
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select stat from auftraege where nr_auf =:0 and id_klient =:1')
    Left = 180
    Top = 120
    object QConsultStatV11STAT: TStringField
      FieldName = 'STAT'
      Size = 2
    end
  end
end
