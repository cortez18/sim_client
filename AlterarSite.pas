unit AlterarSite;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, Variants, Grids, DBGrids,
  inifiles, Mask, ADODB, JvExControls,
  JvGradient, JvExStdCtrls, JvCombobox;

type
  TFrmAlterarSite = class(TForm)
    Panel1: TPanel;
    btnOk: TBitBtn;
    btnCancelar: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edSite: TEdit;
    JvGradient3: TJvGradient;
    cbEmpresa: TJvComboBox;
    QSite: TADOQuery;
    QSiteCODFIL: TBCDField;
    QSiteFILIAL: TStringField;
    QFuncionario: TADOQuery;
    QFuncionarioNAME: TStringField;
    QFuncionarioSITE: TStringField;
    QSiteUF: TStringField;
    QFuncionariocodfunc: TBCDField;
    QParametro: TADOQuery;
    QParametroPDF: TStringField;
    QParametroSCHEMMA: TStringField;
    QParametroPDF_CTE: TStringField;
    QParametroCONTROLADORIA: TStringField;
    QParametroSEGURADORA: TStringField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbEmpresaChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAlterarSite: TFrmAlterarSite;

implementation

uses dados, menu, funcoes;

{$R *.DFM}

procedure TFrmAlterarSite.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

procedure TFrmAlterarSite.FormCreate(Sender: TObject);
var
  emp: string;
begin
  edSite.Text := IntToStr(glbfilial) + '-' + glbnmfilial;
  QFuncionario.close;
  QFuncionario.Parameters[0].Value := GlbUser;
  QFuncionario.open;
  emp := QFuncionarioSITE.AsString;
  QSite.close;
  QSite.SQL[3] := 'and f.codfil in (' + emp + ')';
  QSite.open;
  if QSite.RecordCount > 1 then
  begin
    cbEmpresa.Clear;
    while not QSite.eof do
    begin
      cbEmpresa.Items.add(QSiteFILIAL.Value);
      QSite.Next;
    end;
  end
  else
  begin
    cbEmpresa.Clear;
    cbEmpresa.Items.add(QSiteFILIAL.Value);
    dtmdados.Query1.close;
  end;
  cbEmpresa.itemindex := 0;
end;

procedure TFrmAlterarSite.btnOkClick(Sender: TObject);
begin
  ShowMessage('Site Alterado com Sucesso !!');
  glbfilial := StrToInt(alltrim(copy(cbEmpresa.Text, 1,
    Pos('-', cbEmpresa.Text) - 1)));
  glbnmfilial := copy(cbEmpresa.Text, (Pos('-', cbEmpresa.Text) + 1), 20);
  frmMenu.Caption := 'SIM - Sistema de Integra��o e Monitoramento - Filial : ' +
    cbEmpresa.Text;
  btnOk.Enabled := false;
  frmMenu.ACBrMDFe1.Configuracoes.WebServices.UF := QSiteUF.AsString;
  // verifica ambiente do manifesto
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.Clear;
  dtmdados.IQuery1.SQL.add
    ('select ambiente, certificado, logo, pdf, nvl(custo,0) custo, ');
  dtmdados.IQuery1.SQL.add
    ('schemma, proxy, porta, user_proxy, pass_proxy, fl_conferencia ');
  dtmdados.IQuery1.SQL.add
    ('from tb_controle where doc = ''MDFE'' and filial = :0 ');
  dtmdados.IQuery1.Parameters[0].Value := glbfilial;
  dtmdados.IQuery1.open;
  if (dtmdados.IQuery1.eof) or (dtmdados.IQuery1.FieldByName('ambiente').IsNull)
  then
  begin
    ShowMessage('N�o Encontrado nenhuma configura��o de MDFe, avise o TI');
  end
  else
  begin
    QParametro.open;
    GlbSchema := ALLTRIM(QParametroSCHEMMA.AsString);
    glbPDF :=  ALLTRIM(QParametropdf.AsString);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    frmMenu.ACBrMDFe1.DAMDFe.PathPDF := IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathMDFe :=
      IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrMDFe1.Configuracoes.Arquivos.PathSalvar :=
      IncludeTrailingPathDelimiter(glbPDF);
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathCte :=
      IncludeTrailingPathDelimiter(ALLTRIM(QParametroPDF_CTE.AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSalvar :=
      IncludeTrailingPathDelimiter(ALLTRIM(QParametroPDF_CTE.AsString));
    frmMenu.ACBrCte1.Configuracoes.Arquivos.PathSchemas := GlbSchema;
    QParametro.close;

    GlbMdfeamb := dtmdados.IQuery1.FieldByName('ambiente').AsString;
    GlbMdfecus := dtmdados.IQuery1.FieldByName('custo').Value;
    GlbConf := dtmdados.IQuery1.FieldByName('fl_conferencia').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.Certificados.NumeroSerie :=
      dtmdados.IQuery1.FieldByName('certificado').AsString;

    frmMenu.ACBrMDFe1.DAMDFe.Logo := dtmdados.IQuery1.FieldByName
      ('logo').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyHost :=
      dtmdados.IQuery1.FieldByName('proxy').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPort :=
      dtmdados.IQuery1.FieldByName('porta').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyUser :=
      dtmdados.IQuery1.FieldByName('user_proxy').AsString;
    frmMenu.ACBrMDFe1.Configuracoes.WebServices.ProxyPass :=
      dtmdados.IQuery1.FieldByName('pass_proxy').AsString;
    dtmdados.IQuery1.close;
  end;
  //parametros da filial
  dtmdados.IQuery1.close;
  dtmdados.IQuery1.SQL.Clear;
  dtmdados.IQuery1.SQL.add('select * from tb_empresa where codfil = :0 ');
  dtmdados.IQuery1.Parameters[0].Value := glbfilial;
  dtmdados.IQuery1.open;
  glbVerao := dtmdados.IQuery1.FieldByName('verao').AsString;
  frmMenu.ACBrMail1.Host := Trim(dtmdados.IQuery1.FieldByName('smtp')
    .AsString);
  frmMenu.ACBrMail1.Password := Trim(dtmdados.IQuery1.FieldByName('senha')
    .AsString);
  frmMenu.ACBrMail1.Username := Trim(dtmdados.IQuery1.FieldByName('email')
    .AsString);
  frmMenu.ACBrMail1.From := Trim(dtmdados.IQuery1.FieldByName('email')
    .AsString);
  frmMenu.ACBrMail1.Port := dtmdados.IQuery1.FieldByName('porta').value;

  if dtmdados.IQuery1.FieldByName('senha_a3').value <> ' ' then
    frmMenu.ACBrCte1.Configuracoes.Certificados.Senha :=
      ALLTRIM(dtmdados.IQuery1.FieldByName('senha_a3').AsString);
  glbtrip := dtmdados.IQuery1.FieldByName('trip').AsString;
  glbimpr := dtmdados.IQuery1.FieldByName('IMP_ETI_VOL').AsString;
  dtmdados.IQuery1.close;
  close;
end;

procedure TFrmAlterarSite.cbEmpresaChange(Sender: TObject);
begin
  if cbEmpresa.Text <> edSite.Text then
    btnOk.Enabled := true;
end;

procedure TFrmAlterarSite.btnCancelarClick(Sender: TObject);
begin
  close;
end;

procedure TFrmAlterarSite.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QSite.close;
  close;
end;

end.
