unit PainelControle_Volumes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, DB, ADODB,
  Mask, JvExMask, JvToolEdit, JvMemoryDataset, JvDBLookup, ShellAPI, JvMaskEdit,
  Vcl.Samples.Gauges, Vcl.DBCtrls, JvDBControls, JvExExtCtrls, JvRadioGroup,
  JvBaseEdits, JvExStdCtrls, JvEdit, Vcl.FileCtrl, JvExDBGrids, JvDBGrid;

type
  TfrmPainelControleVol = class(TForm)
    QVolRecebidos: TADOQuery;
    DSVolRecebidos: TDataSource;
    QVolRecebidosVOLUMES: TBCDField;
    QVolRecebidosINVOICE: TStringField;
    QVolRecebidosCUST_NAME: TStringField;
    QVolRecebidosCUST_NUMBER: TStringField;
    QVolRecebidosSTAT: TStringField;
    QVolRecebidosUSER_FIM: TStringField;
    Qcliente: TADOQuery;
    QclienteCLIENTE: TStringField;
    Qcnpj: TADOQuery;
    QcnpjCLIENTE: TStringField;
    QcnpjCUST_NUMBER: TStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cbcliente: TComboBox;
    BTConsultar: TBitBtn;
    dtinicial: TJvDateEdit;
    dtfinal: TJvDateEdit;
    cbcnpj: TComboBox;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBGrid2: TDBGrid;
    QExpedidos: TADOQuery;
    Dsexpedidos: TDataSource;
    QClienteExp: TADOQuery;
    QClienteExpNOME: TStringField;
    dtinicialExp: TJvDateEdit;
    dtfinalexp: TJvDateEdit;
    QVolRecebidosDATAINC: TDateTimeField;
    DSPendentes: TDataSource;
    SPVolSkf: TADOStoredProc;
    qpendentes: TADOQuery;
    DSPend: TDataSource;
    DBGrid3: TDBGrid;
    btconsultarexp: TBitBtn;
    Panel3: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    BtConsultarPe: TBitBtn;
    dtinicialpe: TJvDateEdit;
    dtfinalpe: TJvDateEdit;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    QClienteExpCODCGC: TStringField;
    dtsClienteExp: TDataSource;
    QClienteExpCODCLIFOR: TBCDField;
    ledIdClientePen: TJvDBLookupEdit;
    ledClientePen: TJvDBLookupEdit;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    btnDacte: TBitBtn;
    Label13: TLabel;
    edBOX: TJvMaskEdit;
    btnRelExpedido: TBitBtn;
    Label14: TLabel;
    edBoxP: TJvMaskEdit;
    QCliPen: TADOQuery;
    dtsCliPen: TDataSource;
    QCliPenCUST_NUMBER: TStringField;
    QCliPenRAZSOC: TStringField;
    Label15: TLabel;
    btnRelRec: TBitBtn;
    Label16: TLabel;
    edBoxR: TJvMaskEdit;
    Label17: TLabel;
    edTripR: TJvMaskEdit;
    Label18: TLabel;
    edTripE: TJvMaskEdit;
    Label19: TLabel;
    edTripP: TJvMaskEdit;
    QVolRecebidosBOX: TStringField;
    QVolRecebidosTRIP: TStringField;
    qpendentesBOX: TStringField;
    qpendentesTRIP: TStringField;
    qpendentesINVOICE: TStringField;
    qpendentesDATA_TRIP: TDateTimeField;
    qpendentesCUST_NAME: TStringField;
    qpendentesCUST_NUMBER: TStringField;
    qpendentesDATAINC: TDateTimeField;
    qpendentesCODCON: TBCDField;
    btnExcluirP: TBitBtn;
    Panel7: TPanel;
    tsExpedir: TTabSheet;
    DBGrid4: TDBGrid;
    QExpedir: TADOQuery;
    dtsExpedir: TDataSource;
    Panel8: TPanel;
    btnExcel: TBitBtn;
    lblVolumes: TLabel;
    QExpedirID_TRIP: TStringField;
    QExpedirBOX: TStringField;
    QExpedirCTE: TFMTBCDField;
    QExpedirNR_ROMANEIO: TFMTBCDField;
    QExpedirUSER_LIBERADO: TStringField;
    QExpedirDATA_LIBERADO: TDateTimeField;
    tsVolumes: TTabSheet;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    eddtini: TJvDateEdit;
    eddtfim: TJvDateEdit;
    JvDBLookupEdit1: TJvDBLookupEdit;
    JvDBGrid1: TJvDBGrid;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    edVolume: TJvEdit;
    edmanifesto: TJvCalcEdit;
    RGTipo: TJvRadioGroup;
    edHrF: TJvMaskEdit;
    edHrI: TJvMaskEdit;
    edSerie: TJvEdit;
    JvDBNavigator1: TJvDBNavigator;
    Gauge1: TGauge;
    BitBtn1: TBitBtn;
    btRelatorio: TBitBtn;
    QCtrc: TADOQuery;
    navnavig: TDataSource;
    QOcorr: TADOQuery;
    QOcorrCODBAR: TStringField;
    QOcorrDSC_MENSAGEM: TStringField;
    QOcorrDAT_INC: TDateTimeField;
    QOcorrCOD_USUINC: TBCDField;
    QOcorrNOM_USUINC: TStringField;
    QOcorrCOD_MANIFESTO: TBCDField;
    QCtrcMANIFESTO: TFMTBCDField;
    QCtrcBOX: TStringField;
    QCtrcDATA_EXP: TDateTimeField;
    QCtrcROMANEIO: TStringField;
    QCtrcUSER_EXP: TStringField;
    QCtrcNM_CLIENTE: TStringField;
    JvDBGrid3: TJvDBGrid;
    Panel9: TPanel;
    QCtrcCONF_LIBERADO: TStringField;
    QExpedidosNR_NF: TStringField;
    QExpedidosMANIFESTO: TFMTBCDField;
    QExpedidosCODCON: TFMTBCDField;
    QExpedidosFL_EMPRESA: TFMTBCDField;
    QExpedidosRAZSOC: TStringField;
    QExpedidosCODCGC: TStringField;
    QExpedidosCTE_DATA: TDateTimeField;
    QExpedidosDATA_SAIDA: TDateTimeField;
    QExpedidosBOX: TStringField;
    QExpedidosINVOICE: TStringField;
    QExpedidosDATA_TRIP: TDateTimeField;
    QExpedidosCUST_NAME: TStringField;
    QExpedidosCUST_NUMBER: TStringField;
    QExpedidosTRIP: TStringField;
    QExpedidosDATAINC: TDateTimeField;
    QVolRecebidosDATA_BIP: TDateTimeField;
    procedure BTConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbclienteChange(Sender: TObject);
    procedure btconsultarexpClick(Sender: TObject);
    procedure BtConsultarPeClick(Sender: TObject);
    procedure ledClienteCloseUp(Sender: TObject);
    procedure ledClienteExit(Sender: TObject);
    procedure ledClientePenCloseUp(Sender: TObject);
    procedure ledClientePenExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDacteClick(Sender: TObject);
    procedure btnRelExpedidoClick(Sender: TObject);
    procedure btnRelRecClick(Sender: TObject);
    procedure btnExcluirPClick(Sender: TObject);
    procedure tsExpedirShow(Sender: TObject);
    procedure tsExpedirHide(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure QExpedirAfterOpen(DataSet: TDataSet);
    procedure btRelatorioClick(Sender: TObject);
    procedure QCtrcBeforeOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure QOcorrBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPainelControleVol: TfrmPainelControleVol;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmPainelControleVol.btconsultarexpClick(Sender: TObject);
begin
  Panel5.visible := true;
  Application.ProcessMessages;
  QExpedidos.Close;
  if (copy(dtinicialExp.Text, 1, 2) <> '  ') and
    (copy(dtfinalexp.Text, 1, 2) <> '  ') then
    QExpedidos.SQL[10] := 'and ct.cte_data between to_date(''' +
      dtinicialExp.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' +
      dtfinalexp.Text + ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QExpedidos.SQL[10] := '';

  if Trim(ledIdCliente.LookupValue) <> '' then
    QExpedidos.SQL[11] := 'and cli.codclifor = ' +
      QuotedStr(ledIdCliente.LookupValue)
  else
    QExpedidos.SQL[11] := '';

  if Trim(edBOX.Text) <> '' then
    QExpedidos.SQL[12] := 'and box = ' + QuotedStr(Trim(edBOX.Text))
  else
    QExpedidos.SQL[12] := '';

  if Trim(edTripE.Text) <> '' then
    QExpedidos.SQL[13] := 'and id_trip = ' + QuotedStr(Trim(edTripE.Text))
  else
    QExpedidos.SQL[13] := '';

  // showmessage(QExpedidos.SQL.Text);
  QExpedidos.Open;
  Panel5.visible := false;
end;

procedure TfrmPainelControleVol.BtConsultarPeClick(Sender: TObject);
//var    cnpjsp, cnpjsb, cnpjst, cnpjsa: string;
begin
  Panel4.visible := true;
  Application.ProcessMessages;
  qpendentes.Close;
  if (copy(dtinicialpe.Text, 1, 2) <> '  ') and
    (copy(dtfinalpe.Text, 1, 2) <> '  ') then
    qpendentes.SQL[4] := 'and data_trip between to_date(''' + dtinicialpe.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtfinalpe.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    qpendentes.SQL[4] := '';

  if Trim(ledIdClientePen.LookupValue) <> '' then
    qpendentes.SQL[5] := 'and cust_number = ' +
      QuotedStr(ledIdClientePen.LookupValue)
  else
    qpendentes.SQL[5] := '';

  if Trim(edBoxP.Text) <> '' then
    qpendentes.SQL[6] := 'and box = ' + QuotedStr(Trim(edBoxP.Text))
  else
    qpendentes.SQL[6] := '';

  if Trim(edTripP.Text) <> '' then
    qpendentes.SQL[7] := 'and trip||seq = ' + QuotedStr(Trim(edTripP.Text))
  else
    qpendentes.SQL[7] := '';

  // showmessage(qpendentes.sql.text);
  qpendentes.Open;
  Label15.caption := 'Quantidade de Box Pendentes : ' +
    IntToStr(qpendentes.RecordCount);

  Panel4.visible := false;
end;

procedure TfrmPainelControleVol.btnDacteClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volumes_pendentes.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].Text + '</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.Add('Relat�rio de Volumes da SKF Pendentes');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to DBGrid3.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + DBGrid3.Columns[i].Title.caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  qpendentes.First;
  while not qpendentes.Eof do
  begin
    for i := 0 to DBGrid3.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + qpendentes.fieldbyname
        (DBGrid3.Columns[i].FieldName).AsString + '</th>');
    qpendentes.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelControleVol.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  sarquivo := 'c:\sim\Volumes_a_expedir.html';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  for i := 0 to DBGrid4.Columns.Count - 1 do
  begin
    Memo1.Add('<th><FONT class=texto1>' + DBGrid4.Columns[i].Title.Caption
      + '</th>');
  end;
  Memo1.Add('</tr>');
  QExpedir.First;
  while not QExpedir.Eof do
  begin
    for i := 0 to DBGrid4.Columns.Count - 1 do
      Memo1.Add('<th><FONT class=texto2>' + QExpedir.fieldbyname
        (DBGrid4.Columns[i].FieldName).AsString + '</th>');
    QExpedir.Next;
    Memo1.Add('<tr>');
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  Memo1.Free;
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelControleVol.btnExcluirPClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Esta Pend�ncia ?',
    'Excluir Pend�ncia', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    try
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.clear;
      dtmDados.IQuery1.SQL.Add('update dashboard.skf_tb_trip set stat = -9 ');
      dtmDados.IQuery1.SQL.Add
        ('where trip||seq = :0 and box = :1 and invoice = :2 ');
      dtmDados.IQuery1.Parameters[0].value := qpendentesTRIP.AsString;
      dtmDados.IQuery1.Parameters[1].value := qpendentesBOX.AsString;
      dtmDados.IQuery1.Parameters[2].value := qpendentesINVOICE.AsString;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    except
      on e: Exception do
      begin
        showmessage(e.message);
      end;
    end;
    qpendentes.Close;
    qpendentes.Open;
  end;
end;

procedure TfrmPainelControleVol.btnRelExpedidoClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volumes_expedidos.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].Text + '</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.Add('Relat�rio de Volumes da SKF Expedidos');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to DBGrid2.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + DBGrid2.Columns[i].Title.caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QExpedidos.First;
  while not QExpedidos.Eof do
  begin
    for i := 0 to DBGrid2.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QExpedidos.fieldbyname
        (DBGrid2.Columns[i].FieldName).AsString + '</th>');
    QExpedidos.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelControleVol.btnRelRecClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Volumes_recebidos.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].Text + '</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.Add('Relat�rio de Volumes da SKF Recebidos');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to DBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + DBGrid1.Columns[i].Title.caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QVolRecebidos.First;
  while not QVolRecebidos.Eof do
  begin
    for i := 0 to DBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QVolRecebidos.fieldbyname
        (DBGrid1.Columns[i].FieldName).AsString + '</th>');
    QVolRecebidos.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelControleVol.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Panel9.Visible := true;
  Application.ProcessMessages;
  if RGTipo.ItemIndex = 0 then
  begin
    navnavig.DataSet := QCtrc;
    JvDBGrid3.Visible := false;
    JvDBGrid1.Visible := true;
    QCtrc.close;
    if edmanifesto.value > 0 then
      QCtrc.sql[12] := 'and i.manifesto = ' + QuotedStr(apcarac(edmanifesto.Text))
    else
      QCtrc.sql[12] := '';
    if copy(eddtini.Text, 1, 2) <> '  ' then
      QCtrc.sql[13] := 'and tp.datainc between to_date(''' + eddtini.Text
        + ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        eddtfim.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QCtrc.sql[13] := '';
    if alltrim(edVolume.Text) <> '' then
      QCtrc.sql[14] := 'and tp.box = ' + #39 + edVolume.Text + #39
    else
      QCtrc.sql[14] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QCtrc.sql[15] := 'and nm_cliente = ''' + ledCliente.LookupValue + ''''
    else
      QCtrc.sql[15] := '';
//showmessage(qctrc.SQL.Text);
    QCtrc.open;
  end
  else if RGTipo.ItemIndex = 1 then
  begin
    navnavig.DataSet := QOcorr;
    JvDBGrid1.Visible := false;
    JvDBGrid3.Visible := true;
    QOcorr.close;
    if edmanifesto.value > 0 then
      QOcorr.sql[3] := 'and lg.codman = ' + QuotedStr(apcarac(edmanifesto.Text))
    else
      QOcorr.sql[3] := '';
    if copy(eddtini.Text, 1, 2) <> '  ' then
      QOcorr.sql[4] := 'and lg.dat_inc between to_date(''' + eddtini.Text +
        ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        eddtfim.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QOcorr.sql[4] := '';
    if alltrim(edVolume.Text) <> '' then
      QOcorr.sql[5] := 'and lg.codbar = ' + #39 + edVolume.Text + #39
    else
      QOcorr.sql[5] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QOcorr.sql[6] := 'AND cl.nom_usuario = ''' + ledCliente.LookupValue + ''''
    else
      QOcorr.sql[6] := '';
    QOcorr.open;
  end
  else if RGTipo.ItemIndex = 2 then
  begin
    navnavig.DataSet := QCtrc;
    JvDBGrid3.Visible := false;
    JvDBGrid1.Visible := true;
    QCtrc.close;
    QCtrc.sql[10] := 'and data_exp is null ';
    if copy(dtInicial.Text, 1, 2) <> '  ' then
      QCtrc.sql[12] := 'and tp.datainc between to_date(''' + dtInicial.Text +
        ' ' + edHrI.Text + ''',''dd/mm/yy hh24:mi:ss'') and to_date(''' +
        dtFinal.Text + ' ' + edHrF.Text + ''',''dd/mm/yy hh24:mi:ss'')'
    else
      QCtrc.sql[12] := '';
    if Trim(ledCliente.LookupValue) <> '' then
      QCtrc.sql[13] := 'and nm_usuario = ''' + ledCliente.LookupValue + ''''
    else
      QCtrc.sql[13] := '';
    if alltrim(edVolume.Text) <> '' then
      QCtrc.sql[14] := 'and tp.box = ' + #39 + edVolume.Text + #39
    else
      QCtrc.sql[14] := '';
    if edmanifesto.value > 0 then
      QCtrc.sql[15] := 'and i.manifesto = ' + QuotedStr(apcarac(edmanifesto.Text))
    else
      QCtrc.sql[15] := '';
    QCtrc.open;
  end;
  Panel9.Visible := false;
end;

procedure TfrmPainelControleVol.BitBtn1Click(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  Memo1: TStringList;
begin
  if RGTipo.ItemIndex = 0 then
    sarquivo := DirectoryListBox1.Directory + '\Rel_coletor_expedicao.html'
  else
    sarquivo := DirectoryListBox1.Directory + '\Rel_ocorrencia_expedicao.html';
  Memo1 := TStringList.Create;
  Memo1.Add('  <HTML>');
  Memo1.Add('    <HEAD>');
  Memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  Memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  Memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  Memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  Memo1.Add('         </STYLE>');
  Memo1.Add('    </HEAD>');
  Memo1.Add('    <BODY <Font Color="#004080">');
  Memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  Memo1.Add('</th></font></Center>');
  Memo1.Add('</tr>');
  Memo1.Add('</B></font>');
  Memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  Memo1.Add('<TBODY>');
  Memo1.Add('<tr>');
  if (RGTipo.ItemIndex = 0) or (RGTipo.ItemIndex = 2) then
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
    begin
      Memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
        + '</th>');
    end;
    Memo1.Add('</tr>');
    QCtrc.First;
    while not QCtrc.Eof do
    begin
      for i := 0 to JvDBGrid1.Columns.Count - 1 do
        Memo1.Add('<th><FONT class=texto2>' + QCtrc.fieldbyname
          (JvDBGrid1.Columns[i].FieldName).AsString + '</th>');
      QCtrc.Next;
      Memo1.Add('<tr>');
    end;
  end
  else
  begin
    for i := 0 to JvDBGrid3.Columns.Count - 1 do
    begin
      Memo1.Add('<th><FONT class=texto1>' + JvDBGrid3.Columns[i].Title.Caption
        + '</th>');
    end;
    Memo1.Add('</tr>');
    QOcorr.First;
    while not QOcorr.Eof do
    begin
      for i := 0 to JvDBGrid3.Columns.Count - 1 do
        Memo1.Add('<th><FONT class=texto2>' + QOcorr.fieldbyname
          (JvDBGrid3.Columns[i].FieldName).AsString + '</th>');
      QOcorr.Next;
      Memo1.Add('<tr>');
    end;
  end;
  Memo1.Add('</TBODY>');
  Memo1.Add('</table>');
  Memo1.SaveToFile(sarquivo);
  Memo1.Free;
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
end;

procedure TfrmPainelControleVol.BTConsultarClick(Sender: TObject);
begin
  Panel6.visible := true;
  Application.ProcessMessages;
  QVolRecebidos.Close;
  if (copy(dtinicial.Text, 1, 2) <> '  ') and (copy(dtfinal.Text, 1, 2) <> '  ')
  then
    QVolRecebidos.SQL[3] := 'and datainc between to_date(''' + dtinicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtfinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QVolRecebidos.SQL[3] := '';

  if (cbcliente.Text <> '') and (cbcnpj.Text <> '') then
    QVolRecebidos.SQL[4] := 'and cust_name = ' + QuotedStr(cbcliente.Text) +
      ' and cust_number = ' + QuotedStr(cbcnpj.Text) + ''
  else
    QVolRecebidos.SQL[4] := '';

  if Trim(edBoxR.Text) <> '' then
    QVolRecebidos.SQL[5] := 'and box = ' + QuotedStr(Trim(edBoxR.Text))
  else
    QVolRecebidos.SQL[5] := '';

  if Trim(edTripR.Text) <> '' then
    QVolRecebidos.SQL[6] := 'and (trip||seq) = ' + QuotedStr(Trim(edTripR.Text))
  else
    QVolRecebidos.SQL[6] := '';

  QVolRecebidos.Open;
  Panel6.visible := false;
end;

procedure TfrmPainelControleVol.cbclienteChange(Sender: TObject);
begin
  if cbcliente.Text = '' then
  begin
    cbcnpj.Items.clear;
  end
  else
  begin
    cbcnpj.Items.clear;
    Qcnpj.Close;
    Qcnpj.SQL.clear;
    Qcnpj.SQL.Add('select distinct(CUST_NAME) cliente, cust_number' + #13 +
      'from dashboard.skf_tb_trip ' + #13 + 'where stat = 3 and cust_name = ' +
      QuotedStr(cbcliente.Text) +
      ' and cust_name <> ''0000000000'' order by cliente');
    Qcnpj.Open;
    Qcnpj.Active := true;

    while not Qcnpj.Eof do
    begin
      cbcnpj.Items.Add(QcnpjCUST_NUMBER.AsString);
      Qcnpj.Next;
    end;
    Qcnpj.Close;
  end;
  cbcnpj.ItemIndex := 0;
end;

procedure TfrmPainelControleVol.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QClienteExp.Close;
end;

procedure TfrmPainelControleVol.FormCreate(Sender: TObject);
begin
  PageControl1.TabIndex := 0;
  cbcliente.Items.clear;
  cbcliente.Items.Add('');
  Qcliente.Open;
  while not Qcliente.Eof do
  begin
    cbcliente.Items.Add(QclienteCLIENTE.AsString);
    Qcliente.Next;
  end;
  Qcliente.Close;

  QClienteExp.Open;
end;

procedure TfrmPainelControleVol.ledClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmPainelControleVol.ledClienteExit(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmPainelControleVol.ledClientePenCloseUp(Sender: TObject);
begin
  ledIdClientePen.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledClientePen.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmPainelControleVol.ledClientePenExit(Sender: TObject);
begin
  ledIdClientePen.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledClientePen.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmPainelControleVol.QCtrcBeforeOpen(DataSet: TDataSet);
begin
  QCtrc.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelControleVol.QExpedirAfterOpen(DataSet: TDataSet);
begin
  if QExpedir.RecordCount > 0 then
    lblVolumes.caption := IntToStr(QExpedir.RecordCount) + ' Volumes'
  else
    lblVolumes.caption := '';
end;

procedure TfrmPainelControleVol.QOcorrBeforeOpen(DataSet: TDataSet);
begin
  QOcorr.Parameters[0].Value := GLBFilial;
end;

procedure TfrmPainelControleVol.tsExpedirHide(Sender: TObject);
begin
  QExpedir.Close;
end;

procedure TfrmPainelControleVol.tsExpedirShow(Sender: TObject);
begin
  QExpedir.Open;
end;

end.
