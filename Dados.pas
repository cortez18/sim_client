unit Dados;

interface

uses
  SysUtils, Classes, DB, ADODB, ImgList, Controls, IniFiles, Forms, Dialogs,
  ActnMan, ActnColorMaps, System.ImageList;

type
  TdtmDados = class(TDataModule)
    IMBotoes: TImageList;
    XPColorMap1: TXPColorMap;
    ConRodopar: TADOConnection;
    RQuery1: TADOQuery;
    ConWms: TADOConnection;
    WQuery1: TADOQuery;
    Query2: TADOQuery;
    ConIW: TADOConnection;
    Query1: TADOQuery;
    ConIntecom: TADOConnection;
    IQuery1: TADOQuery;
    QryAcesso: TADOQuery;
    ConWmsWeb: TADOConnection;
    WWQuery1: TADOQuery;
    iml: TImageList;
    QBase: TADOQuery;
    QBasesenha: TStringField;
    ADOConnection1: TADOConnection;
    ConWMSV11: TADOConnection;
    WMSV11Query: TADOQuery;
    ADOConnection2: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    function PodeAlterar(Tela: String): Boolean;
    function PodeApagar(Tela: String): Boolean;
    function PodeInserir(Tela: String): Boolean;
    function PodeVisualizar(Tela: String): Boolean;
  private
    { Private declarations }
  public

  end;

var
  dtmDados: TdtmDados;

implementation

uses Menu, funcoes;

{$R *.dfm}

procedure TdtmDados.DataModuleCreate(Sender: TObject);
var
  text, a, b: string;
  Ini: TIniFile;
  F: TextFile;
begin
  ConRodopar.Connected := false;
  ConWms.Connected := false;
  ConIntecom.Connected := false;
  ConWmsWeb.Connected := false;

  AssignFile(F, '\\192.168.236.112\tns_admin\config.ini');
  Reset(F);
  Readln(F, a);
  Closefile(F);
  Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
  dts := Ini.ReadString('DADOS', 'cami', '');

  if dts = 'INTECDSV' then
  begin
    ConIntecom.ConnectionString := ConIntecom.ConnectionString + ';' +
      'Password= ' + ReverteSenha(a) + ';Data Source=' + dts;
    ConIntecom.Connected := true;
    try
      QBase.Close;
      QBase.Parameters[0].Value := 'WMSWEB';
      QBase.Parameters[1].Value := 'WMS_CUSTOM';
      QBase.Open;
      ConWmsWeb.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
        QBasesenha.AsString +
        ';Persist Security Info=True;User ID=wms_custom;Data Source=wmsweb;Extended Properties="" ';
      ConWmsWeb.Connected := true;
    except
      showMessage('N�o conseguiu acessar a Base WMSWEB');
    end;

    // N]�o consegue acessar em dsv
   { try
      QBase.Close;
      QBase.Parameters[0].Value := 'WMSWBHML';
      QBase.Parameters[1].Value := 'CUSTOM_12_0';
      QBase.Open;
      ConWMSV11.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
        QBasesenha.AsString +
        ';Persist Security Info=True;User ID=CUSTOM_12_0;Data Source=WMSWBHML' +
        ';Extended Properties="" ';
      ConWMSV11.Connected := true;
    except
      showMessage('N�o conseguiu acessar a Base WMSWBHML');
    end;
    }
    try
      QBase.Close;
      QBase.Parameters[0].Value := dts;
      QBase.Parameters[1].Value := 'WMS';
      QBase.Open;
      ConWms.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
        QBasesenha.AsString +
        ';Persist Security Info=True;User ID=wms;Data Source=' + dts +
        ';Extended Properties="" ';
      ConWms.Connected := true;
    except
      showMessage('N�o conseguiu acessar a Base WMS');
    end;

    try
      QBase.Close;
      QBase.Parameters[0].Value := dts;
      QBase.Parameters[1].Value := 'CYBER';
      QBase.Open;
      ConRodopar.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
        QBasesenha.AsString +
        ';Persist Security Info=True;User ID=CYBER;Data Source=' + dts +
        ';Extended Properties="" ';
      ConRodopar.Connected := true;
    except
      showMessage('N�o conseguiu acessar a Base CYBER');
    end;

    try
      QBase.Close;
      ConIW.ConnectionString := ConIntecom.ConnectionString +
        ';Data Source=' + dts;
      ConIW.Connected := true;
    except
      showMessage('N�o conseguiu acessar a Base IW');
    end;
  end
  else
  begin
    ConIntecom.ConnectionString := ConIntecom.ConnectionString + ';' +
      'Password= ' + ReverteSenha(a) + ';Data Source=' + dts;
    ConIntecom.Connected := true;
    // showmessage('Conectou Intecom');
    QBase.Close;
    QBase.Parameters[0].Value := dts;
    QBase.Parameters[1].Value := 'WMS';
    QBase.Open;
    ConWms.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
      QBasesenha.AsString +
      ';Persist Security Info=True;User ID=wms;Data Source=' + dts +
      ';Extended Properties="" ';
    ConWms.Connected := true;
    // showmessage('Conectou WMS');
    QBase.Close;
    QBase.Parameters[0].Value := dts;
    QBase.Parameters[1].Value := 'CYBER';
    QBase.Open;
    ConRodopar.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
      QBasesenha.AsString +
      ';Persist Security Info=True;User ID=CYBER;Data Source=' + dts +
      ';Extended Properties="" ';
    ConRodopar.Connected := true;
    // showmessage('Conectou Cyber');
    QBase.Close;
    ConIW.ConnectionString := ConIW.ConnectionString + ';Data Source=' + dts;
    ConIW.Connected := true;
    // showmessage('Conectou IW');
    QBase.Close;
    QBase.Parameters[0].Value := 'WMSWEBPRD';
    QBase.Parameters[1].Value := 'WMS_CUSTOM';
    QBase.Open;
    ConWmsWeb.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
      QBasesenha.AsString +
      ';Persist Security Info=True;User ID=wms_custom;Data Source=wmswebprd;Extended Properties="" ';
    ConWmsWeb.Connected := true;

    QBase.Close;
    QBase.Parameters[0].Value := 'WMSWEBPRD';
    QBase.Parameters[1].Value := 'CUSTOM_12_0';
    QBase.Open;
    ConWMSV11.ConnectionString := 'Provider=OraOLEDB.Oracle.1;Password=' +
      QBasesenha.AsString +
      ';Persist Security Info=True;User ID=CUSTOM_12_0;Data Source=wmswebprd;Extended Properties="" ';
    ConWMSV11.Connected := true;
    // showmessage('Conectou WMSWEB');
  end;
  glbimagem := Ini.ReadString('DADOS', 'imagem', '');
  Ini.Free;
end;

procedure TdtmDados.DataModuleDestroy(Sender: TObject);
begin
  ConRodopar.Connected := false;
  ConWms.Connected := false;
end;

function TdtmDados.PodeAlterar(Tela: String): Boolean;
begin
  // Verifica se o usu�rio pode alterar algum registro
  QryAcesso.Close;
  QryAcesso.SQL.clear;
  QryAcesso.SQL.Add
    ('SELECT A.*, T.DESCRICAO FROM tb_PERMISSAO_sis A, tb_TELA_sis T');
  QryAcesso.SQL.Add('WHERE A.COD_usuario= ' + #39 + IntToStr(GLBCodUser) + #39);
  QryAcesso.SQL.Add('AND T.IDTELA = A.IDTELA ');
  QryAcesso.SQL.Add('AND upper(T.NAME) = upper(' + #39 + Tela + #39 + ')');
  QryAcesso.Open;
  Result := QryAcesso.FieldByName('ALTERAR').AsInteger = 1;
end;

function TdtmDados.PodeApagar(Tela: String): Boolean;
begin
  // Verifica se o usu�rio pode apagar algum registro
  QryAcesso.Close;
  QryAcesso.SQL.clear;
  QryAcesso.SQL.Add
    ('SELECT A.*, T.DESCRICAO FROM tb_PERMISSAO_sis A, tb_TELA_sis T ');
  QryAcesso.SQL.Add('WHERE A.COD_usuario= ' + #39 + IntToStr(GLBCodUser) + #39);
  QryAcesso.SQL.Add('AND T.IDTELA = A.IDTELA ');
  QryAcesso.SQL.Add('AND upper(T.NAME) = upper(' + #39 + Tela + #39 + ')');
  QryAcesso.Open;
  Result := QryAcesso.FieldByName('APAGAR').AsInteger = 1;
end;

function TdtmDados.PodeInserir(Tela: String): Boolean;
begin
  // Verifica se o usu�rio pode inserir algum registro
  QryAcesso.Close;
  QryAcesso.SQL.clear;
  QryAcesso.SQL.Add
    ('SELECT A.*, T.DESCRICAO FROM tb_PERMISSAO_sis A, tb_TELA_sis T');
  QryAcesso.SQL.Add('WHERE A.COD_usuario= ' + #39 + IntToStr(GLBCodUser) + #39);
  QryAcesso.SQL.Add('AND T.IDTELA = A.IDTELA ');
  QryAcesso.SQL.Add('AND upper(T.NAME) = upper(' + #39 + Tela + #39 + ')');
  QryAcesso.Open;
  Result := QryAcesso.FieldByName('INSERIR').AsInteger = 1;
end;

function TdtmDados.PodeVisualizar(Tela: String): Boolean;
begin
  // Verifica se o usu�rio pode visualizar a tela
  QryAcesso.Close;
  QryAcesso.SQL.clear;
  QryAcesso.SQL.Add
    ('SELECT A.*, T.DESCRICAO FROM tb_PERMISSAO_sis A, tb_TELA_sis T ');
  QryAcesso.SQL.Add('WHERE A.COD_usuario= ' + #39 + IntToStr(GLBCodUser) + #39);
  QryAcesso.SQL.Add('AND T.IDTELA = A.IDTELA ');
  QryAcesso.SQL.Add('AND upper(T.NAME) = upper(' + #39 + Tela + #39 + ')');
  QryAcesso.Open;
  Result := QryAcesso.FieldByName('VISUALIZAR').AsInteger = 1;
end;

end.
