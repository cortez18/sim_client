unit funcoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, MAPI, Registry, Inifiles, ComObj, Types, StrUtils, Math,
  IdMessage, IdCustomTCPServer, IdTCPServer, IdCmdTCPServer,
  IdExplicitTLSClientServerBase, IdPOP3Server, IdPOP3, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTPBase, IdSMTP,
  IdAttachmentFile, IdMessageParts, IdEMailAddress;

  function Arredondar(Valor: Double; Dec: Integer): Double;
  function CalculaCnpjCpf(Numero : String) : String;
  function ApCarac (sTexto: String):String;
  function IIf(pCond:Boolean;pTrue,pFalse:Variant): Variant;
  function Alltrim(const Search: string): string;
  function Envia_Email(sEmail, sAnexo, sAssunto, sArq: String): String;
  function RetZero(ZEROS:string;QUANT:integer):String;
  function RetZeroD(ZEROS:string;QUANT:integer):String;
  function RetBran(ZEROS:string;QUANT:integer):String;
  function RetBranE(ZEROS:string;QUANT:integer):String;
  function Getcomputer : string;
  function BuscaTroca(Text,Busca,Troca : string) : string;
  function RetAno : string;
  function InfoSist:string;
  function Replicate(Caracter: string; Quant: integer): string;
  function BuscaDireita(Busca, Text: string): integer;
  function StrZeroStr(Numero: String; Total, Decimal: integer): string;
  function Buscapalavra(busca,Text : string) : integer;
  Function Diferenca_Horas(Hora_Final:TTime;Hora_Inicial:TTime):String;
  function InserirValor(Mensagem, Caption: String): String;
  function ValidaData(StrD: string): Boolean;
  Function RetornaTexto(Texto:String; Caracter:String):String;
  function ExisteInt(Texto:String): Boolean;
  Function SomaMes (dData : TDateTime; xMeses : Integer) : TDateTime;
  function SendEMail(Handle: THandle; Mail: TStrings): Cardinal;
  function SoNumero(s : string) : Extended;
  function SoCaracter(s : string) : string;
  function Busca(Busca, Text: string; Inicio:integer): integer;
  function ContaCarac(sTexto,busca: String):integer;
  function RemoveChar(Const Texto:String):String;
  function SubData(DataI,DataF: TDate) : String;
  function StringToFloat(s : string) : Extended;
  function StrIsInteger(const S: string): boolean;
  function RemoveAcentos(Str:String): String;
  function tbFileSize(const FileName: string): integer;
  Function FormataCGC(CGC : string): string;
  function GravaLog(form:String; regi:String; acao:String):string;
  function DataHoje(Data: TDate) : String;
  function codbarra(d : integer) : String;
  procedure ByteArrayToFile(const ByteArray : TByteDynArray; const FileName : string );
  function FileToByteArray( const FileName : string ) : TByteDynArray;
  function Ajustaaltura(altura : integer) : Integer;
  function LocalizaArray(fArrayDinamicoString : array of String; fValor : String) : Boolean;
  function tamanho(sTexto: String):integer;
  function StrFloatUSA(S: String): String;
  function regiao_Bloq(UF: String): String;
  function TestaParaPar(TestaInteiro : Integer) : boolean;
  Function ConsisteSenha(senha: String) :String;
  Function ReverteSenha(senha: String) :String;
  function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
  function EncodeBase64(S: string): string;
  function DecodeBase64(S: string): string;
  function ApAspas (sTexto: String):String;
implementation

uses menu;
//uses dados, DB;

const
  Codes64 = '0A1B2C3D4E5F6G7H89IjKlMnOPqRsTuVWXyZabcdefghijkLmNopQrStUvwxYz+/';
  C1 = 52845;
  C2 = 22719;

 type TChars = set of Char;


function Arredondar(Valor: Double; Dec: Integer): Double;
var Valor1, Numero1, Numero2, Numero3: Double;
begin
  Valor1:=Exp(Ln(10) * (Dec + 1));
  Numero1:=Int(Valor * Valor1);
  Numero2:=(Numero1 / 10);
  Numero3:=Round(Numero2);
  Result:=(Numero3 / (Exp(Ln(10) * Dec)));
end;

function CalculaCnpjCpf(Numero : String) : String;
Var i,d,b,Digito : Byte;
    Soma : Integer;
    CNPJ : Boolean;
    DgPass,DgCalc : String;
begin
  Result := numero;
  // Numero := ApenasNumerosStr(Numero);
  // Caso o n�mero n�o seja 11 (CPF) ou 14 (CNPJ), aborta
  Case Length(Numero) of
    11: CNPJ := False;
    15: CNPJ := True;
  else Exit;
  end;
  // Separa o n�mero do digito
  DgCalc := '';
  DgPass := Copy(Numero,Length(Numero)-1,2);
  Numero := Copy(Numero,1,Length(Numero)-2);
  // Calcula o digito 1 e 2
  For d := 1 to 2 do begin
    B := IIF(D=1,2,3); // BYTE
    SOMA := IIF(D=1,0,STRTOINTDEF(DGCALC,0)*2);
    for i := Length(Numero) downto 1 do begin
      Soma := Soma + (Ord(Numero[I])-Ord('0'))*b;
      Inc(b);
      If (b > 9) And CNPJ Then
        b := 2;
    end;
   Digito := 11 - Soma mod 11;
   If Digito >= 10 then
     Digito := 0;
   DgCalc := DgCalc + Chr(Digito + Ord('0'));
  end;
  if DgPass = DgCalc then
    result := 'OK'
   else
    Result := 'N';
end;

function IIf(pCond:Boolean;pTrue,pFalse:Variant): Variant;
begin
  If pCond Then Result := pTrue
  else Result := pFalse;
end;

function ApCarac(sTexto: String):String;
var nPos : Integer;
    carc : String;
begin
  Result := '';
  for nPos := 1 to Length(sTexto) do
  begin
    carc := Copy(sTexto,nPos,1);
    if (Copy(sTexto,nPos,1) = '/')  or (Copy(sTexto,nPos,1) = ' ') or (Copy(sTexto,nPos,1) = '.') or
       (Copy(sTexto,nPos,1) = #39) or (Copy(sTexto,nPos,1) = '-') or (Copy(sTexto,nPos,1) = ',') or
       (Copy(sTexto,nPos,1) = '�') or (Copy(sTexto,nPos,1) = '&') or (Copy(sTexto,nPos,1) = ':') or
       (Copy(sTexto,nPos,1) = '+')  then
      Delete(sTexto,nPos,1);
  end;
  Result := sTexto;
end;

function Alltrim(const Search: string): string; {Remove os espa�os em branco de ambos os lados da string}
const
BlackSpace = [#33..#126];
var
Index: byte;
begin
Index:=1;
while (Index <= Length(Search)) and not (Search[Index] in BlackSpace) do
  begin
  Index:=Index + 1;
  end;
Result:=Copy(Search, Index, 255);
Index := Length(Result);
while (Index > 0) and not (Result[Index] in BlackSpace) do
  begin
  Index:=Index - 1;
  end;
Result := Copy(Result, 1, Index);
end;

function EnDecryptString(StrValue : String; Chave: Word) : String;
var  I: Integer;
     OutValue : String;
begin
  OutValue := '';
  for I := 1 to Length(StrValue) do
     OutValue := OutValue + char(Not(ord(StrValue[I])-Chave));
  Result := OutValue;
end;

function Envia_Email(sEmail, sAnexo, sAssunto, sArq: String) : String;
var IdMessage1: TIdMessage;
    IdSMTP1: TIdSMTP;
    Addressee: TIdEmailAddressItem;
    Destinat�rio: TIdEmailAddressItem;
    Attachment: TIdAttachmentFile;
    ListaEmail, Email : TStringList;
    i, j : integer;
begin
  IdMessage1 := TIdMessage.Create(nil);
  IdMessage1.ContentType := 'text/html';
  IdMessage1.CharSet := 'ISO-8859-1';
  IdSMTP1 := TIdSMTP.Create(nil);
  IdSMTP1.Host := 'mail.iwlogistica.com.br';
  IdMessage1.From.Text := 'iwlogistica@iwlogistica.com.br' ;
  //IdMessage1.From.Name := 'Gest�o de Fretes';
  IdSMTP1.Port := 587;
  //
  Addressee := IdMessage1.Recipients.Add;
  Addressee.Address := sEmail;
  Addressee.Name := '' ;
  //
  IdSMTP1.Username := 'iwlogistica@iwlogistica.com.br'; // SMTP user name IdSMTP1.Username: = 'frankborland'; nome de usu�rio / / SMTP
  IdSMTP1.Password := 'iwlog@2014'; // SMTP user password IdSMTP1.Password: = '1234notgonnatellya '/ / senha do usu�rio SMTP
  //
  IdMessage1.Subject := sAssunto;
  IdSMTP1.Connect;
  IdSMTP1.Send(IdMessage1);
  IdSMTP1.Disconnect;
  //
  Attachment.Free;
  IdMessage1.Free;
  IdSMTP1.Free;
end;


function RetZero(ZEROS:string;QUANT:integer):String;
var I,Tamanho:integer;
    aux: string;
begin
  aux:=zeros;
  Tamanho:=length(ZEROS);
  ZEROS:='';
  for I:=1 to quant-tamanho do
    ZEROS:=ZEROS+'0';
  aux:=zeros+aux;
  RetZero:=aux;
end;

function RetZeroD(ZEROS:string;QUANT:integer):String;
var I,Tamanho:integer;
    aux: string;
begin
  aux:=alltrim(zeros);
  Tamanho:=length(aux);
  ZEROS:='';
  for I:=1 to quant-tamanho do
    ZEROS:=ZEROS+'0';
  aux:=aux+zeros;
  RetZeroD:=aux;
end;

function RetBran(ZEROS:string;QUANT:integer):String;
var I,Tamanho:integer;
    aux: string;
begin
  aux:=zeros;
  Tamanho:=length(ZEROS);
  ZEROS:='';
  for I:=1 to quant-tamanho do
    ZEROS:=' '+zeros;
  aux:=aux+zeros;
  RetBran:=aux;
end;

function RetBranE(ZEROS:string;QUANT:integer):String;
var I,Tamanho:integer;
    aux: string;
begin
  aux:=zeros;
  Tamanho:=length(ZEROS);
  ZEROS:='';
  for I:=1 to quant-tamanho do
    ZEROS:=' '+zeros;
  aux:=zeros+aux;
  RetBranE:=aux;
end;


function Getcomputer : string;
var registro : tregistry;
begin
  registro:=tregistry.create;
  registro.RootKey:=HKEY_LOCAL_MACHINE;
  registro.openkey('System\ControlSet001\Control\ComputerName\ComputerName',false);
  result:=registro.readstring('ComputerName');
end;

function BuscaTroca(Text,Busca,Troca : string) : string;
var n,b : integer;
begin
  b := length(Busca);
  for n := 1 to length(Text) do
  begin
    if Copy(Text,n,b) = Busca then
    begin
      Delete(Text,n,b);
      Insert(Troca,Text,n);
    end;
  end;
  Result := Text;
end;

function RetAno : string;
var Year, Month, Day: Word;
begin
  DecodeDate(now, Year, Month, Day);
  result := IntToStr(Year)
end;

function InfoSist:string;
var VerInfoSize: DWORD;
    VerInfo: Pointer;
    VerValueSize: DWORD;
    VerValue: PVSFixedFileInfo;
    Dummy: DWORD;
    V1, V2, V3, V4: Word;
    Prog : string;
begin
  Prog := Application.Exename;
  VerInfoSize := GetFileVersionInfoSize(PChar(prog), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(prog), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
    begin
      V1 := dwFileVersionMS shr 16;
      V2 := dwFileVersionMS and $FFFF;
      V3 := dwFileVersionLS shr 16;
      V4 := dwFileVersionLS and $FFFF;
    end;
  FreeMem(VerInfo, VerInfoSize);
  result := Copy (IntToStr (100 + v1), 3, 2) + '.' +
    Copy (IntToStr (100 + v2), 3, 2) + '.' +
    Copy (IntToStr (100 + v3), 3, 2) + '.' +
    Copy (IntToStr (100 + v4), 3, 2);
end;

function Replicate(Caracter: string; Quant: integer): string;
var I: integer;
begin
  Result := '';
  for I := 1 to Quant do
    Result := Result + Caracter;
end;

function BuscaDireita(Busca, Text: string): integer;
{Pesquisa um caractere � direita da string,
 retornando sua posi��o}
var
  n, retorno: integer;
begin
  retorno := 0;
  for n := length(Text) downto 1 do
  begin
    if Copy(Text, n, 1) = Busca then
    begin
      retorno := n;
      break;
    end;
  end;
  Result := retorno;
end;

function StrZeroStr(Numero: String; Total, Decimal: integer): string;
  {Insere Zeros e decimais � frente de uma string}
var
  TempStr: string;
begin
  //Str(Numero: 0: Decimal, TempStr);
  TempStr := Numero;
  while length(TempStr) < Total do
  begin
    Insert('0', TempStr, 1);
  end;
  Result := TempStr;
end;

function BuscaPalavra(Busca, Text: string): integer;
var tp, n, retorno, a: integer;
begin
  retorno := 0;
  tp := length(Text);
  a := 1;
  for n := length(Busca) downto 1 do
  begin
    //if Copy(Text, n, tp) = Busca then
    if Copy(Busca, a, tp) = Text then
    begin
      retorno := n;
      break;
    end
    else
      a := a + 1;
  end;
  Result := retorno;
end;

Function Diferenca_Horas(Hora_Final:TTime;Hora_Inicial:TTime):String;
Var  M,H,M1,M2,H1,H2 : Real;
     T1,T2 : String;
Label Passa;
begin
  T1 := TimeToStr(Hora_Final);
  T2 := TimeToStr(Hora_Inicial);
  if (T1 <> '00:00:00') and (T2 <> '00:00:00') then
  begin
    M1 := StrToFloat(T1[4] + T1[5]);
    M2 := StrToFloat(T2[4] + T2[5]);
    if M1 = 0 then
    begin
      M := 60 - M2;
      Goto Passa;
    end;
    if M1 > M2 then
      M  := M1 - M2
    else
      M  := M2 - M1;
    Passa:

    if M < 0 then
      M := 60 - M;

    if M > 60 then
      M := 60 - M;

    if M = 60 then
      M := 0;

    H1 := StrToFloat(T1[1] + T1[2]);
    H2 := StrToFloat(T2[1] + T2[2]);
    H  := H1 - H2;
 //   if H1 <> H2 then
 //     H  := H - A;

    Result := FloatToStr(H) + ':' + FloatToStr(M);
  end
  else
    Result := '00:00';
end;

function InserirValor(Mensagem, Caption: String): String;
begin
   Application.ProcessMessages;
   InputQuery(Caption, Mensagem, Result);
end;

function ValidaData(StrD: string): Boolean;
begin
  if length(StrD) > 0 then
  begin
    Result := true;
    try
      StrToDate(StrD);
    except
      Result:=False;
    end;
  end;
end;

Function RetornaTexto(Texto:String; Caracter:String):String;
var
  I,Posicao1:Integer;
  TextoInvertido:String;
begin
  Result:='';
  for I := Length(Texto) downto 1 do
  begin
    TextoInvertido:=TextoInvertido+Texto[I]
  end;
  Posicao1:=Pos(Caracter,Texto);
  Result:=IntToStr(Posicao1);
 end;

Function ExisteInt(Texto:String): Boolean;
{Testa se em uma string existe um numero inteiro valido ou n�o}
var i:integer;
begin
  try
    i := StrToInt(Texto);
    Result := True;
  except
    Result := False;
  end;
end;

Function SomaMes (dData : TDateTime; xMeses : Integer) : TDateTime;
var Ano, Mes, Dia : word;
    DataAux : TDateTime;
begin
  DecodeDate(dData, Ano, Mes, Dia);
  Mes := Mes + xMeses;
  Ano := Ano + (Mes DIV 12);
  Mes := Mes mod 12;
  DataAux := EnCodeDate(Ano, Mes, Dia);
  SomaMes := DataAux;
end;

function SendEMail(Handle: THandle; Mail: TStrings):Cardinal;
type
  TAttachAccessArray = array [0..0] of TMapiFileDesc;
  PAttachAccessArray = ^TAttachAccessArray;
var
  MapiMessage: TMapiMessage;
  Receip: TMapiRecipDesc;
  Attachments: PAttachAccessArray;
  AttachCount: Integer;
  i1: integer;
  FileName: string;
  dwRet: Cardinal;
  MAPI_Session: Cardinal;
  WndList: Pointer;
begin
  dwRet := MapiLogon(Handle,PAnsiChar(''),PAnsiChar(''),MAPI_LOGON_UI or MAPI_NEW_SESSION,0, @MAPI_Session);
  if (dwRet <> SUCCESS_SUCCESS) then
  begin
    MessageBox(Handle,
    PChar('Erro tentando enviar o e-mail'),
    PChar('Error'),
    MB_ICONERROR or MB_OK);
  end
  else
  begin
    FillChar(MapiMessage, SizeOf(MapiMessage), #0);
    Attachments := nil;
    FillChar(Receip, SizeOf(Receip), #0);

    if Mail.Values['to'] <> '' then
    begin
      Receip.ulReserved := 0;
      Receip.ulRecipClass := MAPI_TO;
      Receip.lpszName :=
      StrNew(PAnsiChar(Mail.Values['to']));
      Receip.lpszAddress := StrNew(PAnsiChar('SMTP:' +
      Mail.Values['to']));
      Receip.ulEIDSize := 0;
      MapiMessage.nRecipCount := 1;
      MapiMessage.lpRecips := @Receip;
    end;
    AttachCount := 0;
    for i1 := 0 to MaxInt do
    begin
      if Mail.Values['attachment' + IntToStr(i1)] = '' then
        break;
      Inc(AttachCount);
    end;
    if AttachCount > 0 then
    begin
      GetMem(Attachments, SizeOf(TMapiFileDesc) * AttachCount);
      for i1 := 0 to AttachCount - 1 do
      begin
        FileName := Mail.Values['attachment' + IntToStr(i1)];
        Attachments[i1].ulReserved := 0;
        Attachments[i1].flFlags := 0;
        Attachments[i1].nPosition := ULONG($FFFFFFFF);
        Attachments[i1].lpszPathName := StrNew(PAnsiChar(FileName));
        Attachments[i1].lpszFileName := StrNew(PAnsiChar(ExtractFileName(FileName)));
        Attachments[i1].lpFileType := nil;
      end;
      MapiMessage.nFileCount := AttachCount;
      MapiMessage.lpFiles := @Attachments^;
    end;
    if Mail.Values['subject'] <> '' then
      MapiMessage.lpszSubject := StrNew(PAnsiChar(Mail.Values['subject']));

    if Mail.Values['body'] <> '' then
      MapiMessage.lpszNoteText := StrNew(PAnsiChar(Mail.Values['body']));

    WndList := DisableTaskWindows(0);

    try
      Result := MapiSendMail(MAPI_Session, Handle,MapiMessage, MAPI_DIALOG, 0);

    finally

     EnableTaskWindows( WndList );

    end;
    for i1 := 0 to AttachCount - 1 do
    begin
      StrDispose(Attachments[i1].lpszPathName);
      StrDispose(Attachments[i1].lpszFileName);
    end;

    if Assigned(MapiMessage.lpszSubject) then
      StrDispose(MapiMessage.lpszSubject);

    if Assigned(MapiMessage.lpszNoteText) then
      StrDispose(MapiMessage.lpszNoteText);

    if Assigned(Receip.lpszAddress) then
      StrDispose(Receip.lpszAddress);

    if Assigned(Receip.lpszName) then
      strDispose(Receip.lpszName);

    MapiLogOff(MAPI_Session, Handle, 0, 0);

  end;

end;

function SoNumero(s : string) : Extended;
{ Filtra uma string qualquer, convertendo as suas partes
  num�ricas para sua representa��o decimal, por exemplo:
  'R$ 1.200,00' para 1200,00 '1AB34TZ' para 134}
var i :Integer;
    t : string;
    SeenDecimal,SeenSgn : Boolean;
begin
  t := '';
  SeenDecimal := False;
  SeenSgn := False;
  {Percorre os caracteres da string:}
  for i := Length(s) downto 0 do
  {Filtra a string, aceitando somente n�meros e separador decimal:}
  if (s[i] in ['0'..'9', '-','+',FormatSettings.DecimalSeparator]) then
  begin
    if (s[i] = FormatSettings.DecimalSeparator) and (not SeenDecimal) then
    begin
      t := s[i] + t;
      SeenDecimal := True;
    end
    else if (s[i] in ['+','-']) and (not SeenSgn) and (i = 1) then
    begin
      t := s[i] + t;
      SeenSgn := True;
    end
    else if s[i] in ['0'..'9'] then
    begin
      t := s[i] + t;
    end;
  end;
  Result := StrToFloat(t);
end;

function RemoveChar(Const Texto:String):String;
//
// Remove caracteres de uma string deixando apenas numeros 
// 
var I: integer;
    S: string;
begin
  S := '';
  for I := 1 To Length(Texto) Do
  begin
    if (Texto[I] in ['0'..'9']) then
    begin
      S := S + Copy(Texto, I, 1);
    end;
  end;
  result := S;
end;

function SoCaracter(s : string) : string;
var i :Integer;
    t : string;
    SeenDecimal,SeenSgn : Boolean;
begin
  t := '';
  SeenDecimal := False;
  SeenSgn := False;
  {Percorre os caracteres da string:}
  for i := Length(s) downto 0 do
  {Filtra a string, aceitando somente caracteres}
  if (s[i] in ['a'..'z','A'..'Z', FormatSettings.DecimalSeparator]) then
  begin
    if (s[i] = FormatSettings.DecimalSeparator) and (not SeenDecimal) then
    begin
      t := s[i] + t;
      SeenDecimal := True;
    end
    else if (s[i] in ['+','-']) and (not SeenSgn) and (i = 1) then
    begin
      t := s[i] + t;
      SeenSgn := True;
    end
    else if s[i] in ['a'..'z','A'..'Z'] then
    begin
      t := s[i] + t;
    end;
  end;
  Result := t;
end;

function Busca(Busca, Text: string; Inicio:integer): integer;
var n, retorno: integer;
    p : string;
begin
  retorno := 0;
  for n := 1 to length(Text) do
 // for n := length(Text) to 1 do
  begin
    p := Copy(Text, inicio, 1);
    if p = busca then
    begin
      retorno := n;
      break;
    end;
    inicio := inicio + 1;
  end;
  Result := retorno;
end;

function ContaCarac(sTexto,busca: String):integer;
var nPos, quantos : Integer;
begin
  quantos := 0;
  for nPos := 1 to Length(sTexto) do
  begin
    if (Copy(sTexto,nPos,1) = busca) then
      quantos := quantos + 1;
  end;
  Result := quantos; 
end;

function SubData(DataI,DataF: TDate) : String;
Var Data: TDateTime;
dia, mes, ano: Word;
begin
  Data := DataF - DataI;
  DecodeDate( Data, ano, mes, dia);
  Result := FloatToStr(Data);
end;

function StringToFloat(s : string) : Extended;
var i :Integer;
    t : string;
    SeenDecimal,SeenSgn : Boolean;
begin
  t := '';
  SeenDecimal := False;
  SeenSgn := False;
  {Percorre os caracteres da string:}
  for i := Length(s) downto 0 do
  {Filtra a string, aceitando somente n�meros e separador decimal:}
  if (s[i] in ['0'..'9', '-','+',FormatSettings.DecimalSeparator]) then
  begin
    if (s[i] = FormatSettings.DecimalSeparator) and (not SeenDecimal) then
    begin
      t := s[i] + t;
      SeenDecimal := True;
    end
    else if (s[i] in ['+','-']) and (not SeenSgn) and (i = 1) then
    begin
      t := s[i] + t;
      SeenSgn := True;
    end
    else if s[i] in ['0'..'9'] then
    begin
      t := s[i] + t;
    end;
  end;
  if t <> '' then
    Result := StrToFloat(t)
  else
    Result := 0;
end;

function StrIsInteger(const S: string): boolean;
begin
  try
    StrToInt(S);
    Result := true;
  except
    Result := false;
  end;
end;

function RemoveAcentos(Str:String): String;
{Remove caracteres acentuados de uma string}
Const ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
Var x : Integer;
Begin
  For x := 1 to Length(Str) do
    Begin
    if Pos(Str[x],ComAcento)<>0 Then
    begin
    Str[x] := SemAcento[Pos(Str[x],ComAcento)];
    end;
    end;
  Result := Str;
end;

function tbFileSize(const FileName: string): integer;
var
  SR: TSearchRec;
  I: integer;
begin
  I := FindFirst(FileName, faArchive, SR);
  try
    if I = 0 then
      Result := SR.Size
    else
      Result := -1;
  finally
    FindClose(SR);
  end;
end;

Function FormataCGC(CGC : string): string;
begin
  if Length(cgc) = 15 then
    Result := Copy(CGC,1,3)+'.'+Copy(CGC,4,3)+'.'+Copy(CGC,7,3)+'/'+Copy(CGC,10,4)+'-'+Copy(CGC,14,2);

  if Length(cgc) = 14 then
    Result := Copy(CGC,1,2)+'.'+Copy(CGC,3,3)+'.'+Copy(CGC,6,3)+'/'+Copy(CGC,9,4)+'-'+Copy(CGC,13,2);

end;

function GravaLog(form:string; regi:string; acao:string):string;
var ArqLog: TextFile;
    caminho : string;
    ini : Tinifile;
begin
    // criar log
    Ini  := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'GTI.INI');
    caminho := Ini.ReadString('LOG', 'caminho', 'C:\IW\');
    Ini.Free;

    AssignFile(ArqLog, caminho);
    If FileExists(caminho) then
      Append(ArqLog)
    else
      ReWrite(ArqLog);
    // Data (dd/mm/yy) hora (hh:mm) Usuario Form Registro
    WriteLN(ArqLog, formatdatetime('dd/mm/yy hh:mm',now()) +'|'+
                    '' +'|'+
                    form +'|'+
                    regi +'|'+
                    acao);
    CloseFile(ArqLog);
end;

function DataHoje(Data: TDate) : String;
begin
  if now < Data then
    Result := 'S'
  else
    Result := 'N';
end;

function codbarra(d : integer) : String;
begin
  if d = 1 then
    result := '1911S0001370018P008P008'
  else if d = 2 then
    result := '1911S0001210018P008P008'
  else if d = 3 then
    result := '1911S0001050018P008P008'
  else if d = 4 then
    result := '1911S0000900018P008P008'
  else if d = 5 then
    result := '1911S0000740018P008P008'
  else if d = 6 then
    result := '1911S0000580018P008P008'
  else if d = 7 then
    result := '1911S0000420018P008P008'
  else if d = 8 then
    result := '1911S0000270018P008P008'
  else
    result := '';
end;

procedure ByteArrayToFile(const ByteArray : TByteDynArray; const FileName : string );
var Count : integer;
    F : FIle of Byte;
    pTemp : Pointer;
begin
   AssignFile( F, FileName );
   Rewrite(F);
   try
      Count := Length( ByteArray );
      pTemp := @ByteArray[0];
      BlockWrite(F, pTemp^, Count );
   finally
      CloseFile( F );
   end;
end;

function FileToByteArray( const FileName : string ) : TByteDynArray;
const
   BLOCK_SIZE=1024;
var
  BytesRead, BytesToWrite, Count : integer; F : FIle of Byte;
  pTemp : Pointer;
begin
   AssignFile( F, FileName );
   Reset(F);
   try
      Count := FileSize( F );
      SetLength(Result, Count );
      pTemp := @Result[0];
      BytesRead := BLOCK_SIZE;
      while (BytesRead = BLOCK_SIZE ) do begin
         BytesToWrite := Min(Count, BLOCK_SIZE);
         BlockRead(F, pTemp^, BytesToWrite , BytesRead );
         pTemp := Pointer(LongInt(pTemp) + BLOCK_SIZE);
         Count := Count-BytesRead;
      end ;
   finally
      CloseFile( F );
   end ;
end;

function Ajustaaltura(altura : integer) : Integer;
begin
  if altura > frmmenu.Height then
   result := frmmenu.Height
  else
   result := altura;
end;

function LocalizaArray(fArrayDinamicoString : array of String; fValor : String) : Boolean;
var iCnt : Integer;
begin
  Result := false;
  iCnt := 0;
  while (not Result) and (iCnt <= Length(fArrayDinamicoString) - 1) do
  begin
    Result := (fArrayDinamicoString[iCnt] = fValor);
    inc(iCnt);
  end;
end;

function tamanho(sTexto: String):integer;
var quantos : Integer;
begin
  quantos := Length(sTexto);
  Result := quantos;
end;

// Converte formatos float Americano para o Brasileiro
function StrFloatUSA(S: String): String;
var
  I: Integer;
  Carac: String;
begin
  for I := 1 to Length(S) do
  begin
    Carac := Copy(S, I, 1);
    if (Carac = '.') then
      Result := Result + ','
    else
      if (Carac = ',') then
        Result := Result + '.'
      else
        Result := Result + Carac;
  end;
end;

function regiao_Bloq(UF: String): String;
var regioes, loc : string;
begin
  regioes := 'AM,RR,AP,PA,TO,RO,AC,MA,PI,CE,RN,PE,PB,SE,AL,BA,';
  loc := (UF+',');
  if pos(loc, regioes) > 0 then
    Result := 'S'
  else
    Result := 'N';  
end;

function TestaParaPar(TestaInteiro : Integer) : boolean;
begin
  if (TestaInteiro div 2) = (TestaInteiro/2) then
    result := True
  else
    result := False;
end;

Function ConsisteSenha(senha: String) : String ;
var SenhaDigi, Dig, nAsc : String;
    nString, I, cAsc : Integer;
    codAsc : byte;
begin
{  For I := 1 To nString do
  begin
    Dig := copy(UpperCase(senha), I, 1);
    if Dig = '' Then
      CodAsc := ORD('32')
    else
      CodAsc := ORD(DIG);
  end;
  For I := 1 To 20 do
  begin
    nAsc := CodAsc(I) + 10;
  end;
  SenhaDigi := '';
  For I := 1 To nString do
  begin
    SenhaDigi := SenhaDigi + Chr(nAsc(I));
  end;

  Result := SenhaDigi;      }

end;

Function ReverteSenha(senha: String) : String ;
var SenhaDigi : String;
    nString, I  : Integer;
    a, b : String;
    Dig  : array[1..20] of Char;
    nAsc : array[1..20] of Integer;
    cAsc : array[1..20] of Byte;
begin
  nString := Length(senha);
  for I := 1 To nString do
  begin
    b := copy(UpperCase(senha), I, 1);
    Dig[i] := b[1];
    if Dig[i] = '' Then
      cAsc[i] := ORD(32)
    else
      cAsc[i] := ORD(DIG[i]);
  end;

  for I := 1 To 8 do
  begin
    nAsc[i] := cAsc[i] - 10;
  end;

  SenhaDigi := '';

  for I := 1 To nString do
  begin
    a := Chr(nAsc[i]);
    SenhaDigi := SenhaDigi + Chr(nAsc[i]);
  end;
  Result := SenhaDigi;

end;

function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
const
  //Lista de caracteres especiais
  xCarEsp: array[1..38] of String = ('�', '�', '�', '�', '�','�', '�', '�', '�', '�',
                                     '�', '�','�', '�','�', '�','�', '�',
                                     '�', '�', '�','�', '�','�', '�', '�', '�', '�',
                                     '�', '�', '�','�','�', '�','�','�','�','�');
  //Lista de caracteres para troca
  xCarTro: array[1..38] of String = ('a', 'a', 'a', 'a', 'a','A', 'A', 'A', 'A', 'A',
                                     'e', 'e','E', 'E','i', 'i','I', 'I',
                                     'o', 'o', 'o','o', 'o','O', 'O', 'O', 'O', 'O',
                                     'u', 'u', 'u','u','u', 'u','c','C','n', 'N');
  //Lista de Caracteres Extras
  xCarExt: array[1..48] of string = ('<','>','!','@','#','$','%','�','&','*',
                                     '(',')','_','+','=','{','}','[',']','?',
                                     ';',':',',','|','*','"','~','^','�','`',
                                     '�','�','�','�','�','�','�','�','�','�',
                                     '�','�','�','�','�','�','�','�');
var
  xTexto : string;
  i : Integer;
begin
   xTexto := aTexto;
   for i:=1 to 38 do
     xTexto := StringReplace(xTexto, xCarEsp[i], xCarTro[i], [rfreplaceall]);
   //De acordo com o par�metro aLimExt, elimina caracteres extras.
   if (aLimExt) then
     for i:=1 to 48 do
       xTexto := StringReplace(xTexto, xCarExt[i], '', [rfreplaceall]);
   Result := xTexto;
end;

function EncodeBase64(S: string): string;
var i, a, x, b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Ord(s[i]);
    b := b * 256 + x;
    a := a + 8;
    while a >= 6 do
    begin
      a := a - 6;
      x := b div (1 shl a);
      b := b mod (1 shl a);
      Result := Result + Codes64[x + 1];
    end;
  end;
  if a > 0 then
  begin
    x := b shl (6 - a);
    Result := Result + Codes64[x + 1];
  end;
end;

function DecodeBase64(S: string): string;
var i, a, x, b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Pos(s[i], codes64) - 1;
    if x >= 0 then
    begin
      b := b * 64 + x;
      a := a + 6;
      if a >= 8 then
      begin
        a := a - 8;
        x := b shr a;
        b := b mod (1 shl a);
        x := x mod 256;
        Result := Result + chr(x);
      end;
    end
    else
      Exit;
  end;
end;

function ApAspas(sTexto: String):String;
var nPos : Integer;
    //carc : string;
    //letra : AnsiString;
    //pos : Char;
begin
  Result := '';
  for nPos := 1 to Length(sTexto) do
  begin
    //carc := Copy(sTexto,nPos,1);
    //letra := Copy(sTexto,nPos,1);
    //pos := sTexto[nPos];
    //ShowMessage(carc + ' - ' + IntToStr(Ord(pos)));
    if (Copy(sTexto,nPos,1) = #39) or (Copy(sTexto,nPos,1) = #34) then
      Delete(sTexto,nPos,1);
  end;
  Result := sTexto;
end;


end.
