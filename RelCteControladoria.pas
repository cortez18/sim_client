unit RelCteControladoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  ShellAPI, FileCtrl, JvComponentBase, JvgExportComponents, JvDBControls;

type
  TfrmRelCteControladoria = class(TForm)
    QCTe: TADOQuery;
    navnavig: TDataSource;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    QCliente: TADOQuery;
    DataSource1: TDataSource;
    QClienteNM_CLIENTE: TStringField;
    QClienteNR_CNPJ_CPF: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    Panel2: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    btnExcel: TBitBtn;
    btnHTML: TBitBtn;
    Panel3: TPanel;
    Gauge1: TGauge;
    JvDBNavigator1: TJvDBNavigator;
    QCTeNR_CONHECIMENTO: TFMTBCDField;
    QCTeDT_CTE: TDateTimeField;
    QCTeFILIAL_CTE: TFMTBCDField;
    QCTeNM_CLI: TStringField;
    QCTeCNPJ_CLI: TStringField;
    QCTeNM_CLIENTE_REM: TStringField;
    QCTeCIDADE_REM: TStringField;
    QCTeUF_REM: TStringField;
    QCTeNM_CLIENTE_DES: TStringField;
    QCTeNR_CNPJ_CPF_DES: TStringField;
    QCTeCIDADE_DES: TStringField;
    QCTeUF_DES: TStringField;
    QCTeVL_NF: TFMTBCDField;
    QCTePESO_KG: TFMTBCDField;
    QCTePESO_CUB: TFMTBCDField;
    QCTeQT_VOLUME: TFMTBCDField;
    QCTeDATNOT: TDateTimeField;
    QCTeNR_NOTA: TStringField;
    QCTeCFOP: TStringField;
    QCTeVL_FRETE: TFMTBCDField;
    QCTeVL_PEDAGIO: TFMTBCDField;
    QCTeVL_SEGURO: TFMTBCDField;
    QCTeVL_GRIS: TFMTBCDField;
    QCTeVL_OUTROS: TFMTBCDField;
    QCTeTAS: TFMTBCDField;
    QCTeVL_TX_CTE: TFMTBCDField;
    QCTeVL_TDE: TFMTBCDField;
    QCTeVL_SEG_BALSA: TFMTBCDField;
    QCTeVL_RED_FLUV: TFMTBCDField;
    QCTeVL_AGENDA: TFMTBCDField;
    QCTeVL_IMPOSTO: TFloatField;
    QCTeVL_TOTAL: TFloatField;
    QCTeVL_ALIQUOTA: TFloatField;
    QCTeNR_FATURA: TFMTBCDField;
    QCTeOPERACAO: TStringField;
    QCTeOBSCON: TStringField;
    QCTeNR_TABELA: TFMTBCDField;
    QCTeREGIAO: TStringField;
    QCTeNR_MANIFESTO: TFMTBCDField;
    QCTeDATAINC: TDateTimeField;
    QCTeDT_ENTREGA: TDateTimeField;
    QCTeNM_TRANSP: TStringField;
    QCTeCNPJ_TRANSPORTADORA: TStringField;
    QCTeCOD_TRANSP: TFMTBCDField;
    QCTeCTE_CUSTO: TFMTBCDField;
    QCTeVALOR_COBRADO: TBCDField;
    QCTeCUSTO_CALC: TBCDField;
    QCTeFL_TDE: TStringField;
    QCTeVLR_TOTAL_C: TBCDField;
    QCTeNR_CTE_PAGAR: TFMTBCDField;
    QCTeCUSTO_COBRADO: TBCDField;
    QCTeSERVICO: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btRelatorioClick(Sender: TObject);
    procedure edCtrcEnter(Sender: TObject);
    procedure btnExcelClick(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edosEnter(Sender: TObject);
    procedure edManifestoEnter(Sender: TObject);
    procedure btnHTMLClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelCteControladoria: TfrmRelCteControladoria;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRelCteControladoria.btnHTMLClick(Sender: TObject);
var
  memo1: TStringList;
  buffer: string;
  i: integer;
begin
  if FileExists('RelCT-e.html') then
    DeleteFile('RelCT-e.html');
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>IW - Intecom</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');

  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QCte.First;
  while not QCte.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QCte.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    QCte.Next;
    memo1.Add('<tr>');
  end;

  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('RelCT-e.html');

  buffer := 'RelCT-e.html';
  ShellExecute(Application.Handle, nil, PChar(buffer), nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmRelCteControladoria.btnExcelClick(Sender: TObject);
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  sarquivo := 'C:\SIM\Relatorio_Cte_NFS_' + glbuser + '.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>' + frmMenu.StatusBar1.Panels[3].Text + '</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.Add('Relat�rio de CT-e�s e NFS Emitidos');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QCte.First;
  Gauge1.MaxValue := QCte.RecordCount;
  while not QCte.Eof do
  begin
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QCte.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    Gauge1.AddProgress(1);
    QCte.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  try
    ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
      SW_SHOWNORMAL);
  except
    showmessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
  end;
  showmessage('Planilha salva em ' + sarquivo);
  {

    var coluna, linha: integer;
    excel: variant;
    valor: string;
    begin
    try
    excel:=CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
    except
    Application.MessageBox ('Vers�o do Ms-Excel Incompat�vel','Erro',MB_OK+MB_ICONEXCLAMATION);
    end;
    QCtrc.First;
    Gauge1.MaxValue := QCtrc.RecordCount;
    try
    for linha:=0 to QCTRC.RecordCount-1 do
    begin
    for coluna:=1 to JvDBGrid1.Columns.Count do
    begin
    valor:= QCtrc.Fields[coluna-1].AsString;
    if QCtrc.Fields[coluna-1].DataType = ftDateTime then
    begin
    excel.cells [linha+2,coluna].NumberFormat := 'dd/mm/aaaa';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsDateTime;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftString then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '@';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsString;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftBCD then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsFloat;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftFloat then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0,00';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsFloat;
    end
    else if QCtrc.Fields[coluna-1].DataType = ftInteger then
    begin
    excel.cells [linha+2,coluna].NumberFormat := '#.###.##0';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsInteger;
    end
    else
    begin
    excel.cells [linha+2,coluna].NumberFormat := '@';
    excel.cells [linha+2,coluna]:= QCtrc.Fields[coluna-1].AsString;
    end;
    end;
    Gauge1.AddProgress(1);
    QCtrc.Next;
    end;
    for coluna:=1 to JvDBGrid1.Columns.Count do
    begin
    valor:= JvDBGrid1.Columns[coluna-1].Title.Caption;
    excel.cells[1,coluna]:=valor;
    end;
    excel.columns.AutoFit;
    excel.visible:=true;
    except
    Application.MessageBox ('Aconteceu um erro desconhecido durante a convers�o'+
    'da tabela para o Ms-Excel','Erro',MB_OK+MB_ICONEXCLAMATION);
    end; }
end;

procedure TfrmRelCteControladoria.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    showmessage('O Per�odo � Obrigat�rio');
    dtInicial.SetFocus;
    exit;
  end;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      showmessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  // if Trim(ledIdCliente.LookupValue) = '' then
  // begin
  // ShowMessage('O Cliente � Obrigat�rio');
  // ledIdCliente.SetFocus;
  // exit;
  // end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QCte.close;
  if Trim(ledIdCliente.LookupValue) = '' then
    QCte.SQL[3] := ''
  else
    QCte.SQL[3] := 'and cnpj_cli = ''' + ledIdCliente.LookupValue + '''';
  QCte.SQL[4] := 'and dt_cte between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCte.open;
  Panel1.Visible := false;
end;

procedure TfrmRelCteControladoria.edCtrcEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelCteControladoria.edManifestoEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelCteControladoria.edosEnter(Sender: TObject);
begin
  dtInicial.Text := '';
  dtFinal.Text := '';
end;

procedure TfrmRelCteControladoria.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCte.close;
  QCliente.close;
end;

procedure TfrmRelCteControladoria.FormCreate(Sender: TObject);
begin
  QCliente.open;
end;

procedure TfrmRelCteControladoria.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCte.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    showmessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRelCteControladoria.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
