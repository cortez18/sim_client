unit CadTabelaReceita_upload;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, Buttons, ComCtrls,
  FileCtrl,  Gauges, ADODB, JvMemoryDataset, ExtCtrls, DBCtrls, Mask, JvExMask,
  JvToolEdit, JvBaseEdits, JvMaskEdit, FMTBcd, SqlExpr, DBClient, Provider,
  JvDBGridExport, JvComponentBase, JvgExportComponents, xmldom, XMLIntf,
  msxmldom, XMLDoc, ComObj;

type
  TfrmCadTabelaReceita_upload = class(TForm)
    DirectoryListBox1: TDirectoryListBox;
    FLB: TFileListBox;
    QImp: TADOQuery;
    QForne: TADOQuery;
    Query: TADOQuery;
    QueryTEM: TBCDField;
    Panel1: TPanel;
    PB1: TGauge;
    btnImportar: TBitBtn;
    btnSair: TBitBtn;
    QForneRAZSOC: TStringField;
    Qcadtabela: TADOQuery;
    QCidade: TADOQuery;
    QCidadeCODMUN: TFMTBCDField;
    Panel2: TPanel;
    Label4: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    DriveComboBox1: TDriveComboBox;
    QcadtabelaCOD_VENDA: TFMTBCDField;
    QcadtabelaCOD_CLIENTE: TFMTBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_P: TFloatField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TFMTBCDField;
    QcadtabelaUSUARIO: TStringField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QcadtabelaCODMUN: TFMTBCDField;
    QcadtabelaCODMUNO: TFMTBCDField;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TFloatField;
    QcadtabelaVL_TDEP: TFloatField;
    QcadtabelaVL_TDEM: TBCDField;
    QcadtabelaFL_STATUS: TStringField;
    QcadtabelaOPERACAO: TStringField;
    QcadtabelaVL_TDEMAX: TBCDField;
    QcadtabelaOBS: TStringField;
    QcadtabelaREGIAO: TStringField;
    QcadtabelaVL_SEG_BALSA: TFloatField;
    QcadtabelaVL_REDEP_FLUVIAL: TFloatField;
    QcadtabelaVL_AGEND: TBCDField;
    QcadtabelaVL_PALLET: TBCDField;
    QcadtabelaVL_TAS: TBCDField;
    QcadtabelaVL_DESPACHO: TBCDField;
    QcadtabelaVL_ENTREGA_PORTO: TFloatField;
    QcadtabelaVL_ALFAND: TFloatField;
    QcadtabelaVL_CANHOTO: TBCDField;
    QcadtabelaVL_FLUVIAL: TBCDField;
    QcadtabelaVL_FLUVIAL_M: TBCDField;
    QcadtabelaDT_INICIAL: TDateTimeField;
    QcadtabelaDT_FINAL: TDateTimeField;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    QcadtabelaVL_DEVOLUCAO: TBCDField;
    QcadtabelaFL_RATEIO: TStringField;
    QcadtabelaVL_FLUVMIN: TBCDField;
    QcadtabelaVL_AJUDA: TBCDField;
    QcadtabelaFL_FRETE: TStringField;
    QcadtabelaFL_PEDAGIO: TStringField;
    QcadtabelaFL_EXCEDENTE: TStringField;
    QcadtabelaFL_AD: TStringField;
    QcadtabelaFL_GRIS: TStringField;
    QcadtabelaFL_ENTREGA: TStringField;
    QcadtabelaFL_SEGURO: TStringField;
    QcadtabelaFL_TDE: TStringField;
    QcadtabelaFL_TR: TStringField;
    QcadtabelaFL_SEG_BALSA: TStringField;
    QcadtabelaFL_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_AGEND: TStringField;
    QcadtabelaFL_PALLET: TStringField;
    QcadtabelaFL_TAS: TStringField;
    QcadtabelaFL_DESPACHO: TStringField;
    QcadtabelaFL_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_ALFAND: TStringField;
    QcadtabelaFL_CANHOTO: TStringField;
    QcadtabelaFL_FLUVIAL: TStringField;
    QcadtabelaFL_DEVOLUCAO: TStringField;
    QcadtabelaFL_AJUDA: TStringField;
    QcadtabelaFL_CTE: TStringField;
    QcadtabelaFL_NF_FRETE: TStringField;
    QcadtabelaFL_NF_PEDAGIO: TStringField;
    QcadtabelaFL_NF_EXCEDENTE: TStringField;
    QcadtabelaFL_NF_AD: TStringField;
    QcadtabelaFL_NF_GRIS: TStringField;
    QcadtabelaFL_NF_ENTREGA: TStringField;
    QcadtabelaFL_NF_SEGURO: TStringField;
    QcadtabelaFL_NF_TDE: TStringField;
    QcadtabelaFL_NF_TR: TStringField;
    QcadtabelaFL_NF_SEG_BALSA: TStringField;
    QcadtabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_NF_AGEND: TStringField;
    QcadtabelaFL_NF_PALLET: TStringField;
    QcadtabelaFL_NF_TAS: TStringField;
    QcadtabelaFL_NF_DESPACHO: TStringField;
    QcadtabelaFL_NF_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_NF_ALFAND: TStringField;
    QcadtabelaFL_NF_CANHOTO: TStringField;
    QcadtabelaFL_NF_FLUVIAL: TStringField;
    QcadtabelaFL_NF_DEVOLUCAO: TStringField;
    QcadtabelaFL_NF_AJUDA: TStringField;
    QcadtabelaFL_NF_CTE: TStringField;
    QcadtabelaVL_REENT_MINIMA: TBCDField;
    QcadtabelaDOC_CONTROLER: TStringField;
    QcadtabelaREPROVADO: TStringField;
    QcadtabelaCONTROLER_REPR: TStringField;
    QcadtabelaVL_ARMAZENAGEM: TBCDField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_DEDICADO: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    QcadtabelaFL_IMPOSTO_ICMS: TStringField;
    QcadtabelaVL_FRETE_VALOR: TFloatField;
    QcadtabelaVL_TAXA_TRT: TFloatField;
    QcadtabelaVL_AGENDG: TBCDField;
    QcadtabelaVL_DIARIA48: TBCDField;
    QcadtabelaVL_DEVOLUCAOP: TBCDField;
    QcadtabelaVL_REENTREGAP: TBCDField;
    QcadtabelaVL_6X1: TBCDField;
    QcadtabelaQT6X1: TFMTBCDField;
    procedure btnSairClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FLBClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  frmCadTabelaReceita_upload: TfrmCadTabelaReceita_upload;

implementation

uses Dados,funcoes, menu;

{$R *.dfm}

procedure TfrmCadTabelaReceita_upload.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadTabelaReceita_upload.FLBClick(Sender: TObject);
begin
  label5.caption := flb.FileName;
end;

procedure TfrmCadTabelaReceita_upload.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Qimp.close;
end;

procedure TfrmCadTabelaReceita_upload.btnImportarClick(Sender: TObject);
var nm, ped, altera, arq: string;
    i, id, x, z, cod_cliente, new : integer;
    xlsObj : OleVariant;
    wb : variant;
    u, t, tra, des, cad : string;
begin
  btnSair.Enabled := false;
  btnImportar.Enabled := false;
  x := 0;
  xlsObj := CreateOleObject('Excel.Application');
  wb  := xlsObj.Workbooks.open(label5.caption);
  i   := 5;  // linha que come�a na planilha
  nm := xlsObj.Workbooks[1].Sheets[1].Cells[i,1];
  while trim(nm) <> '' do
  begin
    x := x + 1;
    nm := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,1]);
    inc(i);
  end;
  PB1.MaxValue := x;
  PB1.Progress := 0;
  x := 0;
  z := 0;
  ped := '';
  i   := 5;  // linha que come�a na planilha
  nm := xlsObj.Workbooks[1].Sheets[1].Cells[i,1];
  cod_cliente := xlsObj.Workbooks[1].Sheets[1].Cells[1,3];

  QForne.close;
  QForne.Parameters[0].Value := cod_cliente;
  QForne.Open;
  if QForne.Eof then
  begin
    QForne.close;
    ShowMessage('Cliente colocado na Planilha n�o est� Cadastrado no ERP');
    btnImportar.Enabled := true;
    exit;
  end
  else
    label8.Caption := QForneRAZSOC.AsString;

  QForne.close;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('SELECT COUNT(EMAIL) EMAIL FROM CYBER.RODCTC WHERE SITUAC = ''A'' AND  RODCTC.CODCLIFOR = :0');
  dtmDados.iQuery1.Parameters[0].value := cod_cliente;
  dtmDados.iQuery1.Open;
  if dtmDados.iQuery1.FieldByName('email').value = 0 then
  begin
    dtmDados.iQuery1.Close;
    ShowMessage('Cliente sem e-mail cadastrado');
    btnImportar.Enabled := true;
    Exit;
  end;
  dtmDados.iQuery1.Close;

  while trim(nm) <> '' do
  begin
    x := x + 1;
    PB1.AddProgress(1);
    Application.ProcessMessages;
    nm := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,1]);
    if trim(nm) = '' then
    begin
      Showmessage('Processo terminado!!!');
      xlsObj.quit;
      arq := label5.caption;
      altera := label5.caption+'.importada';
      RenameFile(arq,altera);
      FLB.Update;
      btnSair.Enabled := true;
      btnImportar.Enabled := true;
      exit;
    end;

    QCadtabela.open;
    try
      z := z + 1;
      label1.caption := 'Itens Processados = ' + inttostr(z);
      // se o peso incial for = 0 � uma nova tabela
      if StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,52]) <= 0.5 then
      begin
        Qcadtabela.append;
        QcadtabelaUSUARIO.value := GLBUSER;
        QcadtabelaDT_CADASTRO.value := date;
        if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,6]) <> '' then
        begin
          QCidade.Close;
          QCidade.Parameters[0].value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,3]);
          QCidade.Parameters[1].value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,6]);
          QCidade.Open;
          if not QCidade.eof then
            QcadtabelaCODMUNO.value := QCidadeCODMUN.AsInteger;
        end
        else
          QcadtabelaCODMUNO.value := 0;

        if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,10]) <> '' then
        begin
          QCidade.Close;
          QCidade.Parameters[0].value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,7]);
          QCidade.Parameters[1].value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,10]);
          QCidade.Open;
          if not QCidade.eof then
            QcadtabelaCODMUN.value := QCidadeCODMUN.AsInteger;
        end
        else
          QcadtabelaCODMUN.value := 0;

        QcadtabelaCOD_CLIENTE.value := cod_cliente;
        QcadtabelaFL_LOCAL.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,4]);
        QcadtabelaREGIAO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,5]);
        QcadtabelaDS_UF.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,3]);
        QcadtabelaDS_UF_DES.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,7]);
        QcadtabelaFL_LOCAL_DES.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,8]);
        QcadtabelaREGIAO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,9]);

        QcadtabelaVEICULO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,11]);
        QcadtabelaOPERACAO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,12]);
        QcadtabelaCONTROLER.value := '';
        QcadtabelaDT_CONTROLER.clear;
        QcadtabelaOBS.value := 'Inclus�o via UPLOAD de Planilha Matriz';
        QcadtabelaFL_IMPOSTO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,55]);
        QcadtabelaFL_PESO.value := alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,56]);
        QcadtabelaFL_STATUS.value := 'S';

        QcadtabelaVL_FRETE_MINIMO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,13]);
        QcadtabelaVL_PEDAGIO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,14]);
        QcadtabelaVL_OUTROS.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,15]);

       // QcadtabelaVL_HE.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,17]);
        QcadtabelaVL_AD.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,18]);
        QcadtabelaSEGURO_MINIMO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,19]);
        QcadtabelaVL_GRIS.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,20]);
        QcadtabelaVL_GRIS_MINIMO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,21]);
        QcadtabelaVL_TDE.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,22]);
        QcadtabelaVL_TDEM.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,23]);
        QcadtabelaVL_TDEMAX.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,24]);
        QcadtabelaVL_TDEP.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,25]);
        QcadtabelaVL_TR.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,26]);
        //QcadtabelaVL_TRP.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,27]);
        //QcadtabelaVL_TRM.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,28]);
        //QcadtabelaVL_OUTROS_REAIS.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,29]); // fluvial
        QcadtabelaVL_OUTROS_MINIMO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,30]);; // fluvial minimo
        QcadtabelaVL_EXCEDENTE.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,31]);
        //QcadtabelaVL_PORTO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,32]);
        //QcadtabelaVL_PORTOMIN.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,33]);

        //QcadtabelaVL_SUFRA.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,34]);
        //QcadtabelaVL_TDA.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,35]);
        //QcadtabelaVL_TDAMIN.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,36]);
        //QcadtabelaVL_TDAMAX.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,37]);

        //QcadtabelaVL_ENTREGA.value := edEntrega.value;  // Sinergia
        //QcadtabelaVL_preceita.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,34]);

        // Implementa��o do projeto generalidades
        //QcadtabelaVL_TDC.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,38]);
        //QcadtabelaVL_PALETIZACAO.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,39]);
        QcadtabelaVL_REENTREGA.value :=  StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,40]);
        //QcadtabelaVL_REENTREGA_MIN.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,41]);
        //QcadtabelaVL_AJUDANTES.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,42]);
        QcadtabelaVL_HORA_PARADA.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,43]);
        QcadtabelaVL_ARMAZENAGEM.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,44]);
        //Qcadtabelavl_armmin.value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,45]);
        QCadTabela.Post;

        Query.Close;
        Query.Open;
        new := QueryTEM.AsInteger;
        Query.Close;
      end;

      if Alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,46]) <> '' then
      begin
        dtmDados.iQuery1.Close;
        dtmDados.iQuery1.sql.clear;
        dtmDados.iQuery1.sql.add
          ('insert into tb_tabelavenda_generalidades ( cod_VENDA,vl_pernoite,vl_vd,vl_diaria,veiculo, vl_palete)');
        dtmDados.iQuery1.sql.add(' values ( :0, :1, :2, :3, :4, :5)');
        dtmDados.iQuery1.Parameters[0].value := new;
        dtmDados.iQuery1.Parameters[1].value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,47]);
        dtmDados.iQuery1.Parameters[2].value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,48]);
        dtmDados.iQuery1.Parameters[3].value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,49]);
        dtmDados.iQuery1.Parameters[4].value := Alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,46]);
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
      end;

      // Valores da fra��o
      dtmDados.iQuery1.Close;
      dtmDados.iQuery1.sql.clear;
      dtmDados.iQuery1.sql.add('insert into tb_tabelavenda_item (cod_venda, ' +
        'datai, dataf, vl_rodoviario, nr_peso_de, nr_peso_ate, vr_ctrc, vr_exced, '
        + 'vl_minimo, diaria) values ( ' + ':0, :1, :2, :3, :4, :5, :6, :7, :8, :9)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value := new;
      dtmDados.iQuery1.Parameters.ParamByName('1').value := StrToDate(xlsObj.Workbooks[1].Sheets[1].Cells[i,1]);
      dtmDados.iQuery1.Parameters.ParamByName('2').value := StrToDate(xlsObj.Workbooks[1].Sheets[1].Cells[i,2]);
      dtmDados.iQuery1.Parameters.ParamByName('3').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,50]);
      dtmDados.iQuery1.Parameters.ParamByName('4').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,52]);
      dtmDados.iQuery1.Parameters.ParamByName('5').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,53]);
      dtmDados.iQuery1.Parameters.ParamByName('6').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,51]);
      dtmDados.iQuery1.Parameters.ParamByName('7').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,54]);
      dtmDados.iQuery1.Parameters.ParamByName('8').value := GLBUSER;
      if alltrim(xlsObj.Workbooks[1].Sheets[1].Cells[i,57]) = '' then
        dtmDados.iQuery1.Parameters.ParamByName('9').value := 0
      else
        dtmDados.iQuery1.Parameters.ParamByName('9').value := StrToFloat(xlsObj.Workbooks[1].Sheets[1].Cells[i,57]);
      dtmDados.iQuery1.ExecSQL;

      inc(i);
    except
      on e: Exception do
      begin
        dtmDados.iQuery1.Close;
        dtmDados.iQuery1.sql.clear;
        dtmDados.iQuery1.sql.add
          ('delete from tb_tabelareceita where cod_cliente = :0 and usuario = :1 and trunc(dt_cadastro) = trunc(sysdate) and dt_controler is null');
        dtmDados.iQuery1.sql.add(' values ( :0, :1)');
        dtmDados.iQuery1.Parameters[0].value := cod_cliente;
        dtmDados.iQuery1.Parameters[1].value := glbcoduser;
        dtmDados.iQuery1.ExecSQL;
        dtmDados.iQuery1.Close;
        ShowMessage(e.message);
      end;

    end;
  end;
end;


end.
