unit Vale_palete;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, jpeg;

type
  TfrmVale_Palete = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLBand2: TRLBand;
    RLDraw15: TRLDraw;
    RLDraw32: TRLDraw;
    RLDraw30: TRLDraw;
    RLDraw21: TRLDraw;
    RLDraw1: TRLDraw;
    RLDraw2: TRLDraw;
    RLDraw3: TRLDraw;
    RLDraw5: TRLDraw;
    RLDraw6: TRLDraw;
    RLLabel2: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel3: TRLLabel;
    RLDraw9: TRLDraw;
    RLLabel11: TRLLabel;
    RLDraw16: TRLDraw;
    RLDraw17: TRLDraw;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLDraw18: TRLDraw;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLDraw19: TRLDraw;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLDraw20: TRLDraw;
    RLDraw22: TRLDraw;
    RLDraw23: TRLDraw;
    RLDraw24: TRLDraw;
    RLDraw25: TRLDraw;
    RLLabel26: TRLLabel;
    RLDraw29: TRLDraw;
    RLDraw26: TRLDraw;
    RLDraw27: TRLDraw;
    RLLabel27: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw72: TRLDraw;
    RLDraw73: TRLDraw;
    RLDraw76: TRLDraw;
    RLDraw77: TRLDraw;
    RLDraw78: TRLDraw;
    RLDraw79: TRLDraw;
    RLLabel68: TRLLabel;
    RLLabel67: TRLLabel;
    RLLabel66: TRLLabel;
    RLLabel65: TRLLabel;
    RLLabel24: TRLLabel;
    RLLabel23: TRLLabel;
    RLMemo4: TRLMemo;
    RLDraw86: TRLDraw;
    RLDraw8: TRLDraw;
    RLDraw88: TRLDraw;
    motorista: TRLLabel;
    transportadora: TRLLabel;
    veiculo: TRLLabel;
    NR_ROMANEIO: TRLLabel;
    placa: TRLLabel;
    DTIC: TRLLabel;
    RLDraw4: TRLDraw;
    RLDraw7: TRLDraw;
    RLMemo2: TRLMemo;
    RLLabel7: TRLLabel;
    RLImage3: TRLImage;
    RLImage5: TRLImage;
    RLLabel50: TRLLabel;
    RLDraw34: TRLDraw;
    RLDraw40: TRLDraw;
    RLDraw36: TRLDraw;
    RLDraw35: TRLDraw;
    RLDraw37: TRLDraw;
    RLDraw38: TRLDraw;
    RLDraw39: TRLDraw;
    RLDraw10: TRLDraw;
    RLLabel41: TRLLabel;
    RLLabel42: TRLLabel;
    RLLabel39: TRLLabel;
    RLLabel40: TRLLabel;
    RLLabel43: TRLLabel;
    RLLabel44: TRLLabel;
    RLLabel36: TRLLabel;
    RLLabel37: TRLLabel;
    RLLabel38: TRLLabel;
    RLLabel49: TRLLabel;
    RLLabel48: TRLLabel;
    RLLabel47: TRLLabel;
    RLLabel46: TRLLabel;
    RLDraw11: TRLDraw;
    RLDraw13: TRLDraw;
    RLDraw14: TRLDraw;
    RLDraw12: TRLDraw;
    RLLabel45: TRLLabel;
    RLDraw31: TRLDraw;
    RLLabel35: TRLLabel;
    RLLabel34: TRLLabel;
    RLLabel29: TRLLabel;
    RLLabel28: TRLLabel;
    RLLabel30: TRLLabel;
    RLLabel31: TRLLabel;
    RLLabel25: TRLLabel;
    RLLabel15: TRLLabel;
    RLLabel32: TRLLabel;
    RLLabel14: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel33: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel10: TRLLabel;
    RLDraw33: TRLDraw;
    RLDraw41: TRLDraw;
    RLDraw42: TRLDraw;
    RLImage1: TRLImage;
    RLImage2: TRLImage;
    RLLabel1: TRLLabel;
    RLLabel51: TRLLabel;
    RLLabel52: TRLLabel;
    RLLabel53: TRLLabel;
    RLLabel54: TRLLabel;
    RLDraw43: TRLDraw;
    RLDraw44: TRLDraw;
    RLDraw45: TRLDraw;
    RLLabel55: TRLLabel;
    RLLabel56: TRLLabel;
    RLDraw46: TRLDraw;
    RLLabel57: TRLLabel;
    RLLabel58: TRLLabel;
    RLLabel59: TRLLabel;
    RLLabel60: TRLLabel;
    RLLabel61: TRLLabel;
    RLLabel62: TRLLabel;
    RLLabel63: TRLLabel;
    RLDraw48: TRLDraw;
    RLLabel64: TRLLabel;
    RLLabel69: TRLLabel;
    RLLabel70: TRLLabel;
    RLDraw28: TRLDraw;
    RLDraw47: TRLDraw;
    RLDraw49: TRLDraw;
    RLDraw50: TRLDraw;
    RLDraw51: TRLDraw;
    RLLabel71: TRLLabel;
    RLLabel72: TRLLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVale_Palete: TfrmVale_Palete;

implementation

{$R *.dfm}

end.
