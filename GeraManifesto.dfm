object frmGeraManifesto: TfrmGeraManifesto
  Left = 0
  Top = 0
  Caption = 'Gerar Mapa de Separa'#231#227'o'
  ClientHeight = 644
  ClientWidth = 1250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 345
    Height = 644
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 343
      Height = 100
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object lblDescricao: TLabel
        Left = 6
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Procurar'
        Transparent = True
      end
      object Label32: TLabel
        Left = 193
        Top = 47
        Width = 146
        Height = 13
        AutoSize = False
        Caption = 
          '                                                                ' +
          '                                 '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label45: TLabel
        Left = 244
        Top = 4
        Width = 58
        Height = 13
        Caption = 'A partir de :'
        Transparent = True
      end
      object edValor: TEdit
        Left = 128
        Top = 20
        Width = 110
        Height = 21
        TabOrder = 1
        OnExit = edValorExit
      end
      object cbPesquisa: TComboBox
        Left = 6
        Top = 20
        Width = 110
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        OnChange = cbPesquisaChange
      end
      object ckRota: TCheckBox
        Left = 5
        Top = 47
        Width = 97
        Height = 17
        Caption = 'Por Rota'
        TabOrder = 2
        OnClick = ckRotaClick
      end
      object cbFiltro: TCheckBox
        Left = 128
        Top = 47
        Width = 49
        Height = 17
        Caption = 'Filtrar'
        TabOrder = 3
        OnClick = cbFiltroClick
      end
      object edData: TJvDateEdit
        Left = 244
        Top = 20
        Width = 91
        Height = 21
        ShowNullDate = False
        YearDigits = dyFour
        TabOrder = 4
        OnExit = edDataExit
      end
      object cbCte: TCheckBox
        Left = 5
        Top = 64
        Width = 166
        Height = 17
        Caption = 'Busca por chave eletr'#244'nica'
        TabOrder = 5
        OnClick = cbCteClick
      end
      object edChave: TEdit
        Left = 158
        Top = 61
        Width = 181
        Height = 21
        TabOrder = 6
        Visible = False
        OnChange = edChaveChange
      end
      object cbTodos: TCheckBox
        Left = 5
        Top = 81
        Width = 97
        Height = 17
        Caption = 'Todos'
        TabOrder = 7
        Visible = False
        OnClick = cbTodosClick
      end
    end
    object pgTarefas: TPageControl
      Left = 1
      Top = 101
      Width = 343
      Height = 542
      ActivePage = tbTarefa
      Align = alClient
      TabOrder = 1
      object tbRota: TTabSheet
        Caption = 'Rota'
        object dgRota: TJvDBUltimGrid
          Left = 0
          Top = 0
          Width = 335
          Height = 514
          Align = alClient
          Ctl3D = False
          DataSource = dsRota
          Options = [dgIndicator, dgTabs, dgRowSelect, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 16
          Columns = <
            item
              Expanded = False
              FieldName = 'ROTA'
              Title.Caption = 'Rota'
              Visible = True
            end>
        end
      end
      object tbTarefa: TTabSheet
        Caption = 'CT-e/NFs'
        ImageIndex = 1
        OnShow = tbTarefaShow
        object dbgColeta: TJvDBUltimGrid
          Left = 0
          Top = 0
          Width = 335
          Height = 514
          Align = alClient
          Ctl3D = False
          DataSource = dsTemp
          Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = dbgColetaCellClick
          OnDrawColumnCell = dbgColetaDrawColumnCell
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 16
          Columns = <
            item
              Expanded = False
              FieldName = 'Coleta'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'filial'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Inserida'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'data'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'destino'
              Visible = True
            end>
        end
      end
      object tsRoad: TTabSheet
        Caption = 'RoadNet'
        ImageIndex = 2
        OnHide = tsRoadHide
        OnShow = tsRoadShow
        object dbgRoad: TJvDBUltimGrid
          Left = 0
          Top = 0
          Width = 335
          Height = 514
          Align = alClient
          Ctl3D = False
          DataSource = dsTemp
          Options = [dgIndicator, dgTabs, dgRowSelect, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentCtl3D = False
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = dbgRoadCellClick
          OnDrawColumnCell = dbgRoadDrawColumnCell
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 16
          Columns = <
            item
              Expanded = False
              FieldName = 'Coleta'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'filial'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Inserida'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'data'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'destino'
              Visible = True
            end>
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 345
    Top = 0
    Width = 905
    Height = 644
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 903
      Height = 642
      ActivePage = tsManT
      Align = alClient
      TabOrder = 0
      object TabSheet4: TTabSheet
        Caption = 'CT-e / NFs'
        ImageIndex = 3
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 31
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object pnBotao: TPanel
            Left = 439
            Top = 1
            Width = 455
            Height = 29
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object btRomaneio: TBitBtn
              Left = 337
              Top = 3
              Width = 118
              Height = 25
              Hint = 'Gerar Mapa de Separa'#231#227'o'
              Caption = 'Gerar Mapa'
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
                FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
                96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
                92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
                BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
                CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
                D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
                EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
                E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
                E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
                8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
                E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
                FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
                C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
                CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
                CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
                D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
                9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              TabStop = False
              OnClick = btRomaneioClick
            end
            object btnCarregar_Tarefas: TBitBtn
              Left = 212
              Top = 3
              Width = 111
              Height = 25
              Hint = 'Carrega Tarefas para Gerar Romaneio'
              Caption = 'Carregar'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF0051A6
                58B84FA356AEFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00418E4517408D43EC3F8B42B3FF00FF0054AB5CB369B3
                6FFF67B16DFF4FA456AEFF00FF004C9F52FFFF00FF00FF00FF00FF00FF00FF00
                FF00459449FF43924817429046F15CA160FF569C5AFF3F8C42B856AD5FEC71B8
                77FF83BF89FF68B16EFF50A457FF4EA255FFFF00FF00FF00FF00FF00FF00FF00
                FF0046964BFF4A974EFF5FA462FF71B075FF579E5BFF408D44AE58B0611757AE
                5FF171B877FF74B87BFF80BD86FF50A457FFFF00FF00FF00FF00FF00FF00FF00
                FF0048994DFF73B377FF63A968FF5AA15EFF429147AEFF00FF00FF00FF0058B0
                61175CB064FF84C18AFF86C18BFF52A759FFFF00FF00FF00FF00FF00FF00FF00
                FF004A9B4FFF79B67EFF74B379FF45954AFFFF00FF00FF00FF00FF00FF005AB3
                63FF59B161FF57AE5FFF55AC5DFF54AA5BFFFF00FF00FF00FF00FF00FF00FF00
                FF004B9E51FF4A9C50FF48994EFF47974CFF45954AFFFF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0062BF
                6DFF61BD6CFF60BB6AFF5EB968FF5DB767FFFF00FF00FF00FF00FF00FF00FF00
                FF0055AB5DFF53A95BFF52A759FF50A457FF4EA255FFFF00FF00FF00FF00FF00
                FF0063BF6DFF97D19FFF98D0A0FF5FB969FFFF00FF00FF00FF00FF00FF00FF00
                FF0057AE5FFF8CC692FF87C38DFF57AA5EFF50A55717FF00FF00FF00FF0065C3
                70AE7ECA87FF8ECD98FF97D19FFF60BC6BFFFF00FF00FF00FF00FF00FF00FF00
                FF0059B161FF8CC793FF7DBF85FF72B979FF52A759F150A5571767C673AE80CD
                8AFF9ED6A7FF83CC8CFF69C273FF62BE6CFFFF00FF00FF00FF00FF00FF00FF00
                FF005AB364FF59B162FF70BB78FF8DC794FF73BA7AFF52A85AEC68C774B881CE
                8BFF86CF90FF65C371F164C26F1763C06EFFFF00FF00FF00FF00FF00FF00FF00
                FF005CB666FFFF00FF0059B162AE71BB79FF6FB977FF54AA5CB3FF00FF0068C7
                74B367C673EC66C57217FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF0059B262AE58AF60B8FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              TabStop = False
              OnClick = btnCarregar_TarefasClick
            end
            object btnVoltaTarefa: TBitBtn
              Left = 99
              Top = 3
              Width = 100
              Height = 25
              Hint = 'Volta Tarefa'
              Caption = 'Volta CT-e'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
                B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
                E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
                EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
                D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
                E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
                D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
                D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
                DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
                DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
                DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
                ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
                E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
                F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
                E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
                F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
                FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
                FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
                F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
                D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              TabStop = False
              OnClick = btnVoltaTarefaClick
            end
          end
        end
        object dgTarefa: TJvDBUltimGrid
          Left = 0
          Top = 31
          Width = 895
          Height = 522
          Align = alClient
          Ctl3D = False
          DataSource = dstTarefa
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = dgTarefaDrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'CODCON'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'CT-e/OST'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 74
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FL_EMPRESA'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Filial'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_CLI_OS'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_EMP_ORI_TA'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Origem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_EMP_DEST_TA_01'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF_DESTINO'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'UF'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_PESO_TA_01'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Peso'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUSUARIO'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'palete'
              Title.Alignment = taCenter
              Title.Caption = 'Palete'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 553
          Width = 895
          Height = 61
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 2
          object Label25: TLabel
            Left = 112
            Top = 9
            Width = 51
            Height = 13
            Caption = 'Total Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label26: TLabel
            Left = 327
            Top = 9
            Width = 41
            Height = 13
            Caption = 'Total NF'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 434
            Top = 9
            Width = 51
            Height = 13
            Caption = 'Total Frete'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TLabel
            Left = 218
            Top = 9
            Width = 64
            Height = 13
            Caption = 'Total Cubado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TLabel
            Left = 8
            Top = 9
            Width = 67
            Height = 13
            Caption = 'Total Volumes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label52: TLabel
            Left = 538
            Top = 9
            Width = 82
            Height = 13
            Caption = 'Total Documento'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edTotPesoTarefa: TJvCalcEdit
            Left = 112
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object edTotValorTarefa: TJvCalcEdit
            Left = 327
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object edTotFreteTarefa: TJvCalcEdit
            Left = 434
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clTeal
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object edTotCubadoTarefa: TJvCalcEdit
            Left = 218
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
          end
          object edTotVolumeTarefa: TJvCalcEdit
            Left = 8
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 4
            DecimalPlacesAlwaysShown = False
          end
          object edTotDoc: TJvCalcEdit
            Left = 538
            Top = 25
            Width = 83
            Height = 21
            TabStop = False
            Color = clSilver
            DecimalPlaces = 0
            DisplayFormat = ',0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 5
            DecimalPlacesAlwaysShown = False
          end
        end
        object MemoRota: TMemo
          Left = 517
          Top = 76
          Width = 352
          Height = 120
          TabOrder = 3
          Visible = False
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Mapa de Separa'#231#227'o'
        OnShow = TabSheet1Show
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 57
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label48: TLabel
            Left = 109
            Top = 9
            Width = 30
            Height = 13
            Caption = 'CT-e :'
            Transparent = True
          end
          object btnMapaRomaneio: TBitBtn
            Left = 8
            Top = 5
            Width = 96
            Height = 25
            Hint = 'Libera'#231#227'o de Confer'#234'ncia'
            Caption = 'Confer'#234'ncia'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E000000000000000000003E9DCA7B3696
              D1E53390CCEB328BCBED3B95C287FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042ACE1CDC4EB
              F7FF7FE1F6FF9FE6F7FF328AC9EF3A90BA91FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFC6F4
              FBFF43D6F1FF48DBF5FF82E1F5FF3188C8F0398DB798FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3F2BBEF
              FAFF39D1F1FF28C5EEFF4EDCF6FF85E2F7FF328BCAEE398BB59BFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFF0FC
              FEFFB0EEFAFF43D8F4FF28C8EEFF41D7F4FF89E2F7FF328BCBED3888B2A3FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0
              E3FF44B0E3FFABEAF9FF4ED8F3FF2BC9EFFF3DD6F3FF8AE1F7FF328ACAEE3383
              B8DC297DD6FF2C85D8FF368EC1CBFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF0044B0E3FFF1FCFEFFBBF1FBFF7BE4F6FF28D2F0FF37D4F5FF83E0F6FF3EA9
              E3FFA0F3FCFFA9F5FCFF2B82D7FF358BBFCFFFFFFF00FFFFFF00FFFFFF00FFFF
              FF0044B0E39644B0E3FF45B2E3FF76C5EAFFACEEFAFF39D6F2FF4DDBF5FF65E4
              F7FF3CCEF2FF32C9EFFF85EFFBFF2B81D7FF3588BBD4FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0044B0E30244B0E3966FC4EAFF80E5F7FF3DD1F1FF5DDB
              F5FF69DFF6FF50D7F3FF34CDEFFF85EFFBFF297FD6FF3486BAD8FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFD5F7FCFF89E7F8FF7EE4F7FF7EE4
              F7FF7EE4F7FF82E5F7FF47D6F2FF38CEF0FFAEF5FCFF297CD6FFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFBEF2FBFF7EE4F7FF7EE4F7FF81E5
              F7FF94E9F8FFBCF1FBFF8BDAF3FF49DDF5FFC1F8FDFF3090DAFFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFDEF8FCFF8DE7F8FF7EE4F7FF94E9
              F8FFBCE9F8FF44B0E3FF42ACE3FFEEFCFEFF3298DDFF3995C8BFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFCEF5FCFF8DE7F8FFA1EC
              F9FF44B0E3FF44B0E3FFFFFFFFFF39A1DFFF3995C8BFFFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFCEF5FCFF9EEB
              F9FFBEF2FBFFFEFFFFFF44B0E3FF42ABDF9CFFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFDEF8
              FCFFDEF8FCFF44B0E3FF44B0E396FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0
              E3FF44B0E3FF44B0E396FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnMapaRomaneioClick
          end
          object btnCancelaManifesto: TBitBtn
            Left = 477
            Top = 5
            Width = 80
            Height = 25
            Hint = 'Cancela Manifesto'
            Caption = 'Cancelar'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
              33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
              993337777F777F3377F3393999707333993337F77737333337FF993399933333
              399377F3777FF333377F993339903333399377F33737FF33377F993333707333
              399377F333377FF3377F993333101933399377F333777FFF377F993333000993
              399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
              99333773FF777F777733339993707339933333773FF7FFF77333333999999999
              3333333777333777333333333999993333333333377777333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnCancelaManifestoClick
          end
          object btnMapaRomaneioManifesto: TBitBtn
            Left = 563
            Top = 5
            Width = 82
            Height = 25
            Caption = 'Mapa'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnMapaRomaneioManifestoClick
          end
          object btnLimpaRomaneio: TBitBtn
            Left = 651
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Excluir Tarefas Romaneadas que ainda nao foram gerados Manifesto'
            Caption = 'Limpa Mapa'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
              305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
              005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
              B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
              B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
              B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
              B0557777FF577777F7F500000E055550805577777F7555575755500000555555
              05555777775555557F5555000555555505555577755555557555}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = False
            OnClick = btnLimpaRomaneioClick
          end
          object btnNovoManifesto: TBitBtn
            Left = 251
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Novo Manifesto'
            Caption = 'Novo'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
              0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
              33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            TabStop = False
            OnClick = btnNovoManifestoClick
          end
          object btnGerarMan: TBitBtn
            Left = 362
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Gerar Manifesto'
            Caption = 'Gerar Manifesto'
            Enabled = False
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
              FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
              96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
              92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
              BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
              CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
              D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
              EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
              E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
              E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
              8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
              E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
              FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
              C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
              CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
              CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
              D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
              9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            TabStop = False
            OnClick = btnGerarManClick
          end
          object edCte: TJvCalcEdit
            Left = 146
            Top = 5
            Width = 66
            Height = 21
            TabStop = False
            Color = clWhite
            DecimalPlaces = 0
            DisplayFormat = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = False
            TabOrder = 6
            DecimalPlacesAlwaysShown = False
          end
          object edFilial: TJvCalcEdit
            Left = 218
            Top = 5
            Width = 22
            Height = 21
            TabStop = False
            Color = clWhite
            DecimalPlaces = 0
            DisplayFormat = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = False
            TabOrder = 7
            DecimalPlacesAlwaysShown = False
            OnExit = edFilialExit
          end
          object btnTransp: TBitBtn
            Left = 775
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Libera Transportadora'
            Caption = 'Libera Transp.'
            Enabled = False
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00007D21EB037B1EFF00791504FF00FF00FF00FF00FF00FF00FF00FF001340
              58FF15425EFF25699CFF2C76B4FF3B8BBAADFF00FF00FF00FF00FF00FF00FF00
              FF0001832BEB43A15FFF007B1FCC00791906FF00FF00FF00FF00FF00FF001242
              59FF5D9CD4FFA6CFF5FFA9CFECFF488BC1FF219752FF1B9149FD158F43FD0F8B
              3BFD3A9F5EFF80C196FF46A362FF007D1FD100791907FF00FF00FF00FF001E6D
              93FFCBE3F9FF61AAECFF4098E8FF1567C2FF299B5BFF90CAA9FF8DC8A5FF8AC6
              A1FF88C59EFF6AB685FF82C297FF48A566FF007D21D700791B09FF00FF001E6D
              93FFC8E1F2FFD1E7FAFF347DB5FF3199C3FF319F63FF94CDADFF6FBA8EFF6BB8
              89FF66B685FF61B380FF67B582FF83C298FF3CA05CFF007F25F9FF00FF002063
              98202689B9FFB0CBE1FF67A9C8FF60DCF5FF37A36BFF96CEB0FF94CDADFF91CB
              AAFF90CBA8FF74BC90FF8AC7A1FF46A568FF078735FB01832D01FF00FF00FF00
              FF00FF00FF002689B9FFBEE6F2FFB3F4FCFF3DA56FFF37A46FFF34A269FF309D
              63FF55AF7CFF91CBAAFF4FAB74FF178F45FB118B3D01FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF002790BFFFC3EDF8FFB3F4FCFF60DCF5FF44D6F4FF8EEE
              FAFF34A16DFF5AB381FF289857FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DCF5FF44D6
              F4FF3EA976FF319F65FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4FCFF60DC
              F5FF44D6F4FF8EEEFAFF5DB4E6FF3B8FD9FFFF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3EDF8FFB3F4
              FCFF68D9F5FF6FCFF3FF599DD0FF73ABDDFF4F91C9FFFF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBAE4FFC3ED
              F8FFA8E2F8FF6CAEDDFFA5CFF4FFA5CFF4FFBDDBF7FF5393CBF7FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF002FBA
              E4FFA7D4F4FFC5E1F8FFCCE3F9FFCCE3F9FFBDDBF7FF4F90C9FDFF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF0050A8D9FF6AA5D8FFC9E1F7FFCBE3F8FF4295CAFF3182C2AEFF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF002FBAE4094FAADBEA5093CAFD4E90C8FF2F9DD2DF35A4DE19FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            TabStop = False
            OnClick = btnTranspClick
          end
          object cbProjeto64: TCheckBox
            Left = 362
            Top = 34
            Width = 239
            Height = 17
            Caption = 'Mesmos dados do manifesto para cada mapa'
            TabOrder = 9
          end
        end
        object dbgFatura: TJvDBUltimGrid
          Left = 0
          Top = 57
          Width = 895
          Height = 161
          Align = alClient
          Ctl3D = False
          DataSource = dsTempRomaneio
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = dbgFaturaCellClick
          OnDrawColumnCell = dbgFaturaDrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SELECIONADO'
              Title.Alignment = taCenter
              Title.Caption = 'Selecionar'
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_ROMANEIO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Mapa Separa'#231#227'o'
              Width = 105
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'rota'
              Title.Caption = 'Rota'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DEMISSAO'
              Title.Alignment = taCenter
              Title.Caption = 'Emiss'#227'o'
              Width = 111
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DESTINO'
              ReadOnly = True
              Title.Alignment = taCenter
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESO'
              Title.Alignment = taCenter
              Width = 94
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUSUARIO'
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Width = 105
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'conferencia'
              Title.Alignment = taCenter
              Title.Caption = 'Confer'#234'ncia'
              Width = 189
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'palete'
              Title.Alignment = taCenter
              Title.Caption = 'QT Palete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_MANIFESTO'
              Title.Alignment = taCenter
              Title.Caption = 'Manifesto'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'PLACA'
              Title.Alignment = taCenter
              Title.Caption = 'Placa'
              Visible = False
            end>
        end
        object DBGItens: TJvDBUltimGrid
          Left = 0
          Top = 218
          Width = 895
          Height = 118
          Align = alBottom
          Ctl3D = False
          DataSource = dtsQItensRoma
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          AlternateRowColor = 14089981
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DOC'
              Title.Caption = 'Tipo'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODCON'
              Title.Caption = 'CTe/OST'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FL_EMPRESA'
              Title.Caption = 'Filial'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ORIGEM'
              Title.Caption = 'Origem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESTINO'
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESO'
              Title.Caption = 'Peso'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VLRMER'
              Title.Caption = 'Valor NF.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = []
              Width = 80
              Visible = True
            end>
        end
        object pnManifesto: TPanel
          Left = 0
          Top = 336
          Width = 895
          Height = 278
          Align = alBottom
          TabOrder = 3
          object Label58: TLabel
            Left = 376
            Top = 10
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 6
            Top = 57
            Width = 43
            Height = 13
            Caption = 'Motorista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TLabel
            Left = 6
            Top = 101
            Width = 49
            Height = 13
            Caption = 'Cap. Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label34: TLabel
            Left = 8
            Top = 144
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 87
            Top = 101
            Width = 70
            Height = 13
            Caption = 'Cap. Cubagem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 171
            Top = 101
            Width = 42
            Height = 13
            Caption = 'Carga % '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 233
            Top = 101
            Width = 64
            Height = 13
            Caption = 'Qtd. Destinos'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 8
            Top = 10
            Width = 72
            Height = 13
            Caption = 'Transportadora'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label27: TLabel
            Left = 308
            Top = 101
            Width = 27
            Height = 13
            Caption = 'Custo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label30: TLabel
            Left = 389
            Top = 101
            Width = 37
            Height = 13
            Caption = 'Receita'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label31: TLabel
            Left = 468
            Top = 101
            Width = 38
            Height = 13
            Caption = 'Margem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label49: TLabel
            Left = 549
            Top = 100
            Width = 16
            Height = 13
            Caption = 'KM'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label28: TLabel
            Left = 618
            Top = 101
            Width = 80
            Height = 13
            Caption = 'Valor Mercadoria'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 705
            Top = 101
            Width = 24
            Height = 13
            Caption = 'Peso'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 490
            Top = 57
            Width = 37
            Height = 13
            Caption = 'Ve'#237'culo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label33: TLabel
            Left = 560
            Top = 10
            Width = 50
            Height = 13
            Caption = 'UF Origem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label43: TLabel
            Left = 635
            Top = 10
            Width = 69
            Height = 13
            Caption = 'Cidade Origem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label42: TLabel
            Left = 607
            Top = 57
            Width = 53
            Height = 13
            Caption = 'UF Destino'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label44: TLabel
            Left = 675
            Top = 57
            Width = 33
            Height = 13
            Caption = 'Cidade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TLabel
            Left = 399
            Top = 57
            Width = 64
            Height = 13
            Caption = 'Placa Carreta'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 307
            Top = 57
            Width = 27
            Height = 13
            Caption = 'Placa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label50: TLabel
            Left = 784
            Top = 144
            Width = 29
            Height = 13
            Caption = 'Isca 1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label51: TLabel
            Left = 783
            Top = 185
            Width = 29
            Height = 13
            Caption = 'Isca 2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label53: TLabel
            Left = 627
            Top = 57
            Width = 20
            Height = 13
            Caption = 'Filial'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object labeltransp: TLabel
            Left = 8
            Top = 33
            Width = 69
            Height = 13
            Caption = '                       '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Labelplaca: TLabel
            Left = 307
            Top = 78
            Width = 69
            Height = 13
            Caption = '                       '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cbOperacao: TComboBox
            Left = 376
            Top = 29
            Width = 172
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = cbOperacaoChange
            OnExit = cbOperacaoExit
          end
          object cbMotorista: TComboBox
            Left = 6
            Top = 73
            Width = 293
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnExit = cbMotoristaExit
          end
          object edPesoLimiteVeiculo: TJvCalcEdit
            Left = 6
            Top = 117
            Width = 69
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object JvMemo1: TJvMemo
            Left = 8
            Top = 160
            Width = 481
            Height = 109
            Lines.Strings = (
              '')
            MaxLength = 300
            TabOrder = 3
          end
          object edQtdCubagem: TJvCalcEdit
            Left = 87
            Top = 117
            Width = 70
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 4
            DecimalPlacesAlwaysShown = False
          end
          object edPercentCarga: TJvCalcEdit
            Left = 171
            Top = 117
            Width = 51
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 5
            DecimalPlacesAlwaysShown = False
          end
          object edQtdDestino: TJvCalcEdit
            Left = 233
            Top = 117
            Width = 66
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 6
            DecimalPlacesAlwaysShown = False
          end
          object edCusto: TJvCalcEdit
            Left = 308
            Top = 117
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 7
            DecimalPlacesAlwaysShown = False
          end
          object edReceita: TJvCalcEdit
            Left = 388
            Top = 117
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 8
            DecimalPlacesAlwaysShown = False
          end
          object edMargem: TJvCalcEdit
            Left = 468
            Top = 117
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##0.00%'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 9
            DecimalPlacesAlwaysShown = False
          end
          object edKm: TJvCalcEdit
            Left = 548
            Top = 117
            Width = 62
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '#,###0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 10
            DecimalPlacesAlwaysShown = False
          end
          object edVlrm: TJvCalcEdit
            Left = 618
            Top = 117
            Width = 80
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '#,###,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 11
            DecimalPlacesAlwaysShown = False
          end
          object edPeso: TJvCalcEdit
            Left = 706
            Top = 117
            Width = 71
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 12
            DecimalPlacesAlwaysShown = False
          end
          object edVeiculo: TJvMaskEdit
            Left = 486
            Top = 73
            Width = 113
            Height = 21
            TabStop = False
            Color = clSilver
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 13
            Text = ''
          end
          object cbCidaded: TJvComboBox
            Left = 673
            Top = 73
            Width = 194
            Height = 21
            DropDownCount = 20
            TabOrder = 14
            Text = ''
            OnExit = cbCidadedExit
          end
          object cbCidadeO: TJvComboBox
            Left = 636
            Top = 29
            Width = 231
            Height = 21
            DropDownCount = 20
            TabOrder = 15
            Text = ''
            OnExit = cbCidadeOExit
          end
          object cbUFO: TJvComboBox
            Left = 560
            Top = 30
            Width = 60
            Height = 21
            DropDownCount = 20
            TabOrder = 16
            Text = ''
            OnExit = cbUFOExit
            Items.Strings = (
              ''
              'AC'
              'AL'
              'AM'
              'AP'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MG'
              'MS'
              'MT'
              'PA'
              'PB'
              'PE'
              'PI'
              'PR'
              'RJ'
              'RN'
              'RO'
              'RR'
              'RS'
              'SC'
              'SE'
              'SP'
              'TO'
              '')
          end
          object cbUF: TJvComboBox
            Left = 607
            Top = 73
            Width = 60
            Height = 21
            DropDownCount = 20
            TabOrder = 17
            Text = ''
            OnExit = cbUFExit
            Items.Strings = (
              ''
              'AC'
              'AL'
              'AM'
              'AP'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MG'
              'MS'
              'MT'
              'PA'
              'PB'
              'PE'
              'PI'
              'PR'
              'RJ'
              'RN'
              'RO'
              'RR'
              'RS'
              'SC'
              'SE'
              'SP'
              'TO'
              '')
          end
          object cbParaFilial: TCheckBox
            Left = 756
            Top = 57
            Width = 77
            Height = 17
            Caption = 'Para Filial'
            TabOrder = 18
            Visible = False
            OnExit = cbParaFilialExit
          end
          object cbPlaca: TJvComboBox
            Left = 305
            Top = 73
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 19
            Text = ''
            OnChange = cbPlacaChange
          end
          object cbPlaca_carreta: TJvComboBox
            Left = 397
            Top = 73
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 20
            Text = ''
            OnChange = cbPlaca_carretaChange
          end
          object cbTransp: TJvDBLookupEdit
            Left = 8
            Top = 29
            Width = 353
            Height = 21
            DropDownCount = 20
            LookupDisplay = 'NOME'
            LookupField = 'CODIGO'
            LookupSource = dtsTrans
            TabOrder = 21
            Text = ''
            OnExit = cbTranspExit
          end
          object cbIsca1: TJvComboBox
            Left = 784
            Top = 163
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 22
            Text = ''
          end
          object cbIsca2: TJvComboBox
            Left = 784
            Top = 200
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 23
            Text = ''
          end
          object ParaFilial: TJvComboBox
            Left = 625
            Top = 73
            Width = 194
            Height = 21
            DropDownCount = 20
            TabOrder = 24
            Text = ''
            Visible = False
            OnChange = ParaFilialChange
          end
          object rgAdiantamento: TRadioGroup
            Left = 495
            Top = 227
            Width = 170
            Height = 42
            Caption = 'Adiantamento Contrato de Frete'
            Columns = 2
            Items.Strings = (
              'Sim'
              'N'#227'o')
            TabOrder = 25
          end
          object RGPedagio: TRadioGroup
            Left = 671
            Top = 227
            Width = 214
            Height = 42
            Caption = 'Ped'#225'gio'
            Columns = 3
            Items.Strings = (
              'N/A'
              'Ida'
              'Ida e Volta')
            TabOrder = 26
          end
          object rgPamcard: TRadioGroup
            Left = 495
            Top = 163
            Width = 170
            Height = 42
            Caption = 'PamCard'
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Sim'
              'N'#227'o')
            TabOrder = 27
          end
        end
        object Panel10: TPanel
          Left = 119
          Top = 163
          Width = 628
          Height = 85
          Color = clRed
          ParentBackground = False
          TabOrder = 4
          Visible = False
          object Label20: TLabel
            Left = 7
            Top = 6
            Width = 50
            Height = 16
            Caption = 'Motivo :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edMotivo: TEdit
            Left = 7
            Top = 28
            Width = 606
            Height = 21
            MaxLength = 100
            TabOrder = 0
          end
          object btnSalva_motivo: TBitBtn
            Left = 530
            Top = 54
            Width = 70
            Height = 24
            Caption = 'Salvar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000BA6A368FB969
              35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
              32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
              ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
              B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
              B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
              C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
              B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
              BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
              BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
              76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
              C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
              77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
              C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
              77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
              C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
              65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
              C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
              E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
              CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
              EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
              D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
              F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
              D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
              F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
              D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
              F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
              3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
              39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
            TabOrder = 1
            OnClick = btnSalva_motivoClick
          end
        end
      end
      object tbMan: TTabSheet
        Caption = 'Manifesto'
        ImageIndex = 1
        OnShow = tbManShow
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object btnCanMan: TBitBtn
            Left = 428
            Top = 4
            Width = 105
            Height = 25
            Hint = 'Cancela Manifesto'
            Caption = 'Cancelar'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
              33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
              993337777F777F3377F3393999707333993337F77737333337FF993399933333
              399377F3777FF333377F993339903333399377F33737FF33377F993333707333
              399377F333377FF3377F993333101933399377F333777FFF377F993333000993
              399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
              99333773FF777F777733339993707339933333773FF7FFF77333333999999999
              3333333777333777333333333999993333333333377777333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = btnCanManClick
          end
          object btnImprMan: TBitBtn
            Left = 546
            Top = 4
            Width = 105
            Height = 25
            Hint = 'Impri o Manifesto'
            Caption = 'Manifesto'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btnImprManClick
          end
          object btnSefaz: TBitBtn
            Left = 317
            Top = 4
            Width = 105
            Height = 25
            Hint = 'Envia ao Sefaz'
            Caption = 'Enviar Sefaz'
            Glyph.Data = {
              B60A0000424DB60A00000000000036000000280000001F0000001C0000000100
              180000000000800A000000000000000000000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFEFCFFFFFFFAF9E4BB9EF9F4F2FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFEFEFFFFF9F2EFE9DBBBD7A76CF1B292FFFFFFFEFFFFFFFEFFFEFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF
              FDFDFFFFFAF4E5E0C79DE9D58EEBDD92D89A61EEBB9FFFFFFFFCFDFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFFFCFFFFFEFE
              FDFCFFFFEEE4C7E2D183E6D58CE1D594E8DA97C77643F3DABEFDFFFFFFFFFEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFFFFFF
              FFFFFFFFFFFFFFE4DDB3E3CE8BEAD391E2DA98EBC98CDB9F7DFEFFFFFEFFFEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F9FAD1D7
              D8D9D6D9F5EEF2FFFFFFF4E8C1EBD892E4D594E4CF90DB8B58FFFFFBFDFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFFFEFFFFF7FFF975B08A34794D537F
              644C7E5C486353898D89CFC598E2D68DE3D892EFE09DD29663DD966BF4E2CCFF
              FEFAFEFFFFFFFFFFFDFEFEFFFFFDFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFEFFFFFFFF4D8F674E655ED6C8CCFFFF
              FFD9F3F297BDB5588E645F7444B4A571DDC98CE8DB98E8DC97DEC27ED79361E1
              A175DA9974F6DCCAFDFFFFFBFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0E0CD466F5AB9A6A7ECFFFF86CB
              D38EB5B5CAD2B5EEDBA1D2D58D507B4DA29868D1C080E8D791E9DA9AEEDEA0E6
              DC96E6CE92CF7B48FEF7F2FEFFFEFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFFFF85B3957C8080DFDFE37ECACE716A
              6CE7D9C8EAD585E8D48DEDD68FEFE1A1558B55979062C8B87EEBD999E6D48EE4
              D490EDDF9ED39D5FE6AB8AFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFF76AD8F8B8582B8DCE0829897A5A6
              A6FEF8E7E4D386E4D48BE4D38AE5D58BFFEBAB6893629C976AD0BA7FE7D992E4
              D38FE4D393EEE3A1D5774DFAF8EDFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFF7DAF91908E8AA4BDC4829291B9C0
              BEFFFEEDD0C483D0C7A6CBC2A8B9B594A49964E1CD8A668D58B0A270D4C685E7
              D793E5D58EE9DF9ED2915BF4D2BDFEFFFFFEFEFFFEFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADD0BA7C857C8AA1A589979AC2BB
              B4E7E0C1FFFDFBFFFFFFFFFFFFFFFFFFFFFFFFB5B3A497976865864FB2A56CDD
              CF90E4D58EEADB98DFB278E9B39AFDFFFFFEFDFFFDFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFFE4F7E9667E6D7C8F9294A1A8E7DA
              8BE8E3BFFBFFFFFFFFFFE3E3DAD0CFBAFFFFFFFFFFFFDDD7D0627C528B8D5DBC
              AC75E7DA9CEED892D8BB7BEAA487FEFFFFFEFEFFFFFFFF000000FFFFFFFEFFFE
              FEFEFEFDFEFFFFFEFFFFFFFFF9FFFFFFFFFFFFFFFF618F7991858C91A287EBD7
              8AECE9CBFFFEFFFFFFFF847C69D3C27DD9CF91FFFFFFFFFFFFCBC9C3507347A4
              A268D4C084E3D793E5CB89CF7E44F7D9C5FEFFFFFDFFFE000000FFFFFFFFFEFE
              FDFFFCFBFFFFFFFEFDE3CCBBF2BBA4FAECE2FFFAED88B081959968D8BF7BE8DB
              8FE7DEB0FFFFFFFFFFFFB8B5B0B1A35EEEDA8CD5CD97C3BC8AD0BE86CECA9070
              8A56B6A26DE7D591E4DA95E6CF8DD7774BFAF2E2FFFFFF000000FFFFFFFFFEFF
              FFFEFFF6EBD6DBB98FEDD290E1BF83C67C4CE8C689EADD9A5E8A53B5A170E8D7
              92DDCC8CFFFFFFFDFFFFFFFFFFADAA9AD3CAB6D0CCB3D5CCB8D7D2BFC3B49E5D
              8A55A6996BCEBD7FE7D68FEADD9DDCB778D68558FBFCF2000000FFFFFFFFFFFF
              FDFAF4DFD089EAD794E6D892E8D999ECDB9EECDC99EEDC9587B0749D9469CBB6
              7DE5D686EBEAD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF74
              7851748E5DB6A270E9DA96E5D995E4DC9DDAA76AE7BFA5000000F7F8F7FBFBFF
              E5E0B3E4CE88E6D991DDC989E5D48FDBC981D6C180D9C788EBDCA1638E55AB9D
              68DFD38FE0CF92FFFFFDFDFFFFFBFBFA706A50D3C48CD7CDAAFFFFFFFFFFFF9B
              907D5D8E5BB1A26FD8C993E7DB93E7D996E2D18AE9AA8C000000F2F2F26C6C6B
              F0EEE8AAA189C4BB85D6D3C9A49D6AE9ECE3E7E7DEDFDDD6AFA28B859D62A7B1
              96A1937ED7C77CE3DAB3FFFFFFFFFFFFABAA9AA69A5BD7C784FFFFFFFFFFFFAE
              B0A3849E6D989061C2B480E9DE97E3DC94E1C481E2BFAC000000FFFFFF727172
              E3E3E7FFFFFF716B52FFFFFF6F673BFFFFFF84846AD9D0A0FFFFFF9F9583D6D9
              C1CAC8C98A824FEFDC88E6E0BFFFFFFFFFFFFFCDCBC5D6D1C6FFFFFFFFFFFFB0
              AD9FB9C087718A60B5A76BEBDA99DFD6AFEEE1CAFDFEF7000000FFFFFF979696
              ECEFEEC3C6C5908F8EFFFFFF7E7655FFFFFD838164E0CF86DED090FFFFFF8581
              65FCFDFF6A5B44B2A779C7BB76E0D3A7F9F9F1FFFFFFFFFFFFFFFFFFF9FDF991
              8A63E8E091638E55AFA671A0A58EB9B9C3FFFFFFFEFFFF000000FCFCFCD2D2D2
              FFFFFF494949FFFFFFD1D3D495967DECE9D49F9E90D2C37DE3D382FFFFFF8984
              70FFFFFFFFFFFFFFFFFF8B7A52EDDC93D9CE82D2CA95D7CCA7CDC497B6A676DB
              C395FAECCE5B89708B999D879FAEB3B0AFFFFFFFFFFFFF000000F9F9F9FFFFFF
              FFFFFF595A5AFFFFFFFFFFFFB5B2A3CAC4A6C1C1BBAD9E67DFD082FFFEFD8F8A
              71FFFFFF646A4F50653A7C7F4DD0BE92D3C286F3DB95F5E8A8EDDAA9F7E1C7FF
              FBFAF4FFFF577E6382A5A47F9594BEBBBBFFFFFFFFFFFF000000FCFCFCFFFFFF
              E2E2E26F6F70FFFFFFFBFCF9E2E1DA99936FFDFCFFBCBAACE1E1D6FFFFFF8B7F
              53FBF7F0F9F8F8F0F9F8D7E2DD494B29C6B37E8A8271A4A29EEAE5E4FFFFFFFF
              FFFFA8DFC15B63557BB9C06C6B6EE2E1E1FFFFFFFFFFFF000000FFFFFFF0F0F0
              B6B6B6C5C5C6FFFFFFECEDEAD2D4D2B2A670E4D6B0DBDBC0D6CCAAB1A86BECED
              D0EFEDE1D4CFB9D1C49FDCCFA79BA9745A8A514A66554A5D65586168817F8B9D
              B49A2D764B558B9162858DA19A9AFFFFFFFFFFFFFFFFFF000000FFFFFFFEFEFE
              FFFFFFFFFFFFFEFDFEFFFFFFFFFEFFFFFFFFF8F5ECDED192DED183E5D27EFCF9
              F6FFFEFFFFFFFFFDFFFDFFFFFFF6EED2E8E8BDB2D6C15B9671387C572E804D38
              8764688E9489A0A2C4C0C1FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFFFFFFFFFFFFFCE8E2B8F9F9
              F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFFFFFFFFFFFFFFFFFBF4F8FA
              F2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = btnSefazClick
          end
          object btnStatus: TBitBtn
            Left = 203
            Top = 4
            Width = 105
            Height = 26
            Hint = 'Status Sefaz'
            Caption = 'Status Sefaz'
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D58871D58871D58871D58
              87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF1D588756A2D6387BAC1D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D588756A2D6458CBE1D58
              87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF1D58874D97CA56A2D61D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D58873C80B156A2D61D58
              87FFFFFFFFFFFFFFFFFF5BA4B20D92C50291CA0290C90892C83E9BB8FFFFFFFF
              FFFFFFFFFF1D58872C6B9C56A2D61D5887FFFFFFFFFFFFFFFFFF43ABBE7DDBF2
              90E0F58EDFF4A3E5F609B9E50297CD59B6CAB5D3CDAABDA30681B1539ED42B6A
              9A1D5887FFFFFFFFFFFF56A9B585DCF32AC4EB22C2EB61D3F097E2F5B3EAF881
              E1ED2AC9DA17CCDC21A1CF56A2D63A7EB01D5887FFFFFFFFFFFF6DA9AE7ED9EF
              62D3F039C8EC3BC8EC6BD6F197E2F5D5F6F9A9ECF49DEBF289C7D056A2D64692
              C71D5887FFFFFFFFFFFF7DA9AA27B2D189DEF42FC5EB33C7EC67D5F092E1F5BD
              F0F6A0EAF277E3EC83D4DD428CC07DB7DF1D5887FFFFFFFFFFFF84AAA93AAFC6
              85DDF32AC4EB2AC4EB66D5F08BDFF4B2EEF5B3EEF57EE3EE4CD1E03579AC6EAF
              DB1D5887FFFFFFFFFFFF83AAAA49ABBC85DDF38BDFF486DEF4AAE7F7BDEDF99F
              EAF2C4F2F880E3EE73E0ED356E978FC1E4215F8F1D5887FFFFFF83AAAA5CAAB5
              0792C80090CA0092CB0093CC7CD9F1CCF4F9DFF8FBBEF1F7B5EEF5A0B6C286BC
              E22E72A41D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0295CD2D
              C3E9C0EDF89BE3F506B7E40599C0EFF7FB3684BB1D5887FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF7FA9AE52A5B626A2C5249FC35C9AA33465881D58
              871D58871D5887FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = False
            OnClick = btnStatusClick
          end
          object btnCusto: TBitBtn
            Left = 8
            Top = 4
            Width = 81
            Height = 25
            Hint = 'Libera'#231#227'o de Manifesto'
            Caption = 'Custo'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E000000000000000000003E9DCA7B3696
              D1E53390CCEB328BCBED3B95C287FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0042ACE1CDC4EB
              F7FF7FE1F6FF9FE6F7FF328AC9EF3A90BA91FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFC6F4
              FBFF43D6F1FF48DBF5FF82E1F5FF3188C8F0398DB798FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3F2BBEF
              FAFF39D1F1FF28C5EEFF4EDCF6FF85E2F7FF328BCAEE398BB59BFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFF0FC
              FEFFB0EEFAFF43D8F4FF28C8EEFF41D7F4FF89E2F7FF328BCBED3888B2A3FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0
              E3FF44B0E3FFABEAF9FF4ED8F3FF2BC9EFFF3DD6F3FF8AE1F7FF328ACAEE3383
              B8DC297DD6FF2C85D8FF368EC1CBFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF0044B0E3FFF1FCFEFFBBF1FBFF7BE4F6FF28D2F0FF37D4F5FF83E0F6FF3EA9
              E3FFA0F3FCFFA9F5FCFF2B82D7FF358BBFCFFFFFFF00FFFFFF00FFFFFF00FFFF
              FF0044B0E39644B0E3FF45B2E3FF76C5EAFFACEEFAFF39D6F2FF4DDBF5FF65E4
              F7FF3CCEF2FF32C9EFFF85EFFBFF2B81D7FF3588BBD4FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0044B0E30244B0E3966FC4EAFF80E5F7FF3DD1F1FF5DDB
              F5FF69DFF6FF50D7F3FF34CDEFFF85EFFBFF297FD6FF3486BAD8FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFD5F7FCFF89E7F8FF7EE4F7FF7EE4
              F7FF7EE4F7FF82E5F7FF47D6F2FF38CEF0FFAEF5FCFF297CD6FFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFBEF2FBFF7EE4F7FF7EE4F7FF81E5
              F7FF94E9F8FFBCF1FBFF8BDAF3FF49DDF5FFC1F8FDFF3090DAFFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E3FFDEF8FCFF8DE7F8FF7EE4F7FF94E9
              F8FFBCE9F8FF44B0E3FF42ACE3FFEEFCFEFF3298DDFF3995C8BFFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFCEF5FCFF8DE7F8FFA1EC
              F9FF44B0E3FF44B0E3FFFFFFFFFF39A1DFFF3995C8BFFFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFCEF5FCFF9EEB
              F9FFBEF2FBFFFEFFFFFF44B0E3FF42ABDF9CFFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0E3FFDEF8
              FCFFDEF8FCFF44B0E3FF44B0E396FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0044B0E39644B0
              E3FF44B0E3FF44B0E396FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = btnCustoClick
          end
          object btnMapa2: TBitBtn
            Left = 657
            Top = 4
            Width = 68
            Height = 25
            Caption = 'Mapa'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = btnMapa2Click
          end
          object btnDiario: TBitBtn
            Left = 730
            Top = 4
            Width = 105
            Height = 25
            Hint = 'Di'#225'rio de Bordo CRM'
            Caption = 'Di'#225'rio de Bordo'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
              52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
              FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
              D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
              FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
              FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
              BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
              FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
              FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
              A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
              B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
              A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
              CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
              B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
              FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
              FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
              F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
              A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
              F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
              F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
              8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
              F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
              F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
              F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
              90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
              D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
              BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = btnDiarioClick
          end
          object btnRefresh: TBitBtn
            Left = 95
            Top = 4
            Width = 39
            Height = 25
            Hint = 'Atualizar Tela'
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFDFD6E9B70FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF6197654F8853FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFB493D59D74
              D19668CE9263CB8E5EC98A5BC7875666945B569D5E53995A2A712F266B2B2366
              274C723AC6D7C7FFFFFFD7A175F8F2EDF7F0EAF6EDE6F4EAE2F3E7DE529A5860
              A7688DCD978ACB9487CA9184C98E81C88C60A7684D8250C6D7C7D9A47AF9F3EE
              EBD2BEFFFFFFEBD3BFFFFFFFFFFFFF75B17B62A96A5DA46535803B317A365197
              5882C88D5BA1637AA27CDDA87EF9F3EFEBD0BAEBD0BBEBD0BBF4E6DAF4EFE7F9
              F1EC70AB72609F62F4E6D9F4E6D96197632D7533296F2E407C44DFAA82F9F3EF
              EACEB7FFFFFFEBD0BBFFFFFFFFFFFFFFFFFFF9F2EC82B886FFFFFFFFFFFFF7F0
              EBC88D5FFFFFFFFFFFFFE1AE87FAF4F0EACBB2EACCB3EACCB3F6E9DEF9F1EAF9
              F2EBF3E5D9F5E6DBF3E3D77CAC78F5EFE9C48654FFFFFFFFFFFFE3B18CFAF6F1
              EAC9AEFFFFFFEAC9B074C57E5DB8685AB3647CBB7DFFFFFFFFFFFF68AC6F6EAA
              72C58655F8FBF8FFFFFFE5B48FFAF6F2E9C6AAE9C6ACEAC7AC9ECF988ECF97AA
              D9B17AC38357AF6152A95C6FB7786BB37468924FFFFFFFFFFFFFE7B794FBF7F4
              E9C3A6FFFFFFE8C4A9D9F1DC84CF8D94D29CABDAB2A8D9AFA5D8ADA2D6AA9FD5
              A76CB4745FA566FFFFFFE9BA98FBF7F4E9C3A6E9C3A6E9C3A6EFD3BDD3E0C38B
              CF9063C06F60BC6B5DB76779C28275BE7E73A15CFFFFFFFFFFFFEBBD9BFBF7F4
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7AC58382C5
              89D1976AFFFFFFFFFFFFECBF9EFBF7F49CD5A598D3A194D09D90CE988BCB9387
              C98EA3D5A8B9DFBCCDE8CF7FC987F9F6F2D49B6FFFFFFFFFFFFFEFC6A8FBF7F4
              FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7F4FBF7
              F4D8A378FFFFFFFFFFFFF7E1D2F1C8ACEDC09FEBBE9DEBBC9AE9BA96E7B793E6
              B590E4B28CE2AF88E0AC84DDA980DCA57DE2B696FFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            OnClick = btnRefreshClick
          end
          object btnLimparRepom: TBitBtn
            Left = 143
            Top = 4
            Width = 39
            Height = 25
            Hint = 'Limpar Viagem Repom'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
              B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
              E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
              EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
              D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
              E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
              D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
              E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
              D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
              E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
              DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
              DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
              DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
              ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
              E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
              F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
              E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
              F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
              FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
              FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
              F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
              D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            OnClick = btnLimparRepomClick
          end
        end
        object dgCtrc: TJvDBUltimGrid
          Left = 0
          Top = 58
          Width = 895
          Height = 392
          Align = alClient
          Ctl3D = False
          DataSource = dtsQManifesto
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = dgCtrcDrawColumnCell
          OnDblClick = dgCtrcDblClick
          TitleArrow = True
          AutoSizeColumns = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAINC'
              Title.Caption = 'Data'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MANIFESTO'
              Title.Caption = 'Manifesto'
              Width = 63
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ROMANEIO'
              Title.Caption = 'Mapa Sep.'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_TRANSP'
              Title.Caption = 'Transportador'
              Width = 169
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PLACA'
              Title.Caption = 'Placa'
              Width = 73
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MDFE_CHAVE'
              Title.Caption = 'Chave Sefaz'
              Width = 179
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MDFE_MENSAGEM'
              Title.Caption = 'Retorno Sefaz'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'VALOR_CUSTO'
              Title.Caption = 'Custo'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_RECEITA'
              Title.Caption = 'Receita'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_MONITORAMENTO'
              Title.Caption = 'SM'
              Width = 60
              Visible = True
            end>
        end
        object Panel11: TPanel
          Left = 118
          Top = 168
          Width = 628
          Height = 85
          Color = clRed
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label29: TLabel
            Left = 7
            Top = 6
            Width = 50
            Height = 16
            Caption = 'Motivo :'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EditCusto: TEdit
            Left = 7
            Top = 28
            Width = 606
            Height = 21
            MaxLength = 100
            TabOrder = 0
          end
          object btnSalvarCusto: TBitBtn
            Left = 530
            Top = 55
            Width = 70
            Height = 24
            Caption = 'Salvar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000BA6A368FB969
              35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
              32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
              ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
              B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
              B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
              C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
              B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
              88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
              BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
              F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
              BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
              76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
              C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
              77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
              C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
              77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
              C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
              65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
              C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
              E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
              CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
              EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
              D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
              F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
              D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
              F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
              D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
              F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
              3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
              39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
            TabOrder = 1
            OnClick = btnSalvarCustoClick
          end
        end
        object MStat: TJvMemo
          Left = 196
          Top = 80
          Width = 361
          Height = 285
          Color = 14089981
          Lines.Strings = (
            'MStat')
          TabOrder = 3
          Visible = False
          OnKeyDown = MStatKeyDown
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 532
          Width = 895
          Height = 82
          Align = alBottom
          Caption = 'Sefaz'
          TabOrder = 4
          object DBMemo1: TDBMemo
            Left = 2
            Top = 15
            Width = 891
            Height = 65
            Align = alClient
            DataField = 'MDFE_MENSAGEM'
            DataSource = dtsQManifesto
            TabOrder = 0
          end
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 450
          Width = 895
          Height = 82
          Align = alBottom
          Caption = 'CIOT'
          TabOrder = 5
          object DBMemo2: TDBMemo
            Left = 2
            Top = 15
            Width = 891
            Height = 65
            Align = alClient
            DataField = 'STATUS_VIAGEM'
            DataSource = dtsQManifesto
            TabOrder = 0
          end
        end
        object pnlRepom: TPanel
          Left = 0
          Top = 32
          Width = 895
          Height = 26
          Align = alTop
          TabOrder = 6
          Visible = False
          object Label63: TLabel
            Left = 118
            Top = 6
            Width = 76
            Height = 13
            Caption = 'Cart'#227'o Repom :'
            Transparent = True
          end
          object JvLabel1: TJvLabel
            Left = 381
            Top = 6
            Width = 34
            Height = 13
            Caption = 'CIOT :'
            Transparent = True
          end
          object edCartao2: TJvCalcEdit
            Left = 200
            Top = 1
            Width = 92
            Height = 21
            TabStop = False
            Color = clWhite
            DecimalPlaces = 0
            DisplayFormat = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object edCiot: TJvCalcEdit
            Left = 421
            Top = 1
            Width = 99
            Height = 21
            TabStop = False
            Color = clSilver
            DecimalPlaces = 0
            DisplayFormat = ',0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object btnSalvar: TBitBtn
            Left = 789
            Top = 1
            Width = 39
            Height = 22
            Hint = 'Limpar Viagem Repom'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
              1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
              96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
              98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
              36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
              6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
              3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
              6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
              42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
              96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
              42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
              FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
              4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
              FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
              54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
              C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
              597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
              71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
              5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
              75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
              FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
              9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
              A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
              52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnSalvarClick
          end
          object rbSemAdiantamento: TRadioButton
            Left = 544
            Top = 4
            Width = 113
            Height = 17
            Caption = 'Sem Adiantamento'
            TabOrder = 3
          end
          object rbComAdiantamento: TRadioButton
            Left = 663
            Top = 4
            Width = 113
            Height = 17
            Caption = 'Com Adiantamento'
            TabOrder = 4
          end
          object btnFechar: TBitBtn
            Left = 834
            Top = 1
            Width = 39
            Height = 22
            Hint = 'Fechar'
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
              9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
              71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
              9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
              FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
              A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
              FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
              AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
              FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
              B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
              FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
              B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
              3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
              BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
              66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
              BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
              47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
              C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
              FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
              C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
              FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
              CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
              FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
              D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
              FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
              D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
              FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
              D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = btnFecharClick
          end
        end
      end
      object tsManT: TTabSheet
        Caption = 'Manifesto Transfer'#234'ncia'
        ImageIndex = 2
        OnShow = tsManTShow
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object btnCanTransf: TBitBtn
            Left = 511
            Top = 5
            Width = 105
            Height = 25
            Hint = 'Cancela Manifesto Transfer'#234'ncia'
            Caption = 'Cancelar'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
              33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
              993337777F777F3377F3393999707333993337F77737333337FF993399933333
              399377F3777FF333377F993339903333399377F33737FF33377F993333707333
              399377F333377FF3377F993333101933399377F333777FFF377F993333000993
              399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
              99333773FF777F777733339993707339933333773FF7FFF77333333999999999
              3333333777333777333333333999993333333333377777333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            TabStop = False
            OnClick = btnCanTransfClick
          end
          object btnNovoTransf: TBitBtn
            Left = 250
            Top = 5
            Width = 124
            Height = 25
            Hint = 'Novo Manifesto Transfer'#234'ncia'
            Caption = 'Nova Transfer'#234'ncia'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
              0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
              33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            TabStop = False
            OnClick = btnNovoTransfClick
          end
          object btnGeraTransf: TBitBtn
            Left = 380
            Top = 5
            Width = 124
            Height = 25
            Hint = 'Gerar Manifesto Transfer'#234'ncia'
            Caption = 'Gerar Transfer'#234'ncia'
            Enabled = False
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
              FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
              96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
              92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
              BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
              CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
              D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
              EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
              E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
              E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
              8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
              E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
              FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
              C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
              CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
              CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
              D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
              9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            TabStop = False
            OnClick = btnGeraTransfClick
          end
        end
        object DBTransf: TJvDBUltimGrid
          Left = 0
          Top = 32
          Width = 895
          Height = 340
          Align = alClient
          Ctl3D = False
          DataSource = dtsMdTransf
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = DBTransfCellClick
          OnDrawColumnCell = DBTransfDrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'Selecionado'
              Title.Alignment = taCenter
              Title.Caption = 'Selecionar'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nr_manifesto'
              Title.Alignment = taCenter
              Title.Caption = 'Manifesto'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PESO'
              Title.Alignment = taCenter
              Title.Caption = 'Peso'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 69
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Valor NF'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Destino'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 79
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'operacao'
              Title.Alignment = taCenter
              Title.Caption = 'Opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end>
        end
        object pnTransf: TPanel
          Left = 0
          Top = 372
          Width = 895
          Height = 242
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 2
          object Label3: TLabel
            Left = 423
            Top = 52
            Width = 37
            Height = 13
            Caption = 'Ve'#237'culo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label7: TLabel
            Left = 612
            Top = 52
            Width = 49
            Height = 13
            Caption = 'Cap. Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label10: TLabel
            Left = 691
            Top = 52
            Width = 70
            Height = 13
            Caption = 'Cap. Cubagem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label17: TLabel
            Left = 775
            Top = 52
            Width = 42
            Height = 13
            Caption = 'Carga % '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label18: TLabel
            Left = 774
            Top = 96
            Width = 24
            Height = 13
            Caption = 'Peso'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label21: TLabel
            Left = 7
            Top = 6
            Width = 72
            Height = 13
            Caption = 'Transportadora'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label22: TLabel
            Left = 531
            Top = 6
            Width = 43
            Height = 13
            Caption = 'Motorista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label23: TLabel
            Left = 167
            Top = 52
            Width = 27
            Height = 13
            Caption = 'Placa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label24: TLabel
            Left = 290
            Top = 52
            Width = 64
            Height = 13
            Caption = 'Placa Carreta'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label19: TLabel
            Left = 10
            Top = 52
            Width = 59
            Height = 13
            Caption = 'Filial Destino'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label35: TLabel
            Left = 11
            Top = 96
            Width = 53
            Height = 13
            Caption = 'UF Destino'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label36: TLabel
            Left = 11
            Top = 140
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label37: TLabel
            Left = 386
            Top = 96
            Width = 27
            Height = 13
            Caption = 'Custo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label38: TLabel
            Left = 481
            Top = 96
            Width = 37
            Height = 13
            Caption = 'Receita'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label39: TLabel
            Left = 566
            Top = 96
            Width = 38
            Height = 13
            Caption = 'Margem'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label40: TLabel
            Left = 660
            Top = 96
            Width = 80
            Height = 13
            Caption = 'Valor Mercadoria'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label41: TLabel
            Left = 290
            Top = 96
            Width = 67
            Height = 13
            Caption = 'Custo Prim'#225'rio'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label46: TLabel
            Left = 348
            Top = 6
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label47: TLabel
            Left = 87
            Top = 96
            Width = 33
            Height = 13
            Caption = 'Cidade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label64: TLabel
            Left = 502
            Top = 156
            Width = 29
            Height = 13
            Caption = 'Isca 1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label65: TLabel
            Left = 607
            Top = 156
            Width = 29
            Height = 13
            Caption = 'Isca 2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edPesoTransf: TJvCalcEdit
            Left = 774
            Top = 112
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object edCapPesoT: TJvCalcEdit
            Left = 612
            Top = 68
            Width = 69
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object edCapCubT: TJvCalcEdit
            Left = 691
            Top = 68
            Width = 70
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object edCargaT: TJvCalcEdit
            Left = 775
            Top = 68
            Width = 51
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
          end
          object DBLTransf: TJvDBLookupEdit
            Left = 9
            Top = 21
            Width = 329
            Height = 21
            DropDownCount = 20
            LookupDisplay = 'NOME'
            LookupField = 'CODIGO'
            LookupSource = dtsTrans
            TabOrder = 4
            Text = ''
            OnExit = DBLTransfExit
          end
          object cbMotoTransf: TComboBox
            Left = 531
            Top = 21
            Width = 293
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object cbPlacaTransf: TComboBox
            Left = 166
            Top = 68
            Width = 118
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            OnChange = cbPlacaTransfChange
          end
          object cbCarretaTransf: TComboBox
            Left = 290
            Top = 68
            Width = 108
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object edVeiculoTRansf: TJvMaskEdit
            Left = 424
            Top = 68
            Width = 160
            Height = 21
            TabStop = False
            Color = clSilver
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 8
            Text = ''
          end
          object cbFilial: TComboBox
            Left = 9
            Top = 68
            Width = 144
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            OnChange = cbFilialChange
          end
          object cbUFD: TComboBox
            Left = 10
            Top = 112
            Width = 69
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
            OnExit = cbUFDExit
          end
          object JvMemo2: TJvMemo
            Left = 11
            Top = 156
            Width = 449
            Height = 77
            Lines.Strings = (
              '')
            MaxLength = 300
            TabOrder = 12
          end
          object edCustoT: TJvCalcEdit
            Left = 386
            Top = 112
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 13
            DecimalPlacesAlwaysShown = False
          end
          object edRecT: TJvCalcEdit
            Left = 481
            Top = 112
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 14
            DecimalPlacesAlwaysShown = False
          end
          object edMargT: TJvCalcEdit
            Left = 566
            Top = 112
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##0.00%'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 15
            DecimalPlacesAlwaysShown = False
          end
          object edValorMT: TJvCalcEdit
            Left = 660
            Top = 112
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '#,###,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 16
            DecimalPlacesAlwaysShown = False
          end
          object edCusto_T: TJvCalcEdit
            Left = 290
            Top = 112
            Width = 72
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 17
            DecimalPlacesAlwaysShown = False
          end
          object cbOperacaoTransf: TComboBox
            Left = 348
            Top = 21
            Width = 172
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            DropDownCount = 20
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 18
            OnChange = cbOperacaoTransfChange
            OnExit = cbOperacaoTransfExit
          end
          object cbCidadeDt: TJvComboBox
            Left = 85
            Top = 112
            Width = 194
            Height = 21
            DropDownCount = 20
            TabOrder = 11
            Text = ''
            OnChange = cbCidadeDtChange
          end
          object cbIsca1T: TJvComboBox
            Left = 502
            Top = 175
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 19
            Text = ''
          end
          object cbIsca2T: TJvComboBox
            Left = 608
            Top = 175
            Width = 81
            Height = 21
            DropDownCount = 20
            TabOrder = 20
            Text = ''
          end
        end
      end
      object tsRoadNet: TTabSheet
        Caption = 'RoadNet'
        ImageIndex = 4
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 31
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Panel12: TPanel
            Left = 439
            Top = 1
            Width = 455
            Height = 29
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object btnRoadNet: TBitBtn
              Left = 337
              Top = 3
              Width = 118
              Height = 25
              Hint = 'EDI RoadNet'
              Caption = 'EDI RoadNet'
              Enabled = False
              Glyph.Data = {
                36050000424D3605000000000000360000002800000015000000140000000100
                18000000000000050000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                8FC07E338D17288F0A33A31A87CB818BFFCF8DFDC6FFFCFFA0FDD59FFCC754D0
                5F20C22D18B82A33C93BB4E6B5FFFFFFFEFEFCFEFEFFFFFFFF00FFFFFF529B3D
                137E00198A002CA310119C0013A40331BF34AAFFE2FFFAFF97FDC606C02207C4
                2A0DCD3322CF4301C31F00BF168EE495FFFDFFFEFEFEFFFFFF00C5DDBA0E7800
                298A0CE9F9E4FFFFFFF3FAF12BB8240BB41574E694FFFFFF2AD65507CE2F61DD
                7EFFFFFFFFFFFFBBF3CA0BCD2E00BD1FF2FDF6FFFEFFFFFFFF0077B662107A00
                BBDCB1FFFFFFFDFDFDFFFFFFCEEFD407BC1633D454FFFFFF00D23100D440FFFF
                FFFFFFFFFFFFFFFFFFFF62E08400CD23A9ECB4FFFFFFFFFFFF006EA8540A7E00
                DDF1D8FFFFFFFDFCFBFFFCFFFCFDFA00C21A2AD956FFFFFF00DB391AE75AFFFE
                FFFDFEFDFEFFFEFFFCFF7FEDA200D83393EAA9FFFFFFFFFFFF00A1C88F0E8700
                71BA65FFFFFFFFFFFFFFFFFFFFFFFD00C22430E159FFFFFF00E24816EA62FFFE
                FFFFFDFFFFFDFFFFFBFF12E55B00DD34D7F7E0FFFEFFFFFFFF00A7D39A1C8D00
                129A005EC653ACE2B1A8E9B1A6E5AB00D23223E057B7F8CC00E65718F365AFF7
                D09DFAC89CF9C81FF06E00EB5529E36DFFFFFFFFFEFEFFFFFF00127C00219100
                20A10B12A70A01B30C03BE1900C82618D84A0CDE4C00E74B0FF16404F46A00F5
                6000F76400F66500F1581CED6FFFFDFCFCFCFEFEFEFEFDFDFD0078B46273BF61
                73C36577CD7077D77973DC836EE1860AD44117E45C79EFA103EF630BF4797AFC
                B16CFDAF6AFDB1AFF9CEFFFDFFFFFBFFFFFDFFFFFFFFFFFEFF00FFFFFFFFFFFF
                FFFFFFFFFFFFE0F3E1E0F6E3D4F9D500D2392BE666EBFBF600EC5F12F879EDFD
                F8DEFCEEDEFCEEDAFDEBCCF9E2CEF9DECFF8DCCBF5D8DDF9E100FFFEFFFFFFFF
                50B2440AA40000B30400BE1500CC231BD94B0FDE5200E74909F36C04F66C00F4
                5F00FB6400FA6200F45C00F05900E94B00DD3800D1263FD85F00FFFFFF379721
                0E9D001DA90F53CC5C54D36353D86B0CD63D14DE5355ED8000ED630AF16251F5
                9B44FA9242F78E00EF5F0AEC6510E8595DE88447DF6D81E49500B4D4AB118D00
                39A221FFFFFFFFFFFFFFFFFFFFFFFF00CD282BDF5BFFFFFF00E14714EC6DFFFF
                FFFFFDFFFFFDFFE6F8F000E75100DC40FBFBFAFFFFFFFFFFFF0073B25C0E8400
                C7E2C0FFFFFFFBFCFEFAFFFCF4FFF700C12125D855FFFFFF00DC3619EA62FFFC
                FFFEFEFEFDFDFCFFFFFF77EC9A00D83199EEAEFFFFFFFEFFFF0070AF580E7D00
                D4E7CCFFFFFFFEFEFDFFFEFFEAF7F000BC1531D654FFFFFF00D43113E048FFFF
                FFFFFEFFFFFEFEFFFFFF85E89F00D42C97E6A5FFFFFFFFFFFF00A1CC92127C00
                5BA646FFFFFFFFFFFFFFFFFF65CB660ABC1458DB7AFFFFFF0CD04205D033A6EF
                B2FFFFFFFFFEFFFFFEFF18CF3F00C21FD8F6D9FFFFFFFFFFFF00FFFFFF1D7A00
                16840042A22980C47645BA330FA40B15B0129BFED1FFFEFF4AF2950EC21F00C8
                2D4CD66967DF7F14CC3508C4264AD259FFFFFFFEFEFEFFFFFF00FFFFFFE6F7E6
                29850B0B80000C8A0007930029A70E79F6AE90FFCFFFFEFF40FFAD50EB841BB9
                2200C21400BB1B00B90F50CC59FFFDFFFEFEFFFFFFFFFFFFFF00FEFEFEFFFFFF
                FFFFFFC9DFBE9FCB95BEDEB9FFFFFF6BFEBB82FDBDFFFEFF2BFD9955FFB2FFFC
                FFB8E7B7ABDFA9DEF4DFFFFFFFFFFBFFFBFDFBFFFFFFFFFFFF00FFFFFFFEFFFC
                FDFEFEFFFFFFFFFFFFFFFFFFF9FFFC54FBAB79FDBDFFFDFF18FE923AFC9DFFFE
                FFFFFFFFFFFFFFFFFFFFFEFFFFFCFFFCFEFEFEFFFFFFFFFFFF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              TabStop = False
              OnClick = btnRoadNetClick
            end
            object BitBtn2: TBitBtn
              Left = 212
              Top = 3
              Width = 111
              Height = 25
              Hint = 'Carrega Tarefas para Gerar Romaneio'
              Caption = 'Carregar'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF0051A6
                58B84FA356AEFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00418E4517408D43EC3F8B42B3FF00FF0054AB5CB369B3
                6FFF67B16DFF4FA456AEFF00FF004C9F52FFFF00FF00FF00FF00FF00FF00FF00
                FF00459449FF43924817429046F15CA160FF569C5AFF3F8C42B856AD5FEC71B8
                77FF83BF89FF68B16EFF50A457FF4EA255FFFF00FF00FF00FF00FF00FF00FF00
                FF0046964BFF4A974EFF5FA462FF71B075FF579E5BFF408D44AE58B0611757AE
                5FF171B877FF74B87BFF80BD86FF50A457FFFF00FF00FF00FF00FF00FF00FF00
                FF0048994DFF73B377FF63A968FF5AA15EFF429147AEFF00FF00FF00FF0058B0
                61175CB064FF84C18AFF86C18BFF52A759FFFF00FF00FF00FF00FF00FF00FF00
                FF004A9B4FFF79B67EFF74B379FF45954AFFFF00FF00FF00FF00FF00FF005AB3
                63FF59B161FF57AE5FFF55AC5DFF54AA5BFFFF00FF00FF00FF00FF00FF00FF00
                FF004B9E51FF4A9C50FF48994EFF47974CFF45954AFFFF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0062BF
                6DFF61BD6CFF60BB6AFF5EB968FF5DB767FFFF00FF00FF00FF00FF00FF00FF00
                FF0055AB5DFF53A95BFF52A759FF50A457FF4EA255FFFF00FF00FF00FF00FF00
                FF0063BF6DFF97D19FFF98D0A0FF5FB969FFFF00FF00FF00FF00FF00FF00FF00
                FF0057AE5FFF8CC692FF87C38DFF57AA5EFF50A55717FF00FF00FF00FF0065C3
                70AE7ECA87FF8ECD98FF97D19FFF60BC6BFFFF00FF00FF00FF00FF00FF00FF00
                FF0059B161FF8CC793FF7DBF85FF72B979FF52A759F150A5571767C673AE80CD
                8AFF9ED6A7FF83CC8CFF69C273FF62BE6CFFFF00FF00FF00FF00FF00FF00FF00
                FF005AB364FF59B162FF70BB78FF8DC794FF73BA7AFF52A85AEC68C774B881CE
                8BFF86CF90FF65C371F164C26F1763C06EFFFF00FF00FF00FF00FF00FF00FF00
                FF005CB666FFFF00FF0059B162AE71BB79FF6FB977FF54AA5CB3FF00FF0068C7
                74B367C673EC66C57217FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF0059B262AE58AF60B8FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              TabStop = False
              OnClick = BitBtn2Click
            end
            object BitBtn3: TBitBtn
              Left = 99
              Top = 3
              Width = 100
              Height = 25
              Hint = 'Volta Tarefa'
              Caption = 'Volta CT-e'
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
                B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
                E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
                EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
                D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
                E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
                D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
                D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
                E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
                DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
                DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
                DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
                ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
                E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
                F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
                E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
                F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
                FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
                FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
                F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
                D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
                FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              TabStop = False
              OnClick = BitBtn3Click
            end
          end
        end
        object JvDBUltimGrid1: TJvDBUltimGrid
          Left = 0
          Top = 31
          Width = 895
          Height = 522
          Align = alClient
          Ctl3D = False
          DataSource = dstTarefa
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = dgTarefaDrawColumnCell
          TitleArrow = True
          SelectColumnsDialogStrings.Caption = 'Selecione as colunas'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'Pelo menos uma coluna deve ser vis'#237'vel!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'CODCON'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'CT-e/OST'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 74
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FL_EMPRESA'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Filial'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_CLI_OS'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Cliente'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_EMP_ORI_TA'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Origem'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NM_EMP_DEST_TA_01'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Destino'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 211
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF_DESTINO'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'UF'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NR_PESO_TA_01'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Peso'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUSUARIO'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = 'Usu'#225'rio'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'palete'
              Title.Alignment = taCenter
              Title.Caption = 'Palete'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Visible = True
            end>
        end
        object Panel13: TPanel
          Left = 0
          Top = 553
          Width = 895
          Height = 61
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 2
          object Label54: TLabel
            Left = 112
            Top = 9
            Width = 51
            Height = 13
            Caption = 'Total Peso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label55: TLabel
            Left = 327
            Top = 9
            Width = 41
            Height = 13
            Caption = 'Total NF'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label56: TLabel
            Left = 434
            Top = 9
            Width = 51
            Height = 13
            Caption = 'Total Frete'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label57: TLabel
            Left = 218
            Top = 9
            Width = 64
            Height = 13
            Caption = 'Total Cubado'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label59: TLabel
            Left = 8
            Top = 9
            Width = 67
            Height = 13
            Caption = 'Total Volumes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label60: TLabel
            Left = 538
            Top = 9
            Width = 82
            Height = 13
            Caption = 'Total Documento'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object JvCalcEdit1: TJvCalcEdit
            Left = 112
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 0
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit2: TJvCalcEdit
            Left = 327
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 1
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit3: TJvCalcEdit
            Left = 434
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clTeal
            DisplayFormat = '##,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 2
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit4: TJvCalcEdit
            Left = 218
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit5: TJvCalcEdit
            Left = 8
            Top = 25
            Width = 95
            Height = 21
            TabStop = False
            Color = clSilver
            DisplayFormat = '##,##0.000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 4
            DecimalPlacesAlwaysShown = False
          end
          object JvCalcEdit6: TJvCalcEdit
            Left = 538
            Top = 25
            Width = 83
            Height = 21
            TabStop = False
            Color = clSilver
            DecimalPlaces = 0
            DisplayFormat = ',0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 5
            DecimalPlacesAlwaysShown = False
          end
        end
      end
    end
  end
  object iml: TImageList
    Left = 128
    Top = 88
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000000000008600000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000000000000000000000000000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000000000000000000000000000000000000000000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000000000000000000000860000000000000000000000000000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000000000000086000000860000008600000000000000000000000000000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000000000000000000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000860000000000000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00E7DFE700E7DF
      E700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DFE700E7DF
      E700E7DFE70000000000000000000000000000000000A59E9C00008600000086
      0000008600000086000000860000008600000086000000860000008600000086
      0000008600000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59E9C00A59E9C00A59E
      9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E
      9C00A59E9C0000000000000000000000000000000000A59E9C00A59E9C00A59E
      9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E9C00A59E
      9C00A59E9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object qrRota: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct r.descri rota, r.codsetcl'
      
        'from cyber.rodcon t left join cyber.rodcli c on c.codclifor = nv' +
        'l(t.terent,t.coddes)'
      
        '                    left join cyber.rodset r on r.codsetcl = c.c' +
        'odsetcl'
      'where t.codfil in(1,6,4,8,9) and r.codfil in(1,6,4,8,9)'
      
        'and r.codsetcl > 0 and t.codman is null and t.situac <> '#39'C'#39' and ' +
        't.datinc > to_date('#39'31/07/2014'#39','#39'dd/mm/yyyy'#39')'
      'and nvl(t.agecar,0)=0 and t.codlin is not null'
      'union'
      'select distinct r.descri rota, r.codsetcl'
      
        'from tb_conhecimento ct left join cyber.rodcli rd on (rd.codclif' +
        'or = case when ct.cod_redespacho = 0 then'
      
        '                                                       ct.cod_de' +
        'stinatario else ct.cod_redespacho end)'
      
        '                        left join cyber.rodset r on (r.codsetcl ' +
        '= rd.codsetcl)'
      'where ct.fl_empresa in(1,6,4,8,9) and r.codfil in(1,6,4,8,9)'
      
        'and r.codsetcl > 0 and ct.nr_manifesto is null and ct.fl_status ' +
        '= '#39'A'#39' and ct.dt_conhecimento > to_date('#39'01/03/2016'#39','#39'dd/mm/yyyy'#39 +
        ')'
      'and nvl(ct.nr_romaneio,0)=0'
      'union'
      'select distinct '#39'SEM ROTA'#39' rota, 0 codsetcl'
      
        'from cyber.rodcon t left join cyber.rodcli c on c.codclifor = nv' +
        'l(t.terent,t.coddes)'
      'where t.codfil in(1,6,4,8,9)'
      
        'and nvl(c.codsetcl,0) = 0 and nvl(t.agecar,0)=0 and t.situac <> ' +
        #39'C'#39' and t.datinc > to_date('#39'31/07/2014'#39','#39'dd/mm/yyyy'#39')'
      'and t.codman is null'
      'union'
      'select distinct r.descri rota, r.codsetcl'
      
        'from cyber.rodord t left join cyber.rodcli c on c.codclifor = nv' +
        'l(t.terent,t.coddes)'
      
        '                    left join cyber.rodset r on r.codsetcl = c.c' +
        'odsetcl'
      'where t.codfil in(1,6,4,8,9) and r.codfil in(1,6,4,8,9)'
      
        'and r.codsetcl > 0 and t.codman is null and t.situac <> '#39'C'#39'  and' +
        ' t.datinc > to_date('#39'31/07/2014'#39','#39'dd/mm/yyyy'#39')'
      'and nvl(t.agecar,0)=0'
      'union'
      'select distinct '#39'SEM ROTA'#39' rota, 0 codsetcl'
      
        'from cyber.rodord t left join cyber.rodcli c on c.codclifor = nv' +
        'l(t.terent,t.coddes)'
      'where t.codfil in(1,6,4,8,9)'
      
        'and nvl(c.codsetcl,0) = 0 and nvl(t.agecar,0)=0 and t.situac <> ' +
        #39'C'#39'  and t.datinc > to_date('#39'31/07/2014'#39','#39'dd/mm/yyyy'#39')'
      'and t.codman is null'
      '')
    Left = 12
    Top = 128
    object qrRotaROTA: TStringField
      FieldName = 'ROTA'
      ReadOnly = True
      Size = 40
    end
    object qrRotaCODSETCL: TBCDField
      FieldName = 'CODSETCL'
      ReadOnly = True
      Precision = 32
    end
  end
  object dsRota: TDataSource
    AutoEdit = False
    DataSet = qrRota
    Left = 52
    Top = 128
  end
  object QSite: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'login'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 0
      end>
    SQL.Strings = (
      'select site from tb_usuarios c where cod_usuario = :login'
      ''
      '')
    Left = 12
    Top = 176
    object QSiteSITE: TStringField
      FieldName = 'SITE'
      Size = 50
    end
  end
  object QDestino: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select distinct v.*, c.nr_oc, o.operacao, op.fl_smdfe'
      
        'from vw_gerar_manifesto v left join cyber.tb_coleta c on v.codco' +
        'n = c.doc and v.codfil = c.filial'
      
        '                          left join tb_ordem o on o.id_ordem = c' +
        '.nr_oc'
      
        '                          left join tb_operacao op on op.operaca' +
        'o = o.operacao'
      'where v.codfil in (1)'
      'and v.codsetcl = :1'
      ''
      'and nvl(op.fl_smdfe,'#39'N'#39') = '#39'N'#39
      'order by v.codcon')
    Left = 12
    Top = 220
    object QDestinoCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QDestinoINSERIDA: TBCDField
      FieldName = 'INSERIDA'
      ReadOnly = True
      Precision = 32
    end
    object QDestinoNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QDestinoRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object QDestinoDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
    end
    object QDestinoCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
    object QDestinoDOC: TStringField
      FieldName = 'DOC'
      ReadOnly = True
      FixedChar = True
      Size = 3
    end
  end
  object mdTemp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'Coleta'
        DataType = ftInteger
      end
      item
        Name = 'Inserida'
        DataType = ftInteger
      end
      item
        Name = 'reg'
        DataType = ftInteger
      end
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end>
    AfterOpen = mdTempAfterOpen
    Left = 60
    Top = 216
    object mdTempColeta: TIntegerField
      FieldName = 'Coleta'
    end
    object mdTempInserida: TIntegerField
      FieldName = 'Inserida'
    end
    object mdTempreg: TIntegerField
      FieldName = 'reg'
    end
    object mdTempnr_romaneio: TIntegerField
      FieldName = 'nr_romaneio'
    end
    object mdTempdestino: TStringField
      FieldName = 'destino'
      Size = 30
    end
    object mdTempdata: TDateField
      FieldName = 'data'
      DisplayFormat = 'DD/MM/YY'
    end
    object mdTempfilial: TIntegerField
      FieldName = 'filial'
    end
    object mdTempdoc: TStringField
      FieldName = 'doc'
      Size = 3
    end
    object mdTempconca: TStringField
      FieldName = 'conca'
      Size = 12
    end
    object mdTempid_road: TIntegerField
      FieldName = 'id_road'
    end
  end
  object mdTempromaneio: TJvMemoryData
    FieldDefs = <
      item
        Name = 'SELECIONADO'
        DataType = ftInteger
      end
      item
        Name = 'NR_OS'
        DataType = ftInteger
      end
      item
        Name = 'NR_ROMANEIO'
        DataType = ftInteger
      end
      item
        Name = 'DEMISSAO'
        DataType = ftDateTime
      end
      item
        Name = 'PESO'
        DataType = ftFloat
      end
      item
        Name = 'SUSUARIO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NR_MANIFESTO'
        DataType = ftInteger
      end
      item
        Name = 'conferencia'
      end>
    AfterScroll = mdTempromaneioAfterScroll
    OnFilterRecord = mdTempromaneioFilterRecord
    Left = 89
    Top = 272
    object mdTempromaneioSELECIONADO: TBCDField
      FieldName = 'SELECIONADO'
      Precision = 32
    end
    object mdTempromaneioNR_ROMANEIO: TBCDField
      DisplayLabel = 'Romaneio'
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object mdTempromaneioDESTINO: TStringField
      DisplayLabel = 'UF Destino'
      FieldName = 'DESTINO'
      Size = 2
    end
    object mdTempromaneioPESO: TFloatField
      DisplayLabel = 'Peso Total'
      FieldName = 'PESO'
      DisplayFormat = '#,##0.00'
    end
    object mdTempromaneioNR_MANIFESTO: TIntegerField
      FieldName = 'NR_MANIFESTO'
    end
    object mdTempromaneioDEMISSAO: TDateTimeField
      FieldName = 'DEMISSAO'
    end
    object mdTempromaneioSUSUARIO: TStringField
      FieldName = 'SUSUARIO'
    end
    object mdTempromaneioPLACA: TStringField
      FieldName = 'PLACA'
      Size = 8
    end
    object mdTempromaneioVEICULO: TStringField
      FieldName = 'VEICULO'
      Size = 30
    end
    object mdTempromaneioselecionou: TIntegerField
      FieldName = 'selecionou'
    end
    object mdTempromaneioconferencia: TStringField
      FieldName = 'conferencia'
      Size = 30
    end
    object mdTempromaneiovlrmer: TFloatField
      FieldName = 'vlrmer'
    end
    object mdTempromaneiototfre: TFloatField
      FieldName = 'totfre'
    end
    object mdTempromaneiopalete: TFMTBCDField
      FieldName = 'palete'
      Size = 0
    end
    object mdTempromaneiorota: TStringField
      DisplayWidth = 20
      FieldName = 'rota'
    end
    object mdTempromaneioKM_TOTAL: TBCDField
      FieldName = 'KM_TOTAL'
      Precision = 5
    end
  end
  object qrRomaneio: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select distinct nr_romaneio, demissao, sum(peso) peso, usuario, ' +
        'selecionado, uf_destino,'
      
        'sum(vlrmer) vlrmer, data_liberado, sum(totfre) totfre, dat_confe' +
        'rencia, filial_roma, rota, km_total from ('
      ''
      'select DISTINCT r.nr_romaneio, r.demissao,'
      
        '(select sum(pescal) from cyber.rodios i where i.codord = t.codig' +
        'o and i.filord = t.codfil) peso,'
      '0 as selecionado, md.estado uf_destino,'
      
        '(select sum(vlrmer) from cyber.rodios i where i.codord = t.codig' +
        'o and i.filord = t.codfil) vlrmer,'
      
        'r.data_liberado, nvl(sum(t.totfre),0) totfre, r.dat_conferencia,' +
        ' r.fl_empresa filial_roma, r.susuario usuario, r.rota, r.km_tota' +
        'l'
      
        'from cyber.rodord t left join tb_romaneio r on (r.nr_romaneio = ' +
        't.agecar)'
      
        '                    left join cyber.rodcli c on (c.codclifor = t' +
        '.codpag)'
      
        '                    left outer join cyber.rodfil o on (o.codfil ' +
        '= t.codfil)'
      
        '                    left join cyber.rodmun mo on (mo.codmun = o.' +
        'codmun)'
      
        '                    left outer join cyber.rodcli d on (d.codclif' +
        'or = nvl(t.terent, t.coddes))'
      
        '                    left join cyber.rodmun md on (md.codmun = d.' +
        'codmun)'
      'where r.nr_manifesto is null'
      
        'group by r.nr_romaneio, r.demissao, md.estado,r.data_liberado,r.' +
        'dat_conferencia, r.fl_empresa, r.susuario, t.codcon, t.codfil, t' +
        '.codigo, r.rota, r.km_total'
      'union all'
      'select DISTINCT r.nr_romaneio, r.demissao,'
      
        '(select case when sum(nvl(i.pesokg,0)) > sum(nvl(i.pescub,0)) th' +
        'en'
      
        '       sum(nvl(i.pesokg,0)) else sum(nvl(i.pescub,0)) end pescal' +
        ' from tb_cte_nf i where i.codcon = ct.nr_conhecimento and i.fl_e' +
        'mpresa = ct.fl_empresa) peso,'
      '0 as selecionado, md.estado uf_destino,'
      
        '(select sum(i.vlrmer) from tb_cte_nf i where i.codcon = ct.nr_co' +
        'nhecimento and i.fl_empresa = ct.fl_empresa) vlrmer,'
      
        'r.data_liberado, nvl(ct.vl_total,0) totfre, r.dat_conferencia, r' +
        '.fl_empresa filial_roma, r.susuario usuario, r.rota, r.km_total'
      
        'from tb_conhecimento ct left join tb_romaneio r on (r.nr_romanei' +
        'o = ct.nr_romaneio)'
      
        '                    left join cyber.rodcli c on (c.codclifor = c' +
        't.cod_pagador)'
      
        '                    left outer join cyber.rodfil o on (o.codfil ' +
        '= ct.fl_empresa)'
      
        '                    left join cyber.rodmun mo on (mo.codmun = o.' +
        'codmun)'
      
        '                    left outer join cyber.rodcli d on d.codclifo' +
        'r = ('
      
        '                      case when nvl(ct.cod_redespacho,0) = 0 the' +
        'n ct.cod_destinatario'
      '                        else ct.cod_redespacho end)'
      
        '                    left join cyber.rodmun md on (md.codmun = d.' +
        'codmun)'
      'where r.nr_manifesto is null)'
      'where filial_roma = :0'
      'and nr_romaneio > 0'
      
        'group by nr_romaneio, demissao, uf_destino, data_liberado, dat_c' +
        'onferencia, selecionado,usuario, filial_roma, rota, km_total'
      'order by 1,6')
    Left = 12
    Top = 272
    object qrRomaneioNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qrRomaneioDEMISSAO: TDateTimeField
      FieldName = 'DEMISSAO'
      ReadOnly = True
    end
    object qrRomaneioPESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 32
    end
    object qrRomaneioUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
      Size = 50
    end
    object qrRomaneioSELECIONADO: TBCDField
      FieldName = 'SELECIONADO'
      ReadOnly = True
      Precision = 32
    end
    object qrRomaneioUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      ReadOnly = True
      Size = 2
    end
    object qrRomaneioVLRMER: TBCDField
      FieldName = 'VLRMER'
      ReadOnly = True
      Precision = 32
    end
    object qrRomaneioDATA_LIBERADO: TDateTimeField
      FieldName = 'DATA_LIBERADO'
      ReadOnly = True
    end
    object qrRomaneioTOTFRE: TBCDField
      FieldName = 'TOTFRE'
      ReadOnly = True
      Precision = 32
    end
    object qrRomaneioDAT_CONFERENCIA: TDateTimeField
      FieldName = 'DAT_CONFERENCIA'
      ReadOnly = True
    end
    object qrRomaneioROTA: TStringField
      DisplayWidth = 20
      FieldName = 'ROTA'
    end
    object qrRomaneioKM_TOTAL: TBCDField
      FieldName = 'KM_TOTAL'
      ReadOnly = True
      Precision = 5
    end
  end
  object SP_ROMANEIO: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_GRAVA_ROMANEIO'
    Parameters = <
      item
        Name = 'vnew'
        DataType = ftFloat
        Direction = pdOutput
        Value = 0.000000000000000000
      end
      item
        Name = 'vromaneio'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'vos'
        DataType = ftInteger
        Value = Null
      end
      item
        Name = 'vufo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'vusua'
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = 'vemp'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'vufd'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'vcusto'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'vTipo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'vsusuario'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'vpesotoal'
        DataType = ftFloat
        Value = 0.000000000000000000
      end>
    Left = 84
    Top = 580
  end
  object QTarefa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select v.*, p.qt_palete'
      'from vw_tarefa_romaneio v'
      
        'left join tb_palet_romaneio p on p.cnpj_destino = v.cnpj_dest an' +
        'd p.cnpj_cliente = v.nr_cnpj_cli and v.nr_romaneio = p.numero an' +
        'd p.tipo = '#39'M'#39
      'where v.conca in ('#39'0'#39')'
      'and nr_manifesto is null'
      'order by v.nr_romaneio, v.uf_destino, v.codcon')
    Left = 8
    Top = 324
    object QTarefaNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object QTarefaCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QTarefaNM_EMP_ORI_TA: TStringField
      FieldName = 'NM_EMP_ORI_TA'
      Size = 80
    end
    object QTarefaNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 32
    end
    object QTarefaNR_CNPJ_CLI: TStringField
      FieldName = 'NR_CNPJ_CLI'
    end
    object QTarefaNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      Size = 80
    end
    object QTarefaVL_CTRC: TBCDField
      FieldName = 'VL_CTRC'
      Precision = 14
      Size = 2
    end
    object QTarefaNM_EMP_DEST_TA_01: TStringField
      FieldName = 'NM_EMP_DEST_TA_01'
      Size = 80
    end
    object QTarefaNR_PESO_TA_01: TBCDField
      FieldName = 'NR_PESO_TA_01'
      DisplayFormat = '###,##0.000'
      Precision = 32
    end
    object QTarefaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object QTarefaUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
    object QTarefaSUSUARIO: TStringField
      FieldName = 'SUSUARIO'
      Size = 50
    end
    object QTarefaDOC: TStringField
      FieldName = 'DOC'
      FixedChar = True
      Size = 3
    end
    object QTarefaCONCA: TStringField
      FieldName = 'CONCA'
      Size = 83
    end
    object QTarefaqt_palete: TBCDField
      FieldName = 'qt_palete'
    end
    object QTarefaCNPJ_DEST: TStringField
      FieldName = 'CNPJ_DEST'
    end
  end
  object qrManifestoCtrc2: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select v.*'
      '  from VW_MANIFESTO_CTRC v'
      ' where v.fl_empresa = 5'
      '   and v.nr_manifesto > 0'
      '   and v.nr_romaneio > 0'
      ' order by v.nr_romaneio')
    Left = 8
    Top = 380
    object qrManifestoCtrc2NR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object qrManifestoCtrc2DESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object qrManifestoCtrc2PLACA: TStringField
      FieldName = 'PLACA'
      Size = 8
    end
    object qrManifestoCtrc2DATA: TDateTimeField
      FieldName = 'DATA'
    end
    object qrManifestoCtrc2VEICULO: TStringField
      FieldName = 'VEICULO'
      Size = 30
    end
    object qrManifestoCtrc2NR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 32
    end
    object qrManifestoCtrc2NM_USUARIO: TStringField
      FieldName = 'NM_USUARIO'
    end
    object qrManifestoCtrc2NM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      Size = 80
    end
    object qrManifestoCtrc2CODPAG: TBCDField
      FieldName = 'CODPAG'
      Precision = 32
    end
    object qrManifestoCtrc2PLACA_CARRETA: TStringField
      FieldName = 'PLACA_CARRETA'
      Size = 8
    end
    object qrManifestoCtrc2COD_TRANSP: TBCDField
      FieldName = 'COD_TRANSP'
      Precision = 32
    end
    object qrManifestoCtrc2FL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object qrManifestoCtrc2NR_CTRC: TBCDField
      FieldName = 'NR_CTRC'
      Precision = 32
    end
    object qrManifestoCtrc2NR_CNPJ_CLI: TStringField
      FieldName = 'NR_CNPJ_CLI'
    end
    object qrManifestoCtrc2CODDESTINO: TBCDField
      FieldName = 'CODDESTINO'
      Precision = 32
    end
    object qrManifestoCtrc2ORIGEM: TStringField
      FieldName = 'ORIGEM'
      Size = 80
    end
    object qrManifestoCtrc2NR_CNPJ_CPF_DESTINO: TStringField
      FieldName = 'NR_CNPJ_CPF_DESTINO'
    end
    object qrManifestoCtrc2CIDADEDESTINO: TStringField
      FieldName = 'CIDADEDESTINO'
      Size = 40
    end
    object qrManifestoCtrc2CODIGODESTINO: TBCDField
      FieldName = 'CODIGODESTINO'
      Precision = 32
    end
    object qrManifestoCtrc2NM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      Size = 80
    end
    object qrManifestoCtrc2CODREM: TBCDField
      FieldName = 'CODREM'
      Precision = 32
    end
    object qrManifestoCtrc2UF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
  end
  object qrCalcula_Frete: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select conca, cub, peso, qtd, valor, frete from '
      
        '(select (c.codigo||c.codfil||'#39'OST'#39') conca, sum(n.pescub) cub, su' +
        'm(n.pesokg) peso, sum(n.quanti) qtd,'
      '   sum(n.vlrmer) valor, c.totfre frete'
      
        '  from cyber.rodios n left join cyber.rodord c on n.codord = c.c' +
        'odigo and n.filord = c.codfil'
      '     group by c.codigo, c.codfil, c.totfre'
      'union all'
      
        '  select (tcn.codcon||tcn.fl_empresa||'#39'CTE'#39') conca, sum(tcn.pesc' +
        'ub) cub, sum(tcn.pesokg) peso, sum(tcn.quanti) qtd,'
      '  sum(tcn.vlrmer) valor, tc.vl_total frete'
      
        '  from tb_cte_nf tcn inner join tb_conhecimento tc on tcn.codcon' +
        ' = tc.nr_conhecimento and tcn.fl_empresa = tc.fl_empresa'
      '  group by  tcn.codcon, tcn.fl_empresa, tc.vl_TOTAL)'
      ' where conca = :0'
      '')
    Left = 104
    Top = 380
    object qrCalcula_FreteCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FretePESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FreteQTD: TBCDField
      FieldName = 'QTD'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FreteVALOR: TBCDField
      FieldName = 'VALOR'
      ReadOnly = True
      Precision = 32
    end
    object qrCalcula_FreteFRETE: TBCDField
      FieldName = 'FRETE'
      ReadOnly = True
      Precision = 32
    end
  end
  object qMapa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    AfterScroll = qMapaAfterScroll
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct r.nr_romaneio, t.codigo cod_ta, d.razsoc destino' +
        ', rt.descri rota, rd.razsoc cliredes, '
      
        'case when t.terent > 0 then rd.endere || '#39'-'#39' || rd.bairro else d' +
        '.endere || '#39'-'#39' || d.bairro end localentrega, r.veiculo, t.codpag' +
        ', t.codfil'
      
        '  from tb_romaneio r right join cyber.rodord t on (r.nr_romaneio' +
        ' = t.agecar)'
      
        '                     left join cyber.rodcli d on (d.codclifor = ' +
        't.coddes)'
      
        '                     left join cyber.rodcli rd on (rd.codclifor ' +
        '= t.terent)'
      
        '                     left join cyber.rodima m on (m.codman = t.c' +
        'odman and m.fildoc = t.codfil)'
      
        '                     left join cyber.rodset rt on (rt.codsetcl =' +
        ' (case when t.terent > 0 then rd.codsetcl else d.codsetcl end)) '
      'where r.nr_romaneio = :0'
      'union all'
      
        'select distinct r.nr_romaneio, ct.nr_conhecimento cod_ta, d.razs' +
        'oc destino, rt.descri rota, rd.razsoc cliredes,'
      
        'case when ct.cod_redespacho > 0 then rd.endere || '#39'-'#39' || rd.bair' +
        'ro else d.endere || '#39'-'#39' || d.bairro end localentrega, r.veiculo,' +
        ' ct.cod_pagador codpag, ct.fl_empresa codfil'
      
        'from intecom.tb_romaneio r left join intecom.tb_conhecimento ct ' +
        'on (ct.nr_romaneio = r.nr_romaneio)'
      
        '                     left join cyber.rodcli d on (d.codclifor = ' +
        'ct.cod_destinatario)'
      
        '                     left join cyber.rodcli rd on (rd.codclifor ' +
        '= ct.cod_redespacho)'
      
        '                     left join cyber.rodset rt on (rt.codsetcl =' +
        ' (case when ct.cod_redespacho > 0 then'
      '                        rd.codsetcl else d.codsetcl end))'
      'where r.nr_romaneio = :1 and nr_conhecimento is not null'
      ''
      'order by 2'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 104
    Top = 434
    object qMapaNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qMapaCOD_TA: TBCDField
      FieldName = 'COD_TA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object qMapaDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
      Size = 50
    end
    object qMapaCLIREDES: TStringField
      FieldName = 'CLIREDES'
      ReadOnly = True
      Size = 50
    end
    object qMapaROTA: TStringField
      FieldName = 'ROTA'
      ReadOnly = True
      Size = 40
    end
    object qMapaLOCALENTREGA: TStringField
      FieldName = 'LOCALENTREGA'
      ReadOnly = True
      Size = 80
    end
    object qMapaVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
    end
    object qMapaCODPAG: TBCDField
      FieldName = 'CODPAG'
      ReadOnly = True
      Precision = 32
    end
    object qMapaCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
  end
  object QVeiculo: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct placa, descri, kg, cub, sitras,codclifor, datven' +
        ' from ('
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras, v.codclifor,'
      
        'case when max(v.tarakg) > 4535 and a.codcmo = 10 and anofab > '#39'1' +
        '990'#39' then'
      '  nvl(datven, sysdate-1) else sysdate end datven'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      
        '                    left join cyber.rodcli t on v.codclifor = t.' +
        'codclifor'
      
        '                    left join cyber.RODCMO a on t.codcmo = a.cod' +
        'cmo'
      
        'where (propri <> '#39'S'#39' and tipvin <> '#39'A'#39' and tipvei <> 7 and v.sit' +
        'uac=1)'
      
        'group by numvei, f.descri, sitras, v.codclifor, datven, a.codcmo' +
        ', anofab'
      ''
      'union all'
      ''
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras, v.codclifor,'
      
        'case when max(v.tarakg) > 4535 and a.codcmo = 10 and anofab > '#39'1' +
        '990'#39' then'
      '  nvl(datven, sysdate-1) else sysdate end datven'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      
        '                    left join cyber.rodcli t on v.codclifor = t.' +
        'codclifor'
      
        '                    left join cyber.RODCMO a on t.codcmo = a.cod' +
        'cmo'
      'where v.datant >= trunc(sysdate)'
      'and v.vendua >= trunc(sysdate)'
      'and propri = '#39'N'#39
      'and tipvin = '#39'A'#39' and tipvei <> 7'
      
        'group by numvei, f.descri, sitras, v.codclifor, datven, a.codcmo' +
        ', anofab)'
      'where codclifor = :0 and datven >= sysdate'
      'order by 1,2')
    Left = 12
    Top = 536
    object QVeiculoDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object QVeiculoKG: TBCDField
      FieldName = 'KG'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QVeiculoSITRAS: TStringField
      FieldName = 'SITRAS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QVeiculoCODCLIFOR: TFMTBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
  end
  object QVeiculoCasa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct  v.numvei placa, f.descri, max(v.capaci) kg, max' +
        '(v.capam3) cub, sitras,codpro'
      
        'from cyber.rodvei v left join cyber.rodfro f on v.codfro = f.cod' +
        'fro'
      'where v.datant >= trunc(sysdate)'
      'and (propri = '#39'S'#39' )'
      'group by numvei, f.descri, sitras,codpro'
      'order by 1,2')
    Left = 8
    Top = 484
    object QVeiculoCasaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QVeiculoCasaDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 100
    end
    object QVeiculoCasaKG: TBCDField
      FieldName = 'KG'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCasaCUB: TBCDField
      FieldName = 'CUB'
      ReadOnly = True
      Precision = 32
    end
    object QVeiculoCasaSITRAS: TStringField
      FieldName = 'SITRAS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QVeiculoCasaCODPRO: TBCDField
      FieldName = 'CODPRO'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsVeiculo: TDataSource
    DataSet = QVeiculo
    Left = 51
    Top = 536
  end
  object QGerados: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct r.*'
      'from tb_romaneio r '
      'where r.veiculo is not null'
      'order by 1 desc')
    Left = 196
    Top = 440
    object QGeradosNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QGeradosDEMISSAO: TDateTimeField
      FieldName = 'DEMISSAO'
      ReadOnly = True
    end
    object QGeradosDEMBARQUE: TDateTimeField
      FieldName = 'DEMBARQUE'
      ReadOnly = True
    end
    object QGeradosNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QGeradosNR_OS: TBCDField
      FieldName = 'NR_OS'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QGeradosFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QGeradosUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
    end
    object QGeradosORIGEM: TStringField
      FieldName = 'ORIGEM'
      ReadOnly = True
      Size = 2
    end
    object QGeradosDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
      Size = 2
    end
    object QGeradosPESO: TFloatField
      FieldName = 'PESO'
      ReadOnly = True
    end
    object QGeradosSUSUARIO: TStringField
      FieldName = 'SUSUARIO'
      ReadOnly = True
      Size = 50
    end
    object QGeradosDAT_CONFERENCIA: TDateTimeField
      FieldName = 'DAT_CONFERENCIA'
      ReadOnly = True
    end
    object QGeradosCOD_USU_CONFERENCIA: TBCDField
      FieldName = 'COD_USU_CONFERENCIA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QGeradosFL_CTRC: TStringField
      FieldName = 'FL_CTRC'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QGeradosVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
    end
  end
  object dtsGerados: TDataSource
    DataSet = QGerados
    Left = 231
    Top = 440
  end
  object QMotor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct (codmot ||'#39' - '#39'||nommot) nome from ('
      
        'select m.codmot, m.cartdt, m.vencha, m.codcmo, r.datval, m.nommo' +
        't, m.codclifor'
      
        'from cyber.rodmot m left join cyber.rodgri r on r.codmot = m.cod' +
        'mot'
      'where m.cartdt >= trunc(sysdate)'
      'and m.codcmo not in(3,2)'
      'and m.situac = '#39'A'#39' and r.situac = '#39'APTO'#39
      'union all'
      
        'select m.codmot, m.cartdt, m.vencha, m.codcmo, r.datval, m.nommo' +
        't, m.codclifor'
      
        'from cyber.rodmot m left join cyber.rodgri r on r.codmot = m.cod' +
        'mot'
      'where m.cartdt >= trunc(sysdate)'
      'and m.codcmo = 3'
      'and m.vencha >= trunc(sysdate)'
      'and r.datval >= trunc(sysdate)'
      'and m.situac = '#39'A'#39' and r.situac = '#39'APTO'#39
      'union all'
      
        'select m.codmot, m.cartdt, m.vencha, m.codcmo, r.datval, m.nommo' +
        't, m.codclifor'
      
        'from cyber.rodmot m left join cyber.rodgri r on r.codmot = m.cod' +
        'mot'
      'where m.cartdt >= trunc(sysdate)'
      'and m.codcmo = 2'
      'and r.datval >= trunc(sysdate)'
      'and m.situac = '#39'A'#39' and r.situac = '#39'APTO'#39
      ')'
      'where codclifor = :0'
      'order by 1')
    Left = 104
    Top = 492
    object QMotorNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 80
    end
  end
  object QTransp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct CODIGO, NOME from ('
      '  select dataf, CODIGO, NOME from ('
      
        '    select distinct t.cod_custo, max(i.dataf) dataf, cod_fornece' +
        'dor CODIGO, fma.razsoc NOME'
      
        '    from tb_custofrac_item i left join tb_custofrac t on i.cod_c' +
        'usto = t.cod_custo'
      
        '                             left join cyber.rodcli fma on t.cod' +
        '_fornecedor = fma.codclifor'
      
        '                             left join tb_operacao o on t.operac' +
        'ao = o.operacao'
      
        '    where t.fl_status = '#39'S'#39' and fma.razsoc is not null and fma.s' +
        'ituac = '#39'A'#39
      '    and o.fl_oc = '#39'N'#39' and o.fl_receita = '#39'N'#39
      '    group by t.cod_custo, t.operacao, cod_fornecedor, fma.razsoc'
      ''
      '    union'
      ''
      
        '    select distinct ie.cod_tabela, max(ec.validade) dataf, ec.co' +
        'd_transp CODIGO, fma.razsoc NOME'
      
        '    from tb_valor_custo_ecommerce ie left join tb_custo_ecommerc' +
        'e ec on ie.cod_tabela = ec.cod_tabela'
      
        '                                     left join cyber.rodcli fma ' +
        'on ec.cod_transp = fma.codclifor'
      
        '                                     left join tb_operacao o on ' +
        'ec.cod_operacao = o.id_operacao'
      
        '    where ec.status = '#39'A'#39' and fma.razsoc is not null and fma.sit' +
        'uac = '#39'A'#39
      '    and o.fl_oc = '#39'N'#39' and o.fl_ecommerce = '#39'S'#39
      
        '    group by ie.cod_tabela, ec.cod_tabela, ec.cod_transp, fma.ra' +
        'zsoc'
      '  )'
      '  where trunc(dataf) >= trunc(sysdate)'
      ')'
      'order by 2')
    Left = 8
    Top = 432
    object QTranspNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QTranspCODIGO: TBCDField
      FieldName = 'CODIGO'
      Precision = 32
      Size = 0
    end
  end
  object dtsTrans: TDataSource
    DataSet = QTransp
    Left = 43
    Top = 432
  end
  object dsManifestoCtrc: TDataSource
    AutoEdit = False
    DataSet = qrManifestoCtrc2
    Left = 43
    Top = 380
  end
  object dstTarefa: TDataSource
    DataSet = mdTarefa
    Left = 40
    Top = 324
  end
  object SP_LIMPA_ROMANEIO: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_LIMPA_ROMANEIO'
    Parameters = <
      item
        Name = 'VROMANEIO'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'V_MOTIVO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'V_USUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 8
    Top = 584
  end
  object QTransf: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 1
      end>
    SQL.Strings = (
      
        'select m.manifesto, m.serie, m.uf_destino uffim, m.operacao, sum' +
        '(c.nr_peso_kg) peso, sum(c.vl_declarado_nf) vlrmer, nvl(sum(c.vl' +
        '_total),0) totfre,'
      'nvl(valor_custo,0) custo, m.id_man'
      
        'from tb_manifesto m left join tb_manitem d on m.manifesto = d.ma' +
        'nifesto and m.filial = d.filial and m.serie = d.serie'
      
        '                    left join tb_conhecimento c on (c.nr_conheci' +
        'mento = d.coddoc and c.fl_empresa = d.fildoc and c.nr_serie = d.' +
        'serdoc)'
      'where mdfe_encerramento is null and m.status = '#39'S'#39
      
        'and m.transferido is null and m.data_saida is null and m.datainc' +
        ' > '#39'01/07/2020'#39
      'and m.filial = :0'
      
        'group by m.manifesto, uf_destino, m.operacao, m.serie, valor_cus' +
        'to, m.id_man'
      'order by 1')
    Left = 188
    Top = 316
    object QTransfMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QTransfUFFIM: TStringField
      FieldName = 'UFFIM'
      Size = 2
    end
    object QTransfOPERACAO: TStringField
      FieldName = 'OPERACAO'
    end
    object QTransfPESO: TBCDField
      FieldName = 'PESO'
      ReadOnly = True
      Precision = 32
    end
    object QTransfVLRMER: TBCDField
      FieldName = 'VLRMER'
      ReadOnly = True
      Precision = 32
    end
    object QTransfTOTFRE: TBCDField
      FieldName = 'TOTFRE'
      ReadOnly = True
      Precision = 32
    end
    object QTransfCUSTO: TBCDField
      FieldName = 'CUSTO'
      ReadOnly = True
      Precision = 32
    end
    object QTransfSERIE: TStringField
      FieldName = 'SERIE'
      FixedChar = True
      Size = 1
    end
    object QTransfID_MAN: TFMTBCDField
      FieldName = 'ID_MAN'
      Precision = 38
      Size = 0
    end
  end
  object SPTransf: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_GRAVA_TRANSF'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VOPERACAO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTRANSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCARRETA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VMOTOR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VROMA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VDESTINO'
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCUSTO'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'VRECEITA'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'VCIDADED'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'Visca1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'visca2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'vobs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 144
    Top = 580
  end
  object dtsMdTransf: TDataSource
    AutoEdit = False
    DataSet = MDTransf
    Left = 240
    Top = 267
  end
  object MDTransf: TJvMemoryData
    FieldDefs = <
      item
        Name = 'SELECIONADO'
        DataType = ftInteger
      end
      item
        Name = 'NR_OS'
        DataType = ftInteger
      end
      item
        Name = 'NR_ROMANEIO'
        DataType = ftInteger
      end
      item
        Name = 'DEMISSAO'
        DataType = ftDateTime
      end
      item
        Name = 'PESO'
        DataType = ftFloat
      end
      item
        Name = 'SUSUARIO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NR_MANIFESTO'
        DataType = ftInteger
      end>
    Left = 189
    Top = 268
    object MDTransfValor: TCurrencyField
      FieldName = 'Valor'
      DisplayFormat = '###,##0.00'
    end
    object MDTransfSelecionado: TIntegerField
      FieldName = 'Selecionado'
    end
    object MDTransfNr_manifesto: TIntegerField
      FieldName = 'Nr_manifesto'
    end
    object MDTransfPESO: TFloatField
      FieldName = 'PESO'
    end
    object MDTransfDestino: TStringField
      FieldName = 'Destino'
    end
    object MDTransfoperacao: TStringField
      FieldName = 'operacao'
    end
    object MDTransftotfre: TFloatField
      FieldName = 'totfre'
    end
    object MDTransfcusto: TFloatField
      FieldName = 'custo'
    end
    object MDTransfserie: TStringField
      FieldName = 'serie'
      Size = 1
    end
    object MDTransfid_man: TIntegerField
      FieldName = 'id_man'
    end
  end
  object dsRomaneio: TDataSource
    AutoEdit = False
    DataSet = qrRomaneio
    Left = 36
    Top = 273
  end
  object dsTempRomaneio: TDataSource
    AutoEdit = False
    DataSet = mdTempromaneio
    Left = 108
    Top = 271
  end
  object dsTemp: TDataSource
    AutoEdit = False
    DataSet = mdTemp
    Left = 108
    Top = 217
  end
  object ADOQuery1: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct codcon, nr_romaneio, inserida, razsoc, datinc, c' +
        'odfil, doc, codsetcl  from ('
      
        'select distinct t.codcon, t.agecar nr_romaneio, case when nvl(t.' +
        'agecar,0) > 0 then 1 else 0 end as inserida, rd.razsoc, t.datinc' +
        ', t.codfil, '#39'CTE'#39' doc, rt.codsetcl'
      
        ' from cyber.rodcon t left join tb_romaneio r on (r.fl_empresa = ' +
        't.codfil and r.nr_romaneio = t.agecar)'
      
        '                     left join cyber.rodcli c on (c.codclifor = ' +
        't.codpag)'
      
        '                     left join cyber.rodcli rd on (rd.codclifor ' +
        '= nvl(t.terent, t.coddes))'
      
        '                     left join cyber.rodima m on (m.codman = t.c' +
        'odman and m.fildoc = t.codfil)'
      
        '                     left join cyber.rodset rt on (rt.codsetcl =' +
        ' rd.codsetcl) left join vw_itens_manifesto i on (i.CODDOC = t.co' +
        'dcon and i.fildoc = t.codfil and i.serman = m.serman)'
      'where (nvl(t.agecar,0) = 0)'
      
        '  and t.situac <> '#39'C'#39'  and t.ctepro is not null and t.datinc > t' +
        'o_date('#39'01/12/2014'#39','#39'dd/mm/yyyy'#39')'
      '  and (c.situac = '#39'A'#39') and t.codman is null and t.tipcon <> '#39'C'#39
      ''
      'union'
      
        'select distinct t.codcon, t.agecar nr_romaneio, case when nvl(t.' +
        'agecar,0) > 0 then 1 else 0 end as inserida, rd.razsoc, t.datinc' +
        ', t.codfil, '#39'CTE'#39' doc, rt.codsetcl'
      
        ' from cyber.rodcon t left join tb_romaneio r on (r.fl_empresa = ' +
        't.codfil and r.nr_romaneio = t.agecar)'
      
        '                     left join cyber.rodcli c on (c.codclifor = ' +
        't.codpag)'
      
        '                     left join cyber.rodcli rd on (rd.codclifor ' +
        '= nvl(t.terent, t.coddes))'
      
        '                     left join cyber.rodima m on (m.codman = t.c' +
        'odman and m.fildoc = t.codfil)'
      
        '                     left join cyber.rodset rt on (rt.codsetcl =' +
        ' rd.codsetcl) left join vw_itens_manifesto i on (i.CODDOC = t.co' +
        'dcon and i.fildoc = t.codfil and i.serman = m.serman)'
      
        'where (t.tipcon in ('#39'R'#39','#39'D'#39') or t.tipcon = '#39'C'#39' and t.difere = '#39'S' +
        'EGUNDA ENTREGA'#39')'
      
        '  and t.situac <> '#39'C'#39'  and t.ctepro is not null and t.datinc > t' +
        'o_date('#39'01/12/2014'#39','#39'dd/mm/yyyy'#39')'
      '  and (c.situac = '#39'A'#39') and t.codman is null'
      'union'
      
        'select distinct t.codigo, t.agecar nr_romaneio, case when nvl(t.' +
        'agecar,0) > 0 then 1 else 0 end as inserida, rd.razsoc, t.datinc' +
        ', t.codfil, '#39'OST'#39' doc, rt.codsetcl '
      
        ' from cyber.rodord t left join tb_romaneio r on (r.fl_empresa = ' +
        't.codfil and r.nr_romaneio = t.agecar)'
      
        '                     left join cyber.rodcli c on (c.codclifor = ' +
        't.codpag)'
      
        '                     left join cyber.rodcli rd on (rd.codclifor ' +
        '= nvl(t.terent, t.coddes))'
      
        '                     left join cyber.rodima m on (m.codman = t.c' +
        'odman and m.fildoc = t.codfil)'
      
        '                     left join cyber.rodset rt on (rt.codsetcl =' +
        ' rd.codsetcl) left join vw_itens_manifesto i on (i.CODDOC = t.co' +
        'dcon and i.fildoc = t.codfil and i.serman = m.serman)'
      'where (nvl(t.agecar,0) = 0)'
      
        '  and t.situac <> '#39'C'#39' and t.datinc > to_date('#39'01/12/2014'#39','#39'dd/mm' +
        '/yyyy'#39')'
      
        '  and (c.situac = '#39'A'#39') and t.codman is null  and t.situac in ('#39'D' +
        #39','#39'B'#39')'
      ')'
      'where codfil in(1)'
      'and codsetcl = :1'
      'order by codcon')
    Left = 100
    Top = 128
    object BCDField1: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object BCDField2: TBCDField
      FieldName = 'INSERIDA'
      ReadOnly = True
      Precision = 32
    end
    object BCDField3: TBCDField
      FieldName = 'NR_ROMANEIO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object StringField1: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
    end
    object BCDField4: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
    object StringField2: TStringField
      FieldName = 'DOC'
      ReadOnly = True
      FixedChar = True
      Size = 3
    end
  end
  object QCusto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ROMA'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'TIPO'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'PLACA'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'forne'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codmun'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = 'codmunD'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'km'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      
        'select nvl(fnc_custo_sim(:ROMA, :TIPO, :PLACA, :forne, :codmun, ' +
        ':codmund, :km),0) custo'
      'from dual')
    Left = 157
    Top = 132
    object QCustoCUSTO: TBCDField
      FieldName = 'CUSTO'
      ReadOnly = True
      Precision = 32
    end
  end
  object QCidade: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select codmun, descri '
      'from cyber.rodmun '
      'where estado = :0 and inativ = '#39'N'#39
      ''
      ''
      ''
      'order by 2')
    Left = 204
    Top = 128
    object QCidadeCODMUN: TBCDField
      FieldName = 'CODMUN'
      Precision = 32
    end
    object QCidadeDESCRI: TStringField
      FieldName = 'DESCRI'
      Size = 40
    end
  end
  object QGeraMan: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct uf_destino from ('
      'select distinct m.estado uf_destino'
      
        '  from cyber.rodord t left outer join cyber.rodcli d on (d.codcl' +
        'ifor = nvl(t.terent, t.coddes))'
      
        '                      left join cyber.rodmun m on (m.codmun = d.' +
        'codmun)'
      ' where t.agecar = 535'
      'union all'
      'select distinct m.estado uf_destino'
      
        '  from tb_conhecimento t left outer join cyber.rodcli d on (d.co' +
        'dclifor = case when t.cod_redespacho = 0 then'
      
        '                      t.cod_destinatario else t.cod_redespacho e' +
        'nd)'
      
        '                         left join cyber.rodmun m on (m.codmun =' +
        ' d.codmun)'
      ' where t.nr_romaneio = 66261 '
      '  )'
      ' order by 1')
    Left = 256
    Top = 128
    object QGeraManUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      ReadOnly = True
      Size = 2
    end
  end
  object SPManifesto: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_GRAVA_MANIFESTO'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VOPERACAO'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 38
        Size = 20
        Value = '0'
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTRANSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VCARRETA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VMOTOR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VROMA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = ''
      end
      item
        Name = 'VUF'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VCODMUNO'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'VOBS'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'V_custo'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'Vcodmund'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'V_parafilial'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'V_RECEITA'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'VCIOT'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VISCA1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VISCA2'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'vcodfilial'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'vrota'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'vtrace'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'vcartao'
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'vdistancia'
        DataType = ftFloat
        Value = 0.000000000000000000
      end
      item
        Name = 'vadiantamento'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'vpgr'
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'vpedagio'
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 192
    Top = 580
  end
  object QManifesto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = QManifestoBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select m.valor_custo, m.manifesto, m.filial, m.serie, m.mdfe_rec' +
        'ibo, m.operacao, m.data_liberado, m.mdfe_chave, m.mdfe_status,'
      
        'm.ciot, m.cartao_repom, (m.retorno_repom || m.status_viagem ) st' +
        'atus_viagem, m.id_operacao, m.mdfe_mensagem,'
      'm.usuario, m.datainc, m.romaneio, m.placa, m.valor_receita,'
      
        'f.razsoc nm_transp, c.estado, m.pgr, m.id_monitoramento , m.fl_c' +
        'ontingencia,'
      
        'case when m.valor_receita = 0 then 100 when m.valor_custo = 0 th' +
        'en 100 else round(((m.valor_custo/m.valor_receita)*100),2) end m' +
        'argem'
      
        'from tb_manifesto m left join cyber.rodcli f on m.transp = f.cod' +
        'clifor'
      
        '                    left join cyber.rodmun c on c.codmun = m.cod' +
        'muno'
      'where filial = :0'
      'and nvl(status,'#39'N'#39') =  '#39'N'#39
      'ORDER BY datainc, manifesto')
    Left = 192
    Top = 220
    object QManifestoVALOR_CUSTO: TBCDField
      FieldName = 'VALOR_CUSTO'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 15
      Size = 2
    end
    object QManifestoMANIFESTO: TFMTBCDField
      FieldName = 'MANIFESTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QManifestoFILIAL: TFMTBCDField
      FieldName = 'FILIAL'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QManifestoSERIE: TStringField
      FieldName = 'SERIE'
      ReadOnly = True
      Size = 1
    end
    object QManifestoNM_TRANSP: TStringField
      FieldName = 'NM_TRANSP'
      ReadOnly = True
      Size = 80
    end
    object QManifestoESTADO: TStringField
      FieldName = 'ESTADO'
      ReadOnly = True
      Size = 2
    end
    object QManifestoMARGEM: TFMTBCDField
      FieldName = 'MARGEM'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QManifestoMDFE_RECIBO: TStringField
      FieldName = 'MDFE_RECIBO'
      ReadOnly = True
    end
    object QManifestoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
    end
    object QManifestoDATA_LIBERADO: TDateTimeField
      FieldName = 'DATA_LIBERADO'
      ReadOnly = True
    end
    object QManifestoMDFE_CHAVE: TStringField
      FieldName = 'MDFE_CHAVE'
      ReadOnly = True
      Size = 48
    end
    object QManifestoMDFE_STATUS: TFMTBCDField
      FieldName = 'MDFE_STATUS'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QManifestoCIOT: TStringField
      FieldName = 'CIOT'
      ReadOnly = True
    end
    object QManifestoCARTAO_REPOM: TStringField
      FieldName = 'CARTAO_REPOM'
      ReadOnly = True
    end
    object QManifestoSTATUS_VIAGEM: TMemoField
      FieldName = 'STATUS_VIAGEM'
      ReadOnly = True
      BlobType = ftMemo
    end
    object QManifestoID_OPERACAO: TStringField
      FieldName = 'ID_OPERACAO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QManifestoMDFE_MENSAGEM: TStringField
      FieldName = 'MDFE_MENSAGEM'
      ReadOnly = True
      Size = 500
    end
    object QManifestoUSUARIO: TStringField
      FieldName = 'USUARIO'
      ReadOnly = True
    end
    object QManifestoDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QManifestoROMANEIO: TStringField
      FieldName = 'ROMANEIO'
      ReadOnly = True
      Size = 300
    end
    object QManifestoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object QManifestoVALOR_RECEITA: TBCDField
      FieldName = 'VALOR_RECEITA'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 15
      Size = 2
    end
    object QManifestoPGR: TStringField
      FieldName = 'PGR'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QManifestoID_MONITORAMENTO: TFMTBCDField
      FieldName = 'ID_MONITORAMENTO'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QManifestofl_contingencia: TStringField
      FieldName = 'fl_contingencia'
      Size = 1
    end
  end
  object dtsQManifesto: TDataSource
    AutoEdit = False
    DataSet = QManifesto
    Left = 239
    Top = 220
  end
  object Mdvolumes: TJvMemoryData
    FieldDefs = <
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end
      item
        Name = 'destino'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cod_ta'
        DataType = ftInteger
      end
      item
        Name = 'cliredes'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'localentrega'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rota'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'nf'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'pedido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'volume'
        DataType = ftString
        Size = 500
      end>
    Left = 280
    Top = 372
    object Mdvolumesnr_romaneio: TIntegerField
      DisplayWidth = 15
      FieldName = 'nr_romaneio'
    end
    object Mdvolumescod_ta: TIntegerField
      FieldName = 'cod_ta'
    end
    object Mdvolumesnf: TStringField
      DisplayWidth = 300
      FieldName = 'nf'
      Size = 300
    end
    object Mdvolumesdestino: TStringField
      DisplayWidth = 50
      FieldName = 'destino'
      Size = 50
    end
    object Mdvolumescliredes: TStringField
      FieldName = 'cliredes'
    end
    object Mdvolumeslocalentrega: TStringField
      DisplayWidth = 50
      FieldName = 'localentrega'
      Size = 50
    end
    object Mdvolumesrota: TStringField
      FieldName = 'rota'
    end
    object Mdvolumespedido: TStringField
      FieldName = 'pedido'
    end
    object Mdvolumesvolume: TStringField
      DisplayWidth = 999999
      FieldName = 'volume'
      Size = 999999
    end
  end
  object qMapaCarga: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT DISTINCT v.NR_MANIFESTO, v.DATAINC, v.PLACA, v.VEICULO, v' +
        '.NM_FORNECEDOR, mo.nommot, mo.cartha cnh , m.obs'
      
        'FROM vw_manifesto_detalhe v left join tb_manifesto m on (v.nr_ma' +
        'nifesto = m.manifesto and v.serie = m.serie and v.fl_empresa = m' +
        '.filial)'
      
        '                            left join cyber.rodmot mo on mo.codm' +
        'ot = m.motorista'
      'where v.nr_manifesto = :0'
      'and v.serie = :1'
      'and v.fl_empresa = :2'
      ''
      ''
      '')
    Left = 255
    Top = 181
    object qMapaCargaNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      ReadOnly = True
      Precision = 32
    end
    object qMapaCargaDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      ReadOnly = True
    end
    object qMapaCargaPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
    object qMapaCargaVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 100
    end
    object qMapaCargaNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
      ReadOnly = True
      Size = 80
    end
    object qMapaCargaNOMMOT: TStringField
      FieldName = 'NOMMOT'
      ReadOnly = True
      Size = 40
    end
    object qMapaCargaCNH: TStringField
      FieldName = 'CNH'
      ReadOnly = True
      Size = 12
    end
    object qMapaCargaOBS: TStringField
      FieldName = 'OBS'
      ReadOnly = True
      Size = 300
    end
  end
  object QMapaItens: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct t.datainc, t.nm_origem nm_emp_ori_ta, t.destino ' +
        'nm_emp_dest_ta, sum(t.volume) nr_quantidade,'
      
        '       sum(t.peso) peso, t.nr_manifesto, t.fl_empresa,  t.nr_ctr' +
        'c , sum(T.vlrmer) nr_valor, '
      '       (t.cidadedestino || '#39' / '#39' || t.uf_destino) cidade'
      '  from vw_manifesto_detalhe t'
      ' where t.nr_manifesto = :0'
      '   and t.serie = :1'
      '   and t.fl_empresa = :2'
      
        'group by t.datainc, t.nm_origem, t.destino,  t.nr_manifesto, t.f' +
        'l_empresa,  t.nr_ctrc , (t.cidadedestino || '#39' / '#39' || t.uf_destin' +
        'o)'
      ' order by 3,2, 7')
    Left = 204
    Top = 182
    object QMapaItensDATAINC: TDateTimeField
      FieldName = 'DATAINC'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QMapaItensNM_EMP_ORI_TA: TStringField
      FieldName = 'NM_EMP_ORI_TA'
      Size = 80
    end
    object QMapaItensNM_EMP_DEST_TA: TStringField
      FieldName = 'NM_EMP_DEST_TA'
      Size = 80
    end
    object QMapaItensNR_QUANTIDADE: TBCDField
      FieldName = 'NR_QUANTIDADE'
      Precision = 32
    end
    object QMapaItensPESO: TBCDField
      FieldName = 'PESO'
      DisplayFormat = '##,##0.000'
      Precision = 12
    end
    object QMapaItensNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 32
    end
    object QMapaItensFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object QMapaItensNR_CTRC: TBCDField
      FieldName = 'NR_CTRC'
      Precision = 32
    end
    object QMapaItensNR_VALOR: TBCDField
      FieldName = 'NR_VALOR'
      DisplayFormat = '###,##0.00'
      Precision = 14
      Size = 2
    end
    object QMapaItensCIDADE: TStringField
      FieldName = 'CIDADE'
      ReadOnly = True
      Size = 45
    end
  end
  object Sp_CancelaMan: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_CANCELA_MANIFESTO'
    Parameters = <
      item
        Name = 'VMAN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end>
    Left = 392
    Top = 580
  end
  object Sp_Liberado: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_AUTORIZADO_CUSTO'
    Parameters = <
      item
        Name = 'VMAN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VSERIE'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end>
    Left = 292
    Top = 576
  end
  object QItensMapa: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select m.codcon, fl_empresa, nm_emp_ori_ta origem, nm_emp_dest_t' +
        'a_01 destino, uf_destino, nr_peso_ta_01 peso, vlrmer, doc '
      'from vw_itens_mapa_sim m '
      'where m.nr_romaneio = :0 and uf_destino = :1')
    Left = 180
    Top = 538
    object QItensMapaCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QItensMapaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object QItensMapaORIGEM: TStringField
      FieldName = 'ORIGEM'
      Size = 80
    end
    object QItensMapaDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object QItensMapaUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
    object QItensMapaPESO: TBCDField
      FieldName = 'PESO'
      DisplayFormat = '##,##0.000'
      Precision = 32
    end
    object QItensMapaVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 32
    end
    object QItensMapaDOC: TStringField
      FieldName = 'DOC'
      FixedChar = True
      Size = 3
    end
  end
  object dtsItens: TDataSource
    AutoEdit = False
    DataSet = QItensMapa
    Left = 228
    Top = 539
  end
  object qNFs: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select notfis nr_nota from cyber.rodnfc n '
      'where n.codfil = :0 and codcon = :1'
      'union all '
      'select notfis nr_nota from cyber.rodios i '
      'where i.filord = :2 and codord = :3')
    Left = 276
    Top = 440
    object qNFsNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
    end
  end
  object QItensRoma: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select m.codcon, fl_empresa, nm_emp_ori_ta origem, nm_emp_dest_t' +
        'a_01 destino, uf_destino, nr_peso_ta_01 peso, vlrmer, doc '
      'from vw_itens_romaneio m '
      'where m.nr_romaneio = :0 and uf_destino = :1')
    Left = 184
    Top = 490
    object QItensRomaCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object QItensRomaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object QItensRomaORIGEM: TStringField
      FieldName = 'ORIGEM'
      Size = 80
    end
    object QItensRomaDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object QItensRomaUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
    object QItensRomaPESO: TBCDField
      FieldName = 'PESO'
      DisplayFormat = '##,##0.000'
      Precision = 32
    end
    object QItensRomaVLRMER: TBCDField
      FieldName = 'VLRMER'
      DisplayFormat = '###,##0.00'
      Precision = 32
    end
    object QItensRomaDOC: TStringField
      FieldName = 'DOC'
      FixedChar = True
      Size = 3
    end
  end
  object dtsQItensRoma: TDataSource
    AutoEdit = False
    DataSet = QItensRoma
    Left = 232
    Top = 487
  end
  object QVols: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codcon'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codfil'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select codcon, codfil, ordcom, codbar from ('
      
        'select c.codcon, c.codfil, nvl(n.ordcom,'#39' '#39') ordcom, nvl(codbar,' +
        'cod_barras_int) codbar'
      
        'from cyber.itc_man_emb e left join cyber.rodocs o on (e.cod_ocs ' +
        '= o.codocs and e.cod_fil = o.filocs) '
      
        'left join cyber.rodcon c on (c.codcon = o.codcon and c.codfil = ' +
        'o.filocs) '
      
        'left join cyber.rodnfc n on (n.codcon = c.codcon and n.codfil = ' +
        'c.codfil and n.notfis = e.nr_nf) '
      'union all '
      
        'select d.codigo codcon, d.codfil, nvl(i.ordcom,'#39' '#39') ordcom, nvl(' +
        'codbar,cod_barras_int) codbar'
      
        'from cyber.itc_man_emb e left join cyber.rodord d on (e.cod_ocs ' +
        '= d.codigo and e.cod_fil = d.codfil)  '
      
        'left join cyber.rodios i on (i.codord = d.codigo and i.filord = ' +
        'd.codfil and i.notfis = e.nr_nf)'
      'union all'
      
        'select c.nr_conhecimento, c.fl_empresa, n.ordcom, nvl(codbar,cod' +
        '_barras_int) codbar'
      
        'from cyber.itc_man_emb e left join cyber.tb_coleta o on (e.cod_o' +
        'cs = o.nro_ocs and e.cod_fil = o.filial) '
      
        'left join tb_conhecimento c on (c.nr_conhecimento = o.doc and c.' +
        'fl_empresa = o.filial) '
      
        'left join tb_cte_nf n on (n.codcon = c.nr_conhecimento and n.fl_' +
        'empresa = c.fl_empresa and n.notfis = e.nr_nf))'
      'where codcon = :codcon and codfil = :codfil order by 3')
    Left = 232
    Top = 370
    object QVolsCODCON: TBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 32
    end
    object QVolsCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
    object QVolsORDCOM: TStringField
      FieldName = 'ORDCOM'
      ReadOnly = True
      Size = 50
    end
    object QVolsCODBAR: TStringField
      FieldName = 'CODBAR'
      ReadOnly = True
      Size = 30
    end
  end
  object QVolume: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codcon'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codfil'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct ordcom, codfil, codcon from ('
      'select distinct n.ordcom, c.codcon, c.codfil '
      
        'from cyber.itc_man_emb e left join cyber.rodocs o on (e.cod_ocs ' +
        '= o.codocs and e.cod_fil = o.filocs) '
      
        'left join cyber.rodcon c on (c.codcon = o.codcon and c.codfil = ' +
        'o.filocs) '
      
        'left join cyber.rodnfc n on (n.codcon = c.codcon and n.codfil = ' +
        'c.codfil and n.notfis = e.nr_nf) '
      'union all '
      'select distinct i.ordcom, d.codigo codcon, d.codfil  '
      
        'from cyber.itc_man_emb e left join cyber.rodord d on (e.cod_ocs ' +
        '= d.codigo and e.cod_fil = d.codfil)  '
      
        'left join cyber.rodios i on (i.codord = d.codigo and i.filord = ' +
        'd.codfil and i.notfis = e.nr_nf)'
      'union all'
      
        'select distinct io.ordcom, c.nr_conhecimento codcon, c.fl_empres' +
        'a cofil'
      
        'from cyber.itc_man_emb e left join cyber.tb_coleta o on (e.cod_o' +
        'cs = o.nro_ocs and e.cod_fil = o.filial) '
      
        'left join tb_conhecimento c on (c.nr_conhecimento = o.doc and c.' +
        'fl_empresa = o.filial) '
      
        'left join tb_cte_nf n on (n.codcon = c.nr_conhecimento and n.fl_' +
        'empresa = c.fl_empresa and n.notfis = e.nr_nf)'
      
        'left join cyber.tb_coleta co on co.doc = c.nr_conhecimento and c' +
        'o.filial = c.fl_empresa'
      
        'left join cyber.rodioc io on io.codocs = co.nro_ocs and io.filoc' +
        's = co.filial) '
      'where codcon = :codcon and codfil = :codfil  order by 1')
    Left = 184
    Top = 370
    object QVolumeORDCOM: TStringField
      FieldName = 'ORDCOM'
      ReadOnly = True
      Size = 50
    end
  end
  object sp_rom_itc: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_ITC_ROMANEIO'
    Parameters = <
      item
        Name = 'VNEW'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VEMP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end>
    Left = 244
    Top = 576
  end
  object SP_ITC: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_INS_TB_ITC'
    Parameters = <
      item
        Name = 'VMAN'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'vSERIE'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VFILIAL'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end>
    Left = 340
    Top = 576
  end
  object QKM: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct lpad(fnc_remove_caracter(REMOVE_ESPACO(d.codcep)' +
        '),8,0) codcep, d.codclifor, (d.latitu ||'#39'+'#39'|| d.longit) geo, m.d' +
        'escri'
      'from tb_conhecimento c left join cyber.rodcli d on d.codclifor ='
      
        '(case when c.cod_redespacho > 0 then c.cod_redespacho else c.cod' +
        '_destinatario end)'
      'left join cyber.rodmun m on m.codmun = d.codmun'
      'where nr_romaneio in (0)'
      'order by 1')
    Left = 1204
    Top = 444
    object QKMCODCLIFOR: TBCDField
      FieldName = 'CODCLIFOR'
      ReadOnly = True
      Precision = 32
    end
    object QKMGEO: TStringField
      FieldName = 'GEO'
      ReadOnly = True
      Size = 81
    end
    object QKMDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
    object QKMCODCEP: TStringField
      FieldName = 'CODCEP'
      ReadOnly = True
      Size = 8
    end
  end
  object XMLDoc: TXMLDocument
    Left = 1204
    Top = 500
    DOMVendorDesc = 'MSXML'
  end
  object dtsMapa: TDataSource
    DataSet = Mdvolumes
    Left = 279
    Top = 335
  end
  object dtsMapaCarga: TDataSource
    DataSet = qMapaCarga
    Left = 307
    Top = 183
  end
  object dtsMapaItens: TDataSource
    DataSet = QMapaItens
    Left = 303
    Top = 235
  end
  object mdManifesto: TJvMemoryData
    FieldDefs = <
      item
        Name = 'nr_romaneio'
        DataType = ftInteger
      end
      item
        Name = 'destino'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cod_ta'
        DataType = ftInteger
      end
      item
        Name = 'cliredes'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'localentrega'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rota'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'nf'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'pedido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'volume'
        DataType = ftString
        Size = 500
      end>
    Left = 272
    Top = 52
    object mdManifestoManifesto: TIntegerField
      FieldName = 'Manifesto'
    end
    object mdManifestodatainc: TDateField
      FieldName = 'datainc'
    end
    object mdManifestoplaca: TStringField
      FieldName = 'placa'
      Size = 8
    end
    object mdManifestoveiculo: TStringField
      FieldName = 'veiculo'
    end
    object mdManifestonm_fornecedor: TStringField
      FieldName = 'nm_fornecedor'
      Size = 50
    end
    object mdManifestonommot: TStringField
      FieldName = 'nommot'
      Size = 40
    end
    object mdManifestoCNH: TStringField
      FieldName = 'CNH'
    end
    object mdManifestocte: TIntegerField
      FieldName = 'cte'
    end
    object mdManifestoremetente: TStringField
      FieldName = 'remetente'
      Size = 30
    end
    object mdManifestodestino: TStringField
      FieldName = 'destino'
      Size = 30
    end
    object mdManifestocidade: TStringField
      FieldName = 'cidade'
      Size = 30
    end
    object mdManifestovolume: TIntegerField
      FieldName = 'volume'
    end
    object mdManifestopeso: TFloatField
      FieldName = 'peso'
    end
    object mdManifestovalor: TFloatField
      FieldName = 'valor'
    end
    object mdManifestoestado: TStringField
      FieldName = 'estado'
      Size = 2
    end
    object mdManifestooperacao: TStringField
      FieldName = 'operacao'
      Size = 30
    end
    object mdManifestoobs: TMemoField
      FieldName = 'obs'
      BlobType = ftMemo
    end
    object mdManifestonota: TMemoField
      FieldName = 'nota'
      BlobType = ftMemo
    end
  end
  object dtsmdManifesto: TDataSource
    DataSet = mdManifesto
    Left = 339
    Top = 51
  end
  object SPItens: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_GRAVA_MANIFESTO_ITENS'
    Parameters = <
      item
        Name = 'vmanifesto'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'vfilial'
        Attributes = [paNullable]
        DataType = ftInteger
        Value = 0
      end>
    Left = 308
    Top = 512
  end
  object QOperacao: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select distinct operacao from ('
      '  select dataf, operacao from ('
      '    select distinct t.operacao, max(i.dataf) dataf'
      
        '    from tb_custofrac_item i left join tb_custofrac t on i.cod_c' +
        'usto = t.cod_custo'
      
        '                             left join tb_operacao o on t.operac' +
        'ao = o.operacao'
      
        '    where t.fl_status = '#39'S'#39' and o.fl_oc = '#39'N'#39' and o.fl_receita =' +
        ' '#39'N'#39
      '    and cod_fornecedor = :0'
      '    group by t.operacao, t.operacao'
      ''
      '    union'
      ''
      '    select distinct o.operacao, max(ec.validade) dataf'
      
        '    from tb_custo_ecommerce ec left join tb_operacao o on ec.cod' +
        '_operacao = o.id_operacao'
      
        '    where ec.status = '#39'A'#39' and o.fl_oc = '#39'N'#39' and o.fl_ecommerce =' +
        ' '#39'S'#39
      '    and ec.cod_transp = :1'
      '    group by o.operacao'
      '  )'
      '  where trunc(dataf) >= trunc(sysdate)'
      ')'
      'order by 1')
    Left = 108
    Top = 540
    object QOperacaoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
  end
  object QCustoT: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'TIPO'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'PLACA'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'forne'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codmun'
        DataType = ftInteger
        Size = 16
        Value = 0
      end
      item
        Name = 'codmunD'
        DataType = ftInteger
        Value = 0
      end
      item
        Name = 'km'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'peso'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'valor'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'receita'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      
        'select nvl(fnc_custo_transf(:TIPO, :PLACA, :forne, :codmun, :cod' +
        'mund, :km, :peso, :valor, :receita),0) custo'
      'from dual')
    Left = 157
    Top = 180
    object QCustoTCUSTO: TFMTBCDField
      FieldName = 'CUSTO'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
  end
  object QIsca: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select nm_isca from tb_iscas where nm_isca is not null order by ' +
        '1')
    Left = 316
    Top = 124
    object QIscaNM_ISCA: TStringField
      FieldName = 'NM_ISCA'
      Size = 10
    end
  end
  object QPGR_M: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      '  select distinct sum(vlrmer), pgr.id_regra, cliente, razsoc'
      '  from ('
      '  select DISTINCT r.nr_romaneio,'
      
        '  (select sum(vlrmer) from cyber.rodios i where i.codord = t.cod' +
        'igo and i.filord = t.codfil) vlrmer, t.codpag cliente,'
      
        '  case when d.codcmo = 12 then '#39'MONTADORA'#39' else '#39'DIVERSOS'#39' end d' +
        'est, cl.razsoc'
      
        '  from cyber.rodord t left join tb_romaneio r on (r.nr_romaneio ' +
        '= t.agecar)'
      
        '                      left outer join cyber.rodcli d on (d.codcl' +
        'ifor = nvl(t.terent, t.coddes))'
      
        '                      left join cyber.rodcli cl on cl.codclifor ' +
        '= t.codpag'
      '  where r.nr_romaneio in (258914,258935)'
      
        '  group by r.nr_romaneio, t.codpag, t.codigo, t.codfil, d.codcmo' +
        ', cl.razsoc'
      '  union all'
      '  select DISTINCT r.nr_romaneio,'
      
        '  (select sum(i.vlrmer) from tb_cte_nf i where i.codcon = ct.nr_' +
        'conhecimento and i.fl_empresa = ct.fl_empresa) vlrmer, ct.cod_pa' +
        'gador cliente,'
      
        '  case when d.codcmo = 12 then '#39'MONTADORA'#39' else '#39'DIVERSOS'#39' end d' +
        'est, cl.razsoc'
      
        '  from tb_conhecimento ct left join tb_romaneio r on (r.nr_roman' +
        'eio = ct.nr_romaneio)'
      
        '                          left outer join cyber.rodcli d on d.co' +
        'dclifor = ('
      
        '                                                         case wh' +
        'en nvl(ct.cod_redespacho,0) = 0 then ct.cod_destinatario'
      
        '                                                         else ct' +
        '.cod_redespacho end)'
      
        '                          left join cyber.rodcli cl on cl.codcli' +
        'for = ct.cod_pagador'
      '  where r.nr_romaneio in (258914,258935)'
      
        '  ) roma left join tb_regra_pgr pgr on roma.cliente = pgr.cod_cl' +
        'iente and roma.dest = pgr.destino'
      '  group by cliente, pgr.id_regra, razsoc'
      '  order by cliente')
    Left = 436
    Top = 112
    object QPGR_MCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_MSUMVLRMER: TFMTBCDField
      FieldName = 'SUM(VLRMER)'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_MID_REGRA: TFMTBCDField
      FieldName = 'ID_REGRA'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QPGR_MRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
  end
  object SP_PGR: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_PGR_M'
    Parameters = <
      item
        Name = 'R_RETORNO'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 300
        Value = #201' necess'#225'ria a An'#225'lise de Risco !'
      end
      item
        Name = 'R_ERRO'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 1
        Value = Null
      end
      item
        Name = 'VREGRA'
        Attributes = [paNullable]
        DataType = ftFloat
        Value = 1.000000000000000000
      end
      item
        Name = 'VISCA1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = ' '
      end
      item
        Name = 'VISCA2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = ' '
      end
      item
        Name = 'VTRANSP'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 10728.000000000000000000
      end
      item
        Name = 'VVALOR'
        Attributes = [paNullable]
        DataType = ftFloat
        NumericScale = 2
        Precision = 15
        Value = 21000.000000000000000000
      end>
    Left = 500
    Top = 112
  end
  object mdTarefa: TJvMemoryData
    FieldDefs = <
      item
        Name = 'NR_ROMANEIO'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'CODCON'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'NM_EMP_ORI_TA'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'NR_MANIFESTO'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'NR_CNPJ_CLI'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NM_CLI_OS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'VL_CTRC'
        DataType = ftBCD
        Precision = 14
        Size = 2
      end
      item
        Name = 'NM_EMP_DEST_TA_01'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'NR_PESO_TA_01'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'FL_EMPRESA'
        DataType = ftBCD
        Precision = 32
        Size = 4
      end
      item
        Name = 'UF_DESTINO'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'SUSUARIO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DOC'
        Attributes = [faFixed]
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CONCA'
        DataType = ftString
        Size = 83
      end>
    AfterScroll = mdTempromaneioAfterScroll
    OnFilterRecord = mdTempromaneioFilterRecord
    Left = 97
    Top = 324
    object mdTarefaNR_ROMANEIO: TBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 32
      Size = 0
    end
    object mdTarefaCODCON: TBCDField
      FieldName = 'CODCON'
      Precision = 32
    end
    object mdTarefaNM_EMP_ORI_TA: TStringField
      FieldName = 'NM_EMP_ORI_TA'
      Size = 80
    end
    object mdTarefaNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 32
    end
    object mdTarefaNR_CNPJ_CLI: TStringField
      FieldName = 'NR_CNPJ_CLI'
    end
    object mdTarefaNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      Size = 80
    end
    object mdTarefaVL_CTRC: TBCDField
      FieldName = 'VL_CTRC'
      Precision = 14
      Size = 2
    end
    object mdTarefaNM_EMP_DEST_TA_01: TStringField
      FieldName = 'NM_EMP_DEST_TA_01'
      Size = 80
    end
    object mdTarefaNR_PESO_TA_01: TBCDField
      FieldName = 'NR_PESO_TA_01'
      DisplayFormat = '###,##0.000'
      Precision = 32
    end
    object mdTarefaFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 32
    end
    object mdTarefaUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
    object mdTarefaSUSUARIO: TStringField
      FieldName = 'SUSUARIO'
      Size = 50
    end
    object mdTarefacnpj_dest: TStringField
      FieldName = 'cnpj_dest'
    end
    object mdTarefaDOC: TStringField
      FieldName = 'DOC'
      FixedChar = True
      Size = 3
    end
    object mdTarefaCONCA: TStringField
      FieldName = 'CONCA'
      Size = 83
    end
    object mdTarefapalete: TFMTBCDField
      FieldName = 'palete'
    end
    object mdTarefatem: TIntegerField
      FieldName = 'tem'
    end
  end
  object QRoad: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct v.*, c.nr_oc, o.operacao, op.fl_smdfe'
      
        'from vw_gerar_manifesto_roadnet v left join cyber.tb_coleta c on' +
        ' v.codcon = c.doc and v.codfil = c.filial'
      
        '                          left join tb_ordem o on o.id_ordem = c' +
        '.nr_oc'
      
        '                          left join tb_operacao op on op.operaca' +
        'o = o.operacao'
      
        '                          left join tb_roadnet rota on rota.id_r' +
        'oadnet = v.id_road'
      'where v.codfil in (1)'
      'and rota.status is null'
      ''
      'and nvl(op.fl_smdfe,'#39'N'#39') = '#39'N'#39
      'order by v.codcon')
    Left = 368
    Top = 228
    object QRoadCODCON: TFMTBCDField
      FieldName = 'CODCON'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QRoadRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object QRoadDATINC: TDateTimeField
      FieldName = 'DATINC'
      ReadOnly = True
    end
    object QRoadCODFIL: TFMTBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QRoadDOC: TStringField
      FieldName = 'DOC'
      ReadOnly = True
      FixedChar = True
      Size = 3
    end
    object QRoadNR_OC: TFMTBCDField
      FieldName = 'NR_OC'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QRoadOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
    object QRoadFL_SMDFE: TStringField
      FieldName = 'FL_SMDFE'
      ReadOnly = True
      Size = 1
    end
    object QRoadNOMEAB: TStringField
      FieldName = 'NOMEAB'
      ReadOnly = True
      Size = 50
    end
    object QRoadID_ROAD: TFMTBCDField
      FieldName = 'ID_ROAD'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
  end
  object SP_Road: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_ROADNET'
    Parameters = <
      item
        Name = 'VID_ROADNET'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VTIPO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VUSER'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'vcnpj'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'VVEIC'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VROTA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end>
    Left = 420
    Top = 228
  end
  object QidRoad: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select v.*, p.qt_palete'
      'from vw_rodnet_romaneio v'
      
        'left join tb_palet_romaneio p on p.cnpj_destino = v.cnpj_dest an' +
        'd p.cnpj_cliente = v.nr_cnpj_cli and v.nr_romaneio = p.numero an' +
        'd p.tipo = '#39'M'#39
      'where id_road in ('#39'0'#39')'
      'and nr_manifesto is null'
      'order by v.nr_romaneio, v.uf_destino, v.codcon')
    Left = 488
    Top = 228
    object QidRoadNR_ROMANEIO: TFMTBCDField
      FieldName = 'NR_ROMANEIO'
      Precision = 38
      Size = 0
    end
    object QidRoadCODCON: TFMTBCDField
      FieldName = 'CODCON'
      Precision = 38
      Size = 4
    end
    object QidRoadNM_EMP_ORI_TA: TStringField
      FieldName = 'NM_EMP_ORI_TA'
      Size = 80
    end
    object QidRoadNR_MANIFESTO: TFMTBCDField
      FieldName = 'NR_MANIFESTO'
      Precision = 38
      Size = 4
    end
    object QidRoadNR_CNPJ_CLI: TStringField
      FieldName = 'NR_CNPJ_CLI'
    end
    object QidRoadNM_CLI_OS: TStringField
      FieldName = 'NM_CLI_OS'
      Size = 80
    end
    object QidRoadVL_CTRC: TFMTBCDField
      FieldName = 'VL_CTRC'
      Precision = 38
      Size = 4
    end
    object QidRoadNM_EMP_DEST_TA_01: TStringField
      FieldName = 'NM_EMP_DEST_TA_01'
      Size = 80
    end
    object QidRoadNR_PESO_TA_01: TFMTBCDField
      FieldName = 'NR_PESO_TA_01'
      Precision = 38
      Size = 4
    end
    object QidRoadFL_EMPRESA: TFMTBCDField
      FieldName = 'FL_EMPRESA'
      Precision = 38
      Size = 4
    end
    object QidRoadUF_DESTINO: TStringField
      FieldName = 'UF_DESTINO'
      Size = 2
    end
    object QidRoadSUSUARIO: TStringField
      FieldName = 'SUSUARIO'
      Size = 50
    end
    object QidRoadDOC: TStringField
      FieldName = 'DOC'
      FixedChar = True
      Size = 3
    end
    object QidRoadID_ROAD: TFMTBCDField
      FieldName = 'ID_ROAD'
      Precision = 38
      Size = 4
    end
    object QidRoadVLRMER: TFMTBCDField
      FieldName = 'VLRMER'
      Precision = 38
      Size = 4
    end
    object QidRoadCNPJ_DEST: TStringField
      FieldName = 'CNPJ_DEST'
    end
    object QidRoadQT_PALETE: TFMTBCDField
      FieldName = 'QT_PALETE'
      Precision = 38
      Size = 0
    end
    object QidRoadCONCA: TStringField
      FieldName = 'CONCA'
      Size = 83
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 692
    Top = 312
    DOMVendorDesc = 'MSXML'
  end
  object MDRota: TJvMemoryData
    Active = True
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'rota'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'id_rota'
        DataType = ftString
        Size = 5
      end
      item
        Name = 'id_trace'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'distancia'
        DataType = ftString
        Size = 10
      end>
    Left = 1053
    Top = 348
    object MDRotaid: TStringField
      FieldName = 'id'
      Size = 5
    end
    object MDRotarota: TStringField
      FieldName = 'rota'
      Size = 40
    end
    object MDRotaid_rota: TStringField
      FieldName = 'id_rota'
      Size = 5
    end
    object MDRotaid_trace: TStringField
      FieldName = 'id_trace'
      Size = 10
    end
    object MDRotadistancia: TStringField
      FieldName = 'distancia'
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = MDRota
    Left = 1110
    Top = 349
  end
  object QPGR_transf: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        '  select distinct sum(vlrmer), pgr.id_regra, vw.codpag cliente, ' +
        'vw.nm_cli razsoc'
      
        '  from vw_manifesto_ctrc_sim vw left outer join cyber.rodcli d o' +
        'n vw.coddestino = d.codclifor'
      
        '                                left join tb_regra_pgr pgr on vw' +
        '.codpag = pgr.cod_cliente and pgr.destino = '
      
        '                                (case when d.codcmo = 12 then '#39'M' +
        'ONTADORA'#39' else '#39'DIVERSOS'#39' end)'
      '  where vw.id_man = 0'
      '  group by vw.codpag, pgr.id_regra, vw.nm_cli'
      '  order by vw.codpag ')
    Left = 584
    Top = 112
    object QPGR_transfSUMVLRMER: TFMTBCDField
      FieldName = 'SUM(VLRMER)'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_transfID_REGRA: TFMTBCDField
      FieldName = 'ID_REGRA'
      ReadOnly = True
      Precision = 38
      Size = 0
    end
    object QPGR_transfCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QPGR_transfRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
  end
  object RESTClient1: TRESTClient
    Accept = 'text/xml'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 
      'https://maps.google.com/maps/api/geocode/xml?sensor=false&addres' +
      's=BARUERI+06440260+BR&key=AIzaSyDihfvH4xK6wmayGzot6mUR7htoJ5RNJh' +
      'g'
    Params = <>
    RaiseExceptionOn500 = False
    Left = 708
    Top = 124
  end
  object RESTRequest1: TRESTRequest
    AssignedValues = [rvAccept]
    Accept = 'text/xml'
    Client = RESTClient1
    Params = <>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 792
    Top = 124
  end
  object RESTResponse1: TRESTResponse
    ContentType = 'application/xml'
    Left = 876
    Top = 124
  end
  object SP_valida_veiculo: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'SP_VALIDA_VEICULO'
    Parameters = <
      item
        Name = 'R_RETORNO'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 300
        Value = 
          'Esta Placa Est'#225' com o Vencimento da ANTT Vencido - Procure Depar' +
          'tamento Respons'#225'vel'
      end
      item
        Name = 'VPLACA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = 'KMN-3487'
      end>
    Left = 488
    Top = 292
  end
end
