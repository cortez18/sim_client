object frmConsPallet: TfrmConsPallet
  Left = 0
  Top = 0
  Caption = 'Movimenta'#231#227'o de Pallets'
  ClientHeight = 602
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 461
    Top = 184
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1184
    Height = 601
    ActivePage = TabSheet4
    Align = alTop
    TabOrder = 1
    object TabSheet4: TTabSheet
      Caption = 'Movimento'
      ImageIndex = 3
      object Label3: TLabel
        Left = 12
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Data Inicial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 109
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data Final'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 1016
        Top = 46
        Width = 59
        Height = 16
        Caption = 'Caminho :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Gauge1: TGauge
        Left = 199
        Top = 544
        Width = 811
        Height = 26
        Progress = 0
      end
      object Label14: TLabel
        Left = 205
        Top = 4
        Width = 32
        Height = 13
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 1020
        Top = 553
        Width = 37
        Height = 13
        Caption = 'Saldo :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dtMovimI: TJvDateEdit
        Left = 12
        Top = 20
        Width = 90
        Height = 21
        MinDate = 42370.000000000000000000
        ShowNullDate = False
        TabOrder = 0
      end
      object dtMovimF: TJvDateEdit
        Left = 109
        Top = 20
        Width = 90
        Height = 21
        ShowNullDate = False
        TabOrder = 1
        OnExit = dtMovimFExit
      end
      object BitBtn2: TBitBtn
        Left = 825
        Top = 14
        Width = 71
        Height = 25
        Caption = 'Consultar'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFF8E8E8E919091A9A9A99291928F8E8FFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EA2A2A2E9E9E9FF
          FFFFF1F1F1A2A2A28D8D8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8C8C8CA2A2A2EFF2EE71B16350B14B7CCD7DF6F7F6A2A2A28E8E8EFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C969697D2D7CD3B81211886060A
          96050AA00E84D58EF1F1F19796978C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          8F8F90CBCBCA52792C2A770B218110118B0E0D9F101BAF2DBEEDCACDCDCD9290
          92FFFFFFFFFFFFFFFFFFFFFFFF8C8C8C9A9A9AA7BB9636620738761BC8DAC59B
          BE920F8B0F17AA202BB840E6F6E89A9A9A8D8D8DFFFFFFFFFFFFFFFFFF8F8F90
          CDD0CD3475123B610F9AAC91F4F4F8FCF9FD488E3C05950717A81C3DB643D2D3
          D2918F91FFFFFFFFFFFFFFFFFF919091D7E5D5267908788C65EBE9ECE5E8E5EC
          EDECDCE0DE247D17119D0E099D07DBEADA939293FFFFFFFFFFFFFFFFFF9C9C9D
          E7F1E53C9329C8D1C5D3D1D35C7F41B9C8B2F9F9FABDC9BB1A7D0D1B920EE9F3
          E89C9C9CFFFFFFFFFFFFFFFFFFA9A9A9E3F1E25BB0527DAF6E5A8F437994596B
          8D4CCCD0CAF3F2F4A9BBA4207106F5F9F4A9A9A9FFFFFFFFFFFFFFFFFFA9A9A9
          E4F1E374C67398CB91AFCFA5ACC59D8DA572859A6BF6F5F6C5C0C85D7B47FFFF
          FFA9A9A9FFFFFFFFFFFFFFFFFFA9A9A9E1F0E086D286B7E1B7E3F1E1D0E3CBA3
          C396799E607AA66861994E659350FFFFFFA9A9A9FFFFFFFFFFFFFFFFFF909090
          CDCDCDEFF5EEBAE4BADDF3DDCCE8CBA0CE9A7EB6715F9E4AC4D9BCECEEEBCDCD
          CD909090FFFFFFFFFFFFFFFFFF8F8F8F999799949494B7B8B7E4EFE3A6DDA78D
          D08C74C470DBE9D9B7B7B79595959B9A9C919091FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF949294969696EBECEAE2F2E1EAECE9969696989598939393FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8E8F9494949A
          9A9A959595929192FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 4
        TabStop = False
        OnClick = BitBtn2Click
      end
      object JvDBGrid4: TJvDBGrid
        Left = 0
        Top = 47
        Width = 1010
        Height = 491
        DataSource = dtsMovim
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 5
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = JvDBGrid4DblClick
        OnTitleClick = JvDBGrid4TitleClick
        AlternateRowColor = clSilver
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'MANIFESTO'
            Title.Caption = 'Manifesto'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CLIENTE'
            Title.Caption = 'Cliente'
            Width = 314
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RAZSOC'
            Title.Caption = 'Destino'
            Width = 325
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TRANSP'
            Title.Caption = 'Transportador'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MOTORISTA'
            Title.Caption = 'Motorista'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PLACA'
            Title.Caption = 'Placa'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PALLET'
            Title.Caption = 'Palete'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATINC'
            Title.Caption = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO'
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USUARIO'
            Title.Caption = 'Usu'#225'rio'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QUANT'
            Title.Caption = 'Quant.'
            Width = 43
            Visible = True
          end>
      end
      object JvDirectoryListBox1: TJvDirectoryListBox
        Left = 1016
        Top = 84
        Width = 157
        Height = 459
        Directory = 'C:\SIM'
        DriveCombo = JvDriveCombo1
        ItemHeight = 17
        TabOrder = 6
      end
      object JvDriveCombo1: TJvDriveCombo
        Left = 1016
        Top = 62
        Width = 157
        Height = 22
        DriveTypes = [dtFixed, dtRemote, dtCDROM]
        Offset = 4
        TabOrder = 7
      end
      object JvDBNavigator4: TJvDBNavigator
        Left = 3
        Top = 545
        Width = 190
        Height = 25
        DataSource = dtsMovim
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        TabOrder = 8
      end
      object ledIdCliente: TJvDBLookupEdit
        Left = 205
        Top = 20
        Width = 384
        Height = 21
        DropDownCount = 25
        LookupDisplay = 'CLIENTE'
        LookupField = 'CLIENTE'
        LookupSource = dtsCliente
        CharCase = ecUpperCase
        TabOrder = 2
        Text = ''
      end
      object btnEntrada: TBitBtn
        Left = 1095
        Top = 14
        Width = 78
        Height = 25
        Caption = 'Entrada'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
          52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
          FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
          D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
          FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
          FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
          BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
          FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
          FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
          A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
          B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
          A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
          CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
          B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
          FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
          FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
          F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
          A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
          F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
          F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
          8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
          F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
          F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
          F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
          90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
          D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
          BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 9
        TabStop = False
        OnClick = btnEntradaClick
      end
      object btnExcluir: TBitBtn
        Left = 921
        Top = 14
        Width = 80
        Height = 25
        Caption = 'Excluir'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF003F54C3233A50C27D3853BEDB3551BDF3304BBCF32E4E
          B8DB2B4CB77D2748B523FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF004658C8534255C6E63C52CCFF757AE8FF8F92EEFF8F92EEFF7178
          E4FF334DC1FF2B4AB7E6294BB553FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF004D5ACD534959CBF45C65E0FFA1A6F5FF7E86EFFF5B63E9FF595DE7FF7D84
          EEFF9EA0F4FF515DD7FF2B4AB7F4294BB553FF00FF00FF00FF00FF00FF00545F
          D2225361CFE5616BE3FFA1ACF5FF545FECFF505CEAFF4D59E9FF4E59E6FF4C56
          E6FF5056E6FF9EA2F4FF5460D6FF2A4AB8E5294BB522FF00FF00FF00FF005860
          D47E4B56DBFFA2ABF6FF5664F0FF5266EEFF4D59E9FF4D59E9FF4D59E9FF4D59
          E9FF4C58E6FF525AE6FF9FA3F5FF3450C4FF2A4AB87EFF00FF00FF00FF005C62
          D7DB818CEEFF7E91F7FF5D73F3FF4D59E9FF4D59E9FF4D59E9FF4D59E9FF4D59
          E9FF4D59E9FF4F5BE9FF7B83F0FF757BE2FF2E4BBADBFF00FF00FF00FF005F63
          DAF6A1ABF7FF7086F8FF6882F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4D59E9FF5C66EAFF969CF1FF3250BCF6FF00FF00FF00FF006469
          DBF6AFB9F9FF7F93FAFF7085F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4D59E9FF5E6AEEFF969DF1FF364FBEF6FF00FF00FF00FF00676A
          DEDBA5AFF5FF9DABFAFF778CF0FF545FECFF545FECFF545FECFF545FECFF545F
          ECFF545FECFF6377F2FF818EF4FF787FE9FF3A53C0DBFF00FF00FF00FF006A69
          E07E7D83EAFFCDD4FCFF8B9DFAFF7E93F7FF758AEEFF6C84F6FF6C84F6FF6C84
          F6FF6C84F6FF6379F3FFA4AFF8FF3E4FD0FF3E54C27EFF00FF00FF00FF006C6C
          E1226A69E0E5A3A7F3FFD4DBFDFF879AFAFF7F91F0FF7A8EF1FF7F94F8FF7E92
          F9FF768CF8FFA8B6F8FF636EE3FF4557C7E54156C522FF00FF00FF00FF00FF00
          FF006D6CE3536A69E0F4AAADF2FFD8DCFDFFAEBAFAFF91A3FAFF8B9DFAFF9CA9
          FBFFBAC7FCFF707BE9FF4C5BCCF44858CA53FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF006D6CE3536A6ADFE68E93EDFFBEC3F8FFCCD3F9FFC4CBF9FFAAB4
          F4FF6670E2FF535ED1E6505DCE53FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF006D6DE2236B6AE17D686ADDDB6364DCF36164DAF35D63
          D9DB5B63D67D5862D423FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 10
        TabStop = False
        OnClick = btnExcluirClick
      end
      object edSaldo: TJvCalcEdit
        Left = 1069
        Top = 549
        Width = 81
        Height = 21
        ShowButton = False
        TabOrder = 11
        DecimalPlacesAlwaysShown = False
      end
      object RGEntrada: TRadioGroup
        Left = 620
        Top = 7
        Width = 185
        Height = 33
        Caption = 'Entrada'
        Columns = 2
        Items.Strings = (
          'Positiva'
          'Negativa')
        TabOrder = 3
      end
      object btnImprimir: TBitBtn
        Left = 1014
        Top = 14
        Width = 75
        Height = 25
        Caption = 'Excel'
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
          346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
          2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
          33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
          EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
          38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
          D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
          3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
          D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
          40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
          73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
          33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
          64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
          33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
          33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
          3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
          D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
          49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
          CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
          53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
          C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
          BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
          62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
          56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
          45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
          4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
          BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
          4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
          B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
          49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
          ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
          4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
          4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
        TabOrder = 12
        OnClick = btnImprimirClick
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Entrada Vale Palete'
      ImageIndex = 1
      object Label1: TLabel
        Left = 61
        Top = 20
        Width = 59
        Height = 19
        Caption = 'Origem'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 86
        Top = 173
        Width = 34
        Height = 19
        Caption = 'PBR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 76
        Top = 214
        Width = 44
        Height = 19
        Caption = 'CHEP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNome: TLabel
        Left = 330
        Top = 20
        Width = 35
        Height = 19
        Caption = '       '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 28
        Top = 137
        Width = 92
        Height = 19
        Alignment = taRightJustify
        Caption = 'Documento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 62
        Top = 59
        Width = 58
        Height = 19
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 3
        Top = 99
        Width = 117
        Height = 19
        Caption = 'Transportador'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 13
        Top = 252
        Width = 120
        Height = 19
        Caption = 'DESCART'#193'VEL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 64
        Top = 293
        Width = 69
        Height = 19
        Caption = 'OUTROS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edPBR: TJvCalcEdit
        Left = 139
        Top = 170
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 4
        DecimalPlacesAlwaysShown = False
      end
      object edChep: TJvCalcEdit
        Left = 139
        Top = 211
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
      end
      object btnConfirma: TBitBtn
        Left = 114
        Top = 335
        Width = 109
        Height = 37
        Caption = 'Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
          1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
          96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
          98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
          36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
          6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
          3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
          6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
          42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
          96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
          42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
          FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
          4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
          FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
          54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
          C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
          597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
          71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
          5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
          75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
          FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
          9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
          A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
          52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 6
        OnClick = btnConfirmaClick
      end
      object edOrigem: TJvMaskEdit
        Left = 139
        Top = 17
        Width = 164
        Height = 27
        EditMask = '00\.000\.000\/0000\-00;1;_'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        TabOrder = 0
        Text = '  .   .   /    -  '
        OnExit = edOrigemExit
      end
      object edDcto: TJvCalcEdit
        Left = 139
        Top = 132
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
      end
      object dbcliente: TJvDBLookupEdit
        Left = 139
        Top = 56
        Width = 384
        Height = 27
        DropDownCount = 20
        LookupDisplay = 'CLIENTE'
        LookupField = 'CLIENTE'
        LookupSource = dtsCliente
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = ''
      end
      object dbTRansp: TJvDBLookupEdit
        Left = 139
        Top = 96
        Width = 384
        Height = 27
        DropDownCount = 20
        LookupDisplay = 'NM_FORNECEDOR'
        LookupField = 'NM_FORNECEDOR'
        LookupSource = dtsTransp
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = ''
      end
      object edDesc: TJvCalcEdit
        Left = 139
        Top = 249
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 7
        DecimalPlacesAlwaysShown = False
      end
      object edOutro: TJvCalcEdit
        Left = 139
        Top = 290
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 8
        DecimalPlacesAlwaysShown = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Sa'#237'da Manual de Pallet'
      ImageIndex = 2
      object Label9: TLabel
        Left = 127
        Top = 20
        Width = 62
        Height = 19
        Caption = 'Destino'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 131
        Top = 59
        Width = 58
        Height = 19
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 155
        Top = 169
        Width = 34
        Height = 19
        Caption = 'PBR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 145
        Top = 210
        Width = 44
        Height = 19
        Caption = 'CHEP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 383
        Top = 20
        Width = 35
        Height = 19
        Caption = '       '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblData: TLabel
        Left = 300
        Top = 113
        Width = 5
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 82
        Top = 251
        Width = 120
        Height = 19
        Caption = 'DESCART'#193'VEL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 133
        Top = 292
        Width = 69
        Height = 19
        Caption = 'OUTROS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RGDcto: TJvRadioGroup
        Left = 88
        Top = 84
        Width = 237
        Height = 61
        Caption = 'Docto Sa'#237'da'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Items.Strings = (
          'Manifesto'
          'Romaneio')
        ParentFont = False
        TabOrder = 6
        OnClick = RGDctoClick
        CaptionVisible = False
        EdgeBorders = []
      end
      object edDestino: TJvMaskEdit
        Left = 208
        Top = 17
        Width = 164
        Height = 27
        EditMask = '00\.000\.000\/0000\-00;1;_'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        TabOrder = 0
        Text = '  .   .   /    -  '
        OnExit = edDestinoExit
      end
      object JvDBLookupEdit1: TJvDBLookupEdit
        Left = 208
        Top = 56
        Width = 384
        Height = 27
        DropDownCount = 20
        LookupDisplay = 'CLIENTE'
        LookupField = 'CLIENTE'
        LookupSource = dtsCliente
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = ''
      end
      object JvCalcEdit2: TJvCalcEdit
        Left = 208
        Top = 166
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
      end
      object JvCalcEdit3: TJvCalcEdit
        Left = 208
        Top = 207
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
      end
      object btnConfirmaS: TBitBtn
        Left = 182
        Top = 347
        Width = 109
        Height = 37
        Caption = 'Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
          1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
          96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
          98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
          36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
          6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
          3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
          6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
          42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
          96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
          42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
          FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
          4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
          FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
          54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
          C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
          597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
          71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
          5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
          75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
          FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
          9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
          A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
          52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 4
        OnClick = btnConfirmaSClick
      end
      object JvCalcEdit1: TJvCalcEdit
        Left = 208
        Top = 105
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
        OnExit = JvCalcEdit1Exit
      end
      object JvCalcEdit4: TJvCalcEdit
        Left = 208
        Top = 248
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 7
        DecimalPlacesAlwaysShown = False
      end
      object JvCalcEdit5: TJvCalcEdit
        Left = 208
        Top = 289
        Width = 69
        Height = 27
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 8
        DecimalPlacesAlwaysShown = False
      end
    end
  end
  object Qmovimento: TADOQuery
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    BeforeOpen = QmovimentoBeforeOpen
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 12
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 12
      end
      item
        Name = '2'
        DataType = ftInteger
        Size = -1
        Value = 12
      end>
    SQL.Strings = (
      
        'select distinct p.id, p.manifesto, p.pallet, p.datinc, p.tipo, p' +
        '.usuario, (p.quant * -1) quant, d.nomeab razsoc, c.razsoc client' +
        'e, t.razsoc transp, p.destino, mo.nommot motorista, ma.placa'
      
        'from tb_controle_pallet p left join tb_manitem M on p.manifesto ' +
        '= M.manifesto'
      
        '                          left join tb_manifesto ma on ma.manife' +
        'sto = M.manifesto and ma.serie = m.serie and ma.filial = m.filia' +
        'l'
      
        '                          left join tb_conhecimento N on m.CODDO' +
        'C = N.nr_conhecimento'
      
        '                          left join cyber.RODCLI D on n.cod_dest' +
        'inatario = D.CODCLIFOR'
      
        '                          left join cyber.rodcli C on c.codclifo' +
        'r = n.cod_pagador'
      
        '                          left join cyber.rodcli T on t.codclifo' +
        'r = ma.transp left join cyber.rodmot MO on ma.motorista = mo.cod' +
        'mot'
      
        'where p.datinc > to_date('#39'01/01/2016 00:00'#39','#39'dd/mm/yy hh24:mi'#39') ' +
        'and d.codcgc = p.destino  and p.vale is null and p.filial = :0'
      ''
      ''
      'union all'
      ''
      
        'select distinct p.id, p.manifesto, p.pallet, p.datinc, p.tipo, p' +
        '.usuario, p.quant, d.razsoc, p.cliente, p.transp , p.destino, '#39#39 +
        ' motorista, '#39#39' placa'
      
        'from tb_controle_pallet p left join cyber.RODCLI D on p.destino ' +
        '= D.codcgc'
      
        'where p.datinc > to_date('#39'01/01/2016 00:00'#39','#39'dd/mm/yy hh24:mi'#39') ' +
        'and p.filial = :1  and p.vale = '#39'S'#39
      ''
      ''
      'union all'
      ''
      
        'select distinct p.id, p.manifesto, p.pallet, p.datinc, p.tipo, p' +
        '.usuario, (p.quant * -1) quant, d.nomeab razsoc, R.razsoc client' +
        'e, t.razsoc transp, p.destino, rw.motorista, rw.placa'
      
        'from tb_controle_pallet p left join tb_romaneio_wms rw on p.mani' +
        'festo = rw.nr_romaneio'
      
        '                          left join cyber.RODCLI D on p.destino ' +
        '= fnc_cnpj(D.codcgc)'
      
        '                          left join auftraege@wmsv11 pw on pw.pe' +
        'rcent = rw.nr_romaneio'
      
        '                          left join klienten@wmsv11 k on pw.id_k' +
        'lient = k.id_klient'
      
        '                          left join cyber.RODCLI R on (replace(r' +
        'eplace(replace(R.codcgc, '#39'.'#39', '#39#39'),'#39'/'#39','#39#39'),'#39'-'#39','#39#39')) = k.suchbegri' +
        'ff'
      
        '                          left join cyber.rodcli T on t.codclifo' +
        'r = rw.cod_transp left join cyber.rodcli T on t.codclifor = rw.c' +
        'od_transp'
      
        'where p.datinc > to_date('#39'01/01/2016 00:00'#39','#39'dd/mm/yy hh24:mi'#39') ' +
        'and p.filial = :2 and p.vale is null and r.razsoc is not null'
      ''
      'order by datinc, pallet, manifesto, tipo desc')
    Left = 412
    Top = 242
    object QmovimentoMANIFESTO: TBCDField
      FieldName = 'MANIFESTO'
      Precision = 32
      Size = 0
    end
    object QmovimentoCLIENTE: TStringField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Size = 80
    end
    object QmovimentoRAZSOC: TStringField
      FieldName = 'RAZSOC'
      ReadOnly = True
      Size = 80
    end
    object QmovimentoPALLET: TStringField
      FieldName = 'PALLET'
    end
    object QmovimentoDATINC: TDateTimeField
      FieldName = 'DATINC'
    end
    object QmovimentoTIPO: TStringField
      FieldName = 'TIPO'
      Size = 1
    end
    object QmovimentoUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
    object QmovimentoQUANT: TBCDField
      FieldName = 'QUANT'
      Precision = 32
      Size = 0
    end
    object QmovimentoID: TBCDField
      FieldName = 'ID'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QmovimentoTRANSP: TStringField
      FieldName = 'TRANSP'
      ReadOnly = True
      Size = 80
    end
    object QmovimentoDESTINO: TStringField
      FieldName = 'DESTINO'
      ReadOnly = True
    end
    object QmovimentoMOTORISTA: TStringField
      FieldName = 'MOTORISTA'
      ReadOnly = True
      Size = 60
    end
    object QmovimentoPLACA: TStringField
      FieldName = 'PLACA'
      ReadOnly = True
      Size = 8
    end
  end
  object dtsMovim: TDataSource
    DataSet = Qmovimento
    Left = 476
    Top = 244
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct c.razsoc cliente'
      'from cyber.rodcli C'
      'where c.codean = '#39'PALLET'#39
      'order by 1')
    Left = 136
    Top = 138
    object QClienteCLIENTE: TStringField
      FieldName = 'CLIENTE'
      ReadOnly = True
      Size = 80
    end
  end
  object dtsCliente: TDataSource
    DataSet = QCliente
    Left = 192
    Top = 140
  end
  object QQuant: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '2'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = '3'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select pallet, quant'
      'from tb_controle_pallet pp '
      'where manifesto = :0'
      'and filial = :1'
      'and destino = :2'
      'and cliente = :3'
      'and tipo = '#39'E'#39)
    Left = 268
    Top = 248
    object QQuantPALLET: TStringField
      FieldName = 'PALLET'
    end
    object QQuantQUANT: TBCDField
      FieldName = 'QUANT'
      Precision = 32
      Size = 0
    end
  end
  object QTRansp: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT C.NOMEAB NM_FORNECEDOR'
      'FROM cyber.RODCLI C  '
      'WHERE C.CODPAD IN (1)  '
      'AND c.situac = '#39'A'#39' and UPPER(C.CLASSI) IN ('#39'4'#39','#39'9'#39','#39'11'#39')')
    Left = 296
    Top = 158
    object QTRanspNM_FORNECEDOR: TStringField
      FieldName = 'NM_FORNECEDOR'
    end
  end
  object dtsTransp: TDataSource
    DataSet = QTRansp
    Left = 352
    Top = 160
  end
end
