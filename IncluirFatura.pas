unit IncluirFatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, Menus, JvMemoryDataset, JvToolEdit, Mask, JvExMask,
  JvBaseEdits, StdCtrls, Buttons, JvGradient, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, JvDBUltimGrid, ExtCtrls, JvExControls, JvEnterTab, Inifiles,
  JvDBLookup, ImgList, strutils, JvExStdCtrls, JvMemo,
  ShellAPI, ComObj, ExcelXP, JvCombobox, System.ImageList, JvComponentBase, DateUtils;

type
  TfrmIncluirFatura = class(TForm)
    Panel3: TPanel;
    JvGradient1: TJvGradient;
    Label1: TLabel;
    btFatura: TBitBtn;
    ds: TDataSource;
    Panel2: TPanel;
    JvEnterAsTab1: TJvEnterAsTab;
    btVerificar: TBitBtn;
    mdTemp: TJvMemoryData;
    QCTRC: TADOQuery;
    edTotal: TJvCalcEdit;
    Label2: TLabel;
    edVcto: TJvDateEdit;
    Label3: TLabel;
    btnImprimir: TBitBtn;
    Label4: TLabel;
    edDesc: TJvCalcEdit;
    Label5: TLabel;
    edTT: TJvCalcEdit;
    mdTempreg: TIntegerField;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    ledIdCliente: TJvDBLookupEdit;
    Label6: TLabel;
    ledCliente: TJvDBLookupEdit;
    Label7: TLabel;
    cbOperacao: TJvDBLookupEdit;
    edFatura: TEdit;
    Label15: TLabel;
    QOperacao: TADOQuery;
    dtsOperacao: TDataSource;
    QClientes: TADOQuery;
    dtsClientes: TDataSource;
    QClientesCOD_CLIENTE: TBCDField;
    QClientesNM_CLIENTE: TStringField;
    QClientesNR_CNPJ_CPF: TStringField;
    QCTRCNR_CONHECIMENTO: TBCDField;
    QCTRCDT_CONHECIMENTO: TDateTimeField;
    QCTRCNM_CLIENTE_EXP: TStringField;
    QCTRCNM_CLIENTE_DES: TStringField;
    QCTRCVL_TOTAL: TBCDField;
    QCTRCNM_CLIENTE: TStringField;
    QCTRCOPERACAO: TStringField;
    mdTempctrc: TIntegerField;
    mdTempcliente: TStringField;
    mdTempexpedidor: TStringField;
    mdTempdestinatario: TStringField;
    mdTempconsignatario: TStringField;
    mdTempvalor: TFloatField;
    mdTempoperacao: TStringField;
    QCTRCNM_CLIENTE_CON: TStringField;
    iml: TImageList;
    QCTRCCOD_CLIENTE: TBCDField;
    SPFatura: TADOStoredProc;
    QCTRCCNPJLOJA: TStringField;
    mdTempFL_TIPO: TStringField;
    mdTempserie: TStringField;
    QCTRCNR_SERIE: TStringField;
    cbT: TCheckBox;
    Panel1: TPanel;
    Panel4: TPanel;
    dbgFatura: TJvDBUltimGrid;
    JvMemo1: TJvMemo;
    mdTempgnre: TStringField;
    mdTempcfop: TIntegerField;
    mdTemppago: TIntegerField;
    QCTRCCODCLIFOR: TBCDField;
    QCTRCNATOPE: TBCDField;
    QCTRCNMGNRE: TStringField;
    QCTRCPAGOU: TBCDField;
    mdTempRegra: TIntegerField;
    SPITens: TADOStoredProc;
    lblCtrc: TLabel;
    EdObs: TJvMemo;
    Label8: TLabel;
    mdTempdt_cte: TDateField;
    mdTempstatus: TStringField;
    btnListagem: TBitBtn;
    Panel8: TPanel;
    QCTRCUFCL: TStringField;
    QCTRCUFRE: TStringField;
    QCTRCUFDE: TStringField;
    RGTipo: TRadioGroup;
    btnRelat: TBitBtn;
    QRelat: TADOQuery;
    dtsRelat: TDataSource;
    QRelatNR_CTRC: TBCDField;
    QRelatDT_CTRC: TDateTimeField;
    QRelatNR_SERIE: TStringField;
    QRelatNM_CLI: TStringField;
    QRelatCNPJ_CLI: TStringField;
    QRelatNM_CLIENTE_EXP: TStringField;
    QRelatNR_CNPJ_CPF_EXP: TStringField;
    QRelatCIDADE_EXP: TStringField;
    QRelatUF_EXP: TStringField;
    QRelatNM_CLIENTE_DES: TStringField;
    QRelatNR_CNPJ_CPF_DES: TStringField;
    QRelatCIDADE_DES: TStringField;
    QRelatUF_DES: TStringField;
    QRelatNM_CLIENTE_RED: TStringField;
    QRelatNR_CNPJ_CPF_RED: TStringField;
    QRelatCIDADE_RED: TStringField;
    QRelatUF_RED: TStringField;
    QRelatVL_NF: TBCDField;
    QRelatPESO_KG: TBCDField;
    QRelatQT_VOLUME: TBCDField;
    QRelatNR_NOTA: TStringField;
    QRelatVL_FRETE: TBCDField;
    QRelatVL_PEDAGIO: TBCDField;
    QRelatVL_SEGURO: TBCDField;
    QRelatVL_GRIS: TBCDField;
    QRelatVL_OUTROS: TBCDField;
    QRelatVL_TOTAL: TBCDField;
    QRelatCAT: TBCDField;
    QRelatVL_ADEME: TBCDField;
    QRelatVL_DESPACHO: TBCDField;
    QRelatVL_DESCARGA: TBCDField;
    QRelatVL_ENTREGA: TBCDField;
    QRelatVL_TDE: TBCDField;
    QRelatVL_ADICIONAL: TBCDField;
    QRelatDESCRICAO_ADICIONAL: TStringField;
    QRelatVL_ALIQUOTA: TBCDField;
    QRelatVL_IMPOSTO: TBCDField;
    QRelatFL_TIPO: TStringField;
    QRelatNR_FATURA: TBCDField;
    QRelatCNPJ_TRANSPORTADORA: TStringField;
    QRelatIESTADUAL: TStringField;
    QRelatOPERACAO: TStringField;
    QRelatCFOP_CLESS: TStringField;
    QRelatTIPCON: TStringField;
    QRelatFATURA: TBCDField;
    btnSKF: TBitBtn;
    QSKF: TADOQuery;
    QSKFNUMDUP: TBCDField;
    QSKFCODCON: TBCDField;
    QSKFSERCON: TStringField;
    QSKFTOTFRE: TBCDField;
    QSKFICMVLR: TBCDField;
    QSKFBASCAL: TBCDField;
    QSKFCTE_ID: TStringField;
    QSKFNATOPE: TBCDField;
    QSKFCODREM: TBCDField;
    QSKFALICOT: TBCDField;
    QNF: TADOQuery;
    QNFNOTFIS: TStringField;
    QSKFFILDOC: TBCDField;
    QSKFCTEDTA: TDateTimeField;
    QRelatPESO_CUB: TBCDField;
    QRelatCTE_ID: TStringField;
    QRelatOBSCON: TStringField;
    QSKFESTADO: TStringField;
    cbRegiao: TJvComboBox;
    QCTRCITNLIN: TStringField;
    QCTRCTIPCON: TStringField;
    QCTRCFL_EMPRESA: TBCDField;
    QOperacaoOPERACAO: TStringField;
    QPrazo: TADOQuery;
    QPrazoprazo_pgto: TBCDField;
    QPrazodis_semana: TBCDField;
    QCTRCLimite: TBCDField;
    QCTRCprazo_pgto: TBCDField;
    QCTRCdia_semana: TBCDField;
    QCTRCPRAZO_ESPECIAL: TStringField;
    QRegra: TADOQuery;
    QRegraTEM: TFMTBCDField;
    QCTRCFL_TIPO_2: TStringField;
    QCTRCfl_fatura: TStringField;
    Label16: TLabel;
    cbServico: TComboBox;
    QServico: TADOQuery;
    QServicoDESCR_SERVICO: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure edValorKeyPress(Sender: TObject; var Key: Char);
    procedure btFaturaClick(Sender: TObject);
    procedure btVerificarClick(Sender: TObject);
    procedure dbgColetaDblClick(Sender: TObject);
    procedure edDescExit(Sender: TObject);
    procedure edVctoExit(Sender: TObject);
    procedure QOperacaoBeforeOpen(DataSet: TDataSet);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure dbgFaturaCellClick(Column: TColumn);
    procedure dbgFaturaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbTClick(Sender: TObject);
    procedure QClientesBeforeOpen(DataSet: TDataSet);
    procedure Panel1Click(Sender: TObject);
    procedure JvMemo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QCTRCBeforeOpen(DataSet: TDataSet);
    function Regra(CTE: Integer): Integer;
    function RegraVcto(): TDateTime;
    procedure dbgFaturaTitleClick(Column: TColumn);
    procedure btnListagemClick(Sender: TObject);
    procedure btnRelatClick(Sender: TObject);
    procedure btnSKFClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIncluirFatura: TfrmIncluirFatura;
  tt, ts: real;
  dife: string;

implementation

uses Dados, menu, funcoes;

{$R *.dfm}

procedure TfrmIncluirFatura.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  // md.Close;
  Action := caFree;
  frmIncluirFatura := nil;
  QClientes.close;
  QCTRC.close;
end;

procedure TfrmIncluirFatura.FormCreate(Sender: TObject);
begin
  mdTemp.close;
  mdTemp.Open;
  QClientes.Open;
  QServico.Close;
  QServico.Open;
  cbServico.clear;
  cbServico.Items.add('');
  while not QServico.eof do
  begin
    cbServico.Items.add
      (UpperCase(QServicoDESCR_SERVICO.AsString));
    QServico.next;
  end;
  QServico.close;
end;

procedure TfrmIncluirFatura.JvMemo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
  begin
    JvMemo1.Clear;
    JvMemo1.Visible := False;
  end;
end;

procedure TfrmIncluirFatura.ledIdClienteCloseUp(Sender: TObject);
begin
  if (ledIdCliente.Text <> '') or (ledCliente.Text <> '') then
  begin
    cbOperacao.Clear;
    ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
    ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
    QOperacao.close;
    if copy(dtInicial.Text, 1, 1) <> ' ' then
      QOperacao.SQL[9] := ' and c.dt_conhecimento between to_date(''' +
        dtInicial.Text + ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' +
        dtFinal.Text + ' 23:59'',''dd/mm/yy hh24:mi'')';
    QOperacao.Parameters[1].value := ledIdCliente.LookupValue;
    QOperacao.Open;

    if (pos('CNH', ledCliente.Text) > 0) or (pos('SKF', ledCliente.Text) > 0) or
    // (pos('MERITOR',ledCliente.Text) > 0) or
      (pos('IVECO', ledCliente.Text) > 0) then
      Panel1.Visible := true
    else
      Panel1.Visible := False;
    mdTemp.close;
    edVcto.date := RegraVcto;
  end;
end;

procedure TfrmIncluirFatura.edValorKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0' .. '9', #8]) then
    Key := #0;
end;

procedure TfrmIncluirFatura.edVctoExit(Sender: TObject);
var
  dia: String;
begin
  if edVcto.date < date then
  begin
    ShowMessage('Data de Vencimento inferior a data de hoje');
    edVcto.SetFocus;
  end;
  if edVcto.date > (date + 45) then
  begin
    ShowMessage('Data de Vencimento Maior que 45 dias');
    edVcto.SetFocus;
  end;

  if (QCTRCdia_semana.AsInteger > 0) then
  begin
    if DayOfWeek(edVcto.date) <> QCTRCdia_semana.AsInteger then
    begin
      if QCTRCdia_semana.AsInteger = 2 then
        dia := 'Segunda-Feira'
      else if QCTRCdia_semana.AsInteger = 3 then
        dia := 'Ter�a-Feira'
      else if QCTRCdia_semana.AsInteger = 4 then
        dia := 'Quarta-Feira'
      else if QCTRCdia_semana.AsInteger = 5 then
        dia := 'Quinta-Feira'
      else if QCTRCdia_semana.AsInteger = 6 then
        dia := 'Sexta-Feira';

      ShowMessage
        ('O dia de semana para vencimento deste cliente deve ser ' + dia);
      edVcto.SetFocus;
    end;
  end;
end;

procedure TfrmIncluirFatura.edDescExit(Sender: TObject);
begin
  edTT.value := edTotal.value - edDesc.value;
end;

procedure TfrmIncluirFatura.btFaturaClick(Sender: TObject);
var
  iCount, vezes, limitado: Integer;
  fat: string;
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  if mdTemp.RecordCount = 0 then
    Exit;

  if dife = 'S' then
  begin
    ShowMessage('Existem Clientes Diferentes para a Mesma Fatura !!');
    Exit;
  end;

  if copy(edVcto.Text, 1, 2) = '  ' then
  begin
    ShowMessage('Informe o data de vencimento');
    edVcto.SetFocus;
    Exit;
  end;

  dtmDados.IQuery1.close;
  dtmDados.IQuery1.SQL.Clear;
  dtmDados.IQuery1.SQL.add
    ('select nvl(limite,0) limite from TB_LIMITE_FATURAMENTO where codcliente = :0');
  dtmDados.IQuery1.Parameters[0].value := QCTRCCOD_CLIENTE.AsInteger;
  dtmDados.IQuery1.Open;

  if (dtmDados.IQuery1.FieldByName('limite').value = 0) or (dtmDados.IQuery1.RecordCount = 0) then
    limitado := 10000
  else
    limitado := dtmDados.IQuery1.FieldByName('limite').value;

  // if (pos('SKF',ledCliente.Text) > 0) then limitado := 60;

  // if (pos('CNH',ledCliente.Text) > 0) or
  // (pos('IVECO',ledCliente.Text) > 0) then limitado := 1;

  // if (pos('BRIL',ledCliente.Text) > 0) then limitado := 1;

  if Application.Messagebox('Voc� Deseja Gerar esta Fatura ?', 'Gerar Fatura',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    btFatura.Enabled := False;
    Screen.Cursor := crHourGlass;
    mdTemp.First;
    iCount := 1;
    vezes := 1;
    while not mdTemp.Eof do
    begin
      if mdTempreg.value <> 1 then
      begin
        if iCount = 1 then
        begin
          SPFatura.Parameters[1].value := QCTRCCOD_CLIENTE.AsInteger;
          SPFatura.Parameters[2].value := edVcto.date;
          SPFatura.Parameters[3].value := edTotal.value;
          SPFatura.Parameters[4].value := GlbUser;
          SPFatura.Parameters[5].value := glbfilial;
          SPFatura.Parameters[6].value := edDesc.value;
          SPFatura.Parameters[7].value := EdObs.Text;
          SPFatura.ExecProc;
          if vezes = 1 then
            fat := IntToStr(SPFatura.Parameters[0].value)
          else
            fat := fat + ',' + IntToStr(SPFatura.Parameters[0].value);
          edFatura.Text := IntToStr(SPFatura.Parameters[0].value);
          SPFatura.close;
          SPITens.Parameters[0].value := StrToInt(edFatura.Text);
          SPITens.Parameters[1].value := mdTempvalor.value;
          SPITens.Parameters[2].value := GlbUser;
          SPITens.Parameters[3].value := glbfilial;
          SPITens.Parameters[4].value := mdTempctrc.AsInteger;
          SPITens.ExecProc;
          SPITens.close;
          vezes := vezes + 1;
        end
        else
        begin
          SPFatura.close;
          SPITens.Parameters[0].value := StrToInt(edFatura.Text);
          SPITens.Parameters[1].value := mdTempvalor.value;
          SPITens.Parameters[2].value := GlbUser;
          SPITens.Parameters[3].value := glbfilial;
          SPITens.Parameters[4].value := mdTempctrc.AsInteger;
          SPITens.ExecProc;
          SPITens.close;
        end;
        inc(iCount);

        if iCount > limitado then
        begin
          dtmDados.RQuery1.close;
          dtmDados.RQuery1.SQL.Clear;
          dtmDados.RQuery1.SQL.add
            ('update cyber.roddup set vlrliq = (select sum(VLRDOC) from cyber.rodidu where numdup = :0 and codfil = :1), ');
          dtmDados.RQuery1.SQL.add
            ('vlrdup = (select sum(VLRDOC) from cyber.rodidu where numdup = :2 and codfil = :3) ');
          dtmDados.RQuery1.SQL.add('where numdup = :4 and codfil = :5 ');
          dtmDados.RQuery1.Parameters[0].value := StrToInt(edFatura.Text);
          dtmDados.RQuery1.Parameters[1].value := glbfilial;
          dtmDados.RQuery1.Parameters[2].value := StrToInt(edFatura.Text);
          dtmDados.RQuery1.Parameters[3].value := glbfilial;
          dtmDados.RQuery1.Parameters[4].value := StrToInt(edFatura.Text);
          dtmDados.RQuery1.Parameters[5].value := glbfilial;
          dtmDados.RQuery1.ExecSQL;
          iCount := 1;
        end;
      end;
      mdTemp.Next;
    end;

    {

      mdTemp.First;
      while not mdTemp.Eof do
      begin
      if MdTempreg.Value <> 1 then
      begin
      SPItens.Parameters[0].value := StrToInt(edFatura.text);
      SPItens.Parameters[1].value := mdTempvalor.Value;
      SPItens.Parameters[2].Value := GlbUser;
      SPItens.Parameters[3].Value := glbfilial;
      SPItens.Parameters[4].Value := mdTempctrc.AsInteger;
      SPItens.ExecProc;
      SPItens.Close;
      end;
      mdTemp.Next;
      end; }

    Application.ProcessMessages;
    Screen.Cursor := crDefault;
    ShowMessage('Fatura(s) Gerada(s) ' + fat);
    btnImprimir.Enabled := true;
    btnRelat.Enabled := true;
    btFatura.Enabled := False;
    mdTemp.close;
    edVcto.Clear;
    edTotal.value := 0;
    lblCtrc.caption := '';
    EdObs.Clear;
  end;
end;

procedure TfrmIncluirFatura.btnListagemClick(Sender: TObject);
var
  memo2: TStringList;
  i: Integer;
begin
  if mdTemp.RecordCount > 0 then
  begin
    if FileExists('fatura.html') then
      DeleteFile('fatura.html');

    memo2 := TStringList.Create;
    memo2.add('<HTML><HEAD><TITLE>Gera��o de Faturas</TITLE>');
    memo2.add
      ('<STYLE>.titulo1 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo2.add
      ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
    memo2.add
      ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
    memo2.add(
      '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
    memo2.add(
      '       .texto2 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
    memo2.add(
      '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo2.add('</STYLE>');
    memo2.add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
    memo2.add('<P align=center class=titulo1>Gera��o de Faturas</P>');
    memo2.add('<Table border=2 bordercolor="#005CB9" align=center>');
    memo2.add('<TBODY>');

    for i := 1 to dbgFatura.Columns.Count - 1 do
    begin
      memo2.add('<th><FONT class=texto1>' + dbgFatura.Columns[i].Title.caption
        + '</th>');
    end;
    memo2.add('</tr>');
    mdTemp.First;
    while not mdTemp.Eof do
    begin
      for i := 1 to dbgFatura.Columns.Count - 1 do
        if mdTemp.FieldByName(dbgFatura.Columns[0].FieldName).AsInteger = 1 then
          memo2.add('<th><FONT class=texto3>' + mdTemp.FieldByName
            (dbgFatura.Columns[i].FieldName).AsString + '</th>')
        else
          memo2.add('<th><FONT class=texto2>' + mdTemp.FieldByName
            (dbgFatura.Columns[i].FieldName).AsString + '</th>');
      mdTemp.Next;
      memo2.add('<tr>');
    end;
    mdTemp.First;
    memo2.add('</TABLE></TBODY><BR>');

    memo2.SaveToFile('fatura.html');
    memo2.Free;

    ShellExecute(Application.Handle, nil, PChar('fatura.html'), nil, nil,
      SW_SHOWNORMAL);
  end;
end;

procedure TfrmIncluirFatura.btnRelatClick(Sender: TObject);
var
  sarquivo: String;
  i: Integer;
  memo1: TStringList;
begin
  QRelat.close;
  QRelat.Parameters[0].value := edFatura.Text;
  QRelat.Open;

  if not QRelat.Eof then
  begin
    sarquivo := 'Rel_Fatura_' + edFatura.Text + '.xls';
    memo1 := TStringList.Create;
    memo1.add('<HTML>');
    memo1.add('  <HEAD>');
    memo1.add('    <TITLE>IW - Intecom</TITLE>');
    memo1.add(
      '       <STYLE>.texto1  {FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
    memo1.add(
      '              .texto2  {FONT: 10px Arial, Helvetica, sans-serif; COLOR: #00000}');
    memo1.add('       </STYLE>');
    memo1.add('  </HEAD>');
    memo1.add('  <BODY <Font Color="#004080">');
    memo1.add('    <Center>');
    memo1.add('      <Table border=1 align=center>');
    memo1.add('        <tr>');
    for i := 0 to QRelat.FieldCount - 1 do
    begin
      memo1.add('          <td><FONT class=texto1>' + QRelat.Fields[i].FieldName
        + '</td>');
    end;
    memo1.add('        </tr>');
    memo1.add('        <tr>');
    QRelat.First;
    while not QRelat.Eof do
    begin
      for i := 0 to QRelat.FieldCount - 1 do
        memo1.add('          <td><FONT class=texto2>' +
          QRelat.FieldByName(QRelat.Fields[i].FieldName).AsString + '</td>');
      QRelat.Next;
      memo1.add('        </tr>');
    end;
    memo1.add('      </table>');
    memo1.add('    </center>');
    memo1.add('  </body>');
    memo1.add('</html>');
    memo1.SaveToFile(sarquivo);
    try
      ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
        SW_SHOWNORMAL);
    except
      ShowMessage('Excel N�o Instalado, arquivo gravado em ' + sarquivo);
    end;
  end;
end;

procedure TfrmIncluirFatura.btnSKFClick(Sender: TObject);
var
  Ini: TIniFile;
  PastaModelo, notas: string;
  Excel: OleVariant;
  wb: Variant;
  Linha, Coluna: Integer;
begin
  if edFatura.Text <> '' then
  begin
    Panel8.Visible := true;
    Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
    PastaModelo := Ini.ReadString('RELATORIO', 'CAMINHO', '');
    Ini.Free;
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    wb := Excel.Workbooks.add(PastaModelo +
      'Integra��o_Frete_Retroativo_Lote.xlt');
    Linha := 3;
    Coluna := 1;
    // Preencher ABA DOF
    QSKF.close;
    QSKF.Parameters[0].value := StrToInt(edFatura.Text);
    QSKF.Open;
    while not QSKF.Eof do
    begin
      Excel.Workbooks[1].Sheets[1].Range['A1:FY1'].copy;
      Excel.Workbooks[1].Sheets[1].Range['A' + IntToStr(Linha) + ':FY' +
        IntToStr(Linha)].Insert(Shift := xlDown);
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 3] :=
        'GKO_INTEGRACAO_OPEN_' + QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 6] :=
        QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 7] :=
        QSKFSERCON.AsString;
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 8] :=
        apcarac(apcarac(formatdatetime('dd/mm/yyyy', QSKFCTEDTA.value)));
      if QSKFCODREM.value = 152 then
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 11] := 'E352.04'
      else
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 11] := 'E352.02';
      if QSKFESTADO.value = 'SP' then
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 12] := '1.352'
      else
        Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 13] := '2.352';
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 81] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 82] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 91] :=
        BuscaTroca(QSKFICMVLR.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 92] :=
        BuscaTroca(QSKFBASCAL.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna + 172] :=
        QSKFCTE_ID.AsString;
      inc(Linha);
      QSKF.Next;
    end;
    Linha := 3;
    Coluna := 1;
    // Preencher ABA IDF
    QSKF.First;
    while not QSKF.Eof do
    begin
      QNF.close;
      QNF.Parameters[0].value := QSKFCODCON.AsInteger;
      QNF.Parameters[1].value := QSKFSERCON.AsString;
      QNF.Parameters[2].value := QSKFFILDOC.AsInteger;
      QNF.Open;
      notas := '';
      while not QNF.Eof do
      begin
        notas := notas + QNFNOTFIS.value + ' ';
        QNF.Next;
      end;
      QNF.close;
      Excel.Workbooks[1].Sheets[2].Range['A1:CW1'].copy;
      Excel.Workbooks[1].Sheets[2].Range['A' + IntToStr(Linha) + ':CW' +
        IntToStr(Linha)].Insert(Shift := xlDown);
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 3] :=
        'GKO_INTEGRACAO_OPEN_' + QSKFCODCON.AsString;
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 16] := notas;
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 18] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 19] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 26] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 27] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 28] :=
        BuscaTroca(QSKFTOTFRE.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 35] :=
        BuscaTroca(QSKFALICOT.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 36] :=
        BuscaTroca(QSKFBASCAL.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 37] :=
        BuscaTroca(QSKFICMVLR.AsString, ',', '.');
      Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 38] :=
        BuscaTroca(QSKFBASCAL.AsString, ',', '.');
      if QSKFCODREM.value = 152 then
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 74] := 'E352.04'
      else
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 74] := 'E352.02';
      if QSKFESTADO.value = 'SP' then
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 75] := '1.352'
      else
        Excel.Workbooks[1].Sheets[2].Cells[Linha, Coluna + 75] := '2.352';
      inc(Linha);
      QSKF.Next;
    end;
    Excel.Workbooks[1].Sheets[1].Rows['1:1'].Delete(Shift := xlUp);
    Excel.Workbooks[1].Sheets[2].Rows['1:1'].Delete(Shift := xlUp);
    Excel.Visible := true;

    // relat�rio de faturamento
    QRelat.close;
    QRelat.Parameters[0].value := edFatura.Text;
    QRelat.Open;

    if not QRelat.Eof then
    begin

      Linha := 2;
      Excel := CreateOleObject('Excel.Application');
      Excel.caption := 'Arquivo de Faturamento';
      Excel.Visible := False;
      Excel.Workbooks.add(1);
      // TITULO DAS COLUNAS

      Excel.Cells[1, 1] := 'Cliente';
      Excel.Cells[1, 2] := 'CNPJ Cliente';
      Excel.Cells[1, 3] := 'Cliente Origem';
      Excel.Cells[1, 4] := 'CNPJ Origem';
      Excel.Cells[1, 5] := 'Cidade Origem';
      Excel.Cells[1, 6] := 'UF Origem';
      Excel.Cells[1, 7] := 'Cliente Destino';
      Excel.Cells[1, 8] := 'CNPJ Destino';
      Excel.Cells[1, 9] := 'Cidade Destino';
      Excel.Cells[1, 10] := 'UF Destino';
      Excel.Cells[1, 11] := 'Redespacho';
      Excel.Cells[1, 12] := 'CNPJ Redespacho';
      Excel.Cells[1, 13] := 'Cidade Redespacho';
      Excel.Cells[1, 14] := 'UF Redespacho';
      Excel.Cells[1, 15] := 'Data CT-e';
      Excel.Cells[1, 16] := 'CT-e';
      Excel.Cells[1, 17] := 'N� S�rie';
      Excel.Cells[1, 18] := 'N� NF';
      Excel.Cells[1, 19] := 'Valor NF';
      Excel.Cells[1, 20] := 'Peso KG';
      Excel.Cells[1, 21] := 'Peso CUB';
      Excel.Cells[1, 22] := 'Volumes';
      Excel.Cells[1, 23] := 'Valor Frete';
      Excel.Cells[1, 24] := 'Valor Ped�gio';
      Excel.Cells[1, 25] := 'Valor Seguro';
      Excel.Cells[1, 26] := 'Valor GRIS';
      Excel.Cells[1, 27] := 'Valor Outros';
      Excel.Cells[1, 28] := 'Valor CAT';
      Excel.Cells[1, 29] := 'Valor Ademe';
      Excel.Cells[1, 30] := 'Valor Despacho';
      Excel.Cells[1, 31] := 'Valor Descarga';
      Excel.Cells[1, 32] := 'Valor Entrega';
      Excel.Cells[1, 33] := 'Valor TDE';
      Excel.Cells[1, 34] := 'Valor Adicional';

      Excel.Cells[1, 35] := 'Valor Total';
      Excel.Cells[1, 36] := 'Aliquota';
      Excel.Cells[1, 37] := 'Valor Imposto';
      Excel.Cells[1, 38] := 'Opera��o';
      Excel.Cells[1, 39] := 'Ve�culo';
      Excel.Cells[1, 40] := 'Fatura';
      Excel.Cells[1, 41] := 'Chave Acesso CT-e';
      Excel.Cells[1, 42] := 'Observa��o CT-e';

      // PRRENCHIMENTO DAS C�LULAS COM OS VALORES DOS CAMPOS DA TABELA
      Try
        While not QRelat.Eof do
        Begin
          Excel.Cells[Linha, 41].NumberFormat := '@';
          Excel.Cells[Linha, 1] := QRelatNM_CLI.AsString;
          Excel.Cells[Linha, 2] := QRelatCNPJ_CLI.AsString;
          Excel.Cells[Linha, 3] := QRelatNM_CLIENTE_EXP.AsString;
          Excel.Cells[Linha, 4] := QRelatNR_CNPJ_CPF_EXP.AsString;
          Excel.Cells[Linha, 5] := QRelatCIDADE_EXP.AsString;
          Excel.Cells[Linha, 6] := QRelatUF_EXP.AsString;
          Excel.Cells[Linha, 7] := QRelatNM_CLIENTE_DES.AsString;
          Excel.Cells[Linha, 8] := QRelatNR_CNPJ_CPF_DES.AsString;
          Excel.Cells[Linha, 9] := QRelatCIDADE_DES.AsString;
          Excel.Cells[Linha, 10] := QRelatUF_DES.AsString;
          Excel.Cells[Linha, 11] := QRelatNM_CLIENTE_RED.AsString;
          Excel.Cells[Linha, 12] := QRelatNR_CNPJ_CPF_RED.AsString;
          Excel.Cells[Linha, 13] := QRelatCIDADE_RED.AsString;
          Excel.Cells[Linha, 14] := QRelatUF_RED.AsString;
          Excel.Cells[Linha, 15] := QRelatDT_CTRC.AsString;
          Excel.Cells[Linha, 16] := QRelatNR_CTRC.AsString;
          Excel.Cells[Linha, 17] := QRelatNR_SERIE.AsString;
          Excel.Cells[Linha, 18] := QRelatNR_NOTA.AsString;
          Excel.Cells[Linha, 19] := QRelatVL_NF.value;
          Excel.Cells[Linha, 20] := QRelatPESO_KG.AsFloat;
          Excel.Cells[Linha, 21] := QRelatPESO_CUB.AsFloat;
          Excel.Cells[Linha, 22] := QRelatQT_VOLUME.AsFloat;
          // if cte <> QRelatNR_CTRC.AsInteger then
          // begin
          Excel.Cells[Linha, 23] := QRelatVL_FRETE.value;
          Excel.Cells[Linha, 24] := QRelatVL_PEDAGIO.value;
          Excel.Cells[Linha, 25] := QRelatVL_SEGURO.value;
          Excel.Cells[Linha, 26] := QRelatVL_GRIS.value;
          Excel.Cells[Linha, 27] := QRelatVL_OUTROS.value;
          Excel.Cells[Linha, 28] := QRelatCAT.value;
          Excel.Cells[Linha, 29] := QRelatVL_ADEME.value;
          Excel.Cells[Linha, 30] := QRelatVL_DESPACHO.value;
          Excel.Cells[Linha, 31] := QRelatVL_DESCARGA.value;
          Excel.Cells[Linha, 32] := QRelatVL_ENTREGA.value;
          Excel.Cells[Linha, 33] := QRelatVL_TDE.value;
          Excel.Cells[Linha, 34] := QRelatVL_ADICIONAL.value;

          Excel.Cells[Linha, 35] := QRelatVL_TOTAL.value;
          Excel.Cells[Linha, 36] := QRelatVL_ALIQUOTA.AsFloat;
          Excel.Cells[Linha, 37] := QRelatVL_IMPOSTO.value;
          Excel.Cells[Linha, 38] := QRelatOPERACAO.AsString;
          Excel.Cells[Linha, 39] := QRelatFL_TIPO.AsString;
          Excel.Cells[Linha, 40] := QRelatNR_FATURA.AsString;
          Excel.Cells[Linha, 41] := BuscaTroca(QRelatCTE_ID.AsString,
            'CTE', '');
          Excel.Cells[Linha, 42] := QRelatOBSCON.AsString;
          // end;
          // cte := QRelatNR_CTRC.AsInteger;
          Linha := Linha + 1;
          QRelat.Next;
        End;
        Excel.Columns.AutoFit;

        Excel.Visible := true;
      Finally
        Excel := Unassigned;
      end; // TRY

    end;
    Panel8.Visible := False;
  end;
end;

procedure TfrmIncluirFatura.btVerificarClick(Sender: TObject);
var
  i, skf: Integer;
begin
  if Trim(ledIdCliente.LookupValue) = '' then
  begin
    ShowMessage('Deve Ser Escolhido Um Cliente');
    Exit;
  end;

  dife := '';
  tt := 0;
  ts := 0;
  i := 0;

  edFatura.Clear;
  edDesc.value := 0;
  lblCtrc.caption := '';
  Panel8.Visible := true;
  Application.ProcessMessages;

  QCTRC.close;
  if copy(dtInicial.Text, 1, 1) <> ' ' then
    QCTRC.SQL[1] := ' and dt_conhecimento between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')';

  if cbOperacao.Text <> '' then
    QCTRC.SQL[2] := ' and operacao = ' + #39 + cbOperacao.LookupValue + #39;

  if Trim(ledIdCliente.LookupValue) <> '' then
    QCTRC.SQL[3] := ' and codclifor = ''' + ledIdCliente.LookupValue + '''';

  if RGTipo.ItemIndex = 0 then
    QCTRC.SQL[4] := ' '
  else if RGTipo.ItemIndex = 1 then
    QCTRC.SQL[4] := ' and tipcon not in (''C'',''R'',''P'',''D'') '
  else
    QCTRC.SQL[4] := ' and tipcon  in (''C'',''R'',''P'',''D'') ';

  if cbRegiao.Text <> '' then
  begin
    if cbRegiao.ItemIndex = 1 then
      QCTRC.SQL[5] := ' and itnlin = ''N'' '
    else if cbRegiao.ItemIndex = 2 then
      QCTRC.SQL[5] := ' and itnlin = ''NE'' '
    else if cbRegiao.ItemIndex = 3 then
      QCTRC.SQL[5] := ' and itnlin = ''CO'' '
    else if cbRegiao.ItemIndex = 4 then
      QCTRC.SQL[5] := ' and itnlin = ''SE'' '
    else if cbRegiao.ItemIndex = 5 then
      QCTRC.SQL[5] := ' and itnlin = ''S'' ';
  end;

  if cbServico.Text <> '' then
    QCTRC.SQL[6] := ' and tipo_serv = ' + #39 + cbServico.Text + #39
  else
    QCTRC.SQL[6] := '';

  // showmessage(QCTRC.SQL.text);
  QCTRC.Open;
  if QCTRCLimite.value > 0 then
    skf := QCTRCLimite.AsInteger
  else
    skf := 9999;
  mdTemp.close;
  if QCTRC.RecordCount > 0 then
  begin
    QCTRC.First;
    mdTemp.Open;
    while not QCTRC.Eof do
    begin
      tt := tt + QCTRCVL_TOTAL.value;
      mdTemp.Append;
      mdTempctrc.value := QCTRCNR_CONHECIMENTO.AsInteger;
      mdTempcliente.value := QCTRCNM_CLIENTE.value;
      mdTempexpedidor.value := QCTRCNM_CLIENTE_EXP.value;
      mdTempdestinatario.value := QCTRCNM_CLIENTE_DES.value;
      mdTempconsignatario.value := QCTRCNM_CLIENTE_CON.value;
      mdTempoperacao.value := QCTRCOPERACAO.value;
      // mdTempcod_conhecimento.value := QCTRCCOD_CONHECIMENTO.AsInteger;
      mdTempserie.value := QCTRCNR_SERIE.AsString;
      mdTempgnre.value := QCTRCNMGNRE.AsString;
      mdTempcfop.value := QCTRCNATOPE.AsInteger;
      mdTemppago.value := QCTRCPAGOU.AsInteger;
      mdTempRegra.value := Regra(QCTRCNR_CONHECIMENTO.AsInteger);
      mdTempdt_cte.value := QCTRCDT_CONHECIMENTO.value;
      mdTempvalor.value := QCTRCVL_TOTAL.value;
      if mdTempRegra.value = 1 then
        mdTempreg.value := 1
      else
      begin
        if skf >= i + 1 then
        begin
          ts := ts + QCTRCVL_TOTAL.value;
          inc(i);
        end
        else
          mdTempreg.value := 1;
      end;
      mdTemp.Post;
      QCTRC.Next;
    end;
    mdTemp.First;
  end;
  edVcto.date := RegraVcto;
  edTotal.value := ts;
  edTT.value := tt;
  // edVcto.setfocus;
  btFatura.Enabled := true;
  lblCtrc.caption := IntToStr(i) + ' Documentos';
  Panel8.Visible := False;
end;

procedure TfrmIncluirFatura.cbTClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  if cbT.Checked then
  begin
    edTotal.value := 0;
    mdTemp.First;
    while not mdTemp.Eof do
    begin
      mdTemp.edit;
      if mdTempRegra.value = 1 then
        mdTempreg.value := 1
      else
        mdTempreg.value := 0;
      edTotal.value := edTotal.value + mdTempvalor.value;
      mdTemp.Post;
      mdTemp.Next;
    end;
    edTT.value := edTotal.value;
  end
  else
  begin
    mdTemp.First;
    while not mdTemp.Eof do
    begin
      mdTemp.edit;
      mdTempreg.value := 1;
      mdTemp.Post;
      mdTemp.Next;
    end;
    edTotal.value := 0;
    edTT.value := 0;
  end
end;

procedure TfrmIncluirFatura.dbgColetaDblClick(Sender: TObject);
begin
  if mdTemp.RecordCount = 0 then
    Exit;
  mdTemp.Delete;
end;

procedure TfrmIncluirFatura.dbgFaturaCellClick(Column: TColumn);
begin
  if mdTempRegra.value = 0 then
  begin
    if (Column.Field = mdTempreg) then
    begin
      mdTemp.edit;
      if (mdTempreg.IsNull) or (mdTempreg.value = 0) then
      begin
        mdTempreg.value := 1;
        edTotal.value := edTotal.value - mdTempvalor.value;
      end
      else
      begin
        mdTempreg.value := 0;
        edTotal.value := edTotal.value + mdTempvalor.value;
      end;
      mdTemp.Post;
    end;
  end;
end;

procedure TfrmIncluirFatura.dbgFaturaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (mdTempRegra.value = 1) then
  begin
    dbgFatura.Canvas.Font.Color := clWhite;
    dbgFatura.Canvas.Brush.Color := clRed;
    dbgFatura.Canvas.FillRect(Rect);
    dbgFatura.DefaultDrawDataCell(Rect, Column.Field, State);
  end;
  if (mdTempRegra.value = 0) and
    ((mdTempcfop.value <> 6932) or (mdTempcfop.value <> 5932)) then
  begin
    dbgFatura.Canvas.Font.Color := clNavy;
    dbgFatura.Canvas.Brush.Color := clYellow;
    dbgFatura.Canvas.FillRect(Rect);
    dbgFatura.DefaultDrawDataCell(Rect, Column.Field, State);
  end;
  if (Column.Field = mdTempreg) then
  begin
    dbgFatura.Canvas.FillRect(Rect);
    iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
    if mdTempreg.value = 0 then
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 1)
    else
      iml.Draw(dbgFatura.Canvas, Rect.Left + 5, Rect.Top + 1, 0);
  end;
end;

procedure TfrmIncluirFatura.dbgFaturaTitleClick(Column: TColumn);
var
  iCount: Integer;
begin
  mdTemp.SortOnFields(Column.FieldName);
  // Muda a cor da coluna do grid
  for iCount := 0 to dbgFatura.Columns.Count - 1 do
  begin
    dbgFatura.Columns[iCount].Title.Font.Color := clWindowText;
  end;
  dbgFatura.Columns[0].Title.Font.Color := clBtnFace;
  Column.Title.Font.Color := clRed;
end;

procedure TfrmIncluirFatura.Panel1Click(Sender: TObject);
begin
  JvMemo1.Visible := true;
  JvMemo1.Clear;
  if (pos('CNH', ledCliente.Text) > 0) then
  begin
    JvMemo1.Lines.add('- S� pode Faturar Fora do Estado com Emiss�o de GNRE');
    JvMemo1.Lines.add('- GNRE deve estar paga e lan�ada no contas a pagar');
  end;
  if (pos('SKF', ledCliente.Text) > 0) then
  begin
    JvMemo1.Lines.add('- Vencimento s� pode cair em uma quarta-feira');
    JvMemo1.Lines.add('- Limitado a 60 documentos');
  end;
  if (pos('IVECO', ledCliente.Text) > 0) then
  begin
    JvMemo1.Lines.add('- S� pode Faturar Fora do Estado com Emiss�o de GNRE');
    JvMemo1.Lines.add('- GNRE deve estar paga e lan�ada no contas a pagar');
    JvMemo1.Lines.add('- Comprovante de Entrega');
  end;
  { if (pos('MERITOR',ledCliente.Text) > 0) then
    begin
    JvMemo1.Lines.Add('- S� pode Faturar Com Sa�da de Mercadoria');
    end; }
end;

procedure TfrmIncluirFatura.QClientesBeforeOpen(DataSet: TDataSet);
begin
  QClientes.Parameters[0].value := glbfilial;
end;

procedure TfrmIncluirFatura.QCTRCBeforeOpen(DataSet: TDataSet);
begin
  QCTRC.Parameters[0].value := glbfilial;
end;

procedure TfrmIncluirFatura.QOperacaoBeforeOpen(DataSet: TDataSet);
begin
  QOperacao.Parameters[0].value := glbfilial;
end;

function TfrmIncluirFatura.Regra(CTE: Integer): Integer;
var
  caminho, arq: string;
begin
  result := 0;

  // Regra de comprovante
  if (pos('IVECO', QCTRCNM_CLIENTE.value) > 0) and (result = 0) then
  begin
    if glbfilial = 1 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_1\'
    else if glbfilial = 2 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_2\'
    else if glbfilial = 9 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_9\'
    else if glbfilial = 8 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_8\'
    else if glbfilial = 4 then
      caminho := '\\192.168.236.112\Comprovantes3\Filial_4\';

    arq := caminho + mdTempctrc.AsString + '-1.jpg';

    if FileExists(arq) then
    begin
      result := 0;
      mdTempstatus.value := 'Liberado';
    end
    else
    begin
      result := 1;
      mdTempstatus.value := 'Falta Comprovante';
    end;
  end;

  if (pos('CNH', QCTRCNM_CLIENTE.value) > 0) or
    (pos('IVECO', QCTRCNM_CLIENTE.value) > 0) and (result = 0) then
  begin
    if (mdTempcfop.value = 6932) then
    begin
      if (mdTemppago.value = 0) then
      begin
        result := 1;
        mdTempstatus.value := 'Guia';
      end;
    end;
  end;

  if (pos('CNH', QCTRCNM_CLIENTE.value) > 0) or
    (pos('IVECO', QCTRCNM_CLIENTE.value) > 0) and (result = 0) then
  begin
    if (mdTempcfop.value = 5932) then
    begin
      if (mdTemppago.value = 0) then
      begin
        result := 1;
        mdTempstatus.value := 'Guia';
      end;
      if (QCTRCUFCL.value = 'MG') and (QCTRCUFRE.value = 'MG') and
        (QCTRCUFDE.value = 'MG') then
      begin
        result := 0;
        mdTempstatus.value := 'Liberado';
      end;
    end;
  end;

  // Regra Meritor - Portaria
  { if (pos('MERITOR',QCTRCNM_CLIENTE.value) > 0) and (result = 0) then
    begin
    if QCTRCPORTARIA.Value = 'N' then
    begin
    result := 1;
    mdTempstatus.Value := 'Sem Portaria';
    end
    else
    begin
    result := 0;
    mdTempstatus.Value := 'Liberado';
    end;
    end; }

  // Regra de faturamento com base nas ocorr�ncias
  QRegra.Close;
  Qregra.Parameters[0].Value := QCTRCCOD_CLIENTE.AsInteger;
  QRegra.open;

  // maior que 0 tem esta regra
  if QRegraTEM.AsInteger > 0 then
  begin
    // Verifica se for Complementar
    if (QCTRCFL_TIPO_2.AsString = 'C') or (QCTRCFL_TIPO_2.AsString = 'D') or (QCTRCFL_FATURA.AsString = 'S')  then
    begin
      result := 0;
      mdTempstatus.value := 'Status Liberado';
    end
    else
    begin
      // verifica se teve baixa
      dtmdados.IQuery1.Close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.sql.Add('select count(*) tem from cyber.tb_coleta x where x.dt_baixa is not null ');
      dtmdados.IQuery1.sql.Add('and doc = :0 and filial = :1 ');
      dtmdados.IQuery1.Parameters[0].Value :=  QCTRCNR_CONHECIMENTO.AsInteger;
      dtmdados.IQuery1.Parameters[1].Value :=  QCTRCFL_EMPRESA.AsInteger;
      dtmdados.IQuery1.Open;

      if dtmdados.IQuery1.FieldByName('tem').AsInteger > 0 then
      begin
        result := 0;
        mdTempstatus.value := 'Status Entrega Realizada';
      end
      else
      begin
        dtmdados.IQuery1.Close;
        dtmdados.IQuery1.SQL.Clear;
        dtmdados.IQuery1.sql.Add('select libera_fatura from ( ');
        dtmdados.IQuery1.sql.Add('select * from crm_tb_ocorrencias oco left join tb_ocorrencia oc on oc.id = oco.id_tipo_ocor ');
        dtmdados.IQuery1.sql.Add('where oco.nro_cte = ' + QuotedStr(QCTRCNR_CONHECIMENTO.AsString) + ' and oco.filial = '+ QuotedStr(QCTRCFL_EMPRESA.AsString) + ' order by 1 desc) ');
        dtmdados.IQuery1.sql.Add('where rownum = 1 ');
        dtmdados.IQuery1.Open;
        if (not dtmdados.IQuery1.eof) and (dtmdados.IQuery1.FieldByName('libera_fatura').AsString = 'S') then
        begin
          result := 0;
          mdTempstatus.value := 'Status Liberado';
        end
        else
        begin
          result := 1;
          mdTempstatus.value := 'Status N�o Liberado';
        end;
      end;
    end;
  end;

end;

function TfrmIncluirFatura.RegraVcto;
var
  data, dtespcial, dttemp: TDateTime;
  ds: Integer;
  diaant, diaatu: string;
begin

  data := date + QCTRCprazo_pgto.AsInteger;  //Pega data atual + valor setado PRAZO DE PAGAMENTO
  diaant := formatdatetime('dd', data);   //Extrai dia do m�s

  if QCTRCPRAZO_ESPECIAL.AsString = 'S' then
    begin
        if StrToInt(diaant) <= 15 then  //caindo na 1� quinzena data recebe dia 15
        begin
           dttemp := StrToDateTime(formatdatetime(''+inttostr(15)+'/mm/yyyy', data));
           dtespcial := dttemp;
        end
        else
        begin
           diaatu := FormatDateTime('dd',(EndOfTheMonth(data))); //sen�o data recebe ultimo dia do m�s da 2� quinzena
           dttemp := StrToDateTime(formatdatetime(''+diaatu+'/mm/yyyy', data));
           dtespcial := dttemp;
        end;
     data := dtespcial;
   end;


 //fluxo normal
  ds := DayOfWeek(data);

  if QCTRCdia_semana.AsInteger > 0 then
  begin
    if ds > QCTRCdia_semana.AsInteger then
    begin
      data := data + (7 - (ds - QCTRCdia_semana.AsInteger));
    end;

    if ds < QCTRCdia_semana.AsInteger then
    begin
      data := data + (QCTRCdia_semana.AsInteger - ds);
    end;
  end;

  result := data;
end;

end.
