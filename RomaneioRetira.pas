unit RomaneioRetira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, JvMemoryDataset, Grids,
  DBGrids, RLReport, RLRichText;

type
  TfrmRomaneioRetira = class(TForm)
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLDBText1: TRLDBText;
    dtsMapaItens: TDataSource;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLLabel13: TRLLabel;
    dtsMapaCarga: TDataSource;
    RLLabel14: TRLLabel;
    RLDBText5: TRLDBText;
    RLBand2: TRLBand;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLDBText9: TRLDBText;
    RLDBText10: TRLDBText;
    RLBand3: TRLBand;
    RLLabel15: TRLLabel;
    RLLabel16: TRLLabel;
    RLLabel17: TRLLabel;
    RLDBRichText1: TRLDBRichText;
    RLDraw1: TRLDraw;
    RLMemo1: TRLMemo;
    RLLabel18: TRLLabel;
    RLLabel19: TRLLabel;
    RLLabel20: TRLLabel;
    RLDBText2: TRLDBText;
    RLBand4: TRLBand;
    RLLabel21: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLDBText11: TRLDBText;
    Query1: TADOQuery;
    QTotal: TADOQuery;
    QTotalTOTAL: TBCDField;
    RLLabel22: TRLLabel;
    RLDBText12: TRLDBText;
    RLLabel23: TRLLabel;
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand4BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private

  public
    { Public declarations }
  end;

var
  frmRomaneioRetira: TfrmRomaneioRetira;

implementation

uses Dados, Menu, GeraRoma, funcoes;

{$R *.dfm}

procedure TfrmRomaneioRetira.RLBand3BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
var qt : integer;
begin
  Query1.close;
  Query1.sql.clear;
  Query1.SQL.add('select nr_auf quant from auftraege p ');
  Query1.SQL.add('where p.percent = :0 ');
  Query1.Parameters[0].value := frmGeraRoma.qMapaCargaNR_ROMANEIO.AsInteger;
  Query1.open;
  qt := Query1.RecordCount;
  if qt = 0 then
  begin
    Query1.close;
    Query1.sql.clear;
    Query1.SQL.add('select nr_auf quant from auftraege p ');
    Query1.SQL.add('where p.percent = :0 ');
    Query1.Parameters[0].value := frmGeraRoma.mdTempromaneioNR_ROMANEIO.AsInteger;
    Query1.open;
    qt := Query1.RecordCount;
  end;
  RLLabel15.Caption := 'Total : ' + inttostr(qt) + ' Notas Fiscais';
  Query1.close;
  QTotal.close;
  QTotal.Parameters[0].value := glbfilial;
  QTotal.Parameters[1].value := frmGeraRoma.qMapaCargaNR_ROMANEIO.AsInteger;
  QTotal.open;
  RLLabel17.caption := QTotalTOTAL.AsString;
  QTotal.close;
end;

procedure TfrmRomaneioRetira.RLBand4BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLLabel21.Caption := 'Impresso Por : ' + GLBUSER;
end;

procedure TfrmRomaneioRetira.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  if frmGeraRoma.qMapaCargaNR_ROMANEIO.AsString = '' then
    RLLabel1.Caption := 'ROMANEIO DE CARGA - N� ' + RetBran(frmGeraRoma.mdTempromaneioNR_ROMANEIO.AsString,6)
  else
    RLLabel1.Caption := 'ROMANEIO DE CARGA - N� ' + RetBran(frmGeraRoma.qMapaCargaNR_ROMANEIO.AsString,6);
end;

end.
