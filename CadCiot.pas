unit CadCiot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DB, JvExStdCtrls, JvCheckBox, ImgList, ADODB,
  ActnList, Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, System.ImageList, System.Actions,
  Vcl.ComCtrls, Vcl.OleCtrls, SHDocVw, Vcl.Samples.Spin;

type
  TfrmCadCiot = class(TForm)
    DataSource1: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QParametro: TADOQuery;
    iml: TImageList;
    pnGrid: TPanel;
    Panel3: TPanel;
    QParametroID: TBCDField;
    QParametroCODCLI: TBCDField;
    QParametroPAR_1: TStringField;
    QParametroPAR_2: TStringField;
    QParametronome: TStringField;
    QParametroPAR_3: TStringField;
    QParametroPAR_4: TStringField;
    QParametroMAPA_VOLUME: TStringField;
    QParametroPRAZO_PGTO: TFMTBCDField;
    QParametroDIA_SEMANA: TFMTBCDField;
    QParametroFATURAMENTO: TStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PageControl4: TPageControl;
    TabSheet4: TTabSheet;
    GroupBox3: TGroupBox;
    sbtnPathSalvar: TSpeedButton;
    Label29: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label42: TLabel;
    spPathSchemas: TSpeedButton;
    Label7: TLabel;
    Label30: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    edtPathLogs: TEdit;
    ckSalvar: TCheckBox;
    cbFormaEmissao: TComboBox;
    cbxAtualizarXML: TCheckBox;
    cbxExibirErroSchema: TCheckBox;
    edtFormatoAlerta: TEdit;
    cbxRetirarAcentos: TCheckBox;
    cbVersaoDF: TComboBox;
    edtPathSchemas: TEdit;
    cbbIntegradora: TComboBox;
    edtUsuarioWebService: TEdit;
    edtSenhaWebService: TEdit;
    edtHashIntegrador: TEdit;
    TabSheet7: TTabSheet;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    lTimeOut: TLabel;
    lSSLLib1: TLabel;
    cbxVisualizar: TCheckBox;
    cbUF: TComboBox;
    rgTipoAmb: TRadioGroup;
    cbxSalvarSOAP: TCheckBox;
    seTimeOut: TSpinEdit;
    cbSSLType: TComboBox;
    gbProxy: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    edtProxyHost: TEdit;
    edtProxyPorta: TEdit;
    edtProxyUser: TEdit;
    edtProxySenha: TEdit;
    gbxRetornoEnvio: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    cbxAjustarAut: TCheckBox;
    edtTentativas: TEdit;
    edtIntervalo: TEdit;
    edtAguardar: TEdit;
    TabSheet13: TTabSheet;
    sbPathCIOT: TSpeedButton;
    Label35: TLabel;
    Label47: TLabel;
    sbPathEvento: TSpeedButton;
    cbxSalvarArqs: TCheckBox;
    cbxPastaMensal: TCheckBox;
    cbxAdicionaLiteral: TCheckBox;
    cbxEmissaoPathCIOT: TCheckBox;
    cbxSalvaPathEvento: TCheckBox;
    cbxSepararPorCNPJ: TCheckBox;
    edtPathCIOT: TEdit;
    edtPathEvento: TEdit;
    cbxSepararPorModelo: TCheckBox;
    TabSheet14: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    edtSmtpHost: TEdit;
    edtSmtpPort: TEdit;
    edtSmtpUser: TEdit;
    edtSmtpPass: TEdit;
    edtEmailAssunto: TEdit;
    cbEmailSSL: TCheckBox;
    mmEmailMsg: TMemo;
    pgcBotoes: TPageControl;
    tsOperacao: TTabSheet;
    rgOperacao: TRadioGroup;
    pgRespostas: TPageControl;
    TabSheet5: TTabSheet;
    MemoResp: TMemo;
    TabSheet6: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet8: TTabSheet;
    memoLog: TMemo;
    TabSheet9: TTabSheet;
    trvwDocumento: TTreeView;
    TabSheet10: TTabSheet;
    memoRespWS: TMemo;
    Dados: TTabSheet;
    MemoDados: TMemo;
    btnGerarCIOT: TButton;
    btnCriarEnviar: TButton;
    btnEnviarCiotEmail: TButton;
  private
    quant: Integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadCiot: TfrmCadCiot;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

end.
