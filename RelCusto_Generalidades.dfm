object frmRelCusto_Generalidades: TfrmRelCusto_Generalidades
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de Generalidades '
  ClientHeight = 741
  ClientWidth = 1276
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbltransp: TLabel
    Left = 24
    Top = 160
    Width = 41
    Height = 13
    Caption = 'lbltransp'
  end
  object dbggen: TJvDBGrid
    Left = 0
    Top = 75
    Width = 1276
    Height = 592
    Align = alClient
    DataSource = dsGen
    DrawingStyle = gdsClassic
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = dbggenCellClick
    OnDrawColumnCell = dbggenDrawColumnCell
    OnKeyDown = dbggenKeyDown
    AutoAppend = False
    AutoSort = False
    OnEditChange = dbggenEditChange
    AlternateRowColor = clSilver
    AutoSizeColumnIndex = 14
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    WordWrap = True
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'escol'
        Title.Alignment = taCenter
        Title.Caption = '     '
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AUTORIZACAO'
        ReadOnly = True
        Title.Caption = 'Autor.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SERVICO'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Servi'#231'o'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 80
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATA'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 73
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FILIAL'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Filial'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 30
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'FATURA'
        Title.Alignment = taCenter
        Title.Caption = 'Fatura'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CTE_PAGAR'
        Title.Alignment = taCenter
        Title.Caption = 'CT-e Pagar'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO_COBRADO'
        Title.Alignment = taCenter
        Title.Caption = 'Custo Cobrado'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUANTIDADE'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Qtd'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_TOTAL_C'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Custo Calculado'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CONHECIMENTO'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'CT-e'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_TOTAL_R'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Receita'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM'
        Title.Alignment = taCenter
        Title.Caption = 'Margem'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CLIENTE'
        Title.Alignment = taCenter
        Title.Caption = 'Cliente'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TRANSPORTADORA'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Transportadora'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DESTINO'
        Title.Alignment = taCenter
        Title.Caption = 'Destinat'#225'rio'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRI'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'Cidade'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ESTADO'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'UF'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'liberacao'
        Title.Caption = 'Libera'#231#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'notfis'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = 'NF'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 300
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'USER_FATURA'
        Visible = False
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DT_FATURA'
        Visible = False
      end>
  end
  object Panel2: TPanel
    Left = 309
    Top = 160
    Width = 674
    Height = 257
    Color = 886527
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object Label8: TLabel
      Left = 519
      Top = 19
      Width = 41
      Height = 16
      Alignment = taRightJustify
      BiDiMode = bdLeftToRight
      Caption = 'Valor :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object Label12: TLabel
      Left = 514
      Top = 51
      Width = 49
      Height = 16
      BiDiMode = bdLeftToRight
      Caption = 'Fatura :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object Label13: TLabel
      Left = 17
      Top = 124
      Width = 83
      Height = 16
      Caption = 'Observa'#231#227'o :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 497
      Top = 100
      Width = 66
      Height = 16
      Caption = 'Desconto :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 153
      Top = 51
      Width = 36
      Height = 16
      Caption = 'Vcto :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 295
      Top = 51
      Width = 114
      Height = 16
      Caption = 'Imposto  P. F'#237'sica:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel
      Left = 17
      Top = 100
      Width = 118
      Height = 16
      Caption = 'Tipo de Desconto :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 498
      Top = 126
      Width = 65
      Height = 16
      Caption = 'Processo :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 17
      Top = 49
      Width = 37
      Height = 16
      Caption = 'Data :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 351
      Top = 17
      Width = 66
      Height = 16
      Caption = 'Imp. NFS :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 102
      Top = 79
      Width = 87
      Height = 16
      Caption = 'Vcto Manual :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnSalvar: TBitBtn
      Left = 164
      Top = 223
      Width = 70
      Height = 24
      Caption = 'Salvar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000BA6A368FB969
        35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
        32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
        ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
        F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
        B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
        88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
        B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
        C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
        B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
        88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
        BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
        F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
        BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
        76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
        C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
        77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
        C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
        77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
        C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
        65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
        C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
        E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
        CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
        EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
        D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
        F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
        D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
        F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
        D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
        F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
        3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
        39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
      TabOrder = 12
      OnClick = btnSalvarClick
    end
    object btnCancelar: TBitBtn
      Left = 52
      Top = 223
      Width = 70
      Height = 24
      Caption = 'Cancelar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
        EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
        F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
        F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
        F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
        F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
        FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
        FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
        FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
        FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
        FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
        FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
        FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
        FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
        FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
        FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
        FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
        FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
        FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
        FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
        FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
        FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 11
      OnClick = btnCancelarClick
    end
    object edVlrPrev: TJvCalcEdit
      Left = 569
      Top = 14
      Width = 79
      Height = 21
      Color = clSilver
      DisplayFormat = ',0.00'
      ReadOnly = True
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
    object btnImpr: TBitBtn
      Left = 270
      Top = 223
      Width = 70
      Height = 25
      Caption = 'Imprimir'
      Enabled = False
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 13
      OnClick = btnImprClick
    end
    object btnSair: TBitBtn
      Left = 380
      Top = 223
      Width = 70
      Height = 25
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
        9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
        71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
        9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
        FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
        A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
        FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
        AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
        FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
        B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
        FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
        B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
        3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
        BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
        66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
        BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
        47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
        C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
        FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
        C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
        FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
        CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
        FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
        D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
        FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
        D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
        FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
        D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
      ParentFont = False
      TabOrder = 14
      OnClick = btnSairClick
    end
    object EdTransp: TEdit
      Left = 15
      Top = 14
      Width = 318
      Height = 21
      Color = clSilver
      ReadOnly = True
      TabOrder = 0
    end
    object edFatura: TEdit
      Left = 569
      Top = 47
      Width = 79
      Height = 21
      TabOrder = 6
    end
    object Memo1: TMemo
      Left = 15
      Top = 146
      Width = 562
      Height = 67
      Lines.Strings = (
        '')
      TabOrder = 10
    end
    object edDesconto: TJvCalcEdit
      Left = 569
      Top = 99
      Width = 79
      Height = 21
      Color = clWhite
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 8
      DecimalPlacesAlwaysShown = False
    end
    object edVcto: TJvDateEdit
      Left = 195
      Top = 47
      Width = 79
      Height = 21
      Color = clSilver
      ReadOnly = True
      ShowButton = False
      ShowNullDate = False
      TabOrder = 4
    end
    object edImp: TJvCalcEdit
      Left = 416
      Top = 47
      Width = 91
      Height = 21
      Color = clSilver
      DisplayFormat = ',0.00'
      ReadOnly = True
      ShowButton = False
      TabOrder = 5
      DecimalPlacesAlwaysShown = False
    end
    object cbTipo: TJvComboBox
      Left = 141
      Top = 99
      Width = 252
      Height = 21
      TabOrder = 7
      Text = ''
      Items.Strings = (
        ''
        '6-Outro'
        '7-Taxa indevida'
        '8-Valor do componente divergente'
        '9-Custo calculado a maior do que o cobrado')
    end
    object edProcesso: TJvCalcEdit
      Left = 569
      Top = 125
      Width = 79
      Height = 21
      Color = clWhite
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 9
      DecimalPlacesAlwaysShown = False
    end
    object edData: TJvDateEdit
      Left = 60
      Top = 47
      Width = 79
      Height = 21
      Color = clSilver
      ShowButton = False
      ShowNullDate = False
      TabOrder = 3
      OnExit = edDataExit
    end
    object edNFs: TJvCalcEdit
      Left = 417
      Top = 14
      Width = 79
      Height = 21
      Color = clWhite
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
      OnExit = edNFsExit
    end
    object edVctoM: TJvDateEdit
      Left = 195
      Top = 74
      Width = 79
      Height = 21
      Color = clWhite
      ShowButton = False
      ShowNullDate = False
      TabOrder = 15
      OnExit = edVctoMExit
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 1276
    Height = 75
    Align = alTop
    TabOrder = 6
    object Label1: TLabel
      Left = 41
      Top = 4
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 138
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 242
      Top = 4
      Width = 66
      Height = 13
      Caption = 'Transportador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 963
      Top = 4
      Width = 20
      Height = 13
      Caption = 'Filial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label23: TLabel
      Left = 12
      Top = 47
      Width = 727
      Height = 13
      Caption = 
        'F2 - Calcular o Custo                 F8 - Detalhes da Generalid' +
        'ades                    F11 - Tabela de Custo Utilizada         ' +
        '           F12 - Tabela de Receita Utilizada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label20: TLabel
      Left = 698
      Top = 4
      Width = 30
      Height = 13
      Caption = 'Fatura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 782
      Top = 4
      Width = 56
      Height = 13
      Caption = 'Autoriza'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtinicial: TJvDateEdit
      Left = 41
      Top = 20
      Width = 87
      Height = 21
      ShowNullDate = False
      TabOrder = 0
    end
    object dtfinal: TJvDateEdit
      Left = 138
      Top = 20
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object ledidcliente: TJvDBLookupEdit
      Left = 242
      Top = 20
      Width = 155
      Height = 21
      LookupDisplay = 'NR_CNPJ_CPF'
      LookupField = 'CODIGO'
      LookupSource = dtsFornecedor
      CharCase = ecUpperCase
      TabOrder = 2
      Text = ''
      OnExit = ledidclienteExit
    end
    object ledcliente: TJvDBLookupEdit
      Left = 416
      Top = 20
      Width = 256
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'NOME'
      LookupField = 'CODIGO'
      LookupSource = dtsFornecedor
      CharCase = ecUpperCase
      TabOrder = 3
      Text = ''
      OnExit = ledclienteExit
    end
    object cbTodos: TCheckBox
      Left = 20
      Top = 24
      Width = 15
      Height = 17
      TabOrder = 4
      OnClick = cbTodosClick
    end
    object btrelatorio: TBitBtn
      Left = 1193
      Top = 12
      Width = 75
      Height = 38
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 5
      OnClick = btrelatorioClick
    end
    object cbfilial: TJvComboBox
      Left = 963
      Top = 20
      Width = 224
      Height = 21
      Style = csDropDownList
      TabOrder = 6
      Text = ''
    end
    object edFat: TJvMaskEdit
      Left = 698
      Top = 20
      Width = 57
      Height = 21
      TabOrder = 7
      Text = ''
    end
    object RadioGroup1: TRadioGroup
      Left = 867
      Top = 5
      Width = 73
      Height = 55
      Caption = 'Condi'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Aberto'
        'Todos')
      ParentFont = False
      TabOrder = 9
    end
    object edId: TJvCalcEdit
      Left = 782
      Top = 20
      Width = 56
      Height = 21
      ShowButton = False
      TabOrder = 8
      DecimalPlacesAlwaysShown = False
    end
  end
  object Panel5: TPanel
    Left = 444
    Top = 192
    Width = 496
    Height = 73
    Color = 8454143
    ParentBackground = False
    TabOrder = 2
    Visible = False
    object Label16: TLabel
      Left = 23
      Top = 14
      Width = 49
      Height = 16
      Caption = 'Fatura :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnSalvarPrevia: TBitBtn
      Left = 237
      Top = 10
      Width = 70
      Height = 24
      Caption = 'Salvar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000BA6A368FB969
        35B5B86935EEB76835FFB56835FFB46734FFB26634FFB06533FFAE6433FFAC63
        32FFAA6232FFA96132FFA86031FFA76031FEA66031F1A86131C4BA6A35DEEBC6
        ADFFEAC5ADFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
        F8FFFEFBF8FFFEFBF8FFFEFBF8FFC89A7CFFC79879FFA76031EDBA6B37FEEDCA
        B3FFE0A27AFFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
        88FF62C088FF62C088FFFDF9F6FFCA8D65FFC99B7CFFA76031FEBB6C38FFEECC
        B6FFE1A27AFFFEFAF7FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDCC2FFBFDC
        C2FFBFDCC2FFBFDCC2FFFDF9F6FFCD9068FFCC9E81FFA86132FFBB6B38FFEFCE
        B8FFE1A279FFFEFAF7FF62C088FF62C088FF62C088FF62C088FF62C088FF62C0
        88FF62C088FF62C088FFFDF9F6FFCF936AFFCEA384FFAA6132FFBA6A36FFEFD0
        BBFFE2A27AFFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFBF8FFFEFB
        F8FFFEFBF8FFFEFBF8FFFEFBF8FFD3966DFFD2A78AFFAB6232FFBB6A36FFF0D2
        BEFFE2A37AFFE2A37AFFE1A37AFFE2A37BFFE1A37BFFE0A178FFDE9F77FFDD9F
        76FFDC9D74FFD99B72FFD89971FFD69970FFD5AB8EFFAD6333FFBB6A36FFF2D5
        C2FFE3A37AFFE3A37AFFE2A37BFFE2A37BFFE2A47BFFE1A279FFE0A178FFDEA0
        77FFDE9E75FFDC9D74FFDA9B73FFD99B73FFDAB095FFAF6433FFBB6A36FFF2D8
        C5FFE3A47BFFE3A37AFFE3A47AFFE2A47BFFE2A37BFFE1A37BFFE1A279FFDFA0
        77FFDE9F76FFDD9E74FFDB9C72FFDC9D74FFDDB59AFFB16534FFBB6B36FFF4D9
        C7FFE6A67DFFC88C64FFC98D65FFC98E67FFCB926CFFCB926DFFCA9069FFC88C
        65FFC88C64FFC88C64FFC88C64FFDA9C74FFE1BA9FFFB36634FFBB6B36FEF4DC
        C9FFE7A77DFFF9ECE1FFF9ECE1FFF9EDE3FFFCF4EEFFFDFAF7FFFDF7F3FFFAED
        E5FFF7E7DBFFF7E5D9FFF6E5D8FFDEA077FFE4BEA4FFB46734FFBC6B36FAF5DD
        CCFFE7A87EFFFAF0E8FFFAF0E8FFC98D66FFFAF0E9FFFDF8F3FFFEFAF8FFFCF4
        EFFFF9E9DFFFF7E7DBFFF7E5D9FFE0A278FFE7C2A9FFB66835FFBC6B36F0F6DF
        D0FFE8A87EFFFCF6F1FFFCF6F1FFC88C64FFFAF1E9FFFBF4EEFFFDFAF7FFFDF9
        F6FFFAF0E8FFF8E8DDFFF7E6DBFFE1A37AFFEFD5C3FFB76935FEBC6B36D8F6DF
        D1FFE9AA80FFFEFAF6FFFDFAF6FFC88C64FFFBF3EEFFFBF1EAFFFCF6F2FFFEFB
        F8FFFCF6F1FFF9ECE2FFF8E7DBFFEED0BAFFECD0BDFFBB703EF8BC6B369BF6E0
        D1FFF7E0D1FFFEFBF8FFFEFBF7FFFDF9F6FFFCF5F0FFFAF0EAFFFBF2EDFFFDF9
        F6FFFDFAF7FFFBF1EBFFF8E9DFFEECD0BDFBC9895EECB5693563BC6B3671BC6B
        3690BC6B36CCBC6B36EEBC6B36FABB6B36FEBB6B36FFBB6A36FFBB6A36FFBC6C
        39FFBD6E3BFFBB6D3AFFBB6B38EFBB703ECBB6693554FFFFFF00}
      TabOrder = 0
      OnClick = btnSalvarPreviaClick
    end
    object btnCancelaPrevia: TBitBtn
      Left = 161
      Top = 10
      Width = 70
      Height = 24
      Caption = 'Cancelar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000F1EC0000F1FF0000F1FF0000F1FF0000EFFF0000
        EFFF0000EDFF0000EDEDFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000F5EC1A20F5FF3C4CF9FF3A49F8FF3847F8FF3545F8FF3443
        F7FF3242F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000000F7EC1D23F9FF4453FAFF2429F9FF1212F7FF0F0FF6FF0C0CF5FF0909
        F5FF161BF5FF3343F7FF141BF1FF0000EDE8FF00FF00FF00FF00FF00FF000000
        F9EC1F25FAFF4A58FBFF4247FBFFC9C9FDFF3B3BF9FF1313F7FF1010F6FF3333
        F7FFC5C5FDFF3035F7FF3444F7FF141BF2FF0000EDE8FF00FF00FF00FF000000
        FBFF4F5DFDFF3237FBFFCBCBFEFFF2F2FFFFEBEBFEFF3B3BF9FF3939F8FFEAEA
        FEFFF1F1FEFFC5C5FDFF181DF6FF3343F7FF0000EFFFFF00FF00FF00FF000000
        FDFF525FFDFF2828FCFF4747FCFFECECFFFFF2F2FFFFECECFFFFECECFEFFF1F1
        FFFFEAEAFEFF3434F7FF0B0BF5FF3545F8FF0000EFFFFF00FF00FF00FF000000
        FDFF5562FEFF2C2CFDFF2929FCFF4848FCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FEFF3A3AF9FF1212F7FF0F0FF6FF3848F8FF0000F1FFFF00FF00FF00FF000000
        FDFF5764FEFF3030FDFF2D2DFDFF4B4BFCFFEDEDFFFFF2F2FFFFF2F2FFFFECEC
        FFFF3D3DF9FF1616F8FF1313F7FF3C4BF8FF0000F1FFFF00FF00FF00FF000000
        FFFF5A67FEFF3333FEFF5050FDFFEDEDFFFFF3F3FFFFEDEDFFFFEDEDFFFFF2F2
        FFFFECECFEFF3E3EFAFF1717F8FF3F4EF9FF0000F1FFFF00FF00FF00FF000000
        FFFF5B68FFFF4347FEFFCFCFFFFFF3F3FFFFEDEDFFFF4C4CFCFF4A4AFCFFECEC
        FFFFF2F2FFFFCACAFEFF2A2FFAFF4251FAFF0000F3FFFF00FF00FF00FF000000
        FFEB262BFFFF5D6AFFFF585BFFFFCFCFFFFF5252FEFF2F2FFDFF2C2CFDFF4B4B
        FCFFCCCCFEFF484CFBFF4957FBFF1D23F9FF0000F5EBFF00FF00FF00FF00FF00
        FF000000FFEB262BFFFF5D6AFFFF4347FFFF3434FEFF3232FEFF3030FDFF2D2D
        FDFF383CFCFF4F5DFCFF1F25FAFF0000F7EBFF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000FFEB262BFFFF5C69FFFF5B68FFFF5A67FEFF5865FEFF5663
        FEFF5461FEFF2227FCFF0000FBF2FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000FFEC0000FFFF0000FFFF0000FFFF0000FDFF0000
        FDFF0000FDFF0000FDECFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 1
      OnClick = btnCancelaPreviaClick
    end
    object btnImprimirPrevia: TBitBtn
      Left = 313
      Top = 10
      Width = 70
      Height = 25
      Caption = 'Imprimir'
      Enabled = False
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 2
      OnClick = btnImprimirPreviaClick
    end
    object btnSairPrevia: TBitBtn
      Left = 389
      Top = 10
      Width = 70
      Height = 25
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
        9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
        71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
        9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
        FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
        A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
        FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
        AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
        FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
        B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
        FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
        B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
        3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
        BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
        66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
        BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
        47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
        C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
        FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
        C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
        FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
        CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
        FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
        D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
        FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
        D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
        FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
        D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
      ParentFont = False
      TabOrder = 3
      OnClick = btnSairPreviaClick
    end
    object edPrevia: TEdit
      Left = 78
      Top = 11
      Width = 77
      Height = 21
      TabOrder = 4
    end
    object btnCarregar: TBitBtn
      Left = 80
      Top = 38
      Width = 75
      Height = 25
      Caption = 'Carregar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 5
      OnClick = btnCarregarClick
    end
  end
  object Panel4: TPanel
    Left = 508
    Top = 93
    Width = 175
    Height = 346
    TabOrder = 3
    Visible = False
    object Label24: TLabel
      Left = 5
      Top = 5
      Width = 47
      Height = 13
      Caption = 'Caminho :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DirectoryListBox1: TDirectoryListBox
      Left = 5
      Top = 44
      Width = 161
      Height = 261
      TabOrder = 0
    end
    object DriveComboBox1: TDriveComboBox
      Left = 5
      Top = 24
      Width = 161
      Height = 19
      DirList = DirectoryListBox1
      Enabled = False
      TabOrder = 1
    end
    object btnGerarExcel: TBitBtn
      Left = 52
      Top = 311
      Width = 75
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
        346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
        2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
        33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
        EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
        38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
        3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
        40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
        73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
        33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
        64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
        33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
        33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
        3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
        D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
        49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
        CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
        53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
        C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
        BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
        62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
        56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
        45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
        4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
        BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
        4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
        B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
        49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
        ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
        4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
        4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 2
      OnClick = btnGerarExcelClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 667
    Width = 1276
    Height = 41
    Align = alBottom
    TabOrder = 4
    object Label25: TLabel
      Left = 116
      Top = 13
      Width = 33
      Height = 13
      Caption = 'Campo'
      Transparent = True
    end
    object Label36: TLabel
      Left = 305
      Top = 13
      Width = 60
      Height = 13
      Caption = 'Procurar por'
    end
    object Label26: TLabel
      Left = 566
      Top = 13
      Width = 46
      Height = 13
      Caption = 'Operador'
    end
    object cbCampo: TComboBox
      Left = 155
      Top = 8
      Width = 143
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = cbCampoChange
      Items.Strings = (
        'Servi'#231'o'
        'Data'
        'Destinat'#225'rio'
        'CT-e Pagar'
        'NF'
        'CT-e'
        'Filial'
        '')
    end
    object edLocalizar: TEdit
      Left = 371
      Top = 8
      Width = 172
      Height = 21
      TabOrder = 1
    end
    object cbOperador: TComboBox
      Left = 621
      Top = 10
      Width = 120
      Height = 21
      Style = csDropDownList
      TabOrder = 2
      OnExit = cbOperadorExit
      Items.Strings = (
        'igual a'
        'contendo')
    end
    object btProcurar: TBitBtn
      Left = 761
      Top = 6
      Width = 64
      Height = 23
      Hint = 'Localizar'
      Caption = 'Localizar'
      Enabled = False
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00BDD6DE00A5B5B500DEEFEF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF002952AD00316BDE00316BEF00ADBDBD00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00B5D6DE00316BE7003973F70010188400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF002152A500316BEF0010187B0094A5AD00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0052525200847352008C63
        2900424A4A0073C6F70094A5AD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00C6D6D600A5948400FFDE8C00D6A5
        3100B584310021100800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00ADBDBD00F7E7D600FFFFCE00F7C6
        3100D6A531007B5A2100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00ADBDBD00F7CEA500FF00FF00FFFF
        AD00FFDE520084735200FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00CEDEDE00847B7300F7DEC600F7E7
        B500A594730052525200FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C0084FFFF00EFBD940073F7FF00BD948C0084FFFF00CEDEDE00ADBDBD00ADBD
        BD00C6D6D600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C0084FFFF0084FFFF0084FFFF00BD948C009CFFFF0084FFFF0084FFFF0084FF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C009CFFFF00EFBD940084FFFF00BD948C009CFFFF00EFBD9400EFBD940084FF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BDFFFF00EFBD94009CFFFF00BD948C00BDFFFF00EFBD9400EFBD9400BDFF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BDFFFF00BDFFFF00BDFFFF00BD948C00BDFFFF00BDFFFF00BDFFFF00BDFF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD94
        8C00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD94
        8C00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btProcurarClick
    end
    object btRetirarFiltro: TBitBtn
      Left = 840
      Top = 8
      Width = 81
      Height = 21
      Hint = 'Retirar Filtro'
      Caption = 'Limpa Pesq.'
      Enabled = False
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFBDD6DEA5B5B5DEEFEFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF2952AD316BDE0000B50000B5FF00FF0000B5
        0000B5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5D6DE316B
        E70000B50000B5FF00FFFF00FF0000B50000B50000B5FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF2152A50000B50000B594A5ADFF00FFFF00FF0000B5
        0000B50000B50000B5FF00FFFF00FF5252528473528C6329424A4A0000B50000
        B5FF00FFFF00FFFF00FFFF00FFFF00FF0000D60000BD0000B50000B5C6D6D6A5
        9484FFDE8CD6A5310000B50000B5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF0000B50000B50000B5F7E7D60000B50000B50000B57B5A21FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000B50000C600
        00C60000CE0000B5FFDE52847352FF00FFFF00FFFF00FFFF00FFBD948CBD948C
        BD948CBD948CBD948CBD948C0000C60000C60000DEF7E7B5A59473525252FF00
        FFFF00FFFF00FFFF00FFBD948CBD948C84FFFFEFBD9473F7FF0000B50000D600
        00CE0000DE0000EFC6D6D6FF00FFFF00FFFF00FFFF00FFFF00FFBD948CBD948C
        84FFFF84FFFF0000E70000DE0000D684FFFF84FFFF0000E70000EFFF00FFFF00
        FFFF00FFFF00FFFF00FFBD948CBD948C9CFFFF0000FF0000DE0000EF9CFFFFEF
        BD94EFBD9484FFFF0000FF0000F7FF00FFFF00FFFF00FFFF00FFBD948CBD948C
        0000F70000F70000FFBD948CBDFFFFEFBD94EFBD94BDFFFFBD948C0000F70000
        F7FF00FFFF00FFFF00FFBD948C0000F70000F70000F7BDFFFFBD948CBDFFFFBD
        FFFFBDFFFFBDFFFFBD948CFF00FFFF00FF0000F7FF00FFFF00FF0000F70000F7
        0000F7BD948CBD948CBD948CBD948CBD948CBD948CBD948CBD948CFF00FFFF00
        FFFF00FFFF00FFFF00FF0000F70000F7BD948CBD948CBD948CBD948CBD948CBD
        948CBD948CBD948CBD948CFF00FFFF00FFFF00FFFF00FFFF00FF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object cbFieldName: TComboBox
      Left = 386
      Top = 10
      Width = 145
      Height = 21
      TabOrder = 5
      Visible = False
      Items.Strings = (
        'servico'
        'dt_conhecimento'
        'destinatario'
        'nr_cte_pagar'
        'notfis'
        'nr_conhecimento'
        'filial'
        ''
        '')
    end
    object btnPrevia: TBitBtn
      Left = 19
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Pr'#233'via'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000F6F2EFC6AA94
        B8967BB08A6CA87E5D3A88BD3486BF3284BE3182BD2F80BC2D7FBC2D7DBC2A7B
        BA2C7DBE2B7CBF5696CCD3BEACB8906DD6BAA3DFC6B3E7D4C34B99CADDECF6BD
        EEF9ACEAF8ABEAF8ABEAF8ABEAF8ADEAF8D4F3FBA4C8E46DA6D3C8AC93C7A485
        FFFFFFFFFFFFFFFFFFAED4EA8EC2E197E8F961DCF65BDBF53288C25BDBF56ADE
        F6B1E7F63F8EC7DCEAF5DECCBBA16E40B48559D9A57BD89E6FD49B6C619CB9AB
        DBEF74E0F758DAF558DAF55DDBF590E6F892C1E19AC5E2FFFFFFF8F4F0C29E7C
        D5AE8CFDF0E5F7C7A2F7CFADC7C8BD7BBDDFA2EAF961DCF63187C277E1F7B6DE
        F04F95BCF5F9FCFFFFFFFFFFFFF2E9E1B78656FEFEFDFADEC2FADCBFF8DBC08B
        BCCDACD9EC82E3F83388C2ACEDFA449ECF949589FFFFFFFFFFFFFFFFFFF6EFE9
        B98652FEFCF9F9DCBFF8DBBFF8DCC0DED4C65DB2D5B4EBF88EE6F8B5DDEE7DBB
        D8B78959FFFFFFFFFFFFFFFFFFFCFBF9B9854BFEFBF7F9DCC1F8DCBFF8DCBFF8
        DBC0A8CACDA4D7EBDCF4FB5CB2D6D8E9EFB8844AFFFFFEFFFFFFFFFFFFFFFFFF
        BF8B4FFCF6F0F9DFC7F9DCBDFADCBFFADBC1F1DBC473C0D9ABDBEDA0C9D3FFFC
        FAC18D53FBF8F4FFFFFFFFFFFFFFFFFFCD9E68F5E7D8FAE5D2F9DABCF9DBBCFA
        DBBFFADDC1C6D3CB7EC4D8EDDECAFFFDFBC89457FBF7F3FFFFFFFFFFFFFFFFFF
        D8B180F0D9C1FBEDE1F9DAC0F9DCC2F9DEC5FAE0C7FAE2CAFAE2CDFAE5D0FFFE
        FDCB8F5ACD9A5BF1E2D0FFFFFFFFFFFFE1BE92EDD0B2FFF6F0FAE1CAFBE3CCFB
        E3D0FBE6D3FBE9D5FCE9D8FCEADBFFFFFDD29D71EED9C1D5A466FFFFFFFFFFFF
        E7C89FEBCAA5FFFDFBFDE9D5FDEBD8FDEADBFDEDDFFDF0E2FDF1E4FCF0E4FFFF
        FFE0A070FFFBF9DFB887FFFFFFFFFFFFECD0AAEBC69AFFFFFFFCEFE2FDF0E7FD
        F1EBFDF5EEFDF8F1FDFAF7FFFCFAFFFFFFFEFBF7F4DAC0DDAB69FFFFFFFFFFFF
        F1DABAEAC08CFFFFFFFFFFFFFFFFFFFFFFFFFDF9F4FBF3EAF8EBD9F8E6D3F5DF
        C6E9CBA6E0AE68F3DFC4FFFFFFFFFFFFF9EDDDE8BF83EABC81E8B777E6B26DE4
        B068E4B168E5B571E7BC7DE8BD7FEAC28AECC896F6E7D0FEFDFC}
      TabOrder = 6
      OnClick = btnPreviaClick
    end
    object btnManifesto: TBitBtn
      Left = 1022
      Top = 6
      Width = 77
      Height = 23
      Hint = 'Filtrar'
      Caption = 'Manifesto'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00BDD6DE00A5B5B500DEEFEF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF002952AD00316BDE00316BEF00ADBDBD00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00B5D6DE00316BE7003973F70010188400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF002152A500316BEF0010187B0094A5AD00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0052525200847352008C63
        2900424A4A0073C6F70094A5AD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00C6D6D600A5948400FFDE8C00D6A5
        3100B584310021100800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00ADBDBD00F7E7D600FFFFCE00F7C6
        3100D6A531007B5A2100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00ADBDBD00F7CEA500FF00FF00FFFF
        AD00FFDE520084735200FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00CEDEDE00847B7300F7DEC600F7E7
        B500A594730052525200FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C0084FFFF00EFBD940073F7FF00BD948C0084FFFF00CEDEDE00ADBDBD00ADBD
        BD00C6D6D600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C0084FFFF0084FFFF0084FFFF00BD948C009CFFFF0084FFFF0084FFFF0084FF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C009CFFFF00EFBD940084FFFF00BD948C009CFFFF00EFBD9400EFBD940084FF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BDFFFF00EFBD94009CFFFF00BD948C00BDFFFF00EFBD9400EFBD9400BDFF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BDFFFF00BDFFFF00BDFFFF00BD948C00BDFFFF00BDFFFF00BDFFFF00BDFF
        FF00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD94
        8C00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BD948C00BD94
        8C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD948C00BD94
        8C00BD948C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      Visible = False
    end
    object btnLimpaFiltro: TBitBtn
      Left = 935
      Top = 8
      Width = 81
      Height = 21
      Hint = 'Retirar Filtro'
      Caption = 'Limpa Filtro'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFBDD6DEA5B5B5DEEFEFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF2952AD316BDE0000B50000B5FF00FF0000B5
        0000B5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5D6DE316B
        E70000B50000B5FF00FFFF00FF0000B50000B50000B5FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF2152A50000B50000B594A5ADFF00FFFF00FF0000B5
        0000B50000B50000B5FF00FFFF00FF5252528473528C6329424A4A0000B50000
        B5FF00FFFF00FFFF00FFFF00FFFF00FF0000D60000BD0000B50000B5C6D6D6A5
        9484FFDE8CD6A5310000B50000B5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF0000B50000B50000B5F7E7D60000B50000B50000B57B5A21FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000B50000C600
        00C60000CE0000B5FFDE52847352FF00FFFF00FFFF00FFFF00FFBD948CBD948C
        BD948CBD948CBD948CBD948C0000C60000C60000DEF7E7B5A59473525252FF00
        FFFF00FFFF00FFFF00FFBD948CBD948C84FFFFEFBD9473F7FF0000B50000D600
        00CE0000DE0000EFC6D6D6FF00FFFF00FFFF00FFFF00FFFF00FFBD948CBD948C
        84FFFF84FFFF0000E70000DE0000D684FFFF84FFFF0000E70000EFFF00FFFF00
        FFFF00FFFF00FFFF00FFBD948CBD948C9CFFFF0000FF0000DE0000EF9CFFFFEF
        BD94EFBD9484FFFF0000FF0000F7FF00FFFF00FFFF00FFFF00FFBD948CBD948C
        0000F70000F70000FFBD948CBDFFFFEFBD94EFBD94BDFFFFBD948C0000F70000
        F7FF00FFFF00FFFF00FFBD948C0000F70000F70000F7BDFFFFBD948CBDFFFFBD
        FFFFBDFFFFBDFFFFBD948CFF00FFFF00FF0000F7FF00FFFF00FF0000F70000F7
        0000F7BD948CBD948CBD948CBD948CBD948CBD948CBD948CBD948CFF00FFFF00
        FFFF00FFFF00FFFF00FF0000F70000F7BD948CBD948CBD948CBD948CBD948CBD
        948CBD948CBD948CBD948CFF00FFFF00FFFF00FFFF00FFFF00FF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = btnLimpaFiltroClick
    end
    object btnOco: TBitBtn
      Left = 1105
      Top = 6
      Width = 81
      Height = 21
      Hint = 'Oco'
      Caption = 'Ocorr'#234'ncia'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000000000000000000000000000000000000F0001110001
        0901010301000100000100000100010000000000000000000700000800020A01
        030A03030C01021203040F00001601000C0100060000242E32406F892B58744D
        667464838B546F7C252F390000000E00000A00001500021303040E0001080300
        0B00004A7F8D387189303F55ADA8B8F8F1F7F7F0F1B3B2B9616B7B3F4F620003
        0E0900001600011602011500000000005E91A30848678491A1FDFAFCE7E8EDE3
        E7E6E1E8E1EDEBECFCF8FB9B9FAD344460090F220A00001B07010400003B636F
        1F6483A9A7BAF9F4F8EAE5E1E9E1E7E7E5E6E3E4E5E8E5E5E5E6E4F8F4F4B5B5
        C039486F00000C1201000D0B1365ABC8597495E4E5EDD8DCE9D4E0DBF1F4FBF0
        EAF0F1EBF1EEECF7E3E4EFF2F3F2FFFFFB7B839C2F445A0600002F3B44000F28
        E0E0F63E4D6800093F5F7394172B5761708CA6B2CA263B5E6B7F9C3C58855F78
        A4F5F2FA446B8118141B000000272A3954546F708CA4042450C3DFFB00214E65
        86A60E316C8DA9CAFFFFFF4E6C96355AA6869BAD6997A00600000C0000112434
        2A4A7C5D7CA401275929456A1F3F71A7C1D6022260ACC1DAFAFAF05477AC2451
        AB5B8DB60E2C3818000004000042707A235F8E4E7098002655799EC0000C3DBF
        E3EB0E38676889A9D9E9FB2B51905A7BB7315279000B162002010D0C0A5D9BAC
        3B6F93518BA4457D994675906FA6BFAFE6EEB1E3F66390A4446D83759BB1D7F6
        FF6170871B203B1300000C030614132E6081A785E1F982DAEC8EDDE895DBE599
        DBE7A8DFE7BCE9EDCDEEEED4ECEBE5EDEE8DA1B25B7F920C00000A000019272B
        000D2384BEDC85D8F574D1F286D3F098DDEDB5EBF2C6EAE8DDE7E3E8E5E9EEE1
        EB6A969C141F27130000120000050000535656161C1E15272F4D6C7D577D912F
        3C4F141122CDB7C5E4D9E0E0D9E19E919C7A858D0500001700001403031D0406
        0B00004442406C69685C5C5F859599635F65A59296CCA5ADFCE4EAFCECF0FFDC
        E498595E5C26244F211E0900000F000010000002000000000000000000000004
        000007000077393DE3A5AAE2A8AC9749504E16172000001C0000}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      Visible = False
    end
    object btnBO_2: TBitBtn
      Left = 1193
      Top = 6
      Width = 81
      Height = 21
      Hint = 'Oco'
      Caption = 'Sem Fatura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000000000000000000000000000000000000F0001110001
        0901010301000100000100000100010000000000000000000700000800020A01
        030A03030C01021203040F00001601000C0100060000242E32406F892B58744D
        667464838B546F7C252F390000000E00000A00001500021303040E0001080300
        0B00004A7F8D387189303F55ADA8B8F8F1F7F7F0F1B3B2B9616B7B3F4F620003
        0E0900001600011602011500000000005E91A30848678491A1FDFAFCE7E8EDE3
        E7E6E1E8E1EDEBECFCF8FB9B9FAD344460090F220A00001B07010400003B636F
        1F6483A9A7BAF9F4F8EAE5E1E9E1E7E7E5E6E3E4E5E8E5E5E5E6E4F8F4F4B5B5
        C039486F00000C1201000D0B1365ABC8597495E4E5EDD8DCE9D4E0DBF1F4FBF0
        EAF0F1EBF1EEECF7E3E4EFF2F3F2FFFFFB7B839C2F445A0600002F3B44000F28
        E0E0F63E4D6800093F5F7394172B5761708CA6B2CA263B5E6B7F9C3C58855F78
        A4F5F2FA446B8118141B000000272A3954546F708CA4042450C3DFFB00214E65
        86A60E316C8DA9CAFFFFFF4E6C96355AA6869BAD6997A00600000C0000112434
        2A4A7C5D7CA401275929456A1F3F71A7C1D6022260ACC1DAFAFAF05477AC2451
        AB5B8DB60E2C3818000004000042707A235F8E4E7098002655799EC0000C3DBF
        E3EB0E38676889A9D9E9FB2B51905A7BB7315279000B162002010D0C0A5D9BAC
        3B6F93518BA4457D994675906FA6BFAFE6EEB1E3F66390A4446D83759BB1D7F6
        FF6170871B203B1300000C030614132E6081A785E1F982DAEC8EDDE895DBE599
        DBE7A8DFE7BCE9EDCDEEEED4ECEBE5EDEE8DA1B25B7F920C00000A000019272B
        000D2384BEDC85D8F574D1F286D3F098DDEDB5EBF2C6EAE8DDE7E3E8E5E9EEE1
        EB6A969C141F27130000120000050000535656161C1E15272F4D6C7D577D912F
        3C4F141122CDB7C5E4D9E0E0D9E19E919C7A858D0500001700001403031D0406
        0B00004442406C69685C5C5F859599635F65A59296CCA5ADFCE4EAFCECF0FFDC
        E498595E5C26244F211E0900000F000010000002000000000000000000000004
        000007000077393DE3A5AAE2A8AC9749504E16172000001C0000}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      Visible = False
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 708
    Width = 1276
    Height = 33
    Align = alBottom
    TabOrder = 5
    object Label29: TLabel
      Left = 516
      Top = 7
      Width = 38
      Height = 16
      Caption = 'Total :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label30: TLabel
      Left = 653
      Top = 7
      Width = 64
      Height = 16
      Caption = 'Calculado :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object btnConfirma_rel: TBitBtn
      Left = 173
      Top = 4
      Width = 143
      Height = 25
      Caption = 'Confirma Relat'#243'rio'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0041924E003D8F49003A8C44003689400032873C002F84
        37002C813300287F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF004999580045965300419950007DC28F0096D0A60096CFA60078BE
        8900368D42002C813400297F3000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00519F61004D9C5D0064B47800A8DBB50087CC980066BC7D0064BA7C0086CB
        9800A5D9B40058AA6B002C813400297F3000FF00FF00FF00FF00FF00FF0059A6
        6B0056A366006AB97D00A8DBB20060BC77005CBA730059B8700059B56F0058B5
        6F005BB77400A5D9B3005AAA6C002C823400297F3000FF00FF00FF00FF005DA9
        700053AB6800AADDB40064C179005FBE710060BC7700FFFFFF00FFFFFF0059B8
        700058B56E005CB77400A6DAB400388F43002C823400FF00FF00FF00FF0061AC
        75008ACC980089D396006BC67A0063C1700055AB6500FFFFFF00FFFFFF0059B8
        700059B870005BB9720085CC97007BBE8D0030853900FF00FF00FF00FF0065AF
        7A00A9DDB3007DCF8A0075CC8100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0059B8700067BE7D009CD4AB0034883D00FF00FF00FF00FF0069B2
        7E00B6E2BE008BD597007AC98600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0059B8700069C17E009DD4AA00388B4200FF00FF00FF00FF006DB5
        8300ACDDB600A6DFAF0081CB8C007CC986006EBD7900FFFFFF00FFFFFF005BAC
        6A0060BC77005CBA73008BD1990080C592003C8E4700FF00FF00FF00FF0070B8
        870085C79700D2EED70095D9A0008AD394007FC88900FFFFFF00FFFFFF0079CD
        85006BC37C006FC77E00ACDFB500459E570040914C00FF00FF00FF00FF0073BA
        8A0070B88700AADAB700D8F1DC0092D89D0088CD930084CC8E008BD496008AD4
        950083D28E00AFE0B7006BB97D004898560044945100FF00FF00FF00FF00FF00
        FF0073BB8B0070B88700AFDCBB00DCF2E000B6E4BD009BDBA50096D9A000A5DF
        AF00C0E8C50079C28A00509E5F004C9B5B00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0073BB8B0071B8870094CEA400C3E6CB00CFEBD400C9E9CE00AFDD
        B8006DB97F0058A5690054A16500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0074BB8B0071B988006EB684006AB3800067B17C0063AE
        770060AB73005CA86E00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 0
      OnClick = btnConfirma_relClick
    end
    object btnImprimir: TBitBtn
      Left = 324
      Top = 4
      Width = 85
      Height = 25
      Caption = 'Imprimir'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D69E72C4D3996EF4D19668FFCE9263FFCB8E5EFFC98A5BFFC78756FFC384
        52FFC38452FFC38452FFC38452FFC38452FFC38452FFBB7742B0FF00FF00FF00
        FF00D7A175FFF8F2EDFFF7F0EAFFF6EDE6FFF4EAE2FFF3E7DEFFF1E4DBFFF0E2
        D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFF0E2D8FFC58A5DFDFF00FF00FF00
        FF00D9A47AFFF9F3EEFFEBD2BEFFFFFFFFFFEBD3BFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEAC7ADFFFFFFFFFFFFFFFFFFFFFFFFFFF0E2D8FFC68C5FFFFF00FF00FF00
        FF00DDA87EFFF9F3EFFFEBD0BAFFEBD0BBFFEBD0BBFFEBD0BBFFEBD0BBFFEBD1
        BDFFEACDB5FFEACDB5FFEACDB5FFEACDB5FFF0E2D8FFC68A5CFFFF00FF00FF00
        FF00DFAA82FFF9F3EFFFEACEB7FFFFFFFFFFEBD0BBFFFFFFFFFFFFFFFFFFFFFF
        FFFFEACFBAFFFBF6F2FFFFFFFFFFFFFFFFFFF0E2D8FFC88D5FFFFF00FF00A0A0
        A005D8AC8AFFE3DEDBFFD2BBA9FFDBC2ADFFE3C7B0FFEACCB3FFEACCB3FFEACE
        B7FFE8C7ACFFE8C7ACFFE8C8B0FFE8C8AEFFF0E2D8FFC48654FFA6A6A606A3A3
        A36AAFA49CFFABABAAFFA4A09CFFA7A7A7FFB6A79CFFF3F3F3FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF1E5DBFFC68655FFA9A9A9A9CBCB
        CBFFEAEAEAFFF0F0F0FFF2F2F2FFECECECFFCACACAFF9A9591FFE9C9AEFFE9C9
        B0FFE8C7ACFFE9C9B0FFE8C8B0FFE8CCB5FFF2E7DEFFC88A59FFACACACF0FAFA
        FAFFD7D7D7FFE2E2E2FFCACACAFFCFCFCFFFFFFFFFFF939393FFFFFFFFFFFFFF
        FFFFE8C7ACFFFFFFFFFFFFFFFFFFFFFFFFFFF7F1EBFFCB8F5FFFAEAEAEE8F8F8
        F8FFD1D1D1FFECECECFFC5C5C5FFC6C6C6FFFFFFFFFF9A9A9AFFE9C3A6FFE9C3
        A6FFE9C3A6FFE9C3A6FFE9C3A6FFE9C3A6FFFBF7F4FFCE9364FFB1B1B1E8F8F8
        F8FFD5D5D5FFEFEFEFFFCBCBCBFFCBCBCBFFFFFFFFFF9E9E9EFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F4FFD1976AFFB4B4B4E8F8F8
        F8FFDBDBDBFFEFEFEFFFD0D0D0FFD0D0D0FFFFFFFFFFA2A2A2FF8BCB93FF87C9
        8EFF82C689FF7EC384FF7AC180FF76BE7CFFFBF7F4FFD49B6FFFB6B6B6E9F2F2
        F2FFD9D9D9FFE4E4E4FFE2E2E2FFC8C8C8FFF1F1F1FFA5A5A5FFFBF7F4FFFBF7
        F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFFBF7F4FFD7A074F8B8B8B8EDF6F6
        F6FFF4F4F4FFF9F9F9FFFBFBFBFFF6F6F6FFF6F6F6FFA8A8A8FFE7B793FFE6B5
        90FFE4B28CFFE2AF88FFE0AC84FFDDA980FFDCA57DFFDAA37ACABABABAA4D6D6
        D6FFEDEDEDFFF9F9F9FFFBFBFBFFF0F0F0FFD9D9D9FFABABABD8FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBCBC10BBBB
        BB6CB9B9B9C0B7B7B7EBB5B5B5F3B2B2B2D0B0B0B08AADADAD25FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      TabOrder = 1
      OnClick = btnImprimirClick
    end
    object btnExcel: TBitBtn
      Left = 420
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Excel'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
        346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
        2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
        33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
        EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
        38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
        3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
        40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
        73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
        33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
        64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
        33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
        33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
        3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
        D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
        49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
        CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
        53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
        C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
        BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
        62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
        56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
        45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
        4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
        BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
        4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
        B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
        49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
        ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
        4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
        4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 2
      OnClick = btnExcelClick
    end
    object edcusto: TJvCalcEdit
      Left = 561
      Top = 5
      Width = 88
      Height = 24
      DisplayFormat = '##,###,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 4
      DecimalPlacesAlwaysShown = False
    end
    object edcalculado: TJvCalcEdit
      Left = 719
      Top = 5
      Width = 88
      Height = 24
      DisplayFormat = '##,###,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object JvDBNavigator2: TJvDBNavigator
      Left = 1
      Top = 5
      Width = 164
      Height = 24
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 5
    end
    object JvDBMaskEdit1: TJvDBMaskEdit
      Left = 813
      Top = 6
      Width = 500
      Height = 21
      DataField = 'xml'
      DataSource = dsGen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      EditMask = ''
    end
  end
  object MMNF: TMemo
    Left = 12
    Top = 135
    Width = 251
    Height = 139
    Lines.Strings = (
      'MMNF')
    TabOrder = 7
    Visible = False
  end
  object Panel1: TPanel
    Left = 485
    Top = 180
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    Visible = False
  end
  object QFornecedor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct(codclifor) codigo,razsoc nome, nvl(codcgc, codcp' +
        'f) nr_cnpj_cpf'
      'from cyber.rodcli'
      'where situac = '#39'A'#39' and codclifor in ('
      'select distinct transp from tb_manifesto m'
      'where (m.manifesto || m.filial) in ('
      
        'select (manifesto || filial) from crm_generalidades where fl_sta' +
        'tus = '#39'N'#39')'
      ')'
      'order by 2')
    Left = 349
    Top = 28
    object QFornecedorCODIGO: TFMTBCDField
      FieldName = 'CODIGO'
      ReadOnly = True
      Precision = 38
      Size = 4
    end
    object QFornecedorNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 80
    end
    object QFornecedorNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
  end
  object dtsFornecedor: TDataSource
    DataSet = QFornecedor
    Left = 269
    Top = 24
  end
  object QGen: TADOQuery
    Connection = dtmDados.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from vw_custo_generalidades'
      'where filial > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'order by data')
    Left = 184
    Top = 152
    object QGenFL_STATUS: TStringField
      FieldName = 'FL_STATUS'
      FixedChar = True
      Size = 1
    end
    object QGenAutorizacao: TBCDField
      FieldName = 'Autorizacao'
    end
    object QGenSERVICO: TStringField
      FieldName = 'SERVICO'
      Size = 50
    end
    object QGenFILIAL: TFMTBCDField
      FieldName = 'FILIAL'
      Precision = 38
      Size = 0
    end
    object QGendata: TDateField
      FieldName = 'data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QGenCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 80
    end
    object QGenTRANSPORTADORA: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 80
    end
    object QGenDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object QGenDESCRI: TStringField
      FieldName = 'DESCRI'
      Size = 40
    end
    object QGenESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object QGenNR_CONHECIMENTO: TFMTBCDField
      FieldName = 'NR_CONHECIMENTO'
      Precision = 38
      Size = 0
    end
    object QGenQUANTIDADE: TBCDField
      FieldName = 'QUANTIDADE'
      Precision = 16
      Size = 2
    end
    object QGenVLR_TOTAL_C: TBCDField
      FieldName = 'VLR_TOTAL_C'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QGenVLR_TOTAL_R: TBCDField
      FieldName = 'VLR_TOTAL_R'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QGenMARGEM: TFMTBCDField
      FieldName = 'MARGEM'
      ReadOnly = True
      DisplayFormat = '##0.00%'
      Precision = 38
      Size = 4
    end
    object QGenFATURA: TStringField
      FieldName = 'FATURA'
    end
    object QGenNR_CTE_PAGAR: TFMTBCDField
      FieldName = 'NR_CTE_PAGAR'
      Precision = 38
      Size = 0
    end
    object QGenCUSTO_COBRADO: TBCDField
      FieldName = 'CUSTO_COBRADO'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QGenUSER_FATURA: TStringField
      FieldName = 'USER_FATURA'
    end
    object QGenDT_FATURA: TDateTimeField
      FieldName = 'DT_FATURA'
    end
    object QGenCODCLIFOR: TFMTBCDField
      FieldName = 'CODCLIFOR'
      Precision = 38
      Size = 4
    end
    object QGenliberacao: TStringField
      FieldName = 'liberacao'
      Size = 500
    end
    object QGennovo_custo: TBCDField
      FieldName = 'novo_custo'
    end
    object QGentabela_custo: TBCDField
      FieldName = 'tabela_custo'
    end
    object QGentabela_receita: TBCDField
      FieldName = 'tabela_receita'
    end
    object QGenMANIFESTO: TFMTBCDField
      FieldName = 'MANIFESTO'
      Precision = 38
      Size = 0
    end
    object QGenveiculo: TStringField
      FieldName = 'veiculo'
      Size = 30
    end
    object QGenCTE_ORIGEM: TFMTBCDField
      FieldName = 'CTE_ORIGEM'
      Precision = 38
      Size = 0
    end
    object QGenintegra_rodopar: TStringField
      FieldName = 'integra_rodopar'
      Size = 1
    end
    object QGenTIPO_TRANSPORTADOR: TStringField
      FieldName = 'TIPO_TRANSPORTADOR'
      Size = 13
    end
  end
  object dsGen: TDataSource
    DataSet = mdGener
    Left = 316
    Top = 204
  end
  object iml: TImageList
    Left = 12
    Top = 284
    Bitmap = {
      494C010102000400200B10001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000000000000000000000000000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000000000000840000000000000000000000000000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000000000000084000000840000008400000000000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000000000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000000000000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00E7DEE700E7DE
      E700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DEE700E7DE
      E700E7DEE70000000000000000000000000000000000A59C9C00008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C0000000000000000000000000000000000A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      FFFFFFFF00000000800780070000000080078007000000008007820700000000
      800787070000000080078F870000000080078DC700000000800788E700000000
      8007806700000000800780270000000080078007000000008007800700000000
      8007800700000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object QSite: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct s.codfil, (s.codfil || '#39' - '#39' || substr(s.razsoc,' +
        '1,instr(s.razsoc,'#39' '#39')-1)|| '#39' - '#39' || c.descri) filial'
      
        'from tb_manifesto m left join cyber.rodfil s on m.filial = s.cod' +
        'fil'
      
        '                                  left join cyber.rodmun c on s.' +
        'codmun = c.codmun'
      'where s.ativa = '#39'S'#39
      'and m.status = '#39'S'#39
      'order by 1')
    Left = 944
    Top = 52
    object QSiteFILIAL: TStringField
      FieldName = 'FILIAL'
      ReadOnly = True
      Size = 106
    end
    object QSiteCODFIL: TBCDField
      FieldName = 'CODFIL'
      ReadOnly = True
      Precision = 32
    end
  end
  object dtsSite: TDataSource
    DataSet = QSite
    Left = 584
    Top = 56
  end
  object QRep: TADOQuery
    AutoCalcFields = False
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 8
        Value = 'TL7-F1'
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 53445
      end>
    SQL.Strings = (
      'select * from vw_fatura_generalidades'
      'where fatura =:0 '
      'and transp = :1')
    Left = 248
    Top = 152
    object QRepDATA: TDateTimeField
      DisplayWidth = 9
      FieldName = 'DATA'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QRepAUTORIZACAO: TFMTBCDField
      DisplayWidth = 14
      FieldName = 'AUTORIZACAO'
      Precision = 38
      Size = 0
    end
    object QRepFL_STATUS: TStringField
      DisplayWidth = 10
      FieldName = 'FL_STATUS'
      Size = 1
    end
    object QRepSERVICO: TStringField
      DisplayWidth = 50
      FieldName = 'SERVICO'
      Size = 50
    end
    object QRepFILIAL: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'FILIAL'
      Precision = 38
      Size = 0
    end
    object QRepCTE_GEN: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'CTE_GEN'
      Precision = 38
      Size = 0
    end
    object QRepQUANTIDADE: TBCDField
      DisplayWidth = 17
      FieldName = 'QUANTIDADE'
      Precision = 16
      Size = 2
    end
    object QRepVLR_TOTAL_C: TBCDField
      DisplayWidth = 17
      FieldName = 'VLR_TOTAL_C'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QRepVLR_TOTAL_R: TBCDField
      DisplayWidth = 17
      FieldName = 'VLR_TOTAL_R'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QRepCNPJ_CLIENTE: TStringField
      DisplayWidth = 14
      FieldName = 'CNPJ_CLIENTE'
      Size = 14
    end
    object QRepFATURA: TStringField
      DisplayWidth = 20
      FieldName = 'FATURA'
    end
    object QRepNR_CTE_PAGAR: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'NR_CTE_PAGAR'
      Precision = 38
      Size = 0
    end
    object QRepCUSTO_COBRADO: TBCDField
      DisplayWidth = 17
      FieldName = 'CUSTO_COBRADO'
      Precision = 16
      Size = 2
    end
    object QRepUSER_FATURA: TStringField
      DisplayWidth = 20
      FieldName = 'USER_FATURA'
    end
    object QRepDT_FATURA: TDateTimeField
      DisplayWidth = 18
      FieldName = 'DT_FATURA'
    end
    object QRepCLIENTE: TStringField
      DisplayWidth = 80
      FieldName = 'CLIENTE'
      Size = 80
    end
    object QRepCODCLIFOR: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'CODCLIFOR'
      Precision = 38
      Size = 4
    end
    object QRepTRANSPORTADORA: TStringField
      DisplayWidth = 80
      FieldName = 'TRANSPORTADORA'
      Size = 80
    end
    object QRepMARGEM: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'MARGEM'
      DisplayFormat = '##0.00 %'
      Precision = 38
      Size = 4
    end
    object QRepMANIFESTO: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'MANIFESTO'
      Precision = 38
      Size = 0
    end
    object QRepLIBERACAO: TStringField
      DisplayWidth = 500
      FieldName = 'LIBERACAO'
      Size = 500
    end
    object QRepNOVO_CUSTO: TBCDField
      DisplayWidth = 17
      FieldName = 'NOVO_CUSTO'
      Precision = 16
      Size = 2
    end
    object QRepVL_DESCONTO: TBCDField
      DisplayWidth = 17
      FieldName = 'VL_DESCONTO'
      Precision = 16
      Size = 2
    end
    object QRepOBS_FATURA: TStringField
      DisplayWidth = 100
      FieldName = 'OBS_FATURA'
      Size = 100
    end
    object QRepVL_IMPOSTO_NFS: TBCDField
      DisplayWidth = 17
      FieldName = 'VL_IMPOSTO_NFS'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object QRepCUSTO: TFMTBCDField
      DisplayWidth = 39
      FieldName = 'CUSTO'
      Precision = 38
      Size = 4
    end
    object QRepVL_PROCESSO: TBCDField
      DisplayWidth = 17
      FieldName = 'VL_PROCESSO'
      Precision = 16
      Size = 2
    end
  end
  object dsRep: TDataSource
    DataSet = QRep
    Left = 244
    Top = 204
  end
  object SP_Exporta: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_EXPORTA_FAT_GENERAL'
    Parameters = <
      item
        Name = 'V_FATURA'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'V_FORNE'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = Null
      end
      item
        Name = 'V_VCTO'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = 'V_USUARIO'
        Attributes = [paNullable]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    Left = 832
    Top = 104
  end
  object QNF: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = '1'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT notfis nr_nf FROM tb_cte_nf cn where cn.codcon = :0 and c' +
        'n.fl_empresa = :1')
    Left = 108
    object QNFNR_NF: TStringField
      FieldName = 'NR_NF'
    end
  end
  object mdGener: TJvMemoryData
    FieldDefs = <
      item
        Name = 'DATA'
        DataType = ftDateTime
      end
      item
        Name = 'AUTORIZACAO'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end
      item
        Name = 'FL_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SERVICO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FILIAL'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end
      item
        Name = 'NR_CONHECIMENTO'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end
      item
        Name = 'QUANTIDADE'
        DataType = ftBCD
        Precision = 16
        Size = 2
      end
      item
        Name = 'VLR_TOTAL_C'
        DataType = ftBCD
        Precision = 16
        Size = 2
      end
      item
        Name = 'VLR_TOTAL_R'
        DataType = ftBCD
        Precision = 16
        Size = 2
      end
      item
        Name = 'CNPJ_CLIENTE'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'FATURA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NR_CTE_PAGAR'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end
      item
        Name = 'CUSTO_COBRADO'
        DataType = ftBCD
        Precision = 16
        Size = 2
      end
      item
        Name = 'USER_FATURA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DT_FATURA'
        DataType = ftDateTime
      end
      item
        Name = 'CLIENTE'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CODCLIFOR'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end
      item
        Name = 'TRANSPORTADORA'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'DESTINO'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'DESCRI'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ESTADO'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'MARGEM'
        DataType = ftFMTBcd
        Precision = 38
        Size = 4
      end>
    DataSet = QGen
    AfterPost = mdGenerAfterPost
    OnFilterRecord = mdGenerFilterRecord
    Left = 316
    Top = 152
    object mdGenerescol: TStringField
      FieldName = 'escol'
      Size = 1
    end
    object mdGenerAutorizacao: TFMTBCDField
      FieldName = 'AUTORIZACAO'
      Precision = 38
      Size = 0
    end
    object mdGenerServico: TStringField
      FieldName = 'SERVICO'
      Size = 50
    end
    object mdGenerFilial: TFMTBCDField
      FieldName = 'FILIAL'
      Precision = 38
      Size = 0
    end
    object mdGenerData: TDateTimeField
      FieldName = 'DATA'
      DisplayFormat = 'dd/mm/yy'
    end
    object mdGenerCliente: TStringField
      FieldName = 'CLIENTE'
      Size = 80
    end
    object mdGenerTransportadora: TStringField
      FieldName = 'TRANSPORTADORA'
      Size = 80
    end
    object mdGenerDestino: TStringField
      FieldName = 'DESTINO'
      Size = 80
    end
    object mdGenerDescri: TStringField
      FieldName = 'DESCRI'
      Size = 40
    end
    object mdGenerEstado: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object mdGenerNr_conhecimento: TFMTBCDField
      FieldName = 'NR_CONHECIMENTO'
      Precision = 38
      Size = 0
    end
    object mdGenerQuantidade: TBCDField
      FieldName = 'QUANTIDADE'
      Precision = 16
      Size = 2
    end
    object mdGenerVlr_total_c: TBCDField
      FieldName = 'VLR_TOTAL_C'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object mdGenerVlr_total_r: TBCDField
      FieldName = 'VLR_TOTAL_R'
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object mdGenerMargem: TFMTBCDField
      FieldName = 'MARGEM'
      DisplayFormat = '##0.00%'
      Precision = 38
      Size = 4
    end
    object mdGenerfatura: TStringField
      FieldName = 'FATURA'
      OnChange = mdGenerfaturaChange
    end
    object mdGenerNr_cte_pagar: TFMTBCDField
      FieldName = 'NR_CTE_PAGAR'
      OnChange = mdGenerNr_cte_pagarChange
      Precision = 38
      Size = 0
    end
    object mdGenerCusto_cobrado: TBCDField
      FieldName = 'CUSTO_COBRADO'
      OnChange = mdGenerCusto_cobradoChange
      DisplayFormat = '###,##0.00'
      Precision = 16
      Size = 2
    end
    object mdGeneruser_fatura: TStringField
      FieldName = 'USER_FATURA'
    end
    object mdGenerDt_fatura: TDateTimeField
      FieldName = 'DT_FATURA'
    end
    object mdGenerCodclifor: TFMTBCDField
      FieldName = 'CODCLIFOR'
      Precision = 38
      Size = 4
    end
    object mdGenernotfis: TStringField
      FieldName = 'notfis'
      Size = 300
    end
    object mdGenerfl_status: TStringField
      FieldName = 'fl_status'
      Size = 1
    end
    object mdGenerliberacao: TStringField
      FieldName = 'liberacao'
      Size = 500
    end
    object mdGenernovo_custo: TBCDField
      FieldName = 'novo_custo'
    end
    object mdGenertabela_custo: TIntegerField
      FieldName = 'tabela_custo'
    end
    object mdGenertabela_receita: TIntegerField
      FieldName = 'tabela_receita'
    end
    object mdGenerManifesto: TBCDField
      FieldName = 'Manifesto'
    end
    object mdGenerveiculo: TStringField
      FieldName = 'veiculo'
      Size = 30
    end
    object mdGenercte_origem: TIntegerField
      FieldName = 'cte_origem'
    end
    object mdGenerintegra_rodopar: TStringField
      FieldName = 'integra_rodopar'
      Size = 1
    end
    object mdGenerxml: TStringField
      FieldName = 'xml'
      Size = 44
    end
    object mdGenertipo_transportdor: TStringField
      FieldName = 'tipo_transportdor'
    end
  end
  object SP_Atualiza: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_ALTERA_CUSTO_GENER'
    Parameters = <
      item
        Name = 'VCTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end
      item
        Name = 'VCUSTO'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 38
        Value = 0.000000000000000000
      end
      item
        Name = 'VFATURA'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'VID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end>
    Left = 244
    Top = 276
  end
  object Sp_Custo: TADOStoredProc
    Connection = dtmDados.ConIntecom
    ProcedureName = 'PRC_GENERALIDADE_CUSTO_NEW'
    Parameters = <
      item
        Name = 'R_ARMAZENAGEM'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_AJUDANTES'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_PALETIZACAO'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 40.000000000000000000
      end
      item
        Name = 'R_DIARIA'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_REENTREGA'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 126.130000000000000000
      end
      item
        Name = 'R_PERNOITE'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_HORA_PARADA'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_DEDICADO'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_CANHOTO'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        Precision = 15
        Value = 0.000000000000000000
      end
      item
        Name = 'R_COD_TAB'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdOutput
        Precision = 38
        Value = 38758
      end
      item
        Name = 'V_CONTROLER'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Size = 16
        Value = Null
      end
      item
        Name = 'V_CTE'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 1532
      end
      item
        Name = 'V_FILIAL'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 12
      end
      item
        Name = 'V_MAN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 158
      end
      item
        Name = 'V_VEICULO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '0'
      end
      item
        Name = 'V_DIAS'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 38
        Value = 0
      end>
    Left = 164
    Top = 276
  end
end
