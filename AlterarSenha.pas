unit AlterarSenha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, Variants, Grids, DBGrids,
  inifiles, Mask, ADODB, JvExControls,
  JvGradient;

type
  TFrmAlterarSenha = class(TForm)
    Panel1: TPanel;
    btnOk: TBitBtn;
    btnCancelar: TBitBtn;
    QSenha: TADOQuery;
    Panel2: TPanel;
    Label1: TLabel;
    edUsuario: TEdit;
    Label3: TLabel;
    edNewSenha: TEdit;
    Label4: TLabel;
    edNewSenha2: TEdit;
    JvGradient3: TJvGradient;
    QSenhaNEW_SENHA: TStringField;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edNewSenha2Exit(Sender: TObject);
    procedure edNewSenhaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    constructor CreateNovo(Sender: TObject; iTipo: integer); Virtual;

  private
    { Private declarations }
    i_Tipo: integer;
  public
    { Public declarations }
  end;

var
  FrmAlterarSenha: TFrmAlterarSenha;

implementation

uses dados, menu, funcoes;

{$R *.DFM}

procedure TFrmAlterarSenha.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

procedure TFrmAlterarSenha.FormShow(Sender: TObject);
begin
  edNewSenha.SetFocus;
end;

procedure TFrmAlterarSenha.FormCreate(Sender: TObject);
begin
  edUsuario.text := GlbUser;
end;

procedure TFrmAlterarSenha.btnOkClick(Sender: TObject);
var
  new: string;
begin
  if edNewSenha.text = '' then
  begin
    ShowMessage('N�o foi escolhida Nova Senha');
    edNewSenha.SetFocus;
    exit;
  end;

  dtmdados.IQuery1.close;
  dtmdados.IQuery1.sql.clear;
  dtmdados.IQuery1.sql.add
    ('update tb_usuarios set senha = (SELECT passwd_encrypt(:0,:1) new_senha ');
  dtmdados.IQuery1.sql.add('FROM DUAL) where COD_USUARIO = :2 ');
  dtmdados.IQuery1.Parameters[0].value := GLBCodUser;
  dtmdados.IQuery1.Parameters[1].value := edNewSenha.text;
  dtmdados.IQuery1.Parameters[2].value := GLBCodUser;
  dtmdados.IQuery1.ExecSQL;
  dtmdados.IQuery1.close;

  ShowMessage('Senha Alterada com Sucesso !!');
  btnOk.Enabled := false;
  close;
end;

constructor TFrmAlterarSenha.CreateNovo(Sender: TObject; iTipo: integer);
begin
  i_Tipo := iTipo;
  Create(Owner);
end;

procedure TFrmAlterarSenha.btnCancelarClick(Sender: TObject);
begin
  close;
end;

procedure TFrmAlterarSenha.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  close;
end;

procedure TFrmAlterarSenha.edNewSenha2Exit(Sender: TObject);
begin
  if edNewSenha.text <> '' then
  begin
    if edNewSenha.text = edNewSenha2.text then
      btnOk.Enabled := true
    else
    begin
      ShowMessage('Favor digite novamente, digita��o diferente !!');
      edNewSenha.SetFocus;
    end;
  end;
end;

procedure TFrmAlterarSenha.edNewSenhaExit(Sender: TObject);
begin
  if edNewSenha.text = 'MUDAR123' then
  begin
    ShowMessage('Esta Senha n�o � Permitida !!');
    edNewSenha.clear;
    edNewSenha.SetFocus;
  end;
end;

end.
