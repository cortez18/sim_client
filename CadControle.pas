unit CadControle;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, JvExControls, JvDBLookup, DB, ImgList, ADODB, ActnList, Buttons,
  Grids, DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ExtCtrls, Mask,
  JvExMask, JvToolEdit, JvBaseEdits, JvExStdCtrls, JvGroupBox, Menus, ComCtrls,
  JvExComCtrls, JvComCtrls, System.ImageList, System.Actions;

type
  TfrmCadControle = class(TForm)
    dtsControle: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QControle: TADOQuery;
    iml: TImageList;
    QControleDOC: TStringField;
    QControleSERIE: TStringField;
    QControleFILIAL: TBCDField;
    QControleDATAINC: TDateTimeField;
    QControleUSUARIO: TStringField;
    dtsSite: TDataSource;
    QSite: TADOQuery;
    QSiteCODFIL: TBCDField;
    QSiteFILIAL: TStringField;
    QControleNUM: TBCDField;
    QControleNM_FILIAL: TStringField;
    QControleAMBIENTE: TStringField;
    QControleNM_AMBIENTE: TStringField;
    QControleREG: TBCDField;
    QControleCERTIFICADO: TStringField;
    QControleLOGO: TStringField;
    QControlePDF: TStringField;
    QControleCUSTO: TBCDField;
    QControleSCHEMMA: TStringField;
    QControlePROXY: TStringField;
    QControlePORTA: TBCDField;
    QControleUSER_PROXY: TStringField;
    QControlePASS_PROXY: TStringField;
    QControleFL_CONFERENCIA: TStringField;
    QControleCODTARJ: TBCDField;
    QControleCODTARF: TBCDField;
    QControleAMBIENTE_CTE: TStringField;
    QControleNUM_CTE: TBCDField;
    QControleSERIE_CTE: TStringField;
    QControleFILIAL_CTE: TBCDField;
    PopupMenu1: TPopupMenu;
    QControleCONT_CTE: TStringField;
    QEmpresa: TADOQuery;
    dtsEmpresa: TDataSource;
    QEmpresaCODFIL: TBCDField;
    QEmpresaEMAIL: TStringField;
    QEmpresaSENHA: TStringField;
    QEmpresaSMTP: TStringField;
    QEmpresaPORTA: TStringField;
    QEmpresaDATATU: TDateTimeField;
    QEmpresaUSUATU: TStringField;
    QEmpresaEFISCAL: TStringField;
    QEmpresaREQAUT: TStringField;
    JvPageControl1: TJvPageControl;
    TabSheet1: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    TabSheet2: TTabSheet;
    pnBotao: TPanel;
    Label6: TLabel;
    sbtnGetCert: TSpeedButton;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    edtNumSerie: TEdit;
    edLogo: TEdit;
    edCusto: TJvCalcEdit;
    edSchema: TEdit;
    edProxy: TEdit;
    edUser: TEdit;
    edPorta: TJvCalcEdit;
    edPass: TEdit;
    cbConf: TCheckBox;
    JvGroupBox1: TJvGroupBox;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label16: TLabel;
    edSerieCte: TEdit;
    edNumCte: TJvCalcEdit;
    cbAmbCte: TComboBox;
    cbCont: TCheckBox;
    JvGroupBox2: TJvGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    cbSite: TJvDBLookupCombo;
    EdNum: TJvCalcEdit;
    cbMdfe: TComboBox;
    dbgContrato: TJvDBUltimGrid;
    Panel3: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnCancelar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnInserir: TBitBtn;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn9: TBitBtn;
    Panel2: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    CheckBox1: TCheckBox;
    JvCalcEdit3: TJvCalcEdit;
    Label38: TLabel;
    JvCalcEdit5: TJvCalcEdit;
    Edit3: TEdit;
    QEmpresaVERAO: TWideStringField;
    CheckBox2: TCheckBox;
    Label8: TLabel;
    edPdf: TEdit;
    Label19: TLabel;
    edPdfCte: TEdit;
    QControlePDF_CTE: TStringField;
    Label23: TLabel;
    edCodtarJ: TJvCalcEdit;
    edCodtarF: TJvCalcEdit;
    Label26: TLabel;
    QEmpresaCADRAT: TFMTBCDField;
    Label27: TLabel;
    edRateio: TJvCalcEdit;
    cbTrip: TCheckBox;
    QEmpresaTMP_CANC_CTE: TFMTBCDField;
    QEmpresaSENHA_A3: TStringField;
    QEmpresaLAGER: TStringField;
    QEmpresaTRIP: TStringField;
    Label28: TLabel;
    edLager: TEdit;
    Label29: TLabel;
    edA3: TEdit;
    Label30: TLabel;
    edImpEt: TEdit;
    QEmpresaIMP_ETI_VOL: TStringField;
    cbRoadnet: TCheckBox;
    QEmpresaTRANSP_ROADNET: TStringField;
    Label31: TLabel;
    cbContMdfe: TCheckBox;
    QControlePRESUMIDO: TStringField;
    QControlePALET: TFMTBCDField;
    QControleCODTAR: TFMTBCDField;
    QControleCONT_MDFE: TStringField;
    QEmpresaUSAR: TStringField;
    QEmpresaLOGO: TStringField;
    QEmpresaFILIAL_TSI: TStringField;
    QEmpresaCNPJ_MATRIZ: TStringField;
    cbAtivo: TCheckBox;

    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Restaura;
    procedure Restaura1;
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnInserirClick(Sender: TObject);
    procedure sbtnGetCertClick(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    quant: integer;
    tipo: string;
  public
    { Public declarations }
  end;

var
  frmCadControle: TfrmCadControle;

implementation

uses Dados, menu, funcoes;

{$R *.DFM}

procedure TfrmCadControle.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadControle.btnInserirClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura;
  tipo := 'I';
  Edit1.text := '';
  Edit2.text := '';
  EdNum.value := 0;
  cbMdfe.ItemIndex := -1;
  edtNumSerie.Clear;
  edLogo.Clear;
  edPdf.Clear;
  edCusto.value := 0;
  edSchema.text := '';
  edProxy.Clear;
  edPorta.value := 0;
  edUser.Clear;
  edPass.Clear;
  cbConf.Checked := false;
  edNumCte.value := 0;
  edSerieCte.Clear;
  cbAmbCte.ItemIndex := -1;
  edCodtarJ.value := 0;
  edCodtarF.value := 0;
  cbCont.Checked := false;
  Edit1.SetFocus;
  edPdfCte.Clear;
end;

procedure TfrmCadControle.btnSalvarClick(Sender: TObject);
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_controle (doc, serie, filial, usuario, ');
      dtmDados.IQuery1.SQL.add
        ('num, ambiente, certificado, logo, pdf, custo, schemma, ');
      dtmDados.IQuery1.SQL.add
        ('proxy, porta, user_proxy, pass_proxy, fl_conferencia, ');
      dtmDados.IQuery1.SQL.add
        ('ambiente_cte, num_cte, serie_cte, cont_cte, pdf_cte, filial_cte, codtarj, codtarf, cont_mdfe) ');
      dtmDados.IQuery1.SQL.add
        ('values (:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, ');
      dtmDados.IQuery1.SQL.add
        (':11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24) ');
      dtmDados.IQuery1.Parameters[0].value := Edit1.text;
      dtmDados.IQuery1.Parameters[1].value := Edit2.text;
      dtmDados.IQuery1.Parameters[2].value := QSiteCODFIL.AsInteger;
      dtmDados.IQuery1.Parameters[3].value := GLBUSER;
      dtmDados.IQuery1.Parameters[4].value := EdNum.value;
      dtmDados.IQuery1.Parameters[5].value := copy(cbMdfe.text, 1, 1);
      dtmDados.IQuery1.Parameters[6].value := edtNumSerie.text;
      dtmDados.IQuery1.Parameters[7].value := edLogo.text;
      dtmDados.IQuery1.Parameters[8].value := edPdf.text;
      dtmDados.IQuery1.Parameters[9].value := edCusto.value;
      dtmDados.IQuery1.Parameters[10].value := edSchema.text;
      dtmDados.IQuery1.Parameters[11].value := edProxy.text;
      dtmDados.IQuery1.Parameters[12].value := edPorta.value;
      dtmDados.IQuery1.Parameters[13].value := edUser.text;
      dtmDados.IQuery1.Parameters[14].value := edPass.text;
      if cbConf.Checked = true then
        dtmDados.IQuery1.Parameters[15].value := 'S'
      else
        dtmDados.IQuery1.Parameters[15].value := 'N';
      dtmDados.IQuery1.Parameters[16].value := copy(cbAmbCte.text, 1, 1);
      dtmDados.IQuery1.Parameters[17].value := edNumCte.value;
      dtmDados.IQuery1.Parameters[18].value := edSerieCte.text;
      if cbCont.Checked = true then
        dtmDados.IQuery1.Parameters[19].value := 'S'
      else
        dtmDados.IQuery1.Parameters[19].value := 'N';
      dtmDados.IQuery1.Parameters[20].value := edPdfCte.text;
      dtmDados.IQuery1.Parameters[21].value := QSiteCODFIL.AsInteger;
      dtmDados.IQuery1.Parameters[22].value := edCodtarJ.value;
      dtmDados.IQuery1.Parameters[23].value := edCodtarF.value;
      if cbContMdfe.Checked = true then
        dtmDados.IQuery1.Parameters[24].value := 'S'
      else
        dtmDados.IQuery1.Parameters[24].value := 'N';

      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_controle set doc =:0, serie = :1, filial =:2, ');
      dtmDados.IQuery1.SQL.add
        ('usuario = :3, num = :4, ambiente = :5, certificado = :6, ');
      dtmDados.IQuery1.SQL.add
        ('logo = :7, pdf = :8, custo = :9, schemma = :10, ');
      dtmDados.IQuery1.SQL.add
        ('proxy = :11, porta = :12, user_proxy = :13, pass_proxy = :14, ');
      dtmDados.IQuery1.SQL.add
        ('fl_conferencia = :15, ambiente_cte = :16, num_cte = :17, ');
      dtmDados.IQuery1.SQL.add
        ('serie_cte = :18, cont_cte = :19, pdf_cte = :20, CodtarJ = :21, CodtarF = :22, cont_mdfe = :23 where reg = :24 ');
      dtmDados.IQuery1.Parameters[0].value := Edit1.text;
      dtmDados.IQuery1.Parameters[1].value := Edit2.text;
      dtmDados.IQuery1.Parameters[2].value := QSiteCODFIL.AsInteger;
      dtmDados.IQuery1.Parameters[3].value := GLBUSER;
      dtmDados.IQuery1.Parameters[4].value := EdNum.value;
      dtmDados.IQuery1.Parameters[5].value := copy(cbMdfe.text, 1, 1);
      dtmDados.IQuery1.Parameters[6].value := edtNumSerie.text;
      dtmDados.IQuery1.Parameters[7].value := edLogo.text;
      dtmDados.IQuery1.Parameters[8].value := edPdf.text;
      dtmDados.IQuery1.Parameters[9].value := edCusto.value;
      dtmDados.IQuery1.Parameters[10].value := edSchema.text;
      dtmDados.IQuery1.Parameters[11].value := edProxy.text;
      dtmDados.IQuery1.Parameters[12].value := edPorta.value;
      dtmDados.IQuery1.Parameters[13].value := edUser.text;
      dtmDados.IQuery1.Parameters[14].value := edPass.text;
      if cbConf.Checked = true then
        dtmDados.IQuery1.Parameters[15].value := 'S'
      else
        dtmDados.IQuery1.Parameters[15].value := 'N';
      dtmDados.IQuery1.Parameters[16].value := copy(cbAmbCte.text, 1, 1);
      dtmDados.IQuery1.Parameters[17].value := edNumCte.value;
      dtmDados.IQuery1.Parameters[18].value := edSerieCte.text;
      if cbCont.Checked = true then
        dtmDados.IQuery1.Parameters[19].value := 'S'
      else
        dtmDados.IQuery1.Parameters[19].value := 'N';
      dtmDados.IQuery1.Parameters[20].value := edPdfCte.text;
      dtmDados.IQuery1.Parameters[21].value := edCodtarJ.value;
      dtmDados.IQuery1.Parameters[22].value := edCodtarF.value;
      if cbContMdfe.Checked = true then
        dtmDados.IQuery1.Parameters[23].value := 'S'
      else
        dtmDados.IQuery1.Parameters[23].value := 'N';
      dtmDados.IQuery1.Parameters[24].value := QControleREG.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura;
  tipo := '';
  QControle.Close;
  QControle.Open;
end;

procedure TfrmCadControle.btnCancelarClick(Sender: TObject);
begin
  Restaura;
  QControle.Cancel;
end;

procedure TfrmCadControle.BitBtn2Click(Sender: TObject);
begin
  Restaura1;
  QEmpresa.Cancel;
end;

procedure TfrmCadControle.BitBtn3Click(Sender: TObject);
begin
  try
    if tipo = 'I' then
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('insert into tb_empresa (codfil, email, senha, smtp, ');
      dtmDados.IQuery1.SQL.add('porta, usuatu, efiscal, reqaut, verao, cadrat, ');
      dtmDados.IQuery1.SQL.add('senha_a3, lager, trip, imp_eti_vol, transp_roadnet, usar) ');
      dtmDados.IQuery1.SQL.add('values (:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, ');
      dtmDados.IQuery1.SQL.add(':10, :11, :12, :13, :14, :15) ');
      dtmDados.IQuery1.Parameters[0].value := JvCalcEdit3.value;
      dtmDados.IQuery1.Parameters[1].value := Edit4.text;
      dtmDados.IQuery1.Parameters[2].value := Edit5.text;
      dtmDados.IQuery1.Parameters[3].value := Edit6.text;
      dtmDados.IQuery1.Parameters[4].value := JvCalcEdit5.text;
      dtmDados.IQuery1.Parameters[5].value := GLBUSER;
      dtmDados.IQuery1.Parameters[6].value := Edit3.text;
      if CheckBox1.Checked = true then
        dtmDados.IQuery1.Parameters[7].value := 'S'
      else
        dtmDados.IQuery1.Parameters[7].value := 'N';
      if CheckBox2.Checked = true then
        dtmDados.IQuery1.Parameters[8].value := 'S'
      else
        dtmDados.IQuery1.Parameters[8].value := 'N';
      dtmDados.IQuery1.Parameters[9].value := edRateio.AsInteger;
      dtmDados.IQuery1.Parameters[10].value := edA3.text;
      dtmDados.IQuery1.Parameters[11].value := edLager.text;
      if CbTRip.Checked = true then
        dtmDados.IQuery1.Parameters[12].value := 'S'
      else
        dtmDados.IQuery1.Parameters[12].value := 'N';
      if edImpEt.Text <> '' then
        dtmDados.IQuery1.Parameters[13].value := edImpEt.Text
      else
        dtmDados.IQuery1.Parameters[13].value := '';
      if cbRoadnet.Checked = true then
        dtmDados.IQuery1.Parameters[14].value := 'S'
      else
        dtmDados.IQuery1.Parameters[14].value := 'N';
      if CBAtivo.Checked = true then
        dtmDados.IQuery1.Parameters[15].value := 'S'
      else
        dtmDados.IQuery1.Parameters[15].value := 'N';
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end
    else
    begin
      dtmDados.IQuery1.Close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_empresa set email = :0, senha = :1, ');
      dtmDados.IQuery1.SQL.add
        ('smtp = :2, porta = :3, usuatu = :4, efiscal = :5, ');
      dtmDados.IQuery1.SQL.add('reqaut = :6, verao = :7, cadrat = :8, ');
      dtmDados.IQuery1.SQL.add('senha_a3 = :9, lager = :10, trip = :11, ');
      dtmDados.IQuery1.SQL.add('imp_eti_vol = :12, transp_roadnet = :13, usar = :14 ');
      dtmDados.IQuery1.SQL.add('where codfil = :15 ');
      dtmDados.IQuery1.Parameters[0].value := Edit4.text;
      dtmDados.IQuery1.Parameters[1].value := Edit5.text;
      dtmDados.IQuery1.Parameters[2].value := Edit6.text;
      dtmDados.IQuery1.Parameters[3].value := JvCalcEdit5.text;
      dtmDados.IQuery1.Parameters[4].value := GLBUSER;
      dtmDados.IQuery1.Parameters[5].value := Edit3.text;
      if CheckBox1.Checked = true then
        dtmDados.IQuery1.Parameters[6].value := 'S'
      else
        dtmDados.IQuery1.Parameters[6].value := 'N';
      if CheckBox2.Checked = true then
        dtmDados.IQuery1.Parameters[7].value := 'S'
      else
        dtmDados.IQuery1.Parameters[7].value := 'N';
      dtmDados.IQuery1.Parameters[8].value := edRateio.AsInteger;

      dtmDados.IQuery1.Parameters[9].value := edA3.text;
      dtmDados.IQuery1.Parameters[10].value := edLager.text;
      if CbTRip.Checked = true then
        dtmDados.IQuery1.Parameters[11].value := 'S'
      else
        dtmDados.IQuery1.Parameters[11].value := 'N';
      if edImpEt.Text <> '' then
        dtmDados.IQuery1.Parameters[12].value := edImpEt.Text
      else
        dtmDados.IQuery1.Parameters[12].value := '';
      if cbRoadnet.Checked = true then
        dtmDados.IQuery1.Parameters[13].value := 'S'
      else
        dtmDados.IQuery1.Parameters[13].value := 'N';
      if cbAtivo.Checked = true then
        dtmDados.IQuery1.Parameters[14].value := 'S'
      else
        dtmDados.IQuery1.Parameters[14].value := 'N';
      dtmDados.IQuery1.Parameters[15].value := JvCalcEdit3.value;

      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.Close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  Restaura1;
  JvCalcEdit3.Enabled := true;
  tipo := '';
  QEmpresa.Close;
  QEmpresa.Open;
end;

procedure TfrmCadControle.BitBtn4Click(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura1;
  tipo := 'A';
  Edit3.text := QEmpresaEFISCAL.value;
  Edit4.text := QEmpresaEMAIL.value;
  Edit5.text := QEmpresaSENHA.value;
  Edit6.text := QEmpresaSMTP.value;
  JvCalcEdit3.value := QEmpresaCODFIL.AsInteger;
  JvCalcEdit5.value := QEmpresaPORTA.AsInteger;
  edRateio.Value := QEmpresaCADRAT.AsInteger;
  if QEmpresaREQAUT.value = 'S' then
    CheckBox1.Checked := true
  else
    CheckBox1.Checked := false;
  JvCalcEdit3.Enabled := false;
  if QEmpresaVERAO.value = 'S' then
    CheckBox2.Checked := true
  else
    CheckBox2.Checked := false;
  edLager.Text := QEmpresaLAGER.AsString;
  edA3.Text := QEmpresaSENHA_A3.AsString;
  if QEmpresaTrip.value = 'S' then
    cbTrip.Checked := true
  else
    CbTrip.Checked := false;
  edImpEt.text := QEmpresaIMP_ETI_VOL.AsString;

  if QEmpresaTRANSP_ROADNET.value = 'S' then
    cbRoadnet.Checked := true
  else
    cbRoadnet.Checked := false;

  if QEmpresaUsar.value = 'S' then
    cbAtivo.Checked := true
  else
    cbAtivo.Checked := false;

  Edit4.SetFocus;
end;

procedure TfrmCadControle.BitBtn9Click(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  Restaura1;
  tipo := 'I';
  Edit3.text := QEmpresaEFISCAL.value;
  Edit4.text := QEmpresaEMAIL.value;
  Edit5.text := QEmpresaSENHA.value;
  Edit6.text := QEmpresaSMTP.value;
  edRateio.Value := QEmpresaCADRAT.AsInteger;
  JvCalcEdit3.value := 0;
  JvCalcEdit5.value := QEmpresaPORTA.AsInteger;
  CheckBox1.Checked := false;
  CheckBox2.Checked := false;
  cbRoadnet.Checked := false;
  cbAtivo.Checked   := false;
  edImpEt.Clear;
  Edit4.SetFocus;
end;

procedure TfrmCadControle.btnAlterarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  Restaura;
  tipo := 'A';
  Edit1.text := QControleDOC.value;
  Edit2.text := QControleSERIE.value;
  EdNum.value := QControleNUM.AsInteger;
  if QControleAMBIENTE.AsString = 'P' then
    cbMdfe.ItemIndex := cbMdfe.Items.IndexOf('Produ��o')
  else
    cbMdfe.ItemIndex := cbMdfe.Items.IndexOf('Homologa��o');
  edtNumSerie.text := QControleCERTIFICADO.value;
  edLogo.text := QControleLOGO.value;
  edPdf.text := QControlePDF.value;
  edCusto.value := QControleCUSTO.value;
  edSchema.text := QControleSCHEMMA.value;
  edProxy.text := QControlePROXY.value;
  edPass.text := QControlePASS_PROXY.value;
  edUser.text := QControleUSER_PROXY.value;
  edPorta.value := QControlePORTA.value;
  if QControleFL_CONFERENCIA.value = 'S' then
    cbConf.Checked := true
  else
    cbConf.Checked := false;
  edNumCte.value := QControleNUM_CTE.value;
  edSerieCte.text := QControleSERIE_CTE.value;
  if QControleAMBIENTE_CTE.AsString = 'P' then
    cbAmbCte.ItemIndex := cbMdfe.Items.IndexOf('Produ��o')
  else
    cbAmbCte.ItemIndex := cbMdfe.Items.IndexOf('Homologa��o');
  if QControleCONT_CTE.value = 'S' then
    cbCont.Checked := true
  else
    cbCont.Checked := false;
  edPdfCte.text := QControlePDF_CTE.AsString;
  edCodtarJ.value := QControleCODTARJ.AsInteger;
  edCodtarF.value := QControleCODTARF.AsInteger;
  if QControlecont_mdfe.value = 'S' then
    cbContMdfe.Checked := true
  else
    cbContMdfe.Checked := false;

  Edit1.SetFocus;
end;

procedure TfrmCadControle.FormCreate(Sender: TObject);
begin
  QEmpresa.Open;
  QControle.Open;
  QSite.Open;
  quant := QControle.RecordCount;
  lblQuant.Caption := IntToStr(QControle.RecNo) + #13#10 + IntToStr(quant);
end;

procedure TfrmCadControle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QControle.Close;
  QSite.Close;
  QEmpresa.Close;
end;

procedure TfrmCadControle.Restaura;
begin
  btnInserir.Enabled := not btnInserir.Enabled;
  btnAlterar.Enabled := not btnAlterar.Enabled;
  btnSalvar.Enabled := not btnSalvar.Enabled;
  btnCancelar.Enabled := not btnCancelar.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;

  if pnBotao.Visible then
  begin
    pnBotao.Visible := false;
  end
  else
  begin
    pnBotao.Visible := true;
  end;
end;

procedure TfrmCadControle.Restaura1;
begin
  BitBtn9.Enabled := not BitBtn9.Enabled;
  BitBtn4.Enabled := not BitBtn4.Enabled;
  BitBtn3.Enabled := not BitBtn3.Enabled;
  BitBtn2.Enabled := not BitBtn2.Enabled;
  BitBtn1.Enabled := not BitBtn1.Enabled;

  if Panel2.Visible then
  begin
    Panel2.Visible := false;
  end
  else
  begin
    Panel2.Visible := true;
  end;
end;

procedure TfrmCadControle.sbtnGetCertClick(Sender: TObject);
begin
{$IFNDEF ACBrCTeOpenSSL}
  edtNumSerie.text := frmMenu.ACBrMdfe1.SSL.SelecionarCertificado;
{$ENDIF}
end;

procedure TfrmCadControle.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QControle.First;
    2:
      QControle.Prior;
    3:
      QControle.Next;
    4:
      QControle.Last;
  end;
  lblQuant.Caption := IntToStr(QControle.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
