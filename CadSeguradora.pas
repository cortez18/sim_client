unit CadSeguradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls,
  StdCtrls, Buttons, Mask, Grids, DBGrids, ComCtrls, ComObj, ADODB, ActnList,
  JvToolEdit, JvExMask, JvMaskEdit, JvExDBGrids, JvDBGrid, JvDBUltimGrid,
  JvExComCtrls, JvComCtrls, JvBaseEdits, ShellAPI, System.Actions, Vcl.DBCtrls,
  JvExStdCtrls, JvCheckBox;

type
  TfrmCadSeguradora = class(TForm)
    dtsSeguradora: TDataSource;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QCadSeguradora: TADOQuery;
    Panel1: TPanel;
    btnFechar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    btnNovo: TBitBtn;
    QCadSeguradoraCODIGO: TBCDField;
    QCadSeguradoraSEGURADORA: TStringField;
    QCadSeguradoraCNPJ: TStringField;
    QCadSeguradoraAPOLICE: TStringField;
    QCadSeguradoraCONTATO: TStringField;
    QCadSeguradoraTELEFONE: TStringField;
    QCadSeguradoraDATAINICIO: TDateTimeField;
    QCadSeguradoraDATAFIM: TDateTimeField;
    QCadSeguradoraVALORMIN: TBCDField;
    QCadSeguradoraVALORMAX: TBCDField;
    QCadSeguradoraUSER_WS: TStringField;
    QCadSeguradoraPASS_WS: TStringField;
    QCadSeguradoraCODE_WS: TStringField;
    QCadSeguradoraFL_EMPRESA: TBCDField;
    btnDcto: TBitBtn;
    btnVer: TBitBtn;
    OpenDialog: TOpenDialog;
    QCadSeguradoraDOC_SEGURADORA: TStringField;
    QCadSeguradorawsdl: TStringField;
    QPGRItem: TADOQuery;
    QPGR: TADOQuery;
    dtsPGR: TDataSource;
    dtsItem: TDataSource;
    QPGRID_REGRA: TFMTBCDField;
    QPGRCOD_CLIENTE: TFMTBCDField;
    QPGRDESTINO: TStringField;
    QPGRDT_INC: TDateTimeField;
    QPGRUSER_INC: TFMTBCDField;
    QPGRDT_ALT: TDateTimeField;
    QPGRUSER_ALT: TFMTBCDField;
    QPGRRAZSOC: TStringField;
    QPGRItemID_REGRA_I: TFMTBCDField;
    QPGRItemID_REGRA: TFMTBCDField;
    QPGRItemFAIXAI: TBCDField;
    QPGRItemFAIXAF: TBCDField;
    QPGRItemPERFIL1: TStringField;
    QPGRItemPERFIL2: TStringField;
    QPGRItemPERFIL3: TStringField;
    QPGRItemPERFIL4: TStringField;
    QPGRItemPERFIL5: TStringField;
    QPGRItemPERFIL6: TStringField;
    QPGRItemDT_INC: TDateTimeField;
    QPGRItemUSER_INC: TFMTBCDField;
    QPGRItemDT_ALT: TDateTimeField;
    QPGRItemUSER_ALT: TFMTBCDField;
    JvPageControl1: TJvPageControl;
    TabSheet1: TTabSheet;
    dbgContrato: TJvDBUltimGrid;
    tbInfo: TTabSheet;
    lbltipo: TLabel;
    JvMaskEdit8: TJvMaskEdit;
    EdNome: TJvMaskEdit;
    JvMaskEdit10: TJvMaskEdit;
    EdApolice: TJvMaskEdit;
    JvMaskEdit16: TJvMaskEdit;
    edContato: TJvMaskEdit;
    JvMaskEdit18: TJvMaskEdit;
    JvMaskEdit6: TJvMaskEdit;
    EDCnpj: TJvMaskEdit;
    JvMaskEdit3: TJvMaskEdit;
    JvMaskEdit7: TJvMaskEdit;
    EdFone: TJvMaskEdit;
    dtI: TJvDateEdit;
    dtF: TJvDateEdit;
    JvMaskEdit4: TJvMaskEdit;
    eduser: TJvMaskEdit;
    JvMaskEdit9: TJvMaskEdit;
    edcod: TJvMaskEdit;
    JvMaskEdit12: TJvMaskEdit;
    edpass: TJvMaskEdit;
    JvMaskEdit1: TJvMaskEdit;
    edwsdl: TJvMaskEdit;
    tbRegras: TTabSheet;
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    JvDBUltimGrid1: TJvDBUltimGrid;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    edFaixaF: TJvCalcEdit;
    edFaixaI: TJvCalcEdit;
    cb1: TJvCheckBox;
    cb2: TJvCheckBox;
    cb3: TJvCheckBox;
    cb4: TJvCheckBox;
    cb5: TJvCheckBox;
    cb6: TJvCheckBox;
    DBGrid1: TJvDBUltimGrid;
    Panel5: TPanel;
    DBNavigator1: TDBNavigator;
    btnInserirI: TBitBtn;
    btnsalvarI: TBitBtn;
    btnAlterarI: TBitBtn;
    btnExcluirI: TBitBtn;
    btnCancelarI: TBitBtn;
    Panel2: TPanel;
    DBNavigator2: TDBNavigator;
    btnInserirR: TBitBtn;
    btnSalvarR: TBitBtn;
    btnAlterarR: TBitBtn;
    btnExcluirR: TBitBtn;
    btnCancelarR: TBitBtn;
    Panel3: TPanel;
    cbCliente: TComboBox;
    cbPerfil: TComboBox;
    QCliente: TADOQuery;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNM_CLIENTE: TStringField;
    Panel6: TPanel;
    Label1: TLabel;
    QPGRItemRJ: TStringField;
    cbRJ: TJvCheckBox;
    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNovoClick(Sender: TObject);
    procedure QCadSeguradoraAfterScroll(DataSet: TDataSet);
    procedure QCadSeguradoraBeforeOpen(DataSet: TDataSet);
    procedure btnDctoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnInserirIClick(Sender: TObject);
    procedure Limpadados;
procedure Restaura;
    procedure RestauraI;
    procedure btnInserirRClick(Sender: TObject);
    procedure btnSalvarRClick(Sender: TObject);
    procedure btnExcluirRClick(Sender: TObject);
    procedure btnCancelarRClick(Sender: TObject);
    procedure btnsalvarIClick(Sender: TObject);
    procedure edFaixaFExit(Sender: TObject);
    procedure btnAlterarIClick(Sender: TObject);
    procedure btnExcluirIClick(Sender: TObject);
    procedure btnCancelarIClick(Sender: TObject);
    procedure tbRegrasShow(Sender: TObject);
    procedure ConsultaShow(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QPGRAfterScroll(DataSet: TDataSet);
    procedure btnAlterarRClick(Sender: TObject);
  private

  public

  end;

var
  frmCadSeguradora: TfrmCadSeguradora;
  tipo: string;

implementation

uses dados, funcoes, Menu;

{$R *.dfm}

procedure TfrmCadSeguradora.btnAlterarIClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  tipo := 'A';
  edFaixaI.value := QPGRItemFAIXAI.value;
  edFaixaF.value := QPGRItemFAIXAF.value;
  if QPGRItemPERFIL1.Value = 'S' then
    cb1.Checked := true
  else
    cb1.Checked := false;
  if QPGRItemPERFIL2.Value = 'S' then
    cb2.Checked := true
  else
    cb2.Checked := false;
  if QPGRItemPERFIL3.Value = 'S' then
    cb3.Checked := true
  else
    cb3.Checked := false;
  if QPGRItemPERFIL4.Value = 'S' then
    cb4.Checked := true
  else
    cb4.Checked := false;
  if QPGRItemPERFIL5.Value = 'S' then
    cb5.Checked := true
  else
    cb5.Checked := false;
  if QPGRItemPERFIL6.Value = 'S' then
    cb6.Checked := true
  else
    cb6.Checked := false;
  if QPGRItemRJ.Value = 'S' then
    cbrj.Checked := true
  else
    cbrj.Checked := false;
  RestauraI;
  edfaixaI.SetFocus;
end;

procedure TfrmCadSeguradora.btnAlterarRClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;
  QCliente.Close;
  QCliente.Open;
  cbcliente.clear;
  cbcliente.Items.add('');
  while not QCliente.Eof do
  begin
    cbcliente.Items.add(QClienteNM_CLIENTE.value);
    QCliente.Next;
  end;
  cbCliente.ItemIndex :=  cbCliente.Items.IndexOf(QPGRRAZSOC.AsString);
  cbPerfil.ItemIndex := cbPerfil.Items.IndexOf(QPGRDESTINO.AsString);
  Restaura;
 // Limpadados;
  tipo := 'A';
  Panel1.Visible := false;
  cbCliente.SetFocus;
end;

procedure TfrmCadSeguradora.btnCancelarIClick(Sender: TObject);
begin
  QPGRItem.Cancel;
  RestauraI;
end;

procedure TfrmCadSeguradora.btnCancelarRClick(Sender: TObject);
begin
  Restaura;
  tipo := '';
  QCliente.Close;
  QCliente.SQL [2] := '';
  QCliente.SQL [3] := 'where situac = ''A'' and c.fl_status = ''S'' ';
  QCliente.Open;
end;

procedure TfrmCadSeguradora.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo: string;
begin
  caminho := GLBSeguradora;
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      CopyFile(Pchar(ori), Pchar(des), True);

      arquivo := caminho + QCadSeguradoraCODIGO.AsString +
        ExtractFileName(OpenDialog.FileName);

      RenameFile(des, arquivo);

      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('update tb_seguradora set doc_seguradora = :0 where codigo = :1');
      dtmdados.IQuery1.Parameters[0].Value := QCadSeguradoraCODIGO.AsString +
        ExtractFileName(OpenDialog.FileName);
      dtmdados.IQuery1.Parameters[1].Value := QCadSeguradoraCODIGO.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadSeguradora.btnExcluirIClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Este Item?',
    'Apagar Item da Tabela', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QPGRItem.Delete;
  end;
end;

procedure TfrmCadSeguradora.btnExcluirRClick(Sender: TObject);
begin
  if not dtmDados.PodeApagar(name) then
    Exit;
  if Application.Messagebox('Voc� Deseja Apagar Esta Regra PGR?', 'Apagar Regra PGR',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_regra_pgr_item where id_regra = :cn');
    dtmDados.iQuery1.Parameters[0].value := QPGRID_REGRA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    dtmDados.iQuery1.sql.add
      ('delete from tb_regra_pgr where id_regra = :cn');
    dtmDados.iQuery1.Parameters[0].value := QPGRID_REGRA.AsInteger;
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;
    QPgr.close;
    QPgr.open;
  end;
end;

procedure TfrmCadSeguradora.btnFecharClick(Sender: TObject);
begin
  close;
end;

procedure TfrmCadSeguradora.btnInserirIClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;

  tipo := 'I';
  RestauraI;
  edFaixaI.SetFocus;
  edFaixaF.Text := '';
  edFaixaI.Text := '';
  cb1.Checked := false;
  cb2.Checked := false;
  cb3.Checked := false;
  cb4.Checked := false;
  cb5.Checked := false;
  cb6.Checked := false;
  cbRJ.Checked := false;
end;

procedure TfrmCadSeguradora.Limpadados;
begin
  cbcLIENTE.ItemIndex := -1;
  cbPerfil.ItemIndex := -1;
end;

procedure TfrmCadSeguradora.btnInserirRClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  QCliente.Close;
  QCliente.SQL [2] := 'left join tb_regra_pgr r on r.cod_cliente = f.codclifor';
  QCliente.SQL [3] := 'where situac = ''A'' and c.fl_status = ''S'' and r.id_regra is null';
  QCliente.Open;
  cbcliente.clear;
  cbcliente.Items.add('');
  while not QCliente.Eof do
  begin
    cbcliente.Items.add(QClienteNM_CLIENTE.value);
    QCliente.Next;
  end;
  Restaura;
  Limpadados;
  tipo := 'I';
  Panel1.Visible := false;
  cbCliente.SetFocus;
end;

procedure TfrmCadSeguradora.btnNovoClick(Sender: TObject);
begin
  tipo := 'I';
  QCadSeguradora.Append;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := True;
  JvPageControl1.ActivePage := tbInfo;
  EDCnpj.SetFocus;
end;

procedure TfrmCadSeguradora.btnSalvarClick(Sender: TObject);
begin
  if EdNome.text = '' then
  begin
    ShowMessage('N�o Foi cadastrado o Nome da Seguradora ');
    EdNome.SetFocus;
  end;
  if EdApolice.text = '' then
  begin
    ShowMessage('N�o Foi cadastrado o N�mero da Ap�lice ');
    EdApolice.SetFocus;
  end;

  if alltrim(ApCarac(dtI.text)) = '' then
  begin
    ShowMessage('N�o Foi cadastrado a data Inicial do Contrato ');
    dtI.SetFocus;
  end;

  if alltrim(ApCarac(dtF.text)) = '' then
  begin
    ShowMessage('N�o Foi cadastrado a data Final do Contrato ');
    dtF.SetFocus;
  end;

  if tipo = 'I' then
  begin
    dtmdados.IQuery1.close;
    dtmdados.IQuery1.SQL.Clear;
    dtmdados.IQuery1.SQL.add
      ('select max(coalesce(codigo,1))+1 numero from tb_seguradora ');
    dtmdados.IQuery1.open;
    QCadSeguradoraCODIGO.Value := dtmdados.IQuery1.FieldByName('numero')
      .AsInteger;
    dtmdados.IQuery1.close;
  end;
  QCadSeguradoraCNPJ.Value := EDCnpj.text;
  QCadSeguradoraAPOLICE.Value := EdApolice.text;
  QCadSeguradoraSEGURADORA.Value := EdNome.text;
  QCadSeguradoraCONTATO.Value := edContato.text;
  QCadSeguradoraTELEFONE.Value := EdFone.text;
  QCadSeguradoraDATAINICIO.Value := dtI.date;
  QCadSeguradoraDATAFIM.Value := dtF.date;
  QCadSeguradoraUSER_WS.Value := eduser.text;
  QCadSeguradoraPASS_WS.Value := edpass.text;
  QCadSeguradoraCODE_WS.Value := edcod.text;
  QCadSeguradoraFL_EMPRESA.Value := GLBFilial;
  QCadSeguradorawsdl.Value := edwsdl.text;
  try
    QCadSeguradora.post;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;

  btnAlterar.Enabled := True;
  btnSalvar.Enabled := false;
  tipo := '';
  JvPageControl1.ActivePageIndex := 0;
end;

procedure TfrmCadSeguradora.btnsalvarIClick(Sender: TObject);
var cod: integer;
begin

  // Valida se as informa��es j� contem no range
  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add('select fnc_tem_range_regra(:0,:1,:2,:3) tem from dual');
  dtmDados.iQuery1.Parameters.ParamByName('0').value := QPGRID_REGRA.AsInteger;
  dtmDados.iQuery1.Parameters.ParamByName('1').value := edFaixaI.Value;
  dtmDados.iQuery1.Parameters.ParamByName('2').value := edFaixaF.Value;
  if tipo = 'I' then
    dtmDados.iQuery1.Parameters.ParamByName('3').value := 0
  else
    dtmDados.iQuery1.Parameters.ParamByName('3').value := QPGRItemID_REGRA_I.AsInteger;
  dtmDados.iQuery1.Open;
  if dtmdados.IQuery1.FieldByName('tem').value > 0 then
  begin
    ShowMessage
      ('Para as informa��es de Faixa digitadas existe um conflito com uma j� cadastrada !!');
    Exit;
  end;
  dtmDados.iQuery1.Close;

  try
    dtmDados.iQuery1.Close;
    dtmDados.iQuery1.sql.clear;
    if tipo = 'I' then
    begin
      dtmDados.iQuery1.sql.add('insert into tb_regra_pgr_item (id_regra, ' +
        'faixai, faixaf, perfil1, perfil2, perfil3, perfil4, perfil5, perfil6, ' +
        'user_inc, rj ) '
        + 'values (:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, :10)');
      dtmDados.iQuery1.Parameters.ParamByName('0').value :=
        QPGRID_REGRA.AsInteger;
    end
    else
    begin
      cod := QPGRItemID_REGRA.AsInteger;
      dtmDados.iQuery1.sql.add('update tb_regra_pgr_item set faixai = :1, ' +
        'faixaf = :2, perfil1 = :3, perfil2 = :4, perfil3 =:5, perfil4 = :6, ' +
        'perfil5 = :7, perfil6 = :8, dt_alt = sysdate, user_alt = :9, rj = :10 ' +
        'where id_regra_i = :11');
      dtmDados.iQuery1.Parameters.ParamByName('11').value :=
        QPGRItemID_REGRA_I.AsInteger;
    end;
    dtmDados.iQuery1.Parameters.ParamByName('1').value := edFaixaI.value;
    dtmDados.iQuery1.Parameters.ParamByName('2').value := edFaixaF.value;
    if cb1.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('3').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('3').value := 'N';
    if cb2.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('4').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('4').value := 'N';
    if cb3.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('5').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('5').value := 'N';
    if cb4.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('6').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('6').value := 'N';
    if cb5.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('7').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('7').value := 'N';
    if cb6.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('8').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('8').value := 'N';
    dtmDados.iQuery1.Parameters.ParamByName('9').value := glbcoduser;
    if cbrj.Checked = true then
      dtmDados.iQuery1.Parameters.ParamByName('10').value := 'S'
    else
      dtmDados.iQuery1.Parameters.ParamByName('10').value := 'N';
    dtmDados.iQuery1.ExecSQL;
    dtmDados.iQuery1.Close;

  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QPGRItem.Close;
  QPGRItem.Parameters[0].value := QPGRID_REGRA.AsInteger;
  QPGRItem.Open;
  if tipo = 'A' then
    QPGRItem.Locate('ID_REGRA_I', cod, [])
  else
    QPGRItem.Last;
  tipo := '';
  RestauraI;
end;

procedure TfrmCadSeguradora.btnSalvarRClick(Sender: TObject);
var new, codcliente : Integer;
begin
  if cbCliente.text = '' then
  begin
    ShowMessage('N�o Foi cadastrado o Nome do Cliente ');
    cbCliente.SetFocus;
  end;
  if cbPerfil.text = '' then
  begin
    ShowMessage('N�o Foi cadastrado o Tipo de Perfil ');
    cbPerfil.SetFocus;
  end;
  try
    // pega c�digo do cliente
    QCliente.Open;
    QCliente.Locate('nm_cliente',cbCliente.Text,[]);
    codcliente := QClienteCOD_CLIENTE.AsInteger;
    if tipo = 'I' then
    begin
      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add
        ('select max(coalesce(id_regra,1))+1 numero from tb_regra_pgr ');
      dtmdados.IQuery1.open;
      new := dtmdados.IQuery1.FieldByName('numero').AsInteger;
      if new = 0 then
        new := 1;

      dtmdados.IQuery1.close;
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add('insert into tb_regra_pgr (id_regra, cod_cliente, destino, user_inc)');
      dtmdados.IQuery1.SQL.add('values (:0, :1, :2, :3)' );
      dtmdados.IQuery1.Parameters[0].Value := new;
      dtmdados.IQuery1.Parameters[1].Value := codcliente;
      dtmdados.IQuery1.Parameters[2].Value := cbPerfil.Text;
      dtmdados.IQuery1.Parameters[3].Value := glbcoduser;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;
    end
    else
    begin
      dtmdados.IQuery1.SQL.Clear;
      dtmdados.IQuery1.SQL.add('update tb_regra_pgr set cod_cliente = :0, destino = :1, ');
      dtmdados.IQuery1.SQL.add('dt_alt = sysdate, user_alt = :2 where id_regra = :3');
      dtmdados.IQuery1.Parameters[0].Value := codcliente;
      dtmdados.IQuery1.Parameters[1].Value := cbPerfil.Text;
      dtmdados.IQuery1.Parameters[2].Value := glbcoduser;
      dtmdados.IQuery1.Parameters[3].Value := QPGRID_REGRA.AsInteger;
      dtmdados.IQuery1.ExecSQL;
      dtmdados.IQuery1.close;
    end;
  except
    on e: Exception do
    begin
      ShowMessage(e.message);
    end;
  end;
  QPGR.Close;
  QPGR.Open;
  QCliente.Close;
  QCliente.SQL [2] := '';
  QCliente.SQL [3] := 'where situac = ''A'' and c.fl_status = ''S'' ';
  QCliente.Open;
  Restaura;
  tipo := '';
end;

procedure TfrmCadSeguradora.btnVerClick(Sender: TObject);
var
  caminho: string;
begin
  if QCadSeguradoraDOC_SEGURADORA.AsString <> '' then
  begin
    caminho := GLBSeguradora + QCadSeguradoraDOC_SEGURADORA.AsString;
    ShellExecute(Handle, nil, Pchar(caminho), nil, nil, SW_SHOWNORMAL);
  end
  else
    ShowMessage('Cadastro Sem Documenta��o Vinculada !!');
end;

procedure TfrmCadSeguradora.ConsultaShow(Sender: TObject);
begin
  QPGR.Open
end;

procedure TfrmCadSeguradora.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.Field = QPGRItemPERFIL1 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPerfil1.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QPGRItemPERFIL2 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPERFIL2.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;

  if Column.Field = QPGRItemPERFIL3 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPerfil3.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QPGRItemPERFIL4 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPERFIL4.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;

  if Column.Field = QPGRItemPERFIL5 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPerfil5.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QPGRItemPERFIL6 then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemPERFIL6.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
  if Column.Field = QPGRItemRJ then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
    if QPGRItemRJ.value = 'S' then
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 1)
    else
      dtmdados.iml.Draw(DBGrid1.Canvas, Rect.Left + 10, Rect.Top + 1, 0);
  end;
end;

procedure TfrmCadSeguradora.edFaixaFExit(Sender: TObject);
begin
  if edFaixaF.value < edFaixaI.value then
  begin
    ShowMessage('A Faixa Final N�o pode ser Menor que a Faixa Inicial');
    edFaixaF.SetFocus;
  end;
end;

procedure TfrmCadSeguradora.btnAlterarClick(Sender: TObject);
begin
  tipo := 'A';
  QCadSeguradora.Edit;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := True;
  JvPageControl1.ActivePage := tbInfo;
end;

procedure TfrmCadSeguradora.FormCreate(Sender: TObject);
begin
  JvPageControl1.ActivePageIndex := 0;
  QCadSeguradora.open;
  EdApolice.text := QCadSeguradoraAPOLICE.Value;
  EDCnpj.text := QCadSeguradoraCNPJ.Value;
  edContato.text := QCadSeguradoraCONTATO.Value;
  EdFone.text := QCadSeguradoraTELEFONE.Value;
  dtI.date := QCadSeguradoraDATAINICIO.Value;
  dtF.date := QCadSeguradoraDATAFIM.Value;
  EdNome.text := QCadSeguradoraSEGURADORA.Value;
  edcod.text := QCadSeguradoraCODE_WS.Value;
  eduser.text := QCadSeguradoraUSER_WS.Value;
  edpass.text := QCadSeguradoraPASS_WS.Value;
end;

procedure TfrmCadSeguradora.QCadSeguradoraAfterScroll(DataSet: TDataSet);
begin
  EdApolice.text := QCadSeguradoraAPOLICE.Value;
  EDCnpj.text := QCadSeguradoraCNPJ.Value;
  edContato.text := QCadSeguradoraCONTATO.Value;
  EdFone.text := QCadSeguradoraTELEFONE.Value;
  dtI.date := QCadSeguradoraDATAINICIO.Value;
  dtF.date := QCadSeguradoraDATAFIM.Value;
  edcod.text := QCadSeguradoraCODE_WS.Value;
  eduser.text := QCadSeguradoraUSER_WS.Value;
  edpass.text := QCadSeguradoraPASS_WS.Value;
  edwsdl.text := QCadSeguradorawsdl.Value;
end;

procedure TfrmCadSeguradora.QCadSeguradoraBeforeOpen(DataSet: TDataSet);
begin
  QCadSeguradora.Parameters[0].Value := GLBFilial;
end;

procedure TfrmCadSeguradora.QPGRAfterScroll(DataSet: TDataSet);
begin
  QPGRItem.Close;
  QPGRItem.Parameters[0].Value := QPGRID_REGRA.AsInteger;
  QPGRItem.Open;
  Label1.Caption := 'Cliente : ' + QPGRRAZSOC.AsString + ' - Destino : '
    + QPGRDESTINO.AsString;
end;

procedure TfrmCadSeguradora.Restaura;
begin
  btnInserirR.Enabled := not btnInserirR.Enabled;
  btnAlterarR.Enabled := not btnAlterarR.Enabled;
  btnExcluirR.enabled  := not btnExcluirR.enabled;
  btnSalvarR.Enabled := not btnSalvarR.Enabled;
  btnCancelarR.Enabled := not btnCancelarR.Enabled;
  btnFechar.Enabled := not btnFechar.Enabled;
  if Panel3.Visible = true then
    Panel3.Visible := false
  else
    Panel3.Visible := true;

end;

procedure TfrmCadSeguradora.RestauraI;
begin
  btnInserirI.Enabled := not btnInserirI.Enabled;
  btnAlterarI.Enabled := not btnAlterarI.Enabled;
  btnExcluirI.Enabled := not btnExcluirI.Enabled;
  btnsalvarI.Enabled := not btnsalvarI.Enabled;
  btnCancelarI.Enabled := not btnCancelarI.Enabled;
  if Panel4.Visible = true then
    Panel4.Visible := false
  else
    Panel4.Visible := true;
end;

procedure TfrmCadSeguradora.tbRegrasShow(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmCadSeguradora.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QCadSeguradora.close;
end;

end.
