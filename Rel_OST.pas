unit Rel_OST;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  Grids, DBGrids, JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBLookup, ExcelXP,
  JvExDBGrids, JvDBGrid, DBCtrls, Inifiles, Gauges, ComObj, JvMaskEdit,
  JvDBControls, jpeg, JvRichEdit, JvDBRichEdit, RLReport,
  JvMemoryDataset, pcnConversao, ShellApi;

type
  TfrmRel_OST = class(TForm)
    QOST: TADOQuery;
    navnavig: TDataSource;
    Panel2: TPanel;
    JvDBGrid3: TJvDBGrid;
    dtsItens: TDataSource;
    Panel3: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btnConsulta: TBitBtn;
    QItens: TADOQuery;
    RLDBText1: TRLDBText;
    lbl1: TLabel;
    QItensNOTFIS: TStringField;
    QItensQUANTI: TBCDField;
    QItensPESOKG: TBCDField;
    QItensVLRMER: TBCDField;
    QItensDATNOT: TDateTimeField;
    QOSTCODIGO: TBCDField;
    QOSTCODFIL: TBCDField;
    QOSTDATINC: TDateTimeField;
    QOSTCODPAG: TBCDField;
    QOSTCODDES: TBCDField;
    QOSTNOMEAB: TStringField;
    QOSTDESTINO: TStringField;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    JvDBGrid1: TJvDBGrid;
    DBNavigator1: TDBNavigator;
    Gauge1: TGauge;
    QOSTTOTFRE: TBCDField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure btnConsultaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnFecharCtrcClick(Sender: TObject);
    procedure QOSTAfterScroll(DataSet: TDataSet);
    procedure QOSTBeforeOpen(DataSet: TDataSet);
  private

  public
    { Public declarations }
  end;

var
  frmRel_OST: TfrmRel_OST;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmRel_OST.BitBtn1Click(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
  wb: Variant;
  i: integer;
begin
  memo1 := TStringList.Create;
  texto := 'Relat�rio de Manifestos';

  memo1.Clear();
  memo1.Add('<HTML><HEAD><TITLE>Relat�rio de OST sem NF</TITLE>');
  memo1.Add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.Add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.Add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add('</STYLE>');
  memo1.Add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.Add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo1.Add('<table border=0><title></title></head><body>');
  memo1.Add('<table border=1 align=center>');
  memo1.Add('<tr>');
  memo1.Add('<th colspan=2><FONT class=titulo1>' + texto + '</th></font>');
  memo1.Add('<tr>');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to JvDBGrid1.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + JvDBGrid1.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QOST.First;
  Gauge1.MaxValue := QOST.RecordCount;
  while not QOST.Eof do
  begin
    memo1.Add('<tr>');
    for i := 0 to JvDBGrid1.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QOST.fieldbyname(JvDBGrid1.Columns
        [i].FieldName).AsString + '</th>');
    QOST.Next;
    Gauge1.AddProgress(1);
    memo1.Add('</tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile('C:/SIM/Rel_OST.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := true;
  wb.Navigate('C:/SIM/Rel_OST.html');
end;

procedure TfrmRel_OST.btnConsultaClick(Sender: TObject);
begin
  lbl1.Caption := '';
  if copy(dtInicial.Text, 1, 2) <> '  ' then
  begin
    if dtFinal.date < dtInicial.date then
    begin
      ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
      exit;
    end;
  end;
  Application.ProcessMessages;

  QOST.close;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QOST.SQL[4] := 'and o.datinc between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'
  else
    QOST.SQL[4] := '';

  QOST.open;

  dtmdados.iQuery1.close;
  dtmdados.iQuery1.SQL.Clear;
  dtmdados.iQuery1.SQL.Add
    ('select sum(totfre) total from cyber.rodord o where o.situac <> ''C'' and o.codnot is null and o.codfil = :0 ');
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    dtmdados.iQuery1.SQL.Add('and datinc between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')');
  dtmdados.iQuery1.Parameters[0].Value := GLBFilial;
  dtmdados.iQuery1.open;

  lbl1.Caption := 'Total : ' + FormatFloat('###,##0.00',
    dtmdados.iQuery1.fieldbyname('total').Value);

end;

procedure TfrmRel_OST.btnFecharCtrcClick(Sender: TObject);
begin
  QOST.close;
  QItens.close;
  Panel2.Visible := false;
end;

procedure TfrmRel_OST.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QOST.close;
  QItens.close;
end;

procedure TfrmRel_OST.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QOST.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].FieldName,
    Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmRel_OST.QOSTAfterScroll(DataSet: TDataSet);
begin
  QItens.close;
  QItens.Parameters[0].Value := QOSTCODIGO.AsInteger;
  QItens.Parameters[1].Value := QOSTCODFIL.AsString;
  QItens.open;
end;

procedure TfrmRel_OST.QOSTBeforeOpen(DataSet: TDataSet);
begin
  QOST.Parameters[0].Value := GLBFilial;
end;

end.
