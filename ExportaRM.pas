unit ExportaRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, FileCtrl, ExtCtrls, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, DBCtrls, JvDBLookup, Buttons, Mask, JvExMask, JvToolEdit,
  Gauges, JvDBControls, ACBrBase, ACBrMail;

type
  TfrmExportaRM = class(TForm)
    Label10: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Gauge1: TGauge;
    Label8: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    ledIdCliente: TJvDBLookupEdit;
    ledCliente: TJvDBLookupEdit;
    JvDBGrid1: TJvDBGrid;
    Panel1: TPanel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    Memo1: TMemo;
    btnExportar: TBitBtn;
    QCtrc: TADOQuery;
    QCtrcNM_CLI_OS: TStringField;
    QCtrcNR_CONHECIMENTO: TBCDField;
    QCtrcVL_TOTAL_IMPRESSO: TBCDField;
    QCtrcDT_CONHECIMENTO: TDateTimeField;
    QCtrcFL_TIPO: TStringField;
    QCtrcVL_DECLARADO_NF: TBCDField;
    QCtrcVL_FRETE: TBCDField;
    QCtrcVL_AD_TERRESTRE: TBCDField;
    QCtrcVL_OUTROS: TBCDField;
    QCtrcVL_ALIQUOTA: TFloatField;
    QCtrcVL_IMPOSTO: TFloatField;
    QCtrcVL_PEDAGIO: TBCDField;
    QCtrcNR_CNPJ_CLI: TStringField;
    navnavig: TDataSource;
    QCliente: TADOQuery;
    QClienteNM_CLIENTE: TStringField;
    QClienteCOD_CLIENTE: TBCDField;
    QClienteNR_CNPJ_CPF: TStringField;
    DataSource1: TDataSource;
    QCtrcNR_SERIE: TStringField;
    QCtrcDS_PLACA: TStringField;
    QCtrcNR_QTDE_VOL: TBCDField;
    QCtrcDS_UF_ORIGEM: TStringField;
    QCtrcCHAVECTE: TStringField;
    QCtrcPIS: TBCDField;
    QCtrcCOFINS: TBCDField;
    QCtrcCODMUNINI: TStringField;
    QCtrcUFINI: TStringField;
    QCtrcCODMUNDES: TStringField;
    QCtrcUFFIM: TStringField;
    ACBrMail1: TACBrMail;
    edEmail: TEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ledIdClienteCloseUp(Sender: TObject);
    procedure ledClienteCloseUp(Sender: TObject);
    procedure JvDBGrid1TitleClick(Column: TColumn);
    procedure btnExportarClick(Sender: TObject);
  private
    { Private declarations }
    function Preenche(s: String; qtd: integer): String;
    function PreencheE(s: String; qtd: integer): String;
    function BuscaTrocaValor(sText: string): string;
  public
    { Public declarations }
  end;

var
  frmExportaRM: TfrmExportaRM;

implementation

uses Dados, Menu, funcoes;

{$R *.dfm}

procedure TfrmExportaRM.btnExportarClick(Sender: TObject);
var
  Info: TextFile;
  sArquivo, icms: String;
  mail: TStringList;
begin
  if QCtrc.IsEmpty then
    ShowMessage('N�o existem dados para exportar !!')
  else
  begin
    sArquivo := DirectoryListBox1.Directory + '\' + 'RM_' +
      FormatDateTime('dd-mm-yyyy', Now) + '.txt';
    AssignFile(Info, sArquivo);
    if not FileExists(sArquivo) then
      Rewrite(Info)
    else
      Rewrite(Info);
    QCtrc.First;
    while not QCtrc.Eof do
    begin
      if QCtrcVL_ALIQUOTA.Value > 0 then
        icms := '1.352.0001           '
      else
        icms := '1.352.0002           ';

      // bloco 000
      Write(Info, 'M'); // 0
      Write(Info, '00001'); // 1
      Write(Info, Replicate(' ', 30)); // 2
      Write(Info, '01.001         '); // 3
      Write(Info, Replicate(' ', 15)); // 4
      Write(Info, Replicate(' ', 15)); // 5
      Write(Info, '000959                   '); // 6
      Write(Info, Replicate(' ', 25)); // 7
      Write(Info, Preenche(RetZero(QCtrcNR_CONHECIMENTO.AsString, 9), 35)); // 8
      Write(Info, Preenche(QCtrcNR_SERIE.AsString, 8)); // 9
      Write(Info, '1.2.24    '); // 10
      Write(Info, 'S'); // 11
      Write(Info, 'A'); // 12
      Write(Info, '1'); // 13
      Write(Info, '1'); // 14
      Write(Info, '1'); // 15

      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 16
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 17
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 18
      Write(Info, FormatDateTime('dd/mm/yyyy', Now)); // 19
      Write(Info, Replicate(' ', 15)); // 20
      Write(Info, '00000000000000000.00'); // 21
      Write(Info, '                    '); // 22
      Write(Info, '001  '); // 23   Condi��o de pagamento
      Write(Info, '00001'); // 24
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 25
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 26
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 27
      Write(Info, Replicate(' ', 60)); // 28

      Write(Info, '00000000000000000.00'); // 29
      Write(Info, '00000000000000000.00'); // 30
      Write(Info, '00000000000000000.00'); // 31
      Write(Info, '00000000000000000.00'); // 32
      Write(Info, '00000000000000000.00'); // 33
      Write(Info, '00000000000000000.00'); // 34
      Write(Info, '00000000000000000.00'); // 35
      Write(Info, '00000000000000000.00'); // 36
      Write(Info, '00000000000000000.00'); // 37
      Write(Info, '00000000000000000.00'); // 38
      Write(Info, '00000000000000000.00'); // 39
      Write(Info, '00000000000000000.00'); // 40

      Write(Info, Replicate(' ', 16)); // 41
      Write(Info, '00000000000000000.00'); // 42
      Write(Info, Replicate(' ', 5)); // 43
      Write(Info, Replicate(' ', 5)); // 44
      Write(Info, Replicate(' ', 15)); // 45
      Write(Info, Preenche(Trim(QCtrcDS_PLACA.AsString), 10)); // 46
      Write(Info, '  '); // 47

      Write(Info, '00000000000000000.00'); // 48
      Write(Info, '00000000000000000.00'); // 49

      Write(Info, Replicate(' ', 10)); // 50
      Write(Info, '0000000000'); // 51
      Write(Info, '          '); // 52

      Write(Info, Replicate(' ', 15)); // 53
      Write(Info, Replicate(' ', 10)); // 54
      Write(Info, Replicate(' ', 10)); // 55
      Write(Info, Replicate(' ', 10)); // 56
      Write(Info, Replicate(' ', 10)); // 57
      Write(Info, Replicate(' ', 10)); // 58
      Write(Info, Replicate(' ', 25)); // 59
      Write(Info, Replicate(' ', 25)); // 60
      Write(Info, Replicate(' ', 25)); // 61
      Write(Info, Replicate(' ', 25)); // 62
      Write(Info, Replicate(' ', 25)); // 63
      Write(Info, Replicate(' ', 100)); // 64
      Write(Info, Replicate(' ', 100)); // 65
      Write(Info, Replicate(' ', 100)); // 66
      Write(Info, Preenche('R$', 10)); // 67
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 68
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 69

      Write(Info, '00000'); // 70
      Write(Info, '00000'); // 71
      Write(Info, '00000'); // 72

      Write(Info, Replicate(' ', 16)); // 73
      Write(Info, Replicate(' ', 20)); // 74

      Write(Info, '00000'); // 75
      Write(Info, ' '); // 76
      Write(Info, '     '); // 77
      Write(Info, '     '); // 78
      Write(Info, '     '); // 79
      Write(Info, '00000'); // 80
      Write(Info, '00000'); // 81
      Write(Info, '     '); // 82
      Write(Info, '00000'); // 83
      Write(Info, '00000'); // 84
      Write(Info, Replicate(' ', 16)); // 85
      Write(Info, Replicate(' ', 16)); // 86
      Write(Info, '00000000000000000.00'); // 87
      Write(Info, '     '); // 88
      Write(Info, '00000000000000000.00'); // 89

      Write(Info, '     '); // 90
      Write(Info, '     '); // 91
      Write(Info, '     '); // 92
      Write(Info, '     '); // 93
      Write(Info, ' '); // 94
      Write(Info, '          '); // 95
      Write(Info, Replicate(' ', 20)); // 96
      Write(Info, Replicate(' ', 25)); // 97
      Write(Info, Replicate(' ', 5)); // 98
      Write(Info, Replicate(' ', 10)); // 99
      Write(Info, '00001'); // 100
      Write(Info, Replicate(' ', 25)); // 101
      Write(Info, '00001'); // 102
      Write(Info, '00000'); // 103
      Write(Info, Replicate(' ', 10)); // 104
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 105
      Write(Info, Replicate('0', 10)); // 106
      Write(Info, Replicate('0', 10)); // 107
      Write(Info, Replicate(' ', 20)); // 108
      Write(Info, '00001'); // 109
      Write(Info, icms); // 110
      Write(Info, Replicate(' ', 20)); // 111
      Write(Info, Replicate(' ', 19)); // 112
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 113
      Write(Info, Replicate(' ', 15)); // 114
      Write(Info, Replicate(' ', 20)); // 115
      Write(Info, QCtrcDS_UF_ORIGEM.AsString); // 116
      Write(Info, Replicate(' ', 10)); // 117
      Write(Info, Replicate(' ', 10)); // 118
      Write(Info, RetBran('CTE', 10)); // 119
      Write(Info, Replicate(' ', 10)); // 120
      Write(Info, '00000000000000000.00'); // 121
      Write(Info, '00000000000000000.00'); // 122
      Write(Info, '00000000000000000.00'); // 123
      Write(Info, Replicate(' ', 24)); // 124
      Write(Info, Replicate('0', 5)); // 125
      Write(Info, Replicate(' ', 8)); // 126
      Write(Info, Replicate(' ', 10)); // 127
      Write(Info, '00000000000000000.00'); // 128
      Write(Info, Replicate('0', 5)); // 129
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 130
      Write(Info, Replicate('0', 10)); // 131
      Write(Info, Replicate('0', 10)); // 132
      Write(Info, Replicate('0', 10)); // 133
      Write(Info, Replicate(' ', 20)); // 134
      Write(Info, Replicate(' ', 15)); // 135
      Write(Info, Replicate(' ', 1)); // 136
      Write(Info, Replicate('0', 5)); // 137
      Write(Info, Replicate('0', 5)); // 138
      Write(Info, Replicate(' ', 30)); // 139
      Write(Info, QCtrcCHAVECTE.AsString); // 140
      Writeln(Info, Replicate('0', 5)); // 141

      // DETALHE
      Write(Info, 'I'); // 0
      Write(Info, Preenche('FRETE', 30)); // 1
      Write(Info, '00001'); // 2
      Write(Info, Replicate(' ', 10)); // 3
      Write(Info, '00000000000000001.00'); // 4
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 5
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 6
      Write(Info, '00000000000000000.00'); // 7
      Write(Info, '00000000000000000.00'); // 8
      Write(Info, '00000000000000000.00'); // 9
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_PEDAGIO.AsString), 20));
      // 10
      // Write(Info,'00000000000000000.00'); // 10
      Write(Info, FormatDateTime('dd/mm/yyyy',
        QCtrcDT_CONHECIMENTO.AsDateTime)); // 11
      Write(Info, '     '); // 12
      Write(Info, '00001'); // 13
      Write(Info, Replicate(' ', 10)); // 14
      Write(Info, Replicate(' ', 10)); // 15
      Write(Info, Replicate(' ', 10)); // 16
      Write(Info, Replicate(' ', 10)); // 17
      Write(Info, Replicate(' ', 10)); // 18
      Write(Info, Replicate(' ', 25)); // 19
      Write(Info, Replicate(' ', 25)); // 20
      Write(Info, Replicate(' ', 25)); // 21
      Write(Info, Replicate(' ', 25)); // 22
      Write(Info, Replicate(' ', 25)); // 23
      Write(Info, Replicate(' ', 15)); // 24
      Write(Info, 'UN   '); // 25
      Write(Info, '00000000000000001.00'); // 26
      Write(Info, '     '); // 27
      Write(Info, '     '); // 28
      Write(Info, '          '); // 29
      Write(Info, Replicate(' ', 15)); // 30
      Write(Info, '          '); // 31
      Write(Info, '00000'); // 32
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 33
      Write(Info, Replicate(' ', 25)); // 34
      Write(Info, icms); // 35
      Write(Info, Replicate(' ', 14)); // 36
      Write(Info, Replicate(' ', 30)); // 37
      Write(Info, Replicate(' ', 25)); // 38
      Write(Info, Replicate(' ', 10)); // 39
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 40
      Write(Info, Replicate(' ', 60)); // 41
      Write(Info, '00000'); // 42
      Write(Info, '000'); // 43
      Write(Info, Replicate(' ', 60)); // 44
      Write(Info, Replicate(' ', 10)); // 45
      Write(Info, '00000000000000000.00'); // 46
      Write(Info, '00000000000000000.00'); // 47
      Write(Info, ' '); // 48
      Write(Info, '00000000000000000.00'); // 49
      Write(Info, Replicate(' ', 10)); // 50
      Write(Info, '00000000000000000.00'); // 51
      Write(Info, '00000000000000000.00'); // 52
      Write(Info, '00001'); // 53
      Write(Info, Replicate(' ', 15)); // 54
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 55
      Write(Info, Replicate(' ', 16)); // 56
      Writeln(Info, '00000000000000000.00'); // 57

      // Bloco O
      Write(Info, 'O'); // 1
      Write(Info, ' '); // 2
      Write(Info, '   '); // 3
      Write(Info, '03'); // 4
      Writeln(Info, '     '); // 5

      // Bloco T
      Write(Info, 'T'); // 0
      Write(Info, '00001'); // 1
      Write(Info, 'ICMS      '); // 2
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 3
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_ALIQUOTA.AsString), 20));
      // 4
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_IMPOSTO.AsString), 20));
      // 5
      Write(Info, '00000000000000000.00'); // 6
      Write(Info, '00000000000000000.00'); // 7
      Write(Info, '00000000000000000.00'); // 8
      Write(Info, '00000'); // 9
      Write(Info, '     '); // 10
      Write(Info, Replicate(' ', 10)); // 11
      Write(Info, Replicate(' ', 20)); // 12
      Write(Info, '00000'); // 13
      Write(Info, '3'); // 14
      Writeln(Info, '  '); // 15

      Write(Info, 'T'); // 0
      Write(Info, '00001'); // 1
      Write(Info, 'PIS       '); // 2
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 3
      Write(Info, PreencheE('1.65', 20)); // 4
      Write(Info, buscatroca(PreencheE(floattostrf(QCtrcPIS.Value, ffnumber, 20,
        2), 20), ',', '.')); // 5
      Write(Info, '00000000000000000.00'); // 6
      Write(Info, '00000000000000000.00'); // 7
      Write(Info, '00000000000000000.00'); // 8
      Write(Info, '00000'); // 9
      Write(Info, '     '); // 10
      Write(Info, Replicate(' ', 10)); // 11
      Write(Info, Replicate(' ', 20)); // 12
      Write(Info, '00000'); // 13
      Write(Info, ' '); // 14
      Writeln(Info, '  '); // 15

      Write(Info, 'T'); // 0
      Write(Info, '00001'); // 1
      Write(Info, 'COFINS    '); // 2
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcVL_TOTAL_IMPRESSO.AsString),
        20)); // 3
      Write(Info, PreencheE('7.60', 20)); // 4
      Write(Info, PreencheE(BuscaTrocaValor(QCtrcCOFINS.AsString), 20)); // 5
      Write(Info, '00000000000000000.00'); // 6
      Write(Info, '00000000000000000.00'); // 7
      Write(Info, '00000000000000000.00'); // 8
      Write(Info, '00000'); // 9
      Write(Info, '     '); // 10
      Write(Info, Replicate(' ', 10)); // 11
      Write(Info, Replicate(' ', 20)); // 12
      Write(Info, '00000'); // 13
      Write(Info, ' '); // 14
      Writeln(Info, '  '); // 15

      Write(Info, '@'); // 1
      Write(Info, QCtrcUFINI.AsString); // 2 e 3
      Write(Info, Preenche(QCtrcCODMUNINI.AsString,20)); // 4 a 23
      Write(Info, QCtrcUFFIM.AsString); // 24 e 25
      Write(Info, Preenche(QCtrcCODMUNDES.AsString,20)); // 26 a 45
      Writeln(Info, Replicate(' ', 5)); // 5

      QCtrc.next;
    end;
    closefile(Info);

    try
      ACBrMail1.AddAddress(edEmail.Text, 'CRM Intecom');
      ACBrMail1.AddAttachment(sArquivo, 'EDI', adAttachment);
      ACBrMail1.Send();
    finally

    end;
  end;
end;

procedure TfrmExportaRM.btRelatorioClick(Sender: TObject);
begin
  if copy(dtInicial.Text, 1, 2) = '  ' then
  begin
    ShowMessage('N�o foi escolhido o per�odo !!');
    dtInicial.setfocus;
    exit;
  end;
  if dtFinal.date < dtInicial.date then
  begin
    ShowMessage('A Data Final n�o pode ser menor que a inicial !!');
    dtFinal.setfocus;
    exit;
  end;
  if Trim(ledIdCliente.LookupValue) = '' then
  begin
    ShowMessage('N�o foi escolhido o Cliente !!');
    ledIdCliente.setfocus;
    exit;
  end;
  Panel1.Visible := true;
  Application.ProcessMessages;
  QCtrc.close;
  QCtrc.SQL[7] := 'and c.emissaocte between to_date(''' + dtInicial.Text +
    ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
    ' 23:59'',''dd/mm/yy hh24:mi'')';
  QCtrc.SQL[9] := 'AND cl.codclifor = ''' + ledIdCliente.LookupValue + '''';
  // QCTRC.SQL.savetofile('c:\sql.txt');
  QCtrc.open;
  Panel1.Visible := false;
  ShowMessage('Processo Finalizado !!');
end;

function TfrmExportaRM.BuscaTrocaValor(sText: string): string;
begin
  sText := buscatroca(sText, ',', '.');
  if copy(copy(sText, length(sText) - 1, 2), 1, 1) = '.' then
    sText := sText + '0';
  Result := sText;
end;

function TfrmExportaRM.Preenche(s: String; qtd: integer): String;
begin
  Result := Trim(s) + Replicate(' ', qtd - length(Trim(s)));
end;

function TfrmExportaRM.PreencheE(s: String; qtd: integer): String;
begin
  Result := Replicate(' ', qtd - length(Trim(s))) + Trim(s);
end;

procedure TfrmExportaRM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QCtrc.close;
  QCliente.close;
end;

procedure TfrmExportaRM.FormCreate(Sender: TObject);
begin
  QCliente.open;
  DriveComboBox1.Enabled := true
end;

procedure TfrmExportaRM.JvDBGrid1TitleClick(Column: TColumn);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + JvDBGrid1.Columns.Items
    [JvDBGrid1.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    exit;
  if not QCtrc.Locate(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(JvDBGrid1.Columns.Items[JvDBGrid1.SelectedIndex].Title.Caption +
      ' n�o localizado');
end;

procedure TfrmExportaRM.ledClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

procedure TfrmExportaRM.ledIdClienteCloseUp(Sender: TObject);
begin
  ledIdCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
  ledCliente.LookupValue := (Sender as TJvDBLookupEdit).LookupValue;
end;

end.
