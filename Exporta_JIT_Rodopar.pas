unit Exporta_JIT_Rodopar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, Variants, Grids, DBGrids,
  inifiles, Mask, ADODB, JvExControls,
  JvGradient;

type
  TFrmExporta_JIT_Rodopar = class(TForm)
    SP: TADOStoredProc;
    btnCancelar: TBitBtn;
    Label5: TLabel;
    cbCliente: TComboBox;
    QCliente: TADOQuery;
    QClienteCNPJ: TStringField;
    btnExportar: TBitBtn;
    DBGrid1: TDBGrid;
    QJIT1: TADOQuery;
    DataSource1: TDataSource;
    QJIT1CNPJ: TStringField;
    QJIT1PRODUTO: TStringField;
    QJIT1QUANT: TBCDField;
    QJIT1PEDIDO: TBCDField;
    QJIT1OS: TBCDField;
    QJIT1CHAVE_NFE: TStringField;
    RadioGroup1: TRadioGroup;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmExporta_JIT_Rodopar: TFrmExporta_JIT_Rodopar;

implementation

uses dados, menu;

{$R *.DFM}

procedure TFrmExporta_JIT_Rodopar.FormShow(Sender: TObject);
begin
  cbCliente.SetFocus;
end;

procedure TFrmExporta_JIT_Rodopar.RadioGroup1Click(Sender: TObject);
begin
  QJIT1.close;
  if RadioGroup1.ItemIndex = 0 then
    if cbCliente.Text = '' then
      QJIT1.SQL[2] := 'WHERE pedido > 0 '
    else
      QJIT1.SQL[2] := 'WHERE pedido > 0 and cnpj = ' + QuotedStr(cbCliente.Text)
  else if RadioGroup1.ItemIndex = 1 then
    if cbCliente.Text = '' then
      QJIT1.SQL[2] := 'WHERE pedido = -99 '
    else
      QJIT1.SQL[2] := 'WHERE pedido = -99 and cnpj = ' +
        QuotedStr(cbCliente.Text)
  else if cbCliente.Text = '' then
    QJIT1.SQL[2] := 'WHERE pedido is null '
  else
    QJIT1.SQL[2] := 'WHERE pedido is null and cnpj = ' +
      QuotedStr(cbCliente.Text);
  QJIT1.open;
  Label1.caption := IntToStr(QJIT1.RecordCount);
end;

procedure TFrmExporta_JIT_Rodopar.FormCreate(Sender: TObject);
begin
  Self.caption := 'Exportar JIT -> Rodopar';
  QCliente.open;
  cbCliente.Clear;
  cbCliente.Items.add('');
  while not QCliente.eof do
  begin
    cbCliente.Items.add(QClienteCNPJ.value);
    QCliente.Next;
  end;
end;

procedure TFrmExporta_JIT_Rodopar.btnExportarClick(Sender: TObject);
begin
  if cbCliente.Text = '' then
  begin
    ShowMessage('Escolha o Cliente !!');
    cbCliente.SetFocus;
    exit;
  end;

  if Application.Messagebox('Voc� Deseja Exportar Este Cliente ?',
    'Exportar Para Rodopar', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin

    SP.Parameters[0].value := cbCliente.Text;
    SP.ExecProc;

    ShowMessage('Processo Finalizado !!');
  end;
end;

procedure TFrmExporta_JIT_Rodopar.BitBtn1Click(Sender: TObject);
begin
  if Application.Messagebox('Voc� Deseja Liberar os Pendentes ?',
    'Liberar Pendentes', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    dtmdados.RQuery1.close;
    dtmdados.RQuery1.SQL.Clear;
    dtmdados.RQuery1.SQL.add
      ('update tb_nf_jit set pedido = null, os = null where pedido in (-99)');
    dtmdados.RQuery1.ExecSQL;
    dtmdados.RQuery1.close;
    ShowMessage
      ('Liberados, ser�o processados automaticamente na pr�xima madrugada, ou pode ser feito manualmente');
  end;
end;

procedure TFrmExporta_JIT_Rodopar.btnCancelarClick(Sender: TObject);
begin
  close;
end;

procedure TFrmExporta_JIT_Rodopar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  close;
end;

end.
