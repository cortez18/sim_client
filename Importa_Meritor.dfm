object frmImporta_Meritor: TfrmImporta_Meritor
  Left = 344
  Top = 206
  Caption = 'Importa'#231#227'o de Pedidos Meritor'
  ClientHeight = 408
  ClientWidth = 528
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 169
    Top = 41
    Width = 58
    Height = 13
    Caption = 'Arquivos :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 4
    Top = 26
    Width = 57
    Height = 13
    Caption = 'Caminho :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PB1: TGauge
    Left = 92
    Top = 375
    Width = 349
    Height = 30
    Progress = 0
  end
  object Label5: TLabel
    Left = 233
    Top = 36
    Width = 6
    Height = 16
    Caption = '  '
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Arial Narrow'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    AlignWithMargins = True
    Left = 374
    Top = 2
    Width = 8
    Height = 16
    Alignment = taRightJustify
    Caption = '  '
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 6
    Top = 60
    Width = 157
    Height = 311
    FileList = FLB
    TabOrder = 0
  end
  object FLB: TFileListBox
    Left = 169
    Top = 58
    Width = 356
    Height = 313
    Color = 8404992
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    Mask = '*.xls'
    ParentFont = False
    TabOrder = 1
    OnClick = FLBClick
  end
  object btnImportar: TBitBtn
    Left = 4
    Top = 375
    Width = 84
    Height = 30
    Caption = '&Importar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
      1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
      96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
      98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
      36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
      6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
      3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
      6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
      42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
      96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
      42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
      FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
      4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
      FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
      54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
      C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
      597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
      71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
      5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
      75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
      FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
      9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
      A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
      52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    ParentFont = False
    TabOrder = 2
    OnClick = btnImportarClick
  end
  object btnSair: TBitBtn
    Left = 447
    Top = 373
    Width = 75
    Height = 30
    Caption = '&Sair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D63
      9B1619609839145D9562105A92880D5890A4135C92FC0C578FED999999FF7171
      71FF545454FF515151FF4F4F4FFF4C4C4CFF4A4A4AFF474747FF454545FF2567
      9DFF3274A8FF3D7CAFFF4784B5FF4E8ABAFF3E7EADFF0C578FEAFFFFFF00FFFF
      FF00585858FFA2A2A2FFA2A2A2FFA3A3A3FFA4A4A4FFA4A4A4FFA5A5A5FF2F6F
      A5FF78ABD2FF78ABD3FF73A7D1FF69A0CDFF407FAEFF0F5991EAFFFFFF00FFFF
      FF005C5C5CFFA0A0A0FF3C7340FFA2A2A2FFA3A3A3FFA3A3A3FFA4A4A4FF3674
      AAFF7DAFD4FF5B9AC9FF5495C7FF5896C8FF4180AEFF135C94EAFFFFFF00FFFF
      FF00606060FF3A773FFF3D7641FFA1A1A1FFA2A2A2FFA2A2A2FFA3A3A3FF3D79
      B0FF82B3D7FF629FCCFF5A9AC9FF5E9BCAFF4381AFFF196098EAFFFFFF00FFFF
      FF0039763EFF4D9554FF499150FF286E2DFF266A2AFF236627FF216325FF457E
      B4FF88B7D9FF67A3CFFF619ECCFF639FCCFF4583B1FF1F649CEAFFFFFF003883
      3FD4569D5DFF80C688FF7BC383FF77C17FFF72BE79FF6FBC75FF246728FF4C84
      BAFF8DBBDBFF6EA8D1FF66A6D1FF5FB4DFFF4785B1FF2569A1EA3E8B46A15EA5
      66FF8BCC94FF7DC586FF73C07CFF6EBD77FF69BB71FF75BF7CFF276C2CFF5489
      BFFF94BFDDFF75ADD4FF63B8E1FF4BD4FFFF428BB8FF2C6EA6EAFFFFFF003F8C
      47D25FA667FF8DCD96FF89CB92FF84C88DFF80C688FF7BC383FF2A7030FF5A8E
      C4FF98C3E0FF7CB3D7FF74AFD6FF5EC4EDFF4B88B3FF3473ABEAFFFFFF00FFFF
      FF0047894FFF60A769FF5DA465FF37823EFF347E3BFF317937FF2E7534FF6092
      C9FF9EC7E2FF83B8DAFF7DB4D7FF7EB3D7FF4F89B4FF3B79B1EAFFFFFF00FFFF
      FF00777777FF4D9054FF3D8A45FF9B9B9BFF9C9C9CFF9D9D9DFF9D9D9DFF6696
      CCFFA2CBE3FF89BDDCFF83B9DAFF84B9DAFF518BB5FF437EB6EAFFFFFF00FFFF
      FF007A7A7AFF989998FF529159FF9A9A9AFF9B9B9BFF9C9C9CFF9C9C9CFF6C9A
      D0FFA7CEE5FF8FC1DFFF89BDDCFF8BBDDCFF538DB6FF4B84BCEAFFFFFF00FFFF
      FF007D7D7DFF999999FF999999FF9A9A9AFF9A9A9AFF9B9B9BFF9B9B9BFF6F9D
      D3FFAAD1E7FFABD1E7FF98C7E1FF91C2DEFF568FB7FF5289C1EAFFFFFF00FFFF
      FF00808080FF7E7E7EFF7C7C7CFF7A7A7AFF777777FF757575FF727272FF719E
      D4FF6F9ED6FF87B2DCFFABD3E8FFA9D0E6FF5890B8FF598EC6EAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00709ED6DB6D9CD4FF85B1DAFF5A91B9FF6093CBEAFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF006D9CD4896A9AD2FB6697CFEE}
    ParentFont = False
    TabOrder = 3
    OnClick = btnSairClick
  end
  object DriveComboBox1: TDriveComboBox
    Left = 6
    Top = 40
    Width = 155
    Height = 19
    DirList = DirectoryListBox1
    TabOrder = 4
  end
  object DBGrid1: TDBGrid
    Left = 24
    Top = 244
    Width = 469
    Height = 120
    DataSource = DataSource1
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    Columns = <
      item
        Expanded = False
        FieldName = 'DATA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PEDIDO_WMS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ORDER_'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ITEM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRODUTO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DELIVERY'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STAT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESTINO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_DETAIL'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF'
        Visible = True
      end>
  end
  object cbLimpar: TCheckBox
    Left = 8
    Top = 3
    Width = 155
    Height = 25
    Caption = 'Limpar Base de Regra'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
  end
  object QImp: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <>
    Left = 236
    Top = 80
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      'select id_empfaenger '
      
        'from kunden k left join adressen e on k.id_kunde = e.id_eigner_2' +
        ' AND E.ID_EIGNER_1 = K.ID_KLIENT'
      'where k.id_klient = '#39'MRTR'#39' and e.name_2 = :0')
    Left = 236
    Top = 144
    object QClienteID_EMPFAENGER: TStringField
      FieldName = 'ID_EMPFAENGER'
      Size = 12
    end
  end
  object Query: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <
      item
        Name = '0'
        DataType = ftString
        Size = 40
        Value = '0'
      end>
    SQL.Strings = (
      'select count(*) tem '
      
        'from tb_pedido_meritor p left join tb_pedido_item i on i.id_pedi' +
        'do = p.id '
      'where i.id_detail = :0')
    Left = 300
    Top = 80
    object QueryTEM: TBCDField
      FieldName = 'TEM'
      ReadOnly = True
      Precision = 32
    end
  end
  object QExporta: TADOQuery
    Connection = dtmDados.ConWms
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select p.time_neu Data, i.nr_auf Pedido_WMS, i.nr_zoll_tarif Ord' +
        'er_, i.id_artikel Item, e.bez_1 Produto, i.mng_best Qt, to_char(' +
        'i.PREIS_EINZEL) Delivery, '
      
        'p.stat, d.name Destino, a.name_2 Fantasia, to_char(I.PREIS_EINZE' +
        'L_INTERN) Id_Detail, p.nr_liefers NF'
      
        'from aufpos i left join artikel e on e.id_klient = i.id_klient a' +
        'nd e.id_artikel = i.id_artikel'
      
        '              left join auftraege p on p.nr_auf = i.nr_auf and p' +
        '.id_klient = i.id_klient'
      
        '              left join kunden d on p.id_empfaenger_ware = d.id_' +
        'empfaenger and p.id_klient = d.id_klient'
      
        '              left join adressen a on a.id_eigner_1 = i.id_klien' +
        't and a.id_eigner_2 = d.id_kunde'
      'where i.id_klient = '#39'MRTR'#39
      ''
      'union'
      ''
      
        'select pe.data Data, pe.ped_wms Pedido_WMS, pe.pedido Order_, pi' +
        '.codpro Item, '#39#39' Produto, pi.quant Qt, pe.Delivery, '
      
        #39'Importar'#39' stat, di.name Destino, ai.name_2 Fantasia, pi.Id_Deta' +
        'il, '#39#39' NF'
      
        'from tb_pedido_item pi left join tb_pedido_meritor pe on pe.id =' +
        ' pi.id_pedido'
      
        '                       left join kunden di on (pe.coddes = di.id' +
        '_empfaenger and di.id_klient = '#39'MRTR'#39')'
      
        '                       left join adressen ai on (ai.id_eigner_1 ' +
        '= di.id_klient and ai.id_eigner_2 = di.id_kunde)'
      'where pe.wms is null'
      'order by 1')
    Left = 300
    Top = 144
    object QExportaDATA: TDateTimeField
      FieldName = 'DATA'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object QExportaPEDIDO_WMS: TStringField
      FieldName = 'PEDIDO_WMS'
    end
    object QExportaORDER_: TStringField
      FieldName = 'ORDER_'
      Size = 22
    end
    object QExportaITEM: TStringField
      FieldName = 'ITEM'
      Size = 40
    end
    object QExportaPRODUTO: TStringField
      FieldName = 'PRODUTO'
      Size = 40
    end
    object QExportaQT: TBCDField
      FieldName = 'QT'
      Precision = 32
    end
    object QExportaDELIVERY: TStringField
      FieldName = 'DELIVERY'
      ReadOnly = True
      Size = 40
    end
    object QExportaSTAT: TStringField
      FieldName = 'STAT'
      Size = 8
    end
    object QExportaDESTINO: TStringField
      FieldName = 'DESTINO'
      Size = 30
    end
    object QExportaFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 30
    end
    object QExportaID_DETAIL: TStringField
      FieldName = 'ID_DETAIL'
      ReadOnly = True
      Size = 40
    end
    object QExportaNF: TStringField
      FieldName = 'NF'
      Size = 12
    end
  end
  object DataSource1: TDataSource
    DataSet = QExporta
    Left = 308
    Top = 292
  end
end
