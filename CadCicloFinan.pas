unit CadCicloFinan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, JvExMask, JvToolEdit, JvBaseEdits, DB, ADODB,
  ImgList, ActnList, Mask, DBCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid, ComCtrls, System.ImageList, System.Actions;

type
  TfrmCadCicloFinan = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    Detalhes: TTabSheet;
    Label2: TLabel;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    lblEnde: TLabel;
    DBEdit1: TDBEdit;
    iml: TImageList;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    btnSalvar: TBitBtn;
    btnAlterar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    btnLimparFiltro: TBitBtn;
    QTabela: TADOQuery;
    dtstabela: TDataSource;
    QTabelaCOD_FORNECEDOR: TBCDField;
    QTabelaNM_FORNECEDOR: TStringField;
    QTabelaDIAS: TBCDField;
    GroupBox1: TGroupBox;
    Label27: TLabel;
    edDias: TJvCalcEdit;
    RGPeriodo: TRadioGroup;

    procedure btnFecharClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    quant: integer;

  public
    { Public declarations }
  end;

var
  frmCadCicloFinan: TfrmCadCicloFinan;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadCicloFinan.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCadCicloFinan.btnSalvarClick(Sender: TObject);
var
  dias, rec: integer;
begin
  rec := QTabelaCOD_FORNECEDOR.AsInteger;
  if edDias.value > 0 then
    dias := edDias.AsInteger
  else if RGPeriodo.ItemIndex = 0 then
    dias := -7
  else if RGPeriodo.ItemIndex = 1 then
    dias := -10
  else if RGPeriodo.ItemIndex = 2 then
    dias := -15
  else if RGPeriodo.ItemIndex = 3 then
    dias := -30
  else
    dias := 0;

  dtmdados.IQuery1.Close;
  dtmdados.IQuery1.SQL.clear;
  dtmdados.IQuery1.SQL.add
    ('Update tb_custofrac set dias = :0 where cod_fornecedor = :1');
  dtmdados.IQuery1.Parameters[0].value := dias;
  dtmdados.IQuery1.Parameters[1].value := QTabelaCOD_FORNECEDOR.AsInteger;
  dtmdados.IQuery1.ExecSQL;
  dtmdados.IQuery1.Close;

  QTabela.Close;
  QTabela.open;
  QTabela.locate('cod_fornecedor', rec, []);
  PageControl1.ActivePageIndex := 0;

  btnAlterar.Enabled := false;
  btnSalvar.Enabled := true;

end;

procedure TfrmCadCicloFinan.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmCadCicloFinan.QTabelaAfterScroll(DataSet: TDataSet);
begin
  if QTabelaDIAS.AsInteger = -7 then
    RGPeriodo.ItemIndex := 0
  else if QTabelaDIAS.AsInteger = -10 then
    RGPeriodo.ItemIndex := 1
  else if QTabelaDIAS.AsInteger = -15 then
    RGPeriodo.ItemIndex := 2
  else if QTabelaDIAS.AsInteger = -30 then
    RGPeriodo.ItemIndex := 3
  else
    RGPeriodo.ItemIndex := -1;
  if QTabelaDIAS.AsInteger >= 0 then
    edDias.value := QTabelaDIAS.AsInteger
  else
    edDias.clear;
end;

procedure TfrmCadCicloFinan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.Close;
end;

procedure TfrmCadCicloFinan.FormCreate(Sender: TObject);
begin
  QTabela.open;
end;

procedure TfrmCadCicloFinan.btnAlterarClick(Sender: TObject);
begin
  if not dtmdados.PodeAlterar(name) then
    Exit;
  PageControl1.ActivePageIndex := 1;
  btnAlterar.Enabled := false;
  btnSalvar.Enabled := true;
end;

procedure TfrmCadCicloFinan.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  Screen.Cursor := crDefault;
end;

end.
