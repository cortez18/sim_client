object frmRel_Analise_custo: TfrmRel_Analise_custo
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de An'#225'lise de Custo'
  ClientHeight = 741
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 61
    Width = 1184
    Height = 647
    Align = alClient
    DataSource = navnavig
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = JvDBGrid1TitleClick
    AlternateRowColor = clSilver
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'NR_CONHECIMENTO'
        Title.Caption = 'N'#186' CTe'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DT_CTRC'
        Title.Caption = 'DT_Cte'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_SERIE'
        Title.Caption = 'N'#186' S'#233'rie'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CTE_COMPLEMNETADO'
        Title.Caption = 'CTE_COMPLEMENTADO'
        Width = 116
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLI'
        Title.Caption = 'Cliente'
        Width = 215
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_CLI'
        Title.Caption = 'CNPJ CLI'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_REM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_REM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_REM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_REM'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_DES'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_DES'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_DES'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_DES'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_RED'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_RED'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_RED'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_RED'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NM_CLIENTE_EXP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_CNPJ_CPF_EXP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE_EXP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF_EXP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_NF'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_KG'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PESO_CUB'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QT_VOLUME'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATNOT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_NOTA'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CFOP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_FRETE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_PEDAGIO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_SEGURO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_GRIS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_OUTROS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TAS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TX_CTE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_DESPACHO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ENTREGA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TDE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TR'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TX_EXCEDENTE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_SEG_BALSA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_RED_FLUV'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_AGENDA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_PALLET'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_PORTO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ALFAND'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_CANHOTO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_AJUDANTE'
        Title.Caption = 'VL_CTE_AJUDANTE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_FRETE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_PEDAGIO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_SEGURO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_GRIS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_TAS'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_DESPACHO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_TDE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_TR'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_TX_EXCEDENTE'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_SEG_BALSA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_RED_FLUV'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_AGENDA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_PALLET'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_PORTO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_ALFAND'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_CANHOTO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF_VL_AJUDANTE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_IMPOSTO'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_TOTAL'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VL_ALIQUOTA'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_TIPO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_FATURA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ_TRANSPORTADORA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IESTADUAL'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPERACAO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FATURA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CTE_ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OBSCON'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_TABELA'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REGIAO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FL_STATUS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NR_MANIFESTO'
        Width = 79
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUSTO'
        Title.Caption = 'Custo'
        Width = 77
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 324
    Top = 180
    Width = 341
    Height = 41
    Caption = 'Gerando..............'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1184
    Height = 61
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 2
    object Label10: TLabel
      Left = 12
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 115
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Data Final'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 224
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Cliente :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtInicial: TJvDateEdit
      Left = 12
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 0
    end
    object dtFinal: TJvDateEdit
      Left = 115
      Top = 27
      Width = 90
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object btRelatorio: TBitBtn
      Left = 727
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Gerar'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001C811F231B7E1F7D1B7A1FDB1A731EF31A701EF31B71
        1FDB1B711F7D1B6C1F23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001C8A21531B831FE642A052FF87CA9AFF9BD3ABFF9BD2ABFF83C7
        96FF3D974CFF1A6E1EE61B701F53FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF001C912B531B8A20F46DBE83FFA8DBB5FF87CC98FF66BC7DFF64BA7CFF86CB
        98FFA5D9B4FF66B77DFF1A6C1DF41B711F53FFFFFF00FFFFFF00FFFFFF001D9B
        36221C962FE572C287FFA8DBB2FF60BC77FF5CBA73FF59B870FF59B56FFF58B5
        6FFF5BB774FFA5D9B3FF69B87FFF1A711EE51B711F22FFFFFF00FFFFFF001EA4
        3D7E4CB064FFAADDB4FF64C179FF5FBE71FF75C585FFD4ECD9FF8ACD99FF56B6
        6CFF58B56EFF5CB774FFA6DAB4FF419B4EFF1B771F7EFFFFFF00FFFFFF001FA9
        42DB91D29FFF8DD49AFF64C374FF79C987FFF2FAF4FFFFFFFFFFFDFEFDFF86CB
        96FF57B76DFF5BB972FF85CC97FF87C79AFF1B781FDBFFFFFF00FFFFFF001FAD
        42F6A6DCAFFF70CA7FFF73CA80FFF0F9F1FFFFFFFFFFEBF7EDFFFFFFFFFFFBFD
        FCFF88CD96FF5BB971FF67BE7DFFA0D7AFFF1B7A1EF6FFFFFF00FFFFFF0026B4
        4BF6A7DDB1FF72CC80FF66C773FFB0E1B7FFD2EED6FF63C170FFB8E3BFFFFFFF
        FFFFFBFDFCFF8CD099FF69C17EFFA1D7AEFF1B7F1EF6FFFFFF00FFFFFF002DBB
        54DB95D7A1FF91D79BFF69C976FF64C66FFF61C46EFF61C36FFF61C26FFFB9E4
        C0FFFFFFFFFFE3F4E6FF8BD199FF8BCE9DFF1C8820DBFFFFFF00FFFFFF0034BE
        597E57BF70FFAFE1B7FF6DCC7AFF68C872FF65C770FF63C56EFF62C46EFF63C4
        71FFB6E3BEFF6FC77EFFACDFB5FF48A95EFF1C8F267EFFFFFF00FFFFFF0039C2
        5C2234BE55E57FCE90FFAEE1B5FF6DCC7AFF6ACA76FF68C872FF68C874FF68C8
        75FF6BC979FFACDFB4FF76C489FF1C962DE51C942D22FFFFFF00FFFFFF00FFFF
        FF003BC55E5334C055F47FCE90FFAFE1B7FF92D89DFF77CE83FF77CE83FF92D8
        9DFFAEE1B5FF78C88BFF1D9D32F41D9D3653FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF003DC7605336C259E659C274FF96D7A3FFA5DCAEFFA5DCAEFF95D6
        A1FF50B96AFF1FAB42E61FA94253FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0040C962233BC55E7D39C25BDB31BD54F32DBB52F32BB9
        52DB2BB7527D28B44E23FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 2
      OnClick = btRelatorioClick
    end
    object ledIdCliente: TJvDBLookupEdit
      Left = 224
      Top = 27
      Width = 145
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'nr_cnpj_cpf'
      LookupField = 'NR_CNPJ_CPF'
      LookupSource = DataSource1
      TabOrder = 3
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object ledCliente: TJvDBLookupEdit
      Left = 375
      Top = 27
      Width = 303
      Height = 21
      DropDownCount = 20
      LookupDisplay = 'nm_cliente'
      LookupField = 'NR_CNPJ_CPF'
      LookupSource = DataSource1
      TabOrder = 4
      Text = ''
      OnCloseUp = ledIdClienteCloseUp
      OnExit = ledIdClienteCloseUp
    end
    object btnExcel: TBitBtn
      Left = 820
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Excel'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000FFFFFF00BB6A
        346BBA6530BCBB6631EDBA6630F7BA6630F7BA6630F7BA6530F7BA652FF7B965
        2EF7B9652EF7B9642EF7B9642EEFB7622CBDB7622E63FFFFFF00FFFFFF00BC69
        33DEF8F1EAF2F7ECDFFDF6EBDEFFF6EADEFFF6EADCFFF6EADCFFFAF3EBFFFAF3
        EBFFFAF2EAFFFCF7F3FFFCF8F4FDFEFEFDF0B7602AD5FFFFFF00FFFFFF00BF71
        38F5F5EBDFFEFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFDFBF8FDB9642DF3FFFFFF00FFFFFF00C178
        3CF7F7EDE3FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4D1FFFCE4
        D1FFFCE4D1FFFCE4D1FFFCE4D1FFFBF7F4FFBB6731F7FFFFFF00FFFFFF00C47C
        40F7F7F0E6FFFCE4D1FFFCE4D1FFE5D9C2FF689E70FF579665FF599766FF6CA0
        73FFFCE4D1FFFCE4D1FFFCE4D1FFFCF9F5FFBF6F36F7FFFFFF001A7533E41A75
        33FF197533FF197433FF448A52FF619B6BFFBBD6C3FF78BB84FF61AB6AFF5796
        64FFFCE2CCFFFBE0C9FFFBE1C8FFFDFAF7FFC1763BF7FFFFFF001A7533661B75
        33FF5BA06EFF49965CFF47905BFFC7DDCDFF5DB671FF67AE75FF448D58FF1B75
        33FFFCE2CDFFFBE1CBFFFBE1C9FFFBF7F2FFC57C3FF7FFFFFF00FFFFFF007F7E
        3FFA1F7837FF48915DFFC7DDCDFF6AC084FF71B682FF448E59FFB1C1A1FFFBE4
        D0FFFBE3CCFFFADFC7FFFADFC6FFFAF2EAFFC68042F7FFFFFF00FFFFFF00C288
        49F7619E71FFC5DCCCFF76C997FF73BC87FF438D58FF559360FFF5E0CCFFFBE1
        CCFFFAE0C7FFF9DDC3FFF8DCC2FFFAF4EDFFC68245F7FFFFFF00438D58076F8B
        53FCC0D9C8FF82D3A3FF6DC18AFF549563FF4B9660FF519764FF679A68FFF4DC
        C3FFF8DCC2FFF6DABDFFF6D8BBFFFAF4EFFFC68346F7FFFFFF00438D58ACB5D3
        BEFF9CDAB5FF74C895FF549563FF4A935FFF5DA474FF59A16EFF509764FF6297
        62FFE9D1B4FFF3D4B5FFF1D2B3FFF8F4F0FFC48246F7FFFFFF00438D58DE558C
        56FE539666FF549563FFA1B995FF8DAE83FF2E7F42FF2E7F41FF3A8448FF3682
        45FF90B490FFF7F2ECFFFBF7F3FFF5EFE9FFC27E45FBFFFFFF00FFFFFF00C689
        4CF6F9F5F1FFFCE3CDFFFBE3CEFFFBE3CDFFFBE2CBFFF9E0C8FFF8DCC2FFF5D6
        BAFFFDFBF8FFFCE6CDFFFAE5C9FFE2B684FFBF7942A6FFFFFF00FFFFFF00C588
        4BEAFAF6F2FCFAE0C7FFFBE1C9FFFBE2C9FFFBE0C8FFF9DFC5FFF8DBC1FFF4D6
        B8FFFFFBF8FFF6D8B4FFE1B07DFFDB9264F6B46B3E07FFFFFF00FFFFFF00C485
        49C3F7F2ECECF8F4EEFCF8F4EDFFF8F3EDFFF8F3EDFFF8F3EDFFF8F2ECFFF7F2
        ECFFF2E6D7FFE2B27DFFDB9465F5B3683B07FFFFFF00FFFFFF00FFFFFF00C17D
        4460C88B4DBBC88C4FEEC88C4FF6C88C4FF7C88C4FF7C88D4FF7C98C4FF7C78B
        4FF7C5894BD4C4763B91B3683C06FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 5
      OnClick = btnExcelClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 708
    Width = 1184
    Height = 33
    Align = alBottom
    TabOrder = 3
    object Gauge1: TGauge
      Left = 204
      Top = 1
      Width = 755
      Height = 23
      Progress = 0
    end
    object JvDBNavigator1: TJvDBNavigator
      Left = 7
      Top = 1
      Width = 188
      Height = 25
      DataSource = navnavig
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 0
    end
  end
  object QCtrc: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct r.*,'
      '    case when cc.status = '#39'P'#39' then'
      
        '      case when cc.new_custo > 0 then cc.new_custo else cc.custo' +
        '_calc end'
      '    else'
      
        '      nvl(fnc_custo_transp_sim2(cc.codcon, cc.transp, cc.doc, cc' +
        '.codfil),0)'
      '    end custo '
      
        'from VW_REL_CTE r left join vw_custo_transp_sim cc on cc.doc = r' +
        '.nr_manifesto and cc.filial = r.fl_empresa'
      'where nr_conhecimento > 0'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 56
    Top = 104
    object QCtrcNR_CONHECIMENTO: TBCDField
      FieldName = 'NR_CONHECIMENTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCtrcDT_CTRC: TDateTimeField
      FieldName = 'DT_CTRC'
      ReadOnly = True
    end
    object QCtrcNR_SERIE: TStringField
      FieldName = 'NR_SERIE'
      ReadOnly = True
      Size = 2
    end
    object QCtrcCTE_COMPLEMNETADO: TBCDField
      FieldName = 'CTE_COMPLEMNETADO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCtrcNM_CLI: TStringField
      FieldName = 'NM_CLI'
      ReadOnly = True
      Size = 80
    end
    object QCtrcCNPJ_CLI: TStringField
      FieldName = 'CNPJ_CLI'
      ReadOnly = True
    end
    object QCtrcNM_CLIENTE_REM: TStringField
      FieldName = 'NM_CLIENTE_REM'
      ReadOnly = True
      Size = 80
    end
    object QCtrcNR_CNPJ_CPF_REM: TStringField
      FieldName = 'NR_CNPJ_CPF_REM'
      ReadOnly = True
    end
    object QCtrcCIDADE_REM: TStringField
      FieldName = 'CIDADE_REM'
      ReadOnly = True
      Size = 40
    end
    object QCtrcUF_REM: TStringField
      FieldName = 'UF_REM'
      ReadOnly = True
      Size = 2
    end
    object QCtrcNM_CLIENTE_DES: TStringField
      FieldName = 'NM_CLIENTE_DES'
      ReadOnly = True
      Size = 80
    end
    object QCtrcNR_CNPJ_CPF_DES: TStringField
      FieldName = 'NR_CNPJ_CPF_DES'
      ReadOnly = True
    end
    object QCtrcCIDADE_DES: TStringField
      FieldName = 'CIDADE_DES'
      ReadOnly = True
      Size = 40
    end
    object QCtrcUF_DES: TStringField
      FieldName = 'UF_DES'
      ReadOnly = True
      Size = 2
    end
    object QCtrcNM_CLIENTE_RED: TStringField
      FieldName = 'NM_CLIENTE_RED'
      ReadOnly = True
      Size = 80
    end
    object QCtrcNR_CNPJ_CPF_RED: TStringField
      FieldName = 'NR_CNPJ_CPF_RED'
      ReadOnly = True
    end
    object QCtrcCIDADE_RED: TStringField
      FieldName = 'CIDADE_RED'
      ReadOnly = True
      Size = 40
    end
    object QCtrcUF_RED: TStringField
      FieldName = 'UF_RED'
      ReadOnly = True
      Size = 2
    end
    object QCtrcNM_CLIENTE_EXP: TStringField
      FieldName = 'NM_CLIENTE_EXP'
      ReadOnly = True
      Size = 80
    end
    object QCtrcNR_CNPJ_CPF_EXP: TStringField
      FieldName = 'NR_CNPJ_CPF_EXP'
      ReadOnly = True
    end
    object QCtrcCIDADE_EXP: TStringField
      FieldName = 'CIDADE_EXP'
      ReadOnly = True
      Size = 40
    end
    object QCtrcUF_EXP: TStringField
      FieldName = 'UF_EXP'
      ReadOnly = True
      Size = 2
    end
    object QCtrcVL_NF: TBCDField
      FieldName = 'VL_NF'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcPESO_KG: TBCDField
      FieldName = 'PESO_KG'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcPESO_CUB: TBCDField
      FieldName = 'PESO_CUB'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcQT_VOLUME: TBCDField
      FieldName = 'QT_VOLUME'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcDATNOT: TDateTimeField
      FieldName = 'DATNOT'
      ReadOnly = True
    end
    object QCtrcNR_NOTA: TStringField
      FieldName = 'NR_NOTA'
      ReadOnly = True
    end
    object QCtrcCFOP: TStringField
      FieldName = 'CFOP'
      ReadOnly = True
      Size = 5
    end
    object QCtrcVL_FRETE: TBCDField
      FieldName = 'VL_FRETE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_PEDAGIO: TBCDField
      FieldName = 'VL_PEDAGIO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_SEGURO: TBCDField
      FieldName = 'VL_SEGURO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_GRIS: TBCDField
      FieldName = 'VL_GRIS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_OUTROS: TBCDField
      FieldName = 'VL_OUTROS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcTAS: TBCDField
      FieldName = 'TAS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_TX_CTE: TBCDField
      FieldName = 'VL_TX_CTE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_DESPACHO: TBCDField
      FieldName = 'VL_DESPACHO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_ENTREGA: TBCDField
      FieldName = 'VL_ENTREGA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_TDE: TBCDField
      FieldName = 'VL_TDE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_TR: TBCDField
      FieldName = 'VL_TR'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcTX_EXCEDENTE: TBCDField
      FieldName = 'TX_EXCEDENTE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_SEG_BALSA: TBCDField
      FieldName = 'VL_SEG_BALSA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_RED_FLUV: TBCDField
      FieldName = 'VL_RED_FLUV'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_AGENDA: TBCDField
      FieldName = 'VL_AGENDA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_PALLET: TBCDField
      FieldName = 'VL_PALLET'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_PORTO: TBCDField
      FieldName = 'VL_PORTO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_ALFAND: TBCDField
      FieldName = 'VL_ALFAND'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_CANHOTO: TBCDField
      FieldName = 'VL_CANHOTO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_AJUDANTE: TBCDField
      FieldName = 'VL_AJUDANTE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_FRETE: TBCDField
      FieldName = 'NF_VL_FRETE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_PEDAGIO: TBCDField
      FieldName = 'NF_VL_PEDAGIO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_SEGURO: TBCDField
      FieldName = 'NF_VL_SEGURO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_GRIS: TBCDField
      FieldName = 'NF_VL_GRIS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_TAS: TBCDField
      FieldName = 'NF_TAS'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_DESPACHO: TBCDField
      FieldName = 'NF_VL_DESPACHO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_TDE: TBCDField
      FieldName = 'NF_VL_TDE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_TR: TBCDField
      FieldName = 'NF_VL_TR'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_TX_EXCEDENTE: TBCDField
      FieldName = 'NF_TX_EXCEDENTE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_SEG_BALSA: TBCDField
      FieldName = 'NF_VL_SEG_BALSA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_RED_FLUV: TBCDField
      FieldName = 'NF_VL_RED_FLUV'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_AGENDA: TBCDField
      FieldName = 'NF_VL_AGENDA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_PALLET: TBCDField
      FieldName = 'NF_VL_PALLET'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_PORTO: TBCDField
      FieldName = 'NF_VL_PORTO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_ALFAND: TBCDField
      FieldName = 'NF_VL_ALFAND'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_CANHOTO: TBCDField
      FieldName = 'NF_VL_CANHOTO'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcNF_VL_AJUDANTE: TBCDField
      FieldName = 'NF_VL_AJUDANTE'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcVL_IMPOSTO: TFloatField
      FieldName = 'VL_IMPOSTO'
      ReadOnly = True
    end
    object QCtrcVL_TOTAL: TFloatField
      FieldName = 'VL_TOTAL'
      ReadOnly = True
    end
    object QCtrcVL_ALIQUOTA: TFloatField
      FieldName = 'VL_ALIQUOTA'
      ReadOnly = True
    end
    object QCtrcFL_TIPO: TStringField
      FieldName = 'FL_TIPO'
      ReadOnly = True
      Size = 100
    end
    object QCtrcNR_FATURA: TBCDField
      FieldName = 'NR_FATURA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcCNPJ_TRANSPORTADORA: TStringField
      FieldName = 'CNPJ_TRANSPORTADORA'
      ReadOnly = True
    end
    object QCtrcIESTADUAL: TStringField
      FieldName = 'IESTADUAL'
      ReadOnly = True
      Size = 16
    end
    object QCtrcOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
    object QCtrcFATURA: TBCDField
      FieldName = 'FATURA'
      ReadOnly = True
      Precision = 32
    end
    object QCtrcCTE_ID: TStringField
      FieldName = 'CTE_ID'
      ReadOnly = True
      Size = 44
    end
    object QCtrcOBSCON: TStringField
      FieldName = 'OBSCON'
      ReadOnly = True
      Size = 500
    end
    object QCtrcNR_TABELA: TBCDField
      FieldName = 'NR_TABELA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCtrcREGIAO: TStringField
      FieldName = 'REGIAO'
      ReadOnly = True
      Size = 12
    end
    object QCtrcFL_STATUS: TStringField
      FieldName = 'FL_STATUS'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object QCtrcNR_MANIFESTO: TBCDField
      FieldName = 'NR_MANIFESTO'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCtrcFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
    object QCtrcCUSTO: TBCDField
      FieldName = 'CUSTO'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 32
    end
  end
  object navnavig: TDataSource
    DataSet = QCtrc
    Left = 116
    Top = 104
  end
  object QCliente: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct c.codclifor cod_cliente, c.razsoc nm_cliente, c.' +
        'codcgc nr_cnpj_cpf '
      
        'from cyber.rodcli c left join tb_conhecimento P on p.cod_pagador' +
        ' = c.codclifor '
      'where p.fl_status = '#39'A'#39' '
      'order by nm_cliente')
    Left = 56
    Top = 52
    object QClienteNM_CLIENTE: TStringField
      FieldName = 'NM_CLIENTE'
      ReadOnly = True
      Size = 50
    end
    object QClienteNR_CNPJ_CPF: TStringField
      FieldName = 'NR_CNPJ_CPF'
      ReadOnly = True
    end
    object QClienteCOD_CLIENTE: TBCDField
      FieldName = 'COD_CLIENTE'
      ReadOnly = True
      Precision = 32
    end
  end
  object DataSource1: TDataSource
    DataSet = QCliente
    Left = 116
    Top = 52
  end
end
