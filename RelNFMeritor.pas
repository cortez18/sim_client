unit RelNFMeritor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl, Grids, DBGrids, JvMaskEdit, Buttons, Mask,
  Excel2010,
  JvExMask, JvToolEdit, ExtCtrls, DB, ADODB, Comobj, Inifiles, ShellAPi;

type
  TfrmRelNFMeritor = class(TForm)
    Panel1: TPanel;
    dtFinal: TJvDateEdit;
    btRelatorio: TBitBtn;
    Label11: TLabel;
    Label10: TLabel;
    dtInicial: TJvDateEdit;
    DBgNf: TDBGrid;
    btnExcel: TBitBtn;
    QNFMeritor: TADOQuery;
    QNFMeritorCODIGO: TBCDField;
    QNFMeritorSERSUB: TStringField;
    QNFMeritorDATEMI: TDateTimeField;
    QNFMeritorOBSFIS: TStringField;
    QNFMeritorDESPRO: TStringField;
    QNFMeritorNFE_ID: TStringField;
    QNFMeritorCODNCM: TStringField;
    QNFMeritorCODCST: TStringField;
    QNFMeritorCODFIS: TBCDField;
    QNFMeritorUNIDAD: TStringField;
    QNFMeritorQUANTI: TBCDField;
    QNFMeritorVLRUNI: TBCDField;
    DSNFmeritor: TDataSource;
    QNFMeritorVLRNOT: TBCDField;
    QNFMeritorREFERE: TStringField;
    procedure btnExcelClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure QNFMeritorAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelNFMeritor: TfrmRelNFMeritor;

implementation

uses Dados, funcoes;

{$R *.dfm}

procedure TfrmRelNFMeritor.btnExcelClick(Sender: TObject);
{ var Ini: TIniFile;
  data : Tdatetime;
  PastaModelo, nf: string;
  Excel: OleVariant;
  wb: Variant;
  Linha, Coluna, saldo, cte, qt, i : Integer;
  peso : real; }
var
  sarquivo: String;
  i: integer;
  memo1: TStringList;
begin
  { Ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'SIM.INI');
    PastaModelo := Ini.ReadString('RELATORIO', 'CAMINHO', '');
    Ini.Free;
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    wb := Excel.Workbooks.Add(PastaModelo + 'RelNFMeritor.xlt');
    Linha  := 3;
    Coluna := 0;
    // QNFMeritor.Close;
    // QNFMeritor.open;
    QNFMeritor.First;
    while not QNFMeritor.Eof do
    begin
    Excel.Workbooks[1].Sheets[1].Range['A1:N1'].Copy;
    Excel.Workbooks[1].Sheets[1].Range['A' + IntToStr(Linha) + ':N' + IntToStr(Linha)].Insert(Shift := xlDown);
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+1] := QNFMeritorCODIGO.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+2]:= QNFMeritorSERSUB.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+3]:= QNFMeritorDATEMI.AsDateTime;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+4]:= QNFMeritorNFE_ID.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+5]:= QNFMeritorREFERE.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+6]:= QNFMeritorDESPRO.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+7] := QNFMeritorCODNCM.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+8]:= QNFMeritorCODCST.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+9]:= QNFMeritorCODFIS.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+10]:= QNFMeritorUNIDAD.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+11]:= QNFMeritorQUANTI.AsString;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+12]:= QNFMeritorVLRUNI.AsFloat;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+13]:= QNFMeritorVLRNOT.AsFloat;
    Excel.Workbooks[1].Sheets[1].Cells[Linha, Coluna+14]:= QNFMeritorOBSFIS.AsString;
    Inc(Linha);
    QNFMeritor.Next;
    end;
    Excel.Workbooks[1].Sheets[1].Rows['1:1'].Delete(Shift := xlUp);
    Excel.Visible := True;
  }
  sarquivo := 'Rel_NF_Meritor.xls';
  memo1 := TStringList.Create;
  memo1.Add('  <HTML>');
  memo1.Add('    <HEAD>');
  memo1.Add('      <TITLE>Relat�rio de NFs Meritor</TITLE>');
  memo1.Add(
    '         <STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.Add(
    '                .texto1  {FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.Add(
    '                .texto2  {FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.Add('         </STYLE>');
  memo1.Add('    </HEAD>');
  memo1.Add('    <BODY <Font Color="#004080">');
  memo1.Add('<Center> <th colspan=2><FONT class=titulo1>');
  memo1.Add('Relat�rio de CT-e�s e NFS Emitidos');
  memo1.Add('</th></font></Center>');
  memo1.Add('</tr>');
  memo1.Add('</B></font>');
  memo1.Add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.Add('<TBODY>');
  memo1.Add('<tr>');
  for i := 0 to DBgNf.Columns.Count - 1 do
  begin
    memo1.Add('<th><FONT class=texto1>' + DBgNf.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.Add('</tr>');
  QNFMeritor.First;
  while not QNFMeritor.Eof do
  begin
    for i := 0 to DBgNf.Columns.Count - 1 do
      memo1.Add('<th><FONT class=texto2>' + QNFMeritor.fieldbyname
        (DBgNf.Columns[i].FieldName).AsString + '</th>');
    QNFMeritor.Next;
    memo1.Add('<tr>');
  end;
  memo1.Add('</TBODY>');
  memo1.Add('</table>');
  memo1.SaveToFile(sarquivo);
  ShellExecute(Application.Handle, nil, PChar(sarquivo), nil, nil,
    SW_SHOWNORMAL);
end;

procedure TfrmRelNFMeritor.btRelatorioClick(Sender: TObject);
begin
  QNFMeritor.Close;
  if copy(dtInicial.Text, 1, 2) <> '  ' then
    QNFMeritor.SQL[5] := 'and nt.datemi between to_date(''' + dtInicial.Text +
      ' 00:00'',''dd/mm/yy hh24:mi'') and to_date(''' + dtFinal.Text +
      ' 23:59'',''dd/mm/yy hh24:mi'')'

  else
    ShowMessage('Favor preencher as datas!');

  QNFMeritor.open;
end;

procedure TfrmRelNFMeritor.QNFMeritorAfterOpen(DataSet: TDataSet);
begin
  TDateTimeField(QNFMeritor.fieldbyname('DATEMI')).DisplayFormat :=
    FormatDateTime('dd/mm/yyyy', QNFMeritor.fieldbyname('DATEMI').AsDateTime);
end;

end.
