object frmMonAverba: TfrmMonAverba
  Left = 0
  Top = 0
  Caption = 'Monitoramento CT-e N'#227'o Averbado'
  ClientHeight = 531
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 37
    Width = 527
    Height = 494
    Align = alClient
    TabOrder = 1
    object grMonitor: TDBGrid
      Left = 1
      Top = 1
      Width = 525
      Height = 492
      Align = alClient
      DataSource = dsMonitor
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = grMonitorDblClick
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'NR_CONHECIMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'N'#250'mero'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CTE_CHAVE'
          Title.Caption = 'Chave'
          Width = 279
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'cte_data'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Width = 125
          Visible = True
        end>
    end
  end
  object mCte: TMemo
    Left = 20
    Top = 148
    Width = 473
    Height = 277
    Hint = 'Clique ESC para sair'
    Align = alCustom
    Color = clWhite
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 2
    Visible = False
    OnClick = mCteClick
    OnKeyDown = mCteKeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 527
    Height = 37
    Align = alTop
    TabOrder = 0
    object lbTotReg: TLabel
      Left = 159
      Top = 10
      Width = 91
      Height = 13
      Caption = 'Total de Registros:'
    end
    object btnATM: TBitBtn
      Left = 20
      Top = 10
      Width = 69
      Height = 22
      Hint = 'Averbar'
      Caption = 'ATM'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF8E512BB06331BB7038BE773CC17B40C27E42C28045C3
        8247C38649C3864AC3874BC3874BC3874CBD8349AC74408E512BA35D31F8F3EE
        F5ECE4FBF5F0FBF7F1FBF7F3FBF8F4FCF9F5FCF9F5FCF9F6FCF9F7FCFAF7FCFA
        F7F7F1ECEAD9CCAB7642BE6F3CFCF9F5ECD0BCF9E4D6FEECDFFEEBDFFEEBDEFE
        EBDBFEEBDCFEEADDFDEADBFDE8D8F8E0CDEACBB3F3EBE3C78B50C27646FDFBF8
        F9E3D2ECCFB9F8E1D0FDE7D6F4D5BDE9BFA0E9BFA2F4D3BDFDE6D4F7DEC9EBCA
        B0F8DBC4F8F2ECC98C50C57D50FDFBF9FDE9D8F9E1D0EBCAB3ECC5A7E3B698F7
        E7DDF7E8DEE3B697ECC3A4EAC5A9F8DAC2FCDFC6F8F3EDC88D50C9865BFDFBF9
        FDE8D7FDE6D4EDC6ABDCAA89F9ECE3FFFBF9FFFCFAF9EEE6DCA887EDBF9CFCDB
        C0FCDBC0F8F3EDC88C50CC8E66FDFBF9FDE5D3F1CCB2E3B596F9EAE0FFF9F5FE
        F3EAFEF4EDFFFBF9F9EDE5E3B08DF0C19EFCD7B7F8F3EDC88C50D09670FDFBF9
        F1CDB1E3B596F9E9DEFEF7F1FDEDE1FEEFE4FEF1E7FEF3EAFFFAF7F9ECE3E2AE
        8AF0BC95F8F4ECC88C50D39D7BFBF6F2E3B496F9E8DCFEF5EEFDE9DAFDEADCFD
        ECDEFDEDE1FEEFE4FEF1E7FFFAF6F9EAE0E2AA85F1E4D9C88C50D7AB91FDFAF8
        FCF5F1FFFCF9FFFCF9FFFCF9FFFCF9FFFCFAFFFCFAFFFCFAFFFCFBFFFDFBFFFD
        FCFBF6F3F8EFEAAB7743C89A7CD5A484D09770CC8F64CC8E62CB8E60CA8C5DC9
        8B5BC88A58C78856C68653C58450C4824DC1834DB279488E512BFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      OnClick = btnATMClick
    end
  end
  object qrMonitor: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    BeforeOpen = qrMonitorBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select distinct cod_conhecimento, cte.nr_conhecimento,cte.averba' +
        '_protocolo, nvl(cte.cte_data,cte.dt_conhecimento) cte_data, cte.' +
        'cte_chave, cte.fl_empresa,  user_ws, pass_ws, code_ws'
      
        'from tb_conhecimento cte left join tb_manitem i on cte.nr_conhec' +
        'imento = i.coddoc and cte.fl_empresa = i.fildoc'
      
        '                         left join tb_manifesto m on m.manifesto' +
        ' = i.manifesto and m.serie = i.serie and m.filial = i.filial'
      
        '                         left join tb_seguradora s on s.fl_empre' +
        'sa = cte.fl_empresa'
      
        'where fl_tipo = '#39'C'#39' and nvl(cte.cte_data,cte.dt_conhecimento) > ' +
        'to_date('#39'01/01/2018'#39','#39'dd/mm/yyyy'#39')'
      'and fl_status ='#39'A'#39' and cte.fl_tipo_2 <> ('#39'C'#39')'
      'and averba_protocolo is null'
      'and cte.cte_chave is not null'
      'and user_ws is not null'
      'and nvl(cte.cte_data,cte.dt_conhecimento) >= (sysdate-60)'
      'order by 2')
    Left = 248
    Top = 176
    object qrMonitorCOD_CONHECIMENTO: TBCDField
      FieldName = 'COD_CONHECIMENTO'
      Precision = 32
      Size = 0
    end
    object qrMonitorNR_CONHECIMENTO: TBCDField
      FieldName = 'NR_CONHECIMENTO'
      Precision = 32
      Size = 0
    end
    object qrMonitorAVERBA_PROTOCOLO: TStringField
      FieldName = 'AVERBA_PROTOCOLO'
      Size = 50
    end
    object qrMonitorCTE_CHAVE: TStringField
      FieldName = 'CTE_CHAVE'
      Size = 44
    end
    object qrMonitorcte_data: TDateTimeField
      FieldName = 'cte_data'
    end
    object qrMonitorFL_EMPRESA: TBCDField
      FieldName = 'FL_EMPRESA'
    end
    object qrMonitoruser_ws: TStringField
      FieldName = 'user_ws'
      Size = 30
    end
    object qrMonitorpass_ws: TStringField
      FieldName = 'pass_ws'
      Size = 30
    end
    object qrMonitorcode_ws: TStringField
      FieldName = 'code_ws'
      Size = 30
    end
  end
  object dsMonitor: TDataSource
    AutoEdit = False
    DataSet = qrMonitor
    Left = 312
    Top = 176
  end
  object XMLDoc: TXMLDocument
    FileName = 'C:\Paulo\GISIS_mssql\Schemas\cteTiposBasico_v2.00.xsd'
    Left = 244
    Top = 232
    DOMVendorDesc = 'MSXML'
  end
end
