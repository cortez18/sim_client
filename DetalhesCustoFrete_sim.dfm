object frmDetalhesCustoFrete_sim: TfrmDetalhesCustoFrete_sim
  Left = 0
  Top = 0
  Caption = 'Detalhes Custo de Frete'
  ClientHeight = 359
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 93
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 412
    object JvDBGrid1: TJvDBGrid
      Left = 1
      Top = 1
      Width = 788
      Height = 91
      Align = alClient
      DataSource = dtsManifesto
      DrawingStyle = gdsClassic
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'VEICULO'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UFO'
          Title.Caption = 'UF O'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOCALO'
          Title.Caption = 'LOCAL O'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCRI'
          Title.Caption = 'CIDADE'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OPERACAO'
          Title.Caption = 'OPERA'#199#195'O'
          Width = 390
          Visible = True
        end>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 93
    Width = 790
    Height = 266
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 1
    ExplicitWidth = 412
    object JvDBGrid3: TJvDBGrid
      Left = 1
      Top = 1
      Width = 788
      Height = 264
      Align = alClient
      DataSource = dtsFracR
      DrawingStyle = gdsClassic
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Visible = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'VLRMER'
          Title.Caption = 'Valor Mercadoria'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PESCAL'
          Title.Caption = 'Peso Calculado'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODMUN'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'UFD'
          Title.Caption = 'UF D'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOCALD'
          Title.Caption = 'LOCAL D'
          Visible = True
        end>
    end
    object JvDBGrid2: TJvDBGrid
      Left = 1
      Top = 1
      Width = 788
      Height = 264
      Align = alClient
      DataSource = dtsViagem
      DrawingStyle = gdsClassic
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Visible = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 17
      TitleRowHeight = 17
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'UFD'
          Title.Caption = 'UF D'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOCALD'
          Title.Caption = 'LOCAL D'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODMUN'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'DESCRI'
          Title.Caption = 'Cidade Destino'
          Visible = True
        end>
    end
  end
  object QManifesto: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'cte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filcte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'transp'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codman'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'serie'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct fr.descri veiculo, mo.codipv localo, mo.estado u' +
        'fo, m.operacao, mo.descri, op.fl_peso'
      
        'from tb_manifesto m left join tb_manitem i on (m.manifesto = i.m' +
        'anifesto and m.filial = i.filial and i.serie = m.serie)'
      
        '                    left join vw_itens_manifesto_sim z on (z.man' +
        'ifesto = m.manifesto and z.CODDOC = i.coddoc and z.fildoc = i.fi' +
        'ldoc)'
      
        '                    left join cyber.rodcli de on (de.codclifor =' +
        ' z.coddes)'
      
        '                    left join cyber.rodmun mo on mo.codmun = m.c' +
        'odmuno'
      
        '                    left join cyber.rodvei ve on ve.numvei = m.p' +
        'laca'
      
        '                    left join cyber.rodfro fr on fr.codfro = ve.' +
        'codfro'
      
        '                    left join tb_operacao op on m.operacao = op.' +
        'operacao'
      
        'where i.coddoc = :cte and i.fildoc = :filcte and m.transp = :tra' +
        'nsp and i.manifesto = :codman and i.serie = :serie'
      'and rownum = 1')
    Left = 241
    Top = 65528
    object QManifestoVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 100
    end
    object QManifestoLOCALO: TStringField
      FieldName = 'LOCALO'
      ReadOnly = True
      Size = 6
    end
    object QManifestoUFO: TStringField
      FieldName = 'UFO'
      ReadOnly = True
      Size = 2
    end
    object QManifestoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 60
    end
    object QManifestoDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
    object QManifestoFL_PESO: TStringField
      FieldName = 'FL_PESO'
      ReadOnly = True
      Size = 1
    end
  end
  object dtsManifesto: TDataSource
    DataSet = QManifesto
    Left = 285
    Top = 65528
  end
  object QRomaneio: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'codman'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filial'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'select distinct fr.descri veiculo, mo.codipv localo, mo.estado u' +
        'fo, mo.descri,  m.operacao'
      
        'from tb_ordem m left join tb_ordemi ic on (ic.id_ordem = m.id_or' +
        'dem and ic.filial = m.filial and rownum = 1)'
      
        '                left join cyber.rodocs oc on oc.codocs = ic.ocs ' +
        'and oc.filocs = ic.filial'
      
        '                left join cyber.rodcli cl on cl.codclifor = nvl(' +
        'oc.tercol,oc.codrem)'
      
        '                left join cyber.rodmun mo on mo.codmun = cl.codm' +
        'un'
      
        '                left join cyber.rodvei ve on (ve.numvei = m.plac' +
        'a and ve.situac = 1)'
      
        '                left join cyber.rodfro fr on fr.codfro = ve.codf' +
        'ro                     '
      'where m.id_ordem = :codman and m.filial = :filial'
      'and rownum = 1')
    Left = 233
    Top = 96
    object QRomaneioVEICULO: TStringField
      FieldName = 'VEICULO'
      ReadOnly = True
      Size = 8
    end
    object QRomaneioLOCALO: TStringField
      FieldName = 'LOCALO'
      ReadOnly = True
      Size = 6
    end
    object QRomaneioUFO: TStringField
      FieldName = 'UFO'
      ReadOnly = True
      Size = 2
    end
    object QRomaneioOPERACAO: TStringField
      FieldName = 'OPERACAO'
      ReadOnly = True
      Size = 30
    end
    object QRomaneioDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
  end
  object dtsRomaneio: TDataSource
    DataSet = QRomaneio
    Left = 295
    Top = 96
  end
  object QDocViagem: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'transp'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codman'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filial'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'serie'
        DataType = ftString
        Size = -1
        Value = ''
      end>
    SQL.Strings = (
      
        'select distinct md.codipv locald, md.estado ufd, md.codmun, md.d' +
        'escri'
      
        'from tb_manifesto m left join tb_manitem i on (m.manifesto = i.m' +
        'anifesto and m.filial = i.filial and i.serie = m.serie)'
      
        '                    left join vw_itens_manifesto_sim z on (z.man' +
        'ifesto = m.manifesto and z.CODDOC = i.coddoc'
      
        '                    and z.fildoc = i.fildoc and z.serie = m.seri' +
        'e)'
      
        '                    left join cyber.rodcli de on (de.codclifor =' +
        ' z.coddes)'
      
        '                    left join cyber.rodend te on (z.id_endent = ' +
        'te.id)'
      '                    left join cyber.rodmun md on  '
      
        '                        md.codmun = decode(nvl(m.codmund,0),0,(n' +
        'vl(te.codmun, de.codmun)),m.codmund)'
      
        'where m.transp = :transp and m.manifesto = :codman and m.filial ' +
        '= :filial and m.serie = :serie')
    Left = 241
    Top = 44
    object QDocViagemLOCALD: TStringField
      FieldName = 'LOCALD'
      ReadOnly = True
      Size = 6
    end
    object QDocViagemUFD: TStringField
      FieldName = 'UFD'
      ReadOnly = True
      Size = 2
    end
    object QDocViagemCODMUN: TBCDField
      FieldName = 'CODMUN'
      ReadOnly = True
      Precision = 32
    end
    object QDocViagemDESCRI: TStringField
      FieldName = 'DESCRI'
      ReadOnly = True
      Size = 40
    end
  end
  object dtsViagem: TDataSource
    DataSet = QDocViagem
    Left = 293
    Top = 44
  end
  object QDocFracR: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'cte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filcte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codman'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        '        select  (select sum(nf.vlrmer) from tb_cte_nf nf where c' +
        't.nr_conhecimento = nf.codcon and ct.fl_empresa = nf.fl_empresa)' +
        ' vlrmer, '
      '                case when nvl(peso_alterado,0) = 0 then '
      
        '                  (select case when nvl(sum(nf.pesokg),0) > nvl(' +
        'sum(nf.pescub),0) then sum(nf.pesokg) else sum(nf.pescub) end '
      
        '                  from tb_cte_nf nf where ct.nr_conhecimento = n' +
        'f.codcon and ct.fl_empresa = nf.fl_empresa)'
      '                else '
      '                 peso_alterado end pescal,        '
      '                '
      '                md.estado ufd, md.codmun, md.codipv locald'
      '                FROM vw_custo_transp_sim cc '
      
        '                             left join tb_conhecimento ct on (cc' +
        '.codcon = ct.nr_conhecimento and cc.codfil = ct.fl_empresa)'
      
        '                             left join cyber.rodfil de on (de.co' +
        'dfil = cc.filial)'
      
        '                             left join cyber.rodmun md on md.cod' +
        'mun = de.codmun'
      
        'where cc.codcon = :cte and cc.codfil = :filcte and cc.doc = :cod' +
        'man'
      
        'group by md.estado, md.codmun, md.codipv,peso_alterado,ct.nr_con' +
        'hecimento,ct.fl_empresa')
    Left = 233
    Top = 148
    object QDocFracRVLRMER: TBCDField
      FieldName = 'VLRMER'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
      Precision = 32
    end
    object QDocFracRPESCAL: TBCDField
      FieldName = 'PESCAL'
      ReadOnly = True
      DisplayFormat = '###,##0.000'
      Precision = 32
    end
    object QDocFracRUFD: TStringField
      FieldName = 'UFD'
      Size = 2
    end
    object QDocFracRCODMUN: TBCDField
      FieldName = 'CODMUN'
      Precision = 32
    end
    object QDocFracRLOCALD: TStringField
      FieldName = 'LOCALD'
      Size = 6
    end
  end
  object dtsFracR: TDataSource
    DataSet = QDocFracR
    Left = 285
    Top = 148
  end
  object QDocFracM: TADOQuery
    Connection = dtmDados.ConIntecom
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'cte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'filcte'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'codman'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT sum(ct.vlrmer) vlrmer, md.estado ufd, md.codmun, md.codip' +
        'v locald,'
      
        'case when nvl(peso_alterado,0) = 0 then sum(ct.pescal) else peso' +
        '_alterado end pescal'
      
        'from tb_manifesto m left join tb_manitem i on (m.manifesto = i.m' +
        'anifesto and m.filial = i.filial and i.serie = m.serie)'
      
        '                    left join vw_itens_manifesto_sim ct on (ct.m' +
        'anifesto = m.manifesto and ct.CODDOC = i.coddoc and ct.fildoc = ' +
        'i.fildoc)'
      
        '                    left join vw_custo_transp_sim cc on (cc.codc' +
        'on = ct.CODDOC and cc.codfil = ct.fildoc and cc.doc = ct.manifes' +
        'to)'
      
        '                    left join cyber.rodcli de on (de.codclifor =' +
        ' ct.coddes)'
      
        '                    left join cyber.rodmun md on md.codmun = de.' +
        'codmun'
      
        'where ct.coddoc = :cte and ct.fildoc = :filcte and cc.doc = :cod' +
        'man'
      'group by md.estado, md.codmun, md.codipv,peso_alterado')
    Left = 233
    Top = 216
    object BCDField1: TBCDField
      FieldName = 'VLRMER'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
      Precision = 32
    end
    object BCDField2: TBCDField
      FieldName = 'PESCAL'
      ReadOnly = True
      DisplayFormat = '###,##0.000'
      Precision = 32
    end
    object StringField1: TStringField
      FieldName = 'UFD'
      Size = 2
    end
    object BCDField3: TBCDField
      FieldName = 'CODMUN'
      Precision = 32
    end
    object StringField2: TStringField
      FieldName = 'LOCALD'
      Size = 6
    end
  end
  object dtsFracM: TDataSource
    DataSet = QDocFracM
    Left = 285
    Top = 216
  end
end
