unit CadTabelaControlerVenda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, DB, ADODB, ActnList, JvExStdCtrls,
  JvCombobox, JvToolEdit, JvBaseEdits, Mask, JvExMask, JvMaskEdit, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvDBUltimGrid, ComCtrls, JvMemo, JvDBControls,
  ShellApi, JvEdit, System.Actions, ComObj, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase,
  IdSMTP, IdBaseComponent, IdMessage;

type
  TfrmCadTabelaControlerVenda = class(TForm)
    PageControl1: TPageControl;
    Consulta: TTabSheet;
    dtstabela: TDataSource;
    Detalhes: TTabSheet;
    dtsItem: TDataSource;
    dbgContrato: TJvDBUltimGrid;
    act: TActionList;
    acPrimeiroApanha: TAction;
    acAnteriorApanha: TAction;
    acProximoApanha: TAction;
    acUltimoApanha: TAction;
    QTabela: TADOQuery;
    QItem: TADOQuery;
    Label6: TLabel;
    Label2: TLabel;
    edTrans: TJvMaskEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edFreteM: TJvCalcEdit;
    edPedagio: TJvCalcEdit;
    edOutros: TJvCalcEdit;
    edOutrosP: TJvCalcEdit;
    edOutrosM: TJvCalcEdit;
    edExc: TJvCalcEdit;
    EdSeg: TJvCalcEdit;
    EdGris: TJvCalcEdit;
    EdGrisM: TJvCalcEdit;
    Qcadtabela: TADOQuery;
    QItemCOD_TAB: TBCDField;
    QItemNR_PESO_DE: TBCDField;
    QItemNR_PESO_ATE: TBCDField;
    QItemVL_RODOVIARIO: TBCDField;
    QItemDATAI: TDateTimeField;
    QItemDATAF: TDateTimeField;
    QItemVR_CTRC: TBCDField;
    QItemVR_EXCED: TBCDField;
    Label14: TLabel;
    Label18: TLabel;
    eddespacho: TJvCalcEdit;
    cbImposto: TCheckBox;
    edsegminimo: TJvCalcEdit;
    Label19: TLabel;
    cbPeso: TCheckBox;
    Panel2: TPanel;
    lblQuant: TLabel;
    btnFechar: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    Label20: TLabel;
    edVeiculo: TJvMaskEdit;
    Label21: TLabel;
    Label1: TLabel;
    Label22: TLabel;
    edTDE: TJvCalcEdit;
    Label23: TLabel;
    edTR: TJvCalcEdit;
    Label25: TLabel;
    edTDEp: TJvCalcEdit;
    Label26: TLabel;
    edTDEm: TJvCalcEdit;
    cbStatus: TCheckBox;
    Label28: TLabel;
    Label30: TLabel;
    edTdeMax: TJvCalcEdit;
    QTabelaCOD_VENDA: TBCDField;
    QTabelaCOD_CLIENTE: TBCDField;
    QTabelaFL_LOCAL: TStringField;
    QTabelaDS_UF: TStringField;
    QTabelaFL_LOCAL_DES: TStringField;
    QTabelaDS_UF_DES: TStringField;
    QTabelaVL_FRETE_MINIMO: TBCDField;
    QTabelaVL_PEDAGIO: TBCDField;
    QTabelaVL_OUTROS: TBCDField;
    QTabelaVL_OUTROS_MINIMO: TBCDField;
    QTabelaVL_EXCEDENTE: TBCDField;
    QTabelaVL_AD: TFloatField;
    QTabelaVL_GRIS: TFloatField;
    QTabelaVL_GRIS_MINIMO: TFloatField;
    QTabelaVL_ENTREGA: TBCDField;
    QTabelaSEGURO_MINIMO: TFloatField;
    QTabelaFL_PESO: TStringField;
    QTabelaFL_IMPOSTO: TStringField;
    QTabelaMODAL: TStringField;
    QTabelaVEICULO: TStringField;
    QTabelaSITE: TBCDField;
    QTabelaUSUARIO: TStringField;
    QTabelaDT_CADASTRO: TDateTimeField;
    QTabelaCODMUN: TBCDField;
    QTabelaCODMUNO: TBCDField;
    QTabelaVL_TDE: TBCDField;
    QTabelaVL_TR: TBCDField;
    QTabelaVL_TDEP: TBCDField;
    QTabelaVL_TDEM: TBCDField;
    QTabelaFL_STATUS: TStringField;
    QTabelaOPERACAO: TStringField;
    QTabelaVL_TDEMAX: TBCDField;
    QTabelaNM_CLIENTE: TStringField;
    QTabelaLOCAL: TStringField;
    QTabelaLOCAL_DES: TStringField;
    QcadtabelaCOD_VENDA: TBCDField;
    QcadtabelaCOD_CLIENTE: TBCDField;
    QcadtabelaFL_LOCAL: TStringField;
    QcadtabelaDS_UF: TStringField;
    QcadtabelaFL_LOCAL_DES: TStringField;
    QcadtabelaDS_UF_DES: TStringField;
    QcadtabelaVL_FRETE_MINIMO: TBCDField;
    QcadtabelaVL_PEDAGIO: TBCDField;
    QcadtabelaVL_OUTROS: TBCDField;
    QcadtabelaVL_OUTROS_MINIMO: TBCDField;
    QcadtabelaVL_EXCEDENTE: TBCDField;
    QcadtabelaVL_AD: TFloatField;
    QcadtabelaVL_GRIS: TFloatField;
    QcadtabelaVL_GRIS_MINIMO: TFloatField;
    QcadtabelaVL_ENTREGA: TBCDField;
    QcadtabelaSEGURO_MINIMO: TFloatField;
    QcadtabelaFL_PESO: TStringField;
    QcadtabelaFL_IMPOSTO: TStringField;
    QcadtabelaMODAL: TStringField;
    QcadtabelaVEICULO: TStringField;
    QcadtabelaSITE: TBCDField;
    QcadtabelaUSUARIO: TStringField;
    QcadtabelaDT_CADASTRO: TDateTimeField;
    QcadtabelaCODMUN: TBCDField;
    QcadtabelaCODMUNO: TBCDField;
    QcadtabelaVL_TDE: TBCDField;
    QcadtabelaVL_TR: TBCDField;
    QcadtabelaVL_TDEP: TBCDField;
    QcadtabelaVL_TDEM: TBCDField;
    QcadtabelaFL_STATUS: TStringField;
    QcadtabelaOPERACAO: TStringField;
    QcadtabelaVL_TDEMAX: TBCDField;
    QItemCOD_VENDA: TBCDField;
    QItemUSUARIO: TStringField;
    QItemDATAINC: TDateTimeField;
    QItemUSUALT: TStringField;
    QItemDATAALT: TDateTimeField;
    QTabelaOBS: TStringField;
    QcadtabelaOBS: TStringField;
    Label27: TLabel;
    Obs: TJvMemo;
    Label31: TLabel;
    QTabelaREGIAO: TStringField;
    QcadtabelaREGIAO: TStringField;
    Label32: TLabel;
    Label33: TLabel;
    ed_fluvial: TJvCalcEdit;
    ed_fluv_min: TJvCalcEdit;
    Label34: TLabel;
    edseg_balsa: TJvCalcEdit;
    Label35: TLabel;
    edred_fluv: TJvCalcEdit;
    Label36: TLabel;
    edagend: TJvCalcEdit;
    Label38: TLabel;
    edtas: TJvCalcEdit;
    TabSheet2: TTabSheet;
    DBGrid1: TJvDBUltimGrid;
    Label3: TLabel;
    edentrega: TJvCalcEdit;
    Label39: TLabel;
    edalfand: TJvCalcEdit;
    QTabelaVL_OUTROS_P: TBCDField;
    QTabelaVL_SEG_BALSA: TBCDField;
    QTabelaVL_REDEP_FLUVIAL: TBCDField;
    QTabelaVL_AGEND: TBCDField;
    QTabelaVL_PALLET: TBCDField;
    QTabelaVL_TAS: TBCDField;
    QTabelaVL_DESPACHO: TBCDField;
    QTabelaVL_ENTREGA_PORTO: TBCDField;
    QTabelaVL_ALFAND: TBCDField;
    QTabelaVL_CANHOTO: TBCDField;
    QcadtabelaVL_OUTROS_P: TBCDField;
    QcadtabelaVL_SEG_BALSA: TBCDField;
    QcadtabelaVL_REDEP_FLUVIAL: TBCDField;
    QcadtabelaVL_AGEND: TBCDField;
    QcadtabelaVL_PALLET: TBCDField;
    QcadtabelaVL_TAS: TBCDField;
    QcadtabelaVL_DESPACHO: TBCDField;
    QcadtabelaVL_ENTREGA_PORTO: TBCDField;
    QcadtabelaVL_ALFAND: TBCDField;
    QcadtabelaVL_CANHOTO: TBCDField;
    QTabelaVL_FLUVIAL: TBCDField;
    QTabelaVL_FLUVIAL_M: TBCDField;
    QcadtabelaVL_FLUVIAL: TBCDField;
    QcadtabelaVL_FLUVIAL_M: TBCDField;
    edDtI: TJvDateEdit;
    edDtF: TJvDateEdit;
    Label41: TLabel;
    Label42: TLabel;
    QTabelaDT_INICIAL: TDateTimeField;
    QTabelaDT_FINAL: TDateTimeField;
    QcadtabelaDT_INICIAL: TDateTimeField;
    QcadtabelaDT_FINAL: TDateTimeField;
    QTabelaCONTROLER: TStringField;
    QTabelaDT_CONTROLER: TDateTimeField;
    btnAprovar: TBitBtn;
    QcadtabelaCONTROLER: TStringField;
    QcadtabelaDT_CONTROLER: TDateTimeField;
    JvDBMaskEdit1: TJvDBMaskEdit;
    JvDBMaskEdit7: TJvDBMaskEdit;
    JvDBMaskEdit3: TJvDBMaskEdit;
    JvDBMaskEdit4: TJvDBMaskEdit;
    JvDBMaskEdit6: TJvDBMaskEdit;
    JvDBMaskEdit2: TJvDBMaskEdit;
    JvDBMaskEdit5: TJvDBMaskEdit;
    JvDBMaskEdit8: TJvDBMaskEdit;
    QTabelaCIDD: TStringField;
    QTabelaCIDO: TStringField;
    Label43: TLabel;
    edDevol: TJvCalcEdit;
    cbRateio: TCheckBox;
    QcadtabelaVL_DEVOLUCAO: TBCDField;
    QcadtabelaFL_RATEIO: TStringField;
    QTabelaVL_DEVOLUCAO: TBCDField;
    QTabelaFL_RATEIO: TStringField;
    Label44: TLabel;
    edFluvMin: TJvCalcEdit;
    QcadtabelaVL_FLUVMIN: TBCDField;
    QcadtabelaVL_AJUDA: TBCDField;
    QTabelaVL_FLUVMIN: TBCDField;
    QTabelaVL_AJUDA: TBCDField;
    btnVer: TBitBtn;
    btnDcto: TBitBtn;
    OpenDialog: TOpenDialog;
    QcadtabelaDOC_CONTROLER: TStringField;
    QcadtabelaVL_REENT_MINIMA: TBCDField;
    btnDesativar: TBitBtn;
    edUser: TJvEdit;
    edDtIns: TJvDateEdit;
    btnListagem: TBitBtn;
    Qcadtabelareprovado: TStringField;
    Label15: TLabel;
    obsRepro: TJvMemo;
    IdMessage1: TIdMessage;
    IdSMTP1: TIdSMTP;
    qtelas: TADOQuery;
    qtelasDESCRICAO: TStringField;
    qtelasEMAIL: TStringField;
    QcadtabelaCONTROLER_REPR: TStringField;
    edDiaria: TJvCalcEdit;
    Label57: TLabel;
    edDedicado: TJvCalcEdit;
    edPernoite: TJvCalcEdit;
    Label61: TLabel;
    Label59: TLabel;
    edfretepeso: TJvCalcEdit;
    Label62: TLabel;
    edtaxatrt: TJvCalcEdit;
    Label64: TLabel;
    QTabelaFL_FRETE: TStringField;
    QTabelaFL_PEDAGIO: TStringField;
    QTabelaFL_EXCEDENTE: TStringField;
    QTabelaFL_AD: TStringField;
    QTabelaFL_GRIS: TStringField;
    QTabelaFL_ENTREGA: TStringField;
    QTabelaFL_SEGURO: TStringField;
    QTabelaFL_TDE: TStringField;
    QTabelaFL_TR: TStringField;
    QTabelaFL_SEG_BALSA: TStringField;
    QTabelaFL_REDEP_FLUVIAL: TStringField;
    QTabelaFL_AGEND: TStringField;
    QTabelaFL_PALLET: TStringField;
    QTabelaFL_TAS: TStringField;
    QTabelaFL_DESPACHO: TStringField;
    QTabelaFL_ENTREGA_PORTO: TStringField;
    QTabelaFL_ALFAND: TStringField;
    QTabelaFL_CANHOTO: TStringField;
    QTabelaFL_FLUVIAL: TStringField;
    QTabelaFL_DEVOLUCAO: TStringField;
    QTabelaFL_AJUDA: TStringField;
    QTabelaFL_CTE: TStringField;
    QTabelaFL_NF_FRETE: TStringField;
    QTabelaFL_NF_PEDAGIO: TStringField;
    QTabelaFL_NF_EXCEDENTE: TStringField;
    QTabelaFL_NF_AD: TStringField;
    QTabelaFL_NF_GRIS: TStringField;
    QTabelaFL_NF_ENTREGA: TStringField;
    QTabelaFL_NF_SEGURO: TStringField;
    QTabelaFL_NF_TDE: TStringField;
    QTabelaFL_NF_TR: TStringField;
    QTabelaFL_NF_SEG_BALSA: TStringField;
    QTabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QTabelaFL_NF_AGEND: TStringField;
    QTabelaFL_NF_PALLET: TStringField;
    QTabelaFL_NF_TAS: TStringField;
    QTabelaFL_NF_DESPACHO: TStringField;
    QTabelaFL_NF_ENTREGA_PORTO: TStringField;
    QTabelaFL_NF_ALFAND: TStringField;
    QTabelaFL_NF_CANHOTO: TStringField;
    QTabelaFL_NF_FLUVIAL: TStringField;
    QTabelaFL_NF_DEVOLUCAO: TStringField;
    QTabelaFL_NF_AJUDA: TStringField;
    QTabelaFL_NF_CTE: TStringField;
    QTabelaVL_REENT_MINIMA: TBCDField;
    QTabelaDOC_CONTROLER: TStringField;
    QTabelaREPROVADO: TStringField;
    QTabelaCONTROLER_REPR: TStringField;
    QTabelaVL_DIARIA: TBCDField;
    QTabelaVL_REENTREGA: TBCDField;
    QTabelaVL_DEDICADO: TBCDField;
    QTabelaVL_PERNOITE: TBCDField;
    QTabelaVL_HORA_PARADA: TBCDField;
    QTabelaFL_IMPOSTO_ICMS: TStringField;
    QTabelaVL_FRETE_VALOR: TFloatField;
    QTabelaVL_TAXA_TRT: TFloatField;
    QcadtabelaFL_FRETE: TStringField;
    QcadtabelaFL_PEDAGIO: TStringField;
    QcadtabelaFL_EXCEDENTE: TStringField;
    QcadtabelaFL_AD: TStringField;
    QcadtabelaFL_GRIS: TStringField;
    QcadtabelaFL_ENTREGA: TStringField;
    QcadtabelaFL_SEGURO: TStringField;
    QcadtabelaFL_TDE: TStringField;
    QcadtabelaFL_TR: TStringField;
    QcadtabelaFL_SEG_BALSA: TStringField;
    QcadtabelaFL_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_AGEND: TStringField;
    QcadtabelaFL_PALLET: TStringField;
    QcadtabelaFL_TAS: TStringField;
    QcadtabelaFL_DESPACHO: TStringField;
    QcadtabelaFL_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_ALFAND: TStringField;
    QcadtabelaFL_CANHOTO: TStringField;
    QcadtabelaFL_FLUVIAL: TStringField;
    QcadtabelaFL_DEVOLUCAO: TStringField;
    QcadtabelaFL_AJUDA: TStringField;
    QcadtabelaFL_CTE: TStringField;
    QcadtabelaFL_NF_FRETE: TStringField;
    QcadtabelaFL_NF_PEDAGIO: TStringField;
    QcadtabelaFL_NF_EXCEDENTE: TStringField;
    QcadtabelaFL_NF_AD: TStringField;
    QcadtabelaFL_NF_GRIS: TStringField;
    QcadtabelaFL_NF_ENTREGA: TStringField;
    QcadtabelaFL_NF_SEGURO: TStringField;
    QcadtabelaFL_NF_TDE: TStringField;
    QcadtabelaFL_NF_TR: TStringField;
    QcadtabelaFL_NF_SEG_BALSA: TStringField;
    QcadtabelaFL_NF_REDEP_FLUVIAL: TStringField;
    QcadtabelaFL_NF_AGEND: TStringField;
    QcadtabelaFL_NF_PALLET: TStringField;
    QcadtabelaFL_NF_TAS: TStringField;
    QcadtabelaFL_NF_DESPACHO: TStringField;
    QcadtabelaFL_NF_ENTREGA_PORTO: TStringField;
    QcadtabelaFL_NF_ALFAND: TStringField;
    QcadtabelaFL_NF_CANHOTO: TStringField;
    QcadtabelaFL_NF_FLUVIAL: TStringField;
    QcadtabelaFL_NF_DEVOLUCAO: TStringField;
    QcadtabelaFL_NF_AJUDA: TStringField;
    QcadtabelaFL_NF_CTE: TStringField;
    QcadtabelaVL_DIARIA: TBCDField;
    QcadtabelaVL_REENTREGA: TBCDField;
    QcadtabelaVL_DEDICADO: TBCDField;
    QcadtabelaVL_PERNOITE: TBCDField;
    QcadtabelaVL_HORA_PARADA: TBCDField;
    QcadtabelaFL_IMPOSTO_ICMS: TStringField;
    QcadtabelaVL_FRETE_VALOR: TFloatField;
    QcadtabelaVL_TAXA_TRT: TFloatField;
    TabSheet1: TTabSheet;
    Panel9: TPanel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    LBLCanhoto: TLabel;
    edpallet: TJvCalcEdit;
    edAjuda: TJvCalcEdit;
    edReentrega: TJvCalcEdit;
    edArmazem: TJvCalcEdit;
    edHoraParada: TJvCalcEdit;
    edcanhoto: TJvCalcEdit;
    dsGenItem: TDataSource;
    QGenItem: TADOQuery;
    QGenItemCOD_ID: TFMTBCDField;
    QGenItemCOD_VENDA: TFMTBCDField;
    QGenItemVL_PERNOITE: TBCDField;
    QGenItemVL_VD: TBCDField;
    QGenItemVL_DIARIA: TBCDField;
    QGenItemVEICULO: TStringField;
    Label37: TLabel;
    edAgendG: TJvCalcEdit;
    Qcadtabelavl_agendg: TBCDField;
    QItemVL_MINIMO: TBCDField;
    cbImpostoICMS: TCheckBox;
    QTabelaVL_AGENDG: TBCDField;
    QTabelaVL_DIARIA48: TBCDField;
    QTabelaVL_DEVOLUCAOP: TBCDField;
    QTabelaVL_REENTREGAP: TBCDField;
    QcadtabelaVL_DIARIA48: TBCDField;
    QcadtabelaVL_DEVOLUCAOP: TBCDField;
    QcadtabelaVL_REENTREGAP: TBCDField;
    QcadtabelaVL_6X1: TBCDField;
    Label16: TLabel;
    ed6x1: TJvCalcEdit;
    Label17: TLabel;
    edDiaria48: TJvCalcEdit;
    PageControl2: TPageControl;
    tsVeiculo: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    Panel6: TPanel;
    DBNavigator2: TDBNavigator;
    tsReentrega: TTabSheet;
    JvDBUltimGrid1: TJvDBUltimGrid;
    Panel10: TPanel;
    DBNavigator3: TDBNavigator;
    btnInserirRee: TBitBtn;
    btnSalvarRee: TBitBtn;
    btnAlterarRee: TBitBtn;
    btnExcluirRee: TBitBtn;
    btnCancelarRee: TBitBtn;
    QItemRee: TADOQuery;
    QItemReeCOD_TAB: TFMTBCDField;
    QItemReeCOD_VENDA: TFMTBCDField;
    QItemReeNR_PESO_DE: TBCDField;
    QItemReeNR_PESO_ATE: TBCDField;
    QItemReeVL_FRETE: TBCDField;
    QItemReeDATAI: TDateTimeField;
    QItemReeDATAF: TDateTimeField;
    QItemReeVR_CTE: TBCDField;
    QItemReeVR_EXCED: TBCDField;
    QItemReeUSUARIO: TStringField;
    QItemReeDATAINC: TDateTimeField;
    QItemReeUSUALT: TStringField;
    QItemReeDATAALT: TDateTimeField;
    QItemReeVL_MINIMO: TBCDField;
    dtsItemRee: TDataSource;
    Label45: TLabel;
    edqt6x1: TJvCalcEdit;
    QcadtabelaQT6x1: TBCDField;
    tsPesquisa: TTabSheet;
    Label24: TLabel;
    cbPesqCliente: TComboBox;
    Label29: TLabel;
    edData: TJvDateEdit;
    btProcurar: TBitBtn;
    btRetirarFiltro: TBitBtn;
    QCliente: TADOQuery;
    QClienteCOD_CLIENTE: TFMTBCDField;
    QClienteNM_CLIENTE: TStringField;
    btnAprovarTodos: TBitBtn;
    tsAereo: TTabSheet;
    JvDBUltimGrid2: TJvDBUltimGrid;
    QAereo: TADOQuery;
    QAereoCOD_TAB: TFMTBCDField;
    QAereoCOD_VENDA: TFMTBCDField;
    QAereoPESO_DE: TBCDField;
    QAereoPESO_ATE: TBCDField;
    QAereoVALOR_COLETA: TBCDField;
    QAereoVALOR_ENTREGA: TBCDField;
    QAereoDATAI: TDateTimeField;
    QAereoDATAF: TDateTimeField;
    QAereoVR_EXCED: TBCDField;
    QAereoUSUARIO: TStringField;
    QAereoDATAINC: TDateTimeField;
    QAereoUSUALT: TStringField;
    QAereoDATAALT: TDateTimeField;
    dtsAereo: TDataSource;
    QcadtabelaVL_ARMAZENAGEM: TFMTBCDField;
    QGenItemVL_PALETE: TBCDField;
    Label46: TLabel;
    edRMinimo: TJvCalcEdit;
    Label40: TLabel;
    cbPesqUFO: TComboBox;
    Label47: TLabel;
    cbPesqUFD: TComboBox;
    Label48: TLabel;
    cbVeiculoPesq: TComboBox;
    Label49: TLabel;
    cbOperacaoPesq: TComboBox;
    cbPesqLD: TComboBox;
    cbPesqLO: TComboBox;
    Label53: TLabel;
    cbCidOPesq: TComboBox;
    Label54: TLabel;
    cbCidDPesq: TComboBox;
    QCidade: TADOQuery;
    QCidadeCODMUN: TBCDField;
    QCidadeDESCRI: TStringField;
    QTabelaVL_6X1: TBCDField;
    QTabelaQT6X1: TFMTBCDField;
    QTabelaVL_ARMAZENAGEM: TFMTBCDField;
    Label50: TLabel;
    edTDA: TJvCalcEdit;
    Label51: TLabel;
    edTDAp: TJvCalcEdit;
    Label52: TLabel;
    edTDAmin: TJvCalcEdit;
    QTabelaVL_TDA: TBCDField;
    QTabelaVL_TDAP: TBCDField;
    QTabelaVL_TDAmin: TBCDField;

    procedure btnFecharClick(Sender: TObject);
    procedure dbgContratoTitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgContratoDblClick(Sender: TObject);
    procedure acPrimeiroApanhaExecute(Sender: TObject);
    procedure QTabelaAfterScroll(DataSet: TDataSet);
    procedure btnAprovarClick(Sender: TObject);
    procedure btnDctoClick(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnDesativarClick(Sender: TObject);
    procedure btnListagemClick(Sender: TObject);
    procedure btProcurarClick(Sender: TObject);
    procedure btRetirarFiltroClick(Sender: TObject);
    procedure btnAprovarTodosClick(Sender: TObject);
    procedure cbPesqLOChange(Sender: TObject);
    procedure cbPesqLDChange(Sender: TObject);
  private
    quant: integer;
    sql : string;
  public
    { Public declarations }
  end;

var
  frmCadTabelaControlerVenda: TfrmCadTabelaControlerVenda;

implementation

uses Dados, funcoes, Menu;

{$R *.DFM}

procedure TfrmCadTabelaControlerVenda.btnAprovarClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  if Application.Messagebox('Aprova esta Tabela ?', 'Aprovar Tabela de Receita',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Qcadtabela.edit;
    Qcadtabelareprovado.Value := '';
    QcadtabelaCONTROLER.Value := glbuser;
    QcadtabelaDT_CONTROLER.Value := now;
    Qcadtabela.Post;
    QTabela.Close;
    QTabela.Open;
    quant := QTabela.RecordCount;
    lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  end;
end;

procedure TfrmCadTabelaControlerVenda.btnAprovarTodosClick(Sender: TObject);
begin
  if not dtmDados.PodeInserir(name) then
    Exit;
  if Application.Messagebox('Aprova Todas estas Tabelas da Tela ?', 'Aprovar Totas as Tabelas da Tela de Receita',
    MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    QTabela.First;
    while not Qtabela.eof do
    begin
      Qcadtabela.edit;
      Qcadtabelareprovado.Value := '';
      QcadtabelaCONTROLER.Value := glbuser;
      QcadtabelaDT_CONTROLER.Value := now;
      Qcadtabela.Post;
      Qtabela.Next;
    end;
    showmessage('Aprova��o Realizada com Sucesso');
  end;
end;

procedure TfrmCadTabelaControlerVenda.btnDctoClick(Sender: TObject);
var
  ori, des, caminho, arquivo, altera: string;
begin
  caminho := GLBControler;
  if OpenDialog.Execute then
  begin
    try
      ori := OpenDialog.FileName;
      des := caminho + ExtractFileName(OpenDialog.FileName);
      arquivo := caminho + 'R_' + QcadtabelaCOD_VENDA.AsString +
        ExtractFileName(OpenDialog.FileName);
      // verifica se existe este arquivo j� gravado, sim renomeia
      if FileExists(arquivo) then
      begin
        altera := copy(arquivo, 1, length(arquivo) - 4) + 'A' +
          apcarac(DateToStr(now())) + '.pdf';
        RenameFile(arquivo, altera);
      end;
      CopyFile(Pchar(ori), Pchar(des), True);

      RenameFile(des, arquivo);

      dtmDados.IQuery1.close;
      dtmDados.IQuery1.SQL.Clear;
      dtmDados.IQuery1.SQL.add
        ('update tb_tabelavenda set doc_controler = :c where cod_venda = :n');
      dtmDados.IQuery1.Parameters[0].Value := 'R_' +
        QcadtabelaCOD_VENDA.AsString + ExtractFileName(OpenDialog.FileName);
      dtmDados.IQuery1.Parameters[1].Value := QcadtabelaCOD_VENDA.AsInteger;
      dtmDados.IQuery1.ExecSQL;
      dtmDados.IQuery1.close;

    except
      on e: Exception do
      begin
        ShowMessage(e.message);
      end;
    end;
  end;
end;

procedure TfrmCadTabelaControlerVenda.btnDesativarClick(Sender: TObject);
begin
  if not dtmDados.PodeAlterar(name) then
    Exit;

  if length(obsRepro.Text) < 10 then
  begin
    ShowMessage
      ('� Obrigat�rio a Explica��o do Porque est� Sendo Desativada !!');
    Exit;
  end;

  if Application.Messagebox('Desativar esta Tabela ?',
    'Desativar Tabela de Receita', MB_YESNO + MB_ICONQUESTION) = IDYES then
  begin
    Qcadtabela.edit;
    Qcadtabelareprovado.Value := obsRepro.Text;
    QcadtabelaFL_STATUS.Value := 'N';
    QcadtabelaCONTROLER_REPR.Value := glbuser;
    Qcadtabela.Post;
  end;

  // Envio de email com a mensagem da tabela que foi reprovada pela controladoria
  qtelas.Open;
  IdMessage1.Recipients.EMailAddresses := qtelasEMAIL.AsString;
  IdMessage1.Subject := 'Tabela de Receita ' +
    IntToStr(QcadtabelaCOD_VENDA.AsInteger) + ' foi Reprovada';
  IdMessage1.Body.add('<html>');
  IdMessage1.Body.add('    <body>');
  IdMessage1.Body.add('<table border="1">');
  IdMessage1.Body.add('<center><tr>');
  IdMessage1.Body.add('<td bgcolor=''#00FFFF''>Numero da Tabela</td>');
  IdMessage1.Body.add
    ('<td bgcolor=''#00FFFF''>Motivo da Reprova&ccedil;&atilde;o</td>');
  IdMessage1.Body.add('<td bgcolor=''#00FFFF''>Reprovada por</td>');
  IdMessage1.Body.add('</tr></center>');
  IdMessage1.Body.add('<tr>');
  IdMessage1.Body.add('<center><td>' + IntToStr(QcadtabelaCOD_VENDA.AsInteger)
    + '</td>');
  IdMessage1.Body.add('<td>' + obsRepro.Text + '</td></center>');
  IdMessage1.Body.add('<td>' + glbuser + '</td></center>');
  IdMessage1.Body.add('</tr>');
  IdMessage1.Body.add('</table>');
  IdMessage1.Body.add('</body>');
  IdMessage1.Body.add('</html>');
  IdMessage1.MessageParts.Clear;
  qtelas.close;
  IdSMTP1.Connect;

  Try
    IdSMTP1.Send(IdMessage1);
  Finally
    IdSMTP1.Disconnect;
  End;

end;

procedure TfrmCadTabelaControlerVenda.btnFecharClick(Sender: TObject);
begin
  close;
end;

procedure TfrmCadTabelaControlerVenda.btnListagemClick(Sender: TObject);
var
  memo1: TStringList;
  texto: string;
  wb: Variant;
  i: integer;
begin
  memo1 := TStringList.Create;
  texto := self.Caption;

  memo1.Clear();
  memo1.add('<HTML><HEAD><TITLE>' + texto + '</TITLE>');
  memo1.add(
    '<STYLE>.titulo1 {FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add
    ('       .titulo2 {	FONT: bold 18px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000}');
  memo1.add
    ('       .titulo3 {	FONT: bold 22px Verdana, Arial, Helvetica, sans-serif; COLOR: blue}');
  memo1.add(
    '       .texto1 {	FONT: 14px Verdana, Arial, Helvetica, sans-serif; COLOR: navy}');
  memo1.add(
    '       .texto2 {	FONT: 12px Arial, Helvetica, sans-serif; COLOR: #00000}');
  memo1.add(
    '       .texto3 {	FONT: 12px Verdana, Arial, Helvetica, sans-serif; COLOR: red}');
  memo1.add('</STYLE>');
  memo1.add('<META content="MSHTML 6.00.6000.16788" name=GENERATOR></HEAD>');
  memo1.add('<DIV style="FONT: 10pt arial">&nbsp;</DIV></FONT></DIV>');
  memo1.add('<table border=0><title></title></head><body>');
  memo1.add('<table border=1 align=center>');
  memo1.add('<tr>');
  memo1.add('<th colspan=2><FONT class=titulo1>' + texto + '</th></font>');
  memo1.add('<tr>');
  memo1.add('</th></font></Center>');
  memo1.add('</tr>');
  memo1.add('<Table border=2 bordercolor="#005CB9" align=center>');
  memo1.add('<TBODY>');
  memo1.add('<tr>');
  for i := 0 to dbgContrato.Columns.Count - 1 do
  begin
    memo1.add('<th><FONT class=texto1>' + dbgContrato.Columns[i].Title.Caption
      + '</th>');
  end;
  memo1.add('</tr>');
  QTabela.First;
  while not QTabela.Eof do
  begin
    memo1.add('<tr>');
    for i := 0 to dbgContrato.Columns.Count - 1 do
      memo1.add('<th><FONT class=texto2>' + QTabela.fieldbyname
        (dbgContrato.Columns[i].FieldName).AsString + '</th>');
    QTabela.Next;
    memo1.add('</tr>');
  end;
  memo1.add('</TBODY>');
  memo1.add('</table>');
  memo1.SaveToFile('C:/SIM/ControlerReceita.html');

  wb := CreateOleObject('InternetExplorer.Application');
  wb.Visible := True;
  wb.Navigate('C:/SIM/ControlerReceita.html');
end;

procedure TfrmCadTabelaControlerVenda.btnVerClick(Sender: TObject);
var
  caminho: string;
begin
  if QcadtabelaDOC_CONTROLER.AsString <> '' then
  begin
    caminho := GLBControler + QcadtabelaDOC_CONTROLER.AsString;
    ShellExecute(Handle, nil, Pchar(caminho), nil, nil, SW_SHOWNORMAL);
  end
  else
    ShowMessage('Tabela Sem Documenta��o Vinculada !!');
end;

procedure TfrmCadTabelaControlerVenda.btProcurarClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QTabela.Close;

  if cbPesqCliente.Text <> '' then
  begin
    QTabela.sql[20] := 'and (f.nomeab || '' - '' || f.codcgc) = ' + #39 +
      cbPesqCliente.Text + #39;
  end;

  if copy(edData.Text,1,2) <> '  ' then
    QTabela.sql[21] := 'and trunc(dataf) = to_date(' + #39 + formatdatetime('dd/mm/yyyy',edData.Date) + #39 + ',''dd/mm/yyyy'')' ;

  if cbVeiculoPesq.Text <> '' then
  begin
   QTabela.sql[22] := 'and t.veiculo = ' + #39 + cbVeiculoPesq.Text + #39;
  end;
  if cbPesqUFO.Text <> '' then
  begin
    QTabela.sql[23] := 'and t.ds_uf = ' + #39 + cbPesqUFO.Text + #39;
  end;
  if cbPesqUFD.Text <> '' then
  begin
    QTabela.sql[24] := 'and t.ds_uf_des = ' + #39 + cbPesqUFD.Text + #39;
  end;
  if cbPesqLO.Text <> '' then
  begin
    QTabela.sql[25] := 'and t.fl_local = ' + #39 + copy(cbPesqLO.Text, 1, 1) + #39;
    if cbCidOPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFO.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidOPesq.Text, []);
      QTabela.sql.add('and t.codmuno = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbPesqLD.Text <> '' then
  begin
    QTabela.sql[26] := 'and t.fl_local_des = ' + #39 + copy(cbPesqLD.Text, 1, 1) + #39;
    if cbCidDPesq.Text <> '' then
    begin
      QCidade.Close;
      QCidade.Parameters[0].value := cbPesqUFD.Text;
      QCidade.sql[4] := '';
      QCidade.Open;
      QCidade.Locate('DESCRI', cbCidDPesq.Text, []);
      QTabela.sql.add('and t.codmun = ' + #39 + QCidadeCODMUN.AsString + #39);
    end;
  end;
  if cbOperacaoPesq.Text <> '' then
  begin
    QTabela.sql[27] := 'and t.operacao = ' + #39 + (cbOperacaoPesq.Text) + #39;
  end;

  //showmessage(Qtabela.sql.Text);
  QTabela.Open;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
  if QTabela.Eof then
  begin
    ShowMessage('N�o Encontrado Nenhum Resultado Para Esta Pesquisa !!');
  end
  else
  begin
    PageControl1.ActivePageIndex := 0;
  end;
end;

procedure TfrmCadTabelaControlerVenda.btRetirarFiltroClick(Sender: TObject);
begin
  cbPesqUFO.ItemIndex := -1;
  cbPesqLO.ItemIndex := -1;
  cbPesqUFD.ItemIndex := -1;
  cbPesqLD.ItemIndex := -1;
  cbPesqCliente.ItemIndex := -1;
  edData.Clear;
  QTabela.Close;
  QTabela.sql.Text := sql;
//showmessage(qtabela.SQL.Text);
  QTabela.Open;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  PageControl1.ActivePageIndex := 0;
  //sql := '';
end;

procedure TfrmCadTabelaControlerVenda.cbPesqLDChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFD.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLD.Text, 1, 1));
  QCidade.Open;
  cbCidDPesq.clear;
  cbCidDPesq.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidDPesq.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaControlerVenda.cbPesqLOChange(Sender: TObject);
begin
  QCidade.Close;
  QCidade.Parameters[0].value := cbPesqUFO.Text;
  QCidade.sql[4] := 'And codipv = ' + QuotedStr(copy(cbPesqLO.Text, 1, 1));
  QCidade.Open;
  cbCidOPesq.clear;
  cbCidOPesq.Items.add('');
  while not QCidade.Eof do
  begin
    cbCidOPesq.Items.add(QCidadeDESCRI.AsString);
    QCidade.Next;
  end;
end;

procedure TfrmCadTabelaControlerVenda.dbgContratoTitleClick(Column: TColumn);
var
  icount: integer;
begin
  QTabela.close;
  if Pos('order by', QTabela.SQL.Text) > 0 then
  begin
    QTabela.SQL.Text := copy(QTabela.SQL.Text, 1,
      Pos('order by', QTabela.SQL.Text) - 1) + 'order by ' + Column.FieldName;
  end
  else
    QTabela.SQL.Text := QTabela.SQL.Text + ' order by ' + Column.FieldName;
  QTabela.Open;
  // Muda a cor da coluna do grid
  for icount := 0 to dbgContrato.Columns.Count - 1 do
    dbgContrato.Columns[icount].Title.font.color := clNavy;
  Column.Title.font.color := clRed;
end;

procedure TfrmCadTabelaControlerVenda.FormActivate(Sender: TObject);
begin
  PageControl1.ActivePage := Consulta;
end;

procedure TfrmCadTabelaControlerVenda.FormCreate(Sender: TObject);
begin
  sql := QTabela.sql.Text;
  QTabela.Open;
  quant := QTabela.RecordCount;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  QCliente.Open;
  cbPesqCliente.clear;
  cbPesqCliente.Items.Add('');
  While not QCliente.Eof do
  begin
    cbPesqCliente.Items.Add(alltrim(QClienteNM_CLIENTE.value));
    QCliente.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct operacao from tb_tabelavenda where fl_status = ''S'' and controler is null order by 1');
  dtmDados.iQuery1.Open;
  cbOperacaoPesq.clear;
  cbOperacaoPesq.Items.add('');
  while not dtmDados.iQuery1.Eof do
  begin
    cbOperacaoPesq.Items.add(dtmDados.iQuery1.FieldByName('operacao').value);
    dtmDados.iQuery1.Next;
  end;

  dtmDados.iQuery1.Close;
  dtmDados.iQuery1.sql.clear;
  dtmDados.iQuery1.sql.add
    ('select distinct veiculo from tb_tabelavenda where veiculo is not null and fl_status = ''S'' and controler is null order by 1');
  dtmDados.iQuery1.Open;

  cbVeiculoPesq.clear;
  cbVeiculoPesq.Items.add('');
  while not dtmDados.iQuery1.Eof do
  begin
    cbVeiculoPesq.Items.add(dtmDados.iQuery1.FieldByName('veiculo').value);
    dtmDados.iQuery1.Next;
  end;

end;

procedure TfrmCadTabelaControlerVenda.QTabelaAfterScroll(DataSet: TDataSet);
begin
  Qcadtabela.close;
  Qcadtabela.Parameters[0].Value := QTabelaCOD_VENDA.Value;
  Qcadtabela.Open;
  QItem.close;
  QItem.Parameters[0].Value := QTabelaCOD_VENDA.Value;
  QItem.Open;
  QItemRee.Close;
  QItemRee.Parameters[0].value := QTabelaCOD_VENDA.value;
  QItemRee.Open;
  QAereo.Close;
  QAereo.Parameters[0].value := QTabelaCOD_VENDA.value;
  QAereo.Open;


  edVeiculo.Text := QTabelaVEICULO.AsString;
  edTrans.Text := QTabelaNM_CLIENTE.Value;
  edFreteM.Value := QTabelaVL_FRETE_MINIMO.Value;
  if edFreteM.Value > 0 then
    edFreteM.color := $0080FFFF
  else
    edFreteM.color := clWhite;
  edPedagio.Value := QTabelaVL_PEDAGIO.Value;
  if edPedagio.Value > 0 then
    edPedagio.color := $0080FFFF
  else
    edPedagio.color := clWhite;
  edOutros.Value := QTabelaVL_OUTROS.Value;
  if edOutros.Value > 0 then
    edOutros.color := $0080FFFF
  else
    edOutros.color := clWhite;
  edOutrosP.Value := QTabelaVL_OUTROS_P.Value;
  if edOutrosP.Value > 0 then
    edOutrosP.color := $0080FFFF
  else
    edOutrosP.color := clWhite;
  edOutrosM.Value := QTabelaVL_OUTROS_MINIMO.Value;
  if edOutrosM.Value > 0 then
    edOutrosM.color := $0080FFFF
  else
    edOutrosM.color := clWhite;
  edExc.Value := QTabelaVL_EXCEDENTE.Value;
  if edExc.Value > 0 then
    edExc.color := $0080FFFF
  else
    edExc.color := clWhite;
  EdSeg.Value := QTabelaVL_AD.Value;
  if EdSeg.Value > 0 then
    EdSeg.color := $0080FFFF
  else
    EdSeg.color := clWhite;
  EdGris.Value := QTabelaVL_GRIS.Value;
  if EdGris.Value > 0 then
    EdGris.color := $0080FFFF
  else
    EdGris.color := clWhite;
  EdGrisM.Value := QTabelaVL_GRIS_MINIMO.Value;
  if EdGrisM.Value > 0 then
    EdGrisM.color := $0080FFFF
  else
    EdGrisM.color := clWhite;
  eddespacho.Value := QTabelaVL_DESPACHO.Value;
  if eddespacho.Value > 0 then
    eddespacho.color := $0080FFFF
  else
    eddespacho.color := clWhite;
  edsegminimo.Value := QcadtabelaSEGURO_MINIMO.Value;
  if edsegminimo.Value > 0 then
    edsegminimo.color := $0080FFFF
  else
    edsegminimo.color := clWhite;
  edTDE.Value := QTabelaVL_TDE.Value;
  if edTDE.Value > 0 then
    edTDE.color := $0080FFFF
  else
    edTDE.color := clWhite;
  edTDEp.Value := QTabelaVL_TDEP.Value;
  if edTDEp.Value > 0 then
    edTDEp.color := $0080FFFF
  else
    edTDEp.color := clWhite;
  edTDEm.Value := QTabelaVL_TDEM.Value;
  if edTDEm.Value > 0 then
    edTDEm.color := $0080FFFF
  else
    edTDEm.color := clWhite;
  edTR.Value := QcadtabelaVL_TR.Value;
  if edTR.Value > 0 then
    edTR.color := $0080FFFF
  else
    edTR.color := clWhite;
  edTdeMax.Value := QcadtabelaVL_TDEMAX.Value;
  if edTdeMax.Value > 0 then
    edTdeMax.color := $0080FFFF
  else
    edTdeMax.color := clWhite;
  ed_fluvial.Value := QcadtabelaVL_FLUVIAL.Value;
  if ed_fluvial.Value > 0 then
    ed_fluvial.color := $0080FFFF
  else
    ed_fluvial.color := clWhite;
  ed_fluv_min.Value := QcadtabelaVL_FLUVIAL_M.Value;
  if ed_fluv_min.Value > 0 then
    ed_fluv_min.color := $0080FFFF
  else
    ed_fluv_min.color := clWhite;
  edseg_balsa.Value := QcadtabelaVL_SEG_BALSA.Value;
  if edseg_balsa.Value > 0 then
    edseg_balsa.color := $0080FFFF
  else
    edseg_balsa.color := clWhite;
  edred_fluv.Value := QcadtabelaVL_REDEP_FLUVIAL.Value;
  if edred_fluv.Value > 0 then
    edred_fluv.color := $0080FFFF
  else
    edred_fluv.color := clWhite;
  edagend.Value := QcadtabelaVL_AGEND.Value;
  if edagend.Value > 0 then
    edagend.color := $0080FFFF
  else
    edagend.color := clWhite;
  edpallet.Value := QcadtabelaVL_PALLET.Value;
  if edpallet.Value > 0 then
    edpallet.color := $0080FFFF
  else
    edpallet.color := clWhite;
  edtas.Value := QcadtabelaVL_TAS.Value;
  if edtas.Value > 0 then
    edtas.color := $0080FFFF
  else
    edtas.color := clWhite;
  edentrega.Value := QcadtabelaVL_ENTREGA_PORTO.Value;
  if edentrega.Value > 0 then
    edentrega.color := $0080FFFF
  else
    edentrega.color := clWhite;
  edalfand.Value := QcadtabelaVL_ALFAND.Value;
  if edalfand.Value > 0 then
    edalfand.color := $0080FFFF
  else
    edalfand.color := clWhite;
  edcanhoto.Value := QcadtabelaVL_CANHOTO.Value;
  if edcanhoto.Value > 0 then
    edcanhoto.color := $0080FFFF
  else
    edcanhoto.color := clWhite;

  edDevol.Value := QcadtabelaVL_DEVOLUCAO.Value;
  if edDevol.Value > 0 then
    edDevol.color := $0080FFFF
  else
    edDevol.color := clWhite;

  if QcadtabelaFL_IMPOSTO.value = 'S' then
    cbImposto.Checked := true
  else
    cbImposto.Checked := false;

  if QcadtabelaFL_IMPOSTO_ICMS.value = 'S' then
    cbImpostoICMS.Checked := true
  else
    cbImpostoICMS.Checked := false;


  if QTabelaFL_PESO.Value = 'S' then
    cbPeso.Checked := True
  else
    cbPeso.Checked := false;

  if QTabelaFL_STATUS.Value = 'S' then
    cbStatus.Checked := True
  else
    cbStatus.Checked := false;

  if QTabelaFL_RATEIO.Value = 'S' then
    cbRateio.Checked := True
  else
    cbRateio.Checked := false;

  edFluvMin.Value := QcadtabelaVL_FLUVMIN.Value;
  if edFluvMin.Value > 0 then
    edFluvMin.color := $0080FFFF
  else
    edFluvMin.color := clWhite;

  edAjuda.Value := QcadtabelaVL_AJUDA.Value;
  if edAjuda.Value > 0 then
    edAjuda.color := $0080FFFF
  else
    edAjuda.color := clWhite;

  edRMinimo.Value := QcadtabelaVL_REENT_MINIMA.Value;
  if edRMinimo.Value > 0 then
    edRMinimo.color := $0080FFFF
  else
    edRMinimo.color := clWhite;

  ed6x1.Value := QcadtabelaVL_6X1.Value;
  edqt6x1.Value := QcadtabelaQT6x1.Value;

  // Implementa��o do projeto de generalidades
  edArmazem.Value := QTabelaVL_ARMAZENAGEM.AsFloat;
  if edArmazem.Value > 0 then
    edArmazem.color := $0080FFFF
  else
    edArmazem.color := clWhite;

  edDiaria.Value := QTabelaVL_DIARIA.Value;
  if edDiaria.Value > 0 then
    edDiaria.color := $0080FFFF
  else
    edDiaria.color := clWhite;

  edReentrega.Value := QTabelaVL_REENTREGA.Value;
  if edReentrega.Value > 0 then
    edReentrega.color := $0080FFFF
  else
    edReentrega.color := clWhite;

  edDedicado.Value := QTabelaVL_DEDICADO.Value;
  if edDedicado.Value > 0 then
    edDedicado.color := $0080FFFF
  else
    edDedicado.color := clWhite;

  edPernoite.Value := QTabelaVL_PERNOITE.Value;
  if edPernoite.Value > 0 then
    edPernoite.color := $0080FFFF
  else
    edPernoite.color := clWhite;

  edHoraParada.Value := QTabelaVL_HORA_PARADA.Value;
  if edHoraParada.Value > 0 then
    edHoraParada.color := $0080FFFF
  else
    edHoraParada.color := clWhite;

  edfretepeso.Value := QTabelaVL_FRETE_VALOR.Value;
  if edfretepeso.Value > 0 then
    edfretepeso.color := $0080FFFF
  else
    edfretepeso.color := clWhite;

  edtaxatrt.Value := QTabelaVL_TAXA_TRT.Value;
  if edtaxatrt.Value > 0 then
    edtaxatrt.color := $0080FFFF
  else
    edtaxatrt.color := clWhite;

  edTDA.Value := QTabelaVL_TDA.Value;
  if edTDA.Value > 0 then
    edTDA.color := $0080FFFF
  else
    edTDA.color := clWhite;
  edTDAp.Value := QTabelaVL_TDAP.Value;
  if edTDAp.Value > 0 then
    edTDAp.color := $0080FFFF
  else
    edTDAp.color := clWhite;
  edTDAmin.Value := QTabelaVL_TDAmin.Value;
  if edTDAmin.Value > 0 then
    edTDAmin.color := $0080FFFF
  else
    edTDAmin.color := clWhite;

  Obs.Clear;
  Obs.Lines.add(QcadtabelaOBS.AsString);
  edDtI.Date := QTabelaDT_INICIAL.Value;
  edDtF.Date := QTabelaDT_FINAL.Value;

  edUser.Text := QcadtabelaUSUARIO.AsString;
  edDtIns.Date := QcadtabelaDT_CADASTRO.Value;

  obsRepro.Clear;
  obsRepro.Lines.add(Qcadtabelareprovado.AsString);

  if QcadtabelaDOC_CONTROLER.IsNull then
    btnVer.Visible := false
  else
    btnVer.Visible := True;

  edAgendG.value := Qcadtabelavl_agendg.value;
  edDiaria48.Value := QcadtabelaVL_DIARIA48.Value;

  QGenItem.close;
  QGenItem.Parameters[0].Value := QTabelaCOD_VENDA.Value;
  QGenItem.Open;

end;

procedure TfrmCadTabelaControlerVenda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QTabela.close;
  QItem.close;
  Qcadtabela.close;
end;

procedure TfrmCadTabelaControlerVenda.dbgContratoDblClick(Sender: TObject);
var
  Retorno: String;
begin
  Retorno := InserirValor('Procurar por ' + dbgContrato.Columns.Items
    [dbgContrato.SelectedIndex].Title.Caption, Caption);
  if Retorno = '' then
    Exit;
  if not QTabela.Locate(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
    .FieldName, Retorno, [loPartialKey]) then
    ShowMessage(dbgContrato.Columns.Items[dbgContrato.SelectedIndex]
      .Title.Caption + ' n�o localizado');
end;

procedure TfrmCadTabelaControlerVenda.acPrimeiroApanhaExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  case (Sender as TAction).Tag of
    1:
      QTabela.First;
    2:
      QTabela.Prior;
    3:
      QTabela.Next;
    4:
      QTabela.Last;
  end;
  lblQuant.Caption := IntToStr(QTabela.RecNo) + #13#10 + IntToStr(quant);
  Screen.Cursor := crDefault;
end;

end.
