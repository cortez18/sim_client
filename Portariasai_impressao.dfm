object frmPortariasai_impressao: TfrmPortariasai_impressao
  Left = 0
  Top = 0
  Caption = 'frmPortariasai_impressao'
  ClientHeight = 552
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 5
    Top = 0
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    BeforePrint = RLReport1BeforePrint
    object RLBand1: TRLBand
      Left = 38
      Top = 38
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel1: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel2: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel3: TRLLabel
        Left = 12
        Top = 212
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel4: TRLLabel
        Left = 9
        Top = 100
        Width = 42
        Height = 16
        Caption = 'Frota :'
      end
      object RLLabel11: TRLLabel
        Left = 12
        Top = 236
        Width = 31
        Height = 16
        Caption = 'RG :'
      end
      object RLLabel12: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel15: TRLLabel
        Left = 264
        Top = 27
        Width = 135
        Height = 16
        Caption = 'REGISTRO DE SA'#205'DA'
      end
      object RLLabel17: TRLLabel
        Left = 456
        Top = 236
        Width = 39
        Height = 16
        Caption = 'CNH :'
      end
      object RLLabel19: TRLLabel
        Left = 240
        Top = 236
        Width = 38
        Height = 16
        Alignment = taRightJustify
        Caption = 'CPF :'
      end
      object RLSystemInfo3: TRLSystemInfo
        Left = 676
        Top = 6
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
      object RLMemo1: TRLMemo
        AlignWithMargins = True
        Left = 8
        Top = 277
        Width = 702
        Height = 49
        Alignment = taJustify
        AutoSize = False
        Behavior = [beSiteExpander]
        Lines.Strings = (
          
            'Declaro, que recebi e conferi, para fins de entrega aos respecti' +
            'vos clientes, todas as mercadorias e volumes constantes das Nota' +
            's Fiscais relacionadas em anexo, assim como todos os documentos ' +
            'fiscais pertinentes, assumindo sobre os mesmos toda e qualquer r' +
            'esponsabilidade pela entrega aos destinat'#225'rios.')
      end
      object RLDraw1: TRLDraw
        AlignWithMargins = True
        Left = 0
        Top = 266
        Width = 717
        Height = 1
      end
      object RLLabel13: TRLLabel
        Left = 46
        Top = 411
        Width = 199
        Height = 16
        Caption = 'Data : _______/ ______/ _______'
      end
      object RLLabel22: TRLLabel
        Left = 297
        Top = 409
        Width = 392
        Height = 16
        Caption = 'Motorista : ______________________________________________'
      end
      object RLDraw2: TRLDraw
        Left = 1
        Top = 134
        Width = 717
        Height = 72
      end
      object RLLabel10: TRLLabel
        Left = 9
        Top = 138
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLLabel5: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLMemo2: TRLMemo
        Left = 12
        Top = 162
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
    object RLBand2: TRLBand
      Left = 38
      Top = 555
      Width = 718
      Height = 517
      BandType = btColumnHeader
      Borders.Sides = sdCustom
      Borders.DrawLeft = True
      Borders.DrawTop = True
      Borders.DrawRight = True
      Borders.DrawBottom = True
      object RLLabel6: TRLLabel
        Left = 9
        Top = 5
        Width = 58
        Height = 16
      end
      object RLLabel7: TRLLabel
        Left = 9
        Top = 55
        Width = 98
        Height = 16
        Caption = 'Transportadora :'
      end
      object RLLabel8: TRLLabel
        Left = 12
        Top = 212
        Width = 66
        Height = 16
        Caption = 'Motorista :'
      end
      object RLLabel9: TRLLabel
        Left = 9
        Top = 100
        Width = 42
        Height = 16
        Caption = 'Frota :'
      end
      object RLLabel14: TRLLabel
        Left = 12
        Top = 236
        Width = 31
        Height = 16
        Caption = 'RG :'
      end
      object RLLabel16: TRLLabel
        Left = 9
        Top = 78
        Width = 45
        Height = 16
        Caption = 'Placa :'
      end
      object RLLabel18: TRLLabel
        Left = 264
        Top = 27
        Width = 135
        Height = 16
        Caption = 'REGISTRO DE SA'#205'DA'
      end
      object RLLabel20: TRLLabel
        Left = 456
        Top = 236
        Width = 39
        Height = 16
        Caption = 'CNH :'
      end
      object RLLabel21: TRLLabel
        Left = 240
        Top = 236
        Width = 38
        Height = 16
        Alignment = taRightJustify
        Caption = 'CPF :'
      end
      object RLSystemInfo1: TRLSystemInfo
        Left = 676
        Top = 6
        Width = 37
        Height = 16
        Alignment = taRightJustify
        Info = itNow
        Text = ''
      end
      object RLMemo3: TRLMemo
        AlignWithMargins = True
        Left = 8
        Top = 277
        Width = 702
        Height = 49
        Alignment = taJustify
        AutoSize = False
        Behavior = [beSiteExpander]
        Lines.Strings = (
          
            'Declaro, que recebi e conferi, para fins de entrega aos respecti' +
            'vos clientes, todas as mercadorias e volumes constantes das Nota' +
            's Fiscais relacionadas em anexo, assim como todos os documentos ' +
            'fiscais pertinentes, assumindo sobre os mesmos toda e qualquer r' +
            'esponsabilidade pela entrega aos destinat'#225'rios.')
      end
      object RLDraw3: TRLDraw
        AlignWithMargins = True
        Left = 0
        Top = 266
        Width = 717
        Height = 1
      end
      object RLLabel23: TRLLabel
        Left = 46
        Top = 411
        Width = 199
        Height = 16
        Caption = 'Data : _______/ ______/ _______'
      end
      object RLLabel24: TRLLabel
        Left = 297
        Top = 409
        Width = 392
        Height = 16
        Caption = 'Motorista : ______________________________________________'
      end
      object RLDraw4: TRLDraw
        Left = 1
        Top = 134
        Width = 717
        Height = 72
      end
      object RLLabel25: TRLLabel
        Left = 9
        Top = 138
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o'
      end
      object RLLabel26: TRLLabel
        Left = 214
        Top = 78
        Width = 32
        Height = 16
        Caption = 'KM :'
      end
      object RLMemo4: TRLMemo
        Left = 12
        Top = 162
        Width = 697
        Height = 39
        AutoSize = False
        Behavior = [beSiteExpander]
      end
    end
  end
end
